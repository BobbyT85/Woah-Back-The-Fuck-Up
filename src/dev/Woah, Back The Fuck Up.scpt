FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 ^ X Donkey icon/logo taken from http://thedeciderist.com/2013/01/29/the-party-of-less-evil/    6 � 7 7 �   D o n k e y   i c o n / l o g o   t a k e n   f r o m   h t t p : / / t h e d e c i d e r i s t . c o m / 2 0 1 3 / 0 1 / 2 9 / t h e - p a r t y - o f - l e s s - e v i l / 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    E � F F   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x C  G H G l     ��������  ��  ��   H  I J I l     ��������  ��  ��   J  K L K l     ��������  ��  ��   L  M N M l     ��������  ��  ��   N  O P O l     ��������  ��  ��   P  Q R Q j     �� S��  0 currentversion currentVersion S m      T T ?�       R  U V U l     ��������  ��  ��   V  W X W l     ��������  ��  ��   X  Y Z Y l     ��������  ��  ��   Z  [ \ [ l     ��������  ��  ��   \  ] ^ ] l     ��������  ��  ��   ^  _ ` _ l     �� a b��   a . ( Properties and includes for the menubar    b � c c P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r `  d e d x    
�� f g��   f 1      ��
�� 
ascr g �� h��
�� 
minv h m       i i � j j  2 . 4��   e  k l k x   
 �� m����   m 2   ��
�� 
osax��   l  n o n x    #�� p����   p 4    �� q
�� 
frmk q m     r r � s s  F o u n d a t i o n��   o  t u t x   # 0�� v����   v 4   % )�� w
�� 
frmk w m   ' ( x x � y y  A p p K i t��   u  z { z l     ��������  ��  ��   {  | } | j   0 2�� ~�� 0 
statusitem 
StatusItem ~ m   0 1��
�� 
msng }   �  l     ��������  ��  ��   �  � � � j   3 5�� ��� 0 
thedisplay 
theDisplay � m   3 4 � � � � �   �  � � � j   6 <�� ��� 0 defaults   � 4   6 ;�� �
�� 
pcls � m   8 9 � � � � �  N S U s e r D e f a u l t s �  � � � j   = E�� ��� $0 internalmenuitem internalMenuItem � 4   = D�� �
�� 
pcls � m   ? B � � � � �  N S M e n u I t e m �  � � � j   F N�� ��� $0 externalmenuitem externalMenuItem � 4   F M�� �
�� 
pcls � m   H K � � � � �  N S M e n u I t e m �  � � � j   O W�� ��� 0 newmenu newMenu � 4   O V�� �
�� 
pcls � m   Q T � � � � �  N S M e n u �  � � � l     ��������  ��  ��   �  � � � j   X l�� ��� 0 thelist theList � J   X k � �  � � � m   X [ � � � � �  B a c k u p   n o w �  � � � m   [ ^ � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   ^ a � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   a d � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   d g � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   m o�� ��� 0 isnsfw isNSFW � m   m n��
�� boovfals �  � � � j   p r�� ��� 0 tonotify toNotify � m   p q��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   s u�� ���  0 backupthenquit backupThenQuit � m   s t��
�� boovfals �  � � � l     ����~��  �  �~   �  � � � j   v x�} ��} 0 	idleready 	idleReady � m   v w�|
�| boovfals �  � � � j   y {�{ ��{ 0 isconnected isConnected � m   y z�z
�z boovtrue �  � � � l     �y�x�w�y  �x  �w   �  � � � j   | ��v ��v 0 
updatefile 
updateFile � m   |  � � � � �   �  � � � j   � ��u ��u 0 
newversion 
newVersion � m   � ��t�t   �  � � � l     �s�r�q�s  �r  �q   �  � � � l     �p � ��p   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �o � ��o   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     �n�m�l�n  �m  �l   �  � � � l     �k�j�i�k  �j  �i   �  � � � l     �h�g�f�h  �g  �f   �  � � � l     �e�d�c�e  �d  �c   �  � � � l     �b�a�`�b  �a  �`   �  � � � l     �_ � ��_   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   � � � � �^ ��^ 0 sourcefolder sourceFolder � �] ��] "0 destinationdisk destinationDisk � �\ ��\ 	0 drive   � �[ ��[ 0 machinefolder machineFolder � �Z ��Z 0 backupfolder backupFolder � �Y ��Y (0 activesourcefolder activeSourceFolder � �X ��X (0 activebackupfolder activeBackupFolder � �W�V�W 0 latestfolder latestFolder�V   �  � � � p   � � � � �U ��U 0 intervaltime intervalTime � �T ��T 0 	starttime 	startTime � �S�R�S 0 endtime endTime�R   �    p   � � �Q�P�Q 0 appicon appIcon�P    l     �O�N�M�O  �N  �M    l    �L�K r     	 4     �J

�J 
alis
 l   �I�H b     l   	�G�F I   	�E
�E .earsffdralis        afdr  f     �D�C
�D 
rtyp m    �B
�B 
ctxt�C  �G  �F   m   	 
 � < C o n t e n t s : R e s o u r c e s : a p p l e t . i c n s�I  �H  	 o      �A�A 0 appicon appIcon�L  �K    l     �@�?�>�@  �?  �>    l     �=�<�;�=  �<  �;    l     �:�9�8�:  �9  �8    l     �7�6�5�7  �6  �5    l     �4�3�2�4  �3  �2    l     �1 !�1      Property assignment   ! �"" (   P r o p e r t y   a s s i g n m e n t #$# j   � ��0%�0 	0 plist  % b   � �&'& n  � �()( 1   � ��/
�/ 
psxp) l  � �*�.�-* I  � ��,+,
�, .earsffdralis        afdr+ m   � ��+
�+ afdrdlib, �*-�)
�* 
from- m   � ��(
�( fldmfldu�)  �.  �-  ' m   � �.. �// f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t$ 010 l     �'�&�%�'  �&  �%  1 232 j   � ��$4�$ 0 initialbackup initialBackup4 m   � ��#
�# boovtrue3 565 j   � ��"7�" 0 manualbackup manualBackup7 m   � ��!
�! boovfals6 898 j   � �� :�  0 isbackingup isBackingUp: m   � ��
� boovfals9 ;<; l     ����  �  �  < =>= j   � ��?� "0 messagesmissing messagesMissing? J   � �@@ ABA m   � �CC �DD t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . .B EFE m   � �GG �HH V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . .F IJI m   � �KK �LL ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !J MNM m   � �OO �PP < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ?N Q�Q m   � �RR �SS � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�  > TUT l     ����  �  �  U VWV j   � ��X� *0 messagesencouraging messagesEncouragingX J   � �YY Z[Z m   � �\\ �]]  C o m e   o n ![ ^_^ m   � �`` �aa 4 C o m e   o n !   E y e   o f   t h e   t i g e r !_ bcb m   � �dd �ee " N o   p a i n !   N o   p a i n !c fgf m   � �hh �ii 0 L e t ' s   d o   t h i s   a s   a   t e a m !g jkj m   � �ll �mm  W e   c a n   d o   i t !k non m   � �pp �qq  F r e e e e e e e e e d o m !o rsr m   � �tt �uu 2 A l t o g e t h e r   o r   n o t   a t   a l l !s vwv m   � �xx �yy H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !w z{z m   � �|| �}} H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !{ ~�~ m   � � ���  Y o u   s e x y   f u c k !�  W ��� l     ����  �  �  � ��� j   ���� $0 messagescomplete messagesComplete� J   ��� ��� m   � ��� ���  N i c e   o n e !� ��� m   � ��� ��� * Y o u   a b s o l u t e   l e g   e n d !� ��� m   � ��� ���  G o o d   l a d !� ��� m   � ��� ���  P e r f i c k !� ��� m   � ��� ���  H a p p y   d a y s !� ��� m   � ��� ���  F r e e e e e e e e e d o m !� ��� m   � ��� ��� H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !� ��� m   ��� ��� B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !� ��� m  �� ��� d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !� ��� m  �� ���  Y o u   s e x y   f u c k !�  � ��� l     ����  �  �  � ��� j  1��� &0 messagescancelled messagesCancelled� J  .�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ���  A h   m a n ,   u n l u c k y� ��� m  �� ���  O h   w e l l� ��� m  �� ��� @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e n� ��� m  �� ��� , W e l l   t h a t ' s   a   l e t   d o w n� ��� m  !�� ��� P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?� ��� m  !$�� ���  A r r o g a n t� ��� m  $'�� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0� ��� m  '*�� ���  E l l   b e n d�  � ��� l     �
�	��
  �	  �  � ��� j  2T��� 40 messagesalreadybackingup messagesAlreadyBackingUp� J  2Q�� ��� m  25�� ���  T h a t ' s   a   s h a m e� ��� m  58�� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  8;�� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  ;>�� ���  D i c k� ��� m  >A�� ���  F u c k t a r d� ��� m  AD�� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  DG�� ���  A r r o g a n t� ��� m  GJ�� ��� $ C h i l l   t h e   f u c k   o u t� ��� m  JM�� ���  H o l d   f i r e�  �    l     ����  �  �    j  U[�� 0 strawake strAwake m  UX � * " C u r r e n t P o w e r S t a t e " = 4  j  \b�	� 0 strsleep strSleep	 m  \_

 � * " C u r r e n t P o w e r S t a t e " = 1  j  cm� �  &0 displaysleepstate displaySleepState I cj����
�� .sysoexecTEXT���     TEXT m  cf � j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t��    l     ��������  ��  ��    j  nt���� 0 	temptitle 	tempTitle m  nq � , W o a h ,   B a c k   T h e   F u c k   U p  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��     l     ��������  ��  ��    !"! l     ��������  ��  ��  " #$# l     ��%&��  % � �-----------------------------------------------------------------------------------------------------------------------------------------------   & �'' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -$ ()( l     ��*+��  * � �-----------------------------------------------------------------------------------------------------------------------------------------------   + �,, - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -) -.- l     ��/0��  / � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   0 �11 - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -. 232 l     ��������  ��  ��  3 454 l     ��67��  6 6 0 Function to check for a .plist preferences file   7 �88 `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e5 9:9 l     ��;<��  ; m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   < �== �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e: >?> l     ��@A��  @ b \ If a .plist file is present, it assigns preferences to their corresponding global variables   A �BB �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s? CDC i  uxEFE I      �������� 0 	plistinit 	plistInit��  ��  F k    �GG HIH q      JJ ��K�� 0 
foldername 
folderNameK ��L�� 0 
backupdisk 
backupDiskL ��M�� 0 
userfolder 
userFolderM ������ 0 
backuptime 
backupTime��  I NON r     PQP m     ����  Q o      ���� 0 
backuptime 
backupTimeO RSR l   ��������  ��  ��  S TUT Z   �VW��XV l   Y����Y =    Z[Z I    ��\���� 0 itexists itExists\ ]^] m    __ �``  f i l e^ a��a o    ���� 	0 plist  ��  ��  [ m    ��
�� boovtrue��  ��  W k    ibb cdc O    cefe k    bgg hih r    $jkj n    "lml 1     "��
�� 
pcntm 4     ��n
�� 
plifn o    ���� 	0 plist  k o      ���� 0 thecontents theContentsi opo r   % *qrq n   % (sts 1   & (��
�� 
valLt o   % &���� 0 thecontents theContentsr o      ���� 0 thevalue theValuep uvu l  + +��������  ��  ��  v wxw r   + 4yzy n   + .{|{ o   , .���� 0 
appversion 
AppVersion| o   + ,���� 0 thevalue theValuez o      ����  0 currentversion currentVersionx }~} r   5 :� n   5 8��� o   6 8����  0 foldertobackup FolderToBackup� o   5 6���� 0 thevalue theValue� o      ���� 0 
foldername 
folderName~ ��� r   ; @��� n   ; >��� o   < >���� 0 backupdrive BackupDrive� o   ; <���� 0 thevalue theValue� o      ���� 0 
backupdisk 
backupDisk� ��� r   A F��� n   A D��� o   B D����  0 computerfolder ComputerFolder� o   A B���� 0 thevalue theValue� o      ���� 0 
userfolder 
userFolder� ��� r   G L��� n   G J��� o   H J���� 0 scheduledtime ScheduledTime� o   G H���� 0 thevalue theValue� o      ���� 0 
backuptime 
backupTime� ��� r   M V��� n   M P��� o   N P���� 0 nsfw NSFW� o   M N���� 0 thevalue theValue� o      ���� 0 isnsfw isNSFW� ��� r   W `��� n   W Z��� o   X Z���� 0 notifications Notifications� o   W X���� 0 thevalue theValue� o      ���� 0 tonotify toNotify� ��� l  a a��������  ��  ��  � ���� l  a a������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  f m    ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  d ��� l  d d��������  ��  ��  � ���� I   d i�������� 0 	initmenus 	initMenus��  ��  ��  ��  X k   l��� ��� r   l s��� I   l q�������� .0 getcomputeridentifier getComputerIdentifier��  ��  � o      ���� 0 
userfolder 
userFolder� ��� l  t t��������  ��  ��  � ��� O   tC��� t   xB��� k   |A�� ��� n  | ���� I   } ��������� 0 setfocus setFocus��  ��  �  f   | }� ��� l  � ���������  ��  ��  � ��� r   � ���� m   � ��� ���L T h i s   p r o g r a m   a u t o m a t i c a l l y   b a c k s   u p   y o u r   H o m e   f o l d e r   b u t   e x c l u d e s   t h e   C a c h e s ,   L o g s   a n d   M o b i l e   D o c u m e n t s   f o l d e r s   f r o m   y o u r   L i b r a r y . 
 
 A l l   y o u   n e e d   t o   d o   i s   s e l e c t   t h e   e x t e r n a l   d r i v e   t o   b a c k u p   t o   a t   t h e   n e x t   p r o m p t . 
 
 Y o u r   b a c k u p s   w i l l   b e   s a v e d   i n : 
 [ E x t e r n a l   H a r d   D r i v e ]   >   B a c k u p s   >   [ C o m p u t e r   N a m e ]� o      ���� 0 welcome  � ��� l  � ���������  ��  ��  � ��� l   � �������  � � �if (isNSFW = true) then					set tempTitle to "Woah, Back The Fuck Up"				else if (isNSFW = false) then					set tempTitle to "WBTFU"				end if   � ���" i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 	 	 e n d   i f� ��� l  � ���������  ��  ��  � ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
bhit� l  � ������� I  � �����
�� .sysodlogaskr        TEXT� o   � ����� 0 welcome  � ����
�� 
appr� o   � ����� 0 	temptitle 	tempTitle� ����
�� 
disp� o   � ����� 0 appicon appIcon� ����
�� 
btns� J   � ��� ���� m   � ��� ���  O K ,   g o t   i t��  � ���~
� 
dflt� m   � ��}�} �~  ��  ��  ��  ��  � o      �|�| 	0 reply  � ��� l  � ��{�z�y�{  �z  �y  � ��� r   � ���� I  � ��x��w
�x .earsffdralis        afdr� l  � ���v�u� m   � ��t
�t afdrcusr�v  �u  �w  � o      �s�s 0 
foldername 
folderName� ��� l  � ��r�q�p�r  �q  �p  � ��� Q   � ����� r   � ���� c   � ���� l  � ���o�n� I  � ��m�l�
�m .sysostflalis    ��� null�l  � �k��j
�k 
prmp� m   � ��� ��� R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o�j  �o  �n  � m   � ��i
�i 
TEXT� o      �h�h 0 
backupdisk 
backupDisk� R      �g�f�e
�g .ascrerr ****      � ****�f  �e  � k   � ��� ��� l  � ��d���d  � 3 -do shell script "kill Woah, Back The Fuck Up"   � �   Z d o   s h e l l   s c r i p t   " k i l l   W o a h ,   B a c k   T h e   F u c k   U p "� �c n  � � I   � ��b�a�`�b 0 killapp killApp�a  �`    f   � ��c  �  l  � ��_�^�]�_  �^  �]    r   � �	 n   � �

 1   � ��\
�\ 
psxp o   � ��[�[ 0 
foldername 
folderName	 o      �Z�Z 0 
foldername 
folderName  r   � � n   � � 1   � ��Y
�Y 
psxp o   � ��X�X 0 
backupdisk 
backupDisk o      �W�W 0 
backupdisk 
backupDisk  l  � ��V�U�T�V  �U  �T    r   � � J   � �  m   � � � 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r  m   � � �   8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r !�S! m   � �"" �## , E v e r y   h o u r   o n   t h e   h o u r�S   o      �R�R 0 backuptimes backupTimes $%$ r   �&'& c   �()( J   �** +�Q+ I  ��P,-
�P .gtqpchltns    @   @ ns  , o   � ��O�O 0 backuptimes backupTimes- �N.�M
�N 
prmp. m   �// �00 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?�M  �Q  ) m  
�L
�L 
TEXT' o      �K�K 0 selectedtime selectedTime% 121 l �J�I�H�J  �I  �H  2 343 Z  ?567�G5 l 8�F�E8 =  9:9 o  �D�D 0 selectedtime selectedTime: m  ;; �<< 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r�F  �E  6 r  =>= m  �C�C > o      �B�B 0 
backuptime 
backupTime7 ?@? l #A�A�@A =  #BCB o  �?�? 0 selectedtime selectedTimeC m  "DD �EE 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r�A  �@  @ FGF r  &+HIH m  &)�>�> I o      �=�= 0 
backuptime 
backupTimeG JKJ l .3L�<�;L =  .3MNM o  ./�:�: 0 selectedtime selectedTimeN m  /2OO �PP , E v e r y   h o u r   o n   t h e   h o u r�<  �;  K Q�9Q r  6;RSR m  69�8�8 <S o      �7�7 0 
backuptime 
backupTime�9  �G  4 TUT l @@�6�5�4�6  �5  �4  U V�3V l @@�2WX�2  W . (log {folderName, backupDisk, backupTime}   X �YY P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�3  � m   x {�1�1  ��� m   t uZZ�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � [\[ l DD�0�/�.�0  �/  �.  \ ]�-] O  D�^_^ O  H�`a` k  c�bb cdc I c��,�+e
�, .corecrel****      � null�+  e �*fg
�* 
koclf m  gj�)
�) 
pliig �(hi
�( 
inshh  ;  moi �'j�&
�' 
prdtj K  r�kk �%lm
�% 
kindl m  ux�$
�$ 
TEXTm �#no
�# 
pnamn m  {~pp �qq  A p p V e r s i o no �"r�!
�" 
valLr o  �� �   0 currentversion currentVersion�!  �&  d sts I ����u
� .corecrel****      � null�  u �vw
� 
koclv m  ���
� 
pliiw �xy
� 
inshx  ;  ��y �z�
� 
prdtz K  ��{{ �|}
� 
kind| m  ���
� 
TEXT} �~
� 
pnam~ m  ���� ���  F o l d e r T o B a c k u p ���
� 
valL� o  ���� 0 
foldername 
folderName�  �  t ��� I �����
� .corecrel****      � null�  � ���
� 
kocl� m  ���
� 
plii� ���
� 
insh�  ;  ��� ���
� 
prdt� K  ���� ���
� 
kind� m  ���

�
 
TEXT� �	��
�	 
pnam� m  ���� ���  B a c k u p D r i v e� ���
� 
valL� o  ���� 0 
backupdisk 
backupDisk�  �  � ��� I ����
� .corecrel****      � null�  � ���
� 
kocl� m  ���
� 
plii� ���
� 
insh�  ;  ��� � ���
�  
prdt� K  � �� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  ������ 0 
userfolder 
userFolder��  ��  � ��� I .�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  ��
�� 
plii� ����
�� 
insh�  ;  � �����
�� 
prdt� K  (�� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  "�� ���  S c h e d u l e d T i m e� �����
�� 
valL� o  #$���� 0 
backuptime 
backupTime��  ��  � ��� I /Z�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  36��
�� 
plii� ����
�� 
insh�  ;  9;� �����
�� 
prdt� K  >T�� ����
�� 
kind� m  AD��
�� 
TEXT� ����
�� 
pnam� m  GJ�� ���  N S F W� �����
�� 
valL� o  KP���� 0 isnsfw isNSFW��  ��  � ���� I [������
�� .corecrel****      � null��  � ����
�� 
kocl� m  _b��
�� 
plii� ����
�� 
insh�  ;  eg� �����
�� 
prdt� K  j��� ����
�� 
kind� m  mp��
�� 
TEXT� ����
�� 
pnam� m  sv�� ���  N o t i f i c a t i o n s� �����
�� 
valL� o  w|���� 0 tonotify toNotify��  ��  ��  a l H`������ I H`�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  LM��
�� 
plif� �����
�� 
prdt� K  PZ�� �����
�� 
pnam� o  SX���� 	0 plist  ��  ��  ��  ��  _ m  DE���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �-  U ��� l ����������  ��  ��  � ��� r  ����� o  ������ 0 
foldername 
folderName� o      ���� 0 sourcefolder sourceFolder� ��� r  ����� o  ������ 0 
backupdisk 
backupDisk� o      ���� "0 destinationdisk destinationDisk� ��� r  ����� o  ������ 0 
userfolder 
userFolder� o      ���� 0 machinefolder machineFolder� ��� r  ����� o  ������ 0 
backuptime 
backupTime� o      ���� 0 intervaltime intervalTime� ��� I �������
�� .ascrcmnt****      � ****� J  ���� ��� o  ������ 0 sourcefolder sourceFolder� ��� o  ������ "0 destinationdisk destinationDisk� ��� o  ������ 0 machinefolder machineFolder� ���� o  ������ 0 intervaltime intervalTime��  ��  � ��� l ����������  ��  ��  � ���� I  ���������� 0 diskinit diskInit��  ��  ��  D ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  �    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   H B Function to detect if the selected hard drive is connected or not    � �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t 	
	 l     ����   T N This only happens once a hard drive has been selected and provides a reminder    � �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r
  i  y| I      �������� 0 diskinit diskInit��  ��   Z     ��� l    	���� =     	 I     ������ 0 itexists itExists  m     �  d i s k �� o    ���� "0 destinationdisk destinationDisk��  ��   m    ��
�� boovtrue��  ��   I    ������ 0 freespaceinit freeSpaceInit �� m    ��
�� boovfals��  ��  ��   t    � !  k    �"" #$# n   %&% I    �������� 0 setfocus setFocus��  ��  &  f    $ '(' r    &)*) n    $+,+ 3   " $��
�� 
cobj, o    "���� "0 messagesmissing messagesMissing* o      ���� 0 msg  ( -.- l  ' '��������  ��  ��  . /0/ l   ' '��12��  1 � �if (isNSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (isNSFW = false) then			set tempTitle to "WBTFU"		end if   2 �33 i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f0 454 l  ' '��������  ��  ��  5 676 r   ' A898 l  ' ?:��~: n   ' ?;<; 1   ; ?�}
�} 
bhit< l  ' ;=�|�{= I  ' ;�z>?
�z .sysodlogaskr        TEXT> o   ' (�y�y 0 msg  ? �x@A
�x 
appr@ o   ) .�w�w 0 	temptitle 	tempTitleA �vBC
�v 
dispB o   / 0�u�u 0 appicon appIconC �tDE
�t 
btnsD J   1 5FF GHG m   1 2II �JJ  C a n c e l   B a c k u pH K�sK m   2 3LL �MM  O K�s  E �rN�q
�r 
dfltN m   6 7�p�p �q  �|  �{  �  �~  9 o      �o�o 	0 reply  7 O�nO Z   B �PQ�mRP l  B GS�l�kS =   B GTUT o   B C�j�j 	0 reply  U m   C FVV �WW  O K�l  �k  Q I   J O�i�h�g�i 0 diskinit diskInit�h  �g  �m  R Z   R �XY�f�eX l  R ]Z�d�cZ E   R ][\[ o   R W�b�b &0 displaysleepstate displaySleepState\ o   W \�a�a 0 strawake strAwake�d  �c  Y k   ` �]] ^_^ r   ` i`a` n   ` gbcb 3   e g�`
�` 
cobjc o   ` e�_�_ &0 messagescancelled messagesCancelleda o      �^�^ 0 msg  _ d�]d Z   j �ef�\�[e l  j qg�Z�Yg =   j qhih o   j o�X�X 0 tonotify toNotifyi m   o p�W
�W boovtrue�Z  �Y  f Z   t �jkl�Vj l  t {m�U�Tm =   t {non o   t y�S�S 0 isnsfw isNSFWo m   y z�R
�R boovtrue�U  �T  k I  ~ ��Qpq
�Q .sysonotfnull��� ��� TEXTp o   ~ �P�P 0 msg  q �Or�N
�O 
apprr m   � �ss �tt > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�N  l uvu l  � �w�M�Lw =   � �xyx o   � ��K�K 0 isnsfw isNSFWy m   � ��J
�J boovfals�M  �L  v z�Iz I  � ��H{|
�H .sysonotfnull��� ��� TEXT{ m   � �}} �~~ , Y o u   s t o p p e d   b a c k i n g   u p| �G�F
�G 
appr m   � ��� ��� 
 W B T F U�F  �I  �V  �\  �[  �]  �f  �e  �n  ! m    �E�E  �� ��� l     �D�C�B�D  �C  �B  � ��� l     �A�@�?�A  �@  �?  � ��� l     �>�=�<�>  �=  �<  � ��� l     �;�:�9�;  �:  �9  � ��� l     �8�7�6�8  �7  �6  � ��� l     �5���5  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     �4���4  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     �3���3  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  }���� I      �2��1�2 0 freespaceinit freeSpaceInit� ��0� o      �/�/ 	0 again  �0  �1  � Z     ����.�� l    	��-�,� =     	��� I     �+��*�+ 0 comparesizes compareSizes� ��� o    �)�) 0 sourcefolder sourceFolder� ��(� o    �'�' "0 destinationdisk destinationDisk�(  �*  � m    �&
�& boovtrue�-  �,  � I    �%�$�#�% 0 
backupinit 
backupInit�$  �#  �.  � t    ���� k    ��� ��� n   ��� I    �"�!� �" 0 setfocus setFocus�!  �   �  f    � ��� l   ����  �  �  � ��� l    ����  � � �if (isNSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (isNSFW = false) then			set tempTitle to "WBTFU"		end if   � ��� i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f� ��� l   ����  �  �  � ��� Z    i����� l   ���� =    ��� o    �� 	0 again  � m    �
� boovfals�  �  � r   " <��� l  " :���� n   " :��� 1   6 :�
� 
bhit� l  " 6���� I  " 6���
� .sysodlogaskr        TEXT� m   " #�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ���
� 
appr� o   $ )�� 0 	temptitle 	tempTitle� ���
� 
disp� o   * +�
�
 0 appicon appIcon� �	��
�	 
btns� J   , 0�� ��� m   , -�� ���  Y e s� ��� m   - .�� ���  C a n c e l   B a c k u p�  � ���
� 
dflt� m   1 2�� �  �  �  �  �  � o      �� 	0 reply  � ��� l  ? B���� =   ? B��� o   ? @�� 	0 again  � m   @ A� 
�  boovtrue�  �  � ���� r   E e��� l  E c������ n   E c��� 1   _ c��
�� 
bhit� l  E _������ I  E _����
�� .sysodlogaskr        TEXT� m   E H�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
appr� o   I N���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   O P���� 0 appicon appIcon� ����
�� 
btns� J   Q Y�� ��� m   Q T�� ���  Y e s� ���� m   T W�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   Z [���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  ��  �  � ��� l  j j��������  ��  ��  � ���� Z   j ����� � l  j o���� =   j o o   j k���� 	0 reply   m   k n �  Y e s��  ��  � I   r w�������� 0 tidyup tidyUp��  ��  ��    k   z �  Z  z �	
����	 l  z ����� E   z � o   z ���� &0 displaysleepstate displaySleepState o    ����� 0 strawake strAwake��  ��  
 r   � � n   � � 3   � ���
�� 
cobj o   � ����� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��    l  � �����    end if    �  e n d   i f  l  � ���������  ��  ��   �� Z   � ����� l  � ����� =   � � o   � ����� 0 tonotify toNotify m   � ���
�� boovtrue��  ��   Z   � � !�� l  � �"����" =   � �#$# o   � ����� 0 isnsfw isNSFW$ m   � ���
�� boovtrue��  ��    I  � ���%&
�� .sysonotfnull��� ��� TEXT% o   � ����� 0 msg  & ��'��
�� 
appr' m   � �(( �)) > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  ! *+* l  � �,����, =   � �-.- o   � ����� 0 isnsfw isNSFW. m   � ���
�� boovfals��  ��  + /��/ I  � ���01
�� .sysonotfnull��� ��� TEXT0 m   � �22 �33 , Y o u   s t o p p e d   b a c k i n g   u p1 ��4��
�� 
appr4 m   � �55 �66 
 W B T F U��  ��  ��  ��  ��  ��  ��  � m    ����  ��� 787 l     ��������  ��  ��  8 9:9 l     ��������  ��  ��  : ;<; l     ��������  ��  ��  < =>= l     ��������  ��  ��  > ?@? l     ��������  ��  ��  @ ABA l     ��CD��  C,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   D �EEL   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .B FGF l     ��HI��  H g a These folders, if required, are then set to their corresponding global variables declared above.   I �JJ �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .G KLK i  ��MNM I      �������� 0 
backupinit 
backupInit��  ��  N k     �OO PQP r     RSR 4     ��T
�� 
psxfT o    ���� "0 destinationdisk destinationDiskS o      ���� 	0 drive  Q UVU l   ��WX��  W ' !log (destinationDisk & "Backups")   X �YY B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )V Z[Z l   ��������  ��  ��  [ \]\ Z    ,^_����^ l   `����` =    aba I    ��c���� 0 itexists itExistsc ded m    	ff �gg  f o l d e re h��h b   	 iji o   	 
���� "0 destinationdisk destinationDiskj m   
 kk �ll  B a c k u p s��  ��  b m    ��
�� boovfals��  ��  _ O    (mnm I   '����o
�� .corecrel****      � null��  o ��pq
�� 
koclp m    ��
�� 
cfolq ��rs
�� 
inshr o    ���� 	0 drive  s ��t��
�� 
prdtt K    #uu ��v��
�� 
pnamv m     !ww �xx  B a c k u p s��  ��  n m    yy�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ��  ] z{z l  - -��������  ��  ��  { |}| Z   - d~����~ l  - >������ =   - >��� I   - <������� 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ���� b   / 8��� b   / 4��� o   / 0���� "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7���� 0 machinefolder machineFolder��  ��  � m   < =��
�� boovfals��  ��   O   A `��� I  E _�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   G H�
� 
cfol� �~��
�~ 
insh� n   I T��� 4   O T�}�
�} 
cfol� m   P S�� ���  B a c k u p s� 4   I O�|�
�| 
cdis� o   M N�{�{ 	0 drive  � �z��y
�z 
prdt� K   U [�� �x��w
�x 
pnam� o   V Y�v�v 0 machinefolder machineFolder�w  �y  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ��  } ��� l  e e�u�t�s�u  �t  �s  � ��� O   e ~��� r   i }��� n   i y��� 4   t y�r�
�r 
cfol� o   u x�q�q 0 machinefolder machineFolder� n   i t��� 4   o t�p�
�p 
cfol� m   p s�� ���  B a c k u p s� 4   i o�o�
�o 
cdis� o   m n�n�n 	0 drive  � o      �m�m 0 backupfolder backupFolder� m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l   �l�k�j�l  �k  �j  � ��� Z    ����i�� l   ���h�g� =    ���� I    ��f��e�f 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��d� b   � ���� b   � ���� b   � ���� o   � ��c�c "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��b�b 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�d  �e  � m   � ��a
�a boovfals�h  �g  � O   � ���� I  � ��`�_�
�` .corecrel****      � null�_  � �^��
�^ 
kocl� m   � ��]
�] 
cfol� �\��
�\ 
insh� o   � ��[�[ 0 backupfolder backupFolder� �Z��Y
�Z 
prdt� K   � ��� �X��W
�X 
pnam� m   � ��� ���  L a t e s t�W  �Y  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �i  � r   � ���� m   � ��V
�V boovfals� o      �U�U 0 initialbackup initialBackup� ��� l  � ��T�S�R�T  �S  �R  � ��� O   � ���� r   � ���� n   � ���� 4   � ��Q�
�Q 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ��P�
�P 
cfol� o   � ��O�O 0 machinefolder machineFolder� n   � ���� 4   � ��N�
�N 
cfol� m   � ��� ���  B a c k u p s� 4   � ��M�
�M 
cdis� o   � ��L�L 	0 drive  � o      �K�K 0 latestfolder latestFolder� m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��J�I�H�J  �I  �H  � ��G� I   � ��F�E�D�F 
0 backup  �E  �D  �G  L ��� l     �C�B�A�C  �B  �A  � ��� l     �@�?�>�@  �?  �>  � ��� l     �=�<�;�=  �<  �;  � ��� l     �:�9�8�:  �9  �8  � ��� l     �7�6�5�7  �6  �5  � � � l     �4�4   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.    � �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .   l     �3�3   � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.    ��   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d . 	
	 l     �2�2   � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.    �P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .
  l     �1�1   � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.    �>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .  i  �� I      �0�/�.�0 0 tidyup tidyUp�/  �.   k      r      4     �-
�- 
psxf o    �,�, "0 destinationdisk destinationDisk o      �+�+ 	0 drive    l   �*�)�(�*  �)  �(   �' O    !  k   "" #$# r    %&% n    '(' 4    �&)
�& 
cfol) o    �%�% 0 machinefolder machineFolder( n    *+* 4    �$,
�$ 
cfol, m    -- �..  B a c k u p s+ 4    �#/
�# 
cdis/ o    �"�" 	0 drive  & o      �!�! 0 bf bF$ 010 r    232 n    454 1    � 
�  
ascd5 n    676 2   �
� 
cobj7 o    �� 0 bf bF3 o      �� 0 creationdates creationDates1 898 r     &:;: n     $<=< 4   ! $�>
� 
cobj> m   " #�� = o     !�� 0 creationdates creationDates; o      �� 0 theoldestdate theOldestDate9 ?@? r   ' *ABA m   ' (�� B o      �� 0 j  @ CDC l  + +����  �  �  D EFE Y   + nG�HI�G k   9 iJJ KLK r   9 ?MNM n   9 =OPO 4   : =�Q
� 
cobjQ o   ; <�� 0 i  P o   9 :�� 0 creationdates creationDatesN o      �� 0 thisdate thisDateL R�R Z   @ iST��S l  @ HU�
�	U >  @ HVWV n   @ FXYX 1   D F�
� 
pnamY 4   @ D�Z
� 
cobjZ o   B C�� 0 i  W m   F G[[ �\\  L a t e s t�
  �	  T Z   K e]^��] l  K N_��_ A   K N`a` o   K L�� 0 thisdate thisDatea o   L M� �  0 theoldestdate theOldestDate�  �  ^ k   Q abb cdc r   Q Tefe o   Q R���� 0 thisdate thisDatef o      ���� 0 theoldestdate theOldestDated ghg r   U Xiji o   U V���� 0 i  j o      ���� 0 j  h klk l  Y Y��mn��  m " log (item j of backupFolder)   n �oo 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )l p��p I  Y a��q��
�� .ascrcmnt****      � ****q l  Y ]r����r n   Y ]sts 4   Z ]��u
�� 
cobju o   [ \���� 0 j  t o   Y Z���� 0 bf bF��  ��  ��  ��  �  �  �  �  �  � 0 i  H m   . /���� I I  / 4��v��
�� .corecnte****       ****v o   / 0���� 0 creationdates creationDates��  �  F wxw l  o o��������  ��  ��  x yzy l  o o��{|��  { ! -- Delete the oldest folder   | �}} 6 - -   D e l e t e   t h e   o l d e s t   f o l d e rz ~~ l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r ��� l  o o������  �  delete item (j + 1) of bF   � ��� 2 d e l e t e   i t e m   ( j   +   1 )   o f   b F� ��� l  o o��������  ��  ��  � ��� r   o y��� c   o w��� l  o u������ n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  ��  � m   u v��
�� 
TEXT� o      ����  0 foldertodelete folderToDelete� ��� r   z ���� c   z ��� n   z }��� 1   { }��
�� 
psxp� o   z {����  0 foldertodelete folderToDelete� m   } ~��
�� 
TEXT� o      ����  0 foldertodelete folderToDelete� ��� I  � ������
�� .ascrcmnt****      � ****� l  � ������� b   � ���� m   � ��� ��� $ f o l d e r   t o   d e l e t e :  � o   � �����  0 foldertodelete folderToDelete��  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� r   � ���� b   � ���� b   � ���� m   � ��� ���  r m   - r f   '� o   � �����  0 foldertodelete folderToDelete� m   � ��� ���  '� o      ���� 0 todelete toDelete� ��� I  � ������
�� .sysoexecTEXT���     TEXT� o   � ����� 0 todelete toDelete��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  �  my setFocus()   � ���  m y   s e t F o c u s ( )� ��� l  � ���������  ��  ��  � ��� l   � �������  ���if (isNSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (isNSFW = false) then			set tempTitle to "WBTFU"		end if				set reply2 to the button returned of (display dialog "You need to empty your trash first before a backup can happen.
Click Empty Trash to do this automatically and continue backing up, or Cancel to stop the process." with title tempTitle with icon appIcon buttons {"Empty Trash", "Cancel Backup"} default button 2)		if (reply2 = "Empty Trash") then			empty the trash		else			if (displaySleepState contains strAwake) then				set msg to some item of messagesCancelled				if (toNotify = true) then					if (isNSFW = true) then						display notification msg with title "You stopped backing the fuck up"					else if (isNSFW = false) then						display notification "You stopped backing up" with title "WBTFU"					end if				end if			end if		end if   � ���  i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f  	 	  	 	 s e t   r e p l y 2   t o   t h e   b u t t o n   r e t u r n e d   o f   ( d i s p l a y   d i a l o g   " Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s . "   w i t h   t i t l e   t e m p T i t l e   w i t h   i c o n   a p p I c o n   b u t t o n s   { " E m p t y   T r a s h " ,   " C a n c e l   B a c k u p " }   d e f a u l t   b u t t o n   2 )  	 	 i f   ( r e p l y 2   =   " E m p t y   T r a s h " )   t h e n  	 	 	 e m p t y   t h e   t r a s h  	 	 e l s e  	 	 	 i f   ( d i s p l a y S l e e p S t a t e   c o n t a i n s   s t r A w a k e )   t h e n  	 	 	 	 s e t   m s g   t o   s o m e   i t e m   o f   m e s s a g e s C a n c e l l e d  	 	 	 	 i f   ( t o N o t i f y   =   t r u e )   t h e n  	 	 	 	 	 i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 	 	 	 d i s p l a y   n o t i f i c a t i o n   m s g   w i t h   t i t l e   " Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p "  	 	 	 	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 	 	 	 d i s p l a y   n o t i f i c a t i o n   " Y o u   s t o p p e d   b a c k i n g   u p "   w i t h   t i t l e   " W B T F U "  	 	 	 	 	 e n d   i f  	 	 	 	 e n d   i f  	 	 	 e n d   i f  	 	 e n d   i f� ��� l  � ���������  ��  ��  � ��� Z   � �������� l  � ������� E   � ���� o   � ����� &0 displaysleepstate displaySleepState� o   � ����� 0 strawake strAwake��  ��  � Z   � �������� l  � ������� =   � ���� o   � ����� 0 tonotify toNotify� m   � ���
�� boovtrue��  ��  � Z   � ������� l  � ������� =   � ���� o   � ����� 0 isnsfw isNSFW� m   � ���
�� boovtrue��  ��  � k   � ��� ��� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .� �����
�� 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  � ��� l  � ������� =   � ���� o   � ����� 0 isnsfw isNSFW� m   � ���
�� boovfals��  ��  � ���� k   � ��� ��� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  � ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  ��  ��  ��  ��  ��  ��  � ��� l   ��������  ��  ��  � ���� I   ������� 0 freespaceinit freeSpaceInit�  ��  m  ��
�� boovtrue��  ��  ��  ! m    �                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �'    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��   	 l     ��������  ��  ��  	 

 l     �������  ��  �    l     �~�~   M G Function that carries out the backup process and is split in 2 parts.     � �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .    l     �}�}  *$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.    �H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .  l     �|�|   � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.    �   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .  i  �� I      �{�z�y�{ 
0 backup  �z  �y   k    �  !  q      "" �x#�x 0 t  # �w$�w 0 x  $ �v�u�v "0 containerfolder containerFolder�u  ! %&% r     '(' I     �t)�s�t 0 gettimestamp getTimestamp) *�r* m    �q
�q boovtrue�r  �s  ( o      �p�p 0 t  & +,+ l  	 	�o�n�m�o  �n  �m  , -.- r   	 /0/ m   	 
�l
�l boovtrue0 o      �k�k 0 isbackingup isBackingUp. 121 l   �j�i�h�j  �i  �h  2 343 n   565 I    �g7�f�g 0 
updateicon 
updateIcon7 8�e8 m    99 �::  a c t i v e�e  �f  6  f    4 ;<; I   �d=�c
�d .sysodelanull��� ��� nmbr= m    �b�b �c  < >?> l   �a�`�_�a  �`  �_  ? @A@ l   �^BC�^  B  stopwatch("start")   C �DD $ s t o p w a t c h ( " s t a r t " )A EFE l   �]�\�[�]  �\  �[  F GHG O   �IJI k   "�KK LML l  " "�ZNO�Z  N f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   O �PP �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e dM QRQ l  " "�YST�Y  S x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   T �UU �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u pR VWV r   " *XYX c   " (Z[Z 4   " &�X\
�X 
psxf\ o   $ %�W�W 0 sourcefolder sourceFolder[ m   & '�V
�V 
TEXTY o      �U�U 0 
foldername 
folderNameW ]^] r   + 3_`_ n   + 1aba 1   / 1�T
�T 
pnamb 4   + /�Sc
�S 
cfolc o   - .�R�R 0 
foldername 
folderName` o      �Q�Q 0 
foldername 
folderName^ ded l  4 4�Pfg�P  f  log (folderName)		   g �hh $ l o g   ( f o l d e r N a m e ) 	 	e iji l  4 4�O�N�M�O  �N  �M  j klk Z   4 �mn�L�Km l  4 ;o�J�Io =   4 ;pqp o   4 9�H�H 0 initialbackup initialBackupq m   9 :�G
�G boovfals�J  �I  n k   > �rr sts I  > L�F�Eu
�F .corecrel****      � null�E  u �Dvw
�D 
koclv m   @ A�C
�C 
cfolw �Bxy
�B 
inshx o   B C�A�A 0 backupfolder backupFoldery �@z�?
�@ 
prdtz K   D H{{ �>|�=
�> 
pnam| o   E F�<�< 0 t  �=  �?  t }~} l  M M�;�:�9�;  �:  �9  ~ � r   M W��� c   M S��� 4   M Q�8�
�8 
psxf� o   O P�7�7 0 sourcefolder sourceFolder� m   Q R�6
�6 
TEXT� o      �5�5 (0 activesourcefolder activeSourceFolder� ��� r   X b��� 4   X ^�4�
�4 
cfol� o   Z ]�3�3 (0 activesourcefolder activeSourceFolder� o      �2�2 (0 activesourcefolder activeSourceFolder� ��� r   c |��� n   c x��� 4   u x�1�
�1 
cfol� o   v w�0�0 0 t  � n   c u��� 4   p u�/�
�/ 
cfol� o   q t�.�. 0 machinefolder machineFolder� n   c p��� 4   k p�-�
�- 
cfol� m   l o�� ���  B a c k u p s� 4   c k�,�
�, 
cdis� o   g j�+�+ 	0 drive  � o      �*�* (0 activebackupfolder activeBackupFolder� ��� l  } }�)���)  � 3 -log (activeSourceFolder & activeBackupFolder)   � ��� Z l o g   ( a c t i v e S o u r c e F o l d e r   &   a c t i v e B a c k u p F o l d e r )� ��(� I  } ��'�&�
�' .corecrel****      � null�&  � �%��
�% 
kocl� m    ��$
�$ 
cfol� �#��
�# 
insh� o   � ��"�" (0 activebackupfolder activeBackupFolder� �!�� 
�! 
prdt� K   � ��� ���
� 
pnam� o   � ��� 0 
foldername 
folderName�  �   �(  �L  �K  l ��� l  � �����  �  �  � ��� l  � �����  �  �  � ��� l  � �����  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �����  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � �����  � D > This means that only new or updated files will be copied over   � ��� |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r� ��� l  � �����  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	� ��� l  � �����  �   Original sync code    � ��� (   O r i g i n a l   s y n c   c o d e  � ��� l  � �����  � z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   � ��� �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "� ��� l  � �����  �  �  � ��� l  � �����  �   Original code   � ���    O r i g i n a l   c o d e� ��� l  � �����  � i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   � ��� �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h� ��� l  � �����  � x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   � ��� �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .� ��� l  � ��
���
  � p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   � ��� �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .� ��� l  � ��	���	  � � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   � ����   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .� ��� l  � �����  � � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   � ���v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .� ��� l  � �����  �  �  � ��� l  � �����  �  �  � ��� Z   ������ � l  � ������� =   � ���� o   � ����� 0 initialbackup initialBackup� m   � ���
�� boovtrue��  ��  � k   ��� ��� r   � ���� n   � ���� 1   � ���
�� 
time� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 	starttime 	startTime� ��� Z   � � ����  l  � ����� E   � � o   � ����� &0 displaysleepstate displaySleepState o   � ����� 0 strawake strAwake��  ��   Z   � ����� l  � ����� =   � �	 o   � ����� 0 tonotify toNotify	 m   � ���
�� boovtrue��  ��   Z   � �
��
 l  � ����� =   � � o   � ����� 0 isnsfw isNSFW m   � ���
�� boovtrue��  ��   I  � ���
�� .sysonotfnull��� ��� TEXT m   � � � � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . . ����
�� 
appr m   � � � 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��    l  � ����� =   � � o   � ����� 0 isnsfw isNSFW m   � ���
�� boovfals��  ��   �� I  � ���
�� .sysonotfnull��� ��� TEXT m   � � �   \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e ��!��
�� 
appr! m   � �"" �## 
 W B T F U��  ��  ��  ��  ��  ��  ��  � $%$ l   ��������  ��  ��  % &'& l   ��������  ��  ��  ' ()( l   ��������  ��  ��  ) *+* l   ��������  ��  ��  + ,-, r   ./. c   010 l  2����2 b   343 b   565 b   787 b   9:9 b   ;<; o   ���� "0 destinationdisk destinationDisk< m  == �>>  B a c k u p s /: o  
���� 0 machinefolder machineFolder8 m  ?? �@@  / L a t e s t /6 o  ���� 0 
foldername 
folderName4 m  AA �BB  /��  ��  1 m  ��
�� 
TEXT/ o      ���� 0 d  - CDC l ��EF��  E A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   F �GG v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "D HIH l ��������  ��  ��  I JKJ Q  ;LMNL k  2OO PQP l ��RS��  R��do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' '" & sourceFolder & "' '" & d & "/'"   S �TT< d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "Q UVU l ��������  ��  ��  V WXW l ��YZ��  Y   optimised(?)   Z �[[    o p t i m i s e d ( ? )X \]\ I 0��^��
�� .sysoexecTEXT���     TEXT^ b  ,_`_ b  (aba b  &cdc b  "efe m   gg �hh	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   'f o   !���� 0 sourcefolder sourceFolderd m  "%ii �jj  '   'b o  &'���� 0 d  ` m  (+kk �ll  / '��  ] mnm l 11��������  ��  ��  n o��o l 11��pq��  p U Odo shell script "rsync -aqvz --cvs-exclude '" & sourceFolder & "' '" & d & "/'"   q �rr � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "��  M R      ������
�� .ascrerr ****      � ****��  ��  N l ::��������  ��  ��  K sts l <<��������  ��  ��  t uvu l <<��������  ��  ��  v wxw l <<��������  ��  ��  x yzy l <<��������  ��  ��  z {|{ l <<��������  ��  ��  | }~} r  <C� m  <=��
�� boovfals� o      ���� 0 isbackingup isBackingUp~ ��� r  DK��� m  DE��
�� boovfals� o      ���� 0 manualbackup manualBackup� ��� r  LS��� m  LM��
�� boovtrue� o      ���� 0 	idleready 	idleReady� ��� l TT��������  ��  ��  � ��� Z  T�������� l T_������ E  T_��� o  TY���� &0 displaysleepstate displaySleepState� o  Y^���� 0 strawake strAwake��  ��  � k  b��� ��� r  bo��� n  bk��� 1  gk��
�� 
time� l bg������ I bg������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 endtime endTime� ��� r  pw��� n pu��� I  qu�������� 0 getduration getDuration��  ��  �  f  pq� o      ���� 0 duration  � ��� r  x���� n  x���� 3  }���
�� 
cobj� o  x}���� $0 messagescomplete messagesComplete� o      ���� 0 msg  � ��� Z  ��������� l �����~� =  ����� o  ���}�} 0 tonotify toNotify� m  ���|
�| boovtrue�  �~  � Z  ������{� l ����z�y� =  ����� o  ���x�x 0 isnsfw isNSFW� m  ���w
�w boovtrue�z  �y  � k  ���� ��� I ���v��
�v .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���u�u 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ���t�t 0 msg  � �s��r
�s 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�r  � ��q� I ���p��o
�p .sysodelanull��� ��� nmbr� m  ���n�n �o  �q  � ��� l ����m�l� =  ����� o  ���k�k 0 isnsfw isNSFW� m  ���j
�j boovfals�m  �l  � ��i� k  ���� ��� I ���h��
�h .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���g�g 0 duration  � m  ���� ���    m i n u t e s ! 
� �f��e
�f 
appr� m  ���� ���  B a c k e d   u p !�e  � ��d� I ���c��b
�c .sysodelanull��� ��� nmbr� m  ���a�a �b  �d  �i  �{  ��  ��  � ��� l ���`�_�^�`  �_  �^  � ��]� n ����� I  ���\��[�\ 0 
updateicon 
updateIcon� ��Z� m  ���� ���  i d l e�Z  �[  �  f  ���]  ��  ��  � ��� l ���Y�X�W�Y  �X  �W  � ��V� Z  ����U�T� l ����S�R� =  ����� o  ���Q�Q  0 backupthenquit backupThenQuit� m  ���P
�P boovtrue�S  �R  � k  ���� ��� l ���O���O  � / )quit application "Woah, Back The Fuck Up"   � ��� R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "� ��N� n ����� I  ���M�L�K�M 0 killapp killApp�L  �K  �  f  ���N  �U  �T  �V  � ��� l ��J�I� =  ��� o  
�H�H 0 initialbackup initialBackup� m  
�G
�G boovfals�J  �I  � ��F� k  ��� 	 		  r  .			 c  ,			 l *	�E�D	 b  *			 b  &			
		 b  $			 b   			 b  			 b  			 b  			 o  �C�C "0 destinationdisk destinationDisk	 m  		 �		  B a c k u p s /	 o  �B�B 0 machinefolder machineFolder	 m  		 �		  /	 o  �A�A 0 t  	 m   #		 �		  /	
 o  $%�@�@ 0 
foldername 
folderName	 m  &)		 �		  /�E  �D  	 m  *+�?
�? 
TEXT	 o      �>�> 0 c  	 			 r  /H		 	 c  /F	!	"	! l /D	#�=�<	# b  /D	$	%	$ b  /@	&	'	& b  />	(	)	( b  /:	*	+	* b  /6	,	-	, o  /2�;�; "0 destinationdisk destinationDisk	- m  25	.	. �	/	/  B a c k u p s /	+ o  69�:�: 0 machinefolder machineFolder	) m  :=	0	0 �	1	1  / L a t e s t /	' o  >?�9�9 0 
foldername 
folderName	% m  @C	2	2 �	3	3  /�=  �<  	" m  DE�8
�8 
TEXT	  o      �7�7 0 d  	 	4	5	4 I IR�6	6�5
�6 .ascrcmnt****      � ****	6 l IN	7�4�3	7 b  IN	8	9	8 m  IL	:	: �	;	;  b a c k i n g   u p   t o :  	9 o  LM�2�2 0 d  �4  �3  �5  	5 	<	=	< l SS�1�0�/�1  �0  �/  	= 	>	?	> Z  S�	@	A�.�-	@ l S^	B�,�+	B E  S^	C	D	C o  SX�*�* &0 displaysleepstate displaySleepState	D o  X]�)�) 0 strawake strAwake�,  �+  	A k  a�	E	E 	F	G	F r  an	H	I	H n  aj	J	K	J 1  fj�(
�( 
time	K l af	L�'�&	L I af�%�$�#
�% .misccurdldt    ��� null�$  �#  �'  �&  	I o      �"�" 0 	starttime 	startTime	G 	M	N	M r  oz	O	P	O n  ox	Q	R	Q 3  tx�!
�! 
cobj	R o  ot� �  *0 messagesencouraging messagesEncouraging	P o      �� 0 msg  	N 	S�	S Z  {�	T	U��	T l {�	V��	V =  {�	W	X	W o  {��� 0 tonotify toNotify	X m  ���
� boovtrue�  �  	U Z  ��	Y	Z	[�	Y l ��	\��	\ =  ��	]	^	] o  ���� 0 isnsfw isNSFW	^ m  ���
� boovtrue�  �  	Z k  ��	_	_ 	`	a	` l ��	b	c	d	b I ���	e	f
� .sysonotfnull��� ��� TEXT	e o  ���� 0 msg  	f �	g�
� 
appr	g o  ���� 0 	temptitle 	tempTitle�  	c ! "Woah, backing the fuck up"   	d �	h	h 6 " W o a h ,   b a c k i n g   t h e   f u c k   u p "	a 	i�	i I ���	j�
� .sysodelanull��� ��� nmbr	j m  ���
�
 �  �  	[ 	k	l	k l ��	m�	�	m =  ��	n	o	n o  ���� 0 isnsfw isNSFW	o m  ���
� boovfals�	  �  	l 	p�	p k  ��	q	q 	r	s	r l ��	t	u	v	t I ���	w	x
� .sysonotfnull��� ��� TEXT	w m  ��	y	y �	z	z  B a c k i n g   u p	x �	{�
� 
appr	{ o  ���� 0 	temptitle 	tempTitle�  	u  "WBTFU"   	v �	|	|  " W B T F U "	s 	}� 	} I ����	~��
�� .sysodelanull��� ��� nmbr	~ m  ������ ��  �   �  �  �  �  �  �.  �-  	? 		�	 l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� Q  ��	�	�	�	� k  ��	�	� 	�	�	� l ����	�	���  	���do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings'  --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	� �	�	�� d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����	�	���  	�   optimised(?)   	� �	�	�    o p t i m i s e d ( ? )	� 	�	�	� I ����	���
�� .sysoexecTEXT���     TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '	� o  ������ 0 c  	� m  ��	�	� �	�	�  '   '	� o  ������ 0 sourcefolder sourceFolder	� m  ��	�	� �	�	�  '   '	� o  ������ 0 d  	� m  ��	�	� �	�	�  / '��  	� 	�	�	� l ����������  ��  ��  	� 	���	� l ����	�	���  	� � zdo shell script "rsync -aqvz --cvs-exclude --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	� �	�	� � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "��  	� R      ������
�� .ascrerr ****      � ****��  ��  	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� l ����������  ��  ��  	� 	�	�	� r  � 	�	�	� m  ����
�� boovfals	� o      ���� 0 isbackingup isBackingUp	� 	�	�	� r  	�	�	� m  ��
�� boovfals	� o      ���� 0 manualbackup manualBackup	� 	�	�	� r  		�	�	� m  	
��
�� boovtrue	� o      ���� 0 	idleready 	idleReady	� 	�	�	� l ��������  ��  ��  	� 	���	� Z  �	�	���	�	� =  	�	�	� n  	�	�	� 2 ��
�� 
cobj	� l 	�����	� c  	�	�	� 4  ��	�
�� 
psxf	� o  ���� 0 c  	� m  ��
�� 
alis��  ��  	� J  ����  	� k  #|	�	� 	�	�	� r  #+	�	�	� c  #)	�	�	� n  #'	�	�	� 4  $'��	�
�� 
cfol	� o  %&���� 0 t  	� o  #$���� 0 backupfolder backupFolder	� m  '(��
�� 
TEXT	� o      ���� 0 oldestfolder oldestFolder	� 	�	�	� r  ,5	�	�	� c  ,3	�	�	� n  ,1	�	�	� 1  -1��
�� 
psxp	� o  ,-���� 0 oldestfolder oldestFolder	� m  12��
�� 
TEXT	� o      ���� 0 oldestfolder oldestFolder	� 	�	�	� I 6?��	���
�� .ascrcmnt****      � ****	� l 6;	�����	� b  6;	�	�	� m  69	�	� �	�	�  t o   d e l e t e :  	� o  9:���� 0 oldestfolder oldestFolder��  ��  ��  	� 	�	�	� l @@��������  ��  ��  	� 	�	�	� r  @K	�	�	� b  @I	�	�	� b  @E	�	�	� m  @C
 
  �

  r m   - r f   '	� o  CD���� 0 oldestfolder oldestFolder	� m  EH

 �

  '	� o      ���� 0 todelete toDelete	� 


 I LQ��
��
�� .sysoexecTEXT���     TEXT
 o  LM���� 0 todelete toDelete��  
 


 l RR��������  ��  ��  
 
	��
	 Z  R|


����

 l R]
����
 E  R]


 o  RW���� &0 displaysleepstate displaySleepState
 o  W\���� 0 strawake strAwake��  ��  
 k  `x

 


 r  `m


 n  `i


 1  ei��
�� 
time
 l `e
����
 I `e������
�� .misccurdldt    ��� null��  ��  ��  ��  
 o      ���� 0 endtime endTime
 


 r  nu


 n ns


 I  os�������� 0 getduration getDuration��  ��  
  f  no
 o      ���� 0 duration  
 


 l vv��������  ��  ��  
 

 
 r  v�
!
"
! n  v
#
$
# 3  {��
�� 
cobj
$ o  v{���� $0 messagescomplete messagesComplete
" o      ���� 0 msg  
  
%
&
% Z  �o
'
(����
' l ��
)����
) =  ��
*
+
* o  ������ 0 tonotify toNotify
+ m  ����
�� boovtrue��  ��  
( k  �k
,
, 
-
.
- Z  �5
/
0��
1
/ l ��
2��~
2 =  ��
3
4
3 o  ���}�} 0 duration  
4 m  ��
5
5 ?�      �  �~  
0 Z  ��
6
7
8�|
6 l ��
9�{�z
9 =  ��
:
;
: o  ���y�y 0 isnsfw isNSFW
; m  ���x
�x boovtrue�{  �z  
7 k  ��
<
< 
=
>
= I ���w
?
@
�w .sysonotfnull��� ��� TEXT
? b  ��
A
B
A b  ��
C
D
C b  ��
E
F
E m  ��
G
G �
H
H  A n d   i n  
F o  ���v�v 0 duration  
D m  ��
I
I �
J
J    m i n u t e ! 

B o  ���u�u 0 msg  
@ �t
K�s
�t 
appr
K m  ��
L
L �
M
M : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�s  
> 
N�r
N I ���q
O�p
�q .sysodelanull��� ��� nmbr
O m  ���o�o �p  �r  
8 
P
Q
P l ��
R�n�m
R =  ��
S
T
S o  ���l�l 0 isnsfw isNSFW
T m  ���k
�k boovfals�n  �m  
Q 
U�j
U k  ��
V
V 
W
X
W I ���i
Y
Z
�i .sysonotfnull��� ��� TEXT
Y b  ��
[
\
[ b  ��
]
^
] m  ��
_
_ �
`
`  A n d   i n  
^ o  ���h�h 0 duration  
\ m  ��
a
a �
b
b    m i n u t e ! 

Z �g
c�f
�g 
appr
c m  ��
d
d �
e
e  B a c k e d   u p !�f  
X 
f�e
f I ���d
g�c
�d .sysodelanull��� ��� nmbr
g m  ���b�b �c  �e  �j  �|  ��  
1 Z  �5
h
i
j�a
h l ��
k�`�_
k =  ��
l
m
l o  ���^�^ 0 isnsfw isNSFW
m m  ���]
�] boovtrue�`  �_  
i k  �
n
n 
o
p
o I ��\
q
r
�\ .sysonotfnull��� ��� TEXT
q b  ��
s
t
s b  ��
u
v
u b  ��
w
x
w m  ��
y
y �
z
z  A n d   i n  
x o  ���[�[ 0 duration  
v m  ��
{
{ �
|
|    m i n u t e s ! 

t o  ���Z�Z 0 msg  
r �Y
}�X
�Y 
appr
} m  �
~
~ �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�X  
p 
��W
� I �V
��U
�V .sysodelanull��� ��� nmbr
� m  �T�T �U  �W  
j 
�
�
� l 
��S�R
� =  
�
�
� o  �Q�Q 0 isnsfw isNSFW
� m  �P
�P boovfals�S  �R  
� 
��O
� k  1
�
� 
�
�
� I +�N
�
�
�N .sysonotfnull��� ��� TEXT
� b  !
�
�
� b  
�
�
� m  
�
� �
�
�  A n d   i n  
� o  �M�M 0 duration  
� m   
�
� �
�
�    m i n u t e s ! 

� �L
��K
�L 
appr
� m  $'
�
� �
�
�  B a c k e d   u p !�K  
� 
��J
� I ,1�I
��H
�I .sysodelanull��� ��� nmbr
� m  ,-�G�G �H  �J  �O  �a  
. 
�
�
� l 66�F�E�D�F  �E  �D  
� 
��C
� Z  6k
�
�
��B
� l 6=
��A�@
� =  6=
�
�
� o  6;�?�? 0 isnsfw isNSFW
� m  ;<�>
�> boovtrue�A  �@  
� I @M�=
�
�
�= .sysonotfnull��� ��� TEXT
� m  @C
�
� �
�
� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .
� �<
��;
�< 
appr
� m  FI
�
� �
�
� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r�;  
� 
�
�
� l PW
��:�9
� =  PW
�
�
� o  PU�8�8 0 isnsfw isNSFW
� m  UV�7
�7 boovfals�:  �9  
� 
��6
� I Zg�5
�
�
�5 .sysonotfnull��� ��� TEXT
� m  Z]
�
� �
�
� p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d
� �4
��3
�4 
appr
� m  `c
�
� �
�
� 2 D e l e t e d   n e w   b a c k u p   f o l d e r�3  �6  �B  �C  ��  ��  
& 
�
�
� l pp�2�1�0�2  �1  �0  
� 
��/
� n px
�
�
� I  qx�.
��-�. 0 
updateicon 
updateIcon
� 
��,
� m  qt
�
� �
�
�  i d l e�,  �-  
�  f  pq�/  ��  ��  ��  ��  	� k  �
�
� 
�
�
� Z  j
�
��+�*
� l �
��)�(
� E  �
�
�
� o  ��'�' &0 displaysleepstate displaySleepState
� o  ���&�& 0 strawake strAwake�)  �(  
� k  �f
�
� 
�
�
� r  ��
�
�
� n  ��
�
�
� 1  ���%
�% 
time
� l ��
��$�#
� I ���"�!� 
�" .misccurdldt    ��� null�!  �   �$  �#  
� o      �� 0 endtime endTime
� 
�
�
� r  ��
�
�
� n ��
�
�
� I  ������ 0 getduration getDuration�  �  
�  f  ��
� o      �� 0 duration  
� 
�
�
� r  ��
�
�
� n  ��
�
�
� 3  ���
� 
cobj
� o  ���� $0 messagescomplete messagesComplete
� o      �� 0 msg  
� 
��
� Z  �f
�
���
� l ��
���
� =  ��
�
�
� o  ���� 0 tonotify toNotify
� m  ���
� boovtrue�  �  
� Z  �b
�
��
�
� l ��
���
� =  ��
�
�
� o  ���� 0 duration  
� m  ��
�
� ?�      �  �  
� Z  �
�
�
��
� l ��
���

� =  ��
�
�
� o  ���	�	 0 isnsfw isNSFW
� m  ���
� boovtrue�  �
  
� k  ��
�
� 
�
�
� I ���
�
�
� .sysonotfnull��� ��� TEXT
� b  ��
�
�
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  A n d   i n  
� o  ���� 0 duration  
� m  ��
�
� �
�
�    m i n u t e ! 

� o  ���� 0 msg  
� �
��
� 
appr
� m  ��
�
� �   : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  
� � I ���� 
� .sysodelanull��� ��� nmbr m  ������ �   �  
�  l ������ =  �� o  ������ 0 isnsfw isNSFW m  ����
�� boovfals��  ��   �� k  �		 

 I ���
�� .sysonotfnull��� ��� TEXT b  �� b  �� m  �� �  A n d   i n   o  ������ 0 duration   m  �� �    m i n u t e ! 
 ����
�� 
appr m  � �  B a c k e d   u p !��   �� I ����
�� .sysodelanull��� ��� nmbr m  ���� ��  ��  ��  �  �  
� Z  b�� l ���� =    o  ���� 0 isnsfw isNSFW  m  ��
�� boovtrue��  ��   k  8!! "#" I 2��$%
�� .sysonotfnull��� ��� TEXT$ b  (&'& b  &()( b  "*+* m   ,, �--  A n d   i n  + o   !���� 0 duration  ) m  "%.. �//    m i n u t e s ! 
' o  &'���� 0 msg  % ��0��
�� 
appr0 m  +.11 �22 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  # 3��3 I 38��4��
�� .sysodelanull��� ��� nmbr4 m  34���� ��  ��   565 l ;B7����7 =  ;B898 o  ;@���� 0 isnsfw isNSFW9 m  @A��
�� boovfals��  ��  6 :��: k  E^;; <=< I EX��>?
�� .sysonotfnull��� ��� TEXT> b  EN@A@ b  EJBCB m  EHDD �EE  A n d   i n  C o  HI���� 0 duration  A m  JMFF �GG    m i n u t e s ! 
? ��H��
�� 
apprH m  QTII �JJ  B a c k e d   u p !��  = K��K I Y^��L��
�� .sysodelanull��� ��� nmbrL m  YZ���� ��  ��  ��  ��  �  �  �  �+  �*  
� MNM l kk��������  ��  ��  N OPO n ksQRQ I  ls��S���� 0 
updateicon 
updateIconS T��T m  loUU �VV  i d l e��  ��  R  f  klP WXW l tt��������  ��  ��  X YZY l t�[\][ Z t�^_����^ l t{`����` =  t{aba o  ty����  0 backupthenquit backupThenQuitb m  yz��
�� boovtrue��  ��  _ n ~�cdc I  ��������� 0 killapp killApp��  ��  d  f  ~��  ��  \ / )quit application "Woah, Back The Fuck Up"   ] �ee R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "Z f��f l ����gh��  g  end if   h �ii  e n d   i f��  ��  �F  �   �  J m    jj�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  H klk l ����������  ��  ��  l m��m l ����no��  n  stopwatch("finish")   o �pp & s t o p w a t c h ( " f i n i s h " )��   qrq l     ��������  ��  ��  r sts l     ��uv��  u � �-----------------------------------------------------------------------------------------------------------------------------------------------   v �ww - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -t xyx l     ��z{��  z � �-----------------------------------------------------------------------------------------------------------------------------------------------   { �|| - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -y }~} l     ��������  ��  ��  ~ � l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ������  � � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   � ��� - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ��������  ��  ��  � ��� l     ������  � � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   � ���   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .� ��� l     ������  � � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   � ����   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .� ��� i  ����� I      ������� 0 itexists itExists� ��� o      ���� 0 
objecttype 
objectType� ���� o      ���� 
0 object  ��  ��  � l    W���� O     W��� Z    V������ l   ������ =    ��� o    ���� 0 
objecttype 
objectType� m    �� ���  d i s k��  ��  � Z   
 ������ I  
 �����
�� .coredoexnull���     ****� 4   
 ���
�� 
cdis� o    ���� 
0 object  ��  � L    �� m    ��
�� boovtrue��  � L    �� m    ��
�� boovfals� ��� l   "������ =    "��� o     ���� 0 
objecttype 
objectType� m     !�� ���  f i l e��  ��  � ��� Z   % 7������ I  % -�����
�� .coredoexnull���     ****� 4   % )���
�� 
file� o   ' (���� 
0 object  ��  � L   0 2�� m   0 1��
�� boovtrue��  � L   5 7�� m   5 6��
�� boovfals� ��� l  : =������ =   : =��� o   : ;���� 0 
objecttype 
objectType� m   ; <�� ���  f o l d e r��  ��  � ���� Z   @ R������ I  @ H�����
�� .coredoexnull���     ****� 4   @ D���
�� 
cfol� o   B C�� 
0 object  ��  � L   K M�� m   K L�~
�~ boovtrue��  � L   P R�� m   P Q�}
�} boovfals��  ��  � m     ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  � "  (string, string) as Boolean   � ��� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n� ��� l     �|�{�z�|  �{  �z  � ��� l     �y�x�w�y  �x  �w  � ��� l     �v�u�t�v  �u  �t  � ��� l     �s�r�q�s  �r  �q  � ��� l     �p�o�n�p  �o  �n  � ��� l     �m���m  � � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   � ���Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .� ��� l     �l���l  � P J This means that backups can be identified from what machine they're from.   � ��� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .� ��� i  ����� I      �k�j�i�k .0 getcomputeridentifier getComputerIdentifier�j  �i  � k     �� ��� r     	��� n     ��� 1    �h
�h 
sicn� l    ��g�f� I    �e�d�c
�e .sysosigtsirr   ��� null�d  �c  �g  �f  � o      �b�b 0 computername computerName� � � r   
  I  
 �a�`
�a .sysoexecTEXT���     TEXT m   
  � � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �`   o      �_�_ "0 strserialnumber strSerialNumber   r    	 l   
�^�]
 b     b     o    �\�\ 0 computername computerName m     �    -   o    �[�[ "0 strserialnumber strSerialNumber�^  �]  	 o      �Z�Z  0 identifiername identifierName �Y L     o    �X�X  0 identifiername identifierName�Y  �  l     �W�V�U�W  �V  �U    l     �T�S�R�T  �S  �R    l     �Q�P�O�Q  �P  �O    l     �N�M�L�N  �M  �L    l     �K�J�I�K  �J  �I    l     �H �H   � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.     �!!   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' . "#" l     �G$%�G  $ � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   % �&&   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .# '(' i  ��)*) I      �F+�E�F 0 gettimestamp getTimestamp+ ,�D, o      �C�C 0 isfolder isFolder�D  �E  * l   �-./- k    �00 121 l     �B34�B  3   Date variables   4 �55    D a t e   v a r i a b l e s2 676 r     )898 l     :�A�@: I     �?�>�=
�? .misccurdldt    ��� null�>  �=  �A  �@  9 K    ;; �<<=
�< 
year< o    �;�; 0 y  = �:>?
�: 
mnth> o    �9�9 0 m  ? �8@A
�8 
day @ o    �7�7 0 d  A �6B�5
�6 
timeB o   	 
�4�4 0 t  �5  7 CDC r   * 1EFE c   * /GHG l  * -I�3�2I c   * -JKJ o   * +�1�1 0 y  K m   + ,�0
�0 
long�3  �2  H m   - .�/
�/ 
TEXTF o      �.�. 0 ty tYD LML r   2 9NON c   2 7PQP l  2 5R�-�,R c   2 5STS o   2 3�+�+ 0 y  T m   3 4�*
�* 
long�-  �,  Q m   5 6�)
�) 
TEXTO o      �(�( 0 ty tYM UVU r   : AWXW c   : ?YZY l  : =[�'�&[ c   : =\]\ o   : ;�%�% 0 m  ] m   ; <�$
�$ 
long�'  �&  Z m   = >�#
�# 
TEXTX o      �"�" 0 tm tMV ^_^ r   B I`a` c   B Gbcb l  B Ed�!� d c   B Eefe o   B C�� 0 d  f m   C D�
� 
long�!  �   c m   E F�
� 
TEXTa o      �� 0 td tD_ ghg r   J Qiji c   J Oklk l  J Mm��m c   J Mnon o   J K�� 0 t  o m   K L�
� 
long�  �  l m   M N�
� 
TEXTj o      �� 0 tt tTh pqp l  R R����  �  �  q rsr l  R R�tu�  t U O Append the month or day with a 0 if the string length is only 1 character long   u �vv �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n gs wxw r   R [yzy c   R Y{|{ l  R W}��} I  R W�~�
� .corecnte****       ****~ o   R S�� 0 tm tM�  �  �  | m   W X�
� 
nmbrz o      �� 
0 tml tMLx � r   \ e��� c   \ c��� l  \ a��
�	� I  \ a���
� .corecnte****       ****� o   \ ]�� 0 tm tM�  �
  �	  � m   a b�
� 
nmbr� o      �� 
0 tdl tDL� ��� l  f f����  �  �  � ��� Z  f u��� ��� l  f i������ =   f i��� o   f g���� 
0 tml tML� m   g h���� ��  ��  � r   l q��� b   l o��� m   l m�� ���  0� o   m n���� 0 tm tM� o      ���� 0 tm tM�   ��  � ��� l  v v������  �  end if   � ���  e n d   i f� ��� l  v v��������  ��  ��  � ��� Z  v �������� l  v y������ =   v y��� o   v w���� 
0 tdl tDL� m   w x���� ��  ��  � r   | ���� b   | ���� m   | �� ���  0� o    ����� 0 td tD� o      ���� 0 td tD��  ��  � ��� l  � �������  �  end if   � ���  e n d   i f� ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  �   Time variables	   � ���     T i m e   v a r i a b l e s 	� ��� l  � �������  �  	 Get hour   � ���    G e t   h o u r� ��� r   � ���� n   � ���� 1   � ���
�� 
tstr� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 timestr timeStr� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 h  � ��� r   � ���� c   � ���� n   � ���� 7 � �����
�� 
cha � l  � ������� [   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  �  ;   � �� o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   ���� c   �� � n   �  7  � ��
�� 
cha  m   � �����  l  � ����� \   � � o   � ����� 0 pos   m   � ����� ��  ��   o   � ����� 0 timestr timeStr  m   ��
�� 
TEXT� o      ���� 0 m  � 	 r  

 c   n   7��
�� 
cha  l ���� [   o  ���� 0 pos   m  ���� ��  ��    ;   o  ���� 0 timestr timeStr m  ��
�� 
TEXT o      ���� 0 timestr timeStr	  l ��������  ��  ��    l ����     Get AM or PM    �    G e t   A M   o r   P M  r  2 I 0 ��!  z����
�� .sysooffslong    ��� null
�� misccura��  ! ��"#
�� 
psof" m  "%$$ �%%   # ��&��
�� 
psin& o  ()���� 0 timestr timeStr��   o      ���� 0 pos   '(' r  3E)*) c  3C+,+ n  3A-.- 74A��/0
�� 
cha / l :>1����1 [  :>232 o  ;<���� 0 pos  3 m  <=���� ��  ��  0  ;  ?@. o  34���� 0 timestr timeStr, m  AB��
�� 
TEXT* o      ���� 0 s  ( 454 l FF��������  ��  ��  5 676 l FF��������  ��  ��  7 898 Z  F:;<��: l FI=����= =  FI>?> o  FG���� 0 isfolder isFolder? m  GH��
�� boovtrue��  ��  ; k  La@@ ABA l LL��CD��  C   Create folder timestamp   D �EE 0   C r e a t e   f o l d e r   t i m e s t a m pB F��F r  LaGHG b  L_IJI b  L]KLK b  L[MNM b  LYOPO b  LUQRQ b  LSSTS b  LQUVU m  LOWW �XX  b a c k u p _V o  OP���� 0 ty tYT o  QR���� 0 tm tMR o  ST���� 0 td tDP m  UXYY �ZZ  _N o  YZ���� 0 h  L o  [\���� 0 m  J o  ]^���� 0 s  H o      �� 0 	timestamp  ��  < [\[ l dg]�~�}] =  dg^_^ o  de�|�| 0 isfolder isFolder_ m  ef�{
�{ boovfals�~  �}  \ `�z` k  j{aa bcb l jj�yde�y  d   Create timestamp   e �ff "   C r e a t e   t i m e s t a m pc g�xg r  j{hih b  jyjkj b  jwlml b  junon b  jspqp b  jorsr b  jmtut o  jk�w�w 0 ty tYu o  kl�v�v 0 tm tMs o  mn�u�u 0 td tDq m  orvv �ww  _o o  st�t�t 0 h  m o  uv�s�s 0 m  k o  wx�r�r 0 s  i o      �q�q 0 	timestamp  �x  �z  ��  9 xyx l ���p�o�n�p  �o  �n  y z�mz L  ��{{ o  ���l�l 0 	timestamp  �m  .  	(boolean)   / �||  ( b o o l e a n )( }~} l     �k�j�i�k  �j  �i  ~ � l     �h�g�f�h  �g  �f  � ��� l     �e�d�c�e  �d  �c  � ��� l     �b�a�`�b  �a  �`  � ��� l     �_�^�]�_  �^  �]  � ��� l     �\���\  � q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   � ��� �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .� ��� l     �[���[  � w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   � ��� �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .� ��� i  ����� I      �Z��Y�Z 0 comparesizes compareSizes� ��� o      �X�X 
0 source  � ��W� o      �V�V 0 destination  �W  �Y  � l   ����� k    ��� ��� r     ��� m     �U
�U boovtrue� o      �T�T 0 fit  � ��� r    
��� 4    �S�
�S 
psxf� o    �R�R 0 destination  � o      �Q�Q 0 destination  � ��� l   �P�O�N�P  �O  �N  � ��� r    ��� b    ��� b    ��� b    ��� o    �M�M "0 destinationdisk destinationDisk� m    �� ���  B a c k u p s /� o    �L�L 0 machinefolder machineFolder� m    �� ���  / L a t e s t� o      �K�K 0 
templatest 
tempLatest� ��� r    ��� m    �J�J  � o      �I�I $0 latestfoldersize latestFolderSize� ��� l   �H�G�F�H  �G  �F  � ��� r    &��� b    $��� n   "��� 1     "�E
�E 
psxp� l    ��D�C� I    �B��
�B .earsffdralis        afdr� m    �A
�A afdrdlib� �@��?
�@ 
from� m    �>
�> fldmfldu�?  �D  �C  � m   " #�� ���  C a c h e s� o      �=�= 0 c  � ��� r   ' 4��� b   ' 2��� n  ' 0��� 1   . 0�<
�< 
psxp� l  ' .��;�:� I  ' .�9��
�9 .earsffdralis        afdr� m   ' (�8
�8 afdrdlib� �7��6
�7 
from� m   ) *�5
�5 fldmfldu�6  �;  �:  � m   0 1�� ���  L o g s� o      �4�4 0 l  � ��� r   5 B��� b   5 @��� n  5 >��� 1   < >�3
�3 
psxp� l  5 <��2�1� I  5 <�0��
�0 .earsffdralis        afdr� m   5 6�/
�/ afdrdlib� �.��-
�. 
from� m   7 8�,
�, fldmfldu�-  �2  �1  � m   > ?�� ���   M o b i l e   D o c u m e n t s� o      �+�+ 0 md  � ��� r   C F��� m   C D�*�*  � o      �)�) 0 	cachesize 	cacheSize� ��� r   G J��� m   G H�(�(  � o      �'�' 0 logssize logsSize� ��� r   K N��� m   K L�&�&  � o      �%�% 0 mdsize mdSize� ��� l  O O�$�#�"�$  �#  �"  � ��� r   O R��� m   O P�� ?�      � o      �!�! 
0 buffer  � ��� l  S S� ���   �  �  � ��� r   S V��� m   S T��  � o      �� (0 adjustedfoldersize adjustedFolderSize� � � l  W W����  �  �     O   W� k   [�  r   [ h	 I  [ f�
�
� .sysoexecTEXT���     TEXT
 b   [ b b   [ ^ m   [ \ �  d u   - m s   ' o   \ ]�� 
0 source   m   ^ a �  '   |   c u t   - f   1�  	 o      �� 0 
foldersize 
folderSize  r   i � ^   i � l  i }�� I  i }� z��
� .sysorondlong        doub
� misccura ]   o x l  o t�� ^   o t  o   o p�� 0 
foldersize 
folderSize  m   p s�� �  �   m   t w�� d�  �  �   m   } ��
�
 d o      �	�	 0 
foldersize 
folderSize !"! l  � �����  �  �  " #$# r   � �%&% ^   � �'(' ^   � �)*) ^   � �+,+ l  � �-��- l  � �.��. n   � �/0/ 1   � ��
� 
frsp0 4   � �� 1
�  
cdis1 o   � ����� 0 destination  �  �  �  �  , m   � ����� * m   � ����� ( m   � ����� & o      ���� 0 	freespace 	freeSpace$ 232 r   � �454 ^   � �676 l  � �8����8 I  � �9:��9 z����
�� .sysorondlong        doub
�� misccura: l  � �;����; ]   � �<=< o   � ����� 0 	freespace 	freeSpace= m   � ����� d��  ��  ��  ��  ��  7 m   � ����� d5 o      ���� 0 	freespace 	freeSpace3 >?> l  � ���������  ��  ��  ? @A@ r   � �BCB I  � ���D��
�� .sysoexecTEXT���     TEXTD b   � �EFE b   � �GHG m   � �II �JJ  d u   - m s   'H o   � ����� 0 c  F m   � �KK �LL  '   |   c u t   - f   1��  C o      ���� 0 	cachesize 	cacheSizeA MNM r   � �OPO ^   � �QRQ l  � �S����S I  � �TU��T z����
�� .sysorondlong        doub
�� misccuraU ]   � �VWV l  � �X����X ^   � �YZY o   � ����� 0 	cachesize 	cacheSizeZ m   � ����� ��  ��  W m   � ����� d��  ��  ��  R m   � ����� dP o      ���� 0 	cachesize 	cacheSizeN [\[ l  � ���������  ��  ��  \ ]^] r   � �_`_ I  � ���a��
�� .sysoexecTEXT���     TEXTa b   � �bcb b   � �ded m   � �ff �gg  d u   - m s   'e o   � ����� 0 l  c m   � �hh �ii  '   |   c u t   - f   1��  ` o      ���� 0 logssize logsSize^ jkj r   �	lml ^   �non l  �p����p I  �qr��q z����
�� .sysorondlong        doub
�� misccurar ]   � �sts l  � �u����u ^   � �vwv o   � ����� 0 logssize logsSizew m   � ����� ��  ��  t m   � ����� d��  ��  ��  o m  ���� dm o      ���� 0 logssize logsSizek xyx l 

��������  ��  ��  y z{z r  
|}| I 
��~��
�� .sysoexecTEXT���     TEXT~ b  
� b  
��� m  
�� ���  d u   - m s   '� o  ���� 0 md  � m  �� ���  '   |   c u t   - f   1��  } o      ���� 0 mdsize mdSize{ ��� r  4��� ^  2��� l .������ I .����� z����
�� .sysorondlong        doub
�� misccura� ]   )��� l  %������ ^   %��� o   !���� 0 mdsize mdSize� m  !$���� ��  ��  � m  %(���� d��  ��  ��  � m  .1���� d� o      ���� 0 mdsize mdSize� ��� l 55��������  ��  ��  � ��� r  5>��� l 5<������ \  5<��� \  5:��� \  58��� o  56���� 0 
foldersize 
folderSize� o  67���� 0 	cachesize 	cacheSize� o  89���� 0 logssize logsSize� o  :;���� 0 mdsize mdSize��  ��  � o      ���� (0 adjustedfoldersize adjustedFolderSize� ��� l ??��������  ��  ��  � ��� I ?H�����
�� .ascrcmnt****      � ****� l ?D������ b  ?D��� b  ?B��� o  ?@���� 0 
foldersize 
folderSize� o  @A���� (0 adjustedfoldersize adjustedFolderSize� o  BC���� 0 	freespace 	freeSpace��  ��  ��  � ��� l II��������  ��  ��  � ���� Z  I�������� l IP������ =  IP��� o  IN���� 0 initialbackup initialBackup� m  NO��
�� boovfals��  ��  � k  S�� ��� r  Sb��� I S`�����
�� .sysoexecTEXT���     TEXT� b  S\��� b  SX��� m  SV�� ���  d u   - m s   '� o  VW���� 0 
templatest 
tempLatest� m  X[�� ���  '   |   c u t   - f   1��  � o      ���� $0 latestfoldersize latestFolderSize� ��� r  c}��� ^  c{��� l cw������ I cw����� z����
�� .sysorondlong        doub
�� misccura� ]  ir��� l in������ ^  in��� o  ij���� $0 latestfoldersize latestFolderSize� m  jm���� ��  ��  � m  nq���� d��  ��  ��  � m  wz���� d� o      ���� $0 latestfoldersize latestFolderSize� ���� l ~~��������  ��  ��  ��  ��  ��  ��   m   W X���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��   ��� l ����������  ��  ��  � ��� Z  �������� l ������� =  ����� o  ���~�~ 0 initialbackup initialBackup� m  ���}
�} boovfals��  �  � k  ���� ��� r  ����� \  ����� o  ���|�| (0 adjustedfoldersize adjustedFolderSize� o  ���{�{ $0 latestfoldersize latestFolderSize� o      �z�z 0 temp  � ��� l ���y�x�w�y  �x  �w  � ��� Z �����v�u� l ����t�s� A  ����� o  ���r�r 0 temp  � m  ���q�q  �t  �s  � r  ����� \  ����� l ����p�o� o  ���n�n 0 temp  �p  �o  � l ����m�l� ]  ����� l ����k�j� o  ���i�i 0 temp  �k  �j  � m  ���h�h �m  �l  � o      �g�g 0 temp  �v  �u  � ��� l ���f���f  �  end if   � ���  e n d   i f� ��� l ���e�d�c�e  �d  �c  � ��� Z �����b�a� l �� �`�_  ?  �� o  ���^�^ 0 temp   l ���]�\ \  �� o  ���[�[ 0 	freespace 	freeSpace o  ���Z�Z 
0 buffer  �]  �\  �`  �_  � r  �� m  ���Y
�Y boovfals o      �X�X 0 fit  �b  �a  � �W l ���V	
�V  	  end if   
 �  e n d   i f�W  �  l ���U�T =  �� o  ���S�S 0 initialbackup initialBackup m  ���R
�R boovtrue�U  �T   �Q k  ��  Z ���P�O l ���N�M ?  �� o  ���L�L (0 adjustedfoldersize adjustedFolderSize l ���K�J \  �� o  ���I�I 0 	freespace 	freeSpace o  ���H�H 
0 buffer  �K  �J  �N  �M   r  �� m  ���G
�G boovfals o      �F�F 0 fit  �P  �O   �E l ���D !�D     end if   ! �""  e n d   i f�E  �Q  ��  � #$# l ���C�B�A�C  �B  �A  $ %�@% L  ��&& o  ���?�? 0 fit  �@  �  (string, string)   � �''   ( s t r i n g ,   s t r i n g )� ()( l     �>�=�<�>  �=  �<  ) *+* l     �;�:�9�;  �:  �9  + ,-, l     �8�7�6�8  �7  �6  - ./. l     �5�4�3�5  �4  �3  / 010 l     �2�1�0�2  �1  �0  1 232 l     �/45�/  4 m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   5 �66 �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .3 787 l      �.9:�.  9 � �on stopwatch(mode) --(string)	local x	if (mode = "start") then		set x to getTimestamp(false)		log ("backup started: " & x)	else if (mode = "finish") then		set x to getTimestamp(false)		log ("backup finished: " & x)	end ifend stopwatch   : �;;� o n   s t o p w a t c h ( m o d e )   - - ( s t r i n g )  	 l o c a l   x  	 i f   ( m o d e   =   " s t a r t " )   t h e n  	 	 s e t   x   t o   g e t T i m e s t a m p ( f a l s e )  	 	 l o g   ( " b a c k u p   s t a r t e d :   "   &   x )  	 e l s e   i f   ( m o d e   =   " f i n i s h " )   t h e n  	 	 s e t   x   t o   g e t T i m e s t a m p ( f a l s e )  	 	 l o g   ( " b a c k u p   f i n i s h e d :   "   &   x )  	 e n d   i f  e n d   s t o p w a t c h8 <=< l     �-�,�+�-  �,  �+  = >?> l     �*�)�(�*  �)  �(  ? @A@ l     �'�&�%�'  �&  �%  A BCB l     �$�#�"�$  �#  �"  C DED l     �!� ��!  �   �  E FGF l     �HI�  H M G Utility function to get the duration of the backup process in minutes.   I �JJ �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .G KLK i  ��MNM I      ���� 0 getduration getDuration�  �  N k     OO PQP r     RSR ^     TUT l    V��V \     WXW o     �� 0 endtime endTimeX o    �� 0 	starttime 	startTime�  �  U m    �� <S o      �� 0 duration  Q YZY r    [\[ ^    ]^] l   _��_ I   `a�` z��
� .sysorondlong        doub
� misccuraa l   b��b ]    cdc o    �� 0 duration  d m    �� d�  �  �  �  �  ^ m    �� d\ o      �
�
 0 duration  Z e�	e L    ff o    �� 0 duration  �	  L ghg l     ����  �  �  h iji l     ����  �  �  j klk l     �� ���  �   ��  l mnm l     ��������  ��  ��  n opo l     ��������  ��  ��  p qrq l     ��st��  s ; 5 Utility function bring WBTFU to the front with focus   t �uu j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u sr vwv i  ��xyx I      �������� 0 setfocus setFocus��  ��  y k     ;zz {|{ l      ��}~��  } D >tell application "Woah, Back The Fuck Up"		activate	end tell   ~ � | t e l l   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 a c t i v a t e  	 e n d   t e l l| ��� l     ��������  ��  ��  � ��� l     ������  � 8 2do shell script "open -a 'Woah, Back The Fuck Up'"   � ��� d d o   s h e l l   s c r i p t   " o p e n   - a   ' W o a h ,   B a c k   T h e   F u c k   U p ' "� ��� l     ������  � ( "do shell script "open -a 'Finder'"   � ��� D d o   s h e l l   s c r i p t   " o p e n   - a   ' F i n d e r ' "� ��� l     ��������  ��  ��  � ���� O     ;��� k    :�� ��� r    ��� n    	��� 1    	��
�� 
pnam� 2    ��
�� 
prcs� o      ���� 0 processlist ProcessList� ���� Z    :������� E   ��� o    ���� 0 processlist ProcessList� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� k    6�� ��� r    ��� n    ��� 1    ��
�� 
idux� 4    ���
�� 
prcs� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      ���� 0 thepid ThePID� ��� I    �����
�� .ascrcmnt****      � ****� l   ������ o    ���� 0 thepid ThePID��  ��  ��  � ��� l  ! !��������  ��  ��  � ��� l   ! !������  � � �set myProcesses to every process whose unix id is ThePID		repeat with myProcess in myProcesses			set the frontmost of myProcess to true		end repeat   � ���, s e t   m y P r o c e s s e s   t o   e v e r y   p r o c e s s   w h o s e   u n i x   i d   i s   T h e P I D  	 	 r e p e a t   w i t h   m y P r o c e s s   i n   m y P r o c e s s e s  	 	 	 s e t   t h e   f r o n t m o s t   o f   m y P r o c e s s   t o   t r u e  	 	 e n d   r e p e a t� ��� l  ! !��������  ��  ��  � ���� O  ! 6��� r   % 5��� m   % &��
�� boovtrue� 6  & 4��� n   & +��� 1   ) +��
�� 
pisf� 2   & )��
�� 
prcs� =  , 3��� 1   - /��
�� 
idux� o   0 2���� 0 thepid ThePID� m   ! "���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  ��  ��  ��  � m     ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  w ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  �   Test connection   � ���     T e s t   c o n n e c t i o n� ��� i  ����� I      ��������  0 testconnection testConnection��  ��  � k     E�� ��� Y     >�������� Q   
 9���� k    �� ��� I   �����
�� .sysoexecTEXT���     TEXT� m    �� ��� * p i n g   - o   w w w . a p p l e . c o m��  � ����  S    ��  � R      ������
�� .ascrerr ****      � ****��  ��  � k    9�� ��� I   !�����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ��� I  " '������
�� .sysobeepnull��� ��� long��  ��  � ���� Z  ( 9������� l  ( +������ =   ( +��� o   ( )���� 0 i  � m   ) *���� ��  ��  � r   . 5��� m   . /��
�� boovfals� o      ���� 0 isconnected isConnected��  ��  ��  �� 0 i  � m    ���� � m    ���� ��  � ��� l  ? ?��������  ��  ��  � ���� L   ? E�� o   ? D���� 0 isconnected isConnected��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  �    l     ��������  ��  ��    l     ����     Update plist file    � $   U p d a t e   p l i s t   f i l e  i  ��	
	 I      ������ 0 updateplist updatePlist  o      ���� 0 tempitem tempItem �� o      ���� 0 	tempvalue 	tempValue��  ��  
 k     q  O      k      r     4    ��
�� 
plif o    ���� 	0 plist   o      ���� 0 	tempplist 	tempPlist �� O     r     o    ���� 0 	tempvalue 	tempValue n        1    ��
�� 
valL  4    ��!
�� 
plii! o    ���� 0 tempitem tempItem o    ���� 0 	tempplist 	tempPlist��   m     ""�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��   #$# l   ��~�}�  �~  �}  $ %&% O    a'(' k   " `)) *+* r   " .,-, n   " ,./. 1   * ,�|
�| 
pcnt/ 4   " *�{0
�{ 
plif0 o   $ )�z�z 	0 plist  - o      �y�y 0 thecontents theContents+ 121 r   / 4343 n   / 2565 1   0 2�x
�x 
valL6 o   / 0�w�w 0 thecontents theContents4 o      �v�v 0 thevalue theValue2 787 l  5 5�u�t�s�u  �t  �s  8 9:9 r   5 :;<; n   5 8=>= o   6 8�r�r  0 foldertobackup FolderToBackup> o   5 6�q�q 0 thevalue theValue< o      �p�p 0 
foldername 
folderName: ?@? r   ; @ABA n   ; >CDC o   < >�o�o 0 backupdrive BackupDriveD o   ; <�n�n 0 thevalue theValueB o      �m�m 0 
backupdisk 
backupDisk@ EFE r   A FGHG n   A DIJI o   B D�l�l  0 computerfolder ComputerFolderJ o   A B�k�k 0 thevalue theValueH o      �j�j  0 computerfolder ComputerFolderF KLK r   G LMNM n   G JOPO o   H J�i�i 0 scheduledtime ScheduledTimeP o   G H�h�h 0 thevalue theValueN o      �g�g 0 
backuptime 
backupTimeL QRQ r   M VSTS n   M PUVU o   N P�f�f 0 nsfw NSFWV o   M N�e�e 0 thevalue theValueT o      �d�d 0 isnsfw isNSFWR W�cW r   W `XYX n   W ZZ[Z o   X Z�b�b 0 notifications Notifications[ o   W X�a�a 0 thevalue theValueY o      �`�` 0 tonotify toNotify�c  ( m    \\�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  & ]^] l  b b�_�^�]�_  �^  �]  ^ _`_ r   b eaba o   b c�\�\ 0 
foldername 
folderNameb o      �[�[ 0 sourcefolder sourceFolder` cdc r   f iefe o   f g�Z�Z 0 
backupdisk 
backupDiskf o      �Y�Y "0 destinationdisk destinationDiskd ghg r   j miji o   j k�X�X  0 computerfolder ComputerFolderj o      �W�W 0 machinefolder machineFolderh k�Vk r   n qlml o   n o�U�U 0 
backuptime 
backupTimem o      �T�T 0 scheduledtime ScheduledTime�V   non l     �S�R�Q�S  �R  �Q  o pqp l     �P�O�N�P  �O  �N  q rsr l     �M�L�K�M  �L  �K  s tut l     �J�I�H�J  �I  �H  u vwv l     �G�F�E�G  �F  �E  w xyx i  ��z{z I      �D�C�B�D 0 killapp killApp�C  �B  { O     '|}| k    &~~ � r    ��� n    	��� 1    	�A
�A 
pnam� 2    �@
�@ 
prcs� o      �?�? 0 processlist ProcessList� ��>� Z    &���=�<� E   ��� o    �;�; 0 processlist ProcessList� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� k    "�� ��� r    ��� n    ��� 1    �:
�: 
idux� 4    �9�
�9 
prcs� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      �8�8 0 thepid ThePID� ��7� I   "�6��5
�6 .sysoexecTEXT���     TEXT� b    ��� m    �� ���  k i l l   - K I L L  � o    �4�4 0 thepid ThePID�5  �7  �=  �<  �>  } m     ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  y ��� l     �3�2�1�3  �2  �1  � ��� l     �0�/�.�0  �/  �.  � ��� l     �-�,�+�-  �,  �+  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� i  ����� I      �$�#�"�$ "0 getlatestupdate getLatestUpdate�#  �"  � k     M�� ��� r     ��� I    �!�� 
�! .sysoexecTEXT���     TEXT� m     �� ��� \ c u r l   h t t p : / / w w w . i n i t b o b b y . c o m / l a b s / v e r s i o n . x m l�   � o      �� 0 xmlfile xmlFile� ��� r    ��� n   ��� I   	 ���� 0 parsexml parseXML� ��� o   	 
�� 0 xmlfile xmlFile� ��� m   
 �� ���  < v e r s i o n >� ��� m    �� ���  < / v e r s i o n >�  �  �  f    	� o      �� 0 xmlitems xmlItems� ��� r    ��� n    ��� 4    ��
� 
cobj� m    �� � o    �� 0 xmlitems xmlItems� o      �� 0 versionitem versionItem� ��� l   ����  �  �  � ��� r    #��� c    !��� o    ��  0 currentversion currentVersion� m     �
� 
nmbr� o      �� 0 tempversion tempVersion� ��� r   $ )��� c   $ '��� o   $ %�� 0 versionitem versionItem� m   % &�
� 
nmbr� o      �� "0 tempversionitem tempVersionItem� ��� l  * *���
�  �  �
  � ��	� Z   * M����� l  * -���� A   * -��� o   * +�� 0 tempversion tempVersion� o   + ,�� "0 tempversionitem tempVersionItem�  �  � k   0 H�� ��� r   0 7��� o   0 1�� 0 versionitem versionItem� o      �� 0 
newversion 
newVersion� ��� r   8 E��� b   8 ?��� b   8 =��� m   8 9�� ��� j h t t p : / / w w w . i n i t b o b b y . c o m / l a b s / W o a h ,   B a c k   T h e   F u c k   U p  � l  9 <��� � c   9 <��� o   9 :���� 0 versionitem versionItem� m   : ;��
�� 
TEXT�  �   � m   = >�� ���  . a p p . z i p� o      ���� 0 
updatefile 
updateFile� ���� L   F H�� m   F G��
�� boovtrue��  �  � L   K M�� m   K L��
�� boovfals�	  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  �    l     ��������  ��  ��    i  �� I      ������ 0 parsexml parseXML  o      ���� 0 xml   	
	 o      ���� 0 opentag openTag
 �� o      ���� 0 closetag closeTag��  ��   k     f  r      J     ����   o      ���� 0 itemlist itemList  r    
 o    ���� 0 opentag openTag n      1    	��
�� 
txdl 1    ��
�� 
ascr  r     c     n     2    ��
�� 
citm o    ���� 0 xml   m    ��
�� 
list o      ���� 0 taglist tagList   r    !"! J    ����  " o      ���� 0 childtaglist childTagList  #$# l   ��������  ��  ��  $ %&% Y    4'��()��' s   ( /*+* n   ( ,,-, 4   ) ,��.
�� 
cobj. o   * +���� 0 x  - o   ( )���� 0 taglist tagList+ n      /0/  ;   - .0 o   , -���� 0 childtaglist childTagList�� 0 x  ( m    ���� ) I   #��1��
�� .corecnte****       ****1 n    232 2    ��
�� 
cobj3 o    ���� 0 taglist tagList��  ��  & 454 l  5 5��������  ��  ��  5 676 X   5 ]8��98 k   E X:: ;<; r   E J=>= o   E F���� 0 closetag closeTag> n     ?@? 1   G I��
�� 
txdl@ 1   F G��
�� 
ascr< ABA s   K RCDC n   K OEFE 4   L O��G
�� 
citmG m   M N���� F o   K L���� 0 thisitem thisItemD n      HIH  ;   P QI o   O P���� 0 itemlist itemListB J��J r   S XKLK o   S T���� 0 opentag openTagL n     MNM 1   U W��
�� 
txdlN 1   T U��
�� 
ascr��  �� 0 thisitem thisItem9 o   8 9���� 0 childtaglist childTagList7 OPO l  ^ ^��������  ��  ��  P QRQ L   ^ `SS o   ^ _���� 0 itemlist itemListR T��T I  a f��U��
�� .ascrcmnt****      � ****U o   a b���� 0 itemlist itemList��  ��   VWV l     ��������  ��  ��  W XYX l     ��������  ��  ��  Y Z[Z l     ��������  ��  ��  [ \]\ l     ��������  ��  ��  ] ^_^ i  ��`a` I      ��������  0 downloadupdate downloadUpdate��  ��  a k     Jbb cdc I    ��e��
�� .sysoexecTEXT���     TEXTe b     	fgf b     hih m     jj �kk  o p e n   'i o    ���� 0 
updatefile 
updateFileg m    ll �mm  '��  d non l   ��������  ��  ��  o pqp r    rsr c    tut b    vwv b    xyx m    zz �{{ . W o a h ,   B a c k   T h e   F u c k   U p  y o    ���� 0 
newversion 
newVersionw m    || �}}  . a p p . z i pu m    ��
�� 
TEXTs o      ���� 0 f  q ~~ r    '��� b    %��� n    #��� 1   ! #��
�� 
psxp� l   !������ I   !�����
�� .earsffdralis        afdr� m    ��
�� afdrdown��  ��  ��  � o   # $���� 0 f  � o      ���� 0 downloadfile downloadFile ��� r   ( .��� 4   ( ,���
�� 
psxf� o   * +���� 0 downloadfile downloadFile� o      ���� 0 downloadfile downloadFile� ��� l  / /������  �  �  � ��� I  / 4���
� .ascrcmnt****      � ****� o   / 0�� 0 f  �  � ��� I  5 :���
� .ascrcmnt****      � ****� o   5 6�� 0 downloadfile downloadFile�  � ��� l  ; ;����  �  �  � ��� O  ; H��� I  ? G���
� .aevtodocnull  �    alis� 4   ? C��
� 
file� o   A B�� 0 downloadfile downloadFile�  � m   ; <���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  I I��~�}�  �~  �}  �  _ ��� l     �|�{�z�|  �{  �z  � ��� l     �y���y  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �x���x  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �w�v�u�w  �v  �u  � ��� l     �t�s�r�t  �s  �r  � ��� l     �q�p�o�q  �p  �o  � ��� l     �n�m�l�n  �m  �l  � ��� l     �k�j�i�k  �j  �i  � ��� l     �h���h  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �g���g  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �f���f  � � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   � ��� - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �e�d�c�e  �d  �c  � ��� i  ����� I      �b��a�b $0 menuneedsupdate_ menuNeedsUpdate_� ��`� l     ��_�^� m      �]
�] 
cmnu�_  �^  �`  �a  � k     �� ��� l     �\���\  � J D NSMenu's delegates method, when the menu is clicked this is called.   � ��� �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .� ��� l     �[���[  � j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   � ��� �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .� ��� l     �Z���Z  � < 6 This means the menu items can be changed dynamically.   � ��� l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .� ��Y� n    ��� I    �X�W�V�X 0 	makemenus 	makeMenus�W  �V  �  f     �Y  � ��� l     �U�T�S�U  �T  �S  � ��� l     �R�Q�P�R  �Q  �P  � ��� l     �O�N�M�O  �N  �M  � ��� l     �L�K�J�L  �K  �J  � ��� l     �I�H�G�I  �H  �G  � ��� i  ����� I      �F�E�D�F 0 	makemenus 	makeMenus�E  �D  � k    @�� ��� l    	���� n    	��� I    	�C�B�A�C  0 removeallitems removeAllItems�B  �A  � o     �@�@ 0 newmenu newMenu� !  remove existing menu items   � ��� 6   r e m o v e   e x i s t i n g   m e n u   i t e m s� ��� l  
 
�?�>�=�?  �>  �=  � ��<� Y   
@��;���:� k   ;�� ��� r    '   n    % 4   " %�9
�9 
cobj o   # $�8�8 0 i   o    "�7�7 0 thelist theList o      �6�6 0 	this_item  �  l  ( (�5�4�3�5  �4  �3    Z   ( 	
�2	 l  ( -�1�0 =   ( - l  ( +�/�. c   ( + o   ( )�-�- 0 	this_item   m   ) *�,
�, 
TEXT�/  �.   m   + , �  B a c k u p   n o w�1  �0  
 r   0 @ l  0 >�+�* n  0 > I   7 >�)�(�) J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   7 8�'�' 0 	this_item    m   8 9 �  b a c k u p H a n d l e r :  �&  m   9 :!! �""  �&  �(   n  0 7#$# I   3 7�%�$�#�% 	0 alloc  �$  �#  $ n  0 3%&% o   1 3�"�" 0 
nsmenuitem 
NSMenuItem& m   0 1�!
�! misccura�+  �*   o      � �  0 thismenuitem thisMenuItem '(' l  C H)��) =   C H*+* l  C F,��, c   C F-.- o   C D�� 0 	this_item  . m   D E�
� 
TEXT�  �  + m   F G// �00 " B a c k u p   n o w   &   q u i t�  �  ( 121 r   K [343 l  K Y5��5 n  K Y676 I   R Y�8�� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_8 9:9 o   R S�� 0 	this_item  : ;<; m   S T== �>> * b a c k u p A n d Q u i t H a n d l e r :< ?�? m   T U@@ �AA  �  �  7 n  K RBCB I   N R���� 	0 alloc  �  �  C n  K NDED o   L N�� 0 
nsmenuitem 
NSMenuItemE m   K L�
� misccura�  �  4 o      �� 0 thismenuitem thisMenuItem2 FGF l  ^ cH��H =   ^ cIJI l  ^ aK��
K c   ^ aLML o   ^ _�	�	 0 	this_item  M m   _ `�
� 
TEXT�  �
  J m   a bNN �OO " T u r n   o n   t h e   b a n t s�  �  G PQP r   f xRSR l  f vT��T n  f vUVU I   m v�W�� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_W XYX o   m n�� 0 	this_item  Y Z[Z m   n o\\ �]]  n s f w O n H a n d l e r :[ ^�^ m   o r__ �``  �  �  V n  f maba I   i m�� ��� 	0 alloc  �   ��  b n  f icdc o   g i���� 0 
nsmenuitem 
NSMenuItemd m   f g��
�� misccura�  �  S o      ���� 0 thismenuitem thisMenuItemQ efe l  { �g����g =   { �hih l  { ~j����j c   { ~klk o   { |���� 0 	this_item  l m   | }��
�� 
TEXT��  ��  i m   ~ �mm �nn $ T u r n   o f f   t h e   b a n t s��  ��  f opo r   � �qrq l  � �s����s n  � �tut I   � ���v���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_v wxw o   � ����� 0 	this_item  x yzy m   � �{{ �||  n s f w O f f H a n d l e r :z }��} m   � �~~ �  ��  ��  u n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  r o      ���� 0 thismenuitem thisMenuItemp ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ��� * T u r n   o n   n o t i f i c a t i o n s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ��� , n o t i f i c a t i o n O n H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ��� , T u r n   o f f   n o t i f i c a t i o n s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ��� . n o t i f i c a t i o n O f f H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ���  Q u i t��  ��  � ��� r   � ���� l  � ����� n  � ���� I   � ����� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ��� 0 	this_item  � ��� m   � ��� ���  q u i t H a n d l e r :� ��� m   � ��� ���  �  �  � n  � ���� I   � ����� 	0 alloc  �  �  � n  � ���� o   � ��� 0 
nsmenuitem 
NSMenuItem� m   � ��
� misccura�  �  � o      �� 0 thismenuitem thisMenuItem�  �2   ��� l ����  �  �  � ��� l ���� n ��� I  ���� 0 additem_ addItem_� ��� o  �� 0 thismenuitem thisMenuItem�  �  � o  �� 0 newmenu newMenu�  �  � ��� l ����  �  �  � ��� l ���� l ���� n ��� I  ���� 0 
settarget_ 
setTarget_� ���  f  �  �  � o  �� 0 thismenuitem thisMenuItem�  �  � * $ required for enabling the menu item   � ��� H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m� ��� l ����  �  �  � ��� l 9���� Z 9����� G  "� � l �� =   o  �� 0 i   m  �� �  �    l �� =   o  �� 0 i   m  �� �  �  � l %5�� n %5	 I  *5�
�� 0 additem_ addItem_
 � l *1�� n *1 o  -1�� 0 separatoritem separatorItem n *- o  +-�� 0 
nsmenuitem 
NSMenuItem m  *+�
� misccura�  �  �  �  	 o  %*�� 0 newmenu newMenu�  �  �  �  �   add a seperator   � �     a d d   a   s e p e r a t o r� � l ::��    end if    �  e n d   i f�  �; 0 i  � m    �� � n     m    �
� 
nmbr n    2   �
� 
cobj o    �� 0 thelist theList�:  �<  �  l     ����  �  �    l     �~�}�|�~  �}  �|    l     �{�z�y�{  �z  �y    !  l     �x�w�v�x  �w  �v  ! "#" l     �u�t�s�u  �t  �s  # $%$ i  ��&'& I      �r�q�p�r 0 	initmenus 	initMenus�q  �p  ' Z     �()*�o( l    +�n�m+ =     ,-, o     �l�l 0 isnsfw isNSFW- m    �k
�k boovtrue�n  �m  ) Z   
 ?./0�j. l  
 1�i�h1 =   
 232 o   
 �g�g 0 tonotify toNotify3 m    �f
�f boovtrue�i  �h  / r    !454 J    66 787 m    99 �::  B a c k u p   n o w8 ;<; m    == �>> " B a c k u p   n o w   &   q u i t< ?@? m    AA �BB $ T u r n   o f f   t h e   b a n t s@ CDC m    EE �FF , T u r n   o f f   n o t i f i c a t i o n sD G�eG m    HH �II  Q u i t�e  5 o      �d�d 0 thelist theList0 JKJ l  $ +L�c�bL =   $ +MNM o   $ )�a�a 0 tonotify toNotifyN m   ) *�`
�` boovfals�c  �b  K O�_O r   . ;PQP J   . 5RR STS m   . /UU �VV  B a c k u p   n o wT WXW m   / 0YY �ZZ " B a c k u p   n o w   &   q u i tX [\[ m   0 1]] �^^ $ T u r n   o f f   t h e   b a n t s\ _`_ m   1 2aa �bb * T u r n   o n   n o t i f i c a t i o n s` c�^c m   2 3dd �ee  Q u i t�^  Q o      �]�] 0 thelist theList�_  �j  * fgf l  B Ih�\�[h =   B Iiji o   B G�Z�Z 0 isnsfw isNSFWj m   G H�Y
�Y boovfals�\  �[  g k�Xk Z   L �lmn�Wl l  L So�V�Uo =   L Spqp o   L Q�T�T 0 tonotify toNotifyq m   Q R�S
�S boovtrue�V  �U  m r   V crsr J   V ]tt uvu m   V Www �xx  B a c k u p   n o wv yzy m   W X{{ �|| " B a c k u p   n o w   &   q u i tz }~} m   X Y ��� " T u r n   o n   t h e   b a n t s~ ��� m   Y Z�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��R� m   Z [�� ���  Q u i t�R  s o      �Q�Q 0 thelist theListn ��� l  f m��P�O� =   f m��� o   f k�N�N 0 tonotify toNotify� m   k l�M
�M boovfals�P  �O  � ��L� r   p ���� J   p ��� ��� m   p s�� ���  B a c k u p   n o w� ��� m   s v�� ��� " B a c k u p   n o w   &   q u i t� ��� m   v y�� ��� " T u r n   o n   t h e   b a n t s� ��� m   y |�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��K� m   | �� ���  Q u i t�K  � o      �J�J 0 thelist theList�L  �W  �X  �o  % ��� l     �I�H�G�I  �H  �G  � ��� l     �F�E�D�F  �E  �D  � ��� l     �C�B�A�C  �B  �A  � ��� l     �@�?�>�@  �?  �>  � ��� l     �=�<�;�=  �<  �;  � ��� i  ����� I      �:��9�:  0 backuphandler_ backupHandler_� ��8� o      �7�7 
0 sender  �8  �9  � Z     �����6� l    ��5�4� =     ��� o     �3�3 0 isbackingup isBackingUp� m    �2
�2 boovfals�5  �4  � k   
 U�� ��� r   
 ��� m   
 �1
�1 boovtrue� o      �0�0 0 manualbackup manualBackup� ��/� Z    U���.�-� l   ��,�+� =    ��� o    �*�* 0 tonotify toNotify� m    �)
�) boovtrue�,  �+  � Z    Q����(� l   #��'�&� =    #��� o    !�%�% 0 isnsfw isNSFW� m   ! "�$
�$ boovtrue�'  �&  � k   & 3�� ��� I  & -�#��
�# .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �"��!
�" 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�!  � �� � I  . 3���
� .sysodelanull��� ��� nmbr� m   . /�� �  �   � ��� l  6 =���� =   6 =��� o   6 ;�� 0 isnsfw isNSFW� m   ; <�
� boovfals�  �  � ��� k   @ M�� ��� I  @ G���
� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� ���
� 
appr� m   B C�� ��� 
 W B T F U�  � ��� I  H M���
� .sysodelanull��� ��� nmbr� m   H I�� �  �  �  �(  �.  �-  �/  � ��� l  X _���� =   X _��� o   X ]�� 0 isbackingup isBackingUp� m   ] ^�
� boovtrue�  �  � ��� k   b ��� ��� r   b k��� n   b i��� 3   g i�
� 
cobj� o   b g�
�
 40 messagesalreadybackingup messagesAlreadyBackingUp� o      �	�	 0 msg  � ��� Z   l ������ l  l s���� =   l s��� o   l q�� 0 tonotify toNotify� m   q r�
� boovtrue�  �  � Z   v ������ l  v }�� ��� =   v }   o   v {���� 0 isnsfw isNSFW m   { |��
�� boovtrue�   ��  � I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � � � P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  �  l  � �	����	 =   � �

 o   � ����� 0 isnsfw isNSFW m   � ���
�� boovfals��  ��   �� I  � ���
�� .sysonotfnull��� ��� TEXT m   � � � 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p ����
�� 
appr m   � � � 
 W B T F U��  ��  �  �  �  �  �  �6  �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    i  �� !  I      ��"���� .0 backupandquithandler_ backupAndQuitHandler_" #��# o      ���� 
0 sender  ��  ��  ! k     �$$ %&% l     ��������  ��  ��  & '(' n    )*) I    �������� 0 setfocus setFocus��  ��  *  f     ( +,+ l   ��������  ��  ��  , -��- Z    �./0��. l   1����1 =    232 o    ���� 0 isbackingup isBackingUp3 m    ��
�� boovfals��  ��  / t    �454 k    �66 787 l    ��9:��  9 � �if (isNSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (isNSFW = false) then			set tempTitle to "WBTFU"		end if   : �;; i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f8 <=< r    *>?> l   (@����@ n    (ABA 1   & (��
�� 
bhitB l   &C����C I   &��DE
�� .sysodlogaskr        TEXTD m    FF �GG T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?E ��HI
�� 
apprH o    ���� 0 	temptitle 	tempTitleI ��JK
�� 
dispJ o    ���� 0 appicon appIconK ��LM
�� 
btnsL J     NN OPO m    QQ �RR  Y e sP S��S m    TT �UU  N o��  M ��V��
�� 
dfltV m   ! "�� ��  ��  ��  ��  ��  ? o      �� 	0 reply  = WXW l  + +����  �  �  X Y�Y Z   + �Z[\�Z l  + .]��] =   + .^_^ o   + ,�� 	0 reply  _ m   , -`` �aa  Y e s�  �  [ k   1 �bb cdc r   1 8efe m   1 2�
� boovtruef o      ��  0 backupthenquit backupThenQuitd ghg r   9 @iji m   9 :�
� boovtruej o      �� 0 manualbackup manualBackuph klk l  A A����  �  �  l m�m Z   A �no��n l  A Hp��p =   A Hqrq o   A F�� 0 tonotify toNotifyr m   F G�
� boovtrue�  �  o Z   K �stu�s l  K Rv��v =   K Rwxw o   K P�� 0 isnsfw isNSFWx m   P Q�
� boovtrue�  �  t k   U byy z{z I  U \�|}
� .sysonotfnull��� ��� TEXT| m   U V~~ � T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p} ���
� 
appr� m   W X�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�  { ��� I  ] b���
� .sysodelanull��� ��� nmbr� m   ] ^�� �  �  u ��� l  e l���� =   e l��� o   e j�� 0 isnsfw isNSFW� m   j k�
� boovfals�  �  � ��� k   o ��� ��� I  o z���
� .sysonotfnull��� ��� TEXT� m   o r�� ���   P r e p a r i n g   b a c k u p� ���
� 
appr� m   s v�� ��� 
 W B T F U�  � ��� I  { ����
� .sysodelanull��� ��� nmbr� m   { |�� �  �  �  �  �  �  �  \ ��� l  � ����� =   � ���� o   � ��� 	0 reply  � m   � ��� ���  N o�  �  � ��� r   � ���� m   � ��
� boovfals� o      ��  0 backupthenquit backupThenQuit�  �  �  5 m    ��  ��0 ��� l  � ����� =   � ���� o   � ��� 0 isbackingup isBackingUp� m   � ��
� boovtrue�  �  � ��� k   � ��� ��� r   � ���� n   � ���� 3   � ��
� 
cobj� o   � ��� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      �� 0 msg  � ��� Z   � �����~� l  � ���}�|� =   � ���� o   � ��{�{ 0 tonotify toNotify� m   � ��z
�z boovtrue�}  �|  � Z   � �����y� l  � ���x�w� =   � ���� o   � ��v�v 0 isnsfw isNSFW� m   � ��u
�u boovtrue�x  �w  � I  � ��t��
�t .sysonotfnull��� ��� TEXT� o   � ��s�s 0 msg  � �r��q
�r 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�q  � ��� l  � ���p�o� =   � ���� o   � ��n�n 0 isnsfw isNSFW� m   � ��m
�m boovfals�p  �o  � ��l� I  � ��k��
�k .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �j��i
�j 
appr� m   � ��� ��� 
 W B T F U�i  �l  �y  �  �~  �  �  ��  ��   ��� l     �h�g�f�h  �g  �f  � ��� l     �e�d�c�e  �d  �c  � ��� l     �b�a�`�b  �a  �`  � ��� l     �_�^�]�_  �^  �]  � ��� l     �\�[�Z�\  �[  �Z  � ��� i  ����� I      �Y��X�Y  0 nsfwonhandler_ nsfwOnHandler_� ��W� o      �V�V 
0 sender  �W  �X  � k     Q�� ��� r     ��� m     �U
�U boovtrue� o      �T�T 0 isnsfw isNSFW� ��� I    �S��R�S 0 updateplist updatePlist� ��� m   	 
�� ���  N S F W� ��Q� o   
 �P�P 0 isnsfw isNSFW�Q  �R  � ��� r    ��� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      �O�O 0 	temptitle 	tempTitle� ��� l   �N�M�L�N  �M  �L  � ��K� Z    Q����J� l   #��I�H� =    #��� o    !�G�G 0 tonotify toNotify� m   ! "�F
�F boovtrue�I  �H  � r   & 3��� J   & -�� ��� m   & '�� ���  B a c k u p   n o w� � � m   ' ( � " B a c k u p   n o w   &   q u i t   m   ( ) � $ T u r n   o f f   t h e   b a n t s  m   ) *		 �

 , T u r n   o f f   n o t i f i c a t i o n s �E m   * + �  Q u i t�E  � o      �D�D 0 thelist theList�  l  6 =�C�B =   6 = o   6 ;�A�A 0 tonotify toNotify m   ; <�@
�@ boovfals�C  �B   �? r   @ M J   @ G  m   @ A �  B a c k u p   n o w  m   A B � " B a c k u p   n o w   &   q u i t   m   B C!! �"" $ T u r n   o f f   t h e   b a n t s  #$# m   C D%% �&& * T u r n   o n   n o t i f i c a t i o n s$ '�>' m   D E(( �))  Q u i t�>   o      �=�= 0 thelist theList�?  �J  �K  � *+* l     �<�;�:�<  �;  �:  + ,-, i  ��./. I      �90�8�9 "0 nsfwoffhandler_ nsfwOffHandler_0 1�71 o      �6�6 
0 sender  �7  �8  / k     Q22 343 r     565 m     �5
�5 boovfals6 o      �4�4 0 isnsfw isNSFW4 787 I    �39�2�3 0 updateplist updatePlist9 :;: m   	 
<< �==  N S F W; >�1> o   
 �0�0 0 isnsfw isNSFW�1  �2  8 ?@? r    ABA m    CC �DD 
 W B T F UB o      �/�/ 0 	temptitle 	tempTitle@ EFE l   �.�-�,�.  �-  �,  F G�+G Z    QHIJ�*H l   #K�)�(K =    #LML o    !�'�' 0 tonotify toNotifyM m   ! "�&
�& boovtrue�)  �(  I r   & 3NON J   & -PP QRQ m   & 'SS �TT  B a c k u p   n o wR UVU m   ' (WW �XX " B a c k u p   n o w   &   q u i tV YZY m   ( )[[ �\\ " T u r n   o n   t h e   b a n t sZ ]^] m   ) *__ �`` , T u r n   o f f   n o t i f i c a t i o n s^ a�%a m   * +bb �cc  Q u i t�%  O o      �$�$ 0 thelist theListJ ded l  6 =f�#�"f =   6 =ghg o   6 ;�!�! 0 tonotify toNotifyh m   ; <� 
�  boovfals�#  �"  e i�i r   @ Mjkj J   @ Gll mnm m   @ Aoo �pp  B a c k u p   n o wn qrq m   A Bss �tt " B a c k u p   n o w   &   q u i tr uvu m   B Cww �xx " T u r n   o n   t h e   b a n t sv yzy m   C D{{ �|| * T u r n   o n   n o t i f i c a t i o n sz }�} m   D E~~ �  Q u i t�  k o      �� 0 thelist theList�  �*  �+  - ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i  ����� I      ���� 00 notificationonhandler_ notificationOnHandler_� ��� o      �
�
 
0 sender  �  �  � k     I�� ��� r     ��� m     �	
�	 boovtrue� o      �� 0 tonotify toNotify� ��� I    ���� 0 updateplist updatePlist� ��� m   	 
�� ���  N o t i f i c a t i o n s� ��� o   
 �� 0 tonotify toNotify�  �  � ��� Z    I����� l   ��� � =    ��� o    ���� 0 isnsfw isNSFW� m    ��
�� boovtrue�  �   � r    +��� J    %�� ��� m    �� ���  B a c k u p   n o w� ��� m     �� ��� " B a c k u p   n o w   &   q u i t� ��� m     !�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   ! "�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ���� m   " #�� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  . 5������ =   . 5��� o   . 3���� 0 isnsfw isNSFW� m   3 4��
�� boovfals��  ��  � ���� r   8 E��� J   8 ?�� ��� m   8 9�� ���  B a c k u p   n o w� ��� m   9 :�� ��� " B a c k u p   n o w   &   q u i t� ��� m   : ;�� ��� " T u r n   o n   t h e   b a n t s� ��� m   ; <�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ���� m   < =�� ���  Q u i t��  � o      ���� 0 thelist theList��  �  �  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 20 notificationoffhandler_ notificationOffHandler_� ���� o      ���� 
0 sender  ��  ��  � k     I�� ��� r     ��� m     ��
�� boovfals� o      ���� 0 tonotify toNotify� ��� I    ������� 0 updateplist updatePlist� ��� m   	 
�� ���  N o t i f i c a t i o n s� ���� o   
 ���� 0 tonotify toNotify��  ��  � ���� Z    I������ l   ������ =    ��� o    ���� 0 isnsfw isNSFW� m    ��
�� boovtrue��  ��  � r    +��� J    %�� ��� m    �� ���  B a c k u p   n o w� ��� m     �� ��� " B a c k u p   n o w   &   q u i t� ��� m     !�� �   $ T u r n   o f f   t h e   b a n t s�  m   ! " � * T u r n   o n   n o t i f i c a t i o n s �� m   " # �  Q u i t��  � o      ���� 0 thelist theList� 	 l  . 5
����
 =   . 5 o   . 3���� 0 isnsfw isNSFW m   3 4��
�� boovfals��  ��  	 �� r   8 E J   8 ?  m   8 9 �  B a c k u p   n o w  m   9 : � " B a c k u p   n o w   &   q u i t  m   : ; � " T u r n   o n   t h e   b a n t s  m   ; < �   * T u r n   o n   n o t i f i c a t i o n s !��! m   < ="" �##  Q u i t��   o      ���� 0 thelist theList��  ��  ��  � $%$ l     ��������  ��  ��  % &'& l     ��������  ��  ��  ' ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��������  ��  ��  - ./. i  ��010 I      ��2���� 0 quithandler_ quitHandler_2 3��3 o      ���� 
0 sender  ��  ��  1 t     �454 k    �66 787 n   9:9 I    �������� 0 setfocus setFocus��  ��  :  f    8 ;<; l   ��������  ��  ��  < =�= Z    �>?@�> l   A��A =    BCB o    �� 0 isbackingup isBackingUpC m    �
� boovtrue�  �  ? k    >DD EFE l    �GH�  G � �if (isNSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (isNSFW = false) then			set tempTitle to "WBTFU"		end if   H �II i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i fF JKJ l   ����  �  �  K LML r    *NON l   (P��P n    (QRQ 1   & (�
� 
bhitR l   &S��S I   &�TU
� .sysodlogaskr        TEXTT m    VV �WW � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?U �XY
� 
apprX o    �� 0 	temptitle 	tempTitleY �Z[
� 
dispZ o    �� 0 appicon appIcon[ �\]
� 
btns\ J     ^^ _`_ m    aa �bb  C a n c e l   &   Q u i t` c�c m    dd �ee  R e s u m e�  ] �f�
� 
dfltf m   ! "�� �  �  �  �  �  O o      �� 	0 reply  M g�g Z   + >hi�jh l  + .k��k =   + .lml o   + ,�� 	0 reply  m m   , -nn �oo  C a n c e l   &   Q u i t�  �  i k   1 6pp qrq l  1 1�st�  s / )quit application "Woah, Back The Fuck Up"   t �uu R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "r vwv l  1 1�xy�  x 5 /do shell script "kill 'Woah, Back The Fuck Up'"   y �zz ^ d o   s h e l l   s c r i p t   " k i l l   ' W o a h ,   B a c k   T h e   F u c k   U p ' "w {�{ n  1 6|}| I   2 6���� 0 killapp killApp�  �  }  f   1 2�  �  j I  9 >�~�
� .ascrcmnt****      � ****~ l  9 :�� m   9 :�� ���  W B T F U   r e s u m e d�  �  �  �  @ ��� l  A H���� =   A H��� o   A F�� 0 isbackingup isBackingUp� m   F G�
� boovfals�  �  � ��� k   K ��� ��� l   K K����  � � �if (isNSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (isNSFW = false) then			set tempTitle to "WBTFU"		end if   � ��� i f   ( i s N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( i s N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f� ��� l  K K����  �  �  � ��� r   K i��� l  K g���� n   K g��� 1   e g�
� 
bhit� l  K e���� I  K e���
� .sysodlogaskr        TEXT� m   K N�� ��� < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?� ���
� 
appr� o   O T�� 0 	temptitle 	tempTitle� ���
� 
disp� o   U V�� 0 appicon appIcon� ���
� 
btns� J   W _�� ��� m   W Z�� ���  Q u i t� ��� m   Z ]�� ���  R e s u m e�  � ���
� 
dflt� m   ` a�� �  �  �  �  �  � o      �~�~ 	0 reply  � ��}� Z   j ����|�� l  j o��{�z� =   j o��� o   j k�y�y 	0 reply  � m   k n�� ���  Q u i t�{  �z  � k   r w�� ��� l  r r�x���x  � / )quit application "Woah, Back The Fuck Up"   � ��� R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "� ��� l  r r�w���w  � 5 /do shell script "kill 'Woah, Back The Fuck Up'"   � ��� ^ d o   s h e l l   s c r i p t   " k i l l   ' W o a h ,   B a c k   T h e   F u c k   U p ' "� ��v� n  r w��� I   s w�u�t�s�u 0 killapp killApp�t  �s  �  f   r s�v  �|  � I  z ��r��q
�r .ascrcmnt****      � ****� l  z }��p�o� m   z }�� ���  W B T F U   r e s u m e d�p  �o  �q  �}  �  �  �  5 m     �n�n  ��/ ��� l     �m�l�k�m  �l  �k  � ��� l     �j�i�h�j  �i  �h  � ��� l     �g�f�e�g  �f  �e  � ��� l     �d�c�b�d  �c  �b  � ��� l     �a�`�_�a  �`  �_  � ��� i  ����� I      �^��]�^ 0 
updateicon 
updateIcon� ��\� o      �[�[ 0 mode  �\  �]  � k     O�� ��� Z     /����Z� l    ��Y�X� =     ��� o     �W�W 0 mode  � m    �� ���  a c t i v e�Y  �X  � r    ��� 4    �V�
�V 
alis� l   ��U�T� b    ��� l   ��S�R� I   �Q��
�Q .earsffdralis        afdr�  f    	� �P��O
�P 
rtyp� m   
 �N
�N 
ctxt�O  �S  �R  � m    �� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g�U  �T  � o      �M�M 0 	imagepath 	imagePath� ��� l   ��L�K� =    ��� o    �J�J 0 mode  � m    �� ���  i d l e�L  �K  � ��I� r    +��� 4    )�H�
�H 
alis� l   (��G�F� b    (��� l   &��E�D� I   &�C��
�C .earsffdralis        afdr�  f     � �B��A
�B 
rtyp� m   ! "�@
�@ 
ctxt�A  �E  �D  � m   & '�� �   J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�G  �F  � o      �?�? 0 	imagepath 	imagePath�I  �Z  �  l  0 0�>�=�<�>  �=  �<    r   0 5 n   0 3 1   1 3�;
�; 
psxp o   0 1�:�: 0 	imagepath 	imagePath o      �9�9 0 	imagepath 	imagePath 	
	 r   6 D n  6 B I   = B�8�7�8 20 initwithcontentsoffile_ initWithContentsOfFile_ �6 o   = >�5�5 0 	imagepath 	imagePath�6  �7   n  6 = I   9 =�4�3�2�4 	0 alloc  �3  �2   n  6 9 o   7 9�1�1 0 nsimage NSImage m   6 7�0
�0 misccura o      �/�/ 	0 image  
 �. n  E O I   J O�-�,�- 0 	setimage_ 	setImage_ �+ o   J K�*�* 	0 image  �+  �,   o   E J�)�) 0 
statusitem 
StatusItem�.  �  l     �(�'�&�(  �'  �&    l     �%�$�#�%  �$  �#    l     �"�!� �"  �!  �     !  l     ����  �  �  ! "#" l     ����  �  �  # $%$ l     �&'�  &   create an NSStatusBar   ' �(( ,   c r e a t e   a n   N S S t a t u s B a r% )*) i  ��+,+ I      ���� 0 makestatusbar makeStatusBar�  �  , k     H-- ./. l     �01�  0  log ("Make status bar")   1 �22 . l o g   ( " M a k e   s t a t u s   b a r " )/ 343 r     565 n    787 o    �� "0 systemstatusbar systemStatusBar8 n    9:9 o    �� 0 nsstatusbar NSStatusBar: m     �
� misccura6 o      �� 0 bar  4 ;<; l   ����  �  �  < =>= r    ?@? n   ABA I   	 �C�� .0 statusitemwithlength_ statusItemWithLength_C D�D m   	 
EE ��      �  �  B o    	�
�
 0 bar  @ o      �	�	 0 
statusitem 
StatusItem> FGF l   ����  �  �  G HIH n   JKJ I    �L�� 0 
updateicon 
updateIconL M�M m    NN �OO  i d l e�  �  K  f    I PQP l    �RS�  R��set imagePath to alias ((path to me as text) & "Contents:Resources:menubar_static.png")	set imagePath to POSIX path of imagePath	set image to current application's NSImage's alloc()'s initWithContentsOfFile:imagePath		--set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"		StatusItem's setImage:image   S �TT
 s e t   i m a g e P a t h   t o   a l i a s   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g " )  	 s e t   i m a g e P a t h   t o   P O S I X   p a t h   o f   i m a g e P a t h  	 s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : i m a g e P a t h  	  	 - - s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "  	  	 S t a t u s I t e m ' s   s e t I m a g e : i m a g eQ UVU l   �� ���  �   ��  V WXW l   ��YZ��  Y , & set up the initial NSStatusBars title   Z �[[ L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l eX \]\ l   ��^_��  ^ # StatusItem's setTitle:"WBTFU"   _ �`` : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "] aba l   ��������  ��  ��  b cdc l   ��ef��  e 1 + set up the initial NSMenu of the statusbar   f �gg V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a rd hih r    .jkj n   (lml I   # (��n����  0 initwithtitle_ initWithTitle_n o��o m   # $pp �qq  C u s t o m��  ��  m n   #rsr I    #�������� 	0 alloc  ��  ��  s n   tut o    ���� 0 nsmenu NSMenuu m    ��
�� misccurak o      ���� 0 newmenu newMenui vwv l  / /��������  ��  ��  w xyx l  / /��z{��  z � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   { �||0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .y }~} n  / 9� I   4 9������� 0 setdelegate_ setDelegate_� ����  f   4 5��  ��  � o   / 4���� 0 newmenu newMenu~ ��� l  : :��������  ��  ��  � ���� n  : H��� I   ? H������� 0 setmenu_ setMenu_� ���� o   ? D���� 0 newmenu newMenu��  ��  � o   : ?���� 0 
statusitem 
StatusItem��  * ��� l     ��������  ��  ��  � ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� 0 runonce runOnce��  ��  � k      �� ��� I    �����
�� .sysodelanull��� ��� nmbr� m     ���� ��  � ��� n   ��� I    ������� 0 makestatusbar makeStatusBar��  �  �  f    � ��� l   ����  �  delay 1   � ���  d e l a y   1� ��� n   ��� I    ���� 0 
updateicon 
updateIcon� ��� m    �� ���  a c t i v e�  �  �  f    � ��� I   ���
� .sysodelanull��� ��� nmbr� m    �� �  � ��� I    ���� 0 	plistinit 	plistInit�  �  � ��� l   ����  �  checkForUpdates()   � ��� " c h e c k F o r U p d a t e s ( )�  � ��� l     ����  �  �  � ��� l   ���� I    ���� 0 runonce runOnce�  �  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i  ����� I      ���� "0 checkforupdates checkForUpdates�  �  � Z     ������ l    ���� =     ��� I     ����  0 testconnection testConnection�  �  � m    �
� boovtrue�  �  � k   
 ��� ��� l  
 
����  �   Show update download   � ��� *   S h o w   u p d a t e   d o w n l o a d� ��� l   
 
����  � � �if (NSFW = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (NSFW = false) then			set tempTitle to "WBTFU"		end if   � ���
 i f   ( N S F W   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( N S F W   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f� ��� l  
 
����  �  �  � ��� Z   
 ������ l  
 ���� =   
 ��� I   
 ����� "0 getlatestupdate getLatestUpdate�  ��  � m    ��
�� boovtrue�  �  � t    d��� k    c�� ��� n   ��� I    �������� 0 setfocus setFocus��  ��  �  f    � ��� l   ��������  ��  ��  � ��� r    4��� l   2������ n    2��� 1   0 2��
�� 
bhit� l   0������ I   0����
�� .sysodlogaskr        TEXT� m    �� ��� d A   n e w   u p d a t e   w a s   f o u n d . 
 W h a t   w o u l d   y o u   l i k e   t o   d o ?� �� 
�� 
appr  o    #���� 0 	temptitle 	tempTitle �
� 
disp o   $ %�~�~ 0 appicon appIcon �}
�} 
btns J   & *  m   & '		 �

  C a n c e l   U p d a t e �| m   ' ( � , D o w n l o a d   U p d a t e   &   Q u i t�|   �{�z
�{ 
dflt m   + ,�y�y �z  ��  ��  ��  ��  � o      �x�x 	0 reply  � �w Z   5 c�v l  5 8�u�t =   5 8 o   5 6�s�s 	0 reply   m   6 7 �  C a n c e l   U p d a t e�u  �t   k   ; U  l  ; ;�r�r   / )quit application "Woah, Back The Fuck Up"    � R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "  l  ; ;�q !�q    5 /do shell script "kill 'Woah, Back The Fuck Up'"   ! �"" ^ d o   s h e l l   s c r i p t   " k i l l   ' W o a h ,   B a c k   T h e   F u c k   U p ' " #$# l  ; ;�p%&�p  %  my killApp()   & �''  m y   k i l l A p p ( )$ ()( I  ; @�o*�n
�o .sysodelanull��� ��� nmbr* m   ; <�m�m �n  ) +,+ n  A I-.- I   B I�l/�k�l 0 
updateicon 
updateIcon/ 0�j0 m   B E11 �22  a c t i v e�j  �k  .  f   A B, 343 I  J O�i5�h
�i .sysodelanull��� ��� nmbr5 m   J K�g�g �h  4 676 l  P P�f89�f  8  animateMenuBarIcon()   9 �:: ( a n i m a t e M e n u B a r I c o n ( )7 ;�e; I   P U�d�c�b�d 0 	plistinit 	plistInit�c  �b  �e  �v   k   X c<< =>= I   X ]�a�`�_�a  0 downloadupdate downloadUpdate�`  �_  > ?�^? n  ^ c@A@ I   _ c�]�\�[�] 0 killapp killApp�\  �[  A  f   ^ _�^  �w  � m    �Z�Z  ��� BCB l  g nD�Y�XD =   g nEFE I   g l�W�V�U�W "0 getlatestupdate getLatestUpdate�V  �U  F m   l m�T
�T boovfals�Y  �X  C G�SG k   q �HH IJI I  q v�RK�Q
�R .sysodelanull��� ��� nmbrK m   q r�P�P �Q  J LML l  w w�ONO�O  N  animateMenuBarIcon()   O �PP ( a n i m a t e M e n u B a r I c o n ( )M QRQ n  w STS I   x �NU�M�N 0 
updateicon 
updateIconU V�LV m   x {WW �XX  a c t i v e�L  �M  T  f   w xR YZY I  � ��K[�J
�K .sysodelanull��� ��� nmbr[ m   � ��I�I �J  Z \�H\ I   � ��G�F�E�G 0 	plistinit 	plistInit�F  �E  �H  �S  �  �  �  � k   � �]] ^_^ l  � ��D`a�D  `   Carry on as normal   a �bb &   C a r r y   o n   a s   n o r m a l_ cdc I  � ��Ce�B
�C .sysodelanull��� ��� nmbre m   � ��A�A �B  d fgf l  � ��@hi�@  h  animateMenuBarIcon()   i �jj ( a n i m a t e M e n u B a r I c o n ( )g k�?k I   � ��>�=�<�> 0 	plistinit 	plistInit�=  �<  �?  � lml l     �;�:�9�;  �:  �9  m non i  ��pqp I      �8�7�6�8 0 asktoupdate askToUpdate�7  �6  q l     �5�4�3�5  �4  �3  o rsr l     �2�1�0�2  �1  �0  s tut l     �/�.�-�/  �.  �-  u vwv l     �,�+�*�,  �+  �*  w xyx l     �)�(�'�)  �(  �'  y z{z l     �&�%�$�&  �%  �$  { |}| l     �#~�#  ~ w q Function that is always running in the background. This doesn't need to get called as it is running from the off    ��� �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f} ��� l     �"���"  � � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   � ����   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .� ��!� i  ����� I     � ��
�  .miscidlenmbr    ��� null�  �  � k     ��� ��� Z     ������ l    ���� =     ��� o     �� 0 	idleready 	idleReady� m    �
� boovtrue�  �  � Z   
 ������ l  
 ���� E   
 ��� o   
 �� &0 displaysleepstate displaySleepState� o    �� 0 strawake strAwake�  �  � Z    ������ l   ���� =    ��� o    �� 0 isbackingup isBackingUp� m    �
� boovfals�  �  � Z   " ������ l  " )��
�	� =   " )��� o   " '�� 0 manualbackup manualBackup� m   ' (�
� boovfals�
  �	  � k   , q�� ��� l  , ,����  � . (if ((hours of (current date)) < 18) then   � ��� P i f   ( ( h o u r s   o f   ( c u r r e n t   d a t e ) )   <   1 8 )   t h e n� ��� r   , 5��� l  , 3���� n   , 3��� 1   1 3�
� 
min � l  , 1���� l  , 1�� ��� I  , 1������
�� .misccurdldt    ��� null��  ��  �   ��  �  �  �  �  � o      ���� 0 m  � ��� l  6 6��������  ��  ��  � ��� Z   6 o������ G   6 A��� l  6 9������ =   6 9��� o   6 7���� 0 m  � m   7 8���� ��  ��  � l  < ?������ =   < ?��� o   < =���� 0 m  � m   = >���� -��  ��  � k   D U�� ��� Z  D S������� l  D G������ =   D G��� o   D E���� 0 intervaltime intervalTime� m   E F���� ��  ��  � I   J O�������� 0 	plistinit 	plistInit��  ��  ��  ��  � ���� l  T T������  �  end if   � ���  e n d   i f��  � ��� G   X c��� l  X [������ =   X [��� o   X Y���� 0 m  � m   Y Z����  ��  ��  � l  ^ a������ =   ^ a��� o   ^ _���� 0 m  � m   _ `���� ��  ��  � ���� I   f k�������� 0 	plistinit 	plistInit��  ��  ��  ��  � ���� l  p p������  �  end if   � ���  e n d   i f��  � ��� l  t {������ =   t {��� o   t y���� 0 manualbackup manualBackup� m   y z��
�� boovtrue��  ��  � ���� I   ~ ��������� 0 	plistinit 	plistInit��  ��  ��  �  �  �  �  �  �  �  � ��� l  � ���������  ��  ��  � ���� L   � ��� m   � ����� ��  �!       B��� T��� ���������������� �������������
��������������� 	
��  � @����������������������������������������������������������������~�  0 currentversion currentVersion
� 
pimr� 0 
statusitem 
StatusItem� 0 
thedisplay 
theDisplay� 0 defaults  � $0 internalmenuitem internalMenuItem� $0 externalmenuitem externalMenuItem� 0 newmenu newMenu� 0 thelist theList� 0 isnsfw isNSFW� 0 tonotify toNotify�  0 backupthenquit backupThenQuit� 0 	idleready 	idleReady� 0 isconnected isConnected� 0 
updatefile 
updateFile� 0 
newversion 
newVersion� 	0 plist  � 0 initialbackup initialBackup� 0 manualbackup manualBackup� 0 isbackingup isBackingUp� "0 messagesmissing messagesMissing� *0 messagesencouraging messagesEncouraging� $0 messagescomplete messagesComplete� &0 messagescancelled messagesCancelled� 40 messagesalreadybackingup messagesAlreadyBackingUp� 0 strawake strAwake� 0 strsleep strSleep� &0 displaysleepstate displaySleepState� 0 	temptitle 	tempTitle� 0 	plistinit 	plistInit� 0 diskinit diskInit� 0 freespaceinit freeSpaceInit� 0 
backupinit 
backupInit� 0 tidyup tidyUp� 
0 backup  � 0 itexists itExists� .0 getcomputeridentifier getComputerIdentifier� 0 gettimestamp getTimestamp� 0 comparesizes compareSizes� 0 getduration getDuration� 0 setfocus setFocus�  0 testconnection testConnection� 0 updateplist updatePlist� 0 killapp killApp� "0 getlatestupdate getLatestUpdate� 0 parsexml parseXML�  0 downloadupdate downloadUpdate� $0 menuneedsupdate_ menuNeedsUpdate_� 0 	makemenus 	makeMenus� 0 	initmenus 	initMenus�  0 backuphandler_ backupHandler_� .0 backupandquithandler_ backupAndQuitHandler_�  0 nsfwonhandler_ nsfwOnHandler_� "0 nsfwoffhandler_ nsfwOffHandler_� 00 notificationonhandler_ notificationOnHandler_� 20 notificationoffhandler_ notificationOffHandler_� 0 quithandler_ quitHandler_� 0 
updateicon 
updateIcon� 0 makestatusbar makeStatusBar� 0 runonce runOnce� "0 checkforupdates checkForUpdates� 0 asktoupdate askToUpdate
� .miscidlenmbr    ��� null
�~ .aevtoappnull  �   � ****� �}�}    �| i�{
�| 
vers�{   �z�y
�z 
cobj    �x
�x 
osax�y   �w�v
�w 
cobj    �u r
�u 
frmk�v   �t�s
�t 
cobj    �r x
�r 
frmk�s  
�� 
msng�    �q�p!
�q misccura
�p 
pcls! �""  N S U s e r D e f a u l t s� ## �o�n$
�o misccura
�n 
pcls$ �%%  N S M e n u I t e m� && �m�l'
�m misccura
�l 
pcls' �((  N S M e n u I t e m� )) �k�j*
�k misccura
�j 
pcls* �++  N S M e n u� �i,�i ,   � � � � �
�� boovfals
�� boovtrue
�� boovfals
�� boovfals
�� boovtrue��  � �-- � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�� boovtrue
� boovfals
� boovfals� �h.�h .  CGKOR� �g/�g 
/ 
 \`dhlptx|� �f0�f 
0 
 ����������� �e1�e 	1 	 ���������� �d2�d 	2 	 ���������� �33�             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 2 1 4 2 0 4 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 0 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 1 9 , " I d l e T i m e r E l a p s e d T i m e " = 1 7 6 2 2 7 3 , " C u r r e n t P o w e r S t a t e " = 4 }� �cF�b�a45�`�c 0 	plistinit 	plistInit�b  �a  4 
�_�^�]�\�[�Z�Y�X�W�V�_ 0 
foldername 
folderName�^ 0 
backupdisk 
backupDisk�] 0 
userfolder 
userFolder�\ 0 
backuptime 
backupTime�[ 0 thecontents theContents�Z 0 thevalue theValue�Y 0 welcome  �X 	0 reply  �W 0 backuptimes backupTimes�V 0 selectedtime selectedTime5 G_�U��T�S�R�Q�P�O�N�M�L�K�J�IZ�H�G��F�E�D�C��B�A�@�?�>�=�<��;�:�9�8�7�6"/�5;�4D�3O�2�1�0�/�.�-�,�+�*p�)�������(�'�&�%�$�#�U 0 itexists itExists
�T 
plif
�S 
pcnt
�R 
valL�Q 0 
appversion 
AppVersion�P  0 foldertobackup FolderToBackup�O 0 backupdrive BackupDrive�N  0 computerfolder ComputerFolder�M 0 scheduledtime ScheduledTime�L 0 nsfw NSFW�K 0 notifications Notifications�J 0 	initmenus 	initMenus�I .0 getcomputeridentifier getComputerIdentifier�H  ���G 0 setfocus setFocus
�F 
appr
�E 
disp�D 0 appicon appIcon
�C 
btns
�B 
dflt�A 
�@ .sysodlogaskr        TEXT
�? 
bhit
�> afdrcusr
�= .earsffdralis        afdr
�< 
prmp
�; .sysostflalis    ��� null
�: 
TEXT�9  �8  �7 0 killapp killApp
�6 
psxp
�5 .gtqpchltns    @   @ ns  �4 �3 �2 <
�1 
kocl
�0 
prdt
�/ 
pnam�. 
�- .corecrel****      � null
�, 
plii
�+ 
insh
�* 
kind�) �( 0 sourcefolder sourceFolder�' "0 destinationdisk destinationDisk�& 0 machinefolder machineFolder�% 0 intervaltime intervalTime
�$ .ascrcmnt****      � ****�# 0 diskinit diskInit�`�jE�O*�b  l+ e  Z� L*�b  /�,E�O��,E�O��,Ec   O��,E�O��,E�O��,E�O��,E�O��,Ec  	O��,Ec  
OPUO*j+ Y*j+ E�O� �a n)j+ Oa E�O�a b  a _ a a kva ka  a ,E�Oa j E�O *a a l  a !&E�W X " #)j+ $O�a %,E�O�a %,E�Oa &a 'a (mvE�O�a a )l *kva !&E�O�a +  
a ,E�Y #�a -  
a .E�Y �a /  
a 0E�Y hOPoUO�A*a 1�a 2a 3b  la 4 5%*a 1a 6a 7*6a 2a 8a !a 3a 9�b   a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a ;�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a <�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a =�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a >�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a ?�b  	a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a @�b  
a :a : 5UUO�E` AO�E` BO�E` CO�E` DO_ A_ B_ C_ Da 4vj EO*j+ F� �"�!� 67��" 0 diskinit diskInit�!  �   6 ��� 0 msg  � 	0 reply  7 ����������IL����V�s�}�� "0 destinationdisk destinationDisk� 0 itexists itExists� 0 freespaceinit freeSpaceInit�  ��� 0 setfocus setFocus
� 
cobj
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit� 0 diskinit diskInit
� .sysonotfnull��� ��� TEXT� �*��l+ e  *fk+ Y ��n)j+ Ob  �.E�O��b  �����lv�l� a ,E�O�a   
*j+ Y [b  b   Lb  �.E�Ob  
e  4b  	e  ��a l Y b  	f  a �a l Y hY hY ho� ����
89�	� 0 freespaceinit freeSpaceInit� �:� :  �� 	0 again  �
  8 ���� 	0 again  � 	0 reply  � 0 msg  9 ���� ������������������������������(��25� 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 comparesizes compareSizes�  0 
backupinit 
backupInit��  ���� 0 setfocus setFocus
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp
�� 
cobj
�� .sysonotfnull��� ��� TEXT�	 �*��l+ e  
*j+ Y ��n)j+ O�f  ��b  �����lv�l� a ,E�Y ,�e  %a �b  ���a a lv�l� a ,E�Y hO�a   
*j+ Y ]b  b   b  a .E�Y hOb  
e  4b  	e  ��a l Y b  	f  a �a l Y hY ho� ��N����;<���� 0 
backupinit 
backupInit��  ��  ;  < ������fk��y����������w������������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  �� 0 itexists itExists
�� 
kocl
�� 
cfol
�� 
insh
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null�� 0 machinefolder machineFolder
�� 
cdis�� 0 backupfolder backupFolder�� 0 latestfolder latestFolder�� 
0 backup  �� �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` UO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` UO*j+ � ������=>���� 0 tidyup tidyUp��  ��  = ������������������ 0 bf bF�� 0 creationdates creationDates�� 0 theoldestdate theOldestDate�� 0 j  �� 0 i  �� 0 thisdate thisDate��  0 foldertodelete folderToDelete�� 0 todelete toDelete> ����������-����������[�����������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  
�� 
cdis
�� 
cfol�� 0 machinefolder machineFolder
�� 
cobj
�� 
ascd
�� .corecnte****       ****
�� 
pnam
�� .ascrcmnt****      � ****
�� 
TEXT
�� 
psxp
�� .sysoexecTEXT���     TEXT
�� 
appr
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr�� 0 freespaceinit freeSpaceInit��*��/E�O� �*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/�&E�O��,�&E�Oa �%j Oa �%a %E�O�j Ob  b   Tb  
e  Fb  	e  a a a l Okj Y #b  	f  a a a l Okj Y hY hY hO*ek+ U� ����?@��� 
0 backup  �  �  ? 
����������� 0 t  � 0 x  � "0 containerfolder containerFolder� 0 
foldername 
folderName� 0 d  � 0 duration  � 0 msg  � 0 c  � 0 oldestfolder oldestFolder� 0 todelete toDelete@ f�9��j����������������������"�=?Agik��������������					.	0	2	:�	y	�	�	�	���	�
 

5
G
I
L
_
a
d
y
{
~
�
�
�
�
�
�
�
�
�
�
�,.1DFIU� 0 gettimestamp getTimestamp� 0 
updateicon 
updateIcon
� .sysodelanull��� ��� nmbr
� 
psxf� 0 sourcefolder sourceFolder
� 
TEXT
� 
cfol
� 
pnam
� 
kocl
� 
insh� 0 backupfolder backupFolder
� 
prdt� 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � 0 machinefolder machineFolder� (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
� 
appr
� .sysonotfnull��� ��� TEXT� "0 destinationdisk destinationDisk
� .sysoexecTEXT���     TEXT�  �  � 0 endtime endTime� 0 getduration getDuration
� 
cobj� 0 killapp killApp
� .ascrcmnt****      � ****
� 
alis
� 
psxp��*ek+  E�OeEc  O)�k+ Okj O�m*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e k*j a ,E` Ob  b   Hb  
e  :b  	e  a a a l Y b  	f  a a a l Y hY hY hO_ a  %_ %a !%�%a "%�&E�O a #�%a $%�%a %%j &OPW X ' (hOfEc  OfEc  OeEc  Ob  b   �*j a ,E` )O)j+ *E�Ob  a +.E�Ob  
e  Tb  	e   a ,�%a -%�%a a .l Okj Y )b  	f  a /�%a 0%a a 1l Okj Y hY hO)a 2k+ Y hOb  e  
)j+ 3Y hY�b  f _ a 4%_ %a 5%�%a 6%�%a 7%�&E�O_ a 8%_ %a 9%�%a :%�&E�Oa ;�%j <Ob  b   p*j a ,E` Ob  a +.E�Ob  
e  Hb  	e  �a b  l Okj Y %b  	f  a =a b  l Okj Y hY hY hO  a >�%a ?%�%a @%�%a A%j &OPW X ' (hOfEc  OfEc  OeEc  O*�/a B&a +-jv ^��/�&E�O�a C,�&E�Oa D�%j <Oa E�%a F%E�O�j &Ob  b  *j a ,E` )O)j+ *E�Ob  a +.E�Ob  
e  �a G  Tb  	e   a H�%a I%�%a a Jl Okj Y )b  	f  a K�%a L%a a Ml Okj Y hY Qb  	e   a N�%a O%�%a a Pl Okj Y )b  	f  a Q�%a R%a a Sl Okj Y hOb  	e  a Ta a Ul Y b  	f  a Va a Wl Y hY hO)a Xk+ Y hYb  b   �*j a ,E` )O)j+ *E�Ob  a +.E�Ob  
e  ��a G  Tb  	e   a Y�%a Z%�%a a [l Okj Y )b  	f  a \�%a ]%a a ^l Okj Y hY Qb  	e   a _�%a `%�%a a al Okj Y )b  	f  a b�%a c%a a dl Okj Y hY hY hO)a ek+ Ob  e  
)j+ 3Y hOPY hUOP� ����AB�� 0 itexists itExists� �C� C  ��� 0 
objecttype 
objectType� 
0 object  �  A ��� 0 
objecttype 
objectType� 
0 object  B ��������
� 
cdis
� .coredoexnull���     ****
� 
file
� 
cfol� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU� ����DE�� .0 getcomputeridentifier getComputerIdentifier�  �  D �~�}�|�~ 0 computername computerName�} "0 strserialnumber strSerialNumber�|  0 identifiername identifierNameE �{�z�y
�{ .sysosigtsirr   ��� null
�z 
sicn
�y .sysoexecTEXT���     TEXT� *j  �,E�O�j E�O��%�%E�O�� �x*�w�vFG�u�x 0 gettimestamp getTimestamp�w �tH�t H  �s�s 0 isfolder isFolder�v  F �r�q�p�o�n�m�l�k�j�i�h�g�f�e�d�c�r 0 isfolder isFolder�q 0 y  �p 0 m  �o 0 d  �n 0 t  �m 0 ty tY�l 0 tm tM�k 0 td tD�j 0 tt tT�i 
0 tml tML�h 
0 tdl tDL�g 0 timestr timeStr�f 0 pos  �e 0 h  �d 0 s  �c 0 	timestamp  G �b�a�`�_�^�]�\�[�Z�Y�X�W�V�U�T���S�R�Q��P�O�N�M�$WYv
�b 
Krtn
�a 
year�` 0 y  
�_ 
mnth�^ 0 m  
�] 
day �\ 0 d  
�[ 
time�Z 0 t  �Y 
�X .misccurdldt    ��� null
�W 
long
�V 
TEXT
�U .corecnte****       ****
�T 
nmbr
�S 
tstr
�R misccura
�Q 
psof
�P 
psin�O 
�N .sysooffslong    ��� null
�M 
cha �u�*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�� �L��K�JIJ�I�L 0 comparesizes compareSizes�K �HK�H K  �G�F�G 
0 source  �F 0 destination  �J  I �E�D�C�B�A�@�?�>�=�<�;�:�9�8�7�6�E 
0 source  �D 0 destination  �C 0 fit  �B 0 
templatest 
tempLatest�A $0 latestfoldersize latestFolderSize�@ 0 c  �? 0 l  �> 0 md  �= 0 	cachesize 	cacheSize�< 0 logssize logsSize�; 0 mdsize mdSize�: 
0 buffer  �9 (0 adjustedfoldersize adjustedFolderSize�8 0 
foldersize 
folderSize�7 0 	freespace 	freeSpace�6 0 temp  J !�5�4��3��2�1�0�/�.������-�,�+�*�)�(�'IKfh���&��
�5 
psxf�4 "0 destinationdisk destinationDisk�3 0 machinefolder machineFolder
�2 afdrdlib
�1 
from
�0 fldmfldu
�/ .earsffdralis        afdr
�. 
psxp
�- .sysoexecTEXT���     TEXT
�, misccura�+ �* d
�) .sysorondlong        doub
�( 
cdis
�' 
frsp
�& .ascrcmnt****      � ****�I�eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O�*�%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�O��%�%j Ob  f  1a �%a  %j E�Oa  �a !a  j Ua !E�OPY hUOb  f  .��E�O�j ��l E�Y hO��� fE�Y hOPY !b  e  ��� fE�Y hOPY hO�� �%N�$�#LM�"�% 0 getduration getDuration�$  �#  L �!�! 0 duration  M � ������  0 endtime endTime� 0 	starttime 	startTime� <
� misccura� d
� .sysorondlong        doub�" ���!E�O� 	�� j U�!E�O�� �y��NO�� 0 setfocus setFocus�  �  N ��� 0 processlist ProcessList� 0 thepid ThePIDO 	��������P
� 
prcs
� 
pnam
� 
idux
� .ascrcmnt****      � ****
� 
pisfP  � <� 8*�-�,E�O�� )*��/�,E�O�j O� e*�-�,�[�,\Z�81FUY hU� ����QR��  0 testconnection testConnection�  �  Q �� 0 i  R �
��	�����
 
�	 .sysoexecTEXT���     TEXT�  �  
� .sysodelanull��� ��� nmbr
� .sysobeepnull��� ��� long� F =k�kh   �j OW $X  �j O*j O��  fEc  Y h[OY��Ob  � �
��ST�� 0 updateplist updatePlist� � U�  U  ������ 0 tempitem tempItem�� 0 	tempvalue 	tempValue�  S 
���������������������� 0 tempitem tempItem�� 0 	tempvalue 	tempValue�� 0 	tempplist 	tempPlist�� 0 thecontents theContents�� 0 thevalue theValue�� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk��  0 computerfolder ComputerFolder�� 0 
backuptime 
backupTime�� 0 scheduledtime ScheduledTimeT "��������������������������
�� 
plif
�� 
plii
�� 
valL
�� 
pcnt��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive��  0 computerfolder ComputerFolder�� 0 scheduledtime ScheduledTime�� 0 nsfw NSFW�� 0 notifications Notifications�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 machinefolder machineFolder� r� *�b  /E�O� 
�*�/�,FUUO� @*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�O��,Ec  	O��,Ec  
UO�E�O�E�O�E�O�E�  ��{����VW���� 0 killapp killApp��  ��  V ������ 0 processlist ProcessList�� 0 thepid ThePIDW ������������
�� 
prcs
�� 
pnam
�� 
idux
�� .sysoexecTEXT���     TEXT�� (� $*�-�,E�O�� *��/�,E�O�%j Y hU �������XY���� "0 getlatestupdate getLatestUpdate��  ��  X ������������ 0 xmlfile xmlFile�� 0 xmlitems xmlItems�� 0 versionitem versionItem�� 0 tempversion tempVersion�� "0 tempversionitem tempVersionItemY 
���������������
�� .sysoexecTEXT���     TEXT�� 0 parsexml parseXML
�� 
cobj
�� 
nmbr
�� 
TEXT�� N�j E�O)���m+ E�O��k/E�Ob   �&E�O��&E�O�� �Ec  O��&%�%Ec  OeY f ������Z[���� 0 parsexml parseXML�� ��\�� \  �������� 0 xml  �� 0 opentag openTag�� 0 closetag closeTag��  Z ������������������ 0 xml  �� 0 opentag openTag�� 0 closetag closeTag�� 0 itemlist itemList�� 0 taglist tagList�� 0 childtaglist childTagList�� 0 x  �� 0 thisitem thisItem[ ����������������
�� 
ascr
�� 
txdl
�� 
citm
�� 
list
�� 
cobj
�� .corecnte****       ****
�� 
kocl
�� .ascrcmnt****      � ****�� gjvE�O���,FO��-�&E�OjvE�O l��-j kh ��/�6G[OY��O '�[��l kh ���,FO��k/�6GO���,F[OY��O�O�j  ��a����]^����  0 downloadupdate downloadUpdate��  ��  ] ������ 0 f  �� 0 downloadfile downloadFile^ jl��z|�����������������
�� .sysoexecTEXT���     TEXT
�� 
TEXT
�� afdrdown
�� .earsffdralis        afdr
�� 
psxp
�� 
psxf
�� .ascrcmnt****      � ****
�� 
file
�� .aevtodocnull  �    alis�� K�b  %�%j O�b  %�%�&E�O�j �,�%E�O*�/E�O�j 
O�j 
O� 
*�/j UOP �������_`���� $0 menuneedsupdate_ menuNeedsUpdate_�� ��a�� a  ��
�� 
cmnu��  _  ` ���� 0 	makemenus 	makeMenus�� )j+   �����bc��� 0 	makemenus 	makeMenus�  �  b ���� 0 i  � 0 	this_item  � 0 thismenuitem thisMenuItemc "�������!�/=@N\_m{~���������������  0 removeallitems removeAllItems
� 
cobj
� 
nmbr
� 
TEXT
� misccura� 0 
nsmenuitem 
NSMenuItem� 	0 alloc  � J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� 0 additem_ addItem_� 0 
settarget_ 
setTarget_� 
� 
bool� 0 separatoritem separatorItem�Ab  j+  O5kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y hOP[OY�� �'��de�� 0 	initmenus 	initMenus�  �  d  e 9=AEH�UY]adw{�������� � �b  	e  :b  
e  ������vEc  Y b  
f  ������vEc  Y hY Ob  	f  Db  
e  ������vEc  Y 'b  
f  a a a a a �vEc  Y hY h ����fg��  0 backuphandler_ backupHandler_� �h� h  �� 
0 sender  �  f ��� 
0 sender  � 0 msg  g ����~�}���|
� 
appr
�~ .sysonotfnull��� ��� TEXT
�} .sysodelanull��� ��� nmbr
�| 
cobj� �b  f  PeEc  Ob  
e  :b  	e  ���l Okj Y b  	f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  
e  .b  	e  ���l Y b  	f  ���l Y hY hY h �{!�z�yij�x�{ .0 backupandquithandler_ backupAndQuitHandler_�z �wk�w k  �v�v 
0 sender  �y  i �u�t�s�u 
0 sender  �t 	0 reply  �s 0 msg  j �r�qF�p�o�n�mQT�l�k�j�i`~��h�g����f����r 0 setfocus setFocus�q  ��
�p 
appr
�o 
disp�n 0 appicon appIcon
�m 
btns
�l 
dflt�k 
�j .sysodlogaskr        TEXT
�i 
bhit
�h .sysonotfnull��� ��� TEXT
�g .sysodelanull��� ��� nmbr
�f 
cobj�x �)j+  Ob  f  ��n��b  �����lv�l� �,E�O��  \eEc  OeEc  Ob  
e  >b  	e  ���l Okj Y !b  	f  a �a l Okj Y hY hY �a   fEc  Y hoY Yb  e  Nb  a .E�Ob  
e  4b  	e  ��a l Y b  	f  a �a l Y hY hY h	 �e��d�clm�b�e  0 nsfwonhandler_ nsfwOnHandler_�d �an�a n  �`�` 
0 sender  �c  l �_�_ 
0 sender  m ��^��	�]!%(�^ 0 updateplist updatePlist�] �b ReEc  	O*�b  	l+ O�Ec  Ob  
e  ������vEc  Y b  
f  ������vEc  Y h
 �\/�[�Zop�Y�\ "0 nsfwoffhandler_ nsfwOffHandler_�[ �Xq�X q  �W�W 
0 sender  �Z  o �V�V 
0 sender  p <�UCSW[_b�Tosw{~�U 0 updateplist updatePlist�T �Y RfEc  	O*�b  	l+ O�Ec  Ob  
e  ������vEc  Y b  
f  ������vEc  Y h �S��R�Qrs�P�S 00 notificationonhandler_ notificationOnHandler_�R �Ot�O t  �N�N 
0 sender  �Q  r �M�M 
0 sender  s ��L������K������L 0 updateplist updatePlist�K �P JeEc  
O*�b  
l+ Ob  	e  ������vEc  Y b  	f  ������vEc  Y h �J��I�Huv�G�J 20 notificationoffhandler_ notificationOffHandler_�I �Fw�F w  �E�E 
0 sender  �H  u �D�D 
0 sender  v ��C����B"�C 0 updateplist updatePlist�B �G JfEc  
O*�b  
l+ Ob  	e  ������vEc  Y b  	f  ������vEc  Y h �A1�@�?xy�>�A 0 quithandler_ quitHandler_�@ �=z�= z  �<�< 
0 sender  �?  x �;�:�; 
0 sender  �: 	0 reply  y �9�8V�7�6�5�4ad�3�2�1�0n�/��.������9  ���8 0 setfocus setFocus
�7 
appr
�6 
disp�5 0 appicon appIcon
�4 
btns
�3 
dflt�2 
�1 .sysodlogaskr        TEXT
�0 
bhit�/ 0 killapp killApp
�. .ascrcmnt****      � ****�> ��n)j+ Ob  e  1��b  �����lv�l� �,E�O��  
)j+ Y �j Y Fb  f  ;a �b  ���a a lv�l� �,E�O�a   
)j+ Y 	a j Y ho �-��,�+{|�*�- 0 
updateicon 
updateIcon�, �)}�) }  �(�( 0 mode  �+  { �'�&�%�' 0 mode  �& 0 	imagepath 	imagePath�% 	0 image  | ��$�#�"�!���� �����
�$ 
alis
�# 
rtyp
�" 
ctxt
�! .earsffdralis        afdr
�  
psxp
� misccura� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_�* P��  *�)��l �%/E�Y ��  *�)��l �%/E�Y hO��,E�O��,j+ �k+ E�Ob  �k+  �,��~�� 0 makestatusbar makeStatusBar�  �  ~ �� 0 bar   ���E�N���p���
� misccura� 0 nsstatusbar NSStatusBar� "0 systemstatusbar systemStatusBar� .0 statusitemwithlength_ statusItemWithLength_� 0 
updateicon 
updateIcon� 0 nsmenu NSMenu� 	0 alloc  �  0 initwithtitle_ initWithTitle_� 0 setdelegate_ setDelegate_� 0 setmenu_ setMenu_� I��,�,E�O��k+ Ec  O)�k+ O��,j+ �k+ 
Ec  Ob  )k+ Ob  b  k+  ���
�	���� 0 runonce runOnce�
  �	  �  � �����
� .sysodelanull��� ��� nmbr� 0 makestatusbar makeStatusBar� 0 
updateicon 
updateIcon� 0 	plistinit 	plistInit� !kj  O)j+ O)�k+ Okj  O*j+ OP ������� � "0 checkforupdates checkForUpdates�  �  � ���� 	0 reply  � �����������������	����������1��������W��  0 testconnection testConnection�� "0 getlatestupdate getLatestUpdate��  ���� 0 setfocus setFocus
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit
�� .sysodelanull��� ��� nmbr�� 0 
updateicon 
updateIcon�� 0 	plistinit 	plistInit��  0 downloadupdate downloadUpdate�� 0 killapp killApp�  �*j+  e  �*j+ e  U�n)j+ O��b  �����lv�l� �,E�O��  kj O)a k+ Okj O*j+ Y *j+ O)j+ oY **j+ f  kj O)a k+ Okj O*j+ Y hY kj O*j+  ��q���������� 0 asktoupdate askToUpdate��  ��  �  �  �� h �����������
�� .miscidlenmbr    ��� null��  ��  � ���� 0 m  � ����������������
�� .misccurdldt    ��� null
�� 
min �� �� -
�� 
bool�� 0 intervaltime intervalTime�� 0 	plistinit 	plistInit�� �� �b  e  �b  b   xb  f  jb  f  J*j  �,E�O�� 
 �� �& ��  
*j+ Y hOPY �j 
 �� �& 
*j+ Y hOPY b  e  
*j+ Y hY hY hY hO� �����������
�� .aevtoappnull  �   � ****� k     �� �� �����  ��  ��  �  � ������������
�� 
alis
�� 
rtyp
�� 
ctxt
�� .earsffdralis        afdr�� 0 appicon appIcon�� 0 runonce runOnce�� *�)��l �%/E�O*j+  ascr  ��ޭ