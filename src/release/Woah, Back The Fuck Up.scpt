FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �  �   T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��  ��    � � This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.     �  �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .      l     ��  ��    � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.     �     Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > .      l     ��������  ��  ��        l     ��   ��    ^ X Donkey icon/logo taken from http://thedeciderist.com/2013/01/29/the-party-of-less-evil/      � ! ! �   D o n k e y   i c o n / l o g o   t a k e n   f r o m   h t t p : / / t h e d e c i d e r i s t . c o m / 2 0 1 3 / 0 1 / 2 9 / t h e - p a r t y - o f - l e s s - e v i l /   " # " l     ��������  ��  ��   #  $ % $ j     �� &��  0 currentversion currentVersion & m      ' ' ?�       %  ( ) ( l     ��������  ��  ��   )  * + * x    
�� , -��   , 1      ��
�� 
ascr - �� .��
�� 
minv . m       / / � 0 0  2 . 4��   +  1 2 1 x   
 �� 3����   3 2   ��
�� 
osax��   2  4 5 4 x    #�� 6����   6 4    �� 7
�� 
frmk 7 m     8 8 � 9 9  F o u n d a t i o n��   5  : ; : x   # 0�� <����   < 4   % )�� =
�� 
frmk = m   ' ( > > � ? ?  A p p K i t��   ;  @ A @ l     ��������  ��  ��   A  B C B j   0 2�� D�� 0 
statusitem 
StatusItem D m   0 1��
�� 
msng C  E F E j   3 5�� G�� 0 
thedisplay 
theDisplay G m   3 4 H H � I I   F  J K J j   6 <�� L�� 0 defaults   L 4   6 ;�� M
�� 
pcls M m   8 9 N N � O O  N S U s e r D e f a u l t s K  P Q P j   = E�� R�� $0 internalmenuitem internalMenuItem R 4   = D�� S
�� 
pcls S m   ? B T T � U U  N S M e n u I t e m Q  V W V j   F N�� X�� $0 externalmenuitem externalMenuItem X 4   F M�� Y
�� 
pcls Y m   H K Z Z � [ [  N S M e n u I t e m W  \ ] \ j   O W�� ^�� 0 newmenu newMenu ^ 4   O V�� _
�� 
pcls _ m   Q T ` ` � a a  N S M e n u ]  b c b j   X l�� d�� 0 thelist theList d J   X k e e  f g f m   X [ h h � i i  B a c k u p   n o w g  j k j m   [ ^ l l � m m " B a c k u p   n o w   &   q u i t k  n o n m   ^ a p p � q q " T u r n   o n   t h e   b a n t s o  r s r m   a d t t � u u , T u r n   o f f   n o t i f i c a t i o n s s  v�� v m   d g w w � x x  Q u i t��   c  y z y j   m o�� {�� 0 isnsfw isNSFW { m   m n��
�� boovfals z  | } | j   p r�� ~�� 0 tonotify toNotify ~ m   p q��
�� boovtrue }   �  j   s u�� ���  0 backupthenquit backupThenQuit � m   s t��
�� boovfals �  � � � j   v x�� ��� 0 	idleready 	idleReady � m   v w��
�� boovfals �  � � � j   y {�� ��� 0 isconnected isConnected � m   y z��
�� boovtrue �  � � � j   | ��� ��� 0 
updatefile 
updateFile � m   |  � � � � �   �  � � � j   � ��� ��� 0 
newversion 
newVersion � m   � �����   �  � � � l     ��������  ��  ��   �  � � � p   � � � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   � � � � �� ��� 0 intervaltime intervalTime � �� ��� 0 	starttime 	startTime � ������ 0 endtime endTime��   �  � � � p   � � � � ������ 0 appicon appIcon��   �  � � � l     ����� � r      � � � 4     �� �
�� 
alis � l    ����� � b     � � � l   	 ����� � I   	�� � �
�� .earsffdralis        afdr �  f     � �� ���
�� 
rtyp � m    ��
�� 
ctxt��  ��  ��   � m   	 
 � � � � � < C o n t e n t s : R e s o u r c e s : a p p l e t . i c n s��  ��   � o      ���� 0 appicon appIcon��  ��   �  � � � l     ��������  ��  ��   �  � � � j   � ��� ��� 	0 plist   � b   � � � � � n  � � � � � 1   � ���
�� 
psxp � l  � � ����� � I  � ��� � �
�� .earsffdralis        afdr � m   � ���
�� afdrdlib � �� ���
�� 
from � m   � ���
�� fldmfldu��  ��  ��   � m   � � � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � j   � ��� ��� 0 initialbackup initialBackup � m   � ���
�� boovtrue �  � � � j   � ��� ��� 0 manualbackup manualBackup � m   � ���
�� boovfals �  � � � j   � ��� ��� 0 isbackingup isBackingUp � m   � ���
�� boovfals �  � � � j   � ��� ��� "0 messagesmissing messagesMissing � J   � � � �  � � � m   � � � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m   � � � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �  � � � m   � � � � � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d ! �  � � � m   � � � � � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �  ��� � m   � � � � � � � � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .��   �  � � � j   � ��� ��� *0 messagesencouraging messagesEncouraging � J   � � � �  � � � m   � � � � � � �  C o m e   o n ! �  � � � m   � � � � � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r ! �  � � � m   � � � � � � � " N o   p a i n !   N o   p a i n ! �  � � � m   � � � � � � � 0 L e t ' s   d o   t h i s   a s   a   t e a m ! �  � � � m   � � � � � � �  W e   c a n   d o   i t ! �  � � � m   � � � � � � �  F r e e e e e e e e e d o m ! �  � � � m   � �   � 2 A l t o g e t h e r   o r   n o t   a t   a l l ! �  m   � � � H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !  m   � � �		 H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e ! 
��
 m   � � �  Y o u   s e x y   f u c k !��   �  j   ����� $0 messagescomplete messagesComplete J   �  m   � � �  N i c e   o n e !  m   � � � * Y o u   a b s o l u t e   l e g   e n d !  m   � � �  G o o d   l a d !  m   � � �    P e r f i c k ! !"! m   � �## �$$  H a p p y   d a y s !" %&% m   � �'' �((  F r e e e e e e e e e d o m !& )*) m   � �++ �,, H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !* -.- m   �// �00 B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !. 121 m  33 �44 d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !2 5��5 m  66 �77  Y o u   s e x y   f u c k !��   898 j  1��:�� &0 messagescancelled messagesCancelled: J  .;; <=< m  >> �??  T h a t ' s   a   s h a m e= @A@ m  BB �CC  A h   m a n ,   u n l u c k yA DED m  FF �GG  O h   w e l lE HIH m  JJ �KK @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e nI LML m  NN �OO , W e l l   t h a t ' s   a   l e t   d o w nM PQP m  !RR �SS P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?Q TUT m  !$VV �WW  A r r o g a n tU XYX m  $'ZZ �[[ > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0Y \��\ m  '*]] �^^  E l l   b e n d��  9 _`_ j  2T��a�� 40 messagesalreadybackingup messagesAlreadyBackingUpa J  2Qbb cdc m  25ee �ff  T h a t ' s   a   s h a m ed ghg m  58ii �jj . T h o u g h t   y o u   w e r e   c l e v e rh klk m  8;mm �nn > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a tl opo m  ;>qq �rr  D i c kp sts m  >Auu �vv  F u c k t a r dt wxw m  ADyy �zz J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n sx {|{ m  DG}} �~~  A r r o g a n t| � m  GJ�� ��� $ C h i l l   t h e   f u c k   o u t� ���� m  JM�� ���  H o l d   f i r e��  ` ��� j  U[����� 0 strawake strAwake� m  UX�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  \b����� 0 strsleep strSleep� m  \_�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  cm����� &0 displaysleepstate displaySleepState� I cj�����
�� .sysoexecTEXT���     TEXT� m  cf�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t��  � ��� j  nt����� 0 	temptitle 	tempTitle� m  nq�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� ��� l     ��������  ��  ��  � ��� l     ��~�}�  �~  �}  � ��� l     �|���|  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� i  ux��� I      �{�z�y�{ 0 	plistinit 	plistInit�z  �y  � k    ��� ��� q      �� �x��x 0 
foldername 
folderName� �w��w 0 
backupdisk 
backupDisk� �v��v 0 
userfolder 
userFolder� �u�t�u 0 
backuptime 
backupTime�t  � ��� r     ��� m     �s�s  � o      �r�r 0 
backuptime 
backupTime� ��� l   �q�p�o�q  �p  �o  � ��� Z   ����n�� l   ��m�l� =    ��� I    �k��j�k 0 itexists itExists� ��� m    �� ���  f i l e� ��i� o    �h�h 	0 plist  �i  �j  � m    �g
�g boovtrue�m  �l  � k    g�� ��� O    a��� k    `�� ��� r    $��� n    "��� 1     "�f
�f 
pcnt� 4     �e�
�e 
plif� o    �d�d 	0 plist  � o      �c�c 0 thecontents theContents� ��� r   % *��� n   % (��� 1   & (�b
�b 
valL� o   % &�a�a 0 thecontents theContents� o      �`�` 0 thevalue theValue� ��� r   + 4��� n   + .��� o   , .�_�_ 0 
appversion 
AppVersion� o   + ,�^�^ 0 thevalue theValue� o      �]�]  0 currentversion currentVersion� ��� r   5 :��� n   5 8��� o   6 8�\�\  0 foldertobackup FolderToBackup� o   5 6�[�[ 0 thevalue theValue� o      �Z�Z 0 
foldername 
folderName� ��� r   ; @��� n   ; >��� o   < >�Y�Y 0 backupdrive BackupDrive� o   ; <�X�X 0 thevalue theValue� o      �W�W 0 
backupdisk 
backupDisk� ��� r   A F��� n   A D��� o   B D�V�V  0 computerfolder ComputerFolder� o   A B�U�U 0 thevalue theValue� o      �T�T 0 
userfolder 
userFolder� ��� r   G L��� n   G J��� o   H J�S�S 0 scheduledtime ScheduledTime� o   G H�R�R 0 thevalue theValue� o      �Q�Q 0 
backuptime 
backupTime� ��� r   M V��� n   M P��� o   N P�P�P 0 nsfw NSFW� o   M N�O�O 0 thevalue theValue� o      �N�N 0 isnsfw isNSFW� ��M� r   W `��� n   W Z��� o   X Z�L�L 0 notifications Notifications� o   W X�K�K 0 thevalue theValue� o      �J�J 0 tonotify toNotify�M  � m    ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �    l  b b�I�H�G�I  �H  �G   �F I   b g�E�D�C�E 0 	initmenus 	initMenus�D  �C  �F  �n  � k   j�  r   j q I   j o�B�A�@�B .0 getcomputeridentifier getComputerIdentifier�A  �@   o      �?�? 0 
userfolder 
userFolder 	 l  r r�>�=�<�>  �=  �<  	 

 O   r? t   v> k   z=  n  z  I   { �;�:�9�; 0 setfocus setFocus�:  �9    f   z {  l  � ��8�7�6�8  �7  �6    r   � � m   � � �L T h i s   p r o g r a m   a u t o m a t i c a l l y   b a c k s   u p   y o u r   H o m e   f o l d e r   b u t   e x c l u d e s   t h e   C a c h e s ,   L o g s   a n d   M o b i l e   D o c u m e n t s   f o l d e r s   f r o m   y o u r   L i b r a r y . 
 
 A l l   y o u   n e e d   t o   d o   i s   s e l e c t   t h e   e x t e r n a l   d r i v e   t o   b a c k u p   t o   a t   t h e   n e x t   p r o m p t . 
 
 Y o u r   b a c k u p s   w i l l   b e   s a v e d   i n : 
 [ E x t e r n a l   H a r d   D r i v e ]   >   B a c k u p s   >   [ C o m p u t e r   N a m e ] o      �5�5 0 welcome    l  � ��4�3�2�4  �3  �2     r   � �!"! l  � �#�1�0# n   � �$%$ 1   � ��/
�/ 
bhit% l  � �&�.�-& I  � ��,'(
�, .sysodlogaskr        TEXT' o   � ��+�+ 0 welcome  ( �*)*
�* 
appr) o   � ��)�) 0 	temptitle 	tempTitle* �(+,
�( 
disp+ o   � ��'�' 0 appicon appIcon, �&-.
�& 
btns- J   � �// 0�%0 m   � �11 �22  O K ,   g o t   i t�%  . �$3�#
�$ 
dflt3 m   � ��"�" �#  �.  �-  �1  �0  " o      �!�! 	0 reply    454 r   � �676 I  � �� 8�
�  .earsffdralis        afdr8 l  � �9��9 m   � ��
� afdrcusr�  �  �  7 o      �� 0 
foldername 
folderName5 :;: l  � �����  �  �  ; <=< Q   � �>?@> r   � �ABA c   � �CDC l  � �E��E I  � ���F
� .sysostflalis    ��� null�  F �G�
� 
prmpG m   � �HH �II R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o�  �  �  D m   � ��
� 
TEXTB o      �� 0 
backupdisk 
backupDisk? R      ���
� .ascrerr ****      � ****�  �  @ n  � �JKJ I   � ����
� 0 killapp killApp�  �
  K  f   � �= LML l  � ��	���	  �  �  M NON r   � �PQP n   � �RSR 1   � ��
� 
psxpS o   � ��� 0 
foldername 
folderNameQ o      �� 0 
foldername 
folderNameO TUT r   � �VWV n   � �XYX 1   � ��
� 
psxpY o   � ��� 0 
backupdisk 
backupDiskW o      �� 0 
backupdisk 
backupDiskU Z[Z r   � �\]\ J   � �^^ _`_ m   � �aa �bb 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r` cdc m   � �ee �ff 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u rd g� g m   � �hh �ii , E v e r y   h o u r   o n   t h e   h o u r�   ] o      ���� 0 backuptimes backupTimes[ jkj r   �lml c   �	non J   �pp q��q I  ���rs
�� .gtqpchltns    @   @ ns  r o   � ����� 0 backuptimes backupTimess ��t��
�� 
prmpt m   � �uu �vv 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  o m  ��
�� 
TEXTm o      ���� 0 selectedtime selectedTimek wxw l ��������  ��  ��  x y��y Z  =z{|��z l }����} =  ~~ o  ���� 0 selectedtime selectedTime m  �� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  { r  ��� m  ���� � o      ���� 0 
backuptime 
backupTime| ��� l !������ =  !��� o  ���� 0 selectedtime selectedTime� m   �� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r  $)��� m  $'���� � o      ���� 0 
backuptime 
backupTime� ��� l ,1������ =  ,1��� o  ,-���� 0 selectedtime selectedTime� m  -0�� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r  49��� m  47���� <� o      ���� 0 
backuptime 
backupTime��  ��  ��   m   v y����  �� m   r s���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��   ��� l @@��������  ��  ��  � ���� O  @���� O  D���� k  _��� ��� I _������
�� .corecrel****      � null��  � ����
�� 
kocl� m  cf��
�� 
plii� ����
�� 
insh�  ;  ik� �����
�� 
prdt� K  n��� ����
�� 
kind� m  qt��
�� 
TEXT� ����
�� 
pnam� m  wz�� ���  A p p V e r s i o n� �����
�� 
valL� o  {�����  0 currentversion currentVersion��  ��  � ��� I �������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ����
�� 
plii� ����
�� 
insh�  ;  ��� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ������ 0 
foldername 
folderName��  ��  � ��� I �������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ����
�� 
plii� ����
�� 
insh�  ;  ��� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  B a c k u p D r i v e� �����
�� 
valL� o  ������ 0 
backupdisk 
backupDisk��  ��  � ��� I ������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ����
�� 
plii� ����
�� 
insh�  ;  ��� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  ������ 0 
userfolder 
userFolder��  ��  � ��� I *�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  
��
�� 
plii� ����
�� 
insh�  ;  � �����
�� 
prdt� K  $�� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  �� ���  S c h e d u l e d T i m e� �����
�� 
valL� o   ���� 0 
backuptime 
backupTime��  ��  � ��� I +V�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  /2��
�� 
plii� ����
�� 
insh�  ;  57� �����
�� 
prdt� K  :P�� ����
�� 
kind� m  =@��
�� 
TEXT� ����
�� 
pnam� m  CF�� �    N S F W� ����
�� 
valL o  GL���� 0 isnsfw isNSFW��  ��  � �� I W�����
�� .corecrel****      � null��   ��
�� 
kocl m  [^��
�� 
plii ��
�� 
insh  ;  ac ����
�� 
prdt K  f|		 ��

�� 
kind
 m  il��
�� 
TEXT ��
�� 
pnam m  or �  N o t i f i c a t i o n s ����
�� 
valL o  sx���� 0 tonotify toNotify��  ��  ��  � l D\���� I D\��~
� .corecrel****      � null�~   �}
�} 
kocl m  HI�|
�| 
plif �{�z
�{ 
prdt K  LV �y�x
�y 
pnam o  OT�w�w 	0 plist  �x  �z  ��  ��  � m  @A�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  �  l ���v�u�t�v  �u  �t    r  �� o  ���s�s 0 
foldername 
folderName o      �r�r 0 sourcefolder sourceFolder   r  ��!"! o  ���q�q 0 
backupdisk 
backupDisk" o      �p�p "0 destinationdisk destinationDisk  #$# r  ��%&% o  ���o�o 0 
userfolder 
userFolder& o      �n�n 0 machinefolder machineFolder$ '(' r  ��)*) o  ���m�m 0 
backuptime 
backupTime* o      �l�l 0 intervaltime intervalTime( +,+ l ���k�j�i�k  �j  �i  , -�h- I  ���g�f�e�g 0 diskinit diskInit�f  �e  �h  � ./. l     �d�c�b�d  �c  �b  / 010 i  y|232 I      �a�`�_�a 0 diskinit diskInit�`  �_  3 Z     �45�^64 l    	7�]�\7 =     	898 I     �[:�Z�[ 0 itexists itExists: ;<; m    == �>>  d i s k< ?�Y? o    �X�X "0 destinationdisk destinationDisk�Y  �Z  9 m    �W
�W boovtrue�]  �\  5 I    �V@�U�V 0 freespaceinit freeSpaceInit@ A�TA m    �S
�S boovfals�T  �U  �^  6 t    �BCB k    �DD EFE n   GHG I    �R�Q�P�R 0 setfocus setFocus�Q  �P  H  f    F IJI r    &KLK n    $MNM 3   " $�O
�O 
cobjN o    "�N�N "0 messagesmissing messagesMissingL o      �M�M 0 msg  J OPO r   ' AQRQ l  ' ?S�L�KS n   ' ?TUT 1   ; ?�J
�J 
bhitU l  ' ;V�I�HV I  ' ;�GWX
�G .sysodlogaskr        TEXTW o   ' (�F�F 0 msg  X �EYZ
�E 
apprY o   ) .�D�D 0 	temptitle 	tempTitleZ �C[\
�C 
disp[ o   / 0�B�B 0 appicon appIcon\ �A]^
�A 
btns] J   1 5__ `a` m   1 2bb �cc  C a n c e l   B a c k u pa d�@d m   2 3ee �ff  O K�@  ^ �?g�>
�? 
dfltg m   6 7�=�= �>  �I  �H  �L  �K  R o      �<�< 	0 reply  P h�;h Z   B �ij�:ki l  B Gl�9�8l =   B Gmnm o   B C�7�7 	0 reply  n m   C Foo �pp  O K�9  �8  j I   J O�6�5�4�6 0 diskinit diskInit�5  �4  �:  k Z   R �qr�3�2q l  R ]s�1�0s E   R ]tut o   R W�/�/ &0 displaysleepstate displaySleepStateu o   W \�.�. 0 strawake strAwake�1  �0  r k   ` �vv wxw r   ` iyzy n   ` g{|{ 3   e g�-
�- 
cobj| o   ` e�,�, &0 messagescancelled messagesCancelledz o      �+�+ 0 msg  x }�*} Z   j �~�)�(~ l  j q��'�&� =   j q��� o   j o�%�% 0 tonotify toNotify� m   o p�$
�$ boovtrue�'  �&   Z   t �����#� l  t {��"�!� =   t {��� o   t y� �  0 isnsfw isNSFW� m   y z�
� boovtrue�"  �!  � I  ~ ����
� .sysonotfnull��� ��� TEXT� o   ~ �� 0 msg  � ���
� 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  � ��� l  � ����� =   � ���� o   � ��� 0 isnsfw isNSFW� m   � ��
� boovfals�  �  � ��� I  � ����
� .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� ���
� 
appr� m   � ��� ��� 
 W B T F U�  �  �#  �)  �(  �*  �3  �2  �;  C m    ��  ��1 ��� l     ����  �  �  � ��� i  }���� I      ���� 0 freespaceinit freeSpaceInit� ��� o      �� 	0 again  �  �  � Z     ����
�� l    	��	�� =     	��� I     ���� 0 comparesizes compareSizes� ��� o    �� 0 sourcefolder sourceFolder� ��� o    �� "0 destinationdisk destinationDisk�  �  � m    �
� boovtrue�	  �  � I    �� ��� 0 
backupinit 
backupInit�   ��  �
  � t    ���� k    ��� ��� n   ��� I    �������� 0 setfocus setFocus��  ��  �  f    � ��� l   ��������  ��  ��  � ��� Z    i������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r   " <��� l  " :������ n   " :��� 1   6 :��
�� 
bhit� l  " 6������ I  " 6����
�� .sysodlogaskr        TEXT� m   " #�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
appr� o   $ )���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   * +���� 0 appicon appIcon� ����
�� 
btns� J   , 0�� ��� m   , -�� ���  Y e s� ���� m   - .�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   1 2���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ��� l  ? B������ =   ? B��� o   ? @���� 	0 again  � m   @ A��
�� boovtrue��  ��  � ���� r   E e��� l  E c������ n   E c��� 1   _ c��
�� 
bhit� l  E _������ I  E _����
�� .sysodlogaskr        TEXT� m   E H�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
appr� o   I N���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   O P���� 0 appicon appIcon� ����
�� 
btns� J   Q Y�� ��� m   Q T�� ���  Y e s� ���� m   T W�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   Z [���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  ��  ��  � ��� l  j j��������  ��  ��  � ���� Z   j ������� l  j o������ =   j o��� o   j k���� 	0 reply  � m   k n�� �    Y e s��  ��  � I   r w�������� 0 tidyup tidyUp��  ��  ��  � k   z �  Z  z ����� l  z ����� E   z � o   z ���� &0 displaysleepstate displaySleepState o    ����� 0 strawake strAwake��  ��   r   � �	
	 n   � � 3   � ���
�� 
cobj o   � ����� &0 messagescancelled messagesCancelled
 o      ���� 0 msg  ��  ��    l  � ���������  ��  ��   �� Z   � ����� l  � ����� =   � � o   � ����� 0 tonotify toNotify m   � ���
�� boovtrue��  ��   Z   � ��� l  � ����� =   � � o   � ����� 0 isnsfw isNSFW m   � ���
�� boovtrue��  ��   I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � � � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��    !  l  � �"����" =   � �#$# o   � ����� 0 isnsfw isNSFW$ m   � ���
�� boovfals��  ��  ! %��% I  � ���&'
�� .sysonotfnull��� ��� TEXT& m   � �(( �)) , Y o u   s t o p p e d   b a c k i n g   u p' ��*��
�� 
appr* m   � �++ �,, 
 W B T F U��  ��  ��  ��  ��  ��  ��  � m    ����  ��� -.- l     ��������  ��  ��  . /0/ i  ��121 I      �������� 0 
backupinit 
backupInit��  ��  2 k     �33 454 r     676 4     ��8
�� 
psxf8 o    ���� "0 destinationdisk destinationDisk7 o      ���� 	0 drive  5 9:9 l   ��������  ��  ��  : ;<; Z   ,=>����= l   ?����? =    @A@ I    ��B���� 0 itexists itExistsB CDC m    	EE �FF  f o l d e rD G��G b   	 HIH o   	 
���� "0 destinationdisk destinationDiskI m   
 JJ �KK  B a c k u p s��  ��  A m    ��
�� boovfals��  ��  > O   (LML I   '����N
�� .corecrel****      � null��  N ��OP
�� 
koclO m    ��
�� 
cfolP ��QR
�� 
inshQ o    ���� 	0 drive  R ��S��
�� 
prdtS K    #TT ��U��
�� 
pnamU m     !VV �WW  B a c k u p s��  ��  M m    XX�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ��  < YZY Z  - d[\��~[ l  - >]�}�|] =   - >^_^ I   - <�{`�z�{ 0 itexists itExists` aba m   . /cc �dd  f o l d e rb e�ye b   / 8fgf b   / 4hih o   / 0�x�x "0 destinationdisk destinationDiski m   0 3jj �kk  B a c k u p s /g o   4 7�w�w 0 machinefolder machineFolder�y  �z  _ m   < =�v
�v boovfals�}  �|  \ O  A `lml I  E _�u�tn
�u .corecrel****      � null�t  n �sop
�s 
koclo m   G H�r
�r 
cfolp �qqr
�q 
inshq n   I Tsts 4   O T�pu
�p 
cfolu m   P Svv �ww  B a c k u p st 4   I O�ox
�o 
cdisx o   M N�n�n 	0 drive  r �my�l
�m 
prdty K   U [zz �k{�j
�k 
pnam{ o   V Y�i�i 0 machinefolder machineFolder�j  �l  m m   A B||�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  �~  Z }~} O  e ~� r   i }��� n   i y��� 4   t y�h�
�h 
cfol� o   u x�g�g 0 machinefolder machineFolder� n   i t��� 4   o t�f�
�f 
cfol� m   p s�� ���  B a c k u p s� 4   i o�e�
�e 
cdis� o   m n�d�d 	0 drive  � o      �c�c 0 backupfolder backupFolder� m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ~ ��� l   �b�a�`�b  �a  �`  � ��� Z    ����_�� l   ���^�]� =    ���� I    ��\��[�\ 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��Z� b   � ���� b   � ���� b   � ���� o   � ��Y�Y "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��X�X 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�Z  �[  � m   � ��W
�W boovfals�^  �]  � O  � ���� I  � ��V�U�
�V .corecrel****      � null�U  � �T��
�T 
kocl� m   � ��S
�S 
cfol� �R��
�R 
insh� o   � ��Q�Q 0 backupfolder backupFolder� �P��O
�P 
prdt� K   � ��� �N��M
�N 
pnam� m   � ��� ���  L a t e s t�M  �O  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �_  � r   � ���� m   � ��L
�L boovfals� o      �K�K 0 initialbackup initialBackup� ��� l  � ��J�I�H�J  �I  �H  � ��� O  � ���� r   � ���� n   � ���� 4   � ��G�
�G 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ��F�
�F 
cfol� o   � ��E�E 0 machinefolder machineFolder� n   � ���� 4   � ��D�
�D 
cfol� m   � ��� ���  B a c k u p s� 4   � ��C�
�C 
cdis� o   � ��B�B 	0 drive  � o      �A�A 0 latestfolder latestFolder� m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��@�?�>�@  �?  �>  � ��=� I   � ��<�;�:�< 
0 backup  �;  �:  �=  0 ��� l     �9�8�7�9  �8  �7  � ��� i  ����� I      �6�5�4�6 0 tidyup tidyUp�5  �4  � k     ��� ��� r     ��� 4     �3�
�3 
psxf� o    �2�2 "0 destinationdisk destinationDisk� o      �1�1 	0 drive  � ��� l   �0�/�.�0  �/  �.  � ��-� O    ���� k    ��� ��� r    ��� n    ��� 4    �,�
�, 
cfol� o    �+�+ 0 machinefolder machineFolder� n    ��� 4    �*�
�* 
cfol� m    �� ���  B a c k u p s� 4    �)�
�) 
cdis� o    �(�( 	0 drive  � o      �'�' 0 bf bF� ��� r    ��� n    ��� 1    �&
�& 
ascd� n    ��� 2   �%
�% 
cobj� o    �$�$ 0 bf bF� o      �#�# 0 creationdates creationDates� ��� r     &��� n     $��� 4   ! $�"�
�" 
cobj� m   " #�!�! � o     !� �  0 creationdates creationDates� o      �� 0 theoldestdate theOldestDate� ��� r   ' *   m   ' (��  o      �� 0 j  �  l  + +����  �  �    Y   + e�� k   9 `		 

 r   9 ? n   9 = 4   : =�
� 
cobj o   ; <�� 0 i   o   9 :�� 0 creationdates creationDates o      �� 0 thisdate thisDate � Z   @ `�� l  @ H�� >  @ H n   @ F 1   D F�
� 
pnam 4   @ D�
� 
cobj o   B C�� 0 i   m   F G �  L a t e s t�  �   Z   K \��
 l  K N�	� A   K N  o   K L�� 0 thisdate thisDate  o   L M�� 0 theoldestdate theOldestDate�	  �   k   Q X!! "#" r   Q T$%$ o   Q R�� 0 thisdate thisDate% o      �� 0 theoldestdate theOldestDate# &�& r   U X'(' o   U V�� 0 i  ( o      �� 0 j  �  �  �
  �  �  �  � 0 i   m   . /� �   I  / 4��)��
�� .corecnte****       ****) o   / 0���� 0 creationdates creationDates��  �   *+* l  f f��������  ��  ��  + ,-, r   f p./. c   f n010 l  f l2����2 n   f l343 4   g l��5
�� 
cobj5 l  h k6����6 [   h k787 o   h i���� 0 j  8 m   i j���� ��  ��  4 o   f g���� 0 bf bF��  ��  1 m   l m��
�� 
TEXT/ o      ����  0 foldertodelete folderToDelete- 9:9 r   q x;<; c   q v=>= n   q t?@? 1   r t��
�� 
psxp@ o   q r����  0 foldertodelete folderToDelete> m   t u��
�� 
TEXT< o      ����  0 foldertodelete folderToDelete: ABA r   y �CDC b   y �EFE b   y |GHG m   y zII �JJ  r m   - r f   'H o   z {����  0 foldertodelete folderToDeleteF m   | KK �LL  'D o      ���� 0 todelete toDeleteB MNM I  � ���O��
�� .sysoexecTEXT���     TEXTO o   � ����� 0 todelete toDelete��  N PQP l  � ���������  ��  ��  Q RSR Z   � �TU����T l  � �V����V E   � �WXW o   � ����� &0 displaysleepstate displaySleepStateX o   � ����� 0 strawake strAwake��  ��  U Z   � �YZ����Y l  � �[����[ =   � �\]\ o   � ����� 0 tonotify toNotify] m   � ���
�� boovtrue��  ��  Z Z   � �^_`��^ l  � �a����a =   � �bcb o   � ����� 0 isnsfw isNSFWc m   � ���
�� boovtrue��  ��  _ k   � �dd efe I  � ���gh
�� .sysonotfnull��� ��� TEXTg m   � �ii �jj � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .h ��k��
�� 
apprk m   � �ll �mm 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  f n��n I  � ���o��
�� .sysodelanull��� ��� nmbro m   � ����� ��  ��  ` pqp l  � �r����r =   � �sts o   � ����� 0 isnsfw isNSFWt m   � ���
�� boovfals��  ��  q u��u k   � �vv wxw I  � ���yz
�� .sysonotfnull��� ��� TEXTy m   � �{{ �|| t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i nz ��}��
�� 
appr} m   � �~~ � 
 W B T F U��  x ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  ��  ��  ��  ��  ��  ��  S ��� l  � ���������  ��  ��  � ���� I   � �������� 0 freespaceinit freeSpaceInit� ���� m   � ���
�� boovtrue��  ��  ��  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �-  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� 
0 backup  ��  ��  � k    t�� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovtrue��  ��  � o      ���� 0 t  � ��� r   	 ��� m   	 
��
�� boovtrue� o      ���� 0 isbackingup isBackingUp� ��� l   ��������  ��  ��  � ��� n   ��� I    ������� 0 
updateicon 
updateIcon� ���� m    �� ���  a c t i v e��  ��  �  f    � ��� I   �����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ��� l   ��������  ��  ��  � ���� O   t��� k   "s�� ��� r   " *��� c   " (��� 4   " &���
�� 
psxf� o   $ %���� 0 sourcefolder sourceFolder� m   & '��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   + 3��� n   + 1��� 1   / 1��
�� 
pnam� 4   + /���
�� 
cfol� o   - .���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  4 4��������  ��  ��  � ��� Z   4 �������� l  4 ;������ =   4 ;��� o   4 9���� 0 initialbackup initialBackup� m   9 :��
�� boovfals��  ��  � k   > ��� ��� I  > L�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   @ A��
�� 
cfol� ����
�� 
insh� o   B C���� 0 backupfolder backupFolder� �����
�� 
prdt� K   D H�� ����
�� 
pnam� o   E F�~�~ 0 t  �  ��  � ��� l  M M�}�|�{�}  �|  �{  � ��� r   M W��� c   M S��� 4   M Q�z�
�z 
psxf� o   O P�y�y 0 sourcefolder sourceFolder� m   Q R�x
�x 
TEXT� o      �w�w (0 activesourcefolder activeSourceFolder� ��� r   X b��� 4   X ^�v�
�v 
cfol� o   Z ]�u�u (0 activesourcefolder activeSourceFolder� o      �t�t (0 activesourcefolder activeSourceFolder� ��� r   c |��� n   c x��� 4   u x�s�
�s 
cfol� o   v w�r�r 0 t  � n   c u��� 4   p u�q�
�q 
cfol� o   q t�p�p 0 machinefolder machineFolder� n   c p��� 4   k p�o�
�o 
cfol� m   l o�� ���  B a c k u p s� 4   c k�n�
�n 
cdis� o   g j�m�m 	0 drive  � o      �l�l (0 activebackupfolder activeBackupFolder� ��k� I  } ��j�i�
�j .corecrel****      � null�i  � �h��
�h 
kocl� m    ��g
�g 
cfol� �f��
�f 
insh� o   � ��e�e (0 activebackupfolder activeBackupFolder� �d��c
�d 
prdt� K   � ��� �b��a
�b 
pnam� o   � ��`�` 0 
foldername 
folderName�a  �c  �k  ��  ��  � ��� l  � ��_�^�]�_  �^  �]  � ��\� Z   �s����[� l  � � �Z�Y  =   � � o   � ��X�X 0 initialbackup initialBackup m   � ��W
�W boovtrue�Z  �Y  � k   �   r   � � n   � �	 1   � ��V
�V 
time	 l  � �
�U�T
 I  � ��S�R�Q
�S .misccurdldt    ��� null�R  �Q  �U  �T   o      �P�P 0 	starttime 	startTime  Z   � ��O�N l  � ��M�L E   � � o   � ��K�K &0 displaysleepstate displaySleepState o   � ��J�J 0 strawake strAwake�M  �L   Z   � ��I�H l  � ��G�F =   � � o   � ��E�E 0 tonotify toNotify m   � ��D
�D boovtrue�G  �F   Z   � ��C l  � ��B�A =   � � o   � ��@�@ 0 isnsfw isNSFW m   � ��?
�? boovtrue�B  �A   I  � ��>
�> .sysonotfnull��� ��� TEXT m   � � �   � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . . �=!�<
�= 
appr! m   � �"" �## 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�<   $%$ l  � �&�;�:& =   � �'(' o   � ��9�9 0 isnsfw isNSFW( m   � ��8
�8 boovfals�;  �:  % )�7) I  � ��6*+
�6 .sysonotfnull��� ��� TEXT* m   � �,, �-- \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e+ �5.�4
�5 
appr. m   � �// �00 
 W B T F U�4  �7  �C  �I  �H  �O  �N   121 l   �3�2�1�3  �2  �1  2 343 r   565 c   787 l  9�0�/9 b   :;: b   <=< b   >?> b   @A@ b   BCB o   �.�. "0 destinationdisk destinationDiskC m  DD �EE  B a c k u p s /A o  
�-�- 0 machinefolder machineFolder? m  FF �GG  / L a t e s t /= o  �,�, 0 
foldername 
folderName; m  HH �II  /�0  �/  8 m  �+
�+ 
TEXT6 o      �*�* 0 d  4 JKJ l �)�(�'�)  �(  �'  K LML Q  9NOPN I 0�&Q�%
�& .sysoexecTEXT���     TEXTQ b  ,RSR b  (TUT b  &VWV b  "XYX m   ZZ �[[	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   'Y o   !�$�$ 0 sourcefolder sourceFolderW m  "%\\ �]]  '   'U o  &'�#�# 0 d  S m  (+^^ �__  / '�%  O R      �"�!� 
�" .ascrerr ****      � ****�!  �   P l 88����  �  �  M `a` l ::����  �  �  a bcb r  :Aded m  :;�
� boovfalse o      �� 0 isbackingup isBackingUpc fgf r  BIhih m  BC�
� boovfalsi o      �� 0 manualbackup manualBackupg jkj r  JQlml m  JK�
� boovtruem o      �� 0 	idleready 	idleReadyk non l RR����  �  �  o pqp Z  R�rs��r l R]t��t E  R]uvu o  RW�� &0 displaysleepstate displaySleepStatev o  W\�� 0 strawake strAwake�  �  s k  `�ww xyx r  `mz{z n  `i|}| 1  ei�

�
 
time} l `e~�	�~ I `e���
� .misccurdldt    ��� null�  �  �	  �  { o      �� 0 endtime endTimey � r  nu��� n ns��� I  os���� 0 getduration getDuration�  �  �  f  no� o      � �  0 duration  � ��� r  v���� n  v��� 3  {��
�� 
cobj� o  v{���� $0 messagescomplete messagesComplete� o      ���� 0 msg  � ��� l ����������  ��  ��  � ��� Z  ��������� l �������� =  ����� o  ������ 0 tonotify toNotify� m  ����
�� boovtrue��  ��  � Z  �������� l �������� =  ����� o  ������ 0 isnsfw isNSFW� m  ����
�� boovtrue��  ��  � k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  � ��� l �������� =  ����� o  ������ 0 isnsfw isNSFW� m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
� �����
�� 
appr� m  ���� ���  B a c k e d   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  � ��� l ����������  ��  ��  � ���� n ����� I  ��������� 0 
updateicon 
updateIcon� ���� m  ���� ���  i d l e��  ��  �  f  ����  �  �  q ��� l ����������  ��  ��  � ���� Z � ������� l �������� =  ����� o  ������  0 backupthenquit backupThenQuit� m  ����
�� boovtrue��  ��  � n ����� I  ���������� 0 killapp killApp��  ��  �  f  ����  ��  ��  � ��� l 
������ =  
��� o  ���� 0 initialbackup initialBackup� m  	��
�� boovfals��  ��  � ���� k  o�� ��� r  ,��� c  *��� l (������ b  (��� b  $��� b  "��� b  ��� b  ��� b  ��� b  ��� o  ���� "0 destinationdisk destinationDisk� m  �� ���  B a c k u p s /� o  ���� 0 machinefolder machineFolder� m  �� ���  /� o  ���� 0 t  � m  !�� ���  /� o  "#���� 0 
foldername 
folderName� m  $'�� ���  /��  ��  � m  ()��
�� 
TEXT� o      ���� 0 c  � ��� r  -F� � c  -D l -B���� b  -B b  -> b  -<	 b  -8

 b  -4 o  -0���� "0 destinationdisk destinationDisk m  03 �  B a c k u p s / o  47���� 0 machinefolder machineFolder	 m  8; �  / L a t e s t / o  <=���� 0 
foldername 
folderName m  >A �  /��  ��   m  BC��
�� 
TEXT  o      ���� 0 d  �  l GG��������  ��  ��    Z  G����� l GR���� E  GR o  GL���� &0 displaysleepstate displaySleepState o  LQ���� 0 strawake strAwake��  ��   k  U�  r  Ub !  n  U^"#" 1  Z^��
�� 
time# l UZ$����$ I UZ������
�� .misccurdldt    ��� null��  ��  ��  ��  ! o      ���� 0 	starttime 	startTime %&% r  cn'(' n  cl)*) 3  hl��
�� 
cobj* o  ch���� *0 messagesencouraging messagesEncouraging( o      ���� 0 msg  & +,+ l oo��������  ��  ��  , -��- Z  o�./����. l ov0����0 =  ov121 o  ot���� 0 tonotify toNotify2 m  tu��
�� boovtrue��  ��  / Z  y�345��3 l y�6����6 =  y�787 o  y~���� 0 isnsfw isNSFW8 m  ~��
�� boovtrue��  ��  4 k  ��99 :;: I ����<=
�� .sysonotfnull��� ��� TEXT< o  ������ 0 msg  = ��>��
�� 
appr> o  ������ 0 	temptitle 	tempTitle��  ; ?��? I ����@��
�� .sysodelanull��� ��� nmbr@ m  ������ ��  ��  5 ABA l ��C����C =  ��DED o  ������ 0 isnsfw isNSFWE m  ����
�� boovfals��  ��  B F��F k  ��GG HIH I ����JK
�� .sysonotfnull��� ��� TEXTJ m  ��LL �MM  B a c k i n g   u pK �N�~
� 
apprN o  ���}�} 0 	temptitle 	tempTitle�~  I O�|O I ���{P�z
�{ .sysodelanull��� ��� nmbrP m  ���y�y �z  �|  ��  ��  ��  ��  ��  ��  ��   QRQ l ���x�w�v�x  �w  �v  R STS Q  ��UVWU I ���uX�t
�u .sysoexecTEXT���     TEXTX b  ��YZY b  ��[\[ b  ��]^] b  ��_`_ b  ��aba b  ��cdc m  ��ee �ff
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = 'd o  ���s�s 0 c  b m  ��gg �hh  '   '` o  ���r�r 0 sourcefolder sourceFolder^ m  ��ii �jj  '   '\ o  ���q�q 0 d  Z m  ��kk �ll  / '�t  V R      �p�o�n
�p .ascrerr ****      � ****�o  �n  W l ���m�l�k�m  �l  �k  T mnm l ���j�i�h�j  �i  �h  n opo r  ��qrq m  ���g
�g boovfalsr o      �f�f 0 isbackingup isBackingUpp sts r  ��uvu m  ���e
�e boovfalsv o      �d�d 0 manualbackup manualBackupt wxw r  �yzy m  ���c
�c boovtruez o      �b�b 0 	idleready 	idleReadyx {|{ l �a�`�_�a  �`  �_  | }�^} Z  o~�]�~ = ��� n  ��� 2 �\
�\ 
cobj� l ��[�Z� c  ��� 4  �Y�
�Y 
psxf� o  �X�X 0 c  � m  
�W
�W 
alis�[  �Z  � J  �V�V   k  d�� ��� r  ��� c  ��� n  ��� 4  �U�
�U 
cfol� o  �T�T 0 t  � o  �S�S 0 backupfolder backupFolder� m  �R
�R 
TEXT� o      �Q�Q 0 oldestfolder oldestFolder� ��� r  '��� c  %��� n  #��� 1  #�P
�P 
psxp� o  �O�O 0 oldestfolder oldestFolder� m  #$�N
�N 
TEXT� o      �M�M 0 oldestfolder oldestFolder� ��� l ((�L�K�J�L  �K  �J  � ��� r  (3��� b  (1��� b  (-��� m  (+�� ���  r m   - r f   '� o  +,�I�I 0 oldestfolder oldestFolder� m  -0�� ���  '� o      �H�H 0 todelete toDelete� ��� I 49�G��F
�G .sysoexecTEXT���     TEXT� o  45�E�E 0 todelete toDelete�F  � ��� l ::�D�C�B�D  �C  �B  � ��A� Z  :d���@�?� l :E��>�=� E  :E��� o  :?�<�< &0 displaysleepstate displaySleepState� o  ?D�;�; 0 strawake strAwake�>  �=  � k  H`�� ��� r  HU��� n  HQ��� 1  MQ�:
�: 
time� l HM��9�8� I HM�7�6�5
�7 .misccurdldt    ��� null�6  �5  �9  �8  � o      �4�4 0 endtime endTime� ��� r  V]��� n V[��� I  W[�3�2�1�3 0 getduration getDuration�2  �1  �  f  VW� o      �0�0 0 duration  � ��� r  ^i��� n  ^g��� 3  cg�/
�/ 
cobj� o  ^c�.�. $0 messagescomplete messagesComplete� o      �-�- 0 msg  � ��� l jj�,�+�*�,  �+  �*  � ��� Z  jW���)�(� l jq��'�&� =  jq��� o  jo�%�% 0 tonotify toNotify� m  op�$
�$ boovtrue�'  �&  � k  tS�� ��� Z  t���#�� l ty��"�!� =  ty��� o  tu� �  0 duration  � m  ux�� ?�      �"  �!  � Z  |������ l |����� =  |���� o  |��� 0 isnsfw isNSFW� m  ���
� boovtrue�  �  � k  ���� ��� I �����
� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ���� 0 duration  � m  ���� ���    m i n u t e ! 
� o  ���� 0 msg  � ���
� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  � ��� I �����
� .sysodelanull��� ��� nmbr� m  ���� �  �  � ��� l ������ =  ����� o  ���� 0 isnsfw isNSFW� m  ���
� boovfals�  �  � ��� k  ���� ��� I �����
� .sysonotfnull��� ��� TEXT� b  ��   b  �� m  �� �  A n d   i n   o  ���� 0 duration   m  �� �    m i n u t e ! 
� �
�	
�
 
appr m  ��		 �

  B a c k e d   u p !�	  � � I ����
� .sysodelanull��� ��� nmbr m  ���� �  �  �  �  �#  � Z  �� l ���� =  �� o  ���� 0 isnsfw isNSFW m  ��� 
�  boovtrue�  �   k  ��  I ����
�� .sysonotfnull��� ��� TEXT b  �� b  �� b  �� m  �� �  A n d   i n   o  ������ 0 duration   m  ��   �!!    m i n u t e s ! 
 o  ������ 0 msg   ��"��
�� 
appr" m  ��## �$$ : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��   %��% I ����&��
�� .sysodelanull��� ��� nmbr& m  ������ ��  ��   '(' l ��)����) =  ��*+* o  ������ 0 isnsfw isNSFW+ m  ����
�� boovfals��  ��  ( ,��, k   -- ./. I  ��01
�� .sysonotfnull��� ��� TEXT0 b   	232 b   454 m   66 �77  A n d   i n  5 o  ���� 0 duration  3 m  88 �99    m i n u t e s ! 
1 ��:��
�� 
appr: m  ;; �<<  B a c k e d   u p !��  / =��= I ��>��
�� .sysodelanull��� ��� nmbr> m  ���� ��  ��  ��  �  � ?@? l ��������  ��  ��  @ A��A Z  SBCD��B l %E����E =  %FGF o  #���� 0 isnsfw isNSFWG m  #$��
�� boovtrue��  ��  C I (5��HI
�� .sysonotfnull��� ��� TEXTH m  (+JJ �KK � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .I ��L��
�� 
apprL m  .1MM �NN @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  D OPO l 8?Q����Q =  8?RSR o  8=���� 0 isnsfw isNSFWS m  =>��
�� boovfals��  ��  P T��T I BO��UV
�� .sysonotfnull��� ��� TEXTU m  BEWW �XX p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e dV ��Y��
�� 
apprY m  HKZZ �[[ 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  �)  �(  � \]\ l XX��������  ��  ��  ] ^��^ n X`_`_ I  Y`��a���� 0 
updateicon 
updateIcona b��b m  Y\cc �dd  i d l e��  ��  `  f  XY��  �@  �?  �A  �]  � k  goee fgf Z  gRhi����h l grj����j E  grklk o  gl���� &0 displaysleepstate displaySleepStatel o  lq���� 0 strawake strAwake��  ��  i k  uNmm non r  u�pqp n  u~rsr 1  z~��
�� 
times l uzt����t I uz������
�� .misccurdldt    ��� null��  ��  ��  ��  q o      ���� 0 endtime endTimeo uvu r  ��wxw n ��yzy I  ���������� 0 getduration getDuration��  ��  z  f  ��x o      ���� 0 duration  v {|{ r  ��}~} n  ��� 3  ����
�� 
cobj� o  ������ $0 messagescomplete messagesComplete~ o      ���� 0 msg  | ���� Z  �N������� l �������� =  ����� o  ������ 0 tonotify toNotify� m  ����
�� boovtrue��  ��  � Z  �J������ l �������� =  ����� o  ������ 0 duration  � m  ���� ?�      ��  ��  � Z  �������� l �������� =  ����� o  ������ 0 isnsfw isNSFW� m  ����
�� boovtrue��  ��  � k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e ! 
� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  � ��� l �������� =  ����� o  ������ 0 isnsfw isNSFW� m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e ! 
� �����
�� 
appr� m  ���� ���  B a c k e d   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  � Z  �J������ l ������� =  ���� o  � ���� 0 isnsfw isNSFW� m   ��
�� boovtrue��  ��  � k   �� ��� I ����
�� .sysonotfnull��� ��� TEXT� b  ��� b  ��� b  
��� m  �� ���  A n d   i n  � o  	���� 0 duration  � m  
�� ���    m i n u t e s ! 
� o  ���� 0 msg  � �����
�� 
appr� m  �� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I  �����
�� .sysodelanull��� ��� nmbr� m  ���� ��  ��  � ��� l #*������ =  #*��� o  #(���� 0 isnsfw isNSFW� m  ()��
�� boovfals��  ��  � ���� k  -F�� ��� I -@����
�� .sysonotfnull��� ��� TEXT� b  -6��� b  -2��� m  -0�� ���  A n d   i n  � o  01���� 0 duration  � m  25�� ���    m i n u t e s ! 
� ���~
� 
appr� m  9<�� ���  B a c k e d   u p !�~  � ��}� I AF�|��{
�| .sysodelanull��� ��� nmbr� m  AB�z�z �{  �}  ��  ��  ��  ��  ��  ��  ��  g ��� l SS�y�x�w�y  �x  �w  � ��� n S[��� I  T[�v��u�v 0 
updateicon 
updateIcon� ��t� m  TW�� ���  i d l e�t  �u  �  f  ST� ��� l \\�s�r�q�s  �r  �q  � ��p� Z \o�	 �o�n� l \c	�m�l	 =  \c			 o  \a�k�k  0 backupthenquit backupThenQuit	 m  ab�j
�j boovtrue�m  �l  	  n fk			 I  gk�i�h�g�i 0 killapp killApp�h  �g  	  f  fg�o  �n  �p  �^  ��  �[  �\  � m    		�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  � 			 l     �f�e�d�f  �e  �d  	 			
		 l     �c�b�a�c  �b  �a  	
 			 l     �`		�`  	 � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   	 �		 - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	 			 i  ��			 I      �_	�^�_ 0 itexists itExists	 			 o      �]�] 0 
objecttype 
objectType	 	�\	 o      �[�[ 
0 object  �\  �^  	 O     W			 Z    V			�Z	 l   	�Y�X	 =    			 o    �W�W 0 
objecttype 
objectType	 m    	 	  �	!	!  d i s k�Y  �X  	 Z   
 	"	#�V	$	" I  
 �U	%�T
�U .coredoexnull���     ****	% 4   
 �S	&
�S 
cdis	& o    �R�R 
0 object  �T  	# L    	'	' m    �Q
�Q boovtrue�V  	$ L    	(	( m    �P
�P boovfals	 	)	*	) l   "	+�O�N	+ =    "	,	-	, o     �M�M 0 
objecttype 
objectType	- m     !	.	. �	/	/  f i l e�O  �N  	* 	0	1	0 Z   % 7	2	3�L	4	2 I  % -�K	5�J
�K .coredoexnull���     ****	5 4   % )�I	6
�I 
file	6 o   ' (�H�H 
0 object  �J  	3 L   0 2	7	7 m   0 1�G
�G boovtrue�L  	4 L   5 7	8	8 m   5 6�F
�F boovfals	1 	9	:	9 l  : =	;�E�D	; =   : =	<	=	< o   : ;�C�C 0 
objecttype 
objectType	= m   ; <	>	> �	?	?  f o l d e r�E  �D  	: 	@�B	@ Z   @ R	A	B�A	C	A I  @ H�@	D�?
�@ .coredoexnull���     ****	D 4   @ D�>	E
�> 
cfol	E o   B C�=�= 
0 object  �?  	B L   K M	F	F m   K L�<
�< boovtrue�A  	C L   P R	G	G m   P Q�;
�; boovfals�B  �Z  	 m     	H	H�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  	 	I	J	I l     �:�9�8�:  �9  �8  	J 	K	L	K i  ��	M	N	M I      �7�6�5�7 .0 getcomputeridentifier getComputerIdentifier�6  �5  	N k     	O	O 	P	Q	P r     		R	S	R n     	T	U	T 1    �4
�4 
sicn	U l    	V�3�2	V I    �1�0�/
�1 .sysosigtsirr   ��� null�0  �/  �3  �2  	S o      �.�. 0 computername computerName	Q 	W	X	W r   
 	Y	Z	Y I  
 �-	[�,
�- .sysoexecTEXT���     TEXT	[ m   
 	\	\ �	]	] � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �,  	Z o      �+�+ "0 strserialnumber strSerialNumber	X 	^	_	^ r    	`	a	` l   	b�*�)	b b    	c	d	c b    	e	f	e o    �(�( 0 computername computerName	f m    	g	g �	h	h    -  	d o    �'�' "0 strserialnumber strSerialNumber�*  �)  	a o      �&�&  0 identifiername identifierName	_ 	i�%	i L    	j	j o    �$�$  0 identifiername identifierName�%  	L 	k	l	k l     �#�"�!�#  �"  �!  	l 	m	n	m i  ��	o	p	o I      � 	q��  0 gettimestamp getTimestamp	q 	r�	r o      �� 0 isfolder isFolder�  �  	p k    �	s	s 	t	u	t r     )	v	w	v l     	x��	x I     ���
� .misccurdldt    ��� null�  �  �  �  	w K    	y	y �	z	{
� 
year	z o    �� 0 y  	{ �	|	}
� 
mnth	| o    �� 0 m  	} �	~	
� 
day 	~ o    �� 0 d  	 �	��
� 
time	� o   	 
�� 0 t  �  	u 	�	�	� r   * 1	�	�	� c   * /	�	�	� l  * -	���	� c   * -	�	�	� o   * +�� 0 y  	� m   + ,�
� 
long�  �  	� m   - .�

�
 
TEXT	� o      �	�	 0 ty tY	� 	�	�	� r   2 9	�	�	� c   2 7	�	�	� l  2 5	���	� c   2 5	�	�	� o   2 3�� 0 y  	� m   3 4�
� 
long�  �  	� m   5 6�
� 
TEXT	� o      �� 0 ty tY	� 	�	�	� r   : A	�	�	� c   : ?	�	�	� l  : =	���	� c   : =	�	�	� o   : ;� �  0 m  	� m   ; <��
�� 
long�  �  	� m   = >��
�� 
TEXT	� o      ���� 0 tm tM	� 	�	�	� r   B I	�	�	� c   B G	�	�	� l  B E	�����	� c   B E	�	�	� o   B C���� 0 d  	� m   C D��
�� 
long��  ��  	� m   E F��
�� 
TEXT	� o      ���� 0 td tD	� 	�	�	� r   J Q	�	�	� c   J O	�	�	� l  J M	�����	� c   J M	�	�	� o   J K���� 0 t  	� m   K L��
�� 
long��  ��  	� m   M N��
�� 
TEXT	� o      ���� 0 tt tT	� 	�	�	� r   R [	�	�	� c   R Y	�	�	� l  R W	�����	� I  R W��	���
�� .corecnte****       ****	� o   R S���� 0 tm tM��  ��  ��  	� m   W X��
�� 
nmbr	� o      ���� 
0 tml tML	� 	�	�	� r   \ e	�	�	� c   \ c	�	�	� l  \ a	�����	� I  \ a��	���
�� .corecnte****       ****	� o   \ ]���� 0 tm tM��  ��  ��  	� m   a b��
�� 
nmbr	� o      ���� 
0 tdl tDL	� 	�	�	� l  f f��������  ��  ��  	� 	�	�	� Z  f u	�	�����	� l  f i	�����	� =   f i	�	�	� o   f g���� 
0 tml tML	� m   g h���� ��  ��  	� r   l q	�	�	� b   l o	�	�	� m   l m	�	� �	�	�  0	� o   m n���� 0 tm tM	� o      ���� 0 tm tM��  ��  	� 	�	�	� Z  v �	�	�����	� l  v y	�����	� =   v y	�	�	� o   v w���� 
0 tdl tDL	� m   w x���� ��  ��  	� r   | �	�	�	� b   | �	�	�	� m   | 	�	� �	�	�  0	� o    ����� 0 td tD	� o      ���� 0 td tD��  ��  	� 	�	�	� l  � ���������  ��  ��  	� 	�	�	� r   � �	�	�	� n   � �	�	�	� 1   � ���
�� 
tstr	� l  � �	�����	� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 timestr timeStr	� 	�	�	� r   � �	�	�	� I  � �	���	�	� z����
�� .sysooffslong    ��� null
�� misccura��  	� ��	�	�
�� 
psof	� m   � �	�	� �	�	�  :	� ��	���
�� 
psin	� o   � ����� 0 timestr timeStr��  	� o      ���� 0 pos  	� 	�	�	� r   � �	�	�	� c   � �	�	�	� n   � �	�	�	� 7  � ���	�	�
�� 
cha 	� m   � ����� 	� l  � �	�����	� \   � �	�	�	� o   � ����� 0 pos  	� m   � ����� ��  ��  	� o   � ����� 0 timestr timeStr	� m   � ���
�� 
TEXT	� o      ���� 0 h  	� 	�	�	� r   � �	�	�	� c   � �	�
 	� n   � �


 7 � ���


�� 
cha 
 l  � �
����
 [   � �


 o   � ����� 0 pos  
 m   � ����� ��  ��  
  ;   � �
 o   � ����� 0 timestr timeStr
  m   � ���
�� 
TEXT	� o      ���� 0 timestr timeStr	� 

	
 r   � �




 I  � �
��

 z����
�� .sysooffslong    ��� null
�� misccura��  
 ��


�� 
psof
 m   � �

 �

  :
 ��
��
�� 
psin
 o   � ����� 0 timestr timeStr��  
 o      ���� 0 pos  
	 


 r   �


 c   �


 n   � 


 7  � ��


�� 
cha 
 m   � ����� 
 l  � �
����
 \   � �


 o   � ����� 0 pos  
 m   � ����� ��  ��  
 o   � ����� 0 timestr timeStr
 m   ��
�� 
TEXT
 o      ���� 0 m  
 
 
!
  r  
"
#
" c  
$
%
$ n  
&
'
& 7��
(
)
�� 
cha 
( l 
*����
* [  
+
,
+ o  ���� 0 pos  
, m  ���� ��  ��  
)  ;  
' o  ���� 0 timestr timeStr
% m  ��
�� 
TEXT
# o      ���� 0 timestr timeStr
! 
-
.
- r  2
/
0
/ I 0
1��
2
1 z����
�� .sysooffslong    ��� null
�� misccura��  
2 ��
3
4
�� 
psof
3 m  "%
5
5 �
6
6   
4 ��
7��
�� 
psin
7 o  ()���� 0 timestr timeStr��  
0 o      ���� 0 pos  
. 
8
9
8 r  3E
:
;
: c  3C
<
=
< n  3A
>
?
> 74A��
@
A
�� 
cha 
@ l :>
B����
B [  :>
C
D
C o  ;<���� 0 pos  
D m  <=���� ��  ��  
A  ;  ?@
? o  34���� 0 timestr timeStr
= m  AB��
�� 
TEXT
; o      ���� 0 s  
9 
E
F
E l FF��������  ��  ��  
F 
G
H
G Z  F
I
J
K��
I l FI
L��~
L =  FI
M
N
M o  FG�}�} 0 isfolder isFolder
N m  GH�|
�| boovtrue�  �~  
J r  La
O
P
O b  L_
Q
R
Q b  L]
S
T
S b  L[
U
V
U b  LY
W
X
W b  LU
Y
Z
Y b  LS
[
\
[ b  LQ
]
^
] m  LO
_
_ �
`
`  b a c k u p _
^ o  OP�{�{ 0 ty tY
\ o  QR�z�z 0 tm tM
Z o  ST�y�y 0 td tD
X m  UX
a
a �
b
b  _
V o  YZ�x�x 0 h  
T o  [\�w�w 0 m  
R o  ]^�v�v 0 s  
P o      �u�u 0 	timestamp  
K 
c
d
c l dg
e�t�s
e =  dg
f
g
f o  de�r�r 0 isfolder isFolder
g m  ef�q
�q boovfals�t  �s  
d 
h�p
h r  j{
i
j
i b  jy
k
l
k b  jw
m
n
m b  ju
o
p
o b  js
q
r
q b  jo
s
t
s b  jm
u
v
u o  jk�o�o 0 ty tY
v o  kl�n�n 0 tm tM
t o  mn�m�m 0 td tD
r m  or
w
w �
x
x  _
p o  st�l�l 0 h  
n o  uv�k�k 0 m  
l o  wx�j�j 0 s  
j o      �i�i 0 	timestamp  �p  ��  
H 
y
z
y l ���h�g�f�h  �g  �f  
z 
{�e
{ L  ��
|
| o  ���d�d 0 	timestamp  �e  	n 
}
~
} l     �c�b�a�c  �b  �a  
~ 

�
 i  ��
�
�
� I      �`
��_�` 0 comparesizes compareSizes
� 
�
�
� o      �^�^ 
0 source  
� 
��]
� o      �\�\ 0 destination  �]  �_  
� k    �
�
� 
�
�
� r     
�
�
� m     �[
�[ boovtrue
� o      �Z�Z 0 fit  
� 
�
�
� r    

�
�
� 4    �Y
�
�Y 
psxf
� o    �X�X 0 destination  
� o      �W�W 0 destination  
� 
�
�
� r    
�
�
� b    
�
�
� b    
�
�
� b    
�
�
� o    �V�V "0 destinationdisk destinationDisk
� m    
�
� �
�
�  B a c k u p s /
� o    �U�U 0 machinefolder machineFolder
� m    
�
� �
�
�  / L a t e s t
� o      �T�T 0 
templatest 
tempLatest
� 
�
�
� r    
�
�
� m    �S�S  
� o      �R�R $0 latestfoldersize latestFolderSize
� 
�
�
� r    &
�
�
� b    $
�
�
� n   "
�
�
� 1     "�Q
�Q 
psxp
� l    
��P�O
� I    �N
�
�
�N .earsffdralis        afdr
� m    �M
�M afdrdlib
� �L
��K
�L 
from
� m    �J
�J fldmfldu�K  �P  �O  
� m   " #
�
� �
�
�  C a c h e s
� o      �I�I 0 c  
� 
�
�
� r   ' 4
�
�
� b   ' 2
�
�
� n  ' 0
�
�
� 1   . 0�H
�H 
psxp
� l  ' .
��G�F
� I  ' .�E
�
�
�E .earsffdralis        afdr
� m   ' (�D
�D afdrdlib
� �C
��B
�C 
from
� m   ) *�A
�A fldmfldu�B  �G  �F  
� m   0 1
�
� �
�
�  L o g s
� o      �@�@ 0 l  
� 
�
�
� r   5 B
�
�
� b   5 @
�
�
� n  5 >
�
�
� 1   < >�?
�? 
psxp
� l  5 <
��>�=
� I  5 <�<
�
�
�< .earsffdralis        afdr
� m   5 6�;
�; afdrdlib
� �:
��9
�: 
from
� m   7 8�8
�8 fldmfldu�9  �>  �=  
� m   > ?
�
� �
�
�   M o b i l e   D o c u m e n t s
� o      �7�7 0 md  
� 
�
�
� r   C F
�
�
� m   C D�6�6  
� o      �5�5 0 	cachesize 	cacheSize
� 
�
�
� r   G J
�
�
� m   G H�4�4  
� o      �3�3 0 logssize logsSize
� 
�
�
� r   K N
�
�
� m   K L�2�2  
� o      �1�1 0 mdsize mdSize
� 
�
�
� r   O R
�
�
� m   O P
�
� ?�      
� o      �0�0 
0 buffer  
� 
�
�
� r   S V
�
�
� m   S T�/�/  
� o      �.�. (0 adjustedfoldersize adjustedFolderSize
� 
�
�
� l  W W�-�,�+�-  �,  �+  
� 
�
�
� O   Wx
�
�
� k   [w
�
� 
�
�
� r   [ h
�
�
� I  [ f�*
��)
�* .sysoexecTEXT���     TEXT
� b   [ b
�
�
� b   [ ^
�
�
� m   [ \
�
� �
�
�  d u   - m s   '
� o   \ ]�(�( 
0 source  
� m   ^ a
�
� �
�
�  '   |   c u t   - f   1�)  
� o      �'�' 0 
foldersize 
folderSize
� 
�
�
� r   i �
�
�
� ^   i �
�
�
� l  i }
��&�%
� I  i }
�
��$
� z�#�"
�# .sysorondlong        doub
�" misccura
� ]   o x
� 
� l  o t�!�  ^   o t o   o p�� 0 
foldersize 
folderSize m   p s�� �!  �     m   t w�� d�$  �&  �%  
� m   } ��� d
� o      �� 0 
foldersize 
folderSize
�  r   � � ^   � �	 ^   � �

 ^   � � l  � ��� l  � ��� n   � � 1   � ��
� 
frsp 4   � ��
� 
cdis o   � ��� 0 destination  �  �  �  �   m   � ���  m   � ��� 	 m   � ���  o      �� 0 	freespace 	freeSpace  r   � � ^   � � l  � ��� I  � �� z��
� .sysorondlong        doub
� misccura l  � ��
�	 ]   � � o   � ��� 0 	freespace 	freeSpace m   � ��� d�
  �	  �  �  �   m   � ��� d o      �� 0 	freespace 	freeSpace   r   � �!"! I  � ��#�
� .sysoexecTEXT���     TEXT# b   � �$%$ b   � �&'& m   � �(( �))  d u   - m s   '' o   � ��� 0 c  % m   � �** �++  '   |   c u t   - f   1�  " o      �� 0 	cachesize 	cacheSize  ,-, r   � �./. ^   � �010 l  � �2� ��2 I  � �34��3 z����
�� .sysorondlong        doub
�� misccura4 ]   � �565 l  � �7����7 ^   � �898 o   � ����� 0 	cachesize 	cacheSize9 m   � ����� ��  ��  6 m   � ����� d��  �   ��  1 m   � ����� d/ o      ���� 0 	cachesize 	cacheSize- :;: r   � �<=< I  � ���>��
�� .sysoexecTEXT���     TEXT> b   � �?@? b   � �ABA m   � �CC �DD  d u   - m s   'B o   � ����� 0 l  @ m   � �EE �FF  '   |   c u t   - f   1��  = o      ���� 0 logssize logsSize; GHG r   �	IJI ^   �KLK l  �M����M I  �NO��N z����
�� .sysorondlong        doub
�� misccuraO ]   � �PQP l  � �R����R ^   � �STS o   � ����� 0 logssize logsSizeT m   � ����� ��  ��  Q m   � ����� d��  ��  ��  L m  ���� dJ o      ���� 0 logssize logsSizeH UVU r  
WXW I 
��Y��
�� .sysoexecTEXT���     TEXTY b  
Z[Z b  
\]\ m  
^^ �__  d u   - m s   '] o  ���� 0 md  [ m  `` �aa  '   |   c u t   - f   1��  X o      ���� 0 mdsize mdSizeV bcb r  4ded ^  2fgf l .h����h I .ij��i z����
�� .sysorondlong        doub
�� misccuraj ]   )klk l  %m����m ^   %non o   !���� 0 mdsize mdSizeo m  !$���� ��  ��  l m  %(���� d��  ��  ��  g m  .1���� de o      ���� 0 mdsize mdSizec pqp r  5>rsr l 5<t����t \  5<uvu \  5:wxw \  58yzy o  56���� 0 
foldersize 
folderSizez o  67���� 0 	cachesize 	cacheSizex o  89���� 0 logssize logsSizev o  :;���� 0 mdsize mdSize��  ��  s o      ���� (0 adjustedfoldersize adjustedFolderSizeq {|{ l ??��������  ��  ��  | }��} Z  ?w~����~ l ?F������ =  ?F��� o  ?D���� 0 initialbackup initialBackup� m  DE��
�� boovfals��  ��   k  Is�� ��� r  IX��� I IV�����
�� .sysoexecTEXT���     TEXT� b  IR��� b  IN��� m  IL�� ���  d u   - m s   '� o  LM���� 0 
templatest 
tempLatest� m  NQ�� ���  '   |   c u t   - f   1��  � o      ���� $0 latestfoldersize latestFolderSize� ���� r  Ys��� ^  Yq��� l Ym������ I Ym����� z����
�� .sysorondlong        doub
�� misccura� ]  _h��� l _d������ ^  _d��� o  _`���� $0 latestfoldersize latestFolderSize� m  `c���� ��  ��  � m  dg���� d��  ��  ��  � m  mp���� d� o      ���� $0 latestfoldersize latestFolderSize��  ��  ��  ��  
� m   W X���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  
� ��� l yy��������  ��  ��  � ��� Z  y������� l y������� =  y���� o  y~���� 0 initialbackup initialBackup� m  ~��
�� boovfals��  ��  � k  ���� ��� r  ����� \  ����� o  ������ (0 adjustedfoldersize adjustedFolderSize� o  ������ $0 latestfoldersize latestFolderSize� o      ���� 0 temp  � ��� Z ��������� l �������� A  ����� o  ������ 0 temp  � m  ������  ��  ��  � r  ����� \  ����� l �������� o  ������ 0 temp  ��  ��  � l �������� ]  ����� l �������� o  ������ 0 temp  ��  ��  � m  ������ ��  ��  � o      ���� 0 temp  ��  ��  � ���� Z ��������� l �������� ?  ����� o  ������ 0 temp  � l �������� \  ����� o  ������ 0 	freespace 	freeSpace� o  ������ 
0 buffer  ��  ��  ��  ��  � r  ����� m  ����
�� boovfals� o      ���� 0 fit  ��  ��  ��  � ��� l �������� =  ����� o  ������ 0 initialbackup initialBackup� m  ����
�� boovtrue��  ��  � ���� Z ��������� l �������� ?  ����� o  ������ (0 adjustedfoldersize adjustedFolderSize� l �������� \  ����� o  ���� 0 	freespace 	freeSpace� o  ���~�~ 
0 buffer  ��  ��  ��  ��  � r  ����� m  ���}
�} boovfals� o      �|�| 0 fit  ��  ��  ��  ��  � ��� l ���{�z�y�{  �z  �y  � ��x� L  ���� o  ���w�w 0 fit  �x  
� ��� l     �v�u�t�v  �u  �t  � ��� i  ����� I      �s�r�q�s 0 getduration getDuration�r  �q  � k     �� ��� r     ��� ^     ��� l    ��p�o� \     ��� o     �n�n 0 endtime endTime� o    �m�m 0 	starttime 	startTime�p  �o  � m    �l�l <� o      �k�k 0 duration  � ��� r    ��� ^    ��� l   ��j�i� I   ���h� z�g�f
�g .sysorondlong        doub
�f misccura� l   ��e�d� ]    ��� o    �c�c 0 duration  � m    �b�b d�e  �d  �h  �j  �i  � m    �a�a d� o      �`�` 0 duration  � ��_� L    �� o    �^�^ 0 duration  �_  � ��� l     �]�\�[�]  �\  �[  � � � i  �� I      �Z�Y�X�Z 0 setfocus setFocus�Y  �X   O     5 k    4  r    	 n    	

 1    	�W
�W 
pnam 2    �V
�V 
prcs	 o      �U�U 0 processlist ProcessList �T Z    4�S�R E    o    �Q�Q 0 processlist ProcessList m     � , W o a h ,   B a c k   T h e   F u c k   U p k    0  r     n     1    �P
�P 
idux 4    �O
�O 
prcs m     � , W o a h ,   B a c k   T h e   F u c k   U p o      �N�N 0 thepid ThePID �M O   0 r    / !  m     �L
�L boovtrue! 6    ."#" n     %$%$ 1   # %�K
�K 
pisf% 2     #�J
�J 
prcs# =  & -&'& 1   ' )�I
�I 
idux' o   * ,�H�H 0 thepid ThePID m    ((�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �M  �S  �R  �T   m     ))�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��    *+* l     �G�F�E�G  �F  �E  + ,-, i  ��./. I      �D0�C�D 0 updateplist updatePlist0 121 o      �B�B 0 tempitem tempItem2 3�A3 o      �@�@ 0 	tempvalue 	tempValue�A  �C  / k     q44 565 O     787 k    99 :;: r    <=< 4    �?>
�? 
plif> o    �>�> 	0 plist  = o      �=�= 0 	tempplist 	tempPlist; ?�<? O   @A@ r    BCB o    �;�; 0 	tempvalue 	tempValueC n      DED 1    �:
�: 
valLE 4    �9F
�9 
pliiF o    �8�8 0 tempitem tempItemA o    �7�7 0 	tempplist 	tempPlist�<  8 m     GG�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  6 HIH l   �6�5�4�6  �5  �4  I JKJ O    aLML k   " `NN OPO r   " .QRQ n   " ,STS 1   * ,�3
�3 
pcntT 4   " *�2U
�2 
plifU o   $ )�1�1 	0 plist  R o      �0�0 0 thecontents theContentsP VWV r   / 4XYX n   / 2Z[Z 1   0 2�/
�/ 
valL[ o   / 0�.�. 0 thecontents theContentsY o      �-�- 0 thevalue theValueW \]\ r   5 :^_^ n   5 8`a` o   6 8�,�,  0 foldertobackup FolderToBackupa o   5 6�+�+ 0 thevalue theValue_ o      �*�* 0 
foldername 
folderName] bcb r   ; @ded n   ; >fgf o   < >�)�) 0 backupdrive BackupDriveg o   ; <�(�( 0 thevalue theValuee o      �'�' 0 
backupdisk 
backupDiskc hih r   A Fjkj n   A Dlml o   B D�&�&  0 computerfolder ComputerFolderm o   A B�%�% 0 thevalue theValuek o      �$�$  0 computerfolder ComputerFolderi non r   G Lpqp n   G Jrsr o   H J�#�# 0 scheduledtime ScheduledTimes o   G H�"�" 0 thevalue theValueq o      �!�! 0 
backuptime 
backupTimeo tut r   M Vvwv n   M Pxyx o   N P� �  0 nsfw NSFWy o   M N�� 0 thevalue theValuew o      �� 0 isnsfw isNSFWu z�z r   W `{|{ n   W Z}~} o   X Z�� 0 notifications Notifications~ o   W X�� 0 thevalue theValue| o      �� 0 tonotify toNotify�  M m    �                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  K ��� l  b b����  �  �  � ��� r   b e��� o   b c�� 0 
foldername 
folderName� o      �� 0 sourcefolder sourceFolder� ��� r   f i��� o   f g�� 0 
backupdisk 
backupDisk� o      �� "0 destinationdisk destinationDisk� ��� r   j m��� o   j k��  0 computerfolder ComputerFolder� o      �� 0 machinefolder machineFolder� ��� r   n q��� o   n o�� 0 
backuptime 
backupTime� o      �� 0 scheduledtime ScheduledTime�  - ��� l     ����  �  �  � ��� i  ����� I      �
�	��
 0 killapp killApp�	  �  � O     '��� k    &�� ��� r    ��� n    	��� 1    	�
� 
pnam� 2    �
� 
prcs� o      �� 0 processlist ProcessList� ��� Z    &����� E   ��� o    �� 0 processlist ProcessList� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� k    "�� ��� r    ��� n    ��� 1    � 
�  
idux� 4    ���
�� 
prcs� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      ���� 0 thepid ThePID� ���� I   "�����
�� .sysoexecTEXT���     TEXT� b    ��� m    �� ���  k i l l   - K I L L  � o    ���� 0 thepid ThePID��  ��  �  �  �  � m     ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   � ��� - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� i  ����� I      ������� $0 menuneedsupdate_ menuNeedsUpdate_� ���� l     ������ m      ��
�� 
cmnu��  ��  ��  ��  � n    ��� I    �������� 0 	makemenus 	makeMenus��  ��  �  f     � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� 0 	makemenus 	makeMenus��  ��  � k    >�� ��� n    	��� I    	��������  0 removeallitems removeAllItems��  ��  � o     ���� 0 newmenu newMenu� ��� l  
 
��������  ��  ��  � ���� Y   
>�������� k   9�� ��� r    '��� n    %��� 4   " %���
�� 
cobj� o   # $���� 0 i  � o    "���� 0 thelist theList� o      ���� 0 	this_item  � ��� l  ( (��������  ��  ��  � ��� Z   ( ������ l  ( -������ =   ( -��� l  ( +������ c   ( +��� o   ( )���� 0 	this_item  � m   ) *��
�� 
TEXT��  ��  � m   + ,�� ���  B a c k u p   n o w��  ��  � r   0 @��� l  0 >������ n  0 >��� I   7 >������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8���� 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ���� m   9 :�� �    ��  ��  � n  0 7 I   3 7�������� 	0 alloc  ��  ��   n  0 3 o   1 3���� 0 
nsmenuitem 
NSMenuItem m   0 1��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem�  l  C H���� =   C H	 l  C F
����
 c   C F o   C D���� 0 	this_item   m   D E��
�� 
TEXT��  ��  	 m   F G � " B a c k u p   n o w   &   q u i t��  ��    r   K [ l  K Y���� n  K Y I   R Y������ J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   R S���� 0 	this_item    m   S T � * b a c k u p A n d Q u i t H a n d l e r : �� m   T U �  ��  ��   n  K R !  I   N R�������� 	0 alloc  ��  ��  ! n  K N"#" o   L N���� 0 
nsmenuitem 
NSMenuItem# m   K L��
�� misccura��  ��   o      ���� 0 thismenuitem thisMenuItem $%$ l  ^ c&����& =   ^ c'(' l  ^ a)����) c   ^ a*+* o   ^ _���� 0 	this_item  + m   _ `��
�� 
TEXT��  ��  ( m   a b,, �-- " T u r n   o n   t h e   b a n t s��  ��  % ./. r   f x010 l  f v2����2 n  f v343 I   m v��5���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_5 676 o   m n���� 0 	this_item  7 898 m   n o:: �;;  n s f w O n H a n d l e r :9 <��< m   o r== �>>  ��  ��  4 n  f m?@? I   i m�������� 	0 alloc  ��  ��  @ n  f iABA o   g i���� 0 
nsmenuitem 
NSMenuItemB m   f g��
�� misccura��  ��  1 o      ���� 0 thismenuitem thisMenuItem/ CDC l  { �E����E =   { �FGF l  { ~H����H c   { ~IJI o   { |���� 0 	this_item  J m   | }��
�� 
TEXT��  ��  G m   ~ �KK �LL $ T u r n   o f f   t h e   b a n t s��  ��  D MNM r   � �OPO l  � �Q����Q n  � �RSR I   � ���T���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_T UVU o   � ����� 0 	this_item  V WXW m   � �YY �ZZ  n s f w O f f H a n d l e r :X [��[ m   � �\\ �]]  ��  ��  S n  � �^_^ I   � ��������� 	0 alloc  ��  ��  _ n  � �`a` o   � ����� 0 
nsmenuitem 
NSMenuItema m   � ���
�� misccura��  ��  P o      ���� 0 thismenuitem thisMenuItemN bcb l  � �d����d =   � �efe l  � �g����g c   � �hih o   � ����� 0 	this_item  i m   � ���
�� 
TEXT��  ��  f m   � �jj �kk * T u r n   o n   n o t i f i c a t i o n s��  ��  c lml r   � �non l  � �p����p n  � �qrq I   � ���s���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_s tut o   � ��� 0 	this_item  u vwv m   � �xx �yy , n o t i f i c a t i o n O n H a n d l e r :w z�~z m   � �{{ �||  �~  ��  r n  � �}~} I   � ��}�|�{�} 	0 alloc  �|  �{  ~ n  � �� o   � ��z�z 0 
nsmenuitem 
NSMenuItem� m   � ��y
�y misccura��  ��  o o      �x�x 0 thismenuitem thisMenuItemm ��� l  � ���w�v� =   � ���� l  � ���u�t� c   � ���� o   � ��s�s 0 	this_item  � m   � ��r
�r 
TEXT�u  �t  � m   � ��� ��� , T u r n   o f f   n o t i f i c a t i o n s�w  �v  � ��� r   � ���� l  � ���q�p� n  � ���� I   � ��o��n�o J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ��m�m 0 	this_item  � ��� m   � ��� ��� . n o t i f i c a t i o n O f f H a n d l e r :� ��l� m   � ��� ���  �l  �n  � n  � ���� I   � ��k�j�i�k 	0 alloc  �j  �i  � n  � ���� o   � ��h�h 0 
nsmenuitem 
NSMenuItem� m   � ��g
�g misccura�q  �p  � o      �f�f 0 thismenuitem thisMenuItem� ��� l  � ���e�d� =   � ���� l  � ���c�b� c   � ���� o   � ��a�a 0 	this_item  � m   � ��`
�` 
TEXT�c  �b  � m   � ��� ���  Q u i t�e  �d  � ��_� r   � ���� l  � ���^�]� n  � ���� I   � ��\��[�\ J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ��Z�Z 0 	this_item  � ��� m   � ��� ���  q u i t H a n d l e r :� ��Y� m   � ��� ���  �Y  �[  � n  � ���� I   � ��X�W�V�X 	0 alloc  �W  �V  � n  � ���� o   � ��U�U 0 
nsmenuitem 
NSMenuItem� m   � ��T
�T misccura�^  �]  � o      �S�S 0 thismenuitem thisMenuItem�_  ��  � ��� l �R�Q�P�R  �Q  �P  � ��� l ��O�N� n ��� I  �M��L�M 0 additem_ addItem_� ��K� o  �J�J 0 thismenuitem thisMenuItem�K  �L  � o  �I�I 0 newmenu newMenu�O  �N  � ��� l ��H�G� n ��� I  �F��E�F 0 
settarget_ 
setTarget_� ��D�  f  �D  �E  � o  �C�C 0 thismenuitem thisMenuItem�H  �G  � ��� l �B�A�@�B  �A  �@  � ��?� Z 9���>�=� G  "��� l ��<�;� =  ��� o  �:�: 0 i  � m  �9�9 �<  �;  � l ��8�7� =  ��� o  �6�6 0 i  � m  �5�5 �8  �7  � l %5��4�3� n %5��� I  *5�2��1�2 0 additem_ addItem_� ��0� l *1��/�.� n *1��� o  -1�-�- 0 separatoritem separatorItem� n *-��� o  +-�,�, 0 
nsmenuitem 
NSMenuItem� m  *+�+
�+ misccura�/  �.  �0  �1  � o  %*�*�* 0 newmenu newMenu�4  �3  �>  �=  �?  �� 0 i  � m    �)�) � n    ��� m    �(
�( 
nmbr� n   ��� 2   �'
�' 
cobj� o    �&�& 0 thelist theList��  ��  � ��� l     �%�$�#�%  �$  �#  � ��� i  ����� I      �"�!� �" 0 	initmenus 	initMenus�!  �   � Z     ������ l    ���� =     ��� o     �� 0 isnsfw isNSFW� m    �
� boovtrue�  �  � Z   
 ?����� l  
 ���� =   
 ��� o   
 �� 0 tonotify toNotify� m    �
� boovtrue�  �  � r    !��� J    �� ��� m       �  B a c k u p   n o w�  m     � " B a c k u p   n o w   &   q u i t  m     �		 $ T u r n   o f f   t h e   b a n t s 

 m     � , T u r n   o f f   n o t i f i c a t i o n s � m     �  Q u i t�  � o      �� 0 thelist theList�  l  $ +�� =   $ + o   $ )�� 0 tonotify toNotify m   ) *�
� boovfals�  �   � r   . ; J   . 5  m   . / �  B a c k u p   n o w  m   / 0   �!! " B a c k u p   n o w   &   q u i t "#" m   0 1$$ �%% $ T u r n   o f f   t h e   b a n t s# &'& m   1 2(( �)) * T u r n   o n   n o t i f i c a t i o n s' *�* m   2 3++ �,,  Q u i t�   o      �� 0 thelist theList�  �  � -.- l  B I/��/ =   B I010 o   B G�
�
 0 isnsfw isNSFW1 m   G H�	
�	 boovfals�  �  . 2�2 Z   L �345�3 l  L S6��6 =   L S787 o   L Q�� 0 tonotify toNotify8 m   Q R�
� boovtrue�  �  4 r   V c9:9 J   V ];; <=< m   V W>> �??  B a c k u p   n o w= @A@ m   W XBB �CC " B a c k u p   n o w   &   q u i tA DED m   X YFF �GG " T u r n   o n   t h e   b a n t sE HIH m   Y ZJJ �KK , T u r n   o f f   n o t i f i c a t i o n sI L�L m   Z [MM �NN  Q u i t�  : o      �� 0 thelist theList5 OPO l  f mQ� ��Q =   f mRSR o   f k���� 0 tonotify toNotifyS m   k l��
�� boovfals�   ��  P T��T r   p �UVU J   p �WW XYX m   p sZZ �[[  B a c k u p   n o wY \]\ m   s v^^ �__ " B a c k u p   n o w   &   q u i t] `a` m   v ybb �cc " T u r n   o n   t h e   b a n t sa ded m   y |ff �gg * T u r n   o n   n o t i f i c a t i o n se h��h m   | ii �jj  Q u i t��  V o      ���� 0 thelist theList��  �  �  �  � klk l     ��������  ��  ��  l mnm i  ��opo I      ��q����  0 backuphandler_ backupHandler_q r��r o      ���� 
0 sender  ��  ��  p Z     �stu��s l    v����v =     wxw o     ���� 0 isbackingup isBackingUpx m    ��
�� boovfals��  ��  t k   
 Uyy z{z r   
 |}| m   
 ��
�� boovtrue} o      ���� 0 manualbackup manualBackup{ ~��~ Z    U����� l   ������ =    ��� o    ���� 0 tonotify toNotify� m    ��
�� boovtrue��  ��  � Z    Q������ l   #������ =    #��� o    !���� 0 isnsfw isNSFW� m   ! "��
�� boovtrue��  ��  � k   & 3�� ��� I  & -����
�� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �����
�� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  . 3�����
�� .sysodelanull��� ��� nmbr� m   . /���� ��  ��  � ��� l  6 =������ =   6 =��� o   6 ;���� 0 isnsfw isNSFW� m   ; <��
�� boovfals��  ��  � ���� k   @ M�� ��� I  @ G����
�� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� �����
�� 
appr� m   B C�� ��� 
 W B T F U��  � ���� I  H M�����
�� .sysodelanull��� ��� nmbr� m   H I���� ��  ��  ��  ��  ��  ��  ��  u ��� l  X _������ =   X _��� o   X ]���� 0 isbackingup isBackingUp� m   ] ^��
�� boovtrue��  ��  � ���� k   b ��� ��� r   b k��� n   b i��� 3   g i��
�� 
cobj� o   b g���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   l �������� l  l s������ =   l s��� o   l q���� 0 tonotify toNotify� m   q r��
�� boovtrue��  ��  � Z   v ������� l  v }������ =   v }��� o   v {���� 0 isnsfw isNSFW� m   { |��
�� boovtrue��  ��  � I  � �����
�� .sysonotfnull��� ��� TEXT� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 isnsfw isNSFW� m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  ��  ��  n ��� l     ��������  ��  ��  � ��� i  ����� I      ������� .0 backupandquithandler_ backupAndQuitHandler_� ���� o      ���� 
0 sender  ��  ��  � k     ��� ��� n    ��� I    �������� 0 setfocus setFocus��  ��  �  f     � ��� l   ��������  ��  ��  � ���� Z    ������� l   ������ =    ��� o    ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � t    ���� k    ��� ��� r    *��� l   (������ n    (��� 1   & (��
�� 
bhit� l   &������ I   &����
�� .sysodlogaskr        TEXT� m    �� ��� T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?� ����
�� 
appr� o    ���� 0 	temptitle 	tempTitle� ����
�� 
disp� o    ���� 0 appicon appIcon� ����
�� 
btns� J     �� � � m     �  Y e s  �� m     �  N o��  � ����
�� 
dflt m   ! "���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  �  l  + +��������  ��  ��   	��	 Z   + �
��
 l  + .���� =   + . o   + ,���� 	0 reply   m   , - �  Y e s��  ��   k   1 �  r   1 8 m   1 2��
�� boovtrue o      ����  0 backupthenquit backupThenQuit  r   9 @ m   9 :�
� boovtrue o      �~�~ 0 manualbackup manualBackup  l  A A�}�|�{�}  �|  �{   �z Z   A ��y�x l  A H �w�v  =   A H!"! o   A F�u�u 0 tonotify toNotify" m   F G�t
�t boovtrue�w  �v   Z   K �#$%�s# l  K R&�r�q& =   K R'(' o   K P�p�p 0 isnsfw isNSFW( m   P Q�o
�o boovtrue�r  �q  $ k   U b)) *+* I  U \�n,-
�n .sysonotfnull��� ��� TEXT, m   U V.. �// T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p- �m0�l
�m 
appr0 m   W X11 �22 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�l  + 3�k3 I  ] b�j4�i
�j .sysodelanull��� ��� nmbr4 m   ] ^�h�h �i  �k  % 565 l  e l7�g�f7 =   e l898 o   e j�e�e 0 isnsfw isNSFW9 m   j k�d
�d boovfals�g  �f  6 :�c: k   o �;; <=< I  o z�b>?
�b .sysonotfnull��� ��� TEXT> m   o r@@ �AA   P r e p a r i n g   b a c k u p? �aB�`
�a 
apprB m   s vCC �DD 
 W B T F U�`  = E�_E I  { ��^F�]
�^ .sysodelanull��� ��� nmbrF m   { |�\�\ �]  �_  �c  �s  �y  �x  �z   GHG l  � �I�[�ZI =   � �JKJ o   � ��Y�Y 	0 reply  K m   � �LL �MM  N o�[  �Z  H N�XN r   � �OPO m   � ��W
�W boovfalsP o      �V�V  0 backupthenquit backupThenQuit�X  ��  ��  � m    �U�U  ��� QRQ l  � �S�T�SS =   � �TUT o   � ��R�R 0 isbackingup isBackingUpU m   � ��Q
�Q boovtrue�T  �S  R V�PV k   � �WW XYX r   � �Z[Z n   � �\]\ 3   � ��O
�O 
cobj] o   � ��N�N 40 messagesalreadybackingup messagesAlreadyBackingUp[ o      �M�M 0 msg  Y ^�L^ Z   � �_`�K�J_ l  � �a�I�Ha =   � �bcb o   � ��G�G 0 tonotify toNotifyc m   � ��F
�F boovtrue�I  �H  ` Z   � �def�Ed l  � �g�D�Cg =   � �hih o   � ��B�B 0 isnsfw isNSFWi m   � ��A
�A boovtrue�D  �C  e I  � ��@jk
�@ .sysonotfnull��� ��� TEXTj o   � ��?�? 0 msg  k �>l�=
�> 
apprl m   � �mm �nn P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�=  f opo l  � �q�<�;q =   � �rsr o   � ��:�: 0 isnsfw isNSFWs m   � ��9
�9 boovfals�<  �;  p t�8t I  � ��7uv
�7 .sysonotfnull��� ��� TEXTu m   � �ww �xx 2 Y o u ' r e   a l r e a d y   b a c k i n g   u pv �6y�5
�6 
appry m   � �zz �{{ 
 W B T F U�5  �8  �E  �K  �J  �L  �P  ��  ��  � |}| l     �4�3�2�4  �3  �2  } ~~ i  ����� I      �1��0�1  0 nsfwonhandler_ nsfwOnHandler_� ��/� o      �.�. 
0 sender  �/  �0  � k     Q�� ��� r     ��� m     �-
�- boovtrue� o      �,�, 0 isnsfw isNSFW� ��� r    ��� m    	�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      �+�+ 0 	temptitle 	tempTitle� ��� I    �*��)�* 0 updateplist updatePlist� ��� m    �� ���  N S F W� ��(� o    �'�' 0 isnsfw isNSFW�(  �)  � ��&� Z    Q����%� l   #��$�#� =    #��� o    !�"�" 0 tonotify toNotify� m   ! "�!
�! boovtrue�$  �#  � r   & 3��� J   & -�� ��� m   & '�� ���  B a c k u p   n o w� ��� m   ' (�� ��� " B a c k u p   n o w   &   q u i t� ��� m   ( )�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   ) *�� ��� , T u r n   o f f   n o t i f i c a t i o n s� �� � m   * +�� ���  Q u i t�   � o      �� 0 thelist theList� ��� l  6 =���� =   6 =��� o   6 ;�� 0 tonotify toNotify� m   ; <�
� boovfals�  �  � ��� r   @ M��� J   @ G�� ��� m   @ A�� ���  B a c k u p   n o w� ��� m   A B�� ��� " B a c k u p   n o w   &   q u i t� ��� m   B C�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   C D�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��� m   D E�� ���  Q u i t�  � o      �� 0 thelist theList�  �%  �&   ��� l     ����  �  �  � ��� i  ����� I      ���� "0 nsfwoffhandler_ nsfwOffHandler_� ��� o      �� 
0 sender  �  �  � k     Q�� ��� r     ��� m     �
� boovfals� o      �� 0 isnsfw isNSFW� ��� r    ��� m    	�� ��� 
 W B T F U� o      �� 0 	temptitle 	tempTitle� ��� I    ���� 0 updateplist updatePlist� ��� m    �� ���  N S F W� ��� o    �
�
 0 isnsfw isNSFW�  �  � ��	� Z    Q����� l   #���� =    #��� o    !�� 0 tonotify toNotify� m   ! "�
� boovtrue�  �  � r   & 3��� J   & -�� ��� m   & '�� ���  B a c k u p   n o w� ��� m   ' (�� ��� " B a c k u p   n o w   &   q u i t� ��� m   ( )�� �   " T u r n   o n   t h e   b a n t s�  m   ) * � , T u r n   o f f   n o t i f i c a t i o n s � m   * + �  Q u i t�  � o      �� 0 thelist theList� 	 l  6 =
�� 
 =   6 = o   6 ;���� 0 tonotify toNotify m   ; <��
�� boovfals�  �   	 �� r   @ M J   @ G  m   @ A �  B a c k u p   n o w  m   A B � " B a c k u p   n o w   &   q u i t  m   B C � " T u r n   o n   t h e   b a n t s  m   C D �   * T u r n   o n   n o t i f i c a t i o n s !��! m   D E"" �##  Q u i t��   o      ���� 0 thelist theList��  �  �	  � $%$ l     ��������  ��  ��  % &'& i  ��()( I      ��*���� 00 notificationonhandler_ notificationOnHandler_* +��+ o      ���� 
0 sender  ��  ��  ) k     I,, -.- r     /0/ m     ��
�� boovtrue0 o      ���� 0 tonotify toNotify. 121 I    ��3���� 0 updateplist updatePlist3 454 m   	 
66 �77  N o t i f i c a t i o n s5 8��8 o   
 ���� 0 tonotify toNotify��  ��  2 9��9 Z    I:;<��: l   =����= =    >?> o    ���� 0 isnsfw isNSFW? m    ��
�� boovtrue��  ��  ; r    +@A@ J    %BB CDC m    EE �FF  B a c k u p   n o wD GHG m     II �JJ " B a c k u p   n o w   &   q u i tH KLK m     !MM �NN $ T u r n   o f f   t h e   b a n t sL OPO m   ! "QQ �RR , T u r n   o f f   n o t i f i c a t i o n sP S��S m   " #TT �UU  Q u i t��  A o      ���� 0 thelist theList< VWV l  . 5X����X =   . 5YZY o   . 3���� 0 isnsfw isNSFWZ m   3 4��
�� boovfals��  ��  W [��[ r   8 E\]\ J   8 ?^^ _`_ m   8 9aa �bb  B a c k u p   n o w` cdc m   9 :ee �ff " B a c k u p   n o w   &   q u i td ghg m   : ;ii �jj " T u r n   o n   t h e   b a n t sh klk m   ; <mm �nn , T u r n   o f f   n o t i f i c a t i o n sl o��o m   < =pp �qq  Q u i t��  ] o      ���� 0 thelist theList��  ��  ��  ' rsr l     ��������  ��  ��  s tut i  ��vwv I      ��x���� 20 notificationoffhandler_ notificationOffHandler_x y��y o      ���� 
0 sender  ��  ��  w k     Izz {|{ r     }~} m     ��
�� boovfals~ o      ���� 0 tonotify toNotify| � I    ������� 0 updateplist updatePlist� ��� m   	 
�� ���  N o t i f i c a t i o n s� ���� o   
 ���� 0 tonotify toNotify��  ��  � ���� Z    I������ l   ������ =    ��� o    ���� 0 isnsfw isNSFW� m    ��
�� boovtrue��  ��  � r    +��� J    %�� ��� m    �� ���  B a c k u p   n o w� ��� m     �� ��� " B a c k u p   n o w   &   q u i t� ��� m     !�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   ! "�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   " #�� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  . 5������ =   . 5��� o   . 3���� 0 isnsfw isNSFW� m   3 4��
�� boovfals��  ��  � ���� r   8 E��� J   8 ?�� ��� m   8 9�� ���  B a c k u p   n o w� ��� m   9 :�� ��� " B a c k u p   n o w   &   q u i t� ��� m   : ;�� ��� " T u r n   o n   t h e   b a n t s� ��� m   ; <�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   < =�� ���  Q u i t��  � o      ���� 0 thelist theList��  ��  ��  u ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 0 quithandler_ quitHandler_� ���� o      ���� 
0 sender  ��  ��  � t     z��� k    y�� ��� n   ��� I    �������� 0 setfocus setFocus��  ��  �  f    � ��� l   ��������  ��  ��  � ���� Z    y������ l   ������ =    ��� o    ���� 0 isbackingup isBackingUp� m    ��
�� boovtrue��  ��  � k    :�� ��� r    *��� l   (������ n    (��� 1   & (��
�� 
bhit� l   &������ I   &����
�� .sysodlogaskr        TEXT� m    �� ��� � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?� ����
�� 
appr� o    ���� 0 	temptitle 	tempTitle� ����
�� 
disp� o    ���� 0 appicon appIcon� ����
�� 
btns� J     �� ��� m    �� ���  C a n c e l   &   Q u i t� ���� m    �� ���  R e s u m e��  � �����
�� 
dflt� m   ! "���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ���� Z   + :������ l  + .������ =   + .��� o   + ,���� 	0 reply  � m   , -�� ���  C a n c e l   &   Q u i t��  ��  � n  1 6��� I   2 6�������� 0 killapp killApp��  ��  �  f   1 2��  � l  9 9��������  ��  ��  ��  � � � l  = D���� =   = D o   = B���� 0 isbackingup isBackingUp m   B C��
�� boovfals��  ��    �� k   G u  r   G c	 l  G a
����
 n   G a 1   _ a��
�� 
bhit l  G _���� I  G _��
�� .sysodlogaskr        TEXT m   G H � < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ? ��
�� 
appr o   I N���� 0 	temptitle 	tempTitle ��
�� 
disp o   O P���� 0 appicon appIcon ��
�� 
btns J   Q Y  m   Q T �  Q u i t �� m   T W �  R e s u m e��   �� ��
�� 
dflt  m   Z [���� ��  ��  ��  ��  ��  	 o      ���� 	0 reply   !�! Z   d u"#�~$" l  d i%�}�|% =   d i&'& o   d e�{�{ 	0 reply  ' m   e h(( �))  Q u i t�}  �|  # n  l q*+* I   m q�z�y�x�z 0 killapp killApp�y  �x  +  f   l m�~  $ l  t t�w�v�u�w  �v  �u  �  ��  ��  ��  � m     �t�t  ��� ,-, l     �s�r�q�s  �r  �q  - ./. i  ��010 I      �p2�o�p 0 
updateicon 
updateIcon2 3�n3 o      �m�m 0 mode  �n  �o  1 k     O44 565 Z     /789�l7 l    :�k�j: =     ;<; o     �i�i 0 mode  < m    == �>>  a c t i v e�k  �j  8 r    ?@? 4    �hA
�h 
alisA l   B�g�fB b    CDC l   E�e�dE I   �cFG
�c .earsffdralis        afdrF  f    	G �bH�a
�b 
rtypH m   
 �`
�` 
ctxt�a  �e  �d  D m    II �JJ J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g�g  �f  @ o      �_�_ 0 	imagepath 	imagePath9 KLK l   M�^�]M =    NON o    �\�\ 0 mode  O m    PP �QQ  i d l e�^  �]  L R�[R r    +STS 4    )�ZU
�Z 
alisU l   (V�Y�XV b    (WXW l   &Y�W�VY I   &�UZ[
�U .earsffdralis        afdrZ  f     [ �T\�S
�T 
rtyp\ m   ! "�R
�R 
ctxt�S  �W  �V  X m   & ']] �^^ J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�Y  �X  T o      �Q�Q 0 	imagepath 	imagePath�[  �l  6 _`_ l  0 0�P�O�N�P  �O  �N  ` aba r   0 5cdc n   0 3efe 1   1 3�M
�M 
psxpf o   0 1�L�L 0 	imagepath 	imagePathd o      �K�K 0 	imagepath 	imagePathb ghg r   6 Diji n  6 Bklk I   = B�Jm�I�J 20 initwithcontentsoffile_ initWithContentsOfFile_m n�Hn o   = >�G�G 0 	imagepath 	imagePath�H  �I  l n  6 =opo I   9 =�F�E�D�F 	0 alloc  �E  �D  p n  6 9qrq o   7 9�C�C 0 nsimage NSImager m   6 7�B
�B misccuraj o      �A�A 	0 image  h s�@s n  E Otut I   J O�?v�>�? 0 	setimage_ 	setImage_v w�=w o   J K�<�< 	0 image  �=  �>  u o   E J�;�; 0 
statusitem 
StatusItem�@  / xyx l     �:�9�8�:  �9  �8  y z{z i  ��|}| I      �7�6�5�7 0 makestatusbar makeStatusBar�6  �5  } k     H~~ � r     ��� n    ��� o    �4�4 "0 systemstatusbar systemStatusBar� n    ��� o    �3�3 0 nsstatusbar NSStatusBar� m     �2
�2 misccura� o      �1�1 0 bar  � ��� r    ��� n   ��� I   	 �0��/�0 .0 statusitemwithlength_ statusItemWithLength_� ��.� m   	 
�� ��      �.  �/  � o    	�-�- 0 bar  � o      �,�, 0 
statusitem 
StatusItem� ��� r    '��� n   !��� I    !�+��*�+  0 initwithtitle_ initWithTitle_� ��)� m    �� ���  C u s t o m�)  �*  � n   ��� I    �(�'�&�( 	0 alloc  �'  �&  � n   ��� o    �%�% 0 nsmenu NSMenu� m    �$
�$ misccura� o      �#�# 0 newmenu newMenu� ��� l  ( (�"�!� �"  �!  �   � ��� n  ( .��� I   ) .���� 0 
updateicon 
updateIcon� ��� m   ) *�� ���  i d l e�  �  �  f   ( )� ��� l  / /����  �  �  � ��� n  / 9��� I   4 9���� 0 setdelegate_ setDelegate_� ���  f   4 5�  �  � o   / 4�� 0 newmenu newMenu� ��� n  : H��� I   ? H���� 0 setmenu_ setMenu_� ��� o   ? D�� 0 newmenu newMenu�  �  � o   : ?�� 0 
statusitem 
StatusItem�  { ��� l     ����  �  �  � ��� l     ���
�  �  �
  � ��� i  ����� I      �	���	 0 runonce runOnce�  �  � k     $�� ��� I    ���
� .sysodelanull��� ��� nmbr� m     �� �  � ��� n   ��� I    ���� 0 makestatusbar makeStatusBar�  �  �  f    � ��� I   � ���
�  .sysodelanull��� ��� nmbr� m    ���� ��  � ��� n   ��� I    ������� 0 
updateicon 
updateIcon� ���� m    �� ���  a c t i v e��  ��  �  f    � ��� I   �����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ���� I    $�������� 0 	plistinit 	plistInit��  ��  ��  � ��� l     ��������  ��  ��  � ��� l   ������ I    �������� 0 runonce runOnce��  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ���� i  ����� I     ������
�� .miscidlenmbr    ��� null��  ��  � k     ��� ��� Z     �������� l    ������ =     ��� o     ���� 0 	idleready 	idleReady� m    ��
�� boovtrue��  ��  � Z   
 �������� l  
 ������ E   
 ��� o   
 ���� &0 displaysleepstate displaySleepState� o    ���� 0 strawake strAwake��  ��  � Z    �������� l   ������ =    ��� o    ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � Z   " ������� l  " )������ =   " )��� o   " '���� 0 manualbackup manualBackup� m   ' (��
�� boovfals��  ��  � k   , m�� ��� r   , 5��� l  , 3������ n   , 3��� 1   1 3��
�� 
min � l  , 1 ����  l  , 1���� I  , 1������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  ��  ��  � o      ���� 0 m  �  l  6 6��������  ��  ��   �� Z   6 m�� G   6 A	 l  6 9
����
 =   6 9 o   6 7���� 0 m   m   7 8���� ��  ��  	 l  < ?���� =   < ? o   < =���� 0 m   m   = >���� -��  ��   Z  D S���� l  D G���� =   D G o   D E���� 0 intervaltime intervalTime m   E F���� ��  ��   I   J O�������� 0 	plistinit 	plistInit��  ��  ��  ��    G   V a l  V Y���� =   V Y o   V W���� 0 m   m   W X����  ��  ��   l  \ _���� =   \ _ o   \ ]���� 0 m   m   ] ^���� ��  ��   �� I   d i�������� 0 	plistinit 	plistInit��  ��  ��  ��  ��  �  !  l  p w"����" =   p w#$# o   p u���� 0 manualbackup manualBackup$ m   u v��
�� boovtrue��  ��  ! %��% I   z �������� 0 	plistinit 	plistInit��  ��  ��  ��  ��  ��  ��  ��  ��  ��  � &'& l  � ���������  ��  ��  ' (��( L   � �)) m   � ����� ��  ��       <��* '+� H,-./0����� ��1���23456��7�89:;<=>?@ABCDEFGHIJKLMNOPQRST��  * :��������~�}�|�{�z�y�x�w�v�u�t�s�r�q�p�o�n�m�l�k�j�i�h�g�f�e�d�c�b�a�`�_�^�]�\�[�Z�Y�X�W�V�U�T�S�R�Q�P�O�N�M�L�  0 currentversion currentVersion
� 
pimr� 0 
statusitem 
StatusItem� 0 
thedisplay 
theDisplay� 0 defaults  � $0 internalmenuitem internalMenuItem� $0 externalmenuitem externalMenuItem�~ 0 newmenu newMenu�} 0 thelist theList�| 0 isnsfw isNSFW�{ 0 tonotify toNotify�z  0 backupthenquit backupThenQuit�y 0 	idleready 	idleReady�x 0 isconnected isConnected�w 0 
updatefile 
updateFile�v 0 
newversion 
newVersion�u 	0 plist  �t 0 initialbackup initialBackup�s 0 manualbackup manualBackup�r 0 isbackingup isBackingUp�q "0 messagesmissing messagesMissing�p *0 messagesencouraging messagesEncouraging�o $0 messagescomplete messagesComplete�n &0 messagescancelled messagesCancelled�m 40 messagesalreadybackingup messagesAlreadyBackingUp�l 0 strawake strAwake�k 0 strsleep strSleep�j &0 displaysleepstate displaySleepState�i 0 	temptitle 	tempTitle�h 0 	plistinit 	plistInit�g 0 diskinit diskInit�f 0 freespaceinit freeSpaceInit�e 0 
backupinit 
backupInit�d 0 tidyup tidyUp�c 
0 backup  �b 0 itexists itExists�a .0 getcomputeridentifier getComputerIdentifier�` 0 gettimestamp getTimestamp�_ 0 comparesizes compareSizes�^ 0 getduration getDuration�] 0 setfocus setFocus�\ 0 updateplist updatePlist�[ 0 killapp killApp�Z $0 menuneedsupdate_ menuNeedsUpdate_�Y 0 	makemenus 	makeMenus�X 0 	initmenus 	initMenus�W  0 backuphandler_ backupHandler_�V .0 backupandquithandler_ backupAndQuitHandler_�U  0 nsfwonhandler_ nsfwOnHandler_�T "0 nsfwoffhandler_ nsfwOffHandler_�S 00 notificationonhandler_ notificationOnHandler_�R 20 notificationoffhandler_ notificationOffHandler_�Q 0 quithandler_ quitHandler_�P 0 
updateicon 
updateIcon�O 0 makestatusbar makeStatusBar�N 0 runonce runOnce
�M .miscidlenmbr    ��� null
�L .aevtoappnull  �   � ****+ �KU�K U  VWXYV �J /�I
�J 
vers�I  W �HZ�G
�H 
cobjZ [[   �F
�F 
osax�G  X �E\�D
�E 
cobj\ ]]   �C 8
�C 
frmk�D  Y �B^�A
�B 
cobj^ __   �@ >
�@ 
frmk�A  
� 
msng, `` �?�>a
�? misccura
�> 
pclsa �bb  N S U s e r D e f a u l t s- cc �=�<d
�= misccura
�< 
pclsd �ee  N S M e n u I t e m. ff �;�:g
�; misccura
�: 
pclsg �hh  N S M e n u I t e m/ ii �9�8j
�9 misccura
�8 
pclsj �kk  N S M e n u0 �7l�7 l   h l p t w
� boovfals
� boovtrue
� boovfals
� boovfals
� boovtrue�  1 �mm � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
� boovtrue
� boovfals
� boovfals2 �6n�6 n   � � � � �3 �5o�5 
o 
  � � � � � � 4 �4p�4 
p 
 #'+/365 �3q�3 	q 	 >BFJNRVZ]6 �2r�2 	r 	 eimquy}��7 �ss�             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 2 6 6 0 7 1 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 2 1 , " I d l e T i m e r E l a p s e d T i m e " = 3 5 0 7 8 2 0 , " C u r r e n t P o w e r S t a t e " = 4 }8 �1��0�/tu�.�1 0 	plistinit 	plistInit�0  �/  t 
�-�,�+�*�)�(�'�&�%�$�- 0 
foldername 
folderName�, 0 
backupdisk 
backupDisk�+ 0 
userfolder 
userFolder�* 0 
backuptime 
backupTime�) 0 thecontents theContents�( 0 thevalue theValue�' 0 welcome  �& 	0 reply  �% 0 backuptimes backupTimes�$ 0 selectedtime selectedTimeu F��#��"�!� ����������������1�������
H�	�����aehu������� �����������������������������������# 0 itexists itExists
�" 
plif
�! 
pcnt
�  
valL� 0 
appversion 
AppVersion�  0 foldertobackup FolderToBackup� 0 backupdrive BackupDrive�  0 computerfolder ComputerFolder� 0 scheduledtime ScheduledTime� 0 nsfw NSFW� 0 notifications Notifications� 0 	initmenus 	initMenus� .0 getcomputeridentifier getComputerIdentifier�  ��� 0 setfocus setFocus
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� afdrcusr
� .earsffdralis        afdr
�
 
prmp
�	 .sysostflalis    ��� null
� 
TEXT�  �  � 0 killapp killApp
� 
psxp
� .gtqpchltns    @   @ ns  � � �  <
�� 
kocl
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null
�� 
plii
�� 
insh
�� 
kind�� �� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 machinefolder machineFolder�� 0 intervaltime intervalTime�� 0 diskinit diskInit�.�jE�O*�b  l+ e  X� J*�b  /�,E�O��,E�O��,Ec   O��,E�O��,E�O��,E�O��,E�O��,Ec  	O��,Ec  
UO*j+ Y*j+ E�O� �a n)j+ Oa E�O�a b  a _ a a kva ka  a ,E�Oa j E�O *a a l  a !&E�W X " #)j+ $O�a %,E�O�a %,E�Oa &a 'a (mvE�O�a a )l *kva !&E�O�a +  
a ,E�Y #�a -  
a .E�Y �a /  
a 0E�Y hoUO�A*a 1�a 2a 3b  la 4 5%*a 1a 6a 7*6a 2a 8a !a 3a 9�b   a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a ;�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a <�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a =�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a >�a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a ?�b  	a :a : 5O*a 1a 6a 7*6a 2a 8a !a 3a @�b  
a :a : 5UUO�E` AO�E` BO�E` CO�E` DO*j+ E9 ��3����vw���� 0 diskinit diskInit��  ��  v ������ 0 msg  �� 	0 reply  w =��������������������be��������o��������� "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit��  ���� 0 setfocus setFocus
�� 
cobj
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  *fk+ Y ��n)j+ Ob  �.E�O��b  �����lv�l� a ,E�O�a   
*j+ Y [b  b   Lb  �.E�Ob  
e  4b  	e  ��a l Y b  	f  a �a l Y hY hY ho: �������xy���� 0 freespaceinit freeSpaceInit�� ��z�� z  ���� 	0 again  ��  x �������� 	0 again  �� 	0 reply  �� 0 msg  y �����������������������������������������(+�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes�� 0 
backupinit 
backupInit��  ���� 0 setfocus setFocus
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp
�� 
cobj
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  
*j+ Y ��n)j+ O�f  ��b  �����lv�l� a ,E�Y ,�e  %a �b  ���a a lv�l� a ,E�Y hO�a   
*j+ Y ]b  b   b  a .E�Y hOb  
e  4b  	e  ��a l Y b  	f  a �a l Y hY ho; ��2���{|��� 0 
backupinit 
backupInit��  �  {  | ���EJ�X�����V��cj��v����������
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  � 0 itexists itExists
� 
kocl
� 
cfol
� 
insh
� 
prdt
� 
pnam� 
� .corecrel****      � null� 0 machinefolder machineFolder
� 
cdis� 0 backupfolder backupFolder� 0 latestfolder latestFolder� 
0 backup  � �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` UO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` UO*j+ < ����}~�� 0 tidyup tidyUp�  �  } ��������� 0 bf bF� 0 creationdates creationDates� 0 theoldestdate theOldestDate� 0 j  � 0 i  � 0 thisdate thisDate�  0 foldertodelete folderToDelete� 0 todelete toDelete~ ��������������IK�i�l��{~�
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  
� 
cdis
� 
cfol� 0 machinefolder machineFolder
� 
cobj
� 
ascd
� .corecnte****       ****
� 
pnam
� 
TEXT
� 
psxp
� .sysoexecTEXT���     TEXT
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr� 0 freespaceinit freeSpaceInit� �*��/E�O� �*��/��/��/E�O��-�,E�O��k/E�OkE�O 9l�j 
kh ��/E�O*�/�,� �� �E�O�E�Y hY h[OY��O��k/�&E�O��,�&E�O�%a %E�O�j Ob  b   Tb  
e  Fb  	e  a a a l Okj Y #b  	f  a a a l Okj Y hY hY hO*ek+ U= ������� 
0 backup  �  �   
����������� 0 t  � 0 x  � "0 containerfolder containerFolder� 0 
foldername 
folderName� 0 d  � 0 duration  � 0 msg  � 0 c  � 0 oldestfolder oldestFolder� 0 todelete toDelete� c����	��~�}�|�{�z�y�x�w�v�u�t�s�r��q�p�o�n�m�l"�k,/�jDFHZ\^�i�h�g�f�e�d��������c����Legik�b�a������	 #68;JMWZc�������������� 0 gettimestamp getTimestamp� 0 
updateicon 
updateIcon
� .sysodelanull��� ��� nmbr
� 
psxf�~ 0 sourcefolder sourceFolder
�} 
TEXT
�| 
cfol
�{ 
pnam
�z 
kocl
�y 
insh�x 0 backupfolder backupFolder
�w 
prdt�v 
�u .corecrel****      � null�t (0 activesourcefolder activeSourceFolder
�s 
cdis�r 	0 drive  �q 0 machinefolder machineFolder�p (0 activebackupfolder activeBackupFolder
�o .misccurdldt    ��� null
�n 
time�m 0 	starttime 	startTime
�l 
appr
�k .sysonotfnull��� ��� TEXT�j "0 destinationdisk destinationDisk
�i .sysoexecTEXT���     TEXT�h  �g  �f 0 endtime endTime�e 0 getduration getDuration
�d 
cobj�c 0 killapp killApp
�b 
alis
�a 
psxp�u*ek+  E�OeEc  O)�k+ Okj O�S*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e i*j a ,E` Ob  b   Hb  
e  :b  	e  a a a l Y b  	f  a a a l Y hY hY hO_ a  %_ %a !%�%a "%�&E�O a #�%a $%�%a %%j &W X ' (hOfEc  OfEc  OeEc  Ob  b   �*j a ,E` )O)j+ *E�Ob  a +.E�Ob  
e  Tb  	e   a ,�%a -%�%a a .l Okj Y )b  	f  a /�%a 0%a a 1l Okj Y hY hO)a 2k+ Y hOb  e  
)j+ 3Y hYrb  f g_ a 4%_ %a 5%�%a 6%�%a 7%�&E�O_ a 8%_ %a 9%�%a :%�&E�Ob  b   p*j a ,E` Ob  a +.E�Ob  
e  Hb  	e  �a b  l Okj Y %b  	f  a ;a b  l Okj Y hY hY hO a <�%a =%�%a >%�%a ?%j &W X ' (hOfEc  OfEc  OeEc  O*�/a @&a +-jv T��/�&E�O�a A,�&E�Oa B�%a C%E�O�j &Ob  b  *j a ,E` )O)j+ *E�Ob  a +.E�Ob  
e  �a D  Tb  	e   a E�%a F%�%a a Gl Okj Y )b  	f  a H�%a I%a a Jl Okj Y hY Qb  	e   a K�%a L%�%a a Ml Okj Y )b  	f  a N�%a O%a a Pl Okj Y hOb  	e  a Qa a Rl Y b  	f  a Sa a Tl Y hY hO)a Uk+ Y hY
b  b   �*j a ,E` )O)j+ *E�Ob  a +.E�Ob  
e  ��a D  Tb  	e   a V�%a W%�%a a Xl Okj Y )b  	f  a Y�%a Z%a a [l Okj Y hY Qb  	e   a \�%a ]%�%a a ^l Okj Y )b  	f  a _�%a `%a a al Okj Y hY hY hO)a bk+ Ob  e  
)j+ 3Y hY hU> �`	�_�^���]�` 0 itexists itExists�_ �\��\ �  �[�Z�[ 0 
objecttype 
objectType�Z 
0 object  �^  � �Y�X�Y 0 
objecttype 
objectType�X 
0 object  � 	H	 �W�V	.�U	>�T
�W 
cdis
�V .coredoexnull���     ****
�U 
file
�T 
cfol�] X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU? �S	N�R�Q���P�S .0 getcomputeridentifier getComputerIdentifier�R  �Q  � �O�N�M�O 0 computername computerName�N "0 strserialnumber strSerialNumber�M  0 identifiername identifierName� �L�K	\�J	g
�L .sysosigtsirr   ��� null
�K 
sicn
�J .sysoexecTEXT���     TEXT�P *j  �,E�O�j E�O��%�%E�O�@ �I	p�H�G���F�I 0 gettimestamp getTimestamp�H �E��E �  �D�D 0 isfolder isFolder�G  � �C�B�A�@�?�>�=�<�;�:�9�8�7�6�5�4�C 0 isfolder isFolder�B 0 y  �A 0 m  �@ 0 d  �? 0 t  �> 0 ty tY�= 0 tm tM�< 0 td tD�; 0 tt tT�: 
0 tml tML�9 
0 tdl tDL�8 0 timestr timeStr�7 0 pos  �6 0 h  �5 0 s  �4 0 	timestamp  � �3�2�1�0�/�.�-�,�+�*�)�(�'�&�%	�	��$�#�"	��!� ��

5
_
a
w
�3 
Krtn
�2 
year�1 0 y  
�0 
mnth�/ 0 m  
�. 
day �- 0 d  
�, 
time�+ 0 t  �* 
�) .misccurdldt    ��� null
�( 
long
�' 
TEXT
�& .corecnte****       ****
�% 
nmbr
�$ 
tstr
�# misccura
�" 
psof
�! 
psin�  
� .sysooffslong    ��� null
� 
cha �F�*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�A �
������� 0 comparesizes compareSizes� ��� �  ��� 
0 source  � 0 destination  �  � �������������
�	��� 
0 source  � 0 destination  � 0 fit  � 0 
templatest 
tempLatest� $0 latestfoldersize latestFolderSize� 0 c  � 0 l  � 0 md  � 0 	cachesize 	cacheSize� 0 logssize logsSize� 0 mdsize mdSize� 
0 buffer  �
 (0 adjustedfoldersize adjustedFolderSize�	 0 
foldersize 
folderSize� 0 	freespace 	freeSpace� 0 temp  �  ��
��
����� ��
�
�
�
��
�
���������������(*CE^`��
� 
psxf� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� afdrdlib
� 
from
� fldmfldu
�  .earsffdralis        afdr
�� 
psxp
�� .sysoexecTEXT���     TEXT
�� misccura�� �� d
�� .sysorondlong        doub
�� 
cdis
�� 
frsp��eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O��%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�Ob  f  /a �%a %j E�Oa  �a !a  j Ua !E�Y hUOb  f  ,��E�O�j ��l E�Y hO��� fE�Y hY b  e  ��� fE�Y hY hO�B ������������� 0 getduration getDuration��  ��  � ���� 0 duration  � �������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
�� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�C ������������ 0 setfocus setFocus��  ��  � ������ 0 processlist ProcessList�� 0 thepid ThePID� )���������
�� 
prcs
�� 
pnam
�� 
idux
�� 
pisf�  �� 6� 2*�-�,E�O�� #*��/�,E�O� e*�-�,�[�,\Z�81FUY hUD ��/���������� 0 updateplist updatePlist�� ����� �  ������ 0 tempitem tempItem�� 0 	tempvalue 	tempValue��  � 
���������������������� 0 tempitem tempItem�� 0 	tempvalue 	tempValue�� 0 	tempplist 	tempPlist�� 0 thecontents theContents�� 0 thevalue theValue�� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk��  0 computerfolder ComputerFolder�� 0 
backuptime 
backupTime�� 0 scheduledtime ScheduledTime� G��������������������������
�� 
plif
�� 
plii
�� 
valL
�� 
pcnt��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive��  0 computerfolder ComputerFolder�� 0 scheduledtime ScheduledTime�� 0 nsfw NSFW�� 0 notifications Notifications�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 machinefolder machineFolder�� r� *�b  /E�O� 
�*�/�,FUUO� @*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�O��,Ec  	O��,Ec  
UO�E�O�E�O�E�O�E�E ������������� 0 killapp killApp��  ��  � ����� 0 processlist ProcessList� 0 thepid ThePID� ��������
� 
prcs
� 
pnam
� 
idux
� .sysoexecTEXT���     TEXT�� (� $*�-�,E�O�� *��/�,E�O�%j Y hUF �������� $0 menuneedsupdate_ menuNeedsUpdate_� ��� �  �
� 
cmnu�  �  � �� 0 	makemenus 	makeMenus� )j+  G �������� 0 	makemenus 	makeMenus�  �  � ���� 0 i  � 0 	this_item  � 0 thismenuitem thisMenuItem� "�����������,:=KY\jx{������������  0 removeallitems removeAllItems
� 
cobj
� 
nmbr
� 
TEXT
� misccura� 0 
nsmenuitem 
NSMenuItem� 	0 alloc  � J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� 0 additem_ addItem_� 0 
settarget_ 
setTarget_� 
� 
bool� 0 separatoritem separatorItem�?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY��H �������� 0 	initmenus 	initMenus�  �  �  �  � $(+>BFJMZ^bfi� � �b  	e  :b  
e  ������vEc  Y b  
f  ������vEc  Y hY Ob  	f  Db  
e  ������vEc  Y 'b  
f  a a a a a �vEc  Y hY hI �p������  0 backuphandler_ backupHandler_� ��� �  �� 
0 sender  �  � ��� 
0 sender  � 0 msg  � �����������
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� �b  f  PeEc  Ob  
e  :b  	e  ���l Okj Y b  	f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  
e  .b  	e  ���l Y b  	f  ���l Y hY hY hJ �������� .0 backupandquithandler_ backupAndQuitHandler_� ��� �  �� 
0 sender  �  � ���� 
0 sender  � 	0 reply  � 0 msg  � ���������~�}�|.1�{�z@CL�ymwz� 0 setfocus setFocus�  ��
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt�~ 
�} .sysodlogaskr        TEXT
�| 
bhit
�{ .sysonotfnull��� ��� TEXT
�z .sysodelanull��� ��� nmbr
�y 
cobj� �)j+  Ob  f  ��n��b  �����lv�l� �,E�O��  \eEc  OeEc  Ob  
e  >b  	e  ���l Okj Y !b  	f  a �a l Okj Y hY hY �a   fEc  Y hoY Yb  e  Nb  a .E�Ob  
e  4b  	e  ��a l Y b  	f  a �a l Y hY hY hK �x��w�v���u�x  0 nsfwonhandler_ nsfwOnHandler_�w �t��t �  �s�s 
0 sender  �v  � �r�r 
0 sender  � ���q������p������q 0 updateplist updatePlist�p �u ReEc  	O�Ec  O*�b  	l+ Ob  
e  ������vEc  Y b  
f  ������vEc  Y hL �o��n�m���l�o "0 nsfwoffhandler_ nsfwOffHandler_�n �k��k �  �j�j 
0 sender  �m  � �i�i 
0 sender  � ���h����g"�h 0 updateplist updatePlist�g �l RfEc  	O�Ec  O*�b  	l+ Ob  
e  ������vEc  Y b  
f  ������vEc  Y hM �f)�e�d���c�f 00 notificationonhandler_ notificationOnHandler_�e �b��b �  �a�a 
0 sender  �d  � �`�` 
0 sender  � 6�_EIMQT�^aeimp�_ 0 updateplist updatePlist�^ �c JeEc  
O*�b  
l+ Ob  	e  ������vEc  Y b  	f  ������vEc  Y hN �]w�\�[���Z�] 20 notificationoffhandler_ notificationOffHandler_�\ �Y��Y �  �X�X 
0 sender  �[  � �W�W 
0 sender  � ��V������U������V 0 updateplist updatePlist�U �Z JfEc  
O*�b  
l+ Ob  	e  ������vEc  Y b  	f  ������vEc  Y hO �T��S�R���Q�T 0 quithandler_ quitHandler_�S �P��P �  �O�O 
0 sender  �R  � �N�M�N 
0 sender  �M 	0 reply  � �L�K��J�I�H�G���F�E�D�C��B(�L  ���K 0 setfocus setFocus
�J 
appr
�I 
disp�H 0 appicon appIcon
�G 
btns
�F 
dflt�E 
�D .sysodlogaskr        TEXT
�C 
bhit�B 0 killapp killApp�Q {�n)j+ Ob  e  -��b  �����lv�l� �,E�O��  
)j+ Y hY >b  f  3��b  ���a a lv�l� �,E�O�a   
)j+ Y hY hoP �A1�@�?���>�A 0 
updateicon 
updateIcon�@ �=��= �  �<�< 0 mode  �?  � �;�:�9�; 0 mode  �: 0 	imagepath 	imagePath�9 	0 image  � =�8�7�6�5IP]�4�3�2�1�0�/
�8 
alis
�7 
rtyp
�6 
ctxt
�5 .earsffdralis        afdr
�4 
psxp
�3 misccura�2 0 nsimage NSImage�1 	0 alloc  �0 20 initwithcontentsoffile_ initWithContentsOfFile_�/ 0 	setimage_ 	setImage_�> P��  *�)��l �%/E�Y ��  *�)��l �%/E�Y hO��,E�O��,j+ �k+ E�Ob  �k+ Q �.}�-�,���+�. 0 makestatusbar makeStatusBar�-  �,  � �*�* 0 bar  � �)�(�'��&�%�$��#��"�!� 
�) misccura�( 0 nsstatusbar NSStatusBar�' "0 systemstatusbar systemStatusBar�& .0 statusitemwithlength_ statusItemWithLength_�% 0 nsmenu NSMenu�$ 	0 alloc  �#  0 initwithtitle_ initWithTitle_�" 0 
updateicon 
updateIcon�! 0 setdelegate_ setDelegate_�  0 setmenu_ setMenu_�+ I��,�,E�O��k+ Ec  O��,j+ �k+ Ec  O)�k+ 
Ob  )k+ Ob  b  k+ R �������� 0 runonce runOnce�  �  �  � �����
� .sysodelanull��� ��� nmbr� 0 makestatusbar makeStatusBar� 0 
updateicon 
updateIcon� 0 	plistinit 	plistInit� %kj  O)j+ Okj  O)�k+ Okj  O*j+ S �������
� .miscidlenmbr    ��� null�  �  � �� 0 m  � ��������
� .misccurdldt    ��� null
� 
min � � -
� 
bool� 0 intervaltime intervalTime� 0 	plistinit 	plistInit� � �b  e  �b  b   tb  f  fb  f  F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY b  e  
*j+ Y hY hY hY hO�T �
��	����
�
 .aevtoappnull  �   � ****� k     ��  ��� ���  �	  �  �  � ���� ��� 
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr� 0 appicon appIcon�  0 runonce runOnce� *�)��l �%/E�O*j+ ascr  ��ޭ