FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S g�� ��� 0 thelist theList � J   S f � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   Y \ � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   \ _ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   _ b � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   q q � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   q q � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ����� 0 endtime endTime�   �  � � � l     �~�}�|�~  �}  �|   �  � � � l     �{�z�y�{  �z  �y   �  � � � l     �x � ��x   �   Property assignment    � � � � (   P r o p e r t y   a s s i g n m e n t �  � � � j   q ��w ��w 	0 plist   � b   q � � � � n  q � � � � 1   ~ ��v
�v 
psxp � l  q ~ ��u�t � I  q ~�s � �
�s .earsffdralis        afdr � m   q t�r
�r afdrdlib � �q ��p
�q 
from � m   w z�o
�o fldmfldu�p  �u  �t   � m   � � � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � l     �n�m�l�n  �m  �l   �  � � � j   � ��k ��k 0 initialbackup initialBackup � m   � ��j
�j boovtrue �  � � � j   � ��i ��i 0 manualbackup manualBackup � m   � ��h
�h boovfals �  � � � j   � ��g ��g 0 isbackingup isBackingUp � m   � ��f
�f boovfals �  � � � l     �e�d�c�e  �d  �c   �  � � � j   � ��b ��b "0 messagesmissing messagesMissing � J   � � � �  � � � m   � � � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m   � � � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �    m   � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !  m   � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �a m   � �		 �

 � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�a   �  l     �`�_�^�`  �_  �^    j   � ��]�] *0 messagesencouraging messagesEncouraging J   � �  m   � � �  C o m e   o n !  m   � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r !  m   � � � " N o   p a i n !   N o   p a i n !  m   � � �   0 L e t ' s   d o   t h i s   a s   a   t e a m ! !"! m   � �## �$$  W e   c a n   d o   i t !" %&% m   � �'' �((  F r e e e e e e e e e d o m !& )*) m   � �++ �,, 2 A l t o g e t h e r   o r   n o t   a t   a l l !* -.- m   � �// �00 H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !. 1�\1 m   � �22 �33 H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�\   454 l     �[�Z�Y�[  �Z  �Y  5 676 j   � ��X8�X $0 messagescomplete messagesComplete8 J   � �99 :;: m   � �<< �==  N i c e   o n e !; >?> m   � �@@ �AA * Y o u   a b s o l u t e   l e g   e n d !? BCB m   � �DD �EE  G o o d   l a d !C FGF m   � �HH �II  P e r f i c k !G JKJ m   � �LL �MM  H a p p y   d a y s !K NON m   � �PP �QQ  F r e e e e e e e e e d o m !O RSR m   � �TT �UU H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !S VWV m   � �XX �YY B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !W Z�WZ m   � �[[ �\\ d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�W  7 ]^] l     �V�U�T�V  �U  �T  ^ _`_ j   ��Sa�S &0 messagescancelled messagesCancelleda J   �bb cdc m   � �ee �ff  T h a t ' s   a   s h a m ed ghg m   � �ii �jj  A h   m a n ,   u n l u c k yh klk m   � �mm �nn  O h   w e l ll opo m   � �qq �rr @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e np sts m   � �uu �vv , W e l l   t h a t ' s   a   l e t   d o w nt wxw m   � �yy �zz P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?x {|{ m   �}} �~~  A r r o g a n t| �R m  �� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�R  ` ��� l     �Q�P�O�Q  �P  �O  � ��� j  .�N��N 40 messagesalreadybackingup messagesAlreadyBackingUp� J  +�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  �� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  �� ���  D i c k� ��� m  �� ���  F u c k t a r d� ��� m  �� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  !�� ���  A r r o g a n t� ��� m  !$�� ��� $ C h i l l   t h e   f u c k   o u t� ��M� m  $'�� ���  H o l d   f i r e�M  � ��� l     �L�K�J�L  �K  �J  � ��� j  /5�I��I 0 strawake strAwake� m  /2�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  6<�H��H 0 strsleep strSleep� m  69�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  =G�G��G &0 displaysleepstate displaySleepState� I =D�F��E
�F .sysoexecTEXT���     TEXT� m  =@�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�E  � ��� l     �D�C�B�D  �C  �B  � ��� l     �A�@�?�A  �@  �?  � ��� l     �>�=�<�>  �=  �<  � ��� l     �;���;  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �:���:  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �9���9  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �8�7�6�8  �7  �6  � ��� l     �5���5  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     �4���4  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     �3���3  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i  HK��� I      �2�1�0�2 0 	plistinit 	plistInit�1  �0  � k    ��� ��� q      �� �/��/ 0 
foldername 
folderName� �.��. 0 
backupdisk 
backupDisk� �-��-  0 computerfolder computerFolder� �,�+�, 0 
backuptime 
backupTime�+  � ��� r     ��� m     �*�*  � o      �)�) 0 
backuptime 
backupTime� ��� l   �(�'�&�(  �'  �&  � ��� Z   ����%�� l   ��$�#� =    ��� I    �"��!�" 0 itexists itExists� ��� m    �� �    f i l e� �  o    �� 	0 plist  �   �!  � m    �
� boovtrue�$  �#  � O    E k    D  r    $ n    "	
	 1     "�
� 
pcnt
 4     �
� 
plif o    �� 	0 plist   o      �� 0 thecontents theContents  r   % * n   % ( 1   & (�
� 
valL o   % &�� 0 thecontents theContents o      �� 0 thevalue theValue  l  + +����  �  �    r   + 0 n   + . o   , .��  0 foldertobackup FolderToBackup o   + ,�� 0 thevalue theValue o      �� 0 
foldername 
folderName  r   1 6 n   1 4 o   2 4�� 0 backupdrive BackupDrive o   1 2�� 0 thevalue theValue o      �� 0 
backupdisk 
backupDisk  !  r   7 <"#" n   7 :$%$ o   8 :��  0 computerfolder computerFolder% o   7 8�� 0 thevalue theValue# o      ��  0 computerfolder computerFolder! &'& r   = B()( n   = @*+* o   > @�
�
 0 scheduledtime scheduledTime+ o   = >�	�	 0 thevalue theValue) o      �� 0 
backuptime 
backupTime' ,-, l  C C����  �  �  - .�. l  C C�/0�  / . (log {folderName, backupDisk, backupTime}   0 �11 P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�   m    22�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �%  � k   H�33 454 r   H O676 I   H M��� � .0 getcomputeridentifier getComputerIdentifier�  �   7 o      ����  0 computerfolder computerFolder5 898 l  P P��������  ��  ��  9 :;: O   P �<=< t   T �>?> k   V �@@ ABA I   V [�������� 0 setfocus setFocus��  ��  B CDC l  \ \��������  ��  ��  D EFE r   \ eGHG l  \ cI����I I  \ c����J
�� .sysostflalis    ��� null��  J ��K��
�� 
prmpK m   ^ _LL �MM @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p��  ��  ��  H o      ���� 0 
foldername 
folderNameF NON r   f uPQP c   f sRSR l  f oT����T I  f o����U
�� .sysostflalis    ��� null��  U ��V��
�� 
prmpV m   h kWW �XX R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  S m   o r��
�� 
TEXTQ o      ���� 0 
backupdisk 
backupDiskO YZY l  v v��������  ��  ��  Z [\[ r   v }]^] n   v {_`_ 1   w {��
�� 
psxp` o   v w���� 0 
foldername 
folderName^ o      ���� 0 
foldername 
folderName\ aba r   ~ �cdc n   ~ �efe 1    ���
�� 
psxpf o   ~ ���� 0 
backupdisk 
backupDiskd o      ���� 0 
backupdisk 
backupDiskb ghg l  � ���������  ��  ��  h iji r   � �klk J   � �mm non m   � �pp �qq 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u ro rsr m   � �tt �uu 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u rs v��v m   � �ww �xx , E v e r y   h o u r   o n   t h e   h o u r��  l o      ���� 0 backuptimes backupTimesj yzy r   � �{|{ c   � �}~} J   � � ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  ~ m   � ���
�� 
TEXT| o      ���� 0 selectedtime selectedTimez ��� l  � �������  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l  � ���������  ��  ��  � ���� l  � �������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  ? m   T U����  ��= m   P Q���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ; ��� l  � ���������  ��  ��  � ���� O   ����� O   ����� k   ���� ��� I  �"�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   ���
�� 
plii� ����
�� 
insh�  ;  � �����
�� 
prdt� K  
�� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  �� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ���� 0 
foldername 
folderName��  ��  � ��� I #J�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  '*��
�� 
plii� ����
�� 
insh�  ;  -/� �����
�� 
prdt� K  2D�� ����
�� 
kind� m  58��
�� 
TEXT� ����
�� 
pnam� m  ;>�� ���  B a c k u p D r i v e� �����
�� 
valL� o  ?@���� 0 
backupdisk 
backupDisk��  ��  � ��� I Kr�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  OR��
�� 
plii� ����
�� 
insh�  ;  UW� �����
�� 
prdt� K  Zl�� ����
�� 
kind� m  ]`��
�� 
TEXT� ����
�� 
pnam� m  cf�� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  gh����  0 computerfolder computerFolder��  ��  � ���� I s������
�� .corecrel****      � null��  � ����
�� 
kocl� m  wz��
�� 
plii� ����
�� 
insh�  ;  }� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  S c h e d u l e d T i m e� ����
�� 
valL� o  ���~�~ 0 
backuptime 
backupTime�  ��  ��  � l  � ���}�|� I  � ��{�z�
�{ .corecrel****      � null�z  � �y��
�y 
kocl� m   � ��x
�x 
plif� �w �v
�w 
prdt  K   � � �u�t
�u 
pnam o   � ��s�s 	0 plist  �t  �v  �}  �|  � m   � ��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  �  l ���r�q�p�r  �q  �p    r  ��	 o  ���o�o 0 
foldername 
folderName	 o      �n�n 0 sourcefolder sourceFolder 

 r  �� o  ���m�m 0 
backupdisk 
backupDisk o      �l�l "0 destinationdisk destinationDisk  r  �� o  ���k�k  0 computerfolder computerFolder o      �j�j 0 machinefolder machineFolder  r  �� o  ���i�i 0 
backuptime 
backupTime o      �h�h 0 scheduledtime scheduledTime  I ���g�f
�g .ascrcmnt****      � **** J  ��  o  ���e�e 0 sourcefolder sourceFolder  o  ���d�d "0 destinationdisk destinationDisk  o  ���c�c 0 machinefolder machineFolder  �b  o  ���a�a 0 scheduledtime scheduledTime�b  �f   !"! l ���`�_�^�`  �_  �^  " #�]# I  ���\�[�Z�\ 0 diskinit diskInit�[  �Z  �]  � $%$ l     �Y�X�W�Y  �X  �W  % &'& l     �V�U�T�V  �U  �T  ' ()( l     �S�R�Q�S  �R  �Q  ) *+* l     �P�O�N�P  �O  �N  + ,-, l     �M�L�K�M  �L  �K  - ./. l     �J01�J  0 H B Function to detect if the selected hard drive is connected or not   1 �22 �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t/ 343 l     �I56�I  5 T N This only happens once a hard drive has been selected and provides a reminder   6 �77 �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r4 898 i  LO:;: I      �H�G�F�H 0 diskinit diskInit�G  �F  ; Z     �<=�E>< l    	?�D�C? =     	@A@ I     �BB�A�B 0 itexists itExistsB CDC m    EE �FF  d i s kD G�@G o    �?�? "0 destinationdisk destinationDisk�@  �A  A m    �>
�> boovtrue�D  �C  = I    �=H�<�= 0 freespaceinit freeSpaceInitH I�;I m    �:
�: boovfals�;  �<  �E  > k    �JJ KLK I    �9�8�7�9 0 setfocus setFocus�8  �7  L MNM r    $OPO n    "QRQ 3     "�6
�6 
cobjR o     �5�5 "0 messagesmissing messagesMissingP o      �4�4 0 msg  N STS r   % 5UVU l  % 3W�3�2W n   % 3XYX 1   1 3�1
�1 
bhitY l  % 1Z�0�/Z I  % 1�.[\
�. .sysodlogaskr        TEXT[ o   % &�-�- 0 msg  \ �,]^
�, 
btns] J   ' +__ `a` m   ' (bb �cc  C a n c e l   B a c k u pa d�+d m   ( )ee �ff  O K�+  ^ �*g�)
�* 
dfltg m   , -�(�( �)  �0  �/  �3  �2  V o      �'�' 	0 reply  T h�&h Z   6 �ij�%ki l  6 9l�$�#l =   6 9mnm o   6 7�"�" 	0 reply  n m   7 8oo �pp  O K�$  �#  j I   < A�!� ��! 0 diskinit diskInit�   �  �%  k Z   D �qr��q l  D Os��s E   D Otut o   D I�� &0 displaysleepstate displaySleepStateu o   I N�� 0 strawake strAwake�  �  r k   R �vv wxw r   R [yzy n   R Y{|{ 3   W Y�
� 
cobj| o   R W�� &0 messagescancelled messagesCancelledz o      �� 0 msg  x }�} Z   \ �~��~ l  \ c���� =   \ c��� o   \ a�� 0 notifications  � m   a b�
� boovtrue�  �   Z   f ������ l  f m���� =   f m��� o   f k�� 0 nsfw  � m   k l�

�
 boovtrue�  �  � I  p y�	��
�	 .sysonotfnull��� ��� TEXT� o   p q�� 0 msg  � ���
� 
appr� m   r u�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  � ��� l  | ����� =   | ���� o   | ��� 0 nsfw  � m   � ��
� boovfals�  �  � ��� I  � �� ��
�  .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  �  �  �  �  �  �  �  �&  9 ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  PS��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � Z     ������� l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    ��� ��� I    �������� 0 setfocus setFocus��  ��  � ��� l   ��������  ��  ��  � ��� Z    M������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r     0��� l    .������ n     .��� 1   , .��
�� 
bhit� l    ,������ I    ,����
�� .sysodlogaskr        TEXT� m     !�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   " &�� ��� m   " #�� ���  Y e s� ���� m   # $�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   ' (���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  � ��� l  3 6������ =   3 6��� o   3 4���� 	0 again  � m   4 5��
�� boovtrue��  ��  � ���� r   9 I��� l  9 G������ n   9 G��� 1   E G��
�� 
bhit� l  9 E������ I  9 E����
�� .sysodlogaskr        TEXT� m   9 :�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   ; ?�� ��� m   ; <�� ���  Y e s� ���� m   < =�� ���  C a n c e l   B a c k u p��  � �� ��
�� 
dflt  m   @ A���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  ��  ��  �  l  N N��������  ��  ��   �� Z   N ��� l  N S���� =   N S	 o   N O���� 
0 reply1  	 m   O R

 �  Y e s��  ��   I   V [�������� 0 tidyup tidyUp��  ��  ��   k   ^ �  Z   ^ {���� l  ^ i���� E   ^ i o   ^ c���� &0 displaysleepstate displaySleepState o   c h���� 0 strawake strAwake��  ��   r   l w n   l u 3   q u��
�� 
cobj o   l q���� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��    l  | |��������  ��  ��   �� Z   | ����� l  | ����� =   | � o   | ����� 0 notifications   m   � ���
�� boovtrue��  ��   Z   � � !"��  l  � �#����# =   � �$%$ o   � ����� 0 nsfw  % m   � ���
�� boovtrue��  ��  ! I  � ���&'
�� .sysonotfnull��� ��� TEXT& o   � ����� 0 msg  ' ��(��
�� 
appr( m   � �)) �** > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  " +,+ l  � �-����- =   � �./. o   � ����� 0 nsfw  / m   � ���
�� boovfals��  ��  , 0��0 I  � ���12
�� .sysonotfnull��� ��� TEXT1 m   � �33 �44 , Y o u   s t o p p e d   b a c k i n g   u p2 ��5��
�� 
appr5 m   � �66 �77 
 W B T F U��  ��  ��  ��  ��  ��  ��  � 898 l     ��������  ��  ��  9 :;: l     �������  ��  �  ; <=< l     �~�}�|�~  �}  �|  = >?> l     �{�z�y�{  �z  �y  ? @A@ l     �x�w�v�x  �w  �v  A BCB l     �uDE�u  D,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   E �FFL   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .C GHG l     �tIJ�t  I g a These folders, if required, are then set to their corresponding global variables declared above.   J �KK �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .H LML i  TWNON I      �s�r�q�s 0 
backupinit 
backupInit�r  �q  O k     �PP QRQ r     STS 4     �pU
�p 
psxfU o    �o�o "0 destinationdisk destinationDiskT o      �n�n 	0 drive  R VWV l   �mXY�m  X ' !log (destinationDisk & "Backups")   Y �ZZ B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )W [\[ l   �l�k�j�l  �k  �j  \ ]^] Z    ,_`�i�h_ l   a�g�fa =    bcb I    �ed�d�e 0 itexists itExistsd efe m    	gg �hh  f o l d e rf i�ci b   	 jkj o   	 
�b�b "0 destinationdisk destinationDiskk m   
 ll �mm  B a c k u p s�c  �d  c m    �a
�a boovfals�g  �f  ` O    (non I   '�`�_p
�` .corecrel****      � null�_  p �^qr
�^ 
koclq m    �]
�] 
cfolr �\st
�\ 
inshs o    �[�[ 	0 drive  t �Zu�Y
�Z 
prdtu K    #vv �Xw�W
�X 
pnamw m     !xx �yy  B a c k u p s�W  �Y  o m    zz�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �i  �h  ^ {|{ l  - -�V�U�T�V  �U  �T  | }~} Z   - d��S�R l  - >��Q�P� =   - >��� I   - <�O��N�O 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ��M� b   / 8��� b   / 4��� o   / 0�L�L "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�K�K 0 machinefolder machineFolder�M  �N  � m   < =�J
�J boovfals�Q  �P  � O   A `��� I  E _�I�H�
�I .corecrel****      � null�H  � �G��
�G 
kocl� m   G H�F
�F 
cfol� �E��
�E 
insh� n   I T��� 4   O T�D�
�D 
cfol� m   P S�� ���  B a c k u p s� 4   I O�C�
�C 
cdis� o   M N�B�B 	0 drive  � �A��@
�A 
prdt� K   U [�� �?��>
�? 
pnam� o   V Y�=�= 0 machinefolder machineFolder�>  �@  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �S  �R  ~ ��� l  e e�<�;�:�<  �;  �:  � ��� O   e ���� k   i �� ��� l  i i�9���9  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y�8�
�8 
cfol� o   u x�7�7 0 machinefolder machineFolder� n   i t��� 4   o t�6�
�6 
cfol� m   p s�� ���  B a c k u p s� 4   i o�5�
�5 
cdis� o   m n�4�4 	0 drive  � o      �3�3 0 backupfolder backupFolder� ��2� l  ~ ~�1���1  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�2  � m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��0�/�.�0  �/  �.  � ��� Z   � ����-�� l  � ���,�+� =   � ���� I   � ��*��)�* 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��(� b   � ���� b   � ���� b   � ���� o   � ��'�' "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��&�& 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�(  �)  � m   � ��%
�% boovfals�,  �+  � O   � ���� I  � ��$�#�
�$ .corecrel****      � null�#  � �"��
�" 
kocl� m   � ��!
�! 
cfol� � ��
�  
insh� o   � ��� 0 backupfolder backupFolder� ���
� 
prdt� K   � ��� ���
� 
pnam� m   � ��� ���  L a t e s t�  �  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �-  � r   � ���� m   � ��
� boovfals� o      �� 0 initialbackup initialBackup� ��� l  � �����  �  �  � ��� O   � ���� k   � ��� ��� r   � ���� n   � ���� 4   � ���
� 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ���
� 
cfol� o   � ��� 0 machinefolder machineFolder� n   � ���� 4   � ���
� 
cfol� m   � ��� ���  B a c k u p s� 4   � �� 
� 
cdis  o   � ��� 	0 drive  � o      �� 0 latestfolder latestFolder� � l  � ���    log (backupFolder)    � $ l o g   ( b a c k u p F o l d e r )�  � m   � ��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  l  � ����
�  �  �
   �	 I   � ����� 
0 backup  �  �  �	  M 	
	 l     ����  �  �  
  l     ��� �  �  �     l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.    � �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .  l     ����   � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.    ��   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .  l     �� ��   � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.     �!!P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r . "#" l     ��$%��  $ � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   % �&&>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .# '(' i  X[)*) I      �������� 0 tidyup tidyUp��  ��  * k    z++ ,-, r     ./. 4     ��0
�� 
psxf0 o    ���� "0 destinationdisk destinationDisk/ o      ���� 	0 drive  - 121 l   ��������  ��  ��  2 3��3 O   z454 k   y66 787 l   ��9:��  9 A ;set creationDates to creation date of items of backupFolder   : �;; v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e r8 <=< r    >?> n    @A@ 4    ��B
�� 
cfolB o    ���� 0 machinefolder machineFolderA n    CDC 4    ��E
�� 
cfolE m    FF �GG  B a c k u p sD 4    ��H
�� 
cdisH o    ���� 	0 drive  ? o      ���� 0 bf bF= IJI r    KLK n    MNM 1    ��
�� 
ascdN n    OPO 2   ��
�� 
cobjP o    ���� 0 bf bFL o      ���� 0 creationdates creationDatesJ QRQ r     &STS n     $UVU 4   ! $��W
�� 
cobjW m   " #���� V o     !���� 0 creationdates creationDatesT o      ���� 0 theoldestdate theOldestDateR XYX r   ' *Z[Z m   ' (���� [ o      ���� 0 j  Y \]\ l  + +��������  ��  ��  ] ^_^ Y   + n`��ab��` k   9 icc ded r   9 ?fgf n   9 =hih 4   : =��j
�� 
cobjj o   ; <���� 0 i  i o   9 :���� 0 creationdates creationDatesg o      ���� 0 thisdate thisDatee k��k Z   @ ilm����l l  @ Hn����n >  @ Hopo n   @ Fqrq 1   D F��
�� 
pnamr 4   @ D��s
�� 
cobjs o   B C���� 0 i  p m   F Gtt �uu  L a t e s t��  ��  m Z   K evw����v l  K Nx����x A   K Nyzy o   K L���� 0 thisdate thisDatez o   L M���� 0 theoldestdate theOldestDate��  ��  w k   Q a{{ |}| r   Q T~~ o   Q R���� 0 thisdate thisDate o      ���� 0 theoldestdate theOldestDate} ��� r   U X��� o   U V���� 0 i  � o      ���� 0 j  � ��� l  Y Y������  � " log (item j of backupFolder)   � ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )� ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  a m   . /���� b I  / 4�����
�� .corecnte****       ****� o   / 0���� 0 creationdates creationDates��  ��  _ ��� l  o o��������  ��  ��  � ��� l  o o������  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� I  o y�����
�� .coredeloobj        obj � n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  � ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l   z z������  � � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� I   z �������� 0 setfocus setFocus��  ��  � ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
bhit� l  � ������� I  � �����
�� .sysodlogaskr        TEXT� m   � ��� ���B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .� ����
�� 
btns� J   � ��� ��� m   � ��� ���  E m p t y   T r a s h� ���� m   � ��� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   � ����� ��  ��  ��  ��  ��  � o      ���� 
0 reply2  � ��� Z   ������� l  � ������� =   � ���� o   � ����� 
0 reply2  � m   � ��� ���  E m p t y   T r a s h��  ��  � I  � ������
�� .fndremptnull��� ��� obj � l  � ������ 1   � ��~
�~ 
trsh��  �  ��  ��  � Z   ����}�|� l  � ���{�z� E   � ���� o   � ��y�y &0 displaysleepstate displaySleepState� o   � ��x�x 0 strawake strAwake�{  �z  � k   ��� ��� r   � ���� n   � ���� 3   � ��w
�w 
cobj� o   � ��v�v &0 messagescancelled messagesCancelled� o      �u�u 0 msg  � ��t� Z   ����s�r� l  � ���q�p� =   � ���� o   � ��o�o 0 notifications  � m   � ��n
�n boovtrue�q  �p  � Z   �����m� l  � ���l�k� =   � ���� o   � ��j�j 0 nsfw  � m   � ��i
�i boovtrue�l  �k  � I  � ��h��
�h .sysonotfnull��� ��� TEXT� o   � ��g�g 0 msg  � �f��e
�f 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�e  � ��� l  � ���d�c� =   � ���� o   � ��b�b 0 nsfw  � m   � ��a
�a boovfals�d  �c  � ��`� I  ��_��
�_ .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �^��]
�^ 
appr� m   � �� �   
 W B T F U�]  �`  �m  �s  �r  �t  �}  �|  �  l �\�[�Z�\  �[  �Z    I  �Y�X�Y 0 freespaceinit freeSpaceInit �W m  �V
�V boovtrue�W  �X   �U Z  y	�T�S l #
�R�Q
 E  # o  �P�P &0 displaysleepstate displaySleepState o  "�O�O 0 strawake strAwake�R  �Q  	 Z  &u�N�M l &-�L�K =  &- o  &+�J�J 0 notifications   m  +,�I
�I boovtrue�L  �K   Z  0q�H l 07�G�F =  07 o  05�E�E 0 nsfw   m  56�D
�D boovtrue�G  �F   k  :M  I :G�C
�C .sysonotfnull��� ��� TEXT m  := � � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . . �B�A
�B 
appr m  @C   �!! 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�A   "�@" I HM�?#�>
�? .sysodelanull��� ��� nmbr# m  HI�=�= �>  �@   $%$ l PW&�<�;& =  PW'(' o  PU�:�: 0 nsfw  ( m  UV�9
�9 boovfals�<  �;  % )�8) k  Zm** +,+ I Zg�7-.
�7 .sysonotfnull��� ��� TEXT- m  Z]// �00 t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n. �61�5
�6 
appr1 m  `c22 �33 
 W B T F U�5  , 4�44 I hm�35�2
�3 .sysodelanull��� ��� nmbr5 m  hi�1�1 �2  �4  �8  �H  �N  �M  �T  �S  �U  5 m    66�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ( 787 l     �0�/�.�0  �/  �.  8 9:9 l     �-�,�+�-  �,  �+  : ;<; l     �*�)�(�*  �)  �(  < =>= l     �'�&�%�'  �&  �%  > ?@? l     �$�#�"�$  �#  �"  @ ABA l     �!CD�!  C M G Function that carries out the backup process and is split in 2 parts.    D �EE �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  B FGF l     � HI�   H*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   I �JJH   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .G KLK l     �MN�  M � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   N �OO   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .L PQP i  \_RSR I      ���� 
0 backup  �  �  S k    xTT UVU q      WW �X� 0 t  X �Y� 0 x  Y ��� "0 containerfolder containerFolder�  V Z[Z r     \]\ I     �^�� 0 gettimestamp getTimestamp^ _�_ m    �
� boovtrue�  �  ] o      �� 0 t  [ `a` l  	 	����  �  �  a bcb r   	 ded m   	 
�
� boovtruee o      �� 0 isbackingup isBackingUpc fgf l   ����  �  �  g hih I    �
�	��
 (0 animatemenubaricon animateMenuBarIcon�	  �  i jkj l   ����  �  �  k lml I    �n�� 0 	stopwatch  n o�o m    pp �qq 
 s t a r t�  �  m rsr l   �� ���  �   ��  s tut O   ovwv k   "nxx yzy l  " "��{|��  { f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   | �}} �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e dz ~~ l  " "������  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p ��� r   " *��� c   " (��� 4   " &���
�� 
psxf� o   $ %���� 0 sourcefolder sourceFolder� m   & '��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   + 3��� n   + 1��� 1   / 1��
�� 
pnam� 4   + /���
�� 
cfol� o   - .���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  4 4������  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  4 4��������  ��  ��  � ��� Z   4 �������� l  4 ;������ =   4 ;��� o   4 9���� 0 initialbackup initialBackup� m   9 :��
�� boovfals��  ��  � k   > ��� ��� I  > L�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   @ A��
�� 
cfol� ����
�� 
insh� o   B C���� 0 backupfolder backupFolder� �����
�� 
prdt� K   D H�� �����
�� 
pnam� o   E F���� 0 t  ��  ��  � ��� l  M M��������  ��  ��  � ��� r   M W��� c   M S��� 4   M Q���
�� 
psxf� o   O P���� 0 sourcefolder sourceFolder� m   Q R��
�� 
TEXT� o      ���� (0 activesourcefolder activeSourceFolder� ��� r   X b��� 4   X ^���
�� 
cfol� o   Z ]���� (0 activesourcefolder activeSourceFolder� o      ���� (0 activesourcefolder activeSourceFolder� ��� l  c c������  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   c |��� n   c x��� 4   u x���
�� 
cfol� o   v w���� 0 t  � n   c u��� 4   p u���
�� 
cfol� o   q t���� 0 machinefolder machineFolder� n   c p��� 4   k p���
�� 
cfol� m   l o�� ���  B a c k u p s� 4   c k���
�� 
cdis� o   g j���� 	0 drive  � o      ���� (0 activebackupfolder activeBackupFolder� ��� l  } }������  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ���� I  } ������
�� .corecrel****      � null��  � ����
�� 
kocl� m    ���
�� 
cfol� ����
�� 
insh� o   � ����� (0 activebackupfolder activeBackupFolder� �����
�� 
prdt� K   � ��� �����
�� 
pnam� o   � ����� 0 
foldername 
folderName��  ��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �������  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � �������  � D > This means that only new or updated files will be copied over   � ��� |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r� ��� l  � �������  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	� ��� l  � �������  �   Original sync code    � ��� (   O r i g i n a l   s y n c   c o d e  � ��� l  � �������  � z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   � ��� �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "� ��� l  � ���������  ��  ��  � � � l  � �����     Original code    �    O r i g i n a l   c o d e   l  � �����   i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path    � �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h 	
	 l  � �����   x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.    � �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .
  l  � �����   p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.    � �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .  l  � �����   � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.    ��   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .  l  � �����   � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.    �v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .  l  � ���������  ��  ��     l  � ���������  ��  ��    !��! Z   �n"#$��" l  � �%����% =   � �&'& o   � ����� 0 initialbackup initialBackup' m   � ���
�� boovtrue��  ��  # k   ��(( )*) r   � �+,+ n   � �-.- 1   � ���
�� 
time. l  � �/����/ I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  , o      ���� 0 	starttime 	startTime* 010 Z   � �23����2 l  � �4����4 E   � �565 o   � ����� &0 displaysleepstate displaySleepState6 o   � ����� 0 strawake strAwake��  ��  3 Z   � �78����7 l  � �9����9 =   � �:;: o   � ����� 0 notifications  ; m   � ���
�� boovtrue��  ��  8 Z   � �<=>��< l  � �?����? =   � �@A@ o   � ����� 0 nsfw  A m   � ���
�� boovtrue��  ��  = I  � ���BC
�� .sysonotfnull��� ��� TEXTB m   � �DD �EE � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .C ��F��
�� 
apprF m   � �GG �HH 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  > IJI l  � �K����K =   � �LML o   � ����� 0 nsfw  M m   � ���
�� boovfals��  ��  J N�N I  � ��~OP
�~ .sysonotfnull��� ��� TEXTO m   � �QQ �RR \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m eP �}S�|
�} 
apprS m   � �TT �UU 
 W B T F U�|  �  ��  ��  ��  ��  ��  1 VWV l   �{�z�y�{  �z  �y  W XYX l   �x�w�v�x  �w  �v  Y Z[Z l   �u�t�s�u  �t  �s  [ \]\ l   �r�q�p�r  �q  �p  ] ^_^ r   `a` c   bcb l  d�o�nd b   efe b   ghg b   iji b   klk b   mnm o   �m�m "0 destinationdisk destinationDiskn m  oo �pp  B a c k u p s /l o  
�l�l 0 machinefolder machineFolderj m  qq �rr  / L a t e s t /h o  �k�k 0 
foldername 
folderNamef m  ss �tt  /�o  �n  c m  �j
�j 
TEXTa o      �i�i 0 d  _ uvu l �hwx�h  w A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   x �yy v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "v z{z l �g�f�e�g  �f  �e  { |}| Q  9~�~ I 0�d��c
�d .sysoexecTEXT���     TEXT� b  ,��� b  (��� b  &��� b  "��� m   �� ���$ r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S a n g h a n i . C a n a r y / '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   '� o   !�b�b 0 sourcefolder sourceFolder� m  "%�� ���  '   '� o  &'�a�a 0 d  � m  (+�� ���  / '�c   R      �`�_�^
�` .ascrerr ****      � ****�_  �^  � l 88�]�\�[�]  �\  �[  } ��� l ::�Z�Y�X�Z  �Y  �X  � ��� l ::�W�V�U�W  �V  �U  � ��� l ::�T�S�R�T  �S  �R  � ��� l ::�Q�P�O�Q  �P  �O  � ��� l ::�N�M�L�N  �M  �L  � ��� r  :A��� m  :;�K
�K boovfals� o      �J�J 0 isbackingup isBackingUp� ��� r  BI��� m  BC�I
�I boovfals� o      �H�H 0 manualbackup manualBackup� ��� l JJ�G�F�E�G  �F  �E  � ��� Z  J����D�C� l JU��B�A� E  JU��� o  JO�@�@ &0 displaysleepstate displaySleepState� o  OT�?�? 0 strawake strAwake�B  �A  � k  X��� ��� r  Xe��� n  Xa��� 1  ]a�>
�> 
time� l X]��=�<� I X]�;�:�9
�; .misccurdldt    ��� null�:  �9  �=  �<  � o      �8�8 0 endtime endTime� ��� r  fm��� n fk��� I  gk�7�6�5�7 0 getduration getDuration�6  �5  �  f  fg� o      �4�4 0 duration  � ��� r  ny��� n  nw��� 3  sw�3
�3 
cobj� o  ns�2�2 $0 messagescomplete messagesComplete� o      �1�1 0 msg  � ��� Z  z����0�/� l z���.�-� =  z���� o  z�,�, 0 notifications  � m  ��+
�+ boovtrue�.  �-  � Z  ������*� l ����)�(� =  ����� o  ���'�' 0 nsfw  � m  ���&
�& boovtrue�)  �(  � k  ���� ��� I ���%��
�% .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���$�$ 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ���#�# 0 msg  � �"��!
�" 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�!  � �� � I �����
� .sysodelanull��� ��� nmbr� m  ���� �  �   � ��� l ������ =  ����� o  ���� 0 nsfw  � m  ���
� boovfals�  �  � ��� k  ���� ��� I �����
� .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���� 0 duration  � m  ���� ���    m i n u t e s ! 
� ���
� 
appr� m  ���� ���  B a c k e d   u p !�  � ��� I �����
� .sysodelanull��� ��� nmbr� m  ���� �  �  �  �*  �0  �/  � ��� l ������  �  �  � ��� n ����� I  ����
�	� $0 resetmenubaricon resetMenuBarIcon�
  �	  �  f  ���  �D  �C  � ��� l ������  �  �  � ��� Z  �� ��  l ���� =  �� o  ��� �   0 backupthenquit backupThenQuit m  ����
�� boovtrue�  �   I ������
�� .aevtquitnull��� ��� null m  ��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  �  �  �  $  l �	����	 =  �

 o  ������ 0 initialbackup initialBackup m  � ��
�� boovfals��  ��   �� k  j  r  # c  ! l ���� b   b   b   b   b   b    b  !"! o  ���� "0 destinationdisk destinationDisk" m  
## �$$  B a c k u p s /  o  ���� 0 machinefolder machineFolder m  %% �&&  / o  ���� 0 t   m  '' �((  / o  ���� 0 
foldername 
folderName m  )) �**  /��  ��   m   ��
�� 
TEXT o      ���� 0 c   +,+ r  $=-.- c  $;/0/ l $91����1 b  $9232 b  $5454 b  $3676 b  $/898 b  $+:;: o  $'���� "0 destinationdisk destinationDisk; m  '*<< �==  B a c k u p s /9 o  +.���� 0 machinefolder machineFolder7 m  /2>> �??  / L a t e s t /5 o  34���� 0 
foldername 
folderName3 m  58@@ �AA  /��  ��  0 m  9:��
�� 
TEXT. o      ���� 0 d  , BCB I >G��D��
�� .ascrcmnt****      � ****D l >CE����E b  >CFGF m  >AHH �II  b a c k i n g   u p   t o :  G o  AB���� 0 d  ��  ��  ��  C JKJ l HH��������  ��  ��  K LML Z  H�NO����N l HSP����P E  HSQRQ o  HM���� &0 displaysleepstate displaySleepStateR o  MR���� 0 strawake strAwake��  ��  O k  V�SS TUT r  VcVWV n  V_XYX 1  [_��
�� 
timeY l V[Z����Z I V[������
�� .misccurdldt    ��� null��  ��  ��  ��  W o      ���� 0 	starttime 	startTimeU [\[ r  do]^] n  dm_`_ 3  im��
�� 
cobj` o  di���� *0 messagesencouraging messagesEncouraging^ o      ���� 0 msg  \ a��a Z  p�bc����b l pwd����d =  pwefe o  pu���� 0 notifications  f m  uv��
�� boovtrue��  ��  c Z  z�ghi��g l z�j����j =  z�klk o  z���� 0 nsfw  l m  ���
�� boovtrue��  ��  h k  ��mm non I ����pq
�� .sysonotfnull��� ��� TEXTp o  ������ 0 msg  q ��r��
�� 
apprr m  ��ss �tt 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  o u��u I ����v��
�� .sysodelanull��� ��� nmbrv m  ������ ��  ��  i wxw l ��y����y =  ��z{z o  ������ 0 nsfw  { m  ����
�� boovfals��  ��  x |��| k  ��}} ~~ I ������
�� .sysonotfnull��� ��� TEXT� m  ���� ���  B a c k i n g   u p� �����
�� 
appr� m  ���� ��� 
 W B T F U��   ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  ��  ��  ��  M ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� Q  ������ I �������
�� .sysoexecTEXT���     TEXT� b  ����� b  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ���d r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S a n g h a n i . C a n a r y / '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ������ 0 c  � m  ���� ���  '   '� o  ������ 0 sourcefolder sourceFolder� m  ���� ���  '   '� o  ������ 0 d  � m  ���� ���  / '��  � R      ������
�� .ascrerr ****      � ****��  ��  � l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� r  ����� m  ����
�� boovfals� o      ���� 0 isbackingup isBackingUp� ��� r  ����� m  ����
�� boovfals� o      ���� 0 manualbackup manualBackup� ��� l ����������  ��  ��  � ���� Z  �j����� = ���� n  ���� 2  �~
�~ 
cobj� l � ��}�|� c  � ��� 4  ���{�
�{ 
psxf� o  ���z�z 0 c  � m  ���y
�y 
alis�}  �|  � J  �x�x  � k  
`�� ��� l 

�w���w  � % delete folder t of backupFolder   � ��� > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r� ��� l 

�v�u�t�v  �u  �t  � ��� r  
��� c  
��� n  
��� 4  �s�
�s 
cfol� o  �r�r 0 t  � o  
�q�q 0 backupfolder backupFolder� m  �p
�p 
TEXT� o      �o�o 0 oldestfolder oldestFolder� ��� r  ��� c  ��� n  ��� 1  �n
�n 
psxp� o  �m�m 0 oldestfolder oldestFolder� m  �l
�l 
TEXT� o      �k�k 0 oldestfolder oldestFolder� ��� I &�j��i
�j .ascrcmnt****      � ****� l "��h�g� b  "��� m   �� ���  t o   d e l e t e :  � o   !�f�f 0 oldestfolder oldestFolder�h  �g  �i  � ��� l ''�e�d�c�e  �d  �c  � ��� r  '2��� b  '0��� b  ',��� m  '*�� ���  r m   - r f   '� o  *+�b�b 0 oldestfolder oldestFolder� m  ,/�� ���  '� o      �a�a 0 todelete toDelete� ��� I 38�`��_
�` .sysoexecTEXT���     TEXT� o  34�^�^ 0 todelete toDelete�_  � �	 � l 99�]�\�[�]  �\  �[  	  	�Z	 Z  9`		�Y�X	 l 9D	�W�V	 E  9D			 o  9>�U�U &0 displaysleepstate displaySleepState	 o  >C�T�T 0 strawake strAwake�W  �V  	 k  G\		 				 r  GT	
		
 n  GP			 1  LP�S
�S 
time	 l GL	�R�Q	 I GL�P�O�N
�P .misccurdldt    ��� null�O  �N  �R  �Q  	 o      �M�M 0 endtime endTime		 			 r  U\			 n UZ			 I  VZ�L�K�J�L 0 getduration getDuration�K  �J  	  f  UV	 o      �I�I 0 duration  	 			 l ]]�H		�H  	 � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   	 �		 d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "	 			 l ]]�G		�G  	  delay 2   	 �		  d e l a y   2	 		 	 l ]]�F�E�D�F  �E  �D  	  	!	"	! r  ]h	#	$	# n  ]f	%	&	% 3  bf�C
�C 
cobj	& o  ]b�B�B $0 messagescomplete messagesComplete	$ o      �A�A 0 msg  	" 	'	(	' Z  iV	)	*�@�?	) l ip	+�>�=	+ =  ip	,	-	, o  in�<�< 0 notifications  	- m  no�;
�; boovtrue�>  �=  	* k  sR	.	. 	/	0	/ Z  s	1	2�:	3	1 l sx	4�9�8	4 =  sx	5	6	5 o  st�7�7 0 duration  	6 m  tw	7	7 ?�      �9  �8  	2 Z  {�	8	9	:�6	8 l {�	;�5�4	; =  {�	<	=	< o  {��3�3 0 nsfw  	= m  ���2
�2 boovtrue�5  �4  	9 k  ��	>	> 	?	@	? I ���1	A	B
�1 .sysonotfnull��� ��� TEXT	A b  ��	C	D	C b  ��	E	F	E b  ��	G	H	G m  ��	I	I �	J	J  A n d   i n  	H o  ���0�0 0 duration  	F m  ��	K	K �	L	L    m i n u t e ! 
	D o  ���/�/ 0 msg  	B �.	M�-
�. 
appr	M m  ��	N	N �	O	O : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�-  	@ 	P�,	P I ���+	Q�*
�+ .sysodelanull��� ��� nmbr	Q m  ���)�) �*  �,  	: 	R	S	R l ��	T�(�'	T =  ��	U	V	U o  ���&�& 0 nsfw  	V m  ���%
�% boovfals�(  �'  	S 	W�$	W k  ��	X	X 	Y	Z	Y I ���#	[	\
�# .sysonotfnull��� ��� TEXT	[ b  ��	]	^	] b  ��	_	`	_ m  ��	a	a �	b	b  A n d   i n  	` o  ���"�" 0 duration  	^ m  ��	c	c �	d	d    m i n u t e ! 
	\ �!	e� 
�! 
appr	e m  ��	f	f �	g	g  B a c k e d   u p !�   	Z 	h�	h I ���	i�
� .sysodelanull��� ��� nmbr	i m  ���� �  �  �$  �6  �:  	3 Z  �	j	k	l�	j l ��	m��	m =  ��	n	o	n o  ���� 0 nsfw  	o m  ���
� boovtrue�  �  	k k  ��	p	p 	q	r	q I ���	s	t
� .sysonotfnull��� ��� TEXT	s b  ��	u	v	u b  ��	w	x	w b  ��	y	z	y m  ��	{	{ �	|	|  A n d   i n  	z o  ���� 0 duration  	x m  ��	}	} �	~	~    m i n u t e s ! 
	v o  ���� 0 msg  	t �	�
� 
appr	 m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  	r 	��	� I ���	��
� .sysodelanull��� ��� nmbr	� m  ���� �  �  	l 	�	�	� l ��	���	� =  ��	�	�	� o  ���� 0 nsfw  	� m  ���

�
 boovfals�  �  	� 	��		� k  �	�	� 	�	�	� I ��	�	�
� .sysonotfnull��� ��� TEXT	� b  �	�	�	� b  �	�	�	� m  �	�	� �	�	�  A n d   i n  	� o  �� 0 duration  	� m  	�	� �	�	�    m i n u t e s ! 
	� �	��
� 
appr	� m  	�	� �	�	�  B a c k e d   u p !�  	� 	��	� I �	��
� .sysodelanull��� ��� nmbr	� m  �� �  �  �	  �  	0 	�	�	� l � �����   ��  ��  	� 	���	� Z  R	�	�	���	� l $	�����	� =  $	�	�	� o  "���� 0 nsfw  	� m  "#��
�� boovtrue��  ��  	� I '4��	�	�
�� .sysonotfnull��� ��� TEXT	� m  '*	�	� �	�	� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .	� ��	���
�� 
appr	� m  -0	�	� �	�	� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  	� 	�	�	� l 7>	�����	� =  7>	�	�	� o  7<���� 0 nsfw  	� m  <=��
�� boovfals��  ��  	� 	���	� I AN��	�	�
�� .sysonotfnull��� ��� TEXT	� m  AD	�	� �	�	� p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d	� ��	���
�� 
appr	� m  GJ	�	� �	�	� 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  �@  �?  	( 	�	�	� l WW��������  ��  ��  	� 	���	� n W\	�	�	� I  X\�������� $0 resetmenubaricon resetMenuBarIcon��  ��  	�  f  WX��  �Y  �X  �Z  �  � k  cj	�	� 	�	�	� Z  cN	�	�����	� l cn	�����	� E  cn	�	�	� o  ch���� &0 displaysleepstate displaySleepState	� o  hm���� 0 strawake strAwake��  ��  	� k  qJ	�	� 	�	�	� r  q~	�	�	� n  qz	�	�	� 1  vz��
�� 
time	� l qv	�����	� I qv������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 endtime endTime	� 	�	�	� r  �	�	�	� n �	�	�	� I  ���������� 0 getduration getDuration��  ��  	�  f  �	� o      ���� 0 duration  	� 	�	�	� r  ��	�	�	� n  ��	�	�	� 3  ����
�� 
cobj	� o  ������ $0 messagescomplete messagesComplete	� o      ���� 0 msg  	� 	���	� Z  �J	�	�����	� l ��	�����	� =  ��	�	�	� o  ������ 0 notifications  	� m  ����
�� boovtrue��  ��  	� Z  �F	�	���	�	� l ��	�����	� =  ��	�	�	� o  ������ 0 duration  	� m  ��	�	� ?�      ��  ��  	� Z  ��	�	�	���	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� o  ������ 0 msg  	� ��	���
�� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 	���	� I ����
 ��
�� .sysodelanull��� ��� nmbr
  m  ������ ��  ��  	� 


 l ��
����
 =  ��


 o  ������ 0 nsfw  
 m  ����
�� boovfals��  ��  
 
��
 k  ��

 

	
 I ����



�� .sysonotfnull��� ��� TEXT

 b  ��


 b  ��


 m  ��

 �

  A n d   i n  
 o  ������ 0 duration  
 m  ��

 �

    m i n u t e ! 

 ��
��
�� 
appr
 m  ��

 �

  B a c k e d   u p !��  
	 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  ��  ��  ��  	� Z  �F


��
 l ��
����
 =  ��


 o  ������ 0 nsfw  
 m  ����
�� boovtrue��  ��  
 k  

 
 
!
  I ��
"
#
�� .sysonotfnull��� ��� TEXT
" b  
$
%
$ b  

&
'
& b  
(
)
( m  
*
* �
+
+  A n d   i n  
) o  ���� 0 duration  
' m  	
,
, �
-
-    m i n u t e s ! 

% o  
���� 0 msg  
# ��
.��
�� 
appr
. m  
/
/ �
0
0 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
! 
1��
1 I ��
2��
�� .sysodelanull��� ��� nmbr
2 m  ���� ��  ��  
 
3
4
3 l &
5����
5 =  &
6
7
6 o  $���� 0 nsfw  
7 m  $%��
�� boovfals��  ��  
4 
8��
8 k  )B
9
9 
:
;
: I )<��
<
=
�� .sysonotfnull��� ��� TEXT
< b  )2
>
?
> b  ).
@
A
@ m  ),
B
B �
C
C  A n d   i n  
A o  ,-���� 0 duration  
? m  .1
D
D �
E
E    m i n u t e s ! 

= ��
F��
�� 
appr
F m  58
G
G �
H
H  B a c k e d   u p !��  
; 
I��
I I =B��
J��
�� .sysodelanull��� ��� nmbr
J m  =>���� ��  ��  ��  ��  ��  ��  ��  ��  ��  	� 
K
L
K l OO��������  ��  ��  
L 
M
N
M n OT
O
P
O I  PT�������� $0 resetmenubaricon resetMenuBarIcon��  ��  
P  f  OP
N 
Q
R
Q l UU��������  ��  ��  
R 
S��
S Z  Uj
T
U����
T l U\
V����
V =  U\
W
X
W o  UZ����  0 backupthenquit backupThenQuit
X m  Z[��
�� boovtrue��  ��  
U I _f��
Y�
�� .aevtquitnull��� ��� null
Y m  _b
Z
Z                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  ��  ��  ��  ��  ��  ��  ��  w m    
[
[�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  u 
\
]
\ l pp�~�}�|�~  �}  �|  
] 
^�{
^ I  px�z
_�y�z 0 	stopwatch  
_ 
`�x
` m  qt
a
a �
b
b  f i n i s h�x  �y  �{  Q 
c
d
c l     �w�v�u�w  �v  �u  
d 
e
f
e l     �t�s�r�t  �s  �r  
f 
g
h
g l     �q�p�o�q  �p  �o  
h 
i
j
i l     �n�m�l�n  �m  �l  
j 
k
l
k l     �k�j�i�k  �j  �i  
l 
m
n
m l     �h
o
p�h  
o � �-----------------------------------------------------------------------------------------------------------------------------------------------   
p �
q
q - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
n 
r
s
r l     �g
t
u�g  
t � �-----------------------------------------------------------------------------------------------------------------------------------------------   
u �
v
v - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
s 
w
x
w l     �f
y
z�f  
y � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   
z �
{
{ - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
x 
|
}
| l     �e�d�c�e  �d  �c  
} 
~

~ l     �b
�
��b  
� � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   
� �
�
�   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .
 
�
�
� l     �a
�
��a  
� � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   
� �
�
��   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .
� 
�
�
� i  `c
�
�
� I      �`
��_�` 0 itexists itExists
� 
�
�
� o      �^�^ 0 
objecttype 
objectType
� 
��]
� o      �\�\ 
0 object  �]  �_  
� l    W
�
�
�
� O     W
�
�
� Z    V
�
�
��[
� l   
��Z�Y
� =    
�
�
� o    �X�X 0 
objecttype 
objectType
� m    
�
� �
�
�  d i s k�Z  �Y  
� Z   
 
�
��W
�
� I  
 �V
��U
�V .coredoexnull���     ****
� 4   
 �T
�
�T 
cdis
� o    �S�S 
0 object  �U  
� L    
�
� m    �R
�R boovtrue�W  
� L    
�
� m    �Q
�Q boovfals
� 
�
�
� l   "
��P�O
� =    "
�
�
� o     �N�N 0 
objecttype 
objectType
� m     !
�
� �
�
�  f i l e�P  �O  
� 
�
�
� Z   % 7
�
��M
�
� I  % -�L
��K
�L .coredoexnull���     ****
� 4   % )�J
�
�J 
file
� o   ' (�I�I 
0 object  �K  
� L   0 2
�
� m   0 1�H
�H boovtrue�M  
� L   5 7
�
� m   5 6�G
�G boovfals
� 
�
�
� l  : =
��F�E
� =   : =
�
�
� o   : ;�D�D 0 
objecttype 
objectType
� m   ; <
�
� �
�
�  f o l d e r�F  �E  
� 
��C
� Z   @ R
�
��B
�
� I  @ H�A
��@
�A .coredoexnull���     ****
� 4   @ D�?
�
�? 
cfol
� o   B C�>�> 
0 object  �@  
� L   K M
�
� m   K L�=
�= boovtrue�B  
� L   P R
�
� m   P Q�<
�< boovfals�C  �[  
� m     
�
��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  
� "  (string, string) as Boolean   
� �
�
� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n
� 
�
�
� l     �;�:�9�;  �:  �9  
� 
�
�
� l     �8�7�6�8  �7  �6  
� 
�
�
� l     �5�4�3�5  �4  �3  
� 
�
�
� l     �2�1�0�2  �1  �0  
� 
�
�
� l     �/�.�-�/  �.  �-  
� 
�
�
� l     �,
�
��,  
� � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   
� �
�
�Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .
� 
�
�
� l     �+
�
��+  
� P J This means that backups can be identified from what machine they're from.   
� �
�
� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .
� 
�
�
� i  dg
�
�
� I      �*�)�(�* .0 getcomputeridentifier getComputerIdentifier�)  �(  
� k     
�
� 
�
�
� r     	
�
�
� n     
�
�
� 1    �'
�' 
sicn
� l    
��&�%
� I    �$�#�"
�$ .sysosigtsirr   ��� null�#  �"  �&  �%  
� o      �!�! 0 computername computerName
� 
�
�
� r   
 
�
�
� I  
 � 
��
�  .sysoexecTEXT���     TEXT
� m   
 
�
� �
�
� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �  
� o      �� "0 strserialnumber strSerialNumber
� 
�
�
� r    
�
�
� l   
���
� b    
�
�
� b    
�
�
� o    �� 0 computername computerName
� m    
�
� �
�
�    -  
� o    �� "0 strserialnumber strSerialNumber�  �  
� o      ��  0 identifiername identifierName
� 
��
� L    
�
� o    ��  0 identifiername identifierName�  
� 
�
�
� l     ����  �  �  
� 
�
�
� l     ����  �  �  
� 
�
�
� l     ����  �  �  
� 
� 
� l     ����  �  �     l     �
�	��
  �	  �    l     ��   � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.    �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' . 	 l     �
�  
 � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.    �   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .	  i  hk I      ��� 0 gettimestamp getTimestamp � o      �� 0 isfolder isFolder�  �   l   � k    �  l     ��     Date variables    �    D a t e   v a r i a b l e s  r     ) l      � ��  I     ������
�� .misccurdldt    ��� null��  ��  �   ��   K    !! ��"#
�� 
year" o    ���� 0 y  # ��$%
�� 
mnth$ o    ���� 0 m  % ��&'
�� 
day & o    ���� 0 d  ' ��(��
�� 
time( o   	 
���� 0 t  ��   )*) r   * 1+,+ c   * /-.- l  * -/����/ c   * -010 o   * +���� 0 y  1 m   + ,��
�� 
long��  ��  . m   - .��
�� 
TEXT, o      ���� 0 ty tY* 232 r   2 9454 c   2 7676 l  2 58����8 c   2 59:9 o   2 3���� 0 y  : m   3 4��
�� 
long��  ��  7 m   5 6��
�� 
TEXT5 o      ���� 0 ty tY3 ;<; r   : A=>= c   : ??@? l  : =A����A c   : =BCB o   : ;���� 0 m  C m   ; <��
�� 
long��  ��  @ m   = >��
�� 
TEXT> o      ���� 0 tm tM< DED r   B IFGF c   B GHIH l  B EJ����J c   B EKLK o   B C���� 0 d  L m   C D��
�� 
long��  ��  I m   E F��
�� 
TEXTG o      ���� 0 td tDE MNM r   J QOPO c   J OQRQ l  J MS����S c   J MTUT o   J K���� 0 t  U m   K L��
�� 
long��  ��  R m   M N��
�� 
TEXTP o      ���� 0 tt tTN VWV l  R R��������  ��  ��  W XYX l  R R��Z[��  Z U O Append the month or day with a 0 if the string length is only 1 character long   [ �\\ �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n gY ]^] r   R [_`_ c   R Yaba l  R Wc����c I  R W��d��
�� .corecnte****       ****d o   R S���� 0 tm tM��  ��  ��  b m   W X��
�� 
nmbr` o      ���� 
0 tml tML^ efe r   \ eghg c   \ ciji l  \ ak����k I  \ a��l��
�� .corecnte****       ****l o   \ ]���� 0 tm tM��  ��  ��  j m   a b��
�� 
nmbrh o      ���� 
0 tdl tDLf mnm l  f f��������  ��  ��  n opo Z   f uqr����q l  f is����s =   f itut o   f g���� 
0 tml tMLu m   g h���� ��  ��  r r   l qvwv b   l oxyx m   l mzz �{{  0y o   m n���� 0 tm tMw o      ���� 0 tm tM��  ��  p |}| l  v v��������  ��  ��  } ~~ Z   v �������� l  v y������ =   v y��� o   v w���� 
0 tdl tDL� m   w x���� ��  ��  � r   | ���� b   | ���� m   | �� ���  0� o    ����� 0 td tD� o      ���� 0 td tD��  ��   ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  �   Time variables	   � ���     T i m e   v a r i a b l e s 	� ��� l  � �������  �  	 Get hour   � ���    G e t   h o u r� ��� r   � ���� n   � ���� 1   � ���
�� 
tstr� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 timestr timeStr� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 h  � ��� r   � ���� c   � ���� n   � ���� 7 � �����
�� 
cha � l  � ������� [   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  �  ;   � �� o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ������ z��~
� .sysooffslong    ��� null
�~ misccura��  � �}��
�} 
psof� m   � ��� ���  :� �|��{
�| 
psin� o   � ��z�z 0 timestr timeStr�{  � o      �y�y 0 pos  � ��� r   ���� c   ���� n   � ��� 7  � �x��
�x 
cha � m   � ��w�w � l  � ���v�u� \   � ���� o   � ��t�t 0 pos  � m   � ��s�s �v  �u  � o   � ��r�r 0 timestr timeStr� m   �q
�q 
TEXT� o      �p�p 0 m  � ��� r  ��� c  ��� n  ��� 7�o��
�o 
cha � l ��n�m� [  ��� o  �l�l 0 pos  � m  �k�k �n  �m  �  ;  � o  �j�j 0 timestr timeStr� m  �i
�i 
TEXT� o      �h�h 0 timestr timeStr� ��� l �g�f�e�g  �f  �e  � ��� l �d���d  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  2��� I 0��c�� z�b�a
�b .sysooffslong    ��� null
�a misccura�c  � �`��
�` 
psof� m  "%   �   � �_�^
�_ 
psin o  ()�]�] 0 timestr timeStr�^  � o      �\�\ 0 pos  �  r  3E c  3C n  3A	
	 74A�[
�[ 
cha  l :>�Z�Y [  :> o  ;<�X�X 0 pos   m  <=�W�W �Z  �Y    ;  ?@
 o  34�V�V 0 timestr timeStr m  AB�U
�U 
TEXT o      �T�T 0 s    l FF�S�R�Q�S  �R  �Q    l FF�P�O�N�P  �O  �N    Z  F�M l FI�L�K =  FI o  FG�J�J 0 isfolder isFolder m  GH�I
�I boovtrue�L  �K   k  La  l LL�H �H     Create folder timestamp     �!! 0   C r e a t e   f o l d e r   t i m e s t a m p "�G" r  La#$# b  L_%&% b  L]'(' b  L[)*) b  LY+,+ b  LU-.- b  LS/0/ b  LQ121 m  LO33 �44  b a c k u p _2 o  OP�F�F 0 ty tY0 o  QR�E�E 0 tm tM. o  ST�D�D 0 td tD, m  UX55 �66  _* o  YZ�C�C 0 h  ( o  [\�B�B 0 m  & o  ]^�A�A 0 s  $ o      �@�@ 0 	timestamp  �G   787 l dg9�?�>9 =  dg:;: o  de�=�= 0 isfolder isFolder; m  ef�<
�< boovfals�?  �>  8 <�;< k  j{== >?> l jj�:@A�:  @   Create timestamp   A �BB "   C r e a t e   t i m e s t a m p? C�9C r  j{DED b  jyFGF b  jwHIH b  juJKJ b  jsLML b  joNON b  jmPQP o  jk�8�8 0 ty tYQ o  kl�7�7 0 tm tMO o  mn�6�6 0 td tDM m  orRR �SS  _K o  st�5�5 0 h  I o  uv�4�4 0 m  G o  wx�3�3 0 s  E o      �2�2 0 	timestamp  �9  �;  �M   TUT l ���1�0�/�1  �0  �/  U V�.V L  ��WW o  ���-�- 0 	timestamp  �.    	(boolean)    �XX  ( b o o l e a n ) YZY l     �,�+�*�,  �+  �*  Z [\[ l     �)�(�'�)  �(  �'  \ ]^] l     �&�%�$�&  �%  �$  ^ _`_ l     �#�"�!�#  �"  �!  ` aba l     � ���   �  �  b cdc l     �ef�  e q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   f �gg �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .d hih l     �jk�  j w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   k �ll �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .i mnm i  loopo I      �q�� 0 comparesizes compareSizesq rsr o      �� 
0 source  s t�t o      �� 0 destination  �  �  p l    huvwu k     hxx yzy r     {|{ m     �
� boovtrue| o      �� 0 fit  z }~} r    
� 4    ��
� 
psxf� o    �� 0 destination  � o      �� 0 destination  ~ ��� l   ����  �  �  � ��� O    V��� k    U�� ��� r    ��� I   ���
� .sysoexecTEXT���     TEXT� b    ��� b    ��� m    �� ���  d u   - m s  � o    �� 
0 source  � m    �� ���    |   c u t   - f   1�  � o      �� 0 
foldersize 
folderSize� ��� r    -��� ^    +��� l   )��
�	� I   )���� z��
� .sysorondlong        doub
� misccura� ]    $��� l   "���� ^    "��� o     �� 0 
foldersize 
folderSize� m     !�� �  �  � m   " #�� d�  �
  �	  � m   ) *� �  d� o      ���� 0 
foldersize 
folderSize� ��� l  . .��������  ��  ��  � ��� r   . <��� ^   . :��� ^   . 8��� ^   . 6��� l  . 4������ l  . 4������ n   . 4��� 1   2 4��
�� 
frsp� 4   . 2���
�� 
cdis� o   0 1���� 0 destination  ��  ��  ��  ��  � m   4 5���� � m   6 7���� � m   8 9���� � o      ���� 0 	freespace 	freeSpace� ��� r   = M��� ^   = K��� l  = I������ I  = I����� z����
�� .sysorondlong        doub
�� misccura� l  A D������ ]   A D��� o   A B���� 0 	freespace 	freeSpace� m   B C���� d��  ��  ��  ��  ��  � m   I J���� d� o      ���� 0 	freespace 	freeSpace� ��� l  N N��������  ��  ��  � ���� I  N U�����
�� .ascrcmnt****      � ****� l  N Q������ b   N Q��� o   N O���� 0 
foldersize 
folderSize� o   O P���� 0 	freespace 	freeSpace��  ��  ��  ��  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  W W��������  ��  ��  � ��� Z   W e������� H   W [�� l  W Z������ A   W Z��� o   W X���� 0 
foldersize 
folderSize� o   X Y���� 0 	freespace 	freeSpace��  ��  � r   ^ a��� m   ^ _��
�� boovfals� o      ���� 0 fit  ��  ��  � ��� l  f f��������  ��  ��  � ���� L   f h�� o   f g���� 0 fit  ��  v  (string, string)   w ���   ( s t r i n g ,   s t r i n g )n ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i  ps��� I      ������� 0 	stopwatch  � ���� o      ���� 0 mode  ��  ��  � l    5���� k     5�� ��� q      �� ������ 0 x  ��  � ��� Z     3������ l    ������ =     ��� o     ���� 0 mode  � m    �� ��� 
 s t a r t��  ��  � k        r     I    ������ 0 gettimestamp getTimestamp �� m    ��
�� boovfals��  ��   o      ���� 0 x   �� I   ����
�� .ascrcmnt****      � **** l   	����	 b    

 m     �   b a c k u p   s t a r t e d :   o    ���� 0 x  ��  ��  ��  ��  �  l   ���� =     o    ���� 0 mode   m     �  f i n i s h��  ��   �� k    /  r    ' I    %������ 0 gettimestamp getTimestamp �� m     !��
�� boovfals��  ��   o      ���� 0 x   �� I  ( /����
�� .ascrcmnt****      � **** l  ( +���� b   ( + !  m   ( )"" �## " b a c k u p   f i n i s h e d :  ! o   ) *���� 0 x  ��  ��  ��  ��  ��  ��  � $��$ l  4 4��������  ��  ��  ��  �  (string)   � �%%  ( s t r i n g )� &'& l     ��������  ��  ��  ' ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��������  ��  ��  - ./. l     ��������  ��  ��  / 010 l     ��23��  2 M G Utility function to get the duration of the backup process in minutes.   3 �44 �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .1 565 i  tw787 I      �������� 0 getduration getDuration��  ��  8 k     99 :;: r     <=< ^     >?> l    @���@ \     ABA o     �~�~ 0 endtime endTimeB o    �}�} 0 	starttime 	startTime��  �  ? m    �|�| <= o      �{�{ 0 duration  ; CDC r    EFE ^    GHG l   I�z�yI I   JK�xJ z�w�v
�w .sysorondlong        doub
�v misccuraK l   L�u�tL ]    MNM o    �s�s 0 duration  N m    �r�r d�u  �t  �x  �z  �y  H m    �q�q dF o      �p�p 0 duration  D O�oO L    PP o    �n�n 0 duration  �o  6 QRQ l     �m�l�k�m  �l  �k  R STS l     �j�i�h�j  �i  �h  T UVU l     �g�f�e�g  �f  �e  V WXW l     �d�c�b�d  �c  �b  X YZY l     �a�`�_�a  �`  �_  Z [\[ l     �^]^�^  ] ; 5 Utility function bring WBTFU to the front with focus   ^ �__ j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s\ `a` i  x{bcb I      �]�\�[�] 0 setfocus setFocus�\  �[  c O     
ded I   	�Z�Y�X
�Z .miscactvnull��� ��� null�Y  �X  e m     ff                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  a ghg l     �W�V�U�W  �V  �U  h iji l     �Tkl�T  k � �-----------------------------------------------------------------------------------------------------------------------------------------------   l �mm - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -j non l     �Spq�S  p � �-----------------------------------------------------------------------------------------------------------------------------------------------   q �rr - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -o sts l     �R�Q�P�R  �Q  �P  t uvu l     �O�N�M�O  �N  �M  v wxw l     �L�K�J�L  �K  �J  x yzy l     �I�H�G�I  �H  �G  z {|{ l     �F�E�D�F  �E  �D  | }~} l     �C��C   � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -~ ��� l     �B���B  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �A���A  � � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   � ��� - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �@�?�>�@  �?  �>  � ��� i  |��� I      �=��<�= $0 menuneedsupdate_ menuNeedsUpdate_� ��;� l     ��:�9� m      �8
�8 
cmnu�:  �9  �;  �<  � k     �� ��� l     �7���7  � J D NSMenu's delegates method, when the menu is clicked this is called.   � ��� �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .� ��� l     �6���6  � j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   � ��� �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .� ��� l     �5���5  � < 6 This means the menu items can be changed dynamically.   � ��� l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .� ��4� n    ��� I    �3�2�1�3 0 	makemenus 	makeMenus�2  �1  �  f     �4  � ��� l     �0�/�.�0  �/  �.  � ��� l     �-�,�+�-  �,  �+  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� l     �$�#�"�$  �#  �"  � ��� i  ����� I      �!� ��! 0 	makemenus 	makeMenus�   �  � k    >�� ��� l    	���� n    	��� I    	����  0 removeallitems removeAllItems�  �  � o     �� 0 newmenu newMenu� !  remove existing menu items   � ��� 6   r e m o v e   e x i s t i n g   m e n u   i t e m s� ��� l  
 
����  �  �  � ��� Y   
>������ k   9�� ��� r    '��� n    %��� 4   " %��
� 
cobj� o   # $�� 0 i  � o    "�� 0 thelist theList� o      �� 0 	this_item  � ��� l  ( (����  �  �  � ��� Z   ( ����� l  ( -���� =   ( -��� l  ( +��
�	� c   ( +��� o   ( )�� 0 	this_item  � m   ) *�
� 
TEXT�
  �	  � m   + ,�� ���  B a c k u p   n o w�  �  � r   0 @��� l  0 >���� n  0 >��� I   7 >���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8�� 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ��� m   9 :�� ���  �  �  � n  0 7��� I   3 7� �����  	0 alloc  ��  ��  � n  0 3��� o   1 3���� 0 
nsmenuitem 
NSMenuItem� m   0 1��
�� misccura�  �  � o      ���� 0 thismenuitem thisMenuItem� ��� l  C H������ =   C H��� l  C F������ c   C F��� o   C D���� 0 	this_item  � m   D E��
�� 
TEXT��  ��  � m   F G�� ��� " B a c k u p   n o w   &   q u i t��  ��  � ��� r   K [��� l  K Y������ n  K Y��� I   R Y�� ���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_   o   R S���� 0 	this_item    m   S T � * b a c k u p A n d Q u i t H a n d l e r : �� m   T U �		  ��  ��  � n  K R

 I   N R�������� 	0 alloc  ��  ��   n  K N o   L N���� 0 
nsmenuitem 
NSMenuItem m   K L��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem�  l  ^ c���� =   ^ c l  ^ a���� c   ^ a o   ^ _���� 0 	this_item   m   _ `��
�� 
TEXT��  ��   m   a b � " T u r n   o n   t h e   b a n t s��  ��    r   f x l  f v���� n  f v I   m v������ J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  !  o   m n���� 0 	this_item  ! "#" m   n o$$ �%%  n s f w O n H a n d l e r :# &��& m   o r'' �((  ��  ��   n  f m)*) I   i m�������� 	0 alloc  ��  ��  * n  f i+,+ o   g i���� 0 
nsmenuitem 
NSMenuItem, m   f g��
�� misccura��  ��   o      ���� 0 thismenuitem thisMenuItem -.- l  { �/����/ =   { �010 l  { ~2����2 c   { ~343 o   { |���� 0 	this_item  4 m   | }��
�� 
TEXT��  ��  1 m   ~ �55 �66 $ T u r n   o f f   t h e   b a n t s��  ��  . 787 r   � �9:9 l  � �;����; n  � �<=< I   � ���>���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_> ?@? o   � ����� 0 	this_item  @ ABA m   � �CC �DD  n s f w O f f H a n d l e r :B E��E m   � �FF �GG  ��  ��  = n  � �HIH I   � ��������� 	0 alloc  ��  ��  I n  � �JKJ o   � ����� 0 
nsmenuitem 
NSMenuItemK m   � ���
�� misccura��  ��  : o      ���� 0 thismenuitem thisMenuItem8 LML l  � �N����N =   � �OPO l  � �Q����Q c   � �RSR o   � ����� 0 	this_item  S m   � ���
�� 
TEXT��  ��  P m   � �TT �UU * T u r n   o n   n o t i f i c a t i o n s��  ��  M VWV r   � �XYX l  � �Z����Z n  � �[\[ I   � ���]���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_] ^_^ o   � ����� 0 	this_item  _ `a` m   � �bb �cc , n o t i f i c a t i o n O n H a n d l e r :a d��d m   � �ee �ff  ��  ��  \ n  � �ghg I   � ��������� 	0 alloc  ��  ��  h n  � �iji o   � ����� 0 
nsmenuitem 
NSMenuItemj m   � ���
�� misccura��  ��  Y o      ���� 0 thismenuitem thisMenuItemW klk l  � �m����m =   � �non l  � �p����p c   � �qrq o   � ����� 0 	this_item  r m   � ���
�� 
TEXT��  ��  o m   � �ss �tt , T u r n   o f f   n o t i f i c a t i o n s��  ��  l uvu r   � �wxw l  � �y����y n  � �z{z I   � ���|���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_| }~} o   � ����� 0 	this_item  ~ � m   � ��� ��� . n o t i f i c a t i o n O f f H a n d l e r :� ���� m   � ��� ���  ��  ��  { n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  x o      ���� 0 thismenuitem thisMenuItemv ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ���  Q u i t��  ��  � ���� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ���  q u i t H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem��  �  � ��� l ��������  ��  ��  � ��� l ������ n ��� I  ������� 0 additem_ addItem_� ���� o  ���� 0 thismenuitem thisMenuItem��  ��  � o  ���� 0 newmenu newMenu��  ��  � ��� l ��������  ��  ��  � ��� l ���� l ����� n ��� I  �~��}�~ 0 
settarget_ 
setTarget_� ��|�  f  �|  �}  � o  �{�{ 0 thismenuitem thisMenuItem��  �  � * $ required for enabling the menu item   � ��� H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m� ��� l �z�y�x�z  �y  �x  � ��w� Z  9���v�u� G  "��� l ��t�s� =  ��� o  �r�r 0 i  � m  �q�q �t  �s  � l ��p�o� =  ��� o  �n�n 0 i  � m  �m�m �p  �o  � l %5���� l %5��l�k� n %5��� I  *5�j��i�j 0 additem_ addItem_� ��h� l *1��g�f� n *1��� o  -1�e�e 0 separatoritem separatorItem� n *-��� o  +-�d�d 0 
nsmenuitem 
NSMenuItem� m  *+�c
�c misccura�g  �f  �h  �i  � o  %*�b�b 0 newmenu newMenu�l  �k  �   add a seperator   � ���     a d d   a   s e p e r a t o r�v  �u  �w  � 0 i  � m    �a�a � n    ��� m    �`
�` 
nmbr� n   ��� 2   �_
�_ 
cobj� o    �^�^ 0 thelist theList�  �  � ��� l     �]�\�[�]  �\  �[  � ��� l     �Z�Y�X�Z  �Y  �X  � ��� l     �W�V�U�W  �V  �U  � ��� l     �T�S�R�T  �S  �R  � ��� l     �Q�P�O�Q  �P  �O  � ��� i  ����� I      �N��M�N  0 backuphandler_ backupHandler_� ��L� o      �K�K 
0 sender  �L  �M  � Z     �����J� l    ��I�H� =     ��� o     �G�G 0 isbackingup isBackingUp� m    �F
�F boovfals�I  �H  � k   
 U�� ��� r   
 ��� m   
 �E
�E boovtrue� o      �D�D 0 manualbackup manualBackup� ��C� Z    U���B�A� l   ��@�?� =    ��� o    �>�> 0 notifications  � m    �=
�= boovtrue�@  �?  � Z    Q�� �<� l   #�;�: =    # o    !�9�9 0 nsfw   m   ! "�8
�8 boovtrue�;  �:  � k   & 3  I  & -�7
�7 .sysonotfnull��� ��� TEXT m   & '		 �

 T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p �6�5
�6 
appr m   ( ) � 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�5   �4 I  . 3�3�2
�3 .sysodelanull��� ��� nmbr m   . /�1�1 �2  �4     l  6 =�0�/ =   6 = o   6 ;�.�. 0 nsfw   m   ; <�-
�- boovfals�0  �/   �, k   @ M  I  @ G�+
�+ .sysonotfnull��� ��� TEXT m   @ A �   P r e p a r i n g   b a c k u p �*�)
�* 
appr m   B C � 
 W B T F U�)    �(  I  H M�'!�&
�' .sysodelanull��� ��� nmbr! m   H I�%�% �&  �(  �,  �<  �B  �A  �C  � "#" l  X _$�$�#$ =   X _%&% o   X ]�"�" 0 isbackingup isBackingUp& m   ] ^�!
�! boovtrue�$  �#  # '� ' k   b �(( )*) r   b k+,+ n   b i-.- 3   g i�
� 
cobj. o   b g�� 40 messagesalreadybackingup messagesAlreadyBackingUp, o      �� 0 msg  * /�/ Z   l �01��0 l  l s2��2 =   l s343 o   l q�� 0 notifications  4 m   q r�
� boovtrue�  �  1 Z   v �567�5 l  v }8��8 =   v }9:9 o   v {�� 0 nsfw  : m   { |�
� boovtrue�  �  6 I  � ��;<
� .sysonotfnull��� ��� TEXT; o   � ��� 0 msg  < �=�
� 
appr= m   � �>> �?? P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�  7 @A@ l  � �B��B =   � �CDC o   � ��
�
 0 nsfw  D m   � ��	
�	 boovfals�  �  A E�E I  � ��FG
� .sysonotfnull��� ��� TEXTF m   � �HH �II 2 Y o u ' r e   a l r e a d y   b a c k i n g   u pG �J�
� 
apprJ m   � �KK �LL 
 W B T F U�  �  �  �  �  �  �   �J  � MNM l     ����  �  �  N OPO l     �� ���  �   ��  P QRQ l     ��������  ��  ��  R STS l     ��������  ��  ��  T UVU l     ��������  ��  ��  V WXW i  ��YZY I      ��[���� .0 backupandquithandler_ backupAndQuitHandler_[ \��\ o      ���� 
0 sender  ��  ��  Z Z     �]^_��] l    `����` =     aba o     ���� 0 isbackingup isBackingUpb m    ��
�� boovfals��  ��  ^ k   
 �cc ded r   
 fgf l  
 h����h n   
 iji 1    ��
�� 
bhitj l  
 k����k I  
 ��lm
�� .sysodlogaskr        TEXTl m   
 nn �oo T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?m ��pq
�� 
btnsp J    rr sts m    uu �vv  Y e st w��w m    xx �yy  N o��  q ��z��
�� 
dfltz m    ���� ��  ��  ��  ��  ��  g o      ���� 	0 reply  e {|{ l   ��������  ��  ��  | }��} Z    �~���~ l   ������ =    ��� o    ���� 	0 reply  � m    �� ���  Y e s��  ��   k   ! t�� ��� r   ! (��� m   ! "��
�� boovtrue� o      ����  0 backupthenquit backupThenQuit� ��� r   ) 0��� m   ) *��
�� boovtrue� o      ���� 0 manualbackup manualBackup� ��� l  1 1��������  ��  ��  � ���� Z   1 t������� l  1 8������ =   1 8��� o   1 6���� 0 notifications  � m   6 7��
�� boovtrue��  ��  � Z   ; p������ l  ; B������ =   ; B��� o   ; @���� 0 nsfw  � m   @ A��
�� boovtrue��  ��  � k   E R�� ��� I  E L����
�� .sysonotfnull��� ��� TEXT� m   E F�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �����
�� 
appr� m   G H�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  M R�����
�� .sysodelanull��� ��� nmbr� m   M N���� ��  ��  � ��� l  U \������ =   U \��� o   U Z���� 0 nsfw  � m   Z [��
�� boovfals��  ��  � ���� k   _ l�� ��� I  _ f����
�� .sysonotfnull��� ��� TEXT� m   _ `�� ���   P r e p a r i n g   b a c k u p� �����
�� 
appr� m   a b�� ��� 
 W B T F U��  � ���� I  g l�����
�� .sysodelanull��� ��� nmbr� m   g h���� ��  ��  ��  ��  ��  ��  ��  � ��� l  w |������ =   w |��� o   w x���� 	0 reply  � m   x {�� ���  N o��  ��  � ���� r    ���� m    ���
�� boovfals� o      ����  0 backupthenquit backupThenQuit��  ��  ��  _ ��� l  � ������� =   � ���� o   � ����� 0 isbackingup isBackingUp� m   � ���
�� boovtrue��  ��  � ���� k   � ��� ��� r   � ���� n   � ���� 3   � ���
�� 
cobj� o   � ����� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   � �������� l  � ������� =   � ���� o   � ����� 0 notifications  � m   � ���
�� boovtrue��  ��  � Z   � ������� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovtrue��  ��  � I  � �����
�� .sysonotfnull��� ��� TEXT� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  ��  ��  X ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ����~��  �  �~  � ��� i  ����� I      �}��|�}  0 nsfwonhandler_ nsfwOnHandler_� ��{� o      �z�z 
0 sender  �{  �|  � k     =    r      m     �y
�y boovtrue o      �x�x 0 nsfw   �w Z    =�v l   	�u�t	 =    

 o    �s�s 0 notifications   m    �r
�r boovtrue�u  �t   r     J      m     �  B a c k u p   n o w  m     � " B a c k u p   n o w   &   q u i t  m     � $ T u r n   o f f   t h e   b a n t s  m     � , T u r n   o f f   n o t i f i c a t i o n s �q m       �!!  Q u i t�q   o      �p�p 0 thelist theList "#" l  " )$�o�n$ =   " )%&% o   " '�m�m 0 notifications  & m   ' (�l
�l boovfals�o  �n  # '�k' r   , 9()( J   , 3** +,+ m   , --- �..  B a c k u p   n o w, /0/ m   - .11 �22 " B a c k u p   n o w   &   q u i t0 343 m   . /55 �66 $ T u r n   o f f   t h e   b a n t s4 787 m   / 099 �:: * T u r n   o n   n o t i f i c a t i o n s8 ;�j; m   0 1<< �==  Q u i t�j  ) o      �i�i 0 thelist theList�k  �v  �w  � >?> l     �h�g�f�h  �g  �f  ? @A@ i  ��BCB I      �eD�d�e "0 nsfwoffhandler_ nsfwOffHandler_D E�cE o      �b�b 
0 sender  �c  �d  C k     =FF GHG r     IJI m     �a
�a boovfalsJ o      �`�` 0 nsfw  H K�_K Z    =LMN�^L l   O�]�\O =    PQP o    �[�[ 0 notifications  Q m    �Z
�Z boovtrue�]  �\  M r    RSR J    TT UVU m    WW �XX  B a c k u p   n o wV YZY m    [[ �\\ " B a c k u p   n o w   &   q u i tZ ]^] m    __ �`` " T u r n   o n   t h e   b a n t s^ aba m    cc �dd , T u r n   o f f   n o t i f i c a t i o n sb e�Ye m    ff �gg  Q u i t�Y  S o      �X�X 0 thelist theListN hih l  " )j�W�Vj =   " )klk o   " '�U�U 0 notifications  l m   ' (�T
�T boovfals�W  �V  i m�Sm r   , 9non J   , 3pp qrq m   , -ss �tt  B a c k u p   n o wr uvu m   - .ww �xx " B a c k u p   n o w   &   q u i tv yzy m   . /{{ �|| " T u r n   o n   t h e   b a n t sz }~} m   / 0 ��� * T u r n   o n   n o t i f i c a t i o n s~ ��R� m   0 1�� ���  Q u i t�R  o o      �Q�Q 0 thelist theList�S  �^  �_  A ��� l     �P�O�N�P  �O  �N  � ��� l     �M�L�K�M  �L  �K  � ��� l     �J�I�H�J  �I  �H  � ��� l     �G�F�E�G  �F  �E  � ��� l     �D�C�B�D  �C  �B  � ��� i  ����� I      �A��@�A 00 notificationonhandler_ notificationOnHandler_� ��?� o      �>�> 
0 sender  �?  �@  � k     =�� ��� r     ��� m     �=
�= boovtrue� o      �<�< 0 notifications  � ��;� Z    =����:� l   ��9�8� =    ��� o    �7�7 0 nsfw  � m    �6
�6 boovtrue�9  �8  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��5� m    �� ���  Q u i t�5  � o      �4�4 0 thelist theList� ��� l  " )��3�2� =   " )��� o   " '�1�1 0 nsfw  � m   ' (�0
�0 boovfals�3  �2  � ��/� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��.� m   0 1�� ���  Q u i t�.  � o      �-�- 0 thelist theList�/  �:  �;  � ��� l     �,�+�*�,  �+  �*  � ��� i  ����� I      �)��(�) 20 notificationoffhandler_ notificationOffHandler_� ��'� o      �&�& 
0 sender  �'  �(  � k     =�� ��� r     ��� m     �%
�% boovfals� o      �$�$ 0 notifications  � ��#� Z    =����"� l   ��!� � =    ��� o    �� 0 nsfw  � m    �
� boovtrue�!  �   � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ��� m    �� ���  Q u i t�  � o      �� 0 thelist theList� ��� l  " )���� =   " )� � o   " '�� 0 nsfw    m   ' (�
� boovfals�  �  � � r   , 9 J   , 3  m   , - �  B a c k u p   n o w 	
	 m   - . � " B a c k u p   n o w   &   q u i t
  m   . / � " T u r n   o n   t h e   b a n t s  m   / 0 � * T u r n   o n   n o t i f i c a t i o n s � m   0 1 �  Q u i t�   o      �� 0 thelist theList�  �"  �#  �  l     ����  �  �    l     ����  �  �    l     ����  �  �    l     ��
�	�  �
  �	    !  l     ����  �  �  ! "#" i  ��$%$ I      �&�� 0 quithandler_ quitHandler_& '�' o      �� 
0 sender  �  �  % Z     u()*�( l    +� ��+ =     ,-, o     ���� 0 isbackingup isBackingUp- m    ��
�� boovtrue�   ��  ) k   
 4.. /0/ I   
 �������� 0 setfocus setFocus��  ��  0 121 r     343 l   5����5 n    676 1    ��
�� 
bhit7 l   8����8 I   ��9:
�� .sysodlogaskr        TEXT9 m    ;; �<< � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?: ��=>
�� 
btns= J    ?? @A@ m    BB �CC  C a n c e l   &   Q u i tA D��D m    EE �FF  R e s u m e��  > ��G��
�� 
dfltG m    ���� ��  ��  ��  ��  ��  4 o      ���� 	0 reply  2 H��H Z   ! 4IJ��KI l  ! $L����L =   ! $MNM o   ! "���� 	0 reply  N m   " #OO �PP  C a n c e l   &   Q u i t��  ��  J I  ' ,��Q��
�� .aevtquitnull��� ��� nullQ m   ' (RR                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  K I  / 4��S��
�� .ascrcmnt****      � ****S l  / 0T����T m   / 0UU �VV  W B T F U   r e s u m e d��  ��  ��  ��  * WXW l  7 >Y����Y =   7 >Z[Z o   7 <���� 0 isbackingup isBackingUp[ m   < =��
�� boovfals��  ��  X \��\ k   A q]] ^_^ I   A F�������� 0 setfocus setFocus��  ��  _ `a` r   G Ybcb l  G Wd����d n   G Wefe 1   U W��
�� 
bhitf l  G Ug����g I  G U��hi
�� .sysodlogaskr        TEXTh m   G Hjj �kk < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?i ��lm
�� 
btnsl J   I Onn opo m   I Jqq �rr  Q u i tp s��s m   J Mtt �uu  R e s u m e��  m ��v��
�� 
dfltv m   P Q���� ��  ��  ��  ��  ��  c o      ���� 	0 reply  a w��w Z   Z qxy��zx l  Z _{����{ =   Z _|}| o   Z [���� 	0 reply  } m   [ ^~~ �  Q u i t��  ��  y I  b g�����
�� .aevtquitnull��� ��� null� m   b c��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  z I  j q�����
�� .ascrcmnt****      � ****� l  j m������ m   j m�� ���  W B T F U   r e s u m e d��  ��  ��  ��  ��  �  # ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� (0 animatemenubaricon animateMenuBarIcon��  ��  � k     .�� ��� r     ��� 4     ���
�� 
alis� l   ������ b    ��� l   	������ I   	����
�� .earsffdralis        afdr�  f    � �����
�� 
rtyp� m    ��
�� 
ctxt��  ��  ��  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g��  ��  � o      ���� 0 	imagepath 	imagePath� ��� r    ��� n    ��� 1    ��
�� 
psxp� o    ���� 0 	imagepath 	imagePath� o      ���� 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !������� 20 initwithcontentsoffile_ initWithContentsOfFile_� ���� o    ���� 0 	imagepath 	imagePath��  ��  � n   ��� I    �������� 	0 alloc  ��  ��  � n   ��� o    ���� 0 nsimage NSImage� m    ��
�� misccura� o      ���� 	0 image  � ���� n  $ .��� I   ) .������� 0 	setimage_ 	setImage_� ���� o   ) *���� 	0 image  ��  ��  � o   $ )���� 0 
statusitem 
StatusItem��  � ��� l     ��������  ��  ��  � ��� l     �������  ��  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i  ����� I      ���� $0 resetmenubaricon resetMenuBarIcon�  �  � k     .�� ��� r     ��� 4     ��
� 
alis� l   ���� b    ��� l   	���~� I   	�}��
�} .earsffdralis        afdr�  f    � �|��{
�| 
rtyp� m    �z
�z 
ctxt�{  �  �~  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�  �  � o      �y�y 0 	imagepath 	imagePath� ��� r    ��� n    ��� 1    �x
�x 
psxp� o    �w�w 0 	imagepath 	imagePath� o      �v�v 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�u��t�u 20 initwithcontentsoffile_ initWithContentsOfFile_� ��s� o    �r�r 0 	imagepath 	imagePath�s  �t  � n   ��� I    �q�p�o�q 	0 alloc  �p  �o  � n   ��� o    �n�n 0 nsimage NSImage� m    �m
�m misccura� o      �l�l 	0 image  � ��k� n  $ .��� I   ) .�j��i�j 0 	setimage_ 	setImage_� ��h� o   ) *�g�g 	0 image  �h  �i  � o   $ )�f�f 0 
statusitem 
StatusItem�k  � ��� l     �e�d�c�e  �d  �c  � ��� l     �b�a�`�b  �a  �`  � ��� l     �_�^�]�_  �^  �]  � ��� l     �\�[�Z�\  �[  �Z  � ��� l     �Y�X�W�Y  �X  �W  � ��� l     �V���V  �   create an NSStatusBar   � ��� ,   c r e a t e   a n   N S S t a t u s B a r� ��� i  ��� � I      �U�T�S�U 0 makestatusbar makeStatusBar�T  �S    k     r  l     �R�R    log ("Make status bar")    � . l o g   ( " M a k e   s t a t u s   b a r " )  r     	
	 n     o    �Q�Q "0 systemstatusbar systemStatusBar n     o    �P�P 0 nsstatusbar NSStatusBar m     �O
�O misccura
 o      �N�N 0 bar    l   �M�L�K�M  �L  �K    r     n    I   	 �J�I�J .0 statusitemwithlength_ statusItemWithLength_ �H m   	 
 ��      �H  �I   o    	�G�G 0 bar   o      �F�F 0 
statusitem 
StatusItem  l   �E�D�C�E  �D  �C    r    # 4    !�B 
�B 
alis  l    !�A�@! b     "#" l   $�?�>$ I   �=%&
�= .earsffdralis        afdr%  f    & �<'�;
�< 
rtyp' m    �:
�: 
ctxt�;  �?  �>  # m    (( �)) J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�A  �@   o      �9�9 0 	imagepath 	imagePath *+* r   $ ),-, n   $ './. 1   % '�8
�8 
psxp/ o   $ %�7�7 0 	imagepath 	imagePath- o      �6�6 0 	imagepath 	imagePath+ 010 r   * 8232 n  * 6454 I   1 6�56�4�5 20 initwithcontentsoffile_ initWithContentsOfFile_6 7�37 o   1 2�2�2 0 	imagepath 	imagePath�3  �4  5 n  * 1898 I   - 1�1�0�/�1 	0 alloc  �0  �/  9 n  * -:;: o   + -�.�. 0 nsimage NSImage; m   * +�-
�- misccura3 o      �,�, 	0 image  1 <=< l  9 9�+�*�)�+  �*  �)  = >?> l  9 9�(@A�(  @ � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   A �BB s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "? CDC l  9 9�'�&�%�'  �&  �%  D EFE n  9 CGHG I   > C�$I�#�$ 0 	setimage_ 	setImage_I J�"J o   > ?�!�! 	0 image  �"  �#  H o   9 >� �  0 
statusitem 
StatusItemF KLK l  D D����  �  �  L MNM l  D D�OP�  O , & set up the initial NSStatusBars title   P �QQ L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l eN RSR l  D D�TU�  T # StatusItem's setTitle:"WBTFU"   U �VV : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "S WXW l  D D����  �  �  X YZY l  D D�[\�  [ 1 + set up the initial NSMenu of the statusbar   \ �]] V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a rZ ^_^ r   D X`a` n  D Rbcb I   K R�d��  0 initwithtitle_ initWithTitle_d e�e m   K Nff �gg  C u s t o m�  �  c n  D Khih I   G K���� 	0 alloc  �  �  i n  D Gjkj o   E G�� 0 nsmenu NSMenuk m   D E�
� misccuraa o      �� 0 newmenu newMenu_ lml l  Y Y����  �  �  m non l  Y Y�
pq�
  p � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   q �rr0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .o sts n  Y cuvu I   ^ c�	w��	 0 setdelegate_ setDelegate_w x�x  f   ^ _�  �  v o   Y ^�� 0 newmenu newMenut yzy l  d d����  �  �  z {�{ n  d r|}| I   i r�~� � 0 setmenu_ setMenu_~ �� o   i n���� 0 newmenu newMenu��  �   } o   d i���� 0 
statusitem 
StatusItem�  � ��� l     ��������  ��  ��  � ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� 0 runonce runOnce��  ��  � k     �� ��� l      ������  � � �if not (current application's NSThread's isMainThread()) as boolean then		display alert "This script must be run from the main thread." buttons {"Cancel"} as critical		error number -128	end if   � ���� i f   n o t   ( c u r r e n t   a p p l i c a t i o n ' s   N S T h r e a d ' s   i s M a i n T h r e a d ( ) )   a s   b o o l e a n   t h e n  	 	 d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l  	 	 e r r o r   n u m b e r   - 1 2 8  	 e n d   i f� ��� l     ��������  ��  ��  � ���� I     �������� 0 	plistinit 	plistInit��  ��  ��  � ��� l     ��������  ��  ��  � ��� l    ������ n    ��� I    �������� 0 makestatusbar makeStatusBar��  ��  �  f     ��  ��  � ��� l   ������ I    �������� 0 runonce runOnce��  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     �������  ��  �  � ��� l     ����  � w q Function that is always running in the background. This doesn't need to get called as it is running from the off   � ��� �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f� ��� l     ����  � � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   � ����   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .� ��� i  ����� I     ���
� .miscidlenmbr    ��� null�  �  � k     ��� ��� Z     ������ l    ���� E     ��� o     �� &0 displaysleepstate displaySleepState� o    
�� 0 strawake strAwake�  �  � Z    ������ l   ���� =    ��� o    �� 0 isbackingup isBackingUp� m    �
� boovfals�  �  � Z    ������ l   ���� =    ��� o    �� 0 manualbackup manualBackup� m    �
� boovfals�  �  � Z   " s����� l  " +���� A   " +��� l  " )���� n   " )��� 1   ' )�
� 
hour� l  " '���� I  " '���
� .misccurdldt    ��� null�  �  �  �  �  �  � m   ) *�� �  �  � k   . o�� ��� r   . 7��� l  . 5���� n   . 5��� 1   3 5�
� 
min � l  . 3���� l  . 3���� I  . 3���
� .misccurdldt    ��� null�  �  �  �  �  �  �  �  � o      �� 0 m  � ��� l  8 8����  �  �  � ��� Z   8 o����� G   8 C��� l  8 ;���� =   8 ;��� o   8 9�� 0 m  � m   9 :�� �  �  � l  > A���� =   > A��� o   > ?�� 0 m  � m   ? @�� -�  �  � Z   F U����� l  F I���� =   F I��� o   F G�~�~ 0 scheduledtime scheduledTime� m   G H�}�} �  �  � I   L Q�|�{�z�| 0 	plistinit 	plistInit�{  �z  �  �  � � � G   X c l  X [�y�x =   X [ o   X Y�w�w 0 m   m   Y Z�v�v  �y  �x   l  ^ a�u�t =   ^ a o   ^ _�s�s 0 m   m   _ `�r�r �u  �t    	�q	 I   f k�p�o�n�p 0 	plistinit 	plistInit�o  �n  �q  �  �  �  �  � 

 l  v }�m�l =   v } o   v {�k�k 0 manualbackup manualBackup m   { |�j
�j boovtrue�m  �l   �i I   � ��h�g�f�h 0 	plistinit 	plistInit�g  �f  �i  �  �  �  �  �  �  l  � ��e�d�c�e  �d  �c   �b L   � � m   � ��a�a �b  �       5�`�_ g�^�]�\�[�Z�Y ��!"#$%&'()*+,-./0123456789:;<=�`   3�X�W�V�U�T�S�R�Q�P�O�N�M�L�K�J�I�H�G�F�E�D�C�B�A�@�?�>�=�<�;�:�9�8�7�6�5�4�3�2�1�0�/�.�-�,�+�*�)�(�'�&
�X 
pimr�W 0 
statusitem 
StatusItem�V 0 
thedisplay 
theDisplay�U 0 defaults  �T $0 internalmenuitem internalMenuItem�S $0 externalmenuitem externalMenuItem�R 0 newmenu newMenu�Q 0 thelist theList�P 0 nsfw  �O 0 notifications  �N  0 backupthenquit backupThenQuit�M 	0 plist  �L 0 initialbackup initialBackup�K 0 manualbackup manualBackup�J 0 isbackingup isBackingUp�I "0 messagesmissing messagesMissing�H *0 messagesencouraging messagesEncouraging�G $0 messagescomplete messagesComplete�F &0 messagescancelled messagesCancelled�E 40 messagesalreadybackingup messagesAlreadyBackingUp�D 0 strawake strAwake�C 0 strsleep strSleep�B &0 displaysleepstate displaySleepState�A 0 	plistinit 	plistInit�@ 0 diskinit diskInit�? 0 freespaceinit freeSpaceInit�> 0 
backupinit 
backupInit�= 0 tidyup tidyUp�< 
0 backup  �; 0 itexists itExists�: .0 getcomputeridentifier getComputerIdentifier�9 0 gettimestamp getTimestamp�8 0 comparesizes compareSizes�7 0 	stopwatch  �6 0 getduration getDuration�5 0 setfocus setFocus�4 $0 menuneedsupdate_ menuNeedsUpdate_�3 0 	makemenus 	makeMenus�2  0 backuphandler_ backupHandler_�1 .0 backupandquithandler_ backupAndQuitHandler_�0  0 nsfwonhandler_ nsfwOnHandler_�/ "0 nsfwoffhandler_ nsfwOffHandler_�. 00 notificationonhandler_ notificationOnHandler_�- 20 notificationoffhandler_ notificationOffHandler_�, 0 quithandler_ quitHandler_�+ (0 animatemenubaricon animateMenuBarIcon�* $0 resetmenubaricon resetMenuBarIcon�) 0 makestatusbar makeStatusBar�( 0 runonce runOnce
�' .miscidlenmbr    ��� null
�& .aevtoappnull  �   � **** �%>�% >  ?@AB? �$ L�#
�$ 
vers�#  @ �"C�!
�" 
cobjC DD   � 
�  
osax�!  A �E�
� 
cobjE FF   � U
� 
frmk�  B �G�
� 
cobjG HH   � [
� 
frmk�  
�_ 
msng II ��J
� misccura
� 
pclsJ �KK  N S U s e r D e f a u l t s LL ��M
� misccura
� 
pclsM �NN  N S M e n u I t e m OO ��P
� misccura
� 
pclsP �QQ  N S M e n u I t e m RR ��S
� misccura
� 
pclsS �TT  N S M e n u �U� U   � � � � �
�^ boovfals
�] boovtrue
�\ boovfals �VV � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�[ boovtrue
�Z boovfals
�Y boovfals �W� W   � �	 �X� 	X 	 #'+/2 �Y� 	Y 	 <@DHLPTX[ �Z� Z  eimquy}�  �[� 	[ 	 ���������! �\\�             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 1 9 7 3 5 7 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 3 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 4 , " I d l e T i m e r E l a p s e d T i m e " = 3 3 6 0 3 6 9 , " C u r r e n t P o w e r S t a t e " = 4 }" ���
�	]^�� 0 	plistinit 	plistInit�
  �	  ] �������� � 0 
foldername 
folderName� 0 
backupdisk 
backupDisk�  0 computerfolder computerFolder� 0 
backuptime 
backupTime� 0 thecontents theContents� 0 thevalue theValue� 0 backuptimes backupTimes�  0 selectedtime selectedTime^ 1���2�����������������������L��W����ptw���������������������������������������������� 0 itexists itExists
�� 
plif
�� 
pcnt
�� 
valL��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive��  0 computerfolder computerFolder�� 0 scheduledtime scheduledTime�� .0 getcomputeridentifier getComputerIdentifier��  ���� 0 setfocus setFocus
�� 
prmp
�� .sysostflalis    ��� null
�� 
TEXT
�� 
psxp
�� .gtqpchltns    @   @ ns  �� �� �� <
�� 
kocl
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null
�� 
plii
�� 
insh
�� 
kind�� �� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 machinefolder machineFolder
�� .ascrcmnt****      � ****�� 0 diskinit diskInit��jE�O*�b  l+ e  6� .*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYV*j+ 
E�O� ��n*j+ O*��l E�O*�a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a �a  a !b  la " # �*a a $a %*6a  a &a a !a '�a (a ( #O*a a $a %*6a  a &a a !a )�a (a ( #O*a a $a %*6a  a &a a !a *�a (a ( #O*a a $a %*6a  a &a a !a +�a (a ( #UUO�E` ,O�E` -O�E` .O�E�O_ ,_ -_ .�a "vj /O*j+ 0# ��;����_`���� 0 diskinit diskInit��  ��  _ ������ 0 msg  �� 	0 reply  ` E������������be��������o����������� "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit�� 0 setfocus setFocus
�� 
cobj
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit
�� 
appr
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  *fk+ Y �*j+ Ob  �.E�O����lv�l� �,E�O��  
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h$ �������ab���� 0 freespaceinit freeSpaceInit�� ��c�� c  ���� 	0 again  ��  a �������� 	0 again  �� 
0 reply1  �� 0 msg  b �������������������
���)�36�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes� 0 
backupinit 
backupInit� 0 setfocus setFocus
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit� 0 tidyup tidyUp
� 
cobj
� 
appr
� .sysonotfnull��� ��� TEXT�� �*��l+ e  
*j+ Y �*j+ O�f  ����lv�l� �,E�Y �e  ����lv�l� �,E�Y hO�a   
*j+ Y ab  b   b  a .E�Y hOb  	e  8b  e  �a a l Y b  f  a a a l Y hY h% �O��de�� 0 
backupinit 
backupInit�  �  d  e ���gl�z�����x�����������������
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  � 0 itexists itExists
� 
kocl
� 
cfol
� 
insh
� 
prdt
� 
pnam� 
� .corecrel****      � null� 0 machinefolder machineFolder
� 
cdis� 0 backupfolder backupFolder� 0 latestfolder latestFolder� 
0 backup  � �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` OPUO*j+ & �*��fg�� 0 tidyup tidyUp�  �  f ��������� 0 bf bF� 0 creationdates creationDates� 0 theoldestdate theOldestDate� 0 j  � 0 i  � 0 thisdate thisDate� 
0 reply2  � 0 msg  g &���6��F�����t��������������������~ �}/2
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  
� 
cdis
� 
cfol� 0 machinefolder machineFolder
� 
cobj
� 
ascd
� .corecnte****       ****
� 
pnam
� .ascrcmnt****      � ****
� .coredeloobj        obj � 0 setfocus setFocus
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� 
trsh
� .fndremptnull��� ��� obj 
� 
appr
� .sysonotfnull��� ��� TEXT�~ 0 freespaceinit freeSpaceInit
�} .sysodelanull��� ��� nmbr�{*��/E�O�p*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/j O*j+ Oa a a a lva la  a ,E�O�a   *a ,j Y _b  b   Pb  �.E�Ob  	e  8b  e  �a a l Y b  f  a a a l Y hY hY hO*ek+  Ob  b   Tb  	e  Fb  e  a !a a "l Okj #Y #b  f  a $a a %l Okj #Y hY hY hU' �|S�{�zhi�y�| 
0 backup  �{  �z  h 
�x�w�v�u�t�s�r�q�p�o�x 0 t  �w 0 x  �v "0 containerfolder containerFolder�u 0 
foldername 
folderName�t 0 d  �s 0 duration  �r 0 msg  �q 0 c  �p 0 oldestfolder oldestFolder�o 0 todelete toDeletei i�n�mp�l
[�k�j�i�h�g�f�e�d�c�b�a�`�_�^��]�\�[�Z�YD�XG�WQT�Voqs����U�T�S�R�Q�P����O����N�M#%')<>@H�Ls�������K�J���	7	I	K	N	a	c	f	{	}	�	�	�	�	�	�	�	�	�	�	�



*
,
/
B
D
G
a�n 0 gettimestamp getTimestamp�m (0 animatemenubaricon animateMenuBarIcon�l 0 	stopwatch  
�k 
psxf�j 0 sourcefolder sourceFolder
�i 
TEXT
�h 
cfol
�g 
pnam
�f 
kocl
�e 
insh�d 0 backupfolder backupFolder
�c 
prdt�b 
�a .corecrel****      � null�` (0 activesourcefolder activeSourceFolder
�_ 
cdis�^ 	0 drive  �] 0 machinefolder machineFolder�\ (0 activebackupfolder activeBackupFolder
�[ .misccurdldt    ��� null
�Z 
time�Y 0 	starttime 	startTime
�X 
appr
�W .sysonotfnull��� ��� TEXT�V "0 destinationdisk destinationDisk
�U .sysoexecTEXT���     TEXT�T  �S  �R 0 endtime endTime�Q 0 getduration getDuration
�P 
cobj
�O .sysodelanull��� ��� nmbr�N $0 resetmenubaricon resetMenuBarIcon
�M .aevtquitnull��� ��� null
�L .ascrcmnt****      � ****
�K 
alis
�J 
psxp�yy*ek+  E�OeEc  O*j+ O*�k+ O�N*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e `*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_ a  %_ %a !%�%a "%�&E�O a #�%a $%�%a %%j &W X ' (hOfEc  OfEc  Ob  b   �*j a ,E` )O)j+ *E�Ob  a +.E�Ob  	e  Tb  e   a ,�%a -%�%a a .l Okj /Y )b  f  a 0�%a 1%a a 2l Okj /Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYvb  f k_ a 6%_ %a 7%�%a 8%�%a 9%�&E�O_ a :%_ %a ;%�%a <%�&E�Oa =�%j >Ob  b   l*j a ,E` Ob  a +.E�Ob  	e  Db  e  �a a ?l Okj /Y #b  f  a @a a Al Okj /Y hY hY hO a B�%a C%�%a D%�%a E%j &W X ' (hOfEc  OfEc  O*�/a F&a +-jv [��/�&E�O�a G,�&E�Oa H�%j >Oa I�%a J%E�O�j &Ob  b  *j a ,E` )O)j+ *E�Ob  a +.E�Ob  	e  �a K  Tb  e   a L�%a M%�%a a Nl Okj /Y )b  f  a O�%a P%a a Ql Okj /Y hY Qb  e   a R�%a S%�%a a Tl Okj /Y )b  f  a U�%a V%a a Wl Okj /Y hOb  e  a Xa a Yl Y b  f  a Za a [l Y hY hO)j+ 3Y hY	b  b   �*j a ,E` )O)j+ *E�Ob  a +.E�Ob  	e  ��a K  Tb  e   a \�%a ]%�%a a ^l Okj /Y )b  f  a _�%a `%a a al Okj /Y hY Qb  e   a b�%a c%�%a a dl Okj /Y )b  f  a e�%a f%a a gl Okj /Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a hk+ ( �I
��H�Gjk�F�I 0 itexists itExists�H �El�E l  �D�C�D 0 
objecttype 
objectType�C 
0 object  �G  j �B�A�B 0 
objecttype 
objectType�A 
0 object  k 
�
��@�?
��>
��=
�@ 
cdis
�? .coredoexnull���     ****
�> 
file
�= 
cfol�F X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU) �<
��;�:mn�9�< .0 getcomputeridentifier getComputerIdentifier�;  �:  m �8�7�6�8 0 computername computerName�7 "0 strserialnumber strSerialNumber�6  0 identifiername identifierNamen �5�4
��3
�
�5 .sysosigtsirr   ��� null
�4 
sicn
�3 .sysoexecTEXT���     TEXT�9 *j  �,E�O�j E�O��%�%E�O�* �2�1�0op�/�2 0 gettimestamp getTimestamp�1 �.q�. q  �-�- 0 isfolder isFolder�0  o �,�+�*�)�(�'�&�%�$�#�"�!� ����, 0 isfolder isFolder�+ 0 y  �* 0 m  �) 0 d  �( 0 t  �' 0 ty tY�& 0 tm tM�% 0 td tD�$ 0 tt tT�# 
0 tml tML�" 
0 tdl tDL�! 0 timestr timeStr�  0 pos  � 0 h  � 0 s  � 0 	timestamp  p ���������������z������
�	��� 35R
� 
Krtn
� 
year� 0 y  
� 
mnth� 0 m  
� 
day � 0 d  
� 
time� 0 t  � 
� .misccurdldt    ��� null
� 
long
� 
TEXT
� .corecnte****       ****
� 
nmbr
� 
tstr
� misccura
� 
psof
�
 
psin�	 
� .sysooffslong    ��� null
� 
cha �/�*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�+ �p��rs�� 0 comparesizes compareSizes� �t� t  �� � 
0 source  �  0 destination  �  r ������������ 
0 source  �� 0 destination  �� 0 fit  �� 0 
foldersize 
folderSize�� 0 	freespace 	freeSpaces ���������������������
�� 
psxf
�� .sysoexecTEXT���     TEXT
�� misccura�� �� d
�� .sysorondlong        doub
�� 
cdis
�� 
frsp
�� .ascrcmnt****      � ****� ieE�O*�/E�O� H�%�%j E�O� ��!� j U�!E�O*�/�,�!�!�!E�O� 	�� j U�!E�O��%j UO�� fE�Y hO�, �������uv���� 0 	stopwatch  �� ��w�� w  ���� 0 mode  ��  u ������ 0 mode  �� 0 x  v �����"�� 0 gettimestamp getTimestamp
�� .ascrcmnt****      � ****�� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP- ��8����xy���� 0 getduration getDuration��  ��  x ���� 0 duration  y �������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
�� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�. ��c����z{���� 0 setfocus setFocus��  ��  z  { f��
�� .miscactvnull��� ��� null�� � *j U/ �������|}���� $0 menuneedsupdate_ menuNeedsUpdate_�� ��~�� ~  ��
�� 
cmnu��  |  } ���� 0 	makemenus 	makeMenus�� )j+  0 ������������ 0 	makemenus 	makeMenus��  ��   �������� 0 i  �� 0 	this_item  �� 0 thismenuitem thisMenuItem� "��������������������$'5CFTbes��������������  0 removeallitems removeAllItems
�� 
cobj
�� 
nmbr
�� 
TEXT
�� misccura�� 0 
nsmenuitem 
NSMenuItem�� 	0 alloc  �� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�� 0 additem_ addItem_�� 0 
settarget_ 
setTarget_� 
� 
bool� 0 separatoritem separatorItem��?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY��1 ��������  0 backuphandler_ backupHandler_� ��� �  �� 
0 sender  �  � ��� 
0 sender  � 0 msg  � 	����>HK
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h2 �Z������ .0 backupandquithandler_ backupAndQuitHandler_� ��� �  �� 
0 sender  �  � ���� 
0 sender  � 	0 reply  � 0 msg  � n�ux�����������������
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� �b  f  �����lv�l� �,E�O��  XeEc  
OeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h3 ��������  0 nsfwonhandler_ nsfwOnHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  �  �-159<� � >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h4 �C������ "0 nsfwoffhandler_ nsfwOffHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � W[_cf�sw{�� � >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h5 �������� 00 notificationonhandler_ notificationOnHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � ������������ � >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h6 �������� 20 notificationoffhandler_ notificationOffHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � ������� � >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h7 �~%�}�|���{�~ 0 quithandler_ quitHandler_�} �z��z �  �y�y 
0 sender  �|  � �x�w�x 
0 sender  �w 	0 reply  � �v;�uBE�t�s�r�qOR�pU�ojqt~��v 0 setfocus setFocus
�u 
btns
�t 
dflt�s 
�r .sysodlogaskr        TEXT
�q 
bhit
�p .aevtquitnull��� ��� null
�o .ascrcmnt****      � ****�{ vb  e  /*j+  O����lv�l� �,E�O��  
�j Y �j Y @b  f  5*j+  O���a lv�l� �,E�O�a   
�j Y 	a j Y h8 �n��m�l���k�n (0 animatemenubaricon animateMenuBarIcon�m  �l  � �j�i�j 0 	imagepath 	imagePath�i 	0 image  � �h�g�f�e��d�c�b�a�`�_
�h 
alis
�g 
rtyp
�f 
ctxt
�e .earsffdralis        afdr
�d 
psxp
�c misccura�b 0 nsimage NSImage�a 	0 alloc  �` 20 initwithcontentsoffile_ initWithContentsOfFile_�_ 0 	setimage_ 	setImage_�k /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
9 �^��]�\���[�^ $0 resetmenubaricon resetMenuBarIcon�]  �\  � �Z�Y�Z 0 	imagepath 	imagePath�Y 	0 image  � �X�W�V�U��T�S�R�Q�P�O
�X 
alis
�W 
rtyp
�V 
ctxt
�U .earsffdralis        afdr
�T 
psxp
�S misccura�R 0 nsimage NSImage�Q 	0 alloc  �P 20 initwithcontentsoffile_ initWithContentsOfFile_�O 0 	setimage_ 	setImage_�[ /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
: �N �M�L���K�N 0 makestatusbar makeStatusBar�M  �L  � �J�I�H�J 0 bar  �I 0 	imagepath 	imagePath�H 	0 image  � �G�F�E�D�C�B�A�@(�?�>�=�<�;�:f�9�8�7
�G misccura�F 0 nsstatusbar NSStatusBar�E "0 systemstatusbar systemStatusBar�D .0 statusitemwithlength_ statusItemWithLength_
�C 
alis
�B 
rtyp
�A 
ctxt
�@ .earsffdralis        afdr
�? 
psxp�> 0 nsimage NSImage�= 	0 alloc  �< 20 initwithcontentsoffile_ initWithContentsOfFile_�; 0 	setimage_ 	setImage_�: 0 nsmenu NSMenu�9  0 initwithtitle_ initWithTitle_�8 0 setdelegate_ setDelegate_�7 0 setmenu_ setMenu_�K s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ ; �6��5�4���3�6 0 runonce runOnce�5  �4  �  � �2�2 0 	plistinit 	plistInit�3 *j+  < �1��0�/���.
�1 .miscidlenmbr    ��� null�0  �/  � �-�- 0 m  � 
�,�+�*�)�(�'�&�%�$�#
�, .misccurdldt    ��� null
�+ 
hour�* 
�) 
min �( �' -
�& 
bool�% 0 scheduledtime scheduledTime�$ 0 	plistinit 	plistInit�# �. �b  b   �b  f  vb  f  V*j  �,� F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY b  e  
*j+ Y hY hY hO�= �"��!� ���
�" .aevtoappnull  �   � ****� k     �� ��� ���  �!  �   �  � ��� 0 makestatusbar makeStatusBar� 0 runonce runOnce� )j+  O*j+ ascr  ��ޭ