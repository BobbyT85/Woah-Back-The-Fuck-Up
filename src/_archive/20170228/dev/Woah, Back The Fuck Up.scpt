FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S g�� ��� 0 thelist theList � J   S f � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   Y \ � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   \ _ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   _ b � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   q q � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   q q � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ����� 0 endtime endTime�   �  � � � p   q q � � �~�}�~ 0 appicon appIcon�}   �  � � � l     �|�{�z�|  �{  �z   �  � � � l     ��y�x � r      � � � 4     �w �
�w 
alis � l    ��v�u � b     � � � l   	 ��t�s � I   	�r � �
�r .earsffdralis        afdr �  f     � �q ��p
�q 
rtyp � m    �o
�o 
ctxt�p  �t  �s   � m   	 
 � � � � � < C o n t e n t s : R e s o u r c e s : a p p l e t . i c n s�v  �u   � o      �n�n 0 appicon appIcon�y  �x   �  � � � l     �m�l�k�m  �l  �k   �  � � � l     �j�i�h�j  �i  �h   �  � � � l     �g�f�e�g  �f  �e   �  � � � l     �d�c�b�d  �c  �b   �  � � � l     �a�`�_�a  �`  �_   �  � � � l     �^ � ��^   �   Property assignment    � � � � (   P r o p e r t y   a s s i g n m e n t �  � � � j   q ��] ��] 	0 plist   � b   q � � � � n  q � � � � 1   ~ ��\
�\ 
psxp � l  q ~ ��[�Z � I  q ~�Y � �
�Y .earsffdralis        afdr � m   q t�X
�X afdrdlib � �W ��V
�W 
from � m   w z�U
�U fldmfldu�V  �[  �Z   � m   � � � � �   f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  l     �T�S�R�T  �S  �R    j   � ��Q�Q 0 initialbackup initialBackup m   � ��P
�P boovtrue  j   � ��O�O 0 manualbackup manualBackup m   � ��N
�N boovfals 	
	 j   � ��M�M 0 isbackingup isBackingUp m   � ��L
�L boovfals
  l     �K�J�I�K  �J  �I    j   � ��H�H "0 messagesmissing messagesMissing J   � �  m   � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . .  m   � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . .  m   � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !  m   � �   �!! < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? "�G" m   � �## �$$ � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�G   %&% l     �F�E�D�F  �E  �D  & '(' j   � ��C)�C *0 messagesencouraging messagesEncouraging) J   � �** +,+ m   � �-- �..  C o m e   o n !, /0/ m   � �11 �22 4 C o m e   o n !   E y e   o f   t h e   t i g e r !0 343 m   � �55 �66 " N o   p a i n !   N o   p a i n !4 787 m   � �99 �:: 0 L e t ' s   d o   t h i s   a s   a   t e a m !8 ;<; m   � �== �>>  W e   c a n   d o   i t !< ?@? m   � �AA �BB  F r e e e e e e e e e d o m !@ CDC m   � �EE �FF 2 A l t o g e t h e r   o r   n o t   a t   a l l !D GHG m   � �II �JJ H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !H KLK m   � �MM �NN H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !L O�BO m   � �PP �QQ  Y o u   s e x y   f u c k !�B  ( RSR l     �A�@�?�A  �@  �?  S TUT j   � ��>V�> $0 messagescomplete messagesCompleteV J   � �WW XYX m   � �ZZ �[[  N i c e   o n e !Y \]\ m   � �^^ �__ * Y o u   a b s o l u t e   l e g   e n d !] `a` m   � �bb �cc  G o o d   l a d !a ded m   � �ff �gg  P e r f i c k !e hih m   � �jj �kk  H a p p y   d a y s !i lml m   � �nn �oo  F r e e e e e e e e e d o m !m pqp m   � �rr �ss H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !q tut m   � �vv �ww B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !u xyx m   � �zz �{{ d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !y |�=| m   � �}} �~~  Y o u   s e x y   f u c k !�=  U � l     �<�;�:�<  �;  �:  � ��� j   ��9��9 &0 messagescancelled messagesCancelled� J   ��� ��� m   � ��� ���  T h a t ' s   a   s h a m e� ��� m   � ��� ���  A h   m a n ,   u n l u c k y� ��� m   � ��� ���  O h   w e l l� ��� m   � ��� ��� @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e n� ��� m   ��� ��� , W e l l   t h a t ' s   a   l e t   d o w n� ��� m  �� ��� P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?� ��� m  �� ���  A r r o g a n t� ��8� m  
�� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�8  � ��� l     �7�6�5�7  �6  �5  � ��� j  4�4��4 40 messagesalreadybackingup messagesAlreadyBackingUp� J  1�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  �� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  �� ���  D i c k� ��� m  !�� ���  F u c k t a r d� ��� m  !$�� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  $'�� ���  A r r o g a n t� ��� m  '*�� ��� $ C h i l l   t h e   f u c k   o u t� ��3� m  *-�� ���  H o l d   f i r e�3  � ��� l     �2�1�0�2  �1  �0  � ��� j  5;�/��/ 0 strawake strAwake� m  58�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  <B�.��. 0 strsleep strSleep� m  <?�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  CM�-��- &0 displaysleepstate displaySleepState� I CJ�,��+
�, .sysoexecTEXT���     TEXT� m  CF�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�+  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� l     �$�#�"�$  �#  �"  � ��� l     �!� ��!  �   �  � ��� l     ����  �  �  � ��� l     ����  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  �  �  � ��� l     ����  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� � � l     ��   m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file    � �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e   l     ��   b \ If a .plist file is present, it assigns preferences to their corresponding global variables    � �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s 	
	 i  NQ I      ���� 0 	plistinit 	plistInit�  �   k    �  q       �� 0 
foldername 
folderName �� 0 
backupdisk 
backupDisk ��  0 computerfolder computerFolder ��� 0 
backuptime 
backupTime�    r      m     �
�
   o      �	�	 0 
backuptime 
backupTime  l   ����  �  �    Z   �� l   �� =     !  I    �"�� 0 itexists itExists" #$# m    %% �&&  f i l e$ '� ' o    ���� 	0 plist  �   �  ! m    ��
�� boovtrue�  �   O    E()( k    D** +,+ r    $-.- n    "/0/ 1     "��
�� 
pcnt0 4     ��1
�� 
plif1 o    ���� 	0 plist  . o      ���� 0 thecontents theContents, 232 r   % *454 n   % (676 1   & (��
�� 
valL7 o   % &���� 0 thecontents theContents5 o      ���� 0 thevalue theValue3 898 l  + +��������  ��  ��  9 :;: r   + 0<=< n   + .>?> o   , .����  0 foldertobackup FolderToBackup? o   + ,���� 0 thevalue theValue= o      ���� 0 
foldername 
folderName; @A@ r   1 6BCB n   1 4DED o   2 4���� 0 backupdrive BackupDriveE o   1 2���� 0 thevalue theValueC o      ���� 0 
backupdisk 
backupDiskA FGF r   7 <HIH n   7 :JKJ o   8 :����  0 computerfolder computerFolderK o   7 8���� 0 thevalue theValueI o      ����  0 computerfolder computerFolderG LML r   = BNON n   = @PQP o   > @���� 0 scheduledtime scheduledTimeQ o   = >���� 0 thevalue theValueO o      ���� 0 
backuptime 
backupTimeM RSR l  C C��������  ��  ��  S T��T l  C C��UV��  U . (log {folderName, backupDisk, backupTime}   V �WW P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  ) m    XX�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �   k   H�YY Z[Z r   H O\]\ I   H M�������� .0 getcomputeridentifier getComputerIdentifier��  ��  ] o      ����  0 computerfolder computerFolder[ ^_^ l  P P��������  ��  ��  _ `a` O   Pbcb t   Tded k   Vff ghg n  V [iji I   W [�������� 0 setfocus setFocus��  ��  j  f   V Wh klk l  \ \��������  ��  ��  l mnm r   \ _opo m   \ ]qq �rr� T h i s   p r o g r a m   a u t o m a t i c a l l y   b a c k s   u p   y o u r   H o m e   f o l d e r   b u t   e x c l u d e s   t h e   C a c h e s ,   L o g s   a n d   M o b i l e   D o c u m e n t s   f o l d e r s   f r o m   y o u r   L i b r a r y . 
 
 A l l   y o u   n e e d   t o   d o   i s   s e l e c t   t h e   d r i v e   t o   b a c k u p   t o   a t   t h e   n e x t   p r o m p t .p o      ���� 0 welcome  n sts r   ` �uvu l  ` �w����w n   ` �xyx 1   } ���
�� 
bhity l  ` }z����z I  ` }��{|
�� .sysodlogaskr        TEXT{ o   ` a���� 0 welcome  | ��}~
�� 
appr} m   b e ��� , W o a h ,   B a c k   T h e   F u c k   U p~ ����
�� 
disp� o   h k���� 0 appicon appIcon� ����
�� 
btns� J   n s�� ���� m   n q�� ���  O K ,   g o t   i t��  � �����
�� 
dflt� m   v w���� ��  ��  ��  ��  ��  v o      ���� 	0 reply  t ��� l  � ���������  ��  ��  � ��� r   � ���� I  � ������
�� .earsffdralis        afdr� l  � ������� m   � ���
�� afdrcusr��  ��  ��  � o      ���� 0 
foldername 
folderName� ��� r   � ���� c   � ���� l  � ������� I  � ������
�� .sysostflalis    ��� null��  � �����
�� 
prmp� m   � ��� ��� R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  � m   � ���
�� 
TEXT� o      ���� 0 
backupdisk 
backupDisk� ��� l  � ���������  ��  ��  � ��� r   � ���� n   � ���� 1   � ���
�� 
psxp� o   � ����� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� r   � ���� n   � ���� 1   � ���
�� 
psxp� o   � ����� 0 
backupdisk 
backupDisk� o      ���� 0 
backupdisk 
backupDisk� ��� l  � ���������  ��  ��  � ��� r   � ���� J   � ��� ��� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r� ��� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r� ���� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  � o      ���� 0 backuptimes backupTimes� ��� r   � ���� c   � ���� J   � ��� ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  � m   � ���
�� 
TEXT� o      ���� 0 selectedtime selectedTime� ��� l  � ���������  ��  ��  � ��� Z   ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l ��������  ��  ��  � ���� l ������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  e m   T U����  ��c m   P Q���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  a ��� l ��������  ��  ��  � ���� O  ���� O  ���� k  '��� ��� I 'N����
�� .corecrel****      � null�  � �~� 
�~ 
kocl� m  +.�}
�} 
plii  �|
�| 
insh  ;  13 �{�z
�{ 
prdt K  6H �y
�y 
kind m  9<�x
�x 
TEXT �w
�w 
pnam m  ?B		 �

  F o l d e r T o B a c k u p �v�u
�v 
valL o  CD�t�t 0 
foldername 
folderName�u  �z  �  I Ov�s�r
�s .corecrel****      � null�r   �q
�q 
kocl m  SV�p
�p 
plii �o
�o 
insh  ;  Y[ �n�m
�n 
prdt K  ^p �l
�l 
kind m  ad�k
�k 
TEXT �j
�j 
pnam m  gj �  B a c k u p D r i v e �i�h
�i 
valL o  kl�g�g 0 
backupdisk 
backupDisk�h  �m    I w��f�e
�f .corecrel****      � null�e   �d 
�d 
kocl m  {~�c
�c 
plii  �b!"
�b 
insh!  ;  ��" �a#�`
�a 
prdt# K  ��$$ �_%&
�_ 
kind% m  ���^
�^ 
TEXT& �]'(
�] 
pnam' m  ��)) �**  C o m p u t e r F o l d e r( �\+�[
�\ 
valL+ o  ���Z�Z  0 computerfolder computerFolder�[  �`   ,�Y, I ���X�W-
�X .corecrel****      � null�W  - �V./
�V 
kocl. m  ���U
�U 
plii/ �T01
�T 
insh0  ;  ��1 �S2�R
�S 
prdt2 K  ��33 �Q45
�Q 
kind4 m  ���P
�P 
TEXT5 �O67
�O 
pnam6 m  ��88 �99  S c h e d u l e d T i m e7 �N:�M
�N 
valL: o  ���L�L 0 
backuptime 
backupTime�M  �R  �Y  � l $;�K�J; I $�I�H<
�I .corecrel****      � null�H  < �G=>
�G 
kocl= m  �F
�F 
plif> �E?�D
�E 
prdt? K  @@ �CA�B
�C 
pnamA o  �A�A 	0 plist  �B  �D  �K  �J  � m  	BB�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��   CDC l ���@�?�>�@  �?  �>  D EFE r  ��GHG o  ���=�= 0 
foldername 
folderNameH o      �<�< 0 sourcefolder sourceFolderF IJI r  ��KLK o  ���;�; 0 
backupdisk 
backupDiskL o      �:�: "0 destinationdisk destinationDiskJ MNM r  ��OPO o  ���9�9  0 computerfolder computerFolderP o      �8�8 0 machinefolder machineFolderN QRQ r  ��STS o  ���7�7 0 
backuptime 
backupTimeT o      �6�6 0 scheduledtime scheduledTimeR UVU I ���5W�4
�5 .ascrcmnt****      � ****W J  ��XX YZY o  ���3�3 0 sourcefolder sourceFolderZ [\[ o  ���2�2 "0 destinationdisk destinationDisk\ ]^] o  ���1�1 0 machinefolder machineFolder^ _�0_ o  ���/�/ 0 scheduledtime scheduledTime�0  �4  V `a` l ���.�-�,�.  �-  �,  a b�+b I  ���*�)�(�* 0 diskinit diskInit�)  �(  �+  
 cdc l     �'�&�%�'  �&  �%  d efe l     �$�#�"�$  �#  �"  f ghg l     �!� ��!  �   �  h iji l     ����  �  �  j klk l     ����  �  �  l mnm l     �op�  o H B Function to detect if the selected hard drive is connected or not   p �qq �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o tn rsr l     �tu�  t T N This only happens once a hard drive has been selected and provides a reminder   u �vv �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e rs wxw i  RUyzy I      ���� 0 diskinit diskInit�  �  z Z     �{|�}{ l    	~��~ =     	� I     ���� 0 itexists itExists� ��� m    �� ���  d i s k� ��� o    �� "0 destinationdisk destinationDisk�  �  � m    �
� boovtrue�  �  | I    ���
� 0 freespaceinit freeSpaceInit� ��	� m    �
� boovfals�	  �
  �  } k    ��� ��� n   ��� I    ���� 0 setfocus setFocus�  �  �  f    � ��� r    $��� n    "��� 3     "�
� 
cobj� o     �� "0 messagesmissing messagesMissing� o      �� 0 msg  � ��� r   % ;��� l  % 9��� � n   % 9��� 1   5 9��
�� 
bhit� l  % 5������ I  % 5����
�� .sysodlogaskr        TEXT� o   % &���� 0 msg  � ����
�� 
appr� m   ' (�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� ����
�� 
disp� o   ) *���� 0 appicon appIcon� ����
�� 
btns� J   + /�� ��� m   + ,�� ���  C a n c e l   B a c k u p� ���� m   , -�� ���  O K��  � �����
�� 
dflt� m   0 1���� ��  ��  ��  �  �   � o      ���� 	0 reply  � ���� Z   < ������� l  < A������ =   < A��� o   < =���� 	0 reply  � m   = @�� ���  O K��  ��  � I   D I�������� 0 diskinit diskInit��  ��  ��  � Z   L �������� l  L W������ E   L W��� o   L Q���� &0 displaysleepstate displaySleepState� o   Q V���� 0 strawake strAwake��  ��  � k   Z ��� ��� r   Z c��� n   Z a��� 3   _ a��
�� 
cobj� o   Z _���� &0 messagescancelled messagesCancelled� o      ���� 0 msg  � ���� Z   d �������� l  d k������ =   d k��� o   d i���� 0 notifications  � m   i j��
�� boovtrue��  ��  � Z   n ������� l  n u������ =   n u��� o   n s���� 0 nsfw  � m   s t��
�� boovtrue��  ��  � I  x �����
�� .sysonotfnull��� ��� TEXT� o   x y���� 0 msg  � �����
�� 
appr� m   z }�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  ��  ��  ��  x ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  VY��� I      ������� 0 freespaceinit freeSpaceInit�  ��  o      ���� 	0 again  ��  ��  � Z     ��� l    	���� =     	 I     ������ 0 comparesizes compareSizes 	 o    ���� 0 sourcefolder sourceFolder	 
��
 o    ���� "0 destinationdisk destinationDisk��  ��   m    ��
�� boovtrue��  ��   I    �������� 0 
backupinit 
backupInit��  ��  ��   k    �  n    I    �������� 0 setfocus setFocus��  ��    f      l   ��������  ��  ��    Z    a�� l   ���� =     o    ���� 	0 again   m    ��
�� boovfals��  ��   r     6 l    4���� n     4 1   0 4��
�� 
bhit l    0���� I    0�� !
�� .sysodlogaskr        TEXT  m     !"" �## � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?! ��$%
�� 
appr$ m   " #&& �'' , W o a h ,   B a c k   T h e   F u c k   U p% ��()
�� 
disp( o   $ %���� 0 appicon appIcon) ��*+
�� 
btns* J   & *,, -.- m   & '// �00  Y e s. 1��1 m   ' (22 �33  C a n c e l   B a c k u p��  + ��4��
�� 
dflt4 m   + ,���� ��  ��  ��  ��  ��   o      ���� 	0 reply   565 l  9 <7����7 =   9 <898 o   9 :���� 	0 again  9 m   : ;��
�� boovtrue��  ��  6 :��: r   ? ];<; l  ? [=����= n   ? [>?> 1   W [��
�� 
bhit? l  ? W@����@ I  ? W��AB
�� .sysodlogaskr        TEXTA m   ? BCC �DD � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?B ��EF
�� 
apprE m   C FGG �HH , W o a h ,   B a c k   T h e   F u c k   U pF ��IJ
�� 
dispI o   G H�� 0 appicon appIconJ �~KL
�~ 
btnsK J   I QMM NON m   I LPP �QQ  Y e sO R�}R m   L OSS �TT  C a n c e l   B a c k u p�}  L �|U�{
�| 
dfltU m   R S�z�z �{  ��  ��  ��  ��  < o      �y�y 	0 reply  ��  ��   VWV l  b b�x�w�v�x  �w  �v  W X�uX Z   b �YZ�t[Y l  b g\�s�r\ =   b g]^] o   b c�q�q 	0 reply  ^ m   c f__ �``  Y e s�s  �r  Z I   j o�p�o�n�p 0 tidyup tidyUp�o  �n  �t  [ k   r �aa bcb Z   r �de�m�ld l  r }f�k�jf E   r }ghg o   r w�i�i &0 displaysleepstate displaySleepStateh o   w |�h�h 0 strawake strAwake�k  �j  e r   � �iji n   � �klk 3   � ��g
�g 
cobjl o   � ��f�f &0 messagescancelled messagesCancelledj o      �e�e 0 msg  �m  �l  c mnm l  � ��d�c�b�d  �c  �b  n o�ao Z   � �pq�`�_p l  � �r�^�]r =   � �sts o   � ��\�\ 0 notifications  t m   � ��[
�[ boovtrue�^  �]  q Z   � �uvw�Zu l  � �x�Y�Xx =   � �yzy o   � ��W�W 0 nsfw  z m   � ��V
�V boovtrue�Y  �X  v I  � ��U{|
�U .sysonotfnull��� ��� TEXT{ o   � ��T�T 0 msg  | �S}�R
�S 
appr} m   � �~~ � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�R  w ��� l  � ���Q�P� =   � ���� o   � ��O�O 0 nsfw  � m   � ��N
�N boovfals�Q  �P  � ��M� I  � ��L��
�L .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �K��J
�K 
appr� m   � ��� ��� 
 W B T F U�J  �M  �Z  �`  �_  �a  �u  � ��� l     �I�H�G�I  �H  �G  � ��� l     �F�E�D�F  �E  �D  � ��� l     �C�B�A�C  �B  �A  � ��� l     �@�?�>�@  �?  �>  � ��� l     �=�<�;�=  �<  �;  � ��� l     �:���:  �,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   � ���L   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .� ��� l     �9���9  � g a These folders, if required, are then set to their corresponding global variables declared above.   � ��� �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .� ��� i  Z]��� I      �8�7�6�8 0 
backupinit 
backupInit�7  �6  � k     ��� ��� r     ��� 4     �5�
�5 
psxf� o    �4�4 "0 destinationdisk destinationDisk� o      �3�3 	0 drive  � ��� l   �2���2  � ' !log (destinationDisk & "Backups")   � ��� B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )� ��� l   �1�0�/�1  �0  �/  � ��� Z    ,���.�-� l   ��,�+� =    ��� I    �*��)�* 0 itexists itExists� ��� m    	�� ���  f o l d e r� ��(� b   	 ��� o   	 
�'�' "0 destinationdisk destinationDisk� m   
 �� ���  B a c k u p s�(  �)  � m    �&
�& boovfals�,  �+  � O    (��� I   '�%�$�
�% .corecrel****      � null�$  � �#��
�# 
kocl� m    �"
�" 
cfol� �!��
�! 
insh� o    � �  	0 drive  � ���
� 
prdt� K    #�� ���
� 
pnam� m     !�� ���  B a c k u p s�  �  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �.  �-  � ��� l  - -����  �  �  � ��� Z   - d����� l  - >���� =   - >��� I   - <���� 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ��� b   / 8��� b   / 4��� o   / 0�� "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�� 0 machinefolder machineFolder�  �  � m   < =�
� boovfals�  �  � O   A `��� I  E _���
� .corecrel****      � null�  � ���
� 
kocl� m   G H�
� 
cfol� �
��
�
 
insh� n   I T��� 4   O T�	�
�	 
cfol� m   P S�� ���  B a c k u p s� 4   I O��
� 
cdis� o   M N�� 	0 drive  � ���
� 
prdt� K   U [�� ���
� 
pnam� o   V Y�� 0 machinefolder machineFolder�  �  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  �  � ��� l  e e�� ���  �   ��  � ��� O   e ~��� r   i }��� n   i y��� 4   t y�� 
�� 
cfol  o   u x���� 0 machinefolder machineFolder� n   i t 4   o t��
�� 
cfol m   p s �  B a c k u p s 4   i o��
�� 
cdis o   m n���� 	0 drive  � o      ���� 0 backupfolder backupFolder� m   e f�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 	 l   ��������  ��  ��  	 

 Z    ��� l   ����� =    � I    ������� 0 itexists itExists  m   � � �  f o l d e r �� b   � � b   � � b   � � o   � ����� "0 destinationdisk destinationDisk m   � � �  B a c k u p s / o   � ����� 0 machinefolder machineFolder m   � �   �!!  / L a t e s t��  ��   m   � ���
�� boovfals��  ��   O   � �"#" I  � �����$
�� .corecrel****      � null��  $ ��%&
�� 
kocl% m   � ���
�� 
cfol& ��'(
�� 
insh' o   � ����� 0 backupfolder backupFolder( ��)��
�� 
prdt) K   � �** ��+��
�� 
pnam+ m   � �,, �--  L a t e s t��  ��  # m   � �..�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��   r   � �/0/ m   � ���
�� boovfals0 o      ���� 0 initialbackup initialBackup 121 l  � ���������  ��  ��  2 343 O   � �565 r   � �787 n   � �9:9 4   � ���;
�� 
cfol; m   � �<< �==  L a t e s t: n   � �>?> 4   � ���@
�� 
cfol@ o   � ����� 0 machinefolder machineFolder? n   � �ABA 4   � ���C
�� 
cfolC m   � �DD �EE  B a c k u p sB 4   � ���F
�� 
cdisF o   � ����� 	0 drive  8 o      ���� 0 latestfolder latestFolder6 m   � �GG�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  4 HIH l  � ���������  ��  ��  I J��J I   � ��������� 
0 backup  ��  ��  ��  � KLK l     ��������  ��  ��  L MNM l     ��������  ��  ��  N OPO l     ��������  ��  ��  P QRQ l     ��������  ��  ��  R STS l     ��������  ��  ��  T UVU l     ��WX��  W j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.   X �YY �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .V Z[Z l     ��\]��  \ � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   ] �^^�   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .[ _`_ l     ��ab��  a � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   b �ccP   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .` ded l     ��fg��  f � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   g �hh>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .e iji i  ^aklk I      �������� 0 tidyup tidyUp��  ��  l k    �mm non r     pqp 4     ��r
�� 
psxfr o    ���� "0 destinationdisk destinationDiskq o      ���� 	0 drive  o sts l   ��������  ��  ��  t u��u O   �vwv k   �xx yzy r    {|{ n    }~} 4    ��
�� 
cfol o    ���� 0 machinefolder machineFolder~ n    ��� 4    ���
�� 
cfol� m    �� ���  B a c k u p s� 4    ���
�� 
cdis� o    ���� 	0 drive  | o      ���� 0 bf bFz ��� r    ��� n    ��� 1    ��
�� 
ascd� n    ��� 2   ��
�� 
cobj� o    ���� 0 bf bF� o      ���� 0 creationdates creationDates� ��� r     &��� n     $��� 4   ! $���
�� 
cobj� m   " #���� � o     !���� 0 creationdates creationDates� o      ���� 0 theoldestdate theOldestDate� ��� r   ' *��� m   ' (���� � o      ���� 0 j  � ��� l  + +��������  ��  ��  � ��� Y   + n�������� k   9 i�� ��� r   9 ?��� n   9 =��� 4   : =���
�� 
cobj� o   ; <���� 0 i  � o   9 :���� 0 creationdates creationDates� o      ���� 0 thisdate thisDate� ���� Z   @ i������� l  @ H������ >  @ H��� n   @ F��� 1   D F��
�� 
pnam� 4   @ D���
�� 
cobj� o   B C���� 0 i  � m   F G�� ���  L a t e s t��  ��  � Z   K e������� l  K N������ A   K N��� o   K L���� 0 thisdate thisDate� o   L M���� 0 theoldestdate theOldestDate��  ��  � k   Q a�� ��� r   Q T��� o   Q R���� 0 thisdate thisDate� o      ���� 0 theoldestdate theOldestDate� ��� r   U X��� o   U V���� 0 i  � o      ���� 0 j  � ��� l  Y Y������  � " log (item j of backupFolder)   � ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )� ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z�� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  � m   . /�~�~ � I  / 4�}��|
�} .corecnte****       ****� o   / 0�{�{ 0 creationdates creationDates�|  ��  � ��� l  o o�z�y�x�z  �y  �x  � ��� l  o o�w���w  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o�v���v  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� l  o o�u���u  �  delete item (j + 1) of bF   � ��� 2 d e l e t e   i t e m   ( j   +   1 )   o f   b F� ��� l  o o�t�s�r�t  �s  �r  � ��� r   o y��� c   o w��� l  o u��q�p� n   o u��� 4   p u�o�
�o 
cobj� l  q t��n�m� [   q t��� o   q r�l�l 0 j  � m   r s�k�k �n  �m  � o   o p�j�j 0 bf bF�q  �p  � m   u v�i
�i 
TEXT� o      �h�h  0 foldertodelete folderToDelete� ��� r   z ���� c   z ��� n   z }��� 1   { }�g
�g 
psxp� o   z {�f�f  0 foldertodelete folderToDelete� m   } ~�e
�e 
TEXT� o      �d�d  0 foldertodelete folderToDelete� ��� I  � ��c��b
�c .ascrcmnt****      � ****� l  � ���a�`� b   � ���� m   � ��� ��� $ f o l d e r   t o   d e l e t e :  � o   � ��_�_  0 foldertodelete folderToDelete�a  �`  �b  � ��� l  � ��^�]�\�^  �]  �\  � � � l  � ��[�Z�Y�[  �Z  �Y     r   � � b   � � b   � � m   � �		 �

  r m   - r f   ' o   � ��X�X  0 foldertodelete folderToDelete m   � � �  ' o      �W�W 0 todelete toDelete  I  � ��V�U
�V .sysoexecTEXT���     TEXT o   � ��T�T 0 todelete toDelete�U    l  � ��S�R�Q�S  �R  �Q    l  � ��P�O�N�P  �O  �N    n  � � I   � ��M�L�K�M 0 setfocus setFocus�L  �K    f   � �  r   � � l  � ��J�I n   � � 1   � ��H
�H 
bhit l  � ��G�F I  � ��E !
�E .sysodlogaskr        TEXT  m   � �"" �##B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .! �D$%
�D 
appr$ m   � �&& �'' , W o a h ,   B a c k   T h e   F u c k   U p% �C()
�C 
disp( o   � ��B�B 0 appicon appIcon) �A*+
�A 
btns* J   � �,, -.- m   � �// �00  E m p t y   T r a s h. 1�@1 m   � �22 �33  C a n c e l   B a c k u p�@  + �?4�>
�? 
dflt4 m   � ��=�= �>  �G  �F  �J  �I   o      �<�< 
0 reply2   565 Z   �@78�;97 l  � �:�:�9: =   � �;<; o   � ��8�8 
0 reply2  < m   � �== �>>  E m p t y   T r a s h�:  �9  8 I  � ��7?�6
�7 .fndremptnull��� ��� obj ? l  � �@�5�4@ 1   � ��3
�3 
trsh�5  �4  �6  �;  9 Z   �@AB�2�1A l  � �C�0�/C E   � �DED o   � ��.�. &0 displaysleepstate displaySleepStateE o   � ��-�- 0 strawake strAwake�0  �/  B k   �<FF GHG r   � �IJI n   � �KLK 3   � ��,
�, 
cobjL o   � ��+�+ &0 messagescancelled messagesCancelledJ o      �*�* 0 msg  H M�)M Z   �<NO�(�'N l  �P�&�%P =   �QRQ o   � �$�$ 0 notifications  R m   �#
�# boovtrue�&  �%  O Z  8STU�"S l V�!� V =  WXW o  
�� 0 nsfw  X m  
�
� boovtrue�!  �   T I �YZ
� .sysonotfnull��� ��� TEXTY o  �� 0 msg  Z �[�
� 
appr[ m  \\ �]] > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  U ^_^ l $`��` =  $aba o  "�� 0 nsfw  b m  "#�
� boovfals�  �  _ c�c I '4�de
� .sysonotfnull��� ��� TEXTd m  '*ff �gg , Y o u   s t o p p e d   b a c k i n g   u pe �h�
� 
apprh m  -0ii �jj 
 W B T F U�  �  �"  �(  �'  �)  �2  �1  6 klk l AA����  �  �  l mnm I  AG�o�� 0 freespaceinit freeSpaceInito p�p m  BC�
� boovtrue�  �  n q�
q Z  H�rs�	�r l HSt��t E  HSuvu o  HM�� &0 displaysleepstate displaySleepStatev o  MR�� 0 strawake strAwake�  �  s Z  V�wx��w l V]y�� y =  V]z{z o  V[���� 0 notifications  { m  [\��
�� boovtrue�  �   x Z  `�|}~��| l `g���� =  `g��� o  `e���� 0 nsfw  � m  ef��
�� boovtrue��  ��  } k  j}�� ��� I jw����
�� .sysonotfnull��� ��� TEXT� m  jm�� ��� � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .� �����
�� 
appr� m  ps�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I x}�����
�� .sysodelanull��� ��� nmbr� m  xy���� ��  ��  ~ ��� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� m  ���� ��� t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n� �����
�� 
appr� m  ���� ��� 
 W B T F U��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  �  �  �	  �  �
  w m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  j ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � M G Function that carries out the backup process and is split in 2 parts.    � ��� �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  � ��� l     ������  �*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   � ���H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .� ��� l     ������  � � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   � ���   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .� ��� i  be��� I      �������� 
0 backup  ��  ��  � k    ��� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovtrue��  ��  � o      ���� 0 t  � ��� l  	 	��������  ��  ��  � ��� r   	 ��� m   	 
��
�� boovtrue� o      ���� 0 isbackingup isBackingUp� ��� l   ��������  ��  ��  � ��� I    �������� (0 animatemenubaricon animateMenuBarIcon��  ��  � ��� I   �����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ��� l   ��������  ��  ��  � ��� I    #������� 0 	stopwatch  � ���� m    �� ��� 
 s t a r t��  ��  � ��� l  $ $��������  ��  ��  � ��� O   $y��� k   (x�� ��� l  ( (������  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d� ��� l  ( (������  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p� ��� r   ( 0��� c   ( .��� 4   ( ,���
�� 
psxf� o   * +���� 0 sourcefolder sourceFolder� m   , -��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   1 9��� n   1 7��� 1   5 7��
�� 
pnam� 4   1 5���
�� 
cfol� o   3 4���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  : :�� ��     log (folderName)		    � $ l o g   ( f o l d e r N a m e ) 	 	�  l  : :��������  ��  ��    Z   : ����� l  : A	����	 =   : A

 o   : ?���� 0 initialbackup initialBackup m   ? @��
�� boovfals��  ��   k   D �  I  D R����
�� .corecrel****      � null��   ��
�� 
kocl m   F G��
�� 
cfol ��
�� 
insh o   H I���� 0 backupfolder backupFolder ����
�� 
prdt K   J N ����
�� 
pnam o   K L���� 0 t  ��  ��    l  S S��������  ��  ��    r   S ] c   S Y 4   S W��
�� 
psxf o   U V���� 0 sourcefolder sourceFolder m   W X��
�� 
TEXT o      ���� (0 activesourcefolder activeSourceFolder  !  r   ^ h"#" 4   ^ d��$
�� 
cfol$ o   ` c���� (0 activesourcefolder activeSourceFolder# o      ���� (0 activesourcefolder activeSourceFolder! %&% r   i �'(' n   i ~)*) 4   { ~��+
�� 
cfol+ o   | }���� 0 t  * n   i {,-, 4   v {��.
�� 
cfol. o   w z���� 0 machinefolder machineFolder- n   i v/0/ 4   q v��1
�� 
cfol1 m   r u22 �33  B a c k u p s0 4   i q��4
�� 
cdis4 o   m p���� 	0 drive  ( o      ���� (0 activebackupfolder activeBackupFolder& 565 l  � ��78�  7 3 -log (activeSourceFolder & activeBackupFolder)   8 �99 Z l o g   ( a c t i v e S o u r c e F o l d e r   &   a c t i v e B a c k u p F o l d e r )6 :�~: I  � ��}�|;
�} .corecrel****      � null�|  ; �{<=
�{ 
kocl< m   � ��z
�z 
cfol= �y>?
�y 
insh> o   � ��x�x (0 activebackupfolder activeBackupFolder? �w@�v
�w 
prdt@ K   � �AA �uB�t
�u 
pnamB o   � ��s�s 0 
foldername 
folderName�t  �v  �~  ��  ��   CDC l  � ��r�q�p�r  �q  �p  D EFE l  � ��o�n�m�o  �n  �m  F GHG l  � ��lIJ�l  I o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   J �KK �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m eH LML l  � ��kNO�k  N q k An atomic version would just to do a straight backup without versioning to see the function at full effect   O �PP �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c tM QRQ l  � ��jST�j  S D > This means that only new or updated files will be copied over   T �UU |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e rR VWV l  � ��iXY�i  X ] W Any deleted files in the source folder will be deleted in the destination folder too		   Y �ZZ �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	W [\[ l  � ��h]^�h  ]   Original sync code    ^ �__ (   O r i g i n a l   s y n c   c o d e  \ `a` l  � ��gbc�g  b z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   c �dd �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "a efe l  � ��f�e�d�f  �e  �d  f ghg l  � ��cij�c  i   Original code   j �kk    O r i g i n a l   c o d eh lml l  � ��bno�b  n i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   o �pp �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t hm qrq l  � ��ast�a  s x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   t �uu �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .r vwv l  � ��`xy�`  x p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   y �zz �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .w {|{ l  � ��_}~�_  } � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   ~ ��   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .| ��� l  � ��^���^  � � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   � ���v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .� ��� l  � ��]�\�[�]  �\  �[  � ��� l  � ��Z�Y�X�Z  �Y  �X  � ��W� Z   �x����V� l  � ���U�T� =   � ���� o   � ��S�S 0 initialbackup initialBackup� m   � ��R
�R boovtrue�U  �T  � k   ���� ��� r   � ���� n   � ���� 1   � ��Q
�Q 
time� l  � ���P�O� I  � ��N�M�L
�N .misccurdldt    ��� null�M  �L  �P  �O  � o      �K�K 0 	starttime 	startTime� ��� Z   ����J�I� l  � ���H�G� E   � ���� o   � ��F�F &0 displaysleepstate displaySleepState� o   � ��E�E 0 strawake strAwake�H  �G  � Z   ����D�C� l  � ���B�A� =   � ���� o   � ��@�@ 0 notifications  � m   � ��?
�? boovtrue�B  �A  � Z   � �����>� l  � ���=�<� =   � ���� o   � ��;�; 0 nsfw  � m   � ��:
�: boovtrue�=  �<  � I  � ��9��
�9 .sysonotfnull��� ��� TEXT� m   � ��� ��� � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .� �8��7
�8 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�7  � ��� l  � ���6�5� =   � ���� o   � ��4�4 0 nsfw  � m   � ��3
�3 boovfals�6  �5  � ��2� I  � ��1��
�1 .sysonotfnull��� ��� TEXT� m   � ��� ��� \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e� �0��/
�0 
appr� m   � ��� ��� 
 W B T F U�/  �2  �>  �D  �C  �J  �I  � ��� l �.�-�,�.  �-  �,  � ��� l �+�*�)�+  �*  �)  � ��� l �(�'�&�(  �'  �&  � ��� l �%�$�#�%  �$  �#  � ��� r  ��� c  ��� l ��"�!� b  ��� b  ��� b  ��� b  ��� b  ��� o  	� �  "0 destinationdisk destinationDisk� m  	�� ���  B a c k u p s /� o  �� 0 machinefolder machineFolder� m  �� ���  / L a t e s t /� o  �� 0 
foldername 
folderName� m  �� ���  /�"  �!  � m  �
� 
TEXT� o      �� 0 d  � ��� l   ����  � A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   � ��� v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l   ����  �  �  � ��� Q   A���� k  #8�� ��� l ##����  ���do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' '" & sourceFolder & "' '" & d & "/'"   � ���< d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l ##����  �  �  � ��� l ##����  �   optimised(?)   � ���    o p t i m i s e d ( ? )� ��� I #6���
� .sysoexecTEXT���     TEXT� b  #2��� b  #.��� b  #,��� b  #(� � m  #& �	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   '  o  &'�� 0 sourcefolder sourceFolder� m  (+ �  '   '� o  ,-�� 0 d  � m  .1 �  / '�  �  l 77����  �  �   	�	 l 77�

�
  
 U Odo shell script "rsync -aqvz --cvs-exclude '" & sourceFolder & "' '" & d & "/'"    � � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "�  � R      �	��
�	 .ascrerr ****      � ****�  �  � l @@����  �  �  �  l BB����  �  �    l BB� �����   ��  ��    l BB��������  ��  ��    l BB��������  ��  ��    l BB��������  ��  ��    r  BI m  BC��
�� boovfals o      ���� 0 isbackingup isBackingUp  r  JQ m  JK��
�� boovfals o      ���� 0 manualbackup manualBackup   l RR��������  ��  ��    !"! Z  R�#$����# l R]%����% E  R]&'& o  RW���� &0 displaysleepstate displaySleepState' o  W\���� 0 strawake strAwake��  ��  $ k  `�(( )*) r  `m+,+ n  `i-.- 1  ei��
�� 
time. l `e/����/ I `e������
�� .misccurdldt    ��� null��  ��  ��  ��  , o      ���� 0 endtime endTime* 010 r  nu232 n ns454 I  os�������� 0 getduration getDuration��  ��  5  f  no3 o      ���� 0 duration  1 676 r  v�898 n  v:;: 3  {��
�� 
cobj; o  v{���� $0 messagescomplete messagesComplete9 o      ���� 0 msg  7 <=< Z  ��>?����> l ��@����@ =  ��ABA o  ������ 0 notifications  B m  ����
�� boovtrue��  ��  ? Z  ��CDE��C l ��F����F =  ��GHG o  ������ 0 nsfw  H m  ����
�� boovtrue��  ��  D k  ��II JKJ I ����LM
�� .sysonotfnull��� ��� TEXTL b  ��NON b  ��PQP b  ��RSR m  ��TT �UU 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  S o  ������ 0 duration  Q m  ��VV �WW    m i n u t e s ! 
O o  ������ 0 msg  M ��X��
�� 
apprX m  ��YY �ZZ : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  K [��[ I ����\��
�� .sysodelanull��� ��� nmbr\ m  ������ ��  ��  E ]^] l ��_����_ =  ��`a` o  ������ 0 nsfw  a m  ����
�� boovfals��  ��  ^ b��b k  ��cc ded I ����fg
�� .sysonotfnull��� ��� TEXTf b  ��hih b  ��jkj m  ��ll �mm 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  k o  ������ 0 duration  i m  ��nn �oo    m i n u t e s ! 
g ��p��
�� 
apprp m  ��qq �rr  B a c k e d   u p !��  e s��s I ����t��
�� .sysodelanull��� ��� nmbrt m  ������ ��  ��  ��  ��  ��  ��  = uvu l ����������  ��  ��  v w��w n ��xyx I  ���������� $0 resetmenubaricon resetMenuBarIcon��  ��  y  f  ����  ��  ��  " z{z l ����������  ��  ��  { |��| Z  ��}~����} l ������ =  ����� o  ������  0 backupthenquit backupThenQuit� m  ����
�� boovtrue��  ��  ~ I �������
�� .aevtquitnull��� ��� null� m  ����                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ��  ��  � ��� l 	������ =  	��� o  ���� 0 initialbackup initialBackup� m  ��
�� boovfals��  ��  � ���� k  t�� ��� r  +��� c  )��� l '������ b  '��� b  #��� b  !��� b  ��� b  ��� b  ��� b  ��� o  ���� "0 destinationdisk destinationDisk� m  �� ���  B a c k u p s /� o  ���� 0 machinefolder machineFolder� m  �� ���  /� o  ���� 0 t  � m   �� ���  /� o  !"���� 0 
foldername 
folderName� m  #&�� ���  /��  ��  � m  '(��
�� 
TEXT� o      ���� 0 c  � ��� r  ,E��� c  ,C��� l ,A������ b  ,A��� b  ,=��� b  ,;��� b  ,7��� b  ,3��� o  ,/���� "0 destinationdisk destinationDisk� m  /2�� ���  B a c k u p s /� o  36���� 0 machinefolder machineFolder� m  7:�� ���  / L a t e s t /� o  ;<���� 0 
foldername 
folderName� m  =@�� ���  /��  ��  � m  AB��
�� 
TEXT� o      ���� 0 d  � ��� I FO�����
�� .ascrcmnt****      � ****� l FK������ b  FK��� m  FI�� ���  b a c k i n g   u p   t o :  � o  IJ���� 0 d  ��  ��  ��  � ��� l PP��������  ��  ��  � ��� Z  P�������� l P[������ E  P[��� o  PU���� &0 displaysleepstate displaySleepState� o  UZ���� 0 strawake strAwake��  ��  � k  ^��� ��� r  ^k��� n  ^g��� 1  cg��
�� 
time� l ^c������ I ^c����~
�� .misccurdldt    ��� null�  �~  ��  ��  � o      �}�} 0 	starttime 	startTime� ��� r  lw��� n  lu��� 3  qu�|
�| 
cobj� o  lq�{�{ *0 messagesencouraging messagesEncouraging� o      �z�z 0 msg  � ��y� Z  x����x�w� l x��v�u� =  x��� o  x}�t�t 0 notifications  � m  }~�s
�s boovtrue�v  �u  � Z  ������r� l ����q�p� =  ����� o  ���o�o 0 nsfw  � m  ���n
�n boovtrue�q  �p  � k  ���� ��� I ���m��
�m .sysonotfnull��� ��� TEXT� o  ���l�l 0 msg  � �k��j
�k 
appr� m  ���� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�j  � ��i� I ���h��g
�h .sysodelanull��� ��� nmbr� m  ���f�f �g  �i  � ��� l ����e�d� =  ����� o  ���c�c 0 nsfw  � m  ���b
�b boovfals�e  �d  � ��a� k  ���� ��� I ���`��
�` .sysonotfnull��� ��� TEXT� m  ���� �	 	   B a c k i n g   u p� �_	�^
�_ 
appr	 m  ��		 �		 
 W B T F U�^  � 	�]	 I ���\	�[
�\ .sysodelanull��� ��� nmbr	 m  ���Z�Z �[  �]  �a  �r  �x  �w  �y  ��  ��  � 			 l ���Y�X�W�Y  �X  �W  	 				 l ���V�U�T�V  �U  �T  		 	
		
 l ���S�R�Q�S  �R  �Q  	 			 l ���P�O�N�P  �O  �N  	 			 l ���M�L�K�M  �L  �K  	 			 Q  ��				 k  ��		 			 l ���J		�J  	��do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings'  --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	 �		� d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "	 			 l ���I�H�G�I  �H  �G  	 			 l ���F		 �F  	   optimised(?)   	  �	!	!    o p t i m i s e d ( ? )	 	"	#	" I ���E	$�D
�E .sysoexecTEXT���     TEXT	$ b  ��	%	&	% b  ��	'	(	' b  ��	)	*	) b  ��	+	,	+ b  ��	-	.	- b  ��	/	0	/ m  ��	1	1 �	2	2
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '	0 o  ���C�C 0 c  	. m  ��	3	3 �	4	4  '   '	, o  ���B�B 0 sourcefolder sourceFolder	* m  ��	5	5 �	6	6  '   '	( o  ���A�A 0 d  	& m  ��	7	7 �	8	8  / '�D  	# 	9	:	9 l ���@�?�>�@  �?  �>  	: 	;�=	; l ���<	<	=�<  	< � zdo shell script "rsync -aqvz --cvs-exclude --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	= �	>	> � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "�=  	 R      �;�:�9
�; .ascrerr ****      � ****�:  �9  	 l ���8�7�6�8  �7  �6  	 	?	@	? l ���5�4�3�5  �4  �3  	@ 	A	B	A l ���2�1�0�2  �1  �0  	B 	C	D	C l ���/�.�-�/  �.  �-  	D 	E	F	E l ���,�+�*�,  �+  �*  	F 	G	H	G l ���)�(�'�)  �(  �'  	H 	I	J	I r  ��	K	L	K m  ���&
�& boovfals	L o      �%�% 0 isbackingup isBackingUp	J 	M	N	M r  �	O	P	O m  ���$
�$ boovfals	P o      �#�# 0 manualbackup manualBackup	N 	Q	R	Q l �"�!� �"  �!  �   	R 	S�	S Z  t	T	U�	V	T = 	W	X	W n  	Y	Z	Y 2 
�
� 
cobj	Z l 
	[��	[ c  
	\	]	\ 4  �	^
� 
psxf	^ o  �� 0 c  	] m  	�
� 
alis�  �  	X J  ��  	U k  j	_	_ 	`	a	` r  	b	c	b c  	d	e	d n  	f	g	f 4  �	h
� 
cfol	h o  �� 0 t  	g o  �� 0 backupfolder backupFolder	e m  �
� 
TEXT	c o      �� 0 oldestfolder oldestFolder	a 	i	j	i r  &	k	l	k c  $	m	n	m n  "	o	p	o 1  "�
� 
psxp	p o  �� 0 oldestfolder oldestFolder	n m  "#�
� 
TEXT	l o      �� 0 oldestfolder oldestFolder	j 	q	r	q I '0�	s�
� .ascrcmnt****      � ****	s l ',	t��
	t b  ',	u	v	u m  '*	w	w �	x	x  t o   d e l e t e :  	v o  *+�	�	 0 oldestfolder oldestFolder�  �
  �  	r 	y	z	y l 11����  �  �  	z 	{	|	{ r  1<	}	~	} b  1:		�	 b  16	�	�	� m  14	�	� �	�	�  r m   - r f   '	� o  45�� 0 oldestfolder oldestFolder	� m  69	�	� �	�	�  '	~ o      �� 0 todelete toDelete	| 	�	�	� I =B�	��
� .sysoexecTEXT���     TEXT	� o  =>�� 0 todelete toDelete�  	� 	�	�	� l CC� �����   ��  ��  	� 	���	� Z  Cj	�	�����	� l CN	�����	� E  CN	�	�	� o  CH���� &0 displaysleepstate displaySleepState	� o  HM���� 0 strawake strAwake��  ��  	� k  Qf	�	� 	�	�	� r  Q^	�	�	� n  QZ	�	�	� 1  VZ��
�� 
time	� l QV	�����	� I QV������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 endtime endTime	� 	�	�	� r  _f	�	�	� n _d	�	�	� I  `d�������� 0 getduration getDuration��  ��  	�  f  _`	� o      ���� 0 duration  	� 	�	�	� l gg��������  ��  ��  	� 	�	�	� r  gr	�	�	� n  gp	�	�	� 3  lp��
�� 
cobj	� o  gl���� $0 messagescomplete messagesComplete	� o      ���� 0 msg  	� 	�	�	� Z  s`	�	�����	� l sz	�����	� =  sz	�	�	� o  sx���� 0 notifications  	� m  xy��
�� boovtrue��  ��  	� k  }\	�	� 	�	�	� Z  }&	�	���	�	� l }�	�����	� =  }�	�	�	� o  }~���� 0 duration  	� m  ~�	�	� ?�      ��  ��  	� Z  ��	�	�	���	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� o  ������ 0 msg  	� ��	���
�� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  	� 	�	�	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovfals��  ��  	� 	���	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� ��	���
�� 
appr	� m  ��	�	� �	�	�  B a c k e d   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  ��  ��  ��  	� Z  �&	�	�	���	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e s ! 
	� o  ������ 0 msg  	� ��
 ��
�� 
appr
  m  ��

 �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  	� 


 l �
����
 =  �

	
 o  ����� 0 nsfw  
	 m  ��
�� boovfals��  ��  
 

��

 k  	"

 


 I 	��


�� .sysonotfnull��� ��� TEXT
 b  	


 b  	


 m  	

 �

  A n d   i n  
 o  ���� 0 duration  
 m  

 �

    m i n u t e s ! 

 ��
��
�� 
appr
 m  

 �

  B a c k e d   u p !��  
 
��
 I "��
��
�� .sysodelanull��� ��� nmbr
 m  ���� ��  ��  ��  ��  	� 


 l ''��������  ��  ��  
 
��
 Z  '\
 
!
"��
  l '.
#����
# =  '.
$
%
$ o  ',���� 0 nsfw  
% m  ,-��
�� boovtrue��  ��  
! I 1>��
&
'
�� .sysonotfnull��� ��� TEXT
& m  14
(
( �
)
) � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .
' ��
*��
�� 
appr
* m  7:
+
+ �
,
, @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  
" 
-
.
- l AH
/����
/ =  AH
0
1
0 o  AF���� 0 nsfw  
1 m  FG��
�� boovfals��  ��  
. 
2��
2 I KX��
3
4
�� .sysonotfnull��� ��� TEXT
3 m  KN
5
5 �
6
6 p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d
4 ��
7��
�� 
appr
7 m  QT
8
8 �
9
9 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  ��  ��  	� 
:
;
: l aa��������  ��  ��  
; 
<��
< n af
=
>
= I  bf�������� $0 resetmenubaricon resetMenuBarIcon��  ��  
>  f  ab��  ��  ��  ��  �  	V k  mt
?
? 
@
A
@ Z  mX
B
C����
B l mx
D����
D E  mx
E
F
E o  mr���� &0 displaysleepstate displaySleepState
F o  rw���� 0 strawake strAwake��  ��  
C k  {T
G
G 
H
I
H r  {�
J
K
J n  {�
L
M
L 1  ����
�� 
time
M l {�
N����
N I {������
�� .misccurdldt    ��� null��  �  ��  ��  
K o      �~�~ 0 endtime endTime
I 
O
P
O r  ��
Q
R
Q n ��
S
T
S I  ���}�|�{�} 0 getduration getDuration�|  �{  
T  f  ��
R o      �z�z 0 duration  
P 
U
V
U r  ��
W
X
W n  ��
Y
Z
Y 3  ���y
�y 
cobj
Z o  ���x�x $0 messagescomplete messagesComplete
X o      �w�w 0 msg  
V 
[�v
[ Z  �T
\
]�u�t
\ l ��
^�s�r
^ =  ��
_
`
_ o  ���q�q 0 notifications  
` m  ���p
�p boovtrue�s  �r  
] Z  �P
a
b�o
c
a l ��
d�n�m
d =  ��
e
f
e o  ���l�l 0 duration  
f m  ��
g
g ?�      �n  �m  
b Z  ��
h
i
j�k
h l ��
k�j�i
k =  ��
l
m
l o  ���h�h 0 nsfw  
m m  ���g
�g boovtrue�j  �i  
i k  ��
n
n 
o
p
o I ���f
q
r
�f .sysonotfnull��� ��� TEXT
q b  ��
s
t
s b  ��
u
v
u b  ��
w
x
w m  ��
y
y �
z
z  A n d   i n  
x o  ���e�e 0 duration  
v m  ��
{
{ �
|
|    m i n u t e ! 

t o  ���d�d 0 msg  
r �c
}�b
�c 
appr
} m  ��
~
~ �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�b  
p 
��a
� I ���`
��_
�` .sysodelanull��� ��� nmbr
� m  ���^�^ �_  �a  
j 
�
�
� l ��
��]�\
� =  ��
�
�
� o  ���[�[ 0 nsfw  
� m  ���Z
�Z boovfals�]  �\  
� 
��Y
� k  ��
�
� 
�
�
� I ���X
�
�
�X .sysonotfnull��� ��� TEXT
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  A n d   i n  
� o  ���W�W 0 duration  
� m  ��
�
� �
�
�    m i n u t e ! 

� �V
��U
�V 
appr
� m  ��
�
� �
�
�  B a c k e d   u p !�U  
� 
��T
� I ���S
��R
�S .sysodelanull��� ��� nmbr
� m  ���Q�Q �R  �T  �Y  �k  �o  
c Z  P
�
�
��P
� l 
��O�N
� =  
�
�
� o  �M�M 0 nsfw  
� m  �L
�L boovtrue�O  �N  
� k  &
�
� 
�
�
� I  �K
�
�
�K .sysonotfnull��� ��� TEXT
� b  
�
�
� b  
�
�
� b  
�
�
� m  
�
� �
�
�  A n d   i n  
� o  �J�J 0 duration  
� m  
�
� �
�
�    m i n u t e s ! 

� o  �I�I 0 msg  
� �H
��G
�H 
appr
� m  
�
� �
�
� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�G  
� 
��F
� I !&�E
��D
�E .sysodelanull��� ��� nmbr
� m  !"�C�C �D  �F  
� 
�
�
� l )0
��B�A
� =  )0
�
�
� o  ).�@�@ 0 nsfw  
� m  ./�?
�? boovfals�B  �A  
� 
��>
� k  3L
�
� 
�
�
� I 3F�=
�
�
�= .sysonotfnull��� ��� TEXT
� b  3<
�
�
� b  38
�
�
� m  36
�
� �
�
�  A n d   i n  
� o  67�<�< 0 duration  
� m  8;
�
� �
�
�    m i n u t e s ! 

� �;
��:
�; 
appr
� m  ?B
�
� �
�
�  B a c k e d   u p !�:  
� 
��9
� I GL�8
��7
�8 .sysodelanull��� ��� nmbr
� m  GH�6�6 �7  �9  �>  �P  �u  �t  �v  ��  ��  
A 
�
�
� l YY�5�4�3�5  �4  �3  
� 
�
�
� n Y^
�
�
� I  Z^�2�1�0�2 $0 resetmenubaricon resetMenuBarIcon�1  �0  
�  f  YZ
� 
�
�
� l __�/�.�-�/  �.  �-  
� 
��,
� Z  _t
�
��+�*
� l _f
��)�(
� =  _f
�
�
� o  _d�'�'  0 backupthenquit backupThenQuit
� m  de�&
�& boovtrue�)  �(  
� I ip�%
��$
�% .aevtquitnull��� ��� null
� m  il
�
�                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �$  �+  �*  �,  �  ��  �V  �W  � m   $ %
�
��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 
�
�
� l zz�#�"�!�#  �"  �!  
� 
�� 
� I  z��
��� 0 	stopwatch  
� 
��
� m  {~
�
� �
�
�  f i n i s h�  �  �   � 
�
�
� l     ����  �  �  
� 
�
�
� l     �
�
��  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �
�
��  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     ����  �  �  
� 
�
�
� l     ����  �  �  
� 
�
�
� l     ����  �  �  
� 
�
�
� l     ����  �  �  
� 
�
�
� l     ��
�	�  �
  �	  
� 
�
�
� l     �
�
��  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
� 
� l     ��   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   l     ��   � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------    � - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 	
	 l     ����  �  �  
  l     ��   � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.    �   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .  l     ��   � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.    ��   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .  i  fi I      � ���  0 itexists itExists  o      ���� 0 
objecttype 
objectType �� o      ���� 
0 object  ��  ��   l    W O     W !  Z    V"#$��" l   %����% =    &'& o    ���� 0 
objecttype 
objectType' m    (( �))  d i s k��  ��  # Z   
 *+��,* I  
 ��-��
�� .coredoexnull���     ****- 4   
 ��.
�� 
cdis. o    ���� 
0 object  ��  + L    // m    ��
�� boovtrue��  , L    00 m    ��
�� boovfals$ 121 l   "3����3 =    "454 o     ���� 0 
objecttype 
objectType5 m     !66 �77  f i l e��  ��  2 898 Z   % 7:;��<: I  % -��=��
�� .coredoexnull���     ****= 4   % )��>
�� 
file> o   ' (���� 
0 object  ��  ; L   0 2?? m   0 1��
�� boovtrue��  < L   5 7@@ m   5 6��
�� boovfals9 ABA l  : =C����C =   : =DED o   : ;���� 0 
objecttype 
objectTypeE m   ; <FF �GG  f o l d e r��  ��  B H��H Z   @ RIJ��KI I  @ H��L��
�� .coredoexnull���     ****L 4   @ D��M
�� 
cfolM o   B C���� 
0 object  ��  J L   K MNN m   K L��
�� boovtrue��  K L   P ROO m   P Q��
�� boovfals��  ��  ! m     PP�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��   "  (string, string) as Boolean    �QQ 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n RSR l     ��������  ��  ��  S TUT l     ��������  ��  ��  U VWV l     ��������  ��  ��  W XYX l     ��������  ��  ��  Y Z[Z l     ��������  ��  ��  [ \]\ l     ��^_��  ^ � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   _ �``Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .] aba l     ��cd��  c P J This means that backups can be identified from what machine they're from.   d �ee �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .b fgf i  jmhih I      �������� .0 getcomputeridentifier getComputerIdentifier��  ��  i k     jj klk r     	mnm n     opo 1    ��
�� 
sicnp l    q����q I    ������
�� .sysosigtsirr   ��� null��  ��  ��  ��  n o      ���� 0 computername computerNamel rsr r   
 tut I  
 ��v��
�� .sysoexecTEXT���     TEXTv m   
 ww �xx � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  ��  u o      ���� "0 strserialnumber strSerialNumbers yzy r    {|{ l   }����} b    ~~ b    ��� o    ���� 0 computername computerName� m    �� ���    -   o    ���� "0 strserialnumber strSerialNumber��  ��  | o      ����  0 identifiername identifierNamez ���� L    �� o    ����  0 identifiername identifierName��  g ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   � ���   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .� ��� l     ������  � � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   � ���   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .� ��� i  nq��� I      ������� 0 gettimestamp getTimestamp� ���� o      ���� 0 isfolder isFolder��  ��  � l   ����� k    ��� ��� l     ������  �   Date variables   � ���    D a t e   v a r i a b l e s� ��� r     )��� l     ������ I     ������
�� .misccurdldt    ��� null��  ��  ��  ��  � K    �� ����
�� 
year� o    ���� 0 y  � ����
�� 
mnth� o    ���� 0 m  � ����
�� 
day � o    ���� 0 d  � �����
�� 
time� o   	 
���� 0 t  ��  � ��� r   * 1��� c   * /��� l  * -������ c   * -��� o   * +���� 0 y  � m   + ,��
�� 
long��  ��  � m   - .��
�� 
TEXT� o      ���� 0 ty tY� ��� r   2 9��� c   2 7��� l  2 5������ c   2 5��� o   2 3���� 0 y  � m   3 4��
�� 
long��  ��  � m   5 6��
�� 
TEXT� o      ���� 0 ty tY� ��� r   : A��� c   : ?��� l  : =������ c   : =��� o   : ;���� 0 m  � m   ; <��
�� 
long��  ��  � m   = >��
�� 
TEXT� o      ���� 0 tm tM� ��� r   B I��� c   B G��� l  B E����� c   B E��� o   B C�~�~ 0 d  � m   C D�}
�} 
long��  �  � m   E F�|
�| 
TEXT� o      �{�{ 0 td tD� ��� r   J Q��� c   J O��� l  J M��z�y� c   J M��� o   J K�x�x 0 t  � m   K L�w
�w 
long�z  �y  � m   M N�v
�v 
TEXT� o      �u�u 0 tt tT� ��� l  R R�t�s�r�t  �s  �r  � ��� l  R R�q���q  � U O Append the month or day with a 0 if the string length is only 1 character long   � ��� �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g� ��� r   R [��� c   R Y��� l  R W��p�o� I  R W�n��m
�n .corecnte****       ****� o   R S�l�l 0 tm tM�m  �p  �o  � m   W X�k
�k 
nmbr� o      �j�j 
0 tml tML� ��� r   \ e��� c   \ c��� l  \ a��i�h� I  \ a�g��f
�g .corecnte****       ****� o   \ ]�e�e 0 tm tM�f  �i  �h  � m   a b�d
�d 
nmbr� o      �c�c 
0 tdl tDL� ��� l  f f�b�a�`�b  �a  �`  � ��� Z   f u���_�^� l  f i �]�\  =   f i o   f g�[�[ 
0 tml tML m   g h�Z�Z �]  �\  � r   l q b   l o m   l m �  0 o   m n�Y�Y 0 tm tM o      �X�X 0 tm tM�_  �^  � 	
	 l  v v�W�V�U�W  �V  �U  
  Z   v ��T�S l  v y�R�Q =   v y o   v w�P�P 
0 tdl tDL m   w x�O�O �R  �Q   r   | � b   | � m   |  �  0 o    ��N�N 0 td tD o      �M�M 0 td tD�T  �S    l  � ��L�K�J�L  �K  �J    l  � ��I�H�G�I  �H  �G    l  � ��F�F     Time variables	    �       T i m e   v a r i a b l e s 	 !"! l  � ��E#$�E  #  	 Get hour   $ �%%    G e t   h o u r" &'& r   � �()( n   � �*+* 1   � ��D
�D 
tstr+ l  � �,�C�B, I  � ��A�@�?
�A .misccurdldt    ��� null�@  �?  �C  �B  ) o      �>�> 0 timestr timeStr' -.- r   � �/0/ I  � �1�=21 z�<�;
�< .sysooffslong    ��� null
�; misccura�=  2 �:34
�: 
psof3 m   � �55 �66  :4 �97�8
�9 
psin7 o   � ��7�7 0 timestr timeStr�8  0 o      �6�6 0 pos  . 898 r   � �:;: c   � �<=< n   � �>?> 7  � ��5@A
�5 
cha @ m   � ��4�4 A l  � �B�3�2B \   � �CDC o   � ��1�1 0 pos  D m   � ��0�0 �3  �2  ? o   � ��/�/ 0 timestr timeStr= m   � ��.
�. 
TEXT; o      �-�- 0 h  9 EFE r   � �GHG c   � �IJI n   � �KLK 7 � ��,MN
�, 
cha M l  � �O�+�*O [   � �PQP o   � ��)�) 0 pos  Q m   � ��(�( �+  �*  N  ;   � �L o   � ��'�' 0 timestr timeStrJ m   � ��&
�& 
TEXTH o      �%�% 0 timestr timeStrF RSR l  � ��$�#�"�$  �#  �"  S TUT l  � ��!VW�!  V   Get minute   W �XX    G e t   m i n u t eU YZY r   � �[\[ I  � �]� ^] z��
� .sysooffslong    ��� null
� misccura�   ^ �_`
� 
psof_ m   � �aa �bb  :` �c�
� 
psinc o   � ��� 0 timestr timeStr�  \ o      �� 0 pos  Z ded r   �fgf c   �hih n   � jkj 7  � �lm
� 
cha l m   � ��� m l  � �n��n \   � �opo o   � ��� 0 pos  p m   � ��� �  �  k o   � ��� 0 timestr timeStri m   �
� 
TEXTg o      �� 0 m  e qrq r  sts c  uvu n  wxw 7�yz
� 
cha y l {��{ [  |}| o  �� 0 pos  } m  �� �  �  z  ;  x o  �
�
 0 timestr timeStrv m  �	
�	 
TEXTt o      �� 0 timestr timeStrr ~~ l ����  �  �   ��� l ����  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  2��� I 0���� z��
� .sysooffslong    ��� null
� misccura�  � � ��
�  
psof� m  "%�� ���   � �����
�� 
psin� o  ()���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  3E��� c  3C��� n  3A��� 74A����
�� 
cha � l :>������ [  :>��� o  ;<���� 0 pos  � m  <=���� ��  ��  �  ;  ?@� o  34���� 0 timestr timeStr� m  AB��
�� 
TEXT� o      ���� 0 s  � ��� l FF��������  ��  ��  � ��� l FF��������  ��  ��  � ��� Z  F������ l FI������ =  FI��� o  FG���� 0 isfolder isFolder� m  GH��
�� boovtrue��  ��  � k  La�� ��� l LL������  �   Create folder timestamp   � ��� 0   C r e a t e   f o l d e r   t i m e s t a m p� ���� r  La��� b  L_��� b  L]��� b  L[��� b  LY��� b  LU��� b  LS��� b  LQ��� m  LO�� ���  b a c k u p _� o  OP���� 0 ty tY� o  QR���� 0 tm tM� o  ST���� 0 td tD� m  UX�� ���  _� o  YZ���� 0 h  � o  [\���� 0 m  � o  ]^���� 0 s  � o      ���� 0 	timestamp  ��  � ��� l dg������ =  dg��� o  de���� 0 isfolder isFolder� m  ef��
�� boovfals��  ��  � ���� k  j{�� ��� l jj������  �   Create timestamp   � ��� "   C r e a t e   t i m e s t a m p� ���� r  j{��� b  jy��� b  jw��� b  ju��� b  js��� b  jo��� b  jm��� o  jk���� 0 ty tY� o  kl���� 0 tm tM� o  mn���� 0 td tD� m  or�� ���  _� o  st���� 0 h  � o  uv���� 0 m  � o  wx���� 0 s  � o      ���� 0 	timestamp  ��  ��  ��  � ��� l ����������  ��  ��  � ���� L  ���� o  ������ 0 	timestamp  ��  �  	(boolean)   � ���  ( b o o l e a n )� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   � ��� �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .� ��� l     ������  � w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   � ��� �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .� ��� i  ru��� I      ������� 0 comparesizes compareSizes� � � o      ���� 
0 source    �� o      ���� 0 destination  ��  ��  � l   � k    �  r     	 m     ��
�� boovtrue	 o      ���� 0 fit   

 r    
 4    ��
�� 
psxf o    ���� 0 destination   o      ���� 0 destination    l   ��������  ��  ��    r     b     b     b     o    ���� "0 destinationdisk destinationDisk m     �  B a c k u p s / o    ���� 0 machinefolder machineFolder m     �  / L a t e s t o      ���� 0 
templatest 
tempLatest   r    !"! m    ����  " o      ���� $0 latestfoldersize latestFolderSize  #$# l   ��������  ��  ��  $ %&% r    &'(' b    $)*) n   "+,+ 1     "��
�� 
psxp, l    -����- I    ��./
�� .earsffdralis        afdr. m    ��
�� afdrdlib/ ��0��
�� 
from0 m    ��
�� fldmfldu��  ��  ��  * m   " #11 �22  C a c h e s( o      ���� 0 c  & 343 r   ' 4565 b   ' 2787 n  ' 09:9 1   . 0��
�� 
psxp: l  ' .;����; I  ' .��<=
�� .earsffdralis        afdr< m   ' (��
�� afdrdlib= ��>��
�� 
from> m   ) *��
�� fldmfldu��  ��  ��  8 m   0 1?? �@@  L o g s6 o      ���� 0 l  4 ABA r   5 BCDC b   5 @EFE n  5 >GHG 1   < >��
�� 
psxpH l  5 <I����I I  5 <��JK
�� .earsffdralis        afdrJ m   5 6��
�� afdrdlibK ��L��
�� 
fromL m   7 8��
�� fldmfldu��  ��  ��  F m   > ?MM �NN   M o b i l e   D o c u m e n t sD o      ���� 0 md  B OPO r   C FQRQ m   C D����  R o      ���� 0 	cachesize 	cacheSizeP STS r   G JUVU m   G H����  V o      ���� 0 logssize logsSizeT WXW r   K NYZY m   K L����  Z o      ���� 0 mdsize mdSizeX [\[ l  O O��������  ��  ��  \ ]^] r   O R_`_ m   O Paa ?�      ` o      ���� 
0 buffer  ^ bcb l  S S�������  ��  �  c ded r   S Vfgf m   S T�~�~  g o      �}�} (0 adjustedfoldersize adjustedFolderSizee hih l  W W�|�{�z�|  �{  �z  i jkj O   W�lml k   [�nn opo r   [ hqrq I  [ f�ys�x
�y .sysoexecTEXT���     TEXTs b   [ btut b   [ ^vwv m   [ \xx �yy  d u   - m s   'w o   \ ]�w�w 
0 source  u m   ^ azz �{{  '   |   c u t   - f   1�x  r o      �v�v 0 
foldersize 
folderSizep |}| r   i �~~ ^   i ���� l  i }��u�t� I  i }���s� z�r�q
�r .sysorondlong        doub
�q misccura� ]   o x��� l  o t��p�o� ^   o t��� o   o p�n�n 0 
foldersize 
folderSize� m   p s�m�m �p  �o  � m   t w�l�l d�s  �u  �t  � m   } ��k�k d o      �j�j 0 
foldersize 
folderSize} ��� l  � ��i�h�g�i  �h  �g  � ��� r   � ���� ^   � ���� ^   � ���� ^   � ���� l  � ���f�e� l  � ���d�c� n   � ���� 1   � ��b
�b 
frsp� 4   � ��a�
�a 
cdis� o   � ��`�` 0 destination  �d  �c  �f  �e  � m   � ��_�_ � m   � ��^�^ � m   � ��]�] � o      �\�\ 0 	freespace 	freeSpace� ��� r   � ���� ^   � ���� l  � ���[�Z� I  � ����Y� z�X�W
�X .sysorondlong        doub
�W misccura� l  � ���V�U� ]   � ���� o   � ��T�T 0 	freespace 	freeSpace� m   � ��S�S d�V  �U  �Y  �[  �Z  � m   � ��R�R d� o      �Q�Q 0 	freespace 	freeSpace� ��� l  � ��P�O�N�P  �O  �N  � ��� r   � ���� I  � ��M��L
�M .sysoexecTEXT���     TEXT� b   � ���� b   � ���� m   � ��� ���  d u   - m s   '� o   � ��K�K 0 c  � m   � ��� ���  '   |   c u t   - f   1�L  � o      �J�J 0 	cachesize 	cacheSize� ��� r   � ���� ^   � ���� l  � ���I�H� I  � ����G� z�F�E
�F .sysorondlong        doub
�E misccura� ]   � ���� l  � ���D�C� ^   � ���� o   � ��B�B 0 	cachesize 	cacheSize� m   � ��A�A �D  �C  � m   � ��@�@ d�G  �I  �H  � m   � ��?�? d� o      �>�> 0 	cachesize 	cacheSize� ��� l  � ��=�<�;�=  �<  �;  � ��� r   � ���� I  � ��:��9
�: .sysoexecTEXT���     TEXT� b   � ���� b   � ���� m   � ��� ���  d u   - m s   '� o   � ��8�8 0 l  � m   � ��� ���  '   |   c u t   - f   1�9  � o      �7�7 0 logssize logsSize� ��� r   �	��� ^   ���� l  ���6�5� I  ����4� z�3�2
�3 .sysorondlong        doub
�2 misccura� ]   � ���� l  � ���1�0� ^   � ���� o   � ��/�/ 0 logssize logsSize� m   � ��.�. �1  �0  � m   � ��-�- d�4  �6  �5  � m  �,�, d� o      �+�+ 0 logssize logsSize� ��� l 

�*�)�(�*  �)  �(  � ��� r  
��� I 
�'��&
�' .sysoexecTEXT���     TEXT� b  
��� b  
��� m  
�� ���  d u   - m s   '� o  �%�% 0 md  � m  �� ���  '   |   c u t   - f   1�&  � o      �$�$ 0 mdsize mdSize� ��� r  4��� ^  2��� l .��#�"� I .���!� z� �
�  .sysorondlong        doub
� misccura� ]   )��� l  %���� ^   %��� o   !�� 0 mdsize mdSize� m  !$�� �  �  � m  %(�� d�!  �#  �"  � m  .1�� d� o      �� 0 mdsize mdSize� ��� l 55����  �  �  �    r  5> l 5<�� \  5< \  5: \  58	
	 o  56�� 0 
foldersize 
folderSize
 o  67�� 0 	cachesize 	cacheSize o  89�� 0 logssize logsSize o  :;�� 0 mdsize mdSize�  �   o      �� (0 adjustedfoldersize adjustedFolderSize  l ??����  �  �    I ?H�
�	
�
 .ascrcmnt****      � **** l ?D�� b  ?D b  ?B o  ?@�� 0 
foldersize 
folderSize o  @A�� (0 adjustedfoldersize adjustedFolderSize o  BC�� 0 	freespace 	freeSpace�  �  �	    l II����  �  �   �  Z  I����� l IP���� =  IP o  IN���� 0 initialbackup initialBackup m  NO��
�� boovfals��  ��   k  S  r  Sb !  I S`��"��
�� .sysoexecTEXT���     TEXT" b  S\#$# b  SX%&% m  SV'' �((  d u   - m s   '& o  VW���� 0 
templatest 
tempLatest$ m  X[)) �**  '   |   c u t   - f   1��  ! o      ���� $0 latestfoldersize latestFolderSize +,+ r  c}-.- ^  c{/0/ l cw1����1 I cw23��2 z����
�� .sysorondlong        doub
�� misccura3 ]  ir454 l in6����6 ^  in787 o  ij���� $0 latestfoldersize latestFolderSize8 m  jm���� ��  ��  5 m  nq���� d��  ��  ��  0 m  wz���� d. o      ���� $0 latestfoldersize latestFolderSize, 9��9 l ~~��������  ��  ��  ��  ��  ��  �   m m   W X::�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  k ;<; l ����������  ��  ��  < =>= Z  ��?@A��? l ��B����B =  ��CDC o  ������ 0 initialbackup initialBackupD m  ����
�� boovfals��  ��  @ k  ��EE FGF r  ��HIH \  ��JKJ o  ������ (0 adjustedfoldersize adjustedFolderSizeK o  ������ $0 latestfoldersize latestFolderSizeI o      ���� 0 temp  G LML l ����������  ��  ��  M NON Z  ��PQ����P l ��R����R A  ��STS o  ������ 0 temp  T m  ������  ��  ��  Q r  ��UVU \  ��WXW l ��Y����Y o  ������ 0 temp  ��  ��  X l ��Z����Z ]  ��[\[ l ��]����] o  ������ 0 temp  ��  ��  \ m  ������ ��  ��  V o      ���� 0 temp  ��  ��  O ^_^ l ����������  ��  ��  _ `��` Z  ��ab����a l ��c����c ?  ��ded o  ������ 0 temp  e l ��f����f \  ��ghg o  ������ 0 	freespace 	freeSpaceh o  ������ 
0 buffer  ��  ��  ��  ��  b r  ��iji m  ����
�� boovfalsj o      ���� 0 fit  ��  ��  ��  A klk l ��m����m =  ��non o  ������ 0 initialbackup initialBackupo m  ����
�� boovtrue��  ��  l p��p Z  ��qr����q l ��s����s ?  ��tut o  ������ (0 adjustedfoldersize adjustedFolderSizeu l ��v����v \  ��wxw o  ������ 0 	freespace 	freeSpacex o  ������ 
0 buffer  ��  ��  ��  ��  r r  ��yzy m  ����
�� boovfalsz o      ���� 0 fit  ��  ��  ��  ��  > {|{ l ����������  ��  ��  | }��} L  ��~~ o  ������ 0 fit  ��    (string, string)    �   ( s t r i n g ,   s t r i n g )� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i  vy��� I      ������� 0 	stopwatch  � ���� o      ���� 0 mode  ��  ��  � l    3���� k     3�� ��� q      �� ������ 0 x  ��  � ���� Z     3������ l    ������ =     ��� o     ���� 0 mode  � m    �� ��� 
 s t a r t��  ��  � k    �� ��� r    ��� I    ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovfals��  ��  � o      ���� 0 x  � ���� I   �����
�� .ascrcmnt****      � ****� l   ����� b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    �~�~ 0 x  ��  �  ��  ��  � ��� l   ��}�|� =    ��� o    �{�{ 0 mode  � m    �� ���  f i n i s h�}  �|  � ��z� k    /�� ��� r    '��� I    %�y��x�y 0 gettimestamp getTimestamp� ��w� m     !�v
�v boovfals�w  �x  � o      �u�u 0 x  � ��t� I  ( /�s��r
�s .ascrcmnt****      � ****� l  ( +��q�p� b   ( +��� m   ( )�� ��� " b a c k u p   f i n i s h e d :  � o   ) *�o�o 0 x  �q  �p  �r  �t  �z  ��  ��  �  (string)   � ���  ( s t r i n g )� ��� l     �n�m�l�n  �m  �l  � ��� l     �k�j�i�k  �j  �i  � ��� l     �h�g�f�h  �g  �f  � ��� l     �e�d�c�e  �d  �c  � ��� l     �b�a�`�b  �a  �`  � ��� l     �_���_  � M G Utility function to get the duration of the backup process in minutes.   � ��� �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .� ��� i  z}��� I      �^�]�\�^ 0 getduration getDuration�]  �\  � k     �� ��� r     ��� ^     ��� l    ��[�Z� \     ��� o     �Y�Y 0 endtime endTime� o    �X�X 0 	starttime 	startTime�[  �Z  � m    �W�W <� o      �V�V 0 duration  � ��� r    ��� ^    ��� l   ��U�T� I   ���S� z�R�Q
�R .sysorondlong        doub
�Q misccura� l   ��P�O� ]    ��� o    �N�N 0 duration  � m    �M�M d�P  �O  �S  �U  �T  � m    �L�L d� o      �K�K 0 duration  � ��J� L    �� o    �I�I 0 duration  �J  � ��� l     �H�G�F�H  �G  �F  � ��� l     �E�D�C�E  �D  �C  � ��� l     �B�A�@�B  �A  �@  � ��� l     �?�>�=�?  �>  �=  � ��� l     �<�;�:�<  �;  �:  � � � l     �9�9   ; 5 Utility function bring WBTFU to the front with focus    � j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s   i  ~� I      �8�7�6�8 0 setfocus setFocus�7  �6   O     
	 I   	�5�4�3
�5 .miscactvnull��� ��� null�4  �3  	 m     

                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��    l     �2�1�0�2  �1  �0    l     �/�/   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     �.�.   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     �-�,�+�-  �,  �+    l     �*�)�(�*  �)  �(    l     �'�&�%�'  �&  �%    l     �$�#�"�$  �#  �"     l     �!� ��!  �   �    !"! l     �#$�  # � �-----------------------------------------------------------------------------------------------------------------------------------------------   $ �%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" &'& l     �()�  ( � �-----------------------------------------------------------------------------------------------------------------------------------------------   ) �** - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -' +,+ l     �-.�  - � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   . �// - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -, 010 l     ����  �  �  1 232 i  ��454 I      �6�� $0 menuneedsupdate_ menuNeedsUpdate_6 7�7 l     8��8 m      �
� 
cmnu�  �  �  �  5 k     99 :;: l     �<=�  < J D NSMenu's delegates method, when the menu is clicked this is called.   = �>> �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .; ?@? l     �AB�  A j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   B �CC �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .@ DED l     �FG�  F < 6 This means the menu items can be changed dynamically.   G �HH l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .E I�I n    JKJ I    ���� 0 	makemenus 	makeMenus�  �  K  f     �  3 LML l     ��
�	�  �
  �	  M NON l     ����  �  �  O PQP l     ����  �  �  Q RSR l     ��� �  �  �   S TUT l     ��������  ��  ��  U VWV i  ��XYX I      �������� 0 	makemenus 	makeMenus��  ��  Y k    >ZZ [\[ l    	]^_] n    	`a` I    	��������  0 removeallitems removeAllItems��  ��  a o     ���� 0 newmenu newMenu^ !  remove existing menu items   _ �bb 6   r e m o v e   e x i s t i n g   m e n u   i t e m s\ cdc l  
 
��������  ��  ��  d e��e Y   
>f��gh��f k   9ii jkj r    'lml n    %non 4   " %��p
�� 
cobjp o   # $���� 0 i  o o    "���� 0 thelist theListm o      ���� 0 	this_item  k qrq l  ( (��������  ��  ��  r sts Z   ( uvw��u l  ( -x����x =   ( -yzy l  ( +{����{ c   ( +|}| o   ( )���� 0 	this_item  } m   ) *��
�� 
TEXT��  ��  z m   + ,~~ �  B a c k u p   n o w��  ��  v r   0 @��� l  0 >������ n  0 >��� I   7 >������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8���� 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ���� m   9 :�� ���  ��  ��  � n  0 7��� I   3 7�������� 	0 alloc  ��  ��  � n  0 3��� o   1 3���� 0 
nsmenuitem 
NSMenuItem� m   0 1��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItemw ��� l  C H������ =   C H��� l  C F������ c   C F��� o   C D���� 0 	this_item  � m   D E��
�� 
TEXT��  ��  � m   F G�� ��� " B a c k u p   n o w   &   q u i t��  ��  � ��� r   K [��� l  K Y������ n  K Y��� I   R Y������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S���� 0 	this_item  � ��� m   S T�� ��� * b a c k u p A n d Q u i t H a n d l e r :� ���� m   T U�� ���  ��  ��  � n  K R��� I   N R�������� 	0 alloc  ��  ��  � n  K N��� o   L N���� 0 
nsmenuitem 
NSMenuItem� m   K L��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  ^ c������ =   ^ c��� l  ^ a������ c   ^ a��� o   ^ _���� 0 	this_item  � m   _ `��
�� 
TEXT��  ��  � m   a b�� ��� " T u r n   o n   t h e   b a n t s��  ��  � ��� r   f x��� l  f v������ n  f v��� I   m v������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n���� 0 	this_item  � ��� m   n o�� ���  n s f w O n H a n d l e r :� ���� m   o r�� ���  ��  ��  � n  f m��� I   i m�������� 	0 alloc  ��  ��  � n  f i��� o   g i���� 0 
nsmenuitem 
NSMenuItem� m   f g��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  { ������� =   { ���� l  { ~������ c   { ~��� o   { |���� 0 	this_item  � m   | }��
�� 
TEXT��  ��  � m   ~ ��� ��� $ T u r n   o f f   t h e   b a n t s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ���  n s f w O f f H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ��� * T u r n   o n   n o t i f i c a t i o n s��  ��  � ��� r   � ���� l  � ������� n  � �� � I   � ������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   � ����� 0 	this_item    m   � � � , n o t i f i c a t i o n O n H a n d l e r : �� m   � �		 �

  ��  ��    n  � � I   � ��������� 	0 alloc  ��  ��   n  � � o   � ����� 0 
nsmenuitem 
NSMenuItem m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem�  l  � ����� =   � � l  � ����� c   � � o   � ����� 0 	this_item   m   � ���
�� 
TEXT��  ��   m   � � � , T u r n   o f f   n o t i f i c a t i o n s��  ��    r   � � l  � ����� n  � � I   � ��� ���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  !"! o   � ����� 0 	this_item  " #$# m   � �%% �&& . n o t i f i c a t i o n O f f H a n d l e r :$ '��' m   � �(( �))  ��  ��   n  � �*+* I   � �������� 	0 alloc  ��  �  + n  � �,-, o   � ��~�~ 0 
nsmenuitem 
NSMenuItem- m   � ��}
�} misccura��  ��   o      �|�| 0 thismenuitem thisMenuItem ./. l  � �0�{�z0 =   � �121 l  � �3�y�x3 c   � �454 o   � ��w�w 0 	this_item  5 m   � ��v
�v 
TEXT�y  �x  2 m   � �66 �77  Q u i t�{  �z  / 8�u8 r   � �9:9 l  � �;�t�s; n  � �<=< I   � ��r>�q�r J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_> ?@? o   � ��p�p 0 	this_item  @ ABA m   � �CC �DD  q u i t H a n d l e r :B E�oE m   � �FF �GG  �o  �q  = n  � �HIH I   � ��n�m�l�n 	0 alloc  �m  �l  I n  � �JKJ o   � ��k�k 0 
nsmenuitem 
NSMenuItemK m   � ��j
�j misccura�t  �s  : o      �i�i 0 thismenuitem thisMenuItem�u  ��  t LML l �h�g�f�h  �g  �f  M NON l P�e�dP n QRQ I  �cS�b�c 0 additem_ addItem_S T�aT o  �`�` 0 thismenuitem thisMenuItem�a  �b  R o  �_�_ 0 newmenu newMenu�e  �d  O UVU l �^�]�\�^  �]  �\  V WXW l YZ[Y l \�[�Z\ n ]^] I  �Y_�X�Y 0 
settarget_ 
setTarget__ `�W`  f  �W  �X  ^ o  �V�V 0 thismenuitem thisMenuItem�[  �Z  Z * $ required for enabling the menu item   [ �aa H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e mX bcb l �U�T�S�U  �T  �S  c d�Rd Z  9ef�Q�Pe G  "ghg l i�O�Ni =  jkj o  �M�M 0 i  k m  �L�L �O  �N  h l l�K�Jl =  mnm o  �I�I 0 i  n m  �H�H �K  �J  f l %5opqo l %5r�G�Fr n %5sts I  *5�Eu�D�E 0 additem_ addItem_u v�Cv l *1w�B�Aw n *1xyx o  -1�@�@ 0 separatoritem separatorItemy n *-z{z o  +-�?�? 0 
nsmenuitem 
NSMenuItem{ m  *+�>
�> misccura�B  �A  �C  �D  t o  %*�=�= 0 newmenu newMenu�G  �F  p   add a seperator   q �||     a d d   a   s e p e r a t o r�Q  �P  �R  �� 0 i  g m    �<�< h n    }~} m    �;
�; 
nmbr~ n   � 2   �:
�: 
cobj� o    �9�9 0 thelist theList��  ��  W ��� l     �8�7�6�8  �7  �6  � ��� l     �5�4�3�5  �4  �3  � ��� l     �2�1�0�2  �1  �0  � ��� l     �/�.�-�/  �.  �-  � ��� l     �,�+�*�,  �+  �*  � ��� i  ����� I      �)��(�)  0 backuphandler_ backupHandler_� ��'� o      �&�& 
0 sender  �'  �(  � Z     �����%� l    ��$�#� =     ��� o     �"�" 0 isbackingup isBackingUp� m    �!
�! boovfals�$  �#  � k   
 U�� ��� r   
 ��� m   
 � 
�  boovtrue� o      �� 0 manualbackup manualBackup� ��� Z    U����� l   ���� =    ��� o    �� 0 notifications  � m    �
� boovtrue�  �  � Z    Q����� l   #���� =    #��� o    !�� 0 nsfw  � m   ! "�
� boovtrue�  �  � k   & 3�� ��� I  & -���
� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� ���
� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�  � ��� I  . 3���
� .sysodelanull��� ��� nmbr� m   . /�� �  �  � ��� l  6 =���
� =   6 =��� o   6 ;�	�	 0 nsfw  � m   ; <�
� boovfals�  �
  � ��� k   @ M�� ��� I  @ G���
� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� ���
� 
appr� m   B C�� ��� 
 W B T F U�  � ��� I  H M���
� .sysodelanull��� ��� nmbr� m   H I� �  �  �  �  �  �  �  �  � ��� l  X _������ =   X _��� o   X ]���� 0 isbackingup isBackingUp� m   ] ^��
�� boovtrue��  ��  � ���� k   b ��� ��� r   b k��� n   b i��� 3   g i��
�� 
cobj� o   b g���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   l �������� l  l s������ =   l s��� o   l q���� 0 notifications  � m   q r��
�� boovtrue��  ��  � Z   v ������� l  v }������ =   v }��� o   v {���� 0 nsfw  � m   { |��
�� boovtrue��  ��  � I  � �����
�� .sysonotfnull��� ��� TEXT� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  ��  �%  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� .0 backupandquithandler_ backupAndQuitHandler_�  ��  o      ���� 
0 sender  ��  ��  � Z     ��� l    ���� =      o     ���� 0 isbackingup isBackingUp m    ��
�� boovfals��  ��   k   
 � 	 r   
 

 l  
 ���� n   
  1    ��
�� 
bhit l  
 ���� I  
 ��
�� .sysodlogaskr        TEXT m   
  � T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ? ��
�� 
appr m     � , W o a h ,   B a c k   T h e   F u c k   U p ��
�� 
disp o    ���� 0 appicon appIcon ��
�� 
btns J      m     �    Y e s !��! m    "" �##  N o��   ��$��
�� 
dflt$ m    ���� ��  ��  ��  ��  ��   o      ���� 	0 reply  	 %&% l   ��������  ��  ��  & '��' Z    �()*��( l   "+����+ =    ",-, o     ���� 	0 reply  - m     !.. �//  Y e s��  ��  ) k   % |00 121 r   % ,343 m   % &��
�� boovtrue4 o      ����  0 backupthenquit backupThenQuit2 565 r   - 4787 m   - .��
�� boovtrue8 o      ���� 0 manualbackup manualBackup6 9:9 l  5 5��������  ��  ��  : ;��; Z   5 |<=����< l  5 <>����> =   5 <?@? o   5 :���� 0 notifications  @ m   : ;��
�� boovtrue��  ��  = Z   ? xABC��A l  ? FD����D =   ? FEFE o   ? D���� 0 nsfw  F m   D E��
�� boovtrue��  ��  B k   I VGG HIH I  I P��JK
�� .sysonotfnull��� ��� TEXTJ m   I JLL �MM T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u pK ��N��
�� 
apprN m   K LOO �PP 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  I Q��Q I  Q V��R��
�� .sysodelanull��� ��� nmbrR m   Q R���� ��  ��  C STS l  Y `U����U =   Y `VWV o   Y ^���� 0 nsfw  W m   ^ _��
�� boovfals��  ��  T X��X k   c tYY Z[Z I  c n��\]
�� .sysonotfnull��� ��� TEXT\ m   c f^^ �__   P r e p a r i n g   b a c k u p] ��`�
�� 
appr` m   g jaa �bb 
 W B T F U�  [ c�c I  o t�d�
� .sysodelanull��� ��� nmbrd m   o p�� �  �  ��  ��  ��  ��  ��  * efe l   �g��g =    �hih o    ��� 	0 reply  i m   � �jj �kk  N o�  �  f l�l r   � �mnm m   � ��
� boovfalsn o      ��  0 backupthenquit backupThenQuit�  ��  ��   opo l  � �q��q =   � �rsr o   � ��� 0 isbackingup isBackingUps m   � ��
� boovtrue�  �  p t�t k   � �uu vwv r   � �xyx n   � �z{z 3   � ��
� 
cobj{ o   � ��~�~ 40 messagesalreadybackingup messagesAlreadyBackingUpy o      �}�} 0 msg  w |�|| Z   � �}~�{�z} l  � ��y�x =   � ���� o   � ��w�w 0 notifications  � m   � ��v
�v boovtrue�y  �x  ~ Z   � �����u� l  � ���t�s� =   � ���� o   � ��r�r 0 nsfw  � m   � ��q
�q boovtrue�t  �s  � I  � ��p��
�p .sysonotfnull��� ��� TEXT� o   � ��o�o 0 msg  � �n��m
�n 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�m  � ��� l  � ���l�k� =   � ���� o   � ��j�j 0 nsfw  � m   � ��i
�i boovfals�l  �k  � ��h� I  � ��g��
�g .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �f��e
�f 
appr� m   � ��� ��� 
 W B T F U�e  �h  �u  �{  �z  �|  �  ��  � ��� l     �d�c�b�d  �c  �b  � ��� l     �a�`�_�a  �`  �_  � ��� l     �^�]�\�^  �]  �\  � ��� l     �[�Z�Y�[  �Z  �Y  � ��� l     �X�W�V�X  �W  �V  � ��� i  ����� I      �U��T�U  0 nsfwonhandler_ nsfwOnHandler_� ��S� o      �R�R 
0 sender  �S  �T  � k     =�� ��� r     ��� m     �Q
�Q boovtrue� o      �P�P 0 nsfw  � ��O� Z    =����N� l   ��M�L� =    ��� o    �K�K 0 notifications  � m    �J
�J boovtrue�M  �L  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��I� m    �� ���  Q u i t�I  � o      �H�H 0 thelist theList� ��� l  " )��G�F� =   " )��� o   " '�E�E 0 notifications  � m   ' (�D
�D boovfals�G  �F  � ��C� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��B� m   0 1�� ���  Q u i t�B  � o      �A�A 0 thelist theList�C  �N  �O  � ��� l     �@�?�>�@  �?  �>  � ��� i  ����� I      �=��<�= "0 nsfwoffhandler_ nsfwOffHandler_� ��;� o      �:�: 
0 sender  �;  �<  � k     =�� ��� r     ��� m     �9
�9 boovfals� o      �8�8 0 nsfw  � ��7� Z    =����6� l   ��5�4� =    ��� o    �3�3 0 notifications  � m    �2
�2 boovtrue�5  �4  � r    ��� J    �� � � m     �  B a c k u p   n o w   m     � " B a c k u p   n o w   &   q u i t  m    		 �

 " T u r n   o n   t h e   b a n t s  m     � , T u r n   o f f   n o t i f i c a t i o n s �1 m     �  Q u i t�1  � o      �0�0 0 thelist theList�  l  " )�/�. =   " ) o   " '�-�- 0 notifications   m   ' (�,
�, boovfals�/  �.   �+ r   , 9 J   , 3  m   , - �  B a c k u p   n o w   m   - .!! �"" " B a c k u p   n o w   &   q u i t  #$# m   . /%% �&& " T u r n   o n   t h e   b a n t s$ '(' m   / 0)) �** * T u r n   o n   n o t i f i c a t i o n s( +�*+ m   0 1,, �--  Q u i t�*   o      �)�) 0 thelist theList�+  �6  �7  � ./. l     �(�'�&�(  �'  �&  / 010 l     �%�$�#�%  �$  �#  1 232 l     �"�!� �"  �!  �   3 454 l     ����  �  �  5 676 l     ����  �  �  7 898 i  ��:;: I      �<�� 00 notificationonhandler_ notificationOnHandler_< =�= o      �� 
0 sender  �  �  ; k     =>> ?@? r     ABA m     �
� boovtrueB o      �� 0 notifications  @ C�C Z    =DEF�D l   G��G =    HIH o    �� 0 nsfw  I m    �
� boovtrue�  �  E r    JKJ J    LL MNM m    OO �PP  B a c k u p   n o wN QRQ m    SS �TT " B a c k u p   n o w   &   q u i tR UVU m    WW �XX $ T u r n   o f f   t h e   b a n t sV YZY m    [[ �\\ , T u r n   o f f   n o t i f i c a t i o n sZ ]�] m    ^^ �__  Q u i t�  K o      �� 0 thelist theListF `a` l  " )b��
b =   " )cdc o   " '�	�	 0 nsfw  d m   ' (�
� boovfals�  �
  a e�e r   , 9fgf J   , 3hh iji m   , -kk �ll  B a c k u p   n o wj mnm m   - .oo �pp " B a c k u p   n o w   &   q u i tn qrq m   . /ss �tt " T u r n   o n   t h e   b a n t sr uvu m   / 0ww �xx , T u r n   o f f   n o t i f i c a t i o n sv y�y m   0 1zz �{{  Q u i t�  g o      �� 0 thelist theList�  �  �  9 |}| l     ����  �  �  } ~~ i  ����� I      ��� � 20 notificationoffhandler_ notificationOffHandler_� ���� o      ���� 
0 sender  ��  �   � k     =�� ��� r     ��� m     ��
�� boovfals� o      ���� 0 notifications  � ���� Z    =������ l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m    �� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  " )������ =   " )��� o   " '���� 0 nsfw  � m   ' (��
�� boovfals��  ��  � ���� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   0 1�� ���  Q u i t��  � o      ���� 0 thelist theList��  ��  ��   ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 0 quithandler_ quitHandler_� ���� o      ���� 
0 sender  ��  ��  � Z     ������� l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovtrue��  ��  � k   
 :�� ��� n  
 ��� I    �������� 0 setfocus setFocus��  ��  �  f   
 � ��� r    $��� l   "������ n    "��� 1     "��
�� 
bhit� l    ������ I    ����
�� .sysodlogaskr        TEXT� m    �� ��� � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?� ����
�� 
appr� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� ����
�� 
disp� o    ���� 0 appicon appIcon� ����
�� 
btns� J    �� ��� m    �� ���  C a n c e l   &   Q u i t� ���� m    �� ���  R e s u m e��  � �����
�� 
dflt� m    ���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ���� Z   % :������ l  % (����� =   % (� � o   % &�� 	0 reply    m   & ' �  C a n c e l   &   Q u i t��  �  � I  + 0��
� .aevtquitnull��� ��� null m   + ,                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  ��  � I  3 :��
� .ascrcmnt****      � **** l  3 6�� m   3 6 �  W B T F U   r e s u m e d�  �  �  ��  � 	
	 l  = D�� =   = D o   = B�� 0 isbackingup isBackingUp m   B C�
� boovfals�  �  
 � k   G �  n  G L I   H L���� 0 setfocus setFocus�  �    f   G H  r   M i l  M g�� n   M g 1   e g�
� 
bhit l  M e�� I  M e�
� .sysodlogaskr        TEXT m   M P � < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ? � !
� 
appr  m   Q T"" �## , W o a h ,   B a c k   T h e   F u c k   U p! �$%
� 
disp$ o   U V�� 0 appicon appIcon% �&'
� 
btns& J   W _(( )*) m   W Z++ �,,  Q u i t* -�- m   Z ].. �//  R e s u m e�  ' �0�
� 
dflt0 m   ` a�� �  �  �  �  �   o      �� 	0 reply   1�1 Z   j �23�42 l  j o5��5 =   j o676 o   j k�� 	0 reply  7 m   k n88 �99  Q u i t�  �  3 I  r w�:�
� .aevtquitnull��� ��� null: m   r s;;                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  �  4 I  z ��<�
� .ascrcmnt****      � ****< l  z }=��= m   z }>> �??  W B T F U   r e s u m e d�  �  �  �  �  ��  � @A@ l     ����  �  �  A BCB l     ����  �  �  C DED l     ����  �  �  E FGF l     ����  �  �  G HIH l     ����  �  �  I JKJ i  ��LML I      ���� (0 animatemenubaricon animateMenuBarIcon�  �  M k     .NN OPO r     QRQ 4     �S
� 
alisS l   T��T b    UVU l   	W��W I   	�~XY
�~ .earsffdralis        afdrX  f    Y �}Z�|
�} 
rtypZ m    �{
�{ 
ctxt�|  �  �  V m   	 
[[ �\\ J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g�  �  R o      �z�z 0 	imagepath 	imagePathP ]^] r    _`_ n    aba 1    �y
�y 
psxpb o    �x�x 0 	imagepath 	imagePath` o      �w�w 0 	imagepath 	imagePath^ cdc r    #efe n   !ghg I    !�vi�u�v 20 initwithcontentsoffile_ initWithContentsOfFile_i j�tj o    �s�s 0 	imagepath 	imagePath�t  �u  h n   klk I    �r�q�p�r 	0 alloc  �q  �p  l n   mnm o    �o�o 0 nsimage NSImagen m    �n
�n misccuraf o      �m�m 	0 image  d o�lo n  $ .pqp I   ) .�kr�j�k 0 	setimage_ 	setImage_r s�is o   ) *�h�h 	0 image  �i  �j  q o   $ )�g�g 0 
statusitem 
StatusItem�l  K tut l     �f�e�d�f  �e  �d  u vwv l     �c�b�a�c  �b  �a  w xyx l     �`�_�^�`  �_  �^  y z{z l     �]�\�[�]  �\  �[  { |}| l     �Z�Y�X�Z  �Y  �X  } ~~ i  ����� I      �W�V�U�W $0 resetmenubaricon resetMenuBarIcon�V  �U  � k     .�� ��� r     ��� 4     �T�
�T 
alis� l   ��S�R� b    ��� l   	��Q�P� I   	�O��
�O .earsffdralis        afdr�  f    � �N��M
�N 
rtyp� m    �L
�L 
ctxt�M  �Q  �P  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�S  �R  � o      �K�K 0 	imagepath 	imagePath� ��� r    ��� n    ��� 1    �J
�J 
psxp� o    �I�I 0 	imagepath 	imagePath� o      �H�H 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�G��F�G 20 initwithcontentsoffile_ initWithContentsOfFile_� ��E� o    �D�D 0 	imagepath 	imagePath�E  �F  � n   ��� I    �C�B�A�C 	0 alloc  �B  �A  � n   ��� o    �@�@ 0 nsimage NSImage� m    �?
�? misccura� o      �>�> 	0 image  � ��=� n  $ .��� I   ) .�<��;�< 0 	setimage_ 	setImage_� ��:� o   ) *�9�9 	0 image  �:  �;  � o   $ )�8�8 0 
statusitem 
StatusItem�=   ��� l     �7�6�5�7  �6  �5  � ��� l     �4�3�2�4  �3  �2  � ��� l     �1�0�/�1  �0  �/  � ��� l     �.�-�,�.  �-  �,  � ��� l     �+�*�)�+  �*  �)  � ��� l     �(���(  �   create an NSStatusBar   � ��� ,   c r e a t e   a n   N S S t a t u s B a r� ��� i  ����� I      �'�&�%�' 0 makestatusbar makeStatusBar�&  �%  � k     r�� ��� l     �$���$  �  log ("Make status bar")   � ��� . l o g   ( " M a k e   s t a t u s   b a r " )� ��� r     ��� n    ��� o    �#�# "0 systemstatusbar systemStatusBar� n    ��� o    �"�" 0 nsstatusbar NSStatusBar� m     �!
�! misccura� o      � �  0 bar  � ��� l   ����  �  �  � ��� r    ��� n   ��� I   	 ���� .0 statusitemwithlength_ statusItemWithLength_� ��� m   	 
�� ��      �  �  � o    	�� 0 bar  � o      �� 0 
statusitem 
StatusItem� ��� l   ����  �  �  � ��� r    #��� 4    !��
� 
alis� l    ���� b     ��� l   ���� I   ���
� .earsffdralis        afdr�  f    � ���
� 
rtyp� m    �
� 
ctxt�  �  �  � m    �� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�  �  � o      �� 0 	imagepath 	imagePath� ��� r   $ )��� n   $ '��� 1   % '�

�
 
psxp� o   $ %�	�	 0 	imagepath 	imagePath� o      �� 0 	imagepath 	imagePath� ��� r   * 8��� n  * 6��� I   1 6���� 20 initwithcontentsoffile_ initWithContentsOfFile_� ��� o   1 2�� 0 	imagepath 	imagePath�  �  � n  * 1��� I   - 1���� 	0 alloc  �  �  � n  * -��� o   + -� �  0 nsimage NSImage� m   * +��
�� misccura� o      ���� 	0 image  � ��� l  9 9��������  ��  ��  � ��� l  9 9������  � � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   � ��� s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "� ��� l  9 9��������  ��  ��  � � � n  9 C I   > C������ 0 	setimage_ 	setImage_ �� o   > ?���� 	0 image  ��  ��   o   9 >���� 0 
statusitem 
StatusItem   l  D D��������  ��  ��    l  D D��	
��  	 , & set up the initial NSStatusBars title   
 � L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e  l  D D����   # StatusItem's setTitle:"WBTFU"    � : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "  l  D D��������  ��  ��    l  D D����   1 + set up the initial NSMenu of the statusbar    � V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r  r   D X n  D R I   K R������  0 initwithtitle_ initWithTitle_ �� m   K N   �!!  C u s t o m��  ��   n  D K"#" I   G K�������� 	0 alloc  ��  ��  # n  D G$%$ o   E G���� 0 nsmenu NSMenu% m   D E��
�� misccura o      ���� 0 newmenu newMenu &'& l  Y Y��������  ��  ��  ' ()( l  Y Y��*+��  * � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   + �,,0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .) -.- n  Y c/0/ I   ^ c��1���� 0 setdelegate_ setDelegate_1 2��2  f   ^ _��  ��  0 o   Y ^���� 0 newmenu newMenu. 343 l  d d��������  ��  ��  4 5��5 n  d r676 I   i r��8���� 0 setmenu_ setMenu_8 9��9 o   i n���� 0 newmenu newMenu��  ��  7 o   d i���� 0 
statusitem 
StatusItem��  � :;: l     ��������  ��  ��  ; <=< l     ��>?��  > � �-----------------------------------------------------------------------------------------------------------------------------------------------   ? �@@ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -= ABA l     ��CD��  C � �-----------------------------------------------------------------------------------------------------------------------------------------------   D �EE - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -B FGF l     ��������  ��  ��  G HIH l     ��������  ��  ��  I JKJ l     ��������  ��  ��  K LML l     ������  �  �  M NON l     ����  �  �  O PQP l     ����  �  �  Q RSR i  ��TUT I      ���� 0 runonce runOnce�  �  U k     VV WXW I     ���� (0 animatemenubaricon animateMenuBarIcon�  �  X Y�Y I    ���� 0 	plistinit 	plistInit�  �  �  S Z[Z l     ����  �  �  [ \]\ l   ^��^ n   _`_ I    ���� 0 makestatusbar makeStatusBar�  �  `  f    �  �  ] aba l   c��c I    ���� 0 runonce runOnce�  �  �  �  b ded l     ����  �  �  e fgf l     ����  �  �  g hih l     ����  �  �  i jkj l     ����  �  �  k lml l     ����  �  �  m non l     �pq�  p w q Function that is always running in the background. This doesn't need to get called as it is running from the off   q �rr �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f fo sts l     �uv�  u � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   v �ww�   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .t x�x i  ��yzy I     ���
� .miscidlenmbr    ��� null�  �  z k     �{{ |}| Z     �~��~ l    ���� E     ��� o     �� &0 displaysleepstate displaySleepState� o    
�� 0 strawake strAwake�  �   Z    ������ l   ���� =    ��� o    �� 0 isbackingup isBackingUp� m    �
� boovfals�  �  � Z    ������ l   ��~�}� =    ��� o    �|�| 0 manualbackup manualBackup� m    �{
�{ boovfals�~  �}  � Z   " s���z�y� l  " +��x�w� A   " +��� l  " )��v�u� n   " )��� 1   ' )�t
�t 
hour� l  " '��s�r� I  " '�q�p�o
�q .misccurdldt    ��� null�p  �o  �s  �r  �v  �u  � m   ) *�n�n �x  �w  � k   . o�� ��� r   . 7��� l  . 5��m�l� n   . 5��� 1   3 5�k
�k 
min � l  . 3��j�i� l  . 3��h�g� I  . 3�f�e�d
�f .misccurdldt    ��� null�e  �d  �h  �g  �j  �i  �m  �l  � o      �c�c 0 m  � ��� l  8 8�b�a�`�b  �a  �`  � ��_� Z   8 o����^� G   8 C��� l  8 ;��]�\� =   8 ;��� o   8 9�[�[ 0 m  � m   9 :�Z�Z �]  �\  � l  > A��Y�X� =   > A��� o   > ?�W�W 0 m  � m   ? @�V�V -�Y  �X  � Z   F U���U�T� l  F I��S�R� =   F I��� o   F G�Q�Q 0 scheduledtime scheduledTime� m   G H�P�P �S  �R  � I   L Q�O�N�M�O 0 	plistinit 	plistInit�N  �M  �U  �T  � ��� G   X c��� l  X [��L�K� =   X [��� o   X Y�J�J 0 m  � m   Y Z�I�I  �L  �K  � l  ^ a��H�G� =   ^ a��� o   ^ _�F�F 0 m  � m   _ `�E�E �H  �G  � ��D� I   f k�C�B�A�C 0 	plistinit 	plistInit�B  �A  �D  �^  �_  �z  �y  � ��� l  v }��@�?� =   v }��� o   v {�>�> 0 manualbackup manualBackup� m   { |�=
�= boovtrue�@  �?  � ��<� I   � ��;�:�9�; 0 	plistinit 	plistInit�:  �9  �<  �  �  �  �  �  } ��� l  � ��8�7�6�8  �7  �6  � ��5� L   � ��� m   � ��4�4 �5  �       5�3���2 g������1�0�/��.�-�,�������������������������������������3  � 3�+�*�)�(�'�&�%�$�#�"�!� ����������������������
�	��������� ��������������
�+ 
pimr�* 0 
statusitem 
StatusItem�) 0 
thedisplay 
theDisplay�( 0 defaults  �' $0 internalmenuitem internalMenuItem�& $0 externalmenuitem externalMenuItem�% 0 newmenu newMenu�$ 0 thelist theList�# 0 nsfw  �" 0 notifications  �!  0 backupthenquit backupThenQuit�  	0 plist  � 0 initialbackup initialBackup� 0 manualbackup manualBackup� 0 isbackingup isBackingUp� "0 messagesmissing messagesMissing� *0 messagesencouraging messagesEncouraging� $0 messagescomplete messagesComplete� &0 messagescancelled messagesCancelled� 40 messagesalreadybackingup messagesAlreadyBackingUp� 0 strawake strAwake� 0 strsleep strSleep� &0 displaysleepstate displaySleepState� 0 	plistinit 	plistInit� 0 diskinit diskInit� 0 freespaceinit freeSpaceInit� 0 
backupinit 
backupInit� 0 tidyup tidyUp� 
0 backup  � 0 itexists itExists� .0 getcomputeridentifier getComputerIdentifier� 0 gettimestamp getTimestamp� 0 comparesizes compareSizes�
 0 	stopwatch  �	 0 getduration getDuration� 0 setfocus setFocus� $0 menuneedsupdate_ menuNeedsUpdate_� 0 	makemenus 	makeMenus�  0 backuphandler_ backupHandler_� .0 backupandquithandler_ backupAndQuitHandler_�  0 nsfwonhandler_ nsfwOnHandler_� "0 nsfwoffhandler_ nsfwOffHandler_� 00 notificationonhandler_ notificationOnHandler_�  20 notificationoffhandler_ notificationOffHandler_�� 0 quithandler_ quitHandler_�� (0 animatemenubaricon animateMenuBarIcon�� $0 resetmenubaricon resetMenuBarIcon�� 0 makestatusbar makeStatusBar�� 0 runonce runOnce
�� .miscidlenmbr    ��� null
�� .aevtoappnull  �   � ****� ����� �  ����� �� L��
�� 
vers��  � �����
�� 
cobj� ��   ��
�� 
osax��  � �����
�� 
cobj� ��   �� U
�� 
frmk��  � �����
�� 
cobj� ��   �� [
�� 
frmk��  
�2 
msng� �� �����
�� misccura
�� 
pcls� �    N S U s e r D e f a u l t s�  ����
�� misccura
�� 
pcls �  N S M e n u I t e m�  ����
�� misccura
�� 
pcls �  N S M e n u I t e m�  ����
�� misccura
�� 
pcls �		  N S M e n u� ��
�� 
   � � � � �
�1 boovfals
�0 boovtrue
�/ boovfals� � � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�. boovtrue
�- boovfals
�, boovfals� ����    #� ���� 
 
 -159=AEIMP� ���� 
 
 Z^bfjnrvz}� ����   ��������� ���� 	 	 ���������� ��             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 3 0 1 9 5 4 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 0 3 , " I d l e T i m e r E l a p s e d T i m e " = 3 2 3 7 8 5 5 , " C u r r e n t P o w e r S t a t e " = 4 }� ���������� 0 	plistinit 	plistInit��  ��   
���������������������� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk��  0 computerfolder computerFolder�� 0 
backuptime 
backupTime�� 0 thecontents theContents�� 0 thevalue theValue�� 0 welcome  �� 	0 reply  �� 0 backuptimes backupTimes�� 0 selectedtime selectedTime =%��X���������������������q�����������������������������������������	�)8������� 0 itexists itExists
�� 
plif
�� 
pcnt
�� 
valL��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive��  0 computerfolder computerFolder�� 0 scheduledtime scheduledTime�� .0 getcomputeridentifier getComputerIdentifier��  ���� 0 setfocus setFocus
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
� .sysodlogaskr        TEXT
� 
bhit
� afdrcusr
� .earsffdralis        afdr
� 
prmp
� .sysostflalis    ��� null
� 
TEXT
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
� .corecrel****      � null
� 
plii
� 
insh
� 
kind� � 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .ascrcmnt****      � ****� 0 diskinit diskInit���jE�O*�b  l+ e  6� .*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUY�*j+ 
E�O� ��n)j+ O�E�O��a a _ a a kva ka  a ,E�Oa j E�O*a a l a &E�O�a ,E�O�a ,E�Oa  a !a "mvE�O�a a #l $kva &E�O�a %  
a &E�Y #�a '  
a (E�Y �a )  
a *E�Y hOPoUO� �*a +�a ,a -b  la . / �*a +a 0a 1*6a ,a 2a a -a 3�a 4a 4 /O*a +a 0a 1*6a ,a 2a a -a 5�a 4a 4 /O*a +a 0a 1*6a ,a 2a a -a 6�a 4a 4 /O*a +a 0a 1*6a ,a 2a a -a 7�a 4a 4 /UUO�E` 8O�E` 9O�E` :O�E�O_ 8_ 9_ :�a .vj ;O*j+ <� �z���� 0 diskinit diskInit�  �   ��� 0 msg  � 	0 reply   ������������������������ "0 destinationdisk destinationDisk� 0 itexists itExists� 0 freespaceinit freeSpaceInit� 0 setfocus setFocus
� 
cobj
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit� 0 diskinit diskInit
� .sysonotfnull��� ��� TEXT� �*��l+ e  *fk+ Y �)j+ Ob  �.E�O��������lv�l� a ,E�O�a   
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� ������ 0 freespaceinit freeSpaceInit� ��   �� 	0 again  �   ���� 	0 again  � 	0 reply  � 0 msg   �����"�&���/2�~�}�|�{CGPS_�z�y~�x��� 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 comparesizes compareSizes� 0 
backupinit 
backupInit� 0 setfocus setFocus
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
�~ 
dflt�} 
�| .sysodlogaskr        TEXT
�{ 
bhit�z 0 tidyup tidyUp
�y 
cobj
�x .sysonotfnull��� ��� TEXT� �*��l+ e  
*j+ Y �)j+ O�f  ��������lv�l� a ,E�Y *�e  #a �a ���a a lv�l� a ,E�Y hO�a   
*j+ Y ]b  b   b  a .E�Y hOb  	e  4b  e  ��a l Y b  f  a �a l Y hY h� �w��v�u�t�w 0 
backupinit 
backupInit�v  �u     �s�r�q���p��o�n�m�l�k��j�i���h�g��f ,D<�e�d
�s 
psxf�r "0 destinationdisk destinationDisk�q 	0 drive  �p 0 itexists itExists
�o 
kocl
�n 
cfol
�m 
insh
�l 
prdt
�k 
pnam�j 
�i .corecrel****      � null�h 0 machinefolder machineFolder
�g 
cdis�f 0 backupfolder backupFolder�e 0 latestfolder latestFolder�d 
0 backup  �t �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` UO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` UO*j+ � �cl�b�a�`�c 0 tidyup tidyUp�b  �a   
�_�^�]�\�[�Z�Y�X�W�V�_ 0 bf bF�^ 0 creationdates creationDates�] 0 theoldestdate theOldestDate�\ 0 j  �[ 0 i  �Z 0 thisdate thisDate�Y  0 foldertodelete folderToDelete�X 0 todelete toDelete�W 
0 reply2  �V 0 msg   .�U�T�S��R�Q��P�O�N�M�L��K�J�I�	�H�G"�F&�E�D�C/2�B�A�@�?=�>�=\�<fi�;���:��
�U 
psxf�T "0 destinationdisk destinationDisk�S 	0 drive  
�R 
cdis
�Q 
cfol�P 0 machinefolder machineFolder
�O 
cobj
�N 
ascd
�M .corecnte****       ****
�L 
pnam
�K .ascrcmnt****      � ****
�J 
TEXT
�I 
psxp
�H .sysoexecTEXT���     TEXT�G 0 setfocus setFocus
�F 
appr
�E 
disp�D 0 appicon appIcon
�C 
btns
�B 
dflt�A 
�@ .sysodlogaskr        TEXT
�? 
bhit
�> 
trsh
�= .fndremptnull��� ��� obj 
�< .sysonotfnull��� ��� TEXT�; 0 freespaceinit freeSpaceInit
�: .sysodelanull��� ��� nmbr�`�*��/E�O��*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/�&E�O��,�&E�Oa �%j Oa �%a %E�O�j O)j+ Oa a a a _ a a a lva la  a  ,E�O�a !  *a ",j #Y _b  b   Pb  �.E�Ob  	e  8b  e  �a a $l %Y b  f  a &a a 'l %Y hY hY hO*ek+ (Ob  b   Tb  	e  Fb  e  a )a a *l %Okj +Y #b  f  a ,a a -l %Okj +Y hY hY hU� �9��8�7�6�9 
0 backup  �8  �7   
�5�4�3�2�1�0�/�.�-�,�5 0 t  �4 0 x  �3 "0 containerfolder containerFolder�2 0 
foldername 
folderName�1 0 d  �0 0 duration  �/ 0 msg  �. 0 c  �- 0 oldestfolder oldestFolder�, 0 todelete toDelete i�+�*�)��(
��'�&�%�$�#�"�!� ������2���������������������TVYlnq���
���������	��		1	3	5	7��	w	�	�	�	�	�	�	�	�	�	�	�




(
+
5
8
y
{
~
�
�
�
�
�
�
�
�
�
��+ 0 gettimestamp getTimestamp�* (0 animatemenubaricon animateMenuBarIcon
�) .sysodelanull��� ��� nmbr�( 0 	stopwatch  
�' 
psxf�& 0 sourcefolder sourceFolder
�% 
TEXT
�$ 
cfol
�# 
pnam
�" 
kocl
�! 
insh�  0 backupfolder backupFolder
� 
prdt� 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � 0 machinefolder machineFolder� (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
� 
appr
� .sysonotfnull��� ��� TEXT� "0 destinationdisk destinationDisk
� .sysoexecTEXT���     TEXT�  �  � 0 endtime endTime� 0 getduration getDuration
� 
cobj� $0 resetmenubaricon resetMenuBarIcon
�
 .aevtquitnull��� ��� null
�	 .ascrcmnt****      � ****
� 
alis
� 
psxp�6�*ek+  E�OeEc  O*j+ Okj O*�k+ O�R*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e b*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_  a !%_ %a "%�%a #%�&E�O a $�%a %%�%a &%j 'OPW X ( )hOfEc  OfEc  Ob  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  Tb  e   a -�%a .%�%a a /l Okj Y )b  f  a 0�%a 1%a a 2l Okj Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYxb  f m_  a 6%_ %a 7%�%a 8%�%a 9%�&E�O_  a :%_ %a ;%�%a <%�&E�Oa =�%j >Ob  b   l*j a ,E` Ob  a ,.E�Ob  	e  Db  e  �a a ?l Okj Y #b  f  a @a a Al Okj Y hY hY hO  a B�%a C%�%a D%�%a E%j 'OPW X ( )hOfEc  OfEc  O*�/a F&a ,-jv [��/�&E�O�a G,�&E�Oa H�%j >Oa I�%a J%E�O�j 'Ob  b  *j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  �a K  Tb  e   a L�%a M%�%a a Nl Okj Y )b  f  a O�%a P%a a Ql Okj Y hY Qb  e   a R�%a S%�%a a Tl Okj Y )b  f  a U�%a V%a a Wl Okj Y hOb  e  a Xa a Yl Y b  f  a Za a [l Y hY hO)j+ 3Y hY	b  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  ��a K  Tb  e   a \�%a ]%�%a a ^l Okj Y )b  f  a _�%a `%a a al Okj Y hY Qb  e   a b�%a c%�%a a dl Okj Y )b  f  a e�%a f%a a gl Okj Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a hk+ � ��� �� 0 itexists itExists� �!� !  �� � 0 
objecttype 
objectType�  
0 object  �   ������ 0 
objecttype 
objectType�� 
0 object    P(����6��F��
�� 
cdis
�� .coredoexnull���     ****
�� 
file
�� 
cfol� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU� ��i����"#���� .0 getcomputeridentifier getComputerIdentifier��  ��  " �������� 0 computername computerName�� "0 strserialnumber strSerialNumber��  0 identifiername identifierName# ����w���
�� .sysosigtsirr   ��� null
�� 
sicn
�� .sysoexecTEXT���     TEXT�� *j  �,E�O�j E�O��%�%E�O�� �������$%���� 0 gettimestamp getTimestamp�� ��&�� &  ���� 0 isfolder isFolder��  $ ���������������������������������� 0 isfolder isFolder�� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  % ������������������������������������5��������a����
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� misccura
�� 
psof
�� 
psin�� 
�� .sysooffslong    ��� null
�� 
cha ���*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�� �������'(���� 0 comparesizes compareSizes�� �)� )  ��� 
0 source  � 0 destination  ��  ' ����������������� 
0 source  � 0 destination  � 0 fit  � 0 
templatest 
tempLatest� $0 latestfoldersize latestFolderSize� 0 c  � 0 l  � 0 md  � 0 	cachesize 	cacheSize� 0 logssize logsSize� 0 mdsize mdSize� 
0 buffer  � (0 adjustedfoldersize adjustedFolderSize� 0 
foldersize 
folderSize� 0 	freespace 	freeSpace� 0 temp  ( !��������1?Ma:xz��������������')
� 
psxf� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� afdrdlib
� 
from
� fldmfldu
� .earsffdralis        afdr
� 
psxp
� .sysoexecTEXT���     TEXT
� misccura� � d
� .sysorondlong        doub
� 
cdis
� 
frsp
� .ascrcmnt****      � ****���eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O�*�%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�O��%�%j Ob  f  1a �%a  %j E�Oa  �a !a  j Ua !E�OPY hUOb  f  ,��E�O�j ��l E�Y hO��� fE�Y hY b  e  ��� fE�Y hY hO�� ����*+�� 0 	stopwatch  � �,� ,  �� 0 mode  �  * ��� 0 mode  � 0 x  + ������� 0 gettimestamp getTimestamp
� .ascrcmnt****      � ****� 4��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y h� ����-.��� 0 getduration getDuration�  �  - ���� 0 duration  . �������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
�� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�� ������/0���� 0 setfocus setFocus��  ��  /  0 
��
�� .miscactvnull��� ��� null�� � *j U� ��5����12��� $0 menuneedsupdate_ menuNeedsUpdate_�� �~3�~ 3  �}
�} 
cmnu��  1  2 �|�| 0 	makemenus 	makeMenus� )j+  � �{Y�z�y45�x�{ 0 	makemenus 	makeMenus�z  �y  4 �w�v�u�w 0 i  �v 0 	this_item  �u 0 thismenuitem thisMenuItem5 "�t�s�r�q~�p�o�n���m����������	%(6CF�l�k�j�i�h�t  0 removeallitems removeAllItems
�s 
cobj
�r 
nmbr
�q 
TEXT
�p misccura�o 0 
nsmenuitem 
NSMenuItem�n 	0 alloc  �m J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�l 0 additem_ addItem_�k 0 
settarget_ 
setTarget_�j 
�i 
bool�h 0 separatoritem separatorItem�x?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY��� �g��f�e67�d�g  0 backuphandler_ backupHandler_�f �c8�c 8  �b�b 
0 sender  �e  6 �a�`�a 
0 sender  �` 0 msg  7 ��_��^�]���\���
�_ 
appr
�^ .sysonotfnull��� ��� TEXT
�] .sysodelanull��� ��� nmbr
�\ 
cobj�d �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h� �[��Z�Y9:�X�[ .0 backupandquithandler_ backupAndQuitHandler_�Z �W;�W ;  �V�V 
0 sender  �Y  9 �U�T�S�U 
0 sender  �T 	0 reply  �S 0 msg  : �R�Q�P�O"�N�M�L�K.LO�J�I^aj�H���
�R 
appr
�Q 
disp�P 0 appicon appIcon
�O 
btns
�N 
dflt�M 
�L .sysodlogaskr        TEXT
�K 
bhit
�J .sysonotfnull��� ��� TEXT
�I .sysodelanull��� ��� nmbr
�H 
cobj�X �b  f  ���������lv�l� 
�,E�O��  \eEc  
OeEc  Ob  	e  >b  e  ���l Okj Y !b  f  a �a l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� �G��F�E<=�D�G  0 nsfwonhandler_ nsfwOnHandler_�F �C>�C >  �B�B 
0 sender  �E  < �A�A 
0 sender  = ������@������@ �D >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �?��>�=?@�<�? "0 nsfwoffhandler_ nsfwOffHandler_�> �;A�; A  �:�: 
0 sender  �=  ? �9�9 
0 sender  @ 	�8!%),�8 �< >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �7;�6�5BC�4�7 00 notificationonhandler_ notificationOnHandler_�6 �3D�3 D  �2�2 
0 sender  �5  B �1�1 
0 sender  C OSW[^�0koswz�0 �4 >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� �/��.�-EF�,�/ 20 notificationoffhandler_ notificationOffHandler_�. �+G�+ G  �*�* 
0 sender  �-  E �)�) 
0 sender  F ������(������( �, >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� �'��&�%HI�$�' 0 quithandler_ quitHandler_�& �#J�# J  �"�" 
0 sender  �%  H �!� �! 
0 sender  �  	0 reply  I ���������������"+.8>� 0 setfocus setFocus
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� .aevtquitnull��� ��� null
� .ascrcmnt****      � ****�$ �b  e  5)j+  O��������lv�l� �,E�O��  
�j Y 	a j Y Jb  f  ?)j+  Oa �a ���a a lv�l� �,E�O�a   
�j Y 	a j Y h� �M��KL�� (0 animatemenubaricon animateMenuBarIcon�  �  K ��� 0 	imagepath 	imagePath� 	0 image  L ����[�
�	����
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr
�
 
psxp
�	 misccura� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
� ����MN�� $0 resetmenubaricon resetMenuBarIcon�  �  M � ���  0 	imagepath 	imagePath�� 	0 image  N ���������������������
�� 
alis
�� 
rtyp
�� 
ctxt
�� .earsffdralis        afdr
�� 
psxp
�� misccura�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
� �������OP���� 0 makestatusbar makeStatusBar��  ��  O �������� 0 bar  �� 0 	imagepath 	imagePath�� 	0 image  P ������������������������������ ������
�� misccura�� 0 nsstatusbar NSStatusBar�� "0 systemstatusbar systemStatusBar�� .0 statusitemwithlength_ statusItemWithLength_
�� 
alis
�� 
rtyp
�� 
ctxt
�� .earsffdralis        afdr
�� 
psxp�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_�� 0 nsmenu NSMenu��  0 initwithtitle_ initWithTitle_�� 0 setdelegate_ setDelegate_�� 0 setmenu_ setMenu_�� s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ � ��U����QR���� 0 runonce runOnce��  ��  Q  R ������ (0 animatemenubaricon animateMenuBarIcon�� 0 	plistinit 	plistInit�� *j+  O*j+ � ��z����ST��
�� .miscidlenmbr    ��� null��  ��  S ���� 0 m  T 
��������������������
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit�� �� �b  b   �b  f  vb  f  V*j  �,� F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY b  e  
*j+ Y hY hY hO�� ��U����VW��
�� .aevtoappnull  �   � ****U k     XX  �YY \ZZ a����  ��  ��  V  W ������� ����
�� 
alis
�� 
rtyp
�� 
ctxt
� .earsffdralis        afdr� 0 appicon appIcon� 0 makestatusbar makeStatusBar� 0 runonce runOnce�� *�)��l �%/E�O)j+ O*j+ ascr  ��ޭ