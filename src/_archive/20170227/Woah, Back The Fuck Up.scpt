FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S g�� ��� 0 thelist theList � J   S f � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   Y \ � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   \ _ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   _ b � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   q q � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   q q � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ����� 0 endtime endTime�   �  � � � p   q q � � �~�}�~ 0 appicon appIcon�}   �  � � � l     �|�{�z�|  �{  �z   �  � � � l     ��y�x � r      � � � 4     �w �
�w 
alis � l    ��v�u � b     � � � l   	 ��t�s � I   	�r � �
�r .earsffdralis        afdr �  f     � �q ��p
�q 
rtyp � m    �o
�o 
ctxt�p  �t  �s   � m   	 
 � � � � � < C o n t e n t s : R e s o u r c e s : a p p l e t . i c n s�v  �u   � o      �n�n 0 appicon appIcon�y  �x   �  � � � l     �m�l�k�m  �l  �k   �  � � � l     �j�i�h�j  �i  �h   �  � � � l     �g � ��g   �   Property assignment    � � � � (   P r o p e r t y   a s s i g n m e n t �  � � � j   q ��f ��f 	0 plist   � b   q � � � � n  q � � � � 1   ~ ��e
�e 
psxp � l  q ~ ��d�c � I  q ~�b � �
�b .earsffdralis        afdr � m   q t�a
�a afdrdlib � �` ��_
�` 
from � m   w z�^
�^ fldmfldu�_  �d  �c   � m   � � � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � l     �]�\�[�]  �\  �[   �  � � � j   � ��Z ��Z 0 initialbackup initialBackup � m   � ��Y
�Y boovtrue �    j   � ��X�X 0 manualbackup manualBackup m   � ��W
�W boovfals  j   � ��V�V 0 isbackingup isBackingUp m   � ��U
�U boovfals  l     �T�S�R�T  �S  �R   	 j   � ��Q
�Q "0 messagesmissing messagesMissing
 J   � �  m   � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . .  m   � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . .  m   � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !  m   � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �P m   � � � � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�P  	   l     �O�N�M�O  �N  �M    !"! j   � ��L#�L *0 messagesencouraging messagesEncouraging# J   � �$$ %&% m   � �'' �((  C o m e   o n !& )*) m   � �++ �,, 4 C o m e   o n !   E y e   o f   t h e   t i g e r !* -.- m   � �// �00 " N o   p a i n !   N o   p a i n !. 121 m   � �33 �44 0 L e t ' s   d o   t h i s   a s   a   t e a m !2 565 m   � �77 �88  W e   c a n   d o   i t !6 9:9 m   � �;; �<<  F r e e e e e e e e e d o m !: =>= m   � �?? �@@ 2 A l t o g e t h e r   o r   n o t   a t   a l l !> ABA m   � �CC �DD H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !B E�KE m   � �FF �GG H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�K  " HIH l     �J�I�H�J  �I  �H  I JKJ j   � ��GL�G $0 messagescomplete messagesCompleteL J   � �MM NON m   � �PP �QQ  N i c e   o n e !O RSR m   � �TT �UU * Y o u   a b s o l u t e   l e g   e n d !S VWV m   � �XX �YY  G o o d   l a d !W Z[Z m   � �\\ �]]  P e r f i c k ![ ^_^ m   � �`` �aa  H a p p y   d a y s !_ bcb m   � �dd �ee  F r e e e e e e e e e d o m !c fgf m   � �hh �ii H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !g jkj m   � �ll �mm B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !k n�Fn m   � �oo �pp d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�F  K qrq l     �E�D�C�E  �D  �C  r sts j   ��Bu�B &0 messagescancelled messagesCancelledu J   �vv wxw m   � �yy �zz  T h a t ' s   a   s h a m ex {|{ m   � �}} �~~  A h   m a n ,   u n l u c k y| � m   � ��� ���  O h   w e l l� ��� m   � ��� ��� @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e n� ��� m   � ��� ��� , W e l l   t h a t ' s   a   l e t   d o w n� ��� m   � ��� ��� P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?� ��� m   ��� ���  A r r o g a n t� ��A� m  �� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�A  t ��� l     �@�?�>�@  �?  �>  � ��� j  .�=��= 40 messagesalreadybackingup messagesAlreadyBackingUp� J  +�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  �� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  �� ���  D i c k� ��� m  �� ���  F u c k t a r d� ��� m  �� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  !�� ���  A r r o g a n t� ��� m  !$�� ��� $ C h i l l   t h e   f u c k   o u t� ��<� m  $'�� ���  H o l d   f i r e�<  � ��� l     �;�:�9�;  �:  �9  � ��� j  /5�8��8 0 strawake strAwake� m  /2�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  6<�7��7 0 strsleep strSleep� m  69�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  =G�6��6 &0 displaysleepstate displaySleepState� I =D�5��4
�5 .sysoexecTEXT���     TEXT� m  =@�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�4  � ��� l     �3�2�1�3  �2  �1  � ��� l     �0�/�.�0  �/  �.  � ��� l     �-�,�+�-  �,  �+  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� l     �$���$  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �#���#  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �"���"  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �!� ��!  �   �  � ��� l     ����  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     ����  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     ����  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i  HK��� I      ���� 0 	plistinit 	plistInit�  �  � k    ���    q       �� 0 
foldername 
folderName �� 0 
backupdisk 
backupDisk ��  0 computerfolder computerFolder ��� 0 
backuptime 
backupTime�    r     	 m     ��  	 o      �� 0 
backuptime 
backupTime 

 l   ����  �  �    Z   �� l   �� =     I    ��
� 0 itexists itExists  m     �  f i l e �	 o    �� 	0 plist  �	  �
   m    �
� boovtrue�  �   O    E k    D  r    $  n    "!"! 1     "�
� 
pcnt" 4     �#
� 
plif# o    �� 	0 plist    o      �� 0 thecontents theContents $%$ r   % *&'& n   % (()( 1   & (�
� 
valL) o   % &�� 0 thecontents theContents' o      � �  0 thevalue theValue% *+* l  + +��������  ��  ��  + ,-, r   + 0./. n   + .010 o   , .����  0 foldertobackup FolderToBackup1 o   + ,���� 0 thevalue theValue/ o      ���� 0 
foldername 
folderName- 232 r   1 6454 n   1 4676 o   2 4���� 0 backupdrive BackupDrive7 o   1 2���� 0 thevalue theValue5 o      ���� 0 
backupdisk 
backupDisk3 898 r   7 <:;: n   7 :<=< o   8 :����  0 computerfolder computerFolder= o   7 8���� 0 thevalue theValue; o      ����  0 computerfolder computerFolder9 >?> r   = B@A@ n   = @BCB o   > @���� 0 scheduledtime scheduledTimeC o   = >���� 0 thevalue theValueA o      ���� 0 
backuptime 
backupTime? DED l  C C��������  ��  ��  E F��F l  C C��GH��  G . (log {folderName, backupDisk, backupTime}   H �II P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��   m    JJ�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �   k   H�KK LML r   H ONON I   H M�������� .0 getcomputeridentifier getComputerIdentifier��  ��  O o      ����  0 computerfolder computerFolderM PQP l  P P��������  ��  ��  Q RSR O   PTUT t   TVWV k   VXX YZY n  V [[\[ I   W [�������� 0 setfocus setFocus��  ��  \  f   V WZ ]^] l  \ \��������  ��  ��  ^ _`_ r   \ _aba m   \ ]cc �dd� T h i s   p r o g r a m   a u t o m a t i c a l l y   b a c k s   u p   y o u r   H o m e   f o l d e r   b u t   e x c l u d e s   t h e   C a c h e s ,   L o g s   a n d   M o b i l e   D o c u m e n t s   f o l d e r s   f r o m   y o u r   L i b r a r y . 
 
 A l l   y o u   n e e d   t o   d o   i s   s e l e c t   t h e   d r i v e   t o   b a c k u p   t o   a t   t h e   n e x t   p r o m p t .b o      ���� 0 welcome  ` efe r   ` �ghg l  ` �i����i n   ` �jkj 1   } ���
�� 
bhitk l  ` }l����l I  ` }��mn
�� .sysodlogaskr        TEXTm o   ` a���� 0 welcome  n ��op
�� 
appro m   b eqq �rr , W o a h ,   B a c k   T h e   F u c k   U pp ��st
�� 
disps o   h k���� 0 appicon appIcont ��uv
�� 
btnsu J   n sww x��x m   n qyy �zz  O K ,   g o t   i t��  v ��{��
�� 
dflt{ m   v w���� ��  ��  ��  ��  ��  h o      ���� 	0 reply  f |}| l  � ���������  ��  ��  } ~~ l  � �������  � V Pset folderName to (choose folder with prompt "Please choose a folder to backup")   � ��� � s e t   f o l d e r N a m e   t o   ( c h o o s e   f o l d e r   w i t h   p r o m p t   " P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p " ) ��� r   � ���� I  � ������
�� .earsffdralis        afdr� l  � ������� m   � ���
�� afdrcusr��  ��  ��  � o      ���� 0 
foldername 
folderName� ��� r   � ���� c   � ���� l  � ������� I  � ������
�� .sysostflalis    ��� null��  � �����
�� 
prmp� m   � ��� ��� R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  � m   � ���
�� 
TEXT� o      ���� 0 
backupdisk 
backupDisk� ��� l  � ���������  ��  ��  � ��� r   � ���� n   � ���� 1   � ���
�� 
psxp� o   � ����� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� r   � ���� n   � ���� 1   � ���
�� 
psxp� o   � ����� 0 
backupdisk 
backupDisk� o      ���� 0 
backupdisk 
backupDisk� ��� l  � ���������  ��  ��  � ��� r   � ���� J   � ��� ��� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r� ��� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r� ���� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  � o      ���� 0 backuptimes backupTimes� ��� r   � ���� c   � ���� J   � ��� ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  � m   � ���
�� 
TEXT� o      ���� 0 selectedtime selectedTime� ��� l  � �������  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ���������  ��  ��  � ��� Z   ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l ��������  ��  ��  � ���� l ������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  W m   T U����  ��U m   P Q���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  S ��� l ��������  ��  ��  � ���� O  ���� O  ���� k  '��� ��� I 'N�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  +.��
�� 
plii� ����
�� 
insh�  ;  13� �����
�� 
prdt� K  6H   ��
�� 
kind m  9<�
� 
TEXT �~
�~ 
pnam m  ?B �  F o l d e r T o B a c k u p �}�|
�} 
valL o  CD�{�{ 0 
foldername 
folderName�|  ��  � 	 I Ov�z�y

�z .corecrel****      � null�y  
 �x
�x 
kocl m  SV�w
�w 
plii �v
�v 
insh  ;  Y[ �u�t
�u 
prdt K  ^p �s
�s 
kind m  ad�r
�r 
TEXT �q
�q 
pnam m  gj �  B a c k u p D r i v e �p�o
�p 
valL o  kl�n�n 0 
backupdisk 
backupDisk�o  �t  	  I w��m�l
�m .corecrel****      � null�l   �k
�k 
kocl m  {~�j
�j 
plii �i
�i 
insh  ;  �� �h�g
�h 
prdt K  ��   �f!"
�f 
kind! m  ���e
�e 
TEXT" �d#$
�d 
pnam# m  ��%% �&&  C o m p u t e r F o l d e r$ �c'�b
�c 
valL' o  ���a�a  0 computerfolder computerFolder�b  �g   (�`( I ���_�^)
�_ .corecrel****      � null�^  ) �]*+
�] 
kocl* m  ���\
�\ 
plii+ �[,-
�[ 
insh,  ;  ��- �Z.�Y
�Z 
prdt. K  ��// �X01
�X 
kind0 m  ���W
�W 
TEXT1 �V23
�V 
pnam2 m  ��44 �55  S c h e d u l e d T i m e3 �U6�T
�U 
valL6 o  ���S�S 0 
backuptime 
backupTime�T  �Y  �`  � l $7�R�Q7 I $�P�O8
�P .corecrel****      � null�O  8 �N9:
�N 
kocl9 m  �M
�M 
plif: �L;�K
�L 
prdt; K  << �J=�I
�J 
pnam= o  �H�H 	0 plist  �I  �K  �R  �Q  � m  	>>�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��   ?@? l ���G�F�E�G  �F  �E  @ ABA r  ��CDC o  ���D�D 0 
foldername 
folderNameD o      �C�C 0 sourcefolder sourceFolderB EFE r  ��GHG o  ���B�B 0 
backupdisk 
backupDiskH o      �A�A "0 destinationdisk destinationDiskF IJI r  ��KLK o  ���@�@  0 computerfolder computerFolderL o      �?�? 0 machinefolder machineFolderJ MNM r  ��OPO o  ���>�> 0 
backuptime 
backupTimeP o      �=�= 0 scheduledtime scheduledTimeN QRQ I ���<S�;
�< .ascrcmnt****      � ****S J  ��TT UVU o  ���:�: 0 sourcefolder sourceFolderV WXW o  ���9�9 "0 destinationdisk destinationDiskX YZY o  ���8�8 0 machinefolder machineFolderZ [�7[ o  ���6�6 0 scheduledtime scheduledTime�7  �;  R \]\ l ���5�4�3�5  �4  �3  ] ^�2^ I  ���1�0�/�1 0 diskinit diskInit�0  �/  �2  � _`_ l     �.�-�,�.  �-  �,  ` aba l     �+�*�)�+  �*  �)  b cdc l     �(�'�&�(  �'  �&  d efe l     �%�$�#�%  �$  �#  f ghg l     �"�!� �"  �!  �   h iji l     �kl�  k H B Function to detect if the selected hard drive is connected or not   l �mm �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o tj non l     �pq�  p T N This only happens once a hard drive has been selected and provides a reminder   q �rr �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e ro sts i  LOuvu I      ���� 0 diskinit diskInit�  �  v Z     �wx�yw l    	z��z =     	{|{ I     �}�� 0 itexists itExists} ~~ m    �� ���  d i s k ��� o    �� "0 destinationdisk destinationDisk�  �  | m    �
� boovtrue�  �  x I    ���� 0 freespaceinit freeSpaceInit� ��� m    �
� boovfals�  �  �  y k    ��� ��� n   ��� I    ���� 0 setfocus setFocus�  �  �  f    � ��� r    $��� n    "��� 3     "�
� 
cobj� o     �
�
 "0 messagesmissing messagesMissing� o      �	�	 0 msg  � ��� r   % ;��� l  % 9���� n   % 9��� 1   5 9�
� 
bhit� l  % 5���� I  % 5���
� .sysodlogaskr        TEXT� o   % &�� 0 msg  � ���
� 
appr� m   ' (�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� � ��
�  
disp� o   ) *���� 0 appicon appIcon� ����
�� 
btns� J   + /�� ��� m   + ,�� ���  C a n c e l   B a c k u p� ���� m   , -�� ���  O K��  � �����
�� 
dflt� m   0 1���� ��  �  �  �  �  � o      ���� 	0 reply  � ���� Z   < ������� l  < A������ =   < A��� o   < =���� 	0 reply  � m   = @�� ���  O K��  ��  � I   D I�������� 0 diskinit diskInit��  ��  ��  � Z   L �������� l  L W������ E   L W��� o   L Q���� &0 displaysleepstate displaySleepState� o   Q V���� 0 strawake strAwake��  ��  � k   Z ��� ��� r   Z c��� n   Z a��� 3   _ a��
�� 
cobj� o   Z _���� &0 messagescancelled messagesCancelled� o      ���� 0 msg  � ���� Z   d �������� l  d k������ =   d k��� o   d i���� 0 notifications  � m   i j��
�� boovtrue��  ��  � Z   n ������� l  n u������ =   n u��� o   n s���� 0 nsfw  � m   s t��
�� boovtrue��  ��  � I  x �����
�� .sysonotfnull��� ��� TEXT� o   x y���� 0 msg  � �����
�� 
appr� m   z }�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  ��  ��  ��  t ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  PS��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � Z     ������� l    	 ����  =     	 I     ������ 0 comparesizes compareSizes  o    ���� 0 sourcefolder sourceFolder �� o    ���� "0 destinationdisk destinationDisk��  ��   m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    � 	 n   

 I    �������� 0 setfocus setFocus��  ��    f    	  l   ��������  ��  ��    Z    a�� l   ���� =     o    ���� 	0 again   m    ��
�� boovfals��  ��   r     6 l    4���� n     4 1   0 4��
�� 
bhit l    0���� I    0��
�� .sysodlogaskr        TEXT m     ! � � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ? �� !
�� 
appr  m   " #"" �## , W o a h ,   B a c k   T h e   F u c k   U p! ��$%
�� 
disp$ o   $ %���� 0 appicon appIcon% ��&'
�� 
btns& J   & *(( )*) m   & '++ �,,  Y e s* -��- m   ' (.. �//  C a n c e l   B a c k u p��  ' ��0��
�� 
dflt0 m   + ,���� ��  ��  ��  ��  ��   o      ���� 	0 reply   121 l  9 <3����3 =   9 <454 o   9 :���� 	0 again  5 m   : ;��
�� boovtrue��  ��  2 6��6 r   ? ]787 l  ? [9����9 n   ? [:;: 1   W [��
�� 
bhit; l  ? W<����< I  ? W��=>
�� .sysodlogaskr        TEXT= m   ? B?? �@@ � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?> ��AB
�� 
apprA m   C FCC �DD , W o a h ,   B a c k   T h e   F u c k   U pB ��EF
�� 
dispE o   G H���� 0 appicon appIconF ��GH
�� 
btnsG J   I QII JKJ m   I LLL �MM  Y e sK N��N m   L OOO �PP  C a n c e l   B a c k u p��  H ��Q��
�� 
dfltQ m   R S���� ��  ��  ��  ��  ��  8 o      ���� 	0 reply  ��  ��   RSR l  b b��~�}�  �~  �}  S T�|T Z   b �UV�{WU l  b gX�z�yX =   b gYZY o   b c�x�x 	0 reply  Z m   c f[[ �\\  Y e s�z  �y  V I   j o�w�v�u�w 0 tidyup tidyUp�v  �u  �{  W k   r �]] ^_^ Z   r �`a�t�s` l  r }b�r�qb E   r }cdc o   r w�p�p &0 displaysleepstate displaySleepStated o   w |�o�o 0 strawake strAwake�r  �q  a r   � �efe n   � �ghg 3   � ��n
�n 
cobjh o   � ��m�m &0 messagescancelled messagesCancelledf o      �l�l 0 msg  �t  �s  _ iji l  � ��k�j�i�k  �j  �i  j k�hk Z   � �lm�g�fl l  � �n�e�dn =   � �opo o   � ��c�c 0 notifications  p m   � ��b
�b boovtrue�e  �d  m Z   � �qrs�aq l  � �t�`�_t =   � �uvu o   � ��^�^ 0 nsfw  v m   � ��]
�] boovtrue�`  �_  r I  � ��\wx
�\ .sysonotfnull��� ��� TEXTw o   � ��[�[ 0 msg  x �Zy�Y
�Z 
appry m   � �zz �{{ > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�Y  s |}| l  � �~�X�W~ =   � �� o   � ��V�V 0 nsfw  � m   � ��U
�U boovfals�X  �W  } ��T� I  � ��S��
�S .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �R��Q
�R 
appr� m   � ��� ��� 
 W B T F U�Q  �T  �a  �g  �f  �h  �|  � ��� l     �P�O�N�P  �O  �N  � ��� l     �M�L�K�M  �L  �K  � ��� l     �J�I�H�J  �I  �H  � ��� l     �G�F�E�G  �F  �E  � ��� l     �D�C�B�D  �C  �B  � ��� l     �A���A  �,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   � ���L   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .� ��� l     �@���@  � g a These folders, if required, are then set to their corresponding global variables declared above.   � ��� �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .� ��� i  TW��� I      �?�>�=�? 0 
backupinit 
backupInit�>  �=  � k     ��� ��� r     ��� 4     �<�
�< 
psxf� o    �;�; "0 destinationdisk destinationDisk� o      �:�: 	0 drive  � ��� l   �9���9  � ' !log (destinationDisk & "Backups")   � ��� B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )� ��� l   �8�7�6�8  �7  �6  � ��� Z    ,���5�4� l   ��3�2� =    ��� I    �1��0�1 0 itexists itExists� ��� m    	�� ���  f o l d e r� ��/� b   	 ��� o   	 
�.�. "0 destinationdisk destinationDisk� m   
 �� ���  B a c k u p s�/  �0  � m    �-
�- boovfals�3  �2  � O    (��� I   '�,�+�
�, .corecrel****      � null�+  � �*��
�* 
kocl� m    �)
�) 
cfol� �(��
�( 
insh� o    �'�' 	0 drive  � �&��%
�& 
prdt� K    #�� �$��#
�$ 
pnam� m     !�� ���  B a c k u p s�#  �%  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �5  �4  � ��� l  - -�"�!� �"  �!  �   � ��� Z   - d����� l  - >���� =   - >��� I   - <���� 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ��� b   / 8��� b   / 4��� o   / 0�� "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�� 0 machinefolder machineFolder�  �  � m   < =�
� boovfals�  �  � O   A `��� I  E _���
� .corecrel****      � null�  � ���
� 
kocl� m   G H�
� 
cfol� ���
� 
insh� n   I T��� 4   O T��
� 
cfol� m   P S�� ���  B a c k u p s� 4   I O��
� 
cdis� o   M N�� 	0 drive  � ���
� 
prdt� K   U [�� ���

� 
pnam� o   V Y�	�	 0 machinefolder machineFolder�
  �  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  �  � ��� l  e e����  �  �  � ��� O   e ���� k   i �� ��� l  i i����  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }   n   i y 4   t y�
� 
cfol o   u x�� 0 machinefolder machineFolder n   i t 4   o t�
� 
cfol m   p s �		  B a c k u p s 4   i o�

� 
cdis
 o   m n� �  	0 drive   o      ���� 0 backupfolder backupFolder� �� l  ~ ~����    log (backupFolder)    � $ l o g   ( b a c k u p F o l d e r )��  � m   e f�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  l  � ���������  ��  ��    Z   � ��� l  � ����� =   � � I   � ������� 0 itexists itExists  m   � � �  f o l d e r �� b   � � !  b   � �"#" b   � �$%$ o   � ����� "0 destinationdisk destinationDisk% m   � �&& �''  B a c k u p s /# o   � ����� 0 machinefolder machineFolder! m   � �(( �))  / L a t e s t��  ��   m   � ���
�� boovfals��  ��   O   � �*+* I  � �����,
�� .corecrel****      � null��  , ��-.
�� 
kocl- m   � ���
�� 
cfol. ��/0
�� 
insh/ o   � ����� 0 backupfolder backupFolder0 ��1��
�� 
prdt1 K   � �22 ��3��
�� 
pnam3 m   � �44 �55  L a t e s t��  ��  + m   � �66�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��   r   � �787 m   � ���
�� boovfals8 o      ���� 0 initialbackup initialBackup 9:9 l  � ���������  ��  ��  : ;<; O   � �=>= k   � �?? @A@ r   � �BCB n   � �DED 4   � ���F
�� 
cfolF m   � �GG �HH  L a t e s tE n   � �IJI 4   � ���K
�� 
cfolK o   � ����� 0 machinefolder machineFolderJ n   � �LML 4   � ���N
�� 
cfolN m   � �OO �PP  B a c k u p sM 4   � ���Q
�� 
cdisQ o   � ����� 	0 drive  C o      ���� 0 latestfolder latestFolderA R��R l  � ���ST��  S  log (backupFolder)   T �UU $ l o g   ( b a c k u p F o l d e r )��  > m   � �VV�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  < WXW l  � ���������  ��  ��  X Y��Y I   � ��������� 
0 backup  ��  ��  ��  � Z[Z l     ��������  ��  ��  [ \]\ l     ��������  ��  ��  ] ^_^ l     ��������  ��  ��  _ `a` l     ��������  ��  ��  a bcb l     ��������  ��  ��  c ded l     ��fg��  f j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.   g �hh �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .e iji l     ��kl��  k � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   l �mm�   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .j non l     ��pq��  p � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   q �rrP   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .o sts l     ��uv��  u � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   v �ww>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .t xyx i  X[z{z I      �������� 0 tidyup tidyUp��  ��  { k    �|| }~} r     � 4     ���
�� 
psxf� o    ���� "0 destinationdisk destinationDisk� o      ���� 	0 drive  ~ ��� l   ��������  ��  ��  � ���� O   ���� k   ��� ��� l   ������  � A ;set creationDates to creation date of items of backupFolder   � ��� v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e r� ��� r    ��� n    ��� 4    ���
�� 
cfol� o    ���� 0 machinefolder machineFolder� n    ��� 4    ���
�� 
cfol� m    �� ���  B a c k u p s� 4    ���
�� 
cdis� o    ���� 	0 drive  � o      ���� 0 bf bF� ��� r    ��� n    ��� 1    ��
�� 
ascd� n    ��� 2   ��
�� 
cobj� o    ���� 0 bf bF� o      ���� 0 creationdates creationDates� ��� r     &��� n     $��� 4   ! $���
�� 
cobj� m   " #���� � o     !���� 0 creationdates creationDates� o      ���� 0 theoldestdate theOldestDate� ��� r   ' *��� m   ' (���� � o      ���� 0 j  � ��� l  + +��������  ��  ��  � ��� Y   + n�������� k   9 i�� ��� r   9 ?��� n   9 =��� 4   : =���
�� 
cobj� o   ; <���� 0 i  � o   9 :���� 0 creationdates creationDates� o      ���� 0 thisdate thisDate� ���� Z   @ i������� l  @ H������ >  @ H��� n   @ F��� 1   D F��
�� 
pnam� 4   @ D���
�� 
cobj� o   B C���� 0 i  � m   F G�� ���  L a t e s t��  ��  � Z   K e������� l  K N������ A   K N��� o   K L���� 0 thisdate thisDate� o   L M���� 0 theoldestdate theOldestDate��  ��  � k   Q a�� ��� r   Q T��� o   Q R���� 0 thisdate thisDate� o      ���� 0 theoldestdate theOldestDate� ��� r   U X��� o   U V���� 0 i  � o      ���� 0 j  � ��� l  Y Y������  � " log (item j of backupFolder)   � ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )� ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  � m   . /�� � I  / 4�~��}
�~ .corecnte****       ****� o   / 0�|�| 0 creationdates creationDates�}  ��  � ��� l  o o�{�z�y�{  �z  �y  � ��� l  o o�x���x  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o�w���w  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� l  o o�v���v  �  delete item (j + 1) of bF   � ��� 2 d e l e t e   i t e m   ( j   +   1 )   o f   b F� ��� l  o o�u�t�s�u  �t  �s  � ��� r   o y��� c   o w��� l  o u��r�q� n   o u��� 4   p u�p�
�p 
cobj� l  q t��o�n� [   q t� � o   q r�m�m 0 j    m   r s�l�l �o  �n  � o   o p�k�k 0 bf bF�r  �q  � m   u v�j
�j 
TEXT� o      �i�i  0 foldertodelete folderToDelete�  r   z � c   z  n   z } 1   { }�h
�h 
psxp o   z {�g�g  0 foldertodelete folderToDelete m   } ~�f
�f 
TEXT o      �e�e  0 foldertodelete folderToDelete 	
	 I  � ��d�c
�d .ascrcmnt****      � **** l  � ��b�a b   � � m   � � � $ f o l d e r   t o   d e l e t e :   o   � ��`�`  0 foldertodelete folderToDelete�b  �a  �c  
  l  � ��_�^�]�_  �^  �]    r   � � b   � � b   � � m   � � �  r m   - r f   ' o   � ��\�\  0 foldertodelete folderToDelete m   � � �  ' o      �[�[ 0 todelete toDelete   I  � ��Z!�Y
�Z .sysoexecTEXT���     TEXT! o   � ��X�X 0 todelete toDelete�Y    "#" l  � ��W�V�U�W  �V  �U  # $%$ l  � ��T�S�R�T  �S  �R  % &'& n  � �()( I   � ��Q�P�O�Q 0 setfocus setFocus�P  �O  )  f   � �' *+* r   � �,-, l  � �.�N�M. n   � �/0/ 1   � ��L
�L 
bhit0 l  � �1�K�J1 I  � ��I23
�I .sysodlogaskr        TEXT2 m   � �44 �55B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .3 �H67
�H 
appr6 m   � �88 �99 , W o a h ,   B a c k   T h e   F u c k   U p7 �G:;
�G 
disp: o   � ��F�F 0 appicon appIcon; �E<=
�E 
btns< J   � �>> ?@? m   � �AA �BB  E m p t y   T r a s h@ C�DC m   � �DD �EE  C a n c e l   B a c k u p�D  = �CF�B
�C 
dfltF m   � ��A�A �B  �K  �J  �N  �M  - o      �@�@ 
0 reply2  + GHG Z   �@IJ�?KI l  � �L�>�=L =   � �MNM o   � ��<�< 
0 reply2  N m   � �OO �PP  E m p t y   T r a s h�>  �=  J I  � ��;Q�:
�; .fndremptnull��� ��� obj Q l  � �R�9�8R 1   � ��7
�7 
trsh�9  �8  �:  �?  K Z   �@ST�6�5S l  � �U�4�3U E   � �VWV o   � ��2�2 &0 displaysleepstate displaySleepStateW o   � ��1�1 0 strawake strAwake�4  �3  T k   �<XX YZY r   � �[\[ n   � �]^] 3   � ��0
�0 
cobj^ o   � ��/�/ &0 messagescancelled messagesCancelled\ o      �.�. 0 msg  Z _�-_ Z   �<`a�,�+` l  �b�*�)b =   �cdc o   � �(�( 0 notifications  d m   �'
�' boovtrue�*  �)  a Z  8efg�&e l h�%�$h =  iji o  
�#�# 0 nsfw  j m  
�"
�" boovtrue�%  �$  f I �!kl
�! .sysonotfnull��� ��� TEXTk o  � �  0 msg  l �m�
� 
apprm m  nn �oo > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  g pqp l $r��r =  $sts o  "�� 0 nsfw  t m  "#�
� boovfals�  �  q u�u I '4�vw
� .sysonotfnull��� ��� TEXTv m  '*xx �yy , Y o u   s t o p p e d   b a c k i n g   u pw �z�
� 
apprz m  -0{{ �|| 
 W B T F U�  �  �&  �,  �+  �-  �6  �5  H }~} l AA����  �  �  ~ � I  AG���� 0 freespaceinit freeSpaceInit� ��� m  BC�
� boovtrue�  �  � ��� Z  H������ l HS���
� E  HS��� o  HM�	�	 &0 displaysleepstate displaySleepState� o  MR�� 0 strawake strAwake�  �
  � Z  V������ l V]���� =  V]��� o  V[�� 0 notifications  � m  [\�
� boovtrue�  �  � Z  `������ l `g�� ��� =  `g��� o  `e���� 0 nsfw  � m  ef��
�� boovtrue�   ��  � k  j}�� ��� I jw����
�� .sysonotfnull��� ��� TEXT� m  jm�� ��� � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .� �����
�� 
appr� m  ps�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I x}�����
�� .sysodelanull��� ��� nmbr� m  xy���� ��  ��  � ��� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� m  ���� ��� t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n� �����
�� 
appr� m  ���� ��� 
 W B T F U��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  �  �  �  �  �  �  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  y ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � M G Function that carries out the backup process and is split in 2 parts.    � ��� �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  � ��� l     ������  �*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   � ���H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .� ��� l     ������  � � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   � ���   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .� ��� i  \_��� I      �������� 
0 backup  ��  ��  � k    ��� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovtrue��  ��  � o      ���� 0 t  � ��� l  	 	��������  ��  ��  � ��� r   	 ��� m   	 
��
�� boovtrue� o      ���� 0 isbackingup isBackingUp� ��� l   ��������  ��  ��  � ��� I    �������� (0 animatemenubaricon animateMenuBarIcon��  ��  � ��� I   �����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ��� l   ��������  ��  ��  � ��� I    #������� 0 	stopwatch  � ���� m    �� ��� 
 s t a r t��  ��  � ��� l  $ $��������  ��  ��  � ��� O   $y��� k   (x�� ��� l  ( (������  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d� ��� l  ( (��� ��  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up     � �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p�  r   ( 0 c   ( . 4   ( ,��
�� 
psxf o   * +���� 0 sourcefolder sourceFolder m   , -��
�� 
TEXT o      ���� 0 
foldername 
folderName 	
	 r   1 9 n   1 7 1   5 7��
�� 
pnam 4   1 5��
�� 
cfol o   3 4���� 0 
foldername 
folderName o      ���� 0 
foldername 
folderName
  l  : :����    log (folderName)		    � $ l o g   ( f o l d e r N a m e ) 	 	  l  : :��������  ��  ��    Z   : ����� l  : A���� =   : A o   : ?���� 0 initialbackup initialBackup m   ? @��
�� boovfals��  ��   k   D �   I  D R����!
�� .corecrel****      � null��  ! ��"#
�� 
kocl" m   F G��
�� 
cfol# ��$%
�� 
insh$ o   H I���� 0 backupfolder backupFolder% ��&��
�� 
prdt& K   J N'' ��(��
�� 
pnam( o   K L���� 0 t  ��  ��    )*) l  S S��������  ��  ��  * +,+ r   S ]-.- c   S Y/0/ 4   S W��1
�� 
psxf1 o   U V���� 0 sourcefolder sourceFolder0 m   W X��
�� 
TEXT. o      ���� (0 activesourcefolder activeSourceFolder, 232 r   ^ h454 4   ^ d��6
�� 
cfol6 o   ` c���� (0 activesourcefolder activeSourceFolder5 o      ���� (0 activesourcefolder activeSourceFolder3 787 l  i i��9:��  9  log (activeSourceFolder)   : �;; 0 l o g   ( a c t i v e S o u r c e F o l d e r )8 <=< r   i �>?> n   i ~@A@ 4   { ~��B
�� 
cfolB o   | }���� 0 t  A n   i {CDC 4   v {��E
�� 
cfolE o   w z���� 0 machinefolder machineFolderD n   i vFGF 4   q v��H
�� 
cfolH m   r uII �JJ  B a c k u p sG 4   i q��K
�� 
cdisK o   m p���� 	0 drive  ? o      ���� (0 activebackupfolder activeBackupFolder= LML l  � ���NO��  N  log (activeBackupFolder)   O �PP 0 l o g   ( a c t i v e B a c k u p F o l d e r )M Q��Q I  � ����R
�� .corecrel****      � null�  R �~ST
�~ 
koclS m   � ��}
�} 
cfolT �|UV
�| 
inshU o   � ��{�{ (0 activebackupfolder activeBackupFolderV �zW�y
�z 
prdtW K   � �XX �xY�w
�x 
pnamY o   � ��v�v 0 
foldername 
folderName�w  �y  ��  ��  ��   Z[Z l  � ��u�t�s�u  �t  �s  [ \]\ l  � ��r�q�p�r  �q  �p  ] ^_^ l  � ��o`a�o  ` o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   a �bb �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e_ cdc l  � ��nef�n  e q k An atomic version would just to do a straight backup without versioning to see the function at full effect   f �gg �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c td hih l  � ��mjk�m  j D > This means that only new or updated files will be copied over   k �ll |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e ri mnm l  � ��lop�l  o ] W Any deleted files in the source folder will be deleted in the destination folder too		   p �qq �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	n rsr l  � ��ktu�k  t   Original sync code    u �vv (   O r i g i n a l   s y n c   c o d e  s wxw l  � ��jyz�j  y z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   z �{{ �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "x |}| l  � ��i�h�g�i  �h  �g  } ~~ l  � ��f���f  �   Original code   � ���    O r i g i n a l   c o d e ��� l  � ��e���e  � i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   � ��� �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h� ��� l  � ��d���d  � x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   � ��� �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .� ��� l  � ��c���c  � p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   � ��� �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .� ��� l  � ��b���b  � � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   � ����   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .� ��� l  � ��a���a  � � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   � ���v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .� ��� l  � ��`�_�^�`  �_  �^  � ��� l  � ��]�\�[�]  �\  �[  � ��Z� Z   �x����Y� l  � ���X�W� =   � ���� o   � ��V�V 0 initialbackup initialBackup� m   � ��U
�U boovtrue�X  �W  � k   ���� ��� r   � ���� n   � ���� 1   � ��T
�T 
time� l  � ���S�R� I  � ��Q�P�O
�Q .misccurdldt    ��� null�P  �O  �S  �R  � o      �N�N 0 	starttime 	startTime� ��� Z   ����M�L� l  � ���K�J� E   � ���� o   � ��I�I &0 displaysleepstate displaySleepState� o   � ��H�H 0 strawake strAwake�K  �J  � Z   ����G�F� l  � ���E�D� =   � ���� o   � ��C�C 0 notifications  � m   � ��B
�B boovtrue�E  �D  � Z   � �����A� l  � ���@�?� =   � ���� o   � ��>�> 0 nsfw  � m   � ��=
�= boovtrue�@  �?  � I  � ��<��
�< .sysonotfnull��� ��� TEXT� m   � ��� ��� � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .� �;��:
�; 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�:  � ��� l  � ���9�8� =   � ���� o   � ��7�7 0 nsfw  � m   � ��6
�6 boovfals�9  �8  � ��5� I  � ��4��
�4 .sysonotfnull��� ��� TEXT� m   � ��� ��� \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e� �3��2
�3 
appr� m   � ��� ��� 
 W B T F U�2  �5  �A  �G  �F  �M  �L  � ��� l �1�0�/�1  �0  �/  � ��� l �.�-�,�.  �-  �,  � ��� l �+�*�)�+  �*  �)  � ��� l �(�'�&�(  �'  �&  � ��� r  ��� c  ��� l ��%�$� b  ��� b  ��� b  ��� b  ��� b  ��� o  	�#�# "0 destinationdisk destinationDisk� m  	�� ���  B a c k u p s /� o  �"�" 0 machinefolder machineFolder� m  �� ���  / L a t e s t /� o  �!�! 0 
foldername 
folderName� m  �� ���  /�%  �$  � m  � 
�  
TEXT� o      �� 0 d  � ��� l   ����  � A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   � ��� v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l   ����  �  �  � ��� Q   A���� k  #8    l ##��  ��do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' '" & sourceFolder & "' '" & d & "/'"    �< d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "  l ##����  �  �   	 l ##�
�  
   optimised(?)    �    o p t i m i s e d ( ? )	  I #6��
� .sysoexecTEXT���     TEXT b  #2 b  #. b  #, b  #( m  #& �	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   ' o  &'�� 0 sourcefolder sourceFolder m  (+ �  '   ' o  ,-�� 0 d   m  .1 �  / '�    l 77����  �  �    �  l 77�!"�  ! U Odo shell script "rsync -aqvz --cvs-exclude '" & sourceFolder & "' '" & d & "/'"   " �## � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "�  � R      ���

� .ascrerr ****      � ****�  �
  � l @@�	���	  �  �  � $%$ l BB����  �  �  % &'& l BB����  �  �  ' ()( l BB� �����   ��  ��  ) *+* l BB��������  ��  ��  + ,-, l BB��������  ��  ��  - ./. r  BI010 m  BC��
�� boovfals1 o      ���� 0 isbackingup isBackingUp/ 232 r  JQ454 m  JK��
�� boovfals5 o      ���� 0 manualbackup manualBackup3 676 l RR��������  ��  ��  7 898 Z  R�:;����: l R]<����< E  R]=>= o  RW���� &0 displaysleepstate displaySleepState> o  W\���� 0 strawake strAwake��  ��  ; k  `�?? @A@ r  `mBCB n  `iDED 1  ei��
�� 
timeE l `eF����F I `e������
�� .misccurdldt    ��� null��  ��  ��  ��  C o      ���� 0 endtime endTimeA GHG r  nuIJI n nsKLK I  os�������� 0 getduration getDuration��  ��  L  f  noJ o      ���� 0 duration  H MNM r  v�OPO n  vQRQ 3  {��
�� 
cobjR o  v{���� $0 messagescomplete messagesCompleteP o      ���� 0 msg  N STS Z  ��UV����U l ��W����W =  ��XYX o  ������ 0 notifications  Y m  ����
�� boovtrue��  ��  V Z  ��Z[\��Z l ��]����] =  ��^_^ o  ������ 0 nsfw  _ m  ����
�� boovtrue��  ��  [ k  ��`` aba I ����cd
�� .sysonotfnull��� ��� TEXTc b  ��efe b  ��ghg b  ��iji m  ��kk �ll 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  j o  ������ 0 duration  h m  ��mm �nn    m i n u t e s ! 
f o  ������ 0 msg  d ��o��
�� 
appro m  ��pp �qq : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  b r��r I ����s��
�� .sysodelanull��� ��� nmbrs m  ������ ��  ��  \ tut l ��v����v =  ��wxw o  ������ 0 nsfw  x m  ����
�� boovfals��  ��  u y��y k  ��zz {|{ I ����}~
�� .sysonotfnull��� ��� TEXT} b  ��� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
~ �����
�� 
appr� m  ���� ���  B a c k e d   u p !��  | ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  T ��� l ����������  ��  ��  � ���� n ����� I  ���������� $0 resetmenubaricon resetMenuBarIcon��  ��  �  f  ����  ��  ��  9 ��� l ����������  ��  ��  � ���� Z  ��������� l �������� =  ����� o  ������  0 backupthenquit backupThenQuit� m  ����
�� boovtrue��  ��  � I �������
�� .aevtquitnull��� ��� null� m  ����                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ��  ��  � ��� l 	������ =  	��� o  ���� 0 initialbackup initialBackup� m  ��
�� boovfals��  ��  � ���� k  t�� ��� r  +��� c  )��� l '������ b  '��� b  #��� b  !��� b  ��� b  ��� b  ��� b  ��� o  ���� "0 destinationdisk destinationDisk� m  �� ���  B a c k u p s /� o  ���� 0 machinefolder machineFolder� m  �� ���  /� o  ���� 0 t  � m   �� ���  /� o  !"���� 0 
foldername 
folderName� m  #&�� ���  /��  ��  � m  '(��
�� 
TEXT� o      ���� 0 c  � ��� r  ,E��� c  ,C��� l ,A������ b  ,A��� b  ,=��� b  ,;��� b  ,7��� b  ,3��� o  ,/���� "0 destinationdisk destinationDisk� m  /2�� ���  B a c k u p s /� o  36���� 0 machinefolder machineFolder� m  7:�� ���  / L a t e s t /� o  ;<���� 0 
foldername 
folderName� m  =@�� ���  /��  ��  � m  AB��
�� 
TEXT� o      ���� 0 d  � ��� I FO�����
�� .ascrcmnt****      � ****� l FK������ b  FK��� m  FI�� ���  b a c k i n g   u p   t o :  � o  IJ���� 0 d  ��  ��  ��  � ��� l PP��������  ��  ��  � ��� Z  P�������� l P[������ E  P[��� o  PU���� &0 displaysleepstate displaySleepState� o  UZ���� 0 strawake strAwake��  ��  � k  ^��� ��� r  ^k��� n  ^g��� 1  cg��
�� 
time� l ^c������ I ^c������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 	starttime 	startTime� ��� r  lw��� n  lu��� 3  qu�
� 
cobj� o  lq�~�~ *0 messagesencouraging messagesEncouraging� o      �}�} 0 msg  � ��|� Z  x����{�z� l x��y�x� =  x��� o  x}�w�w 0 notifications  � m  }~�v
�v boovtrue�y  �x  � Z  ������u� l ����t�s� =  ���	 � o  ���r�r 0 nsfw  	  m  ���q
�q boovtrue�t  �s  � k  ��		 			 I ���p		
�p .sysonotfnull��� ��� TEXT	 o  ���o�o 0 msg  	 �n	�m
�n 
appr	 m  ��		 �		 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�m  	 		�l		 I ���k	
�j
�k .sysodelanull��� ��� nmbr	
 m  ���i�i �j  �l  � 			 l ��	�h�g	 =  ��			 o  ���f�f 0 nsfw  	 m  ���e
�e boovfals�h  �g  	 	�d	 k  ��		 			 I ���c		
�c .sysonotfnull��� ��� TEXT	 m  ��		 �		  B a c k i n g   u p	 �b	�a
�b 
appr	 m  ��		 �		 
 W B T F U�a  	 	�`	 I ���_	�^
�_ .sysodelanull��� ��� nmbr	 m  ���]�] �^  �`  �d  �u  �{  �z  �|  ��  ��  � 			 l ���\�[�Z�\  �[  �Z  	 		 	 l ���Y�X�W�Y  �X  �W  	  	!	"	! l ���V�U�T�V  �U  �T  	" 	#	$	# l ���S�R�Q�S  �R  �Q  	$ 	%	&	% l ���P�O�N�P  �O  �N  	& 	'	(	' Q  ��	)	*	+	) k  ��	,	, 	-	.	- l ���M	/	0�M  	/��do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings'  --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	0 �	1	1� d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "	. 	2	3	2 l ���L�K�J�L  �K  �J  	3 	4	5	4 l ���I	6	7�I  	6   optimised(?)   	7 �	8	8    o p t i m i s e d ( ? )	5 	9	:	9 I ���H	;�G
�H .sysoexecTEXT���     TEXT	; b  ��	<	=	< b  ��	>	?	> b  ��	@	A	@ b  ��	B	C	B b  ��	D	E	D b  ��	F	G	F m  ��	H	H �	I	I
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '	G o  ���F�F 0 c  	E m  ��	J	J �	K	K  '   '	C o  ���E�E 0 sourcefolder sourceFolder	A m  ��	L	L �	M	M  '   '	? o  ���D�D 0 d  	= m  ��	N	N �	O	O  / '�G  	: 	P	Q	P l ���C�B�A�C  �B  �A  	Q 	R�@	R l ���?	S	T�?  	S � zdo shell script "rsync -aqvz --cvs-exclude --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	T �	U	U � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "�@  	* R      �>�=�<
�> .ascrerr ****      � ****�=  �<  	+ l ���;�:�9�;  �:  �9  	( 	V	W	V l ���8�7�6�8  �7  �6  	W 	X	Y	X l ���5�4�3�5  �4  �3  	Y 	Z	[	Z l ���2�1�0�2  �1  �0  	[ 	\	]	\ l ���/�.�-�/  �.  �-  	] 	^	_	^ l ���,�+�*�,  �+  �*  	_ 	`	a	` r  ��	b	c	b m  ���)
�) boovfals	c o      �(�( 0 isbackingup isBackingUp	a 	d	e	d r  �	f	g	f m  ���'
�' boovfals	g o      �&�& 0 manualbackup manualBackup	e 	h	i	h l �%�$�#�%  �$  �#  	i 	j�"	j Z  t	k	l�!	m	k = 	n	o	n n  	p	q	p 2 
� 
�  
cobj	q l 
	r��	r c  
	s	t	s 4  �	u
� 
psxf	u o  �� 0 c  	t m  	�
� 
alis�  �  	o J  ��  	l k  j	v	v 	w	x	w l �	y	z�  	y % delete folder t of backupFolder   	z �	{	{ > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r	x 	|	}	| l ����  �  �  	} 	~		~ r  	�	�	� c  	�	�	� n  	�	�	� 4  �	�
� 
cfol	� o  �� 0 t  	� o  �� 0 backupfolder backupFolder	� m  �
� 
TEXT	� o      �� 0 oldestfolder oldestFolder	 	�	�	� r  &	�	�	� c  $	�	�	� n  "	�	�	� 1  "�
� 
psxp	� o  �� 0 oldestfolder oldestFolder	� m  "#�
� 
TEXT	� o      �� 0 oldestfolder oldestFolder	� 	�	�	� I '0�	��
� .ascrcmnt****      � ****	� l ',	��
�		� b  ',	�	�	� m  '*	�	� �	�	�  t o   d e l e t e :  	� o  *+�� 0 oldestfolder oldestFolder�
  �	  �  	� 	�	�	� l 11����  �  �  	� 	�	�	� r  1<	�	�	� b  1:	�	�	� b  16	�	�	� m  14	�	� �	�	�  r m   - r f   '	� o  45�� 0 oldestfolder oldestFolder	� m  69	�	� �	�	�  '	� o      �� 0 todelete toDelete	� 	�	�	� I =B�	��
� .sysoexecTEXT���     TEXT	� o  =>� �  0 todelete toDelete�  	� 	�	�	� l CC��������  ��  ��  	� 	���	� Z  Cj	�	�����	� l CN	�����	� E  CN	�	�	� o  CH���� &0 displaysleepstate displaySleepState	� o  HM���� 0 strawake strAwake��  ��  	� k  Qf	�	� 	�	�	� r  Q^	�	�	� n  QZ	�	�	� 1  VZ��
�� 
time	� l QV	�����	� I QV������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 endtime endTime	� 	�	�	� r  _f	�	�	� n _d	�	�	� I  `d�������� 0 getduration getDuration��  ��  	�  f  _`	� o      ���� 0 duration  	� 	�	�	� l gg��	�	���  	� � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   	� �	�	� d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "	� 	�	�	� l gg��	�	���  	�  delay 2   	� �	�	�  d e l a y   2	� 	�	�	� l gg��������  ��  ��  	� 	�	�	� r  gr	�	�	� n  gp	�	�	� 3  lp��
�� 
cobj	� o  gl���� $0 messagescomplete messagesComplete	� o      ���� 0 msg  	� 	�	�	� Z  s`	�	�����	� l sz	�����	� =  sz	�	�	� o  sx���� 0 notifications  	� m  xy��
�� boovtrue��  ��  	� k  }\	�	� 	�	�	� Z  }&	�	���	�	� l }�	�����	� =  }�	�	�	� o  }~���� 0 duration  	� m  ~�	�	� ?�      ��  ��  	� Z  ��	�	�	���	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� o  ������ 0 msg  	� ��	���
�� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  	� 	�	�	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovfals��  ��  	� 
 ��
  k  ��

 


 I ����


�� .sysonotfnull��� ��� TEXT
 b  ��


 b  ��

	
 m  ��



 �

  A n d   i n  
	 o  ������ 0 duration  
 m  ��

 �

    m i n u t e ! 

 ��
��
�� 
appr
 m  ��

 �

  B a c k e d   u p !��  
 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  ��  ��  ��  	� Z  �&


��
 l ��
����
 =  ��


 o  ������ 0 nsfw  
 m  ����
�� boovtrue��  ��  
 k  ��

 


 I ����


�� .sysonotfnull��� ��� TEXT
 b  ��


 b  ��
 
!
  b  ��
"
#
" m  ��
$
$ �
%
%  A n d   i n  
# o  ������ 0 duration  
! m  ��
&
& �
'
'    m i n u t e s ! 

 o  ������ 0 msg  
 ��
(��
�� 
appr
( m  ��
)
) �
*
* : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
 
+��
+ I ����
,��
�� .sysodelanull��� ��� nmbr
, m  ������ ��  ��  
 
-
.
- l �
/����
/ =  �
0
1
0 o  ����� 0 nsfw  
1 m  ��
�� boovfals��  ��  
. 
2��
2 k  	"
3
3 
4
5
4 I 	��
6
7
�� .sysonotfnull��� ��� TEXT
6 b  	
8
9
8 b  	
:
;
: m  	
<
< �
=
=  A n d   i n  
; o  ���� 0 duration  
9 m  
>
> �
?
?    m i n u t e s ! 

7 ��
@��
�� 
appr
@ m  
A
A �
B
B  B a c k e d   u p !��  
5 
C��
C I "��
D��
�� .sysodelanull��� ��� nmbr
D m  ���� ��  ��  ��  ��  	� 
E
F
E l ''��������  ��  ��  
F 
G��
G Z  '\
H
I
J��
H l '.
K����
K =  '.
L
M
L o  ',���� 0 nsfw  
M m  ,-��
�� boovtrue��  ��  
I I 1>��
N
O
�� .sysonotfnull��� ��� TEXT
N m  14
P
P �
Q
Q � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .
O ��
R��
�� 
appr
R m  7:
S
S �
T
T @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  
J 
U
V
U l AH
W����
W =  AH
X
Y
X o  AF���� 0 nsfw  
Y m  FG��
�� boovfals��  ��  
V 
Z��
Z I KX��
[
\
�� .sysonotfnull��� ��� TEXT
[ m  KN
]
] �
^
^ p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d
\ ��
_��
�� 
appr
_ m  QT
`
` �
a
a 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  ��  ��  	� 
b
c
b l aa��������  ��  ��  
c 
d��
d n af
e
f
e I  bf�������� $0 resetmenubaricon resetMenuBarIcon��  ��  
f  f  ab��  ��  ��  ��  �!  	m k  mt
g
g 
h
i
h Z  mX
j
k����
j l mx
l����
l E  mx
m
n
m o  mr���� &0 displaysleepstate displaySleepState
n o  rw���� 0 strawake strAwake��  ��  
k k  {T
o
o 
p
q
p r  {�
r
s
r n  {�
t
u
t 1  ����
�� 
time
u l {�
v���
v I {��~�}�|
�~ .misccurdldt    ��� null�}  �|  ��  �  
s o      �{�{ 0 endtime endTime
q 
w
x
w r  ��
y
z
y n ��
{
|
{ I  ���z�y�x�z 0 getduration getDuration�y  �x  
|  f  ��
z o      �w�w 0 duration  
x 
}
~
} r  ��

�
 n  ��
�
�
� 3  ���v
�v 
cobj
� o  ���u�u $0 messagescomplete messagesComplete
� o      �t�t 0 msg  
~ 
��s
� Z  �T
�
��r�q
� l ��
��p�o
� =  ��
�
�
� o  ���n�n 0 notifications  
� m  ���m
�m boovtrue�p  �o  
� Z  �P
�
��l
�
� l ��
��k�j
� =  ��
�
�
� o  ���i�i 0 duration  
� m  ��
�
� ?�      �k  �j  
� Z  ��
�
�
��h
� l ��
��g�f
� =  ��
�
�
� o  ���e�e 0 nsfw  
� m  ���d
�d boovtrue�g  �f  
� k  ��
�
� 
�
�
� I ���c
�
�
�c .sysonotfnull��� ��� TEXT
� b  ��
�
�
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  A n d   i n  
� o  ���b�b 0 duration  
� m  ��
�
� �
�
�    m i n u t e ! 

� o  ���a�a 0 msg  
� �`
��_
�` 
appr
� m  ��
�
� �
�
� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�_  
� 
��^
� I ���]
��\
�] .sysodelanull��� ��� nmbr
� m  ���[�[ �\  �^  
� 
�
�
� l ��
��Z�Y
� =  ��
�
�
� o  ���X�X 0 nsfw  
� m  ���W
�W boovfals�Z  �Y  
� 
��V
� k  ��
�
� 
�
�
� I ���U
�
�
�U .sysonotfnull��� ��� TEXT
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  A n d   i n  
� o  ���T�T 0 duration  
� m  ��
�
� �
�
�    m i n u t e ! 

� �S
��R
�S 
appr
� m  ��
�
� �
�
�  B a c k e d   u p !�R  
� 
��Q
� I ���P
��O
�P .sysodelanull��� ��� nmbr
� m  ���N�N �O  �Q  �V  �h  �l  
� Z  P
�
�
��M
� l 
��L�K
� =  
�
�
� o  �J�J 0 nsfw  
� m  �I
�I boovtrue�L  �K  
� k  &
�
� 
�
�
� I  �H
�
�
�H .sysonotfnull��� ��� TEXT
� b  
�
�
� b  
�
�
� b  
�
�
� m  
�
� �
�
�  A n d   i n  
� o  �G�G 0 duration  
� m  
�
� �
�
�    m i n u t e s ! 

� o  �F�F 0 msg  
� �E
��D
�E 
appr
� m  
�
� �
�
� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�D  
� 
��C
� I !&�B
��A
�B .sysodelanull��� ��� nmbr
� m  !"�@�@ �A  �C  
� 
�
�
� l )0
��?�>
� =  )0
�
�
� o  ).�=�= 0 nsfw  
� m  ./�<
�< boovfals�?  �>  
� 
��;
� k  3L
�
� 
�
�
� I 3F�:
�
�
�: .sysonotfnull��� ��� TEXT
� b  3<
�
�
� b  38
�
�
� m  36
�
� �
�
�  A n d   i n  
� o  67�9�9 0 duration  
� m  8;
�
� �
�
�    m i n u t e s ! 

� �8
��7
�8 
appr
� m  ?B
�
� �
�
�  B a c k e d   u p !�7  
� 
��6
� I GL�5
��4
�5 .sysodelanull��� ��� nmbr
� m  GH�3�3 �4  �6  �;  �M  �r  �q  �s  ��  ��  
i 
�
�
� l YY�2�1�0�2  �1  �0  
� 
�
�
� n Y^
�
�
� I  Z^�/�.�-�/ $0 resetmenubaricon resetMenuBarIcon�.  �-  
�  f  YZ
� 
�
�
� l __�,�+�*�,  �+  �*  
� 
��)
� Z  _t
�
��(�'
� l _f
��&�%
� =  _f   o  _d�$�$  0 backupthenquit backupThenQuit m  de�#
�# boovtrue�&  �%  
� I ip�"�!
�" .aevtquitnull��� ��� null m  il                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �!  �(  �'  �)  �"  ��  �Y  �Z  � m   $ %�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  l zz� ���   �  �   � I  z���� 0 	stopwatch   	�	 m  {~

 �  f i n i s h�  �  �  �  l     ����  �  �    l     ����  �  �    l     ����  �  �    l     ����  �  �    l     ����  �  �    l     �
�
   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     �	�	   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  !  l     �"#�  " � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   # �$$ - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -! %&% l     ����  �  �  & '(' l     �)*�  ) � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   * �++   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .( ,-, l     �./�  . � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   / �00�   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .- 121 i  `c343 I      �5�� 0 itexists itExists5 676 o      � �  0 
objecttype 
objectType7 8��8 o      ���� 
0 object  ��  �  4 l    W9:;9 O     W<=< Z    V>?@��> l   A����A =    BCB o    ���� 0 
objecttype 
objectTypeC m    DD �EE  d i s k��  ��  ? Z   
 FG��HF I  
 ��I��
�� .coredoexnull���     ****I 4   
 ��J
�� 
cdisJ o    ���� 
0 object  ��  G L    KK m    ��
�� boovtrue��  H L    LL m    ��
�� boovfals@ MNM l   "O����O =    "PQP o     ���� 0 
objecttype 
objectTypeQ m     !RR �SS  f i l e��  ��  N TUT Z   % 7VW��XV I  % -��Y��
�� .coredoexnull���     ****Y 4   % )��Z
�� 
fileZ o   ' (���� 
0 object  ��  W L   0 2[[ m   0 1��
�� boovtrue��  X L   5 7\\ m   5 6��
�� boovfalsU ]^] l  : =_����_ =   : =`a` o   : ;���� 0 
objecttype 
objectTypea m   ; <bb �cc  f o l d e r��  ��  ^ d��d Z   @ Ref��ge I  @ H��h��
�� .coredoexnull���     ****h 4   @ D��i
�� 
cfoli o   B C���� 
0 object  ��  f L   K Mjj m   K L��
�� boovtrue��  g L   P Rkk m   P Q��
�� boovfals��  ��  = m     ll�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  : "  (string, string) as Boolean   ; �mm 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n2 non l     ��������  ��  ��  o pqp l     ��������  ��  ��  q rsr l     ��������  ��  ��  s tut l     ��������  ��  ��  u vwv l     ��������  ��  ��  w xyx l     ��z{��  z � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   { �||Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .y }~} l     �����   P J This means that backups can be identified from what machine they're from.   � ��� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .~ ��� i  dg��� I      �������� .0 getcomputeridentifier getComputerIdentifier��  ��  � k     �� ��� r     	��� n     ��� 1    ��
�� 
sicn� l    ������ I    ������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 computername computerName� ��� r   
 ��� I  
 �����
�� .sysoexecTEXT���     TEXT� m   
 �� ��� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  ��  � o      ���� "0 strserialnumber strSerialNumber� ��� r    ��� l   ������ b    ��� b    ��� o    ���� 0 computername computerName� m    �� ���    -  � o    ���� "0 strserialnumber strSerialNumber��  ��  � o      ����  0 identifiername identifierName� ���� L    �� o    ����  0 identifiername identifierName��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   � ���   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .� ��� l     ������  � � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   � ���   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .� ��� i  hk��� I      ������� 0 gettimestamp getTimestamp� ���� o      ���� 0 isfolder isFolder��  ��  � l   ����� k    ��� ��� l     ������  �   Date variables   � ���    D a t e   v a r i a b l e s� ��� r     )��� l     ������ I     ������
�� .misccurdldt    ��� null��  ��  ��  ��  � K    �� ����
�� 
year� o    ���� 0 y  � ����
�� 
mnth� o    ���� 0 m  � ����
�� 
day � o    ���� 0 d  � �����
�� 
time� o   	 
���� 0 t  ��  � ��� r   * 1��� c   * /��� l  * -������ c   * -��� o   * +���� 0 y  � m   + ,��
�� 
long��  ��  � m   - .��
�� 
TEXT� o      ���� 0 ty tY� ��� r   2 9��� c   2 7��� l  2 5������ c   2 5��� o   2 3���� 0 y  � m   3 4��
�� 
long��  ��  � m   5 6��
�� 
TEXT� o      ���� 0 ty tY� ��� r   : A��� c   : ?��� l  : =������ c   : =��� o   : ;���� 0 m  � m   ; <��
�� 
long��  ��  � m   = >��
�� 
TEXT� o      ���� 0 tm tM� ��� r   B I��� c   B G��� l  B E������ c   B E��� o   B C���� 0 d  � m   C D�
� 
long��  ��  � m   E F�~
�~ 
TEXT� o      �}�} 0 td tD� ��� r   J Q��� c   J O��� l  J M��|�{� c   J M��� o   J K�z�z 0 t  � m   K L�y
�y 
long�|  �{  � m   M N�x
�x 
TEXT� o      �w�w 0 tt tT� � � l  R R�v�u�t�v  �u  �t     l  R R�s�s   U O Append the month or day with a 0 if the string length is only 1 character long    � �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g  r   R [	 c   R Y

 l  R W�r�q I  R W�p�o
�p .corecnte****       **** o   R S�n�n 0 tm tM�o  �r  �q   m   W X�m
�m 
nmbr	 o      �l�l 
0 tml tML  r   \ e c   \ c l  \ a�k�j I  \ a�i�h
�i .corecnte****       **** o   \ ]�g�g 0 tm tM�h  �k  �j   m   a b�f
�f 
nmbr o      �e�e 
0 tdl tDL  l  f f�d�c�b�d  �c  �b    Z   f u�a�` l  f i�_�^ =   f i o   f g�]�] 
0 tml tML m   g h�\�\ �_  �^   r   l q  b   l o!"! m   l m## �$$  0" o   m n�[�[ 0 tm tM  o      �Z�Z 0 tm tM�a  �`   %&% l  v v�Y�X�W�Y  �X  �W  & '(' Z   v �)*�V�U) l  v y+�T�S+ =   v y,-, o   v w�R�R 
0 tdl tDL- m   w x�Q�Q �T  �S  * r   | �./. b   | �010 m   | 22 �33  01 o    ��P�P 0 td tD/ o      �O�O 0 td tD�V  �U  ( 454 l  � ��N�M�L�N  �M  �L  5 676 l  � ��K�J�I�K  �J  �I  7 898 l  � ��H:;�H  :   Time variables	   ; �<<     T i m e   v a r i a b l e s 	9 =>= l  � ��G?@�G  ?  	 Get hour   @ �AA    G e t   h o u r> BCB r   � �DED n   � �FGF 1   � ��F
�F 
tstrG l  � �H�E�DH I  � ��C�B�A
�C .misccurdldt    ��� null�B  �A  �E  �D  E o      �@�@ 0 timestr timeStrC IJI r   � �KLK I  � �M�?NM z�>�=
�> .sysooffslong    ��� null
�= misccura�?  N �<OP
�< 
psofO m   � �QQ �RR  :P �;S�:
�; 
psinS o   � ��9�9 0 timestr timeStr�:  L o      �8�8 0 pos  J TUT r   � �VWV c   � �XYX n   � �Z[Z 7  � ��7\]
�7 
cha \ m   � ��6�6 ] l  � �^�5�4^ \   � �_`_ o   � ��3�3 0 pos  ` m   � ��2�2 �5  �4  [ o   � ��1�1 0 timestr timeStrY m   � ��0
�0 
TEXTW o      �/�/ 0 h  U aba r   � �cdc c   � �efe n   � �ghg 7 � ��.ij
�. 
cha i l  � �k�-�,k [   � �lml o   � ��+�+ 0 pos  m m   � ��*�* �-  �,  j  ;   � �h o   � ��)�) 0 timestr timeStrf m   � ��(
�( 
TEXTd o      �'�' 0 timestr timeStrb non l  � ��&�%�$�&  �%  �$  o pqp l  � ��#rs�#  r   Get minute   s �tt    G e t   m i n u t eq uvu r   � �wxw I  � �y�"zy z�!� 
�! .sysooffslong    ��� null
�  misccura�"  z �{|
� 
psof{ m   � �}} �~~  :| ��
� 
psin o   � ��� 0 timestr timeStr�  x o      �� 0 pos  v ��� r   ���� c   ���� n   � ��� 7  � ���
� 
cha � m   � ��� � l  � ����� \   � ���� o   � ��� 0 pos  � m   � ��� �  �  � o   � ��� 0 timestr timeStr� m   �
� 
TEXT� o      �� 0 m  � ��� r  ��� c  ��� n  ��� 7���
� 
cha � l ���� [  ��� o  �� 0 pos  � m  �� �  �  �  ;  � o  �� 0 timestr timeStr� m  �
� 
TEXT� o      �
�
 0 timestr timeStr� ��� l �	���	  �  �  � ��� l ����  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  2��� I 0���� z��
� .sysooffslong    ��� null
� misccura�  � ���
� 
psof� m  "%�� ���   � ��� 
� 
psin� o  ()���� 0 timestr timeStr�   � o      ���� 0 pos  � ��� r  3E��� c  3C��� n  3A��� 74A����
�� 
cha � l :>������ [  :>��� o  ;<���� 0 pos  � m  <=���� ��  ��  �  ;  ?@� o  34���� 0 timestr timeStr� m  AB��
�� 
TEXT� o      ���� 0 s  � ��� l FF��������  ��  ��  � ��� l FF��������  ��  ��  � ��� Z  F������ l FI������ =  FI��� o  FG���� 0 isfolder isFolder� m  GH��
�� boovtrue��  ��  � k  La�� ��� l LL������  �   Create folder timestamp   � ��� 0   C r e a t e   f o l d e r   t i m e s t a m p� ���� r  La��� b  L_��� b  L]��� b  L[��� b  LY��� b  LU��� b  LS��� b  LQ��� m  LO�� ���  b a c k u p _� o  OP���� 0 ty tY� o  QR���� 0 tm tM� o  ST���� 0 td tD� m  UX�� ���  _� o  YZ���� 0 h  � o  [\���� 0 m  � o  ]^���� 0 s  � o      ���� 0 	timestamp  ��  � ��� l dg������ =  dg��� o  de���� 0 isfolder isFolder� m  ef��
�� boovfals��  ��  � ���� k  j{�� ��� l jj������  �   Create timestamp   � ��� "   C r e a t e   t i m e s t a m p� ���� r  j{��� b  jy��� b  jw��� b  ju��� b  js��� b  jo��� b  jm��� o  jk���� 0 ty tY� o  kl���� 0 tm tM� o  mn���� 0 td tD� m  or�� ���  _� o  st���� 0 h  � o  uv���� 0 m  � o  wx���� 0 s  � o      ���� 0 	timestamp  ��  ��  ��  � ��� l ����������  ��  ��  � ���� L  ��   o  ������ 0 	timestamp  ��  �  	(boolean)   � �  ( b o o l e a n )�  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��   	 l     ��������  ��  ��  	 

 l     ��������  ��  ��    l     ����   q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.    � �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .  l     ����   w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.    � �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .  i  lo I      ������ 0 comparesizes compareSizes  o      ���� 
0 source   �� o      ���� 0 destination  ��  ��   l   �  k    �!! "#" r     $%$ m     ��
�� boovtrue% o      ���� 0 fit  # &'& r    
()( 4    ��*
�� 
psxf* o    ���� 0 destination  ) o      ���� 0 destination  ' +,+ l   ��������  ��  ��  , -.- l   ��/0��  / w q Get sizes for Library > Caches, Library > Logs and Library > Mobile Documents then subtract them from folderSize   0 �11 �   G e t   s i z e s   f o r   L i b r a r y   >   C a c h e s ,   L i b r a r y   >   L o g s   a n d   L i b r a r y   >   M o b i l e   D o c u m e n t s   t h e n   s u b t r a c t   t h e m   f r o m   f o l d e r S i z e. 232 l   ��������  ��  ��  3 454 r    676 b    898 b    :;: b    <=< o    ���� "0 destinationdisk destinationDisk= m    >> �??  B a c k u p s /; o    ���� 0 machinefolder machineFolder9 m    @@ �AA  / L a t e s t7 o      ���� 0 
templatest 
tempLatest5 BCB r    DED m    ����  E o      ���� $0 latestfoldersize latestFolderSizeC FGF l   ��������  ��  ��  G HIH r    &JKJ b    $LML n   "NON 1     "��
�� 
psxpO l    P����P I    ��QR
�� .earsffdralis        afdrQ m    ��
�� afdrdlibR ��S��
�� 
fromS m    ��
�� fldmfldu��  ��  ��  M m   " #TT �UU  C a c h e sK o      ���� 0 c  I VWV r   ' 4XYX b   ' 2Z[Z n  ' 0\]\ 1   . 0��
�� 
psxp] l  ' .^����^ I  ' .��_`
�� .earsffdralis        afdr_ m   ' (��
�� afdrdlib` ��a��
�� 
froma m   ) *��
�� fldmfldu��  ��  ��  [ m   0 1bb �cc  L o g sY o      ���� 0 l  W ded r   5 Bfgf b   5 @hih n  5 >jkj 1   < >��
�� 
psxpk l  5 <l����l I  5 <��mn
�� .earsffdralis        afdrm m   5 6��
�� afdrdlibn ��o��
�� 
fromo m   7 8��
�� fldmfldu��  ��  ��  i m   > ?pp �qq   M o b i l e   D o c u m e n t sg o      ���� 0 md  e rsr r   C Ftut m   C D����  u o      ���� 0 	cachesize 	cacheSizes vwv r   G Jxyx m   G H����  y o      ���� 0 logssize logsSizew z{z r   K N|}| m   K L����  } o      ���� 0 mdsize mdSize{ ~~ l  O O��������  ��  ��   ��� r   O R��� m   O P�� ?�      � o      ���� 
0 buffer  � ��� l  S S��~�}�  �~  �}  � ��� r   S V��� m   S T�|�|  � o      �{�{ (0 adjustedfoldersize adjustedFolderSize� ��� l  W W�z�y�x�z  �y  �x  � ��� O   W���� k   [��� ��� r   [ h��� I  [ f�w��v
�w .sysoexecTEXT���     TEXT� b   [ b��� b   [ ^��� m   [ \�� ���  d u   - m s   '� o   \ ]�u�u 
0 source  � m   ^ a�� ���  '   |   c u t   - f   1�v  � o      �t�t 0 
foldersize 
folderSize� ��� r   i ���� ^   i ���� l  i }��s�r� I  i }���q� z�p�o
�p .sysorondlong        doub
�o misccura� ]   o x��� l  o t��n�m� ^   o t��� o   o p�l�l 0 
foldersize 
folderSize� m   p s�k�k �n  �m  � m   t w�j�j d�q  �s  �r  � m   } ��i�i d� o      �h�h 0 
foldersize 
folderSize� ��� l  � ��g�f�e�g  �f  �e  � ��� r   � ���� ^   � ���� ^   � ���� ^   � ���� l  � ���d�c� l  � ���b�a� n   � ���� 1   � ��`
�` 
frsp� 4   � ��_�
�_ 
cdis� o   � ��^�^ 0 destination  �b  �a  �d  �c  � m   � ��]�] � m   � ��\�\ � m   � ��[�[ � o      �Z�Z 0 	freespace 	freeSpace� ��� r   � ���� ^   � ���� l  � ���Y�X� I  � ����W� z�V�U
�V .sysorondlong        doub
�U misccura� l  � ���T�S� ]   � ���� o   � ��R�R 0 	freespace 	freeSpace� m   � ��Q�Q d�T  �S  �W  �Y  �X  � m   � ��P�P d� o      �O�O 0 	freespace 	freeSpace� ��� l  � ��N�M�L�N  �M  �L  � ��� r   � ���� I  � ��K��J
�K .sysoexecTEXT���     TEXT� b   � ���� b   � ���� m   � ��� ���  d u   - m s   '� o   � ��I�I 0 c  � m   � ��� ���  '   |   c u t   - f   1�J  � o      �H�H 0 	cachesize 	cacheSize� ��� r   � ���� ^   � ���� l  � ���G�F� I  � ����E� z�D�C
�D .sysorondlong        doub
�C misccura� ]   � ���� l  � ���B�A� ^   � ���� o   � ��@�@ 0 	cachesize 	cacheSize� m   � ��?�? �B  �A  � m   � ��>�> d�E  �G  �F  � m   � ��=�= d� o      �<�< 0 	cachesize 	cacheSize� ��� l  � ��;�:�9�;  �:  �9  � ��� r   � ���� I  � ��8��7
�8 .sysoexecTEXT���     TEXT� b   � ���� b   � ���� m   � ��� ���  d u   - m s   '� o   � ��6�6 0 l  � m   � ��� ���  '   |   c u t   - f   1�7  � o      �5�5 0 logssize logsSize� ��� r   �	��� ^   ���� l  ���4�3� I  ����2� z�1�0
�1 .sysorondlong        doub
�0 misccura� ]   � �� � l  � ��/�. ^   � � o   � ��-�- 0 logssize logsSize m   � ��,�, �/  �.    m   � ��+�+ d�2  �4  �3  � m  �*�* d� o      �)�) 0 logssize logsSize�  l 

�(�'�&�(  �'  �&    r  
	 I 
�%
�$
�% .sysoexecTEXT���     TEXT
 b  
 b  
 m  
 �  d u   - m s   ' o  �#�# 0 md   m   �  '   |   c u t   - f   1�$  	 o      �"�" 0 mdsize mdSize  r  4 ^  2 l .�!�  I .� z��
� .sysorondlong        doub
� misccura ]   ) l  %�� ^   %  o   !�� 0 mdsize mdSize  m  !$�� �  �   m  %(�� d�  �!  �    m  .1�� d o      �� 0 mdsize mdSize !"! l 55����  �  �  " #$# r  5>%&% l 5<'��' \  5<()( \  5:*+* \  58,-, o  56�� 0 
foldersize 
folderSize- o  67�� 0 	cachesize 	cacheSize+ o  89�� 0 logssize logsSize) o  :;�� 0 mdsize mdSize�  �  & o      �� (0 adjustedfoldersize adjustedFolderSize$ ./. l ??��
�	�  �
  �	  / 010 I ?H�2�
� .ascrcmnt****      � ****2 l ?D3��3 b  ?D454 b  ?B676 o  ?@�� 0 
foldersize 
folderSize7 o  @A�� (0 adjustedfoldersize adjustedFolderSize5 o  BC�� 0 	freespace 	freeSpace�  �  �  1 898 l II�� ���  �   ��  9 :��: Z  I�;<����; l IP=����= =  IP>?> o  IN���� 0 initialbackup initialBackup? m  NO��
�� boovfals��  ��  < k  S@@ ABA r  SbCDC I S`��E��
�� .sysoexecTEXT���     TEXTE b  S\FGF b  SXHIH m  SVJJ �KK  d u   - m s   'I o  VW���� 0 
templatest 
tempLatestG m  X[LL �MM  '   |   c u t   - f   1��  D o      ���� $0 latestfoldersize latestFolderSizeB NON r  c}PQP ^  c{RSR l cwT����T I cwUV��U z����
�� .sysorondlong        doub
�� misccuraV ]  irWXW l inY����Y ^  inZ[Z o  ij���� $0 latestfoldersize latestFolderSize[ m  jm���� ��  ��  X m  nq���� d��  ��  ��  S m  wz���� dQ o      ���� $0 latestfoldersize latestFolderSizeO \��\ l ~~��������  ��  ��  ��  ��  ��  ��  � m   W X]]�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ^_^ l ����������  ��  ��  _ `a` Z  ��bcd��b l ��e����e =  ��fgf o  ������ 0 initialbackup initialBackupg m  ����
�� boovfals��  ��  c k  ��hh iji r  ��klk \  ��mnm o  ������ (0 adjustedfoldersize adjustedFolderSizen o  ������ $0 latestfoldersize latestFolderSizel o      ���� 0 temp  j opo l ����������  ��  ��  p qrq Z  ��st����s l ��u����u A  ��vwv o  ������ 0 temp  w m  ������  ��  ��  t r  ��xyx \  ��z{z l ��|����| o  ������ 0 temp  ��  ��  { l ��}����} ]  ��~~ l �������� o  ������ 0 temp  ��  ��   m  ������ ��  ��  y o      ���� 0 temp  ��  ��  r ��� l ����������  ��  ��  � ���� Z  ��������� l �������� ?  ����� o  ������ 0 temp  � l �������� \  ����� o  ������ 0 	freespace 	freeSpace� o  ������ 
0 buffer  ��  ��  ��  ��  � r  ����� m  ����
�� boovfals� o      ���� 0 fit  ��  ��  ��  d ��� l �������� =  ����� o  ������ 0 initialbackup initialBackup� m  ����
�� boovtrue��  ��  � ���� Z  ��������� l �������� ?  ����� o  ������ (0 adjustedfoldersize adjustedFolderSize� l �������� \  ����� o  ������ 0 	freespace 	freeSpace� o  ������ 
0 buffer  ��  ��  ��  ��  � r  ����� m  ����
�� boovfals� o      ���� 0 fit  ��  ��  ��  ��  a ��� l ����������  ��  ��  � ���� L  ���� o  ������ 0 fit  ��    (string, string)     ���   ( s t r i n g ,   s t r i n g ) ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i  ps��� I      ������� 0 	stopwatch  � ���� o      ���� 0 mode  ��  ��  � l    5���� k     5�� ��� q      �� ������ 0 x  ��  � ��� Z     3������ l    ������ =     ��� o     ���� 0 mode  � m    �� ��� 
 s t a r t��  ��  � k    �� ��� r    ��� I    ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovfals��  ��  � o      ���� 0 x  � ���� I   �����
�� .ascrcmnt****      � ****� l   ���~� b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    �}�} 0 x  �  �~  ��  ��  � ��� l   ��|�{� =    ��� o    �z�z 0 mode  � m    �� ���  f i n i s h�|  �{  � ��y� k    /�� ��� r    '��� I    %�x��w�x 0 gettimestamp getTimestamp� ��v� m     !�u
�u boovfals�v  �w  � o      �t�t 0 x  � ��s� I  ( /�r��q
�r .ascrcmnt****      � ****� l  ( +��p�o� b   ( +��� m   ( )�� ��� " b a c k u p   f i n i s h e d :  � o   ) *�n�n 0 x  �p  �o  �q  �s  �y  ��  � ��m� l  4 4�l�k�j�l  �k  �j  �m  �  (string)   � ���  ( s t r i n g )� ��� l     �i�h�g�i  �h  �g  � ��� l     �f�e�d�f  �e  �d  � ��� l     �c�b�a�c  �b  �a  � ��� l     �`�_�^�`  �_  �^  � ��� l     �]�\�[�]  �\  �[  � ��� l     �Z���Z  � M G Utility function to get the duration of the backup process in minutes.   � ��� �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .� ��� i  tw   I      �Y�X�W�Y 0 getduration getDuration�X  �W   k       r      ^      l    	�V�U	 \     

 o     �T�T 0 endtime endTime o    �S�S 0 	starttime 	startTime�V  �U   m    �R�R < o      �Q�Q 0 duration    r     ^     l   �P�O I   �N z�M�L
�M .sysorondlong        doub
�L misccura l   �K�J ]     o    �I�I 0 duration   m    �H�H d�K  �J  �N  �P  �O   m    �G�G d o      �F�F 0 duration   �E L     o    �D�D 0 duration  �E  �  l     �C�B�A�C  �B  �A    l     �@�?�>�@  �?  �>    l     �=�<�;�=  �<  �;    !  l     �:�9�8�:  �9  �8  ! "#" l     �7�6�5�7  �6  �5  # $%$ l     �4&'�4  & ; 5 Utility function bring WBTFU to the front with focus   ' �(( j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s% )*) i  x{+,+ I      �3�2�1�3 0 setfocus setFocus�2  �1  , O     
-.- I   	�0�/�.
�0 .miscactvnull��� ��� null�/  �.  . m     //                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  * 010 l     �-�,�+�-  �,  �+  1 232 l     �*45�*  4 � �-----------------------------------------------------------------------------------------------------------------------------------------------   5 �66 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -3 787 l     �)9:�)  9 � �-----------------------------------------------------------------------------------------------------------------------------------------------   : �;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -8 <=< l     �(�'�&�(  �'  �&  = >?> l     �%�$�#�%  �$  �#  ? @A@ l     �"�!� �"  �!  �   A BCB l     ����  �  �  C DED l     ����  �  �  E FGF l     �HI�  H � �-----------------------------------------------------------------------------------------------------------------------------------------------   I �JJ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -G KLK l     �MN�  M � �-----------------------------------------------------------------------------------------------------------------------------------------------   N �OO - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -L PQP l     �RS�  R � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   S �TT - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -Q UVU l     ����  �  �  V WXW i  |YZY I      �[�� $0 menuneedsupdate_ menuNeedsUpdate_[ \�\ l     ]��] m      �
� 
cmnu�  �  �  �  Z k     ^^ _`_ l     �ab�  a J D NSMenu's delegates method, when the menu is clicked this is called.   b �cc �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .` ded l     �fg�  f j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   g �hh �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .e iji l     �kl�  k < 6 This means the menu items can be changed dynamically.   l �mm l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .j n�
n n    opo I    �	���	 0 	makemenus 	makeMenus�  �  p  f     �
  X qrq l     ����  �  �  r sts l     ����  �  �  t uvu l     � �����   ��  ��  v wxw l     ��������  ��  ��  x yzy l     ��������  ��  ��  z {|{ i  ��}~} I      �������� 0 	makemenus 	makeMenus��  ��  ~ k    > ��� l    	���� n    	��� I    	��������  0 removeallitems removeAllItems��  ��  � o     ���� 0 newmenu newMenu� !  remove existing menu items   � ��� 6   r e m o v e   e x i s t i n g   m e n u   i t e m s� ��� l  
 
��������  ��  ��  � ���� Y   
>�������� k   9�� ��� r    '��� n    %��� 4   " %���
�� 
cobj� o   # $���� 0 i  � o    "���� 0 thelist theList� o      ���� 0 	this_item  � ��� l  ( (��������  ��  ��  � ��� Z   ( ������ l  ( -������ =   ( -��� l  ( +������ c   ( +��� o   ( )���� 0 	this_item  � m   ) *��
�� 
TEXT��  ��  � m   + ,�� ���  B a c k u p   n o w��  ��  � r   0 @��� l  0 >������ n  0 >��� I   7 >������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8���� 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ���� m   9 :�� ���  ��  ��  � n  0 7��� I   3 7�������� 	0 alloc  ��  ��  � n  0 3��� o   1 3���� 0 
nsmenuitem 
NSMenuItem� m   0 1��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  C H������ =   C H��� l  C F������ c   C F��� o   C D���� 0 	this_item  � m   D E��
�� 
TEXT��  ��  � m   F G�� ��� " B a c k u p   n o w   &   q u i t��  ��  � ��� r   K [��� l  K Y������ n  K Y��� I   R Y������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S���� 0 	this_item  � ��� m   S T�� ��� * b a c k u p A n d Q u i t H a n d l e r :� ���� m   T U�� ���  ��  ��  � n  K R��� I   N R�������� 	0 alloc  ��  ��  � n  K N��� o   L N���� 0 
nsmenuitem 
NSMenuItem� m   K L��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  ^ c������ =   ^ c��� l  ^ a������ c   ^ a��� o   ^ _���� 0 	this_item  � m   _ `��
�� 
TEXT��  ��  � m   a b�� ��� " T u r n   o n   t h e   b a n t s��  ��  � ��� r   f x��� l  f v������ n  f v��� I   m v������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n���� 0 	this_item  � ��� m   n o�� ���  n s f w O n H a n d l e r :� ���� m   o r�� ���  ��  ��  � n  f m��� I   i m�������� 	0 alloc  ��  ��  � n  f i��� o   g i���� 0 
nsmenuitem 
NSMenuItem� m   f g��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  { ������� =   { ���� l  { ~������ c   { ~��� o   { |���� 0 	this_item  � m   | }��
�� 
TEXT��  ��  � m   ~ ��� ��� $ T u r n   o f f   t h e   b a n t s��  ��  �    r   � � l  � ����� n  � � I   � ������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_ 	 o   � ����� 0 	this_item  	 

 m   � � �  n s f w O f f H a n d l e r : �� m   � � �  ��  ��   n  � � I   � ��������� 	0 alloc  ��  ��   n  � � o   � ����� 0 
nsmenuitem 
NSMenuItem m   � ���
�� misccura��  ��   o      ���� 0 thismenuitem thisMenuItem  l  � ����� =   � � l  � ����� c   � � o   � ����� 0 	this_item   m   � ���
�� 
TEXT��  ��   m   � � � * T u r n   o n   n o t i f i c a t i o n s��  ��     r   � �!"! l  � �#����# n  � �$%$ I   � ���&���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_& '(' o   � ����� 0 	this_item  ( )*) m   � �++ �,, , n o t i f i c a t i o n O n H a n d l e r :* -��- m   � �.. �//  ��  ��  % n  � �010 I   � ��������� 	0 alloc  ��  ��  1 n  � �232 o   � ����� 0 
nsmenuitem 
NSMenuItem3 m   � ���
�� misccura��  ��  " o      ���� 0 thismenuitem thisMenuItem  454 l  � �6����6 =   � �787 l  � �9����9 c   � �:;: o   � ����� 0 	this_item  ; m   � ���
�� 
TEXT��  ��  8 m   � �<< �== , T u r n   o f f   n o t i f i c a t i o n s��  ��  5 >?> r   � �@A@ l  � �B����B n  � �CDC I   � ���E��� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_E FGF o   � ��~�~ 0 	this_item  G HIH m   � �JJ �KK . n o t i f i c a t i o n O f f H a n d l e r :I L�}L m   � �MM �NN  �}  �  D n  � �OPO I   � ��|�{�z�| 	0 alloc  �{  �z  P n  � �QRQ o   � ��y�y 0 
nsmenuitem 
NSMenuItemR m   � ��x
�x misccura��  ��  A o      �w�w 0 thismenuitem thisMenuItem? STS l  � �U�v�uU =   � �VWV l  � �X�t�sX c   � �YZY o   � ��r�r 0 	this_item  Z m   � ��q
�q 
TEXT�t  �s  W m   � �[[ �\\  Q u i t�v  �u  T ]�p] r   � �^_^ l  � �`�o�n` n  � �aba I   � ��mc�l�m J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_c ded o   � ��k�k 0 	this_item  e fgf m   � �hh �ii  q u i t H a n d l e r :g j�jj m   � �kk �ll  �j  �l  b n  � �mnm I   � ��i�h�g�i 	0 alloc  �h  �g  n n  � �opo o   � ��f�f 0 
nsmenuitem 
NSMenuItemp m   � ��e
�e misccura�o  �n  _ o      �d�d 0 thismenuitem thisMenuItem�p  ��  � qrq l �c�b�a�c  �b  �a  r sts l u�`�_u n vwv I  �^x�]�^ 0 additem_ addItem_x y�\y o  �[�[ 0 thismenuitem thisMenuItem�\  �]  w o  �Z�Z 0 newmenu newMenu�`  �_  t z{z l �Y�X�W�Y  �X  �W  { |}| l ~�~ l ��V�U� n ��� I  �T��S�T 0 
settarget_ 
setTarget_� ��R�  f  �R  �S  � o  �Q�Q 0 thismenuitem thisMenuItem�V  �U   * $ required for enabling the menu item   � ��� H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m} ��� l �P�O�N�P  �O  �N  � ��M� Z  9���L�K� G  "��� l ��J�I� =  ��� o  �H�H 0 i  � m  �G�G �J  �I  � l ��F�E� =  ��� o  �D�D 0 i  � m  �C�C �F  �E  � l %5���� l %5��B�A� n %5��� I  *5�@��?�@ 0 additem_ addItem_� ��>� l *1��=�<� n *1��� o  -1�;�; 0 separatoritem separatorItem� n *-��� o  +-�:�: 0 
nsmenuitem 
NSMenuItem� m  *+�9
�9 misccura�=  �<  �>  �?  � o  %*�8�8 0 newmenu newMenu�B  �A  �   add a seperator   � ���     a d d   a   s e p e r a t o r�L  �K  �M  �� 0 i  � m    �7�7 � n    ��� m    �6
�6 
nmbr� n   ��� 2   �5
�5 
cobj� o    �4�4 0 thelist theList��  ��  | ��� l     �3�2�1�3  �2  �1  � ��� l     �0�/�.�0  �/  �.  � ��� l     �-�,�+�-  �,  �+  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� i  ����� I      �$��#�$  0 backuphandler_ backupHandler_� ��"� o      �!�! 
0 sender  �"  �#  � Z     ����� � l    ���� =     ��� o     �� 0 isbackingup isBackingUp� m    �
� boovfals�  �  � k   
 U�� ��� r   
 ��� m   
 �
� boovtrue� o      �� 0 manualbackup manualBackup� ��� Z    U����� l   ���� =    ��� o    �� 0 notifications  � m    �
� boovtrue�  �  � Z    Q����� l   #���� =    #��� o    !�� 0 nsfw  � m   ! "�
� boovtrue�  �  � k   & 3�� ��� I  & -���
� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� ���
� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�  � ��
� I  . 3�	��
�	 .sysodelanull��� ��� nmbr� m   . /�� �  �
  � ��� l  6 =���� =   6 =��� o   6 ;�� 0 nsfw  � m   ; <�
� boovfals�  �  � ��� k   @ M�� ��� I  @ G���
� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� � ���
�  
appr� m   B C�� ��� 
 W B T F U��  � ���� I  H M�����
�� .sysodelanull��� ��� nmbr� m   H I���� ��  ��  �  �  �  �  �  � ��� l  X _������ =   X _��� o   X ]���� 0 isbackingup isBackingUp� m   ] ^��
�� boovtrue��  ��  � ���� k   b ��� ��� r   b k��� n   b i��� 3   g i��
�� 
cobj� o   b g���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   l �������� l  l s������ =   l s��� o   l q���� 0 notifications  � m   q r��
�� boovtrue��  ��  � Z   v ��� ��� l  v }���� =   v } o   v {���� 0 nsfw   m   { |��
�� boovtrue��  ��  � I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � � � P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��    	
	 l  � ����� =   � � o   � ����� 0 nsfw   m   � ���
�� boovfals��  ��  
 �� I  � ���
�� .sysonotfnull��� ��� TEXT m   � � � 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p ����
�� 
appr m   � � � 
 W B T F U��  ��  ��  ��  ��  ��  ��  �   �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    !  i  ��"#" I      ��$���� .0 backupandquithandler_ backupAndQuitHandler_$ %��% o      ���� 
0 sender  ��  ��  # Z     �&'(��& l    )����) =     *+* o     ���� 0 isbackingup isBackingUp+ m    ��
�� boovfals��  ��  ' k   
 �,, -.- r   
 /0/ l  
 1����1 n   
 232 1    ��
�� 
bhit3 l  
 4����4 I  
 ��56
�� .sysodlogaskr        TEXT5 m   
 77 �88 T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?6 ��9:
�� 
appr9 m    ;; �<< , W o a h ,   B a c k   T h e   F u c k   U p: ��=>
�� 
disp= o    ���� 0 appicon appIcon> ��?@
�� 
btns? J    AA BCB m    DD �EE  Y e sC F��F m    GG �HH  N o��  @ ��I��
�� 
dfltI m    ���� ��  ��  ��  ��  ��  0 o      ���� 	0 reply  . JKJ l   ��������  ��  ��  K L��L Z    �MNO��M l   "P����P =    "QRQ o     ���� 	0 reply  R m     !SS �TT  Y e s��  ��  N k   % |UU VWV r   % ,XYX m   % &��
�� boovtrueY o      ����  0 backupthenquit backupThenQuitW Z[Z r   - 4\]\ m   - .��
�� boovtrue] o      ���� 0 manualbackup manualBackup[ ^_^ l  5 5��������  ��  ��  _ `��` Z   5 |ab����a l  5 <c����c =   5 <ded o   5 :���� 0 notifications  e m   : ;��
�� boovtrue��  ��  b Z   ? xfgh��f l  ? Fi����i =   ? Fjkj o   ? D���� 0 nsfw  k m   D E��
�� boovtrue��  ��  g k   I Vll mnm I  I P��op
�� .sysonotfnull��� ��� TEXTo m   I Jqq �rr T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u pp ��s��
�� 
apprs m   K Ltt �uu 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  n v��v I  Q V��w��
�� .sysodelanull��� ��� nmbrw m   Q R���� ��  ��  h xyx l  Y `z����z =   Y `{|{ o   Y ^�� 0 nsfw  | m   ^ _�
� boovfals��  ��  y }�} k   c t~~ � I  c n���
� .sysonotfnull��� ��� TEXT� m   c f�� ���   P r e p a r i n g   b a c k u p� ���
� 
appr� m   g j�� ��� 
 W B T F U�  � ��� I  o t���
� .sysodelanull��� ��� nmbr� m   o p�� �  �  �  ��  ��  ��  ��  O ��� l   ����� =    ���� o    ��� 	0 reply  � m   � ��� ���  N o�  �  � ��� r   � ���� m   � ��
� boovfals� o      ��  0 backupthenquit backupThenQuit�  ��  ��  ( ��� l  � ����~� =   � ���� o   � ��}�} 0 isbackingup isBackingUp� m   � ��|
�| boovtrue�  �~  � ��{� k   � ��� ��� r   � ���� n   � ���� 3   � ��z
�z 
cobj� o   � ��y�y 40 messagesalreadybackingup messagesAlreadyBackingUp� o      �x�x 0 msg  � ��w� Z   � ����v�u� l  � ���t�s� =   � ���� o   � ��r�r 0 notifications  � m   � ��q
�q boovtrue�t  �s  � Z   � �����p� l  � ���o�n� =   � ���� o   � ��m�m 0 nsfw  � m   � ��l
�l boovtrue�o  �n  � I  � ��k��
�k .sysonotfnull��� ��� TEXT� o   � ��j�j 0 msg  � �i��h
�i 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�h  � ��� l  � ���g�f� =   � ���� o   � ��e�e 0 nsfw  � m   � ��d
�d boovfals�g  �f  � ��c� I  � ��b��
�b .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �a��`
�a 
appr� m   � ��� ��� 
 W B T F U�`  �c  �p  �v  �u  �w  �{  ��  ! ��� l     �_�^�]�_  �^  �]  � ��� l     �\�[�Z�\  �[  �Z  � ��� l     �Y�X�W�Y  �X  �W  � ��� l     �V�U�T�V  �U  �T  � ��� l     �S�R�Q�S  �R  �Q  � ��� i  ����� I      �P��O�P  0 nsfwonhandler_ nsfwOnHandler_� ��N� o      �M�M 
0 sender  �N  �O  � k     =�� ��� r     ��� m     �L
�L boovtrue� o      �K�K 0 nsfw  � ��J� Z    =����I� l   ��H�G� =    ��� o    �F�F 0 notifications  � m    �E
�E boovtrue�H  �G  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��D� m    �� ���  Q u i t�D  � o      �C�C 0 thelist theList� ��� l  " )��B�A� =   " )��� o   " '�@�@ 0 notifications  � m   ' (�?
�? boovfals�B  �A  � ��>� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .   � " B a c k u p   n o w   &   q u i t�  m   . / � $ T u r n   o f f   t h e   b a n t s  m   / 0 �		 * T u r n   o n   n o t i f i c a t i o n s 
�=
 m   0 1 �  Q u i t�=  � o      �<�< 0 thelist theList�>  �I  �J  �  l     �;�:�9�;  �:  �9    i  �� I      �8�7�8 "0 nsfwoffhandler_ nsfwOffHandler_ �6 o      �5�5 
0 sender  �6  �7   k     =  r      m     �4
�4 boovfals o      �3�3 0 nsfw   �2 Z    =�1 l   �0�/ =      o    �.�. 0 notifications    m    �-
�- boovtrue�0  �/   r    !"! J    ## $%$ m    && �''  B a c k u p   n o w% ()( m    ** �++ " B a c k u p   n o w   &   q u i t) ,-, m    .. �// " T u r n   o n   t h e   b a n t s- 010 m    22 �33 , T u r n   o f f   n o t i f i c a t i o n s1 4�,4 m    55 �66  Q u i t�,  " o      �+�+ 0 thelist theList 787 l  " )9�*�)9 =   " ):;: o   " '�(�( 0 notifications  ; m   ' (�'
�' boovfals�*  �)  8 <�&< r   , 9=>= J   , 3?? @A@ m   , -BB �CC  B a c k u p   n o wA DED m   - .FF �GG " B a c k u p   n o w   &   q u i tE HIH m   . /JJ �KK " T u r n   o n   t h e   b a n t sI LML m   / 0NN �OO * T u r n   o n   n o t i f i c a t i o n sM P�%P m   0 1QQ �RR  Q u i t�%  > o      �$�$ 0 thelist theList�&  �1  �2   STS l     �#�"�!�#  �"  �!  T UVU l     � ���   �  �  V WXW l     ����  �  �  X YZY l     ����  �  �  Z [\[ l     ����  �  �  \ ]^] i  ��_`_ I      �a�� 00 notificationonhandler_ notificationOnHandler_a b�b o      �� 
0 sender  �  �  ` k     =cc ded r     fgf m     �
� boovtrueg o      �� 0 notifications  e h�h Z    =ijk�i l   l��l =    mnm o    �
�
 0 nsfw  n m    �	
�	 boovtrue�  �  j r    opo J    qq rsr m    tt �uu  B a c k u p   n o ws vwv m    xx �yy " B a c k u p   n o w   &   q u i tw z{z m    || �}} $ T u r n   o f f   t h e   b a n t s{ ~~ m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s ��� m    �� ���  Q u i t�  p o      �� 0 thelist theListk ��� l  " )���� =   " )��� o   " '�� 0 nsfw  � m   ' (�
� boovfals�  �  � ��� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��� m   0 1�� ���  Q u i t�  � o      � �  0 thelist theList�  �  �  ^ ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 20 notificationoffhandler_ notificationOffHandler_� ���� o      ���� 
0 sender  ��  ��  � k     =�� ��� r     ��� m     ��
�� boovfals� o      ���� 0 notifications  � ���� Z    =������ l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m    �� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  " )������ =   " )��� o   " '���� 0 nsfw  � m   ' (��
�� boovfals��  ��  � ���� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   0 1�� ���  Q u i t��  � o      ���� 0 thelist theList��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 0 quithandler_ quitHandler_� ���� o      ���� 
0 sender  ��  ��  � Z     ������� l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovtrue��  ��  � k   
 :�� ��� n  
    I    �������� 0 setfocus setFocus��  ��    f   
 �  r    $ l   "���� n    " 1     "��
�� 
bhit l    	����	 I    ��

�� .sysodlogaskr        TEXT
 m     � � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ? ��
�� 
appr m     � , W o a h ,   B a c k   T h e   F u c k   U p ��
�� 
disp o    ���� 0 appicon appIcon ��
�� 
btns J      m     �  C a n c e l   &   Q u i t �� m     �  R e s u m e��   ����
�� 
dflt m    �� ��  ��  ��  ��  ��   o      �� 	0 reply   � Z   % : !�"  l  % (#��# =   % ($%$ o   % &�� 	0 reply  % m   & '&& �''  C a n c e l   &   Q u i t�  �  ! I  + 0�(�
� .aevtquitnull��� ��� null( m   + ,))                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  �  " I  3 :�*�
� .ascrcmnt****      � ***** l  3 6+��+ m   3 6,, �--  W B T F U   r e s u m e d�  �  �  �  � ./. l  = D0��0 =   = D121 o   = B�� 0 isbackingup isBackingUp2 m   B C�
� boovfals�  �  / 3�3 k   G �44 565 n  G L787 I   H L���� 0 setfocus setFocus�  �  8  f   G H6 9:9 r   M i;<; l  M g=��= n   M g>?> 1   e g�
� 
bhit? l  M e@��@ I  M e�AB
� .sysodlogaskr        TEXTA m   M PCC �DD < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?B �EF
� 
apprE m   Q TGG �HH , W o a h ,   B a c k   T h e   F u c k   U pF �IJ
� 
dispI o   U V�� 0 appicon appIconJ �KL
� 
btnsK J   W _MM NON m   W ZPP �QQ  Q u i tO R�R m   Z ]SS �TT  R e s u m e�  L �U�
� 
dfltU m   ` a�� �  �  �  �  �  < o      �� 	0 reply  : V�V Z   j �WX�YW l  j oZ��Z =   j o[\[ o   j k�� 	0 reply  \ m   k n]] �^^  Q u i t�  �  X I  r w�_�
� .aevtquitnull��� ��� null_ m   r s``                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  �  Y I  z ��a�
� .ascrcmnt****      � ****a l  z }b��b m   z }cc �dd  W B T F U   r e s u m e d�  �  �  �  �  ��  � efe l     ����  �  �  f ghg l     ����  �  �  h iji l     ����  �  �  j klk l     ����  �  �  l mnm l     ����  �  �  n opo i  ��qrq I      ���� (0 animatemenubaricon animateMenuBarIcon�  �  r k     .ss tut r     vwv 4     �~x
�~ 
alisx l   y�}�|y b    z{z l   	|�{�z| I   	�y}~
�y .earsffdralis        afdr}  f    ~ �x�w
�x 
rtyp m    �v
�v 
ctxt�w  �{  �z  { m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g�}  �|  w o      �u�u 0 	imagepath 	imagePathu ��� r    ��� n    ��� 1    �t
�t 
psxp� o    �s�s 0 	imagepath 	imagePath� o      �r�r 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�q��p�q 20 initwithcontentsoffile_ initWithContentsOfFile_� ��o� o    �n�n 0 	imagepath 	imagePath�o  �p  � n   ��� I    �m�l�k�m 	0 alloc  �l  �k  � n   ��� o    �j�j 0 nsimage NSImage� m    �i
�i misccura� o      �h�h 	0 image  � ��g� n  $ .��� I   ) .�f��e�f 0 	setimage_ 	setImage_� ��d� o   ) *�c�c 	0 image  �d  �e  � o   $ )�b�b 0 
statusitem 
StatusItem�g  p ��� l     �a�`�_�a  �`  �_  � ��� l     �^�]�\�^  �]  �\  � ��� l     �[�Z�Y�[  �Z  �Y  � ��� l     �X�W�V�X  �W  �V  � ��� l     �U�T�S�U  �T  �S  � ��� i  ����� I      �R�Q�P�R $0 resetmenubaricon resetMenuBarIcon�Q  �P  � k     .�� ��� r     ��� 4     �O�
�O 
alis� l   ��N�M� b    ��� l   	��L�K� I   	�J��
�J .earsffdralis        afdr�  f    � �I��H
�I 
rtyp� m    �G
�G 
ctxt�H  �L  �K  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�N  �M  � o      �F�F 0 	imagepath 	imagePath� ��� r    ��� n    ��� 1    �E
�E 
psxp� o    �D�D 0 	imagepath 	imagePath� o      �C�C 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�B��A�B 20 initwithcontentsoffile_ initWithContentsOfFile_� ��@� o    �?�? 0 	imagepath 	imagePath�@  �A  � n   ��� I    �>�=�<�> 	0 alloc  �=  �<  � n   ��� o    �;�; 0 nsimage NSImage� m    �:
�: misccura� o      �9�9 	0 image  � ��8� n  $ .��� I   ) .�7��6�7 0 	setimage_ 	setImage_� ��5� o   ) *�4�4 	0 image  �5  �6  � o   $ )�3�3 0 
statusitem 
StatusItem�8  � ��� l     �2�1�0�2  �1  �0  � ��� l     �/�.�-�/  �.  �-  � ��� l     �,�+�*�,  �+  �*  � ��� l     �)�(�'�)  �(  �'  � ��� l     �&�%�$�&  �%  �$  � ��� l     �#���#  �   create an NSStatusBar   � ��� ,   c r e a t e   a n   N S S t a t u s B a r� ��� i  ����� I      �"�!� �" 0 makestatusbar makeStatusBar�!  �   � k     r�� ��� l     ����  �  log ("Make status bar")   � ��� . l o g   ( " M a k e   s t a t u s   b a r " )� ��� r     ��� n    ��� o    �� "0 systemstatusbar systemStatusBar� n    ��� o    �� 0 nsstatusbar NSStatusBar� m     �
� misccura� o      �� 0 bar  � ��� l   ����  �  �  � ��� r    ��� n   ��� I   	 ���� .0 statusitemwithlength_ statusItemWithLength_� ��� m   	 
�� ��      �  �  � o    	�� 0 bar  � o      �� 0 
statusitem 
StatusItem� ��� l   ����  �  �  � ��� r    #��� 4    !��
� 
alis� l     ��  b      l   �� I   �

�
 .earsffdralis        afdr  f     �	�
�	 
rtyp m    �
� 
ctxt�  �  �   m     � J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�  �  � o      �� 0 	imagepath 	imagePath� 	
	 r   $ ) n   $ ' 1   % '�
� 
psxp o   $ %�� 0 	imagepath 	imagePath o      �� 0 	imagepath 	imagePath
  r   * 8 n  * 6 I   1 6��� 20 initwithcontentsoffile_ initWithContentsOfFile_ �  o   1 2���� 0 	imagepath 	imagePath�   �   n  * 1 I   - 1�������� 	0 alloc  ��  ��   n  * - o   + -���� 0 nsimage NSImage m   * +��
�� misccura o      ���� 	0 image    l  9 9��������  ��  ��    l  9 9�� ��   � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"     �!! s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g " "#" l  9 9��������  ��  ��  # $%$ n  9 C&'& I   > C��(���� 0 	setimage_ 	setImage_( )��) o   > ?���� 	0 image  ��  ��  ' o   9 >���� 0 
statusitem 
StatusItem% *+* l  D D��������  ��  ��  + ,-, l  D D��./��  . , & set up the initial NSStatusBars title   / �00 L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e- 121 l  D D��34��  3 # StatusItem's setTitle:"WBTFU"   4 �55 : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "2 676 l  D D��������  ��  ��  7 898 l  D D��:;��  : 1 + set up the initial NSMenu of the statusbar   ; �<< V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r9 =>= r   D X?@? n  D RABA I   K R��C����  0 initwithtitle_ initWithTitle_C D��D m   K NEE �FF  C u s t o m��  ��  B n  D KGHG I   G K�������� 	0 alloc  ��  ��  H n  D GIJI o   E G���� 0 nsmenu NSMenuJ m   D E��
�� misccura@ o      ���� 0 newmenu newMenu> KLK l  Y Y��������  ��  ��  L MNM l  Y Y��OP��  O � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   P �QQ0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .N RSR n  Y cTUT I   ^ c��V���� 0 setdelegate_ setDelegate_V W��W  f   ^ _��  ��  U o   Y ^���� 0 newmenu newMenuS XYX l  d d��������  ��  ��  Y Z��Z n  d r[\[ I   i r��]���� 0 setmenu_ setMenu_] ^��^ o   i n���� 0 newmenu newMenu��  ��  \ o   d i���� 0 
statusitem 
StatusItem��  � _`_ l     ��������  ��  ��  ` aba l     ��cd��  c � �-----------------------------------------------------------------------------------------------------------------------------------------------   d �ee - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -b fgf l     ��hi��  h � �-----------------------------------------------------------------------------------------------------------------------------------------------   i �jj - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -g klk l     ��������  ��  ��  l mnm l     �������  ��  �  n opo l     ����  �  �  p qrq l     ����  �  �  r sts l     ����  �  �  t uvu l     ����  �  �  v wxw i  ��yzy I      ���� 0 runonce runOnce�  �  z k     {{ |}| l      �~�  ~ � �if not (current application's NSThread's isMainThread()) as boolean then		display alert "This script must be run from the main thread." buttons {"Cancel"} as critical		error number -128	end if    ���� i f   n o t   ( c u r r e n t   a p p l i c a t i o n ' s   N S T h r e a d ' s   i s M a i n T h r e a d ( ) )   a s   b o o l e a n   t h e n  	 	 d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l  	 	 e r r o r   n u m b e r   - 1 2 8  	 e n d   i f} ��� l     ����  �  �  � ��� I     ���� (0 animatemenubaricon animateMenuBarIcon�  �  � ��� I    ���� 0 	plistinit 	plistInit�  �  �  x ��� l     ����  �  �  � ��� l   ���� n   ��� I    ���� 0 makestatusbar makeStatusBar�  �  �  f    �  �  � ��� l   ���� I    ���� 0 runonce runOnce�  �  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  � w q Function that is always running in the background. This doesn't need to get called as it is running from the off   � ��� �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f� ��� l     ����  � � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   � ����   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .� ��� i  ����� I     ���
� .miscidlenmbr    ��� null�  �  � k     ��� ��� Z     ������ l    ���� E     ��� o     �~�~ &0 displaysleepstate displaySleepState� o    
�}�} 0 strawake strAwake�  �  � Z    ����|�{� l   ��z�y� =    ��� o    �x�x 0 isbackingup isBackingUp� m    �w
�w boovfals�z  �y  � Z    �����v� l   ��u�t� =    ��� o    �s�s 0 manualbackup manualBackup� m    �r
�r boovfals�u  �t  � Z   " s���q�p� l  " +��o�n� A   " +��� l  " )��m�l� n   " )��� 1   ' )�k
�k 
hour� l  " '��j�i� I  " '�h�g�f
�h .misccurdldt    ��� null�g  �f  �j  �i  �m  �l  � m   ) *�e�e �o  �n  � k   . o�� ��� r   . 7��� l  . 5��d�c� n   . 5��� 1   3 5�b
�b 
min � l  . 3��a�`� l  . 3��_�^� I  . 3�]�\�[
�] .misccurdldt    ��� null�\  �[  �_  �^  �a  �`  �d  �c  � o      �Z�Z 0 m  � ��� l  8 8�Y�X�W�Y  �X  �W  � ��V� Z   8 o����U� G   8 C��� l  8 ;��T�S� =   8 ;��� o   8 9�R�R 0 m  � m   9 :�Q�Q �T  �S  � l  > A��P�O� =   > A��� o   > ?�N�N 0 m  � m   ? @�M�M -�P  �O  � Z   F U���L�K� l  F I��J�I� =   F I��� o   F G�H�H 0 scheduledtime scheduledTime� m   G H�G�G �J  �I  � I   L Q�F�E�D�F 0 	plistinit 	plistInit�E  �D  �L  �K  � ��� G   X c��� l  X [��C�B� =   X [��� o   X Y�A�A 0 m  � m   Y Z�@�@  �C  �B  � l  ^ a��?�>� =   ^ a��� o   ^ _�=�= 0 m  � m   _ `�<�< �?  �>  � ��;� I   f k�:�9�8�: 0 	plistinit 	plistInit�9  �8  �;  �U  �V  �q  �p  � ��� l  v }��7�6� =   v }��� o   v {�5�5 0 manualbackup manualBackup� m   { |�4
�4 boovtrue�7  �6  � ��3� I   � ��2�1�0�2 0 	plistinit 	plistInit�1  �0  �3  �v  �|  �{  �  �  � ��� l  � ��/�.�-�/  �.  �-  � ��,� L   � ��� m   � ��+�+ �,  �       5�*���) g������(�'�&��%�$�#��� ��	
�*  � 3�"�!� ����������������������
�	��������� ��������������������������������
�" 
pimr�! 0 
statusitem 
StatusItem�  0 
thedisplay 
theDisplay� 0 defaults  � $0 internalmenuitem internalMenuItem� $0 externalmenuitem externalMenuItem� 0 newmenu newMenu� 0 thelist theList� 0 nsfw  � 0 notifications  �  0 backupthenquit backupThenQuit� 	0 plist  � 0 initialbackup initialBackup� 0 manualbackup manualBackup� 0 isbackingup isBackingUp� "0 messagesmissing messagesMissing� *0 messagesencouraging messagesEncouraging� $0 messagescomplete messagesComplete� &0 messagescancelled messagesCancelled� 40 messagesalreadybackingup messagesAlreadyBackingUp� 0 strawake strAwake� 0 strsleep strSleep� &0 displaysleepstate displaySleepState� 0 	plistinit 	plistInit�
 0 diskinit diskInit�	 0 freespaceinit freeSpaceInit� 0 
backupinit 
backupInit� 0 tidyup tidyUp� 
0 backup  � 0 itexists itExists� .0 getcomputeridentifier getComputerIdentifier� 0 gettimestamp getTimestamp� 0 comparesizes compareSizes� 0 	stopwatch  �  0 getduration getDuration�� 0 setfocus setFocus�� $0 menuneedsupdate_ menuNeedsUpdate_�� 0 	makemenus 	makeMenus��  0 backuphandler_ backupHandler_�� .0 backupandquithandler_ backupAndQuitHandler_��  0 nsfwonhandler_ nsfwOnHandler_�� "0 nsfwoffhandler_ nsfwOffHandler_�� 00 notificationonhandler_ notificationOnHandler_�� 20 notificationoffhandler_ notificationOffHandler_�� 0 quithandler_ quitHandler_�� (0 animatemenubaricon animateMenuBarIcon�� $0 resetmenubaricon resetMenuBarIcon�� 0 makestatusbar makeStatusBar�� 0 runonce runOnce
�� .miscidlenmbr    ��� null
�� .aevtoappnull  �   � ****� ����    !"#  �� L��
�� 
vers��  ! ��$��
�� 
cobj$ %%   ��
�� 
osax��  " ��&��
�� 
cobj& ''   �� U
�� 
frmk��  # ��(��
�� 
cobj( ))   �� [
�� 
frmk��  
�) 
msng� ** ����+
�� misccura
�� 
pcls+ �,,  N S U s e r D e f a u l t s� -- ����.
�� misccura
�� 
pcls. �//  N S M e n u I t e m� 00 ����1
�� misccura
�� 
pcls1 �22  N S M e n u I t e m� 33 ����4
�� misccura
�� 
pcls4 �55  N S M e n u� ��6�� 6   � � � � �
�( boovfals
�' boovtrue
�& boovfals� �77 � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�% boovtrue
�$ boovfals
�# boovfals� ��8�� 8  � ��9�� 	9 	 '+/37;?CF� ��:�� 	: 	 PTX\`dhlo  ��;�� ;  y}������ ��<�� 	< 	 ��������� �==�             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 3 3 3 8 3 4 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 0 0 , " I d l e T i m e r E l a p s e d T i m e " = 1 4 4 3 9 7 4 , " C u r r e n t P o w e r S t a t e " = 4 } �������>?���� 0 	plistinit 	plistInit��  ��  > 
���������������������� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk��  0 computerfolder computerFolder�� 0 
backuptime 
backupTime�� 0 thecontents theContents�� 0 thevalue theValue�� 0 welcome  �� 	0 reply  �� 0 backuptimes backupTimes�� 0 selectedtime selectedTime? =��J������������������c�q���y�������������������������������%4������� 0 itexists itExists
�� 
plif
�� 
pcnt
�� 
valL��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive��  0 computerfolder computerFolder�� 0 scheduledtime scheduledTime� .0 getcomputeridentifier getComputerIdentifier�  ��� 0 setfocus setFocus
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� afdrcusr
� .earsffdralis        afdr
� 
prmp
� .sysostflalis    ��� null
� 
TEXT
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
� .corecrel****      � null
� 
plii
� 
insh
� 
kind� � 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .ascrcmnt****      � ****� 0 diskinit diskInit���jE�O*�b  l+ e  6� .*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUY�*j+ 
E�O� ��n)j+ O�E�O��a a _ a a kva ka  a ,E�Oa j E�O*a a l a &E�O�a ,E�O�a ,E�Oa  a !a "mvE�O�a a #l $kva &E�O�a %  
a &E�Y #�a '  
a (E�Y �a )  
a *E�Y hOPoUO� �*a +�a ,a -b  la . / �*a +a 0a 1*6a ,a 2a a -a 3�a 4a 4 /O*a +a 0a 1*6a ,a 2a a -a 5�a 4a 4 /O*a +a 0a 1*6a ,a 2a a -a 6�a 4a 4 /O*a +a 0a 1*6a ,a 2a a -a 7�a 4a 4 /UUO�E` 8O�E` 9O�E` :O�E�O_ 8_ 9_ :�a .vj ;O*j+ < �v��@A�� 0 diskinit diskInit�  �  @ ��� 0 msg  � 	0 reply  A ������������������������ "0 destinationdisk destinationDisk� 0 itexists itExists� 0 freespaceinit freeSpaceInit� 0 setfocus setFocus
� 
cobj
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit� 0 diskinit diskInit
� .sysonotfnull��� ��� TEXT� �*��l+ e  *fk+ Y �)j+ Ob  �.E�O��������lv�l� a ,E�O�a   
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h ����BC�� 0 freespaceinit freeSpaceInit� �D� D  �� 	0 again  �  B ���� 	0 again  � 	0 reply  � 0 msg  C �~�}�|�{�z�y"�x�w�v+.�u�t�s�r?CLO[�q�pz�o���~ 0 sourcefolder sourceFolder�} "0 destinationdisk destinationDisk�| 0 comparesizes compareSizes�{ 0 
backupinit 
backupInit�z 0 setfocus setFocus
�y 
appr
�x 
disp�w 0 appicon appIcon
�v 
btns
�u 
dflt�t 
�s .sysodlogaskr        TEXT
�r 
bhit�q 0 tidyup tidyUp
�p 
cobj
�o .sysonotfnull��� ��� TEXT� �*��l+ e  
*j+ Y �)j+ O�f  ��������lv�l� a ,E�Y *�e  #a �a ���a a lv�l� a ,E�Y hO�a   
*j+ Y ]b  b   b  a .E�Y hOb  	e  4b  e  ��a l Y b  f  a �a l Y hY h �n��m�lEF�k�n 0 
backupinit 
backupInit�m  �l  E  F �j�i�h���g��f�e�d�c�b��a�`���_�^��]&(4OG�\�[
�j 
psxf�i "0 destinationdisk destinationDisk�h 	0 drive  �g 0 itexists itExists
�f 
kocl
�e 
cfol
�d 
insh
�c 
prdt
�b 
pnam�a 
�` .corecrel****      � null�_ 0 machinefolder machineFolder
�^ 
cdis�] 0 backupfolder backupFolder�\ 0 latestfolder latestFolder�[ 
0 backup  �k �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` OPUO*j+  �Z{�Y�XGH�W�Z 0 tidyup tidyUp�Y  �X  G 
�V�U�T�S�R�Q�P�O�N�M�V 0 bf bF�U 0 creationdates creationDates�T 0 theoldestdate theOldestDate�S 0 j  �R 0 i  �Q 0 thisdate thisDate�P  0 foldertodelete folderToDelete�O 0 todelete toDelete�N 
0 reply2  �M 0 msg  H .�L�K�J��I�H��G�F�E�D�C��B�A�@�?�>4�=8�<�;�:AD�9�8�7�6O�5�4n�3x{�2���1��
�L 
psxf�K "0 destinationdisk destinationDisk�J 	0 drive  
�I 
cdis
�H 
cfol�G 0 machinefolder machineFolder
�F 
cobj
�E 
ascd
�D .corecnte****       ****
�C 
pnam
�B .ascrcmnt****      � ****
�A 
TEXT
�@ 
psxp
�? .sysoexecTEXT���     TEXT�> 0 setfocus setFocus
�= 
appr
�< 
disp�; 0 appicon appIcon
�: 
btns
�9 
dflt�8 
�7 .sysodlogaskr        TEXT
�6 
bhit
�5 
trsh
�4 .fndremptnull��� ��� obj 
�3 .sysonotfnull��� ��� TEXT�2 0 freespaceinit freeSpaceInit
�1 .sysodelanull��� ��� nmbr�W�*��/E�O��*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/�&E�O��,�&E�Oa �%j Oa �%a %E�O�j O)j+ Oa a a a _ a a a lva la  a  ,E�O�a !  *a ",j #Y _b  b   Pb  �.E�Ob  	e  8b  e  �a a $l %Y b  f  a &a a 'l %Y hY hY hO*ek+ (Ob  b   Tb  	e  Fb  e  a )a a *l %Okj +Y #b  f  a ,a a -l %Okj +Y hY hY hU �0��/�.IJ�-�0 
0 backup  �/  �.  I 
�,�+�*�)�(�'�&�%�$�#�, 0 t  �+ 0 x  �* "0 containerfolder containerFolder�) 0 
foldername 
folderName�( 0 d  �' 0 duration  �& 0 msg  �% 0 c  �$ 0 oldestfolder oldestFolder�# 0 todelete toDeleteJ i�"�!� ����������������I���������
���	���������kmp��������������� 				H	J	L	N����	�	�	�	�	�	�	�




$
&
)
<
>
A
P
S
]
`
�
�
�
�
�
�
�
�
�
�
�
�
�" 0 gettimestamp getTimestamp�! (0 animatemenubaricon animateMenuBarIcon
�  .sysodelanull��� ��� nmbr� 0 	stopwatch  
� 
psxf� 0 sourcefolder sourceFolder
� 
TEXT
� 
cfol
� 
pnam
� 
kocl
� 
insh� 0 backupfolder backupFolder
� 
prdt� 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � 0 machinefolder machineFolder� (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
� 
appr
�
 .sysonotfnull��� ��� TEXT�	 "0 destinationdisk destinationDisk
� .sysoexecTEXT���     TEXT�  �  � 0 endtime endTime� 0 getduration getDuration
� 
cobj� $0 resetmenubaricon resetMenuBarIcon
� .aevtquitnull��� ��� null
�  .ascrcmnt****      � ****
�� 
alis
�� 
psxp�-�*ek+  E�OeEc  O*j+ Okj O*�k+ O�R*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e b*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_  a !%_ %a "%�%a #%�&E�O a $�%a %%�%a &%j 'OPW X ( )hOfEc  OfEc  Ob  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  Tb  e   a -�%a .%�%a a /l Okj Y )b  f  a 0�%a 1%a a 2l Okj Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYxb  f m_  a 6%_ %a 7%�%a 8%�%a 9%�&E�O_  a :%_ %a ;%�%a <%�&E�Oa =�%j >Ob  b   l*j a ,E` Ob  a ,.E�Ob  	e  Db  e  �a a ?l Okj Y #b  f  a @a a Al Okj Y hY hY hO  a B�%a C%�%a D%�%a E%j 'OPW X ( )hOfEc  OfEc  O*�/a F&a ,-jv [��/�&E�O�a G,�&E�Oa H�%j >Oa I�%a J%E�O�j 'Ob  b  *j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  �a K  Tb  e   a L�%a M%�%a a Nl Okj Y )b  f  a O�%a P%a a Ql Okj Y hY Qb  e   a R�%a S%�%a a Tl Okj Y )b  f  a U�%a V%a a Wl Okj Y hOb  e  a Xa a Yl Y b  f  a Za a [l Y hY hO)j+ 3Y hY	b  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  ��a K  Tb  e   a \�%a ]%�%a a ^l Okj Y )b  f  a _�%a `%a a al Okj Y hY Qb  e   a b�%a c%�%a a dl Okj Y )b  f  a e�%a f%a a gl Okj Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a hk+ 	 ��4����KL���� 0 itexists itExists�� ��M�� M  ������ 0 
objecttype 
objectType�� 
0 object  ��  K ������ 0 
objecttype 
objectType�� 
0 object  L lD����R��b��
�� 
cdis
�� .coredoexnull���     ****
�� 
file
�� 
cfol�� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU
 �������NO���� .0 getcomputeridentifier getComputerIdentifier��  ��  N �������� 0 computername computerName�� "0 strserialnumber strSerialNumber��  0 identifiername identifierNameO ��������
�� .sysosigtsirr   ��� null
�� 
sicn
�� .sysoexecTEXT���     TEXT�� *j  �,E�O�j E�O��%�%E�O� �������PQ���� 0 gettimestamp getTimestamp�� ��R�� R  ���� 0 isfolder isFolder��  P ���������������������������������� 0 isfolder isFolder�� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  Q ������������������������������#2�����Q����}����
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� misccura
� 
psof
� 
psin� 
� .sysooffslong    ��� null
� 
cha ���*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO� ���ST�� 0 comparesizes compareSizes� �U� U  ��� 
0 source  � 0 destination  �  S ����������������� 
0 source  � 0 destination  � 0 fit  � 0 
templatest 
tempLatest� $0 latestfoldersize latestFolderSize� 0 c  � 0 l  � 0 md  � 0 	cachesize 	cacheSize� 0 logssize logsSize� 0 mdsize mdSize� 
0 buffer  � (0 adjustedfoldersize adjustedFolderSize� 0 
foldersize 
folderSize� 0 	freespace 	freeSpace� 0 temp  T !��>�@�����Tbp�]��������������JL
� 
psxf� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� afdrdlib
� 
from
� fldmfldu
� .earsffdralis        afdr
� 
psxp
� .sysoexecTEXT���     TEXT
� misccura� � d
� .sysorondlong        doub
� 
cdis
� 
frsp
� .ascrcmnt****      � ****��eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O�*�%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�O��%�%j Ob  f  1a �%a  %j E�Oa  �a !a  j Ua !E�OPY hUOb  f  ,��E�O�j ��l E�Y hO��� fE�Y hY b  e  ��� fE�Y hY hO� ����VW�� 0 	stopwatch  � ��X�� X  ���� 0 mode  �  V ������ 0 mode  �� 0 x  W ���������� 0 gettimestamp getTimestamp
�� .ascrcmnt****      � ****� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP ������YZ���� 0 getduration getDuration��  ��  Y ���� 0 duration  Z ������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O� �~,�}�|[\�{�~ 0 setfocus setFocus�}  �|  [  \ /�z
�z .miscactvnull��� ��� null�{ � *j U �yZ�x�w]^�v�y $0 menuneedsupdate_ menuNeedsUpdate_�x �u_�u _  �t
�t 
cmnu�w  ]  ^ �s�s 0 	makemenus 	makeMenus�v )j+   �r~�q�p`a�o�r 0 	makemenus 	makeMenus�q  �p  ` �n�m�l�n 0 i  �m 0 	this_item  �l 0 thismenuitem thisMenuItema "�k�j�i�h��g�f�e���d�������+.<JM[hk�c�b�a�`�_�k  0 removeallitems removeAllItems
�j 
cobj
�i 
nmbr
�h 
TEXT
�g misccura�f 0 
nsmenuitem 
NSMenuItem�e 	0 alloc  �d J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�c 0 additem_ addItem_�b 0 
settarget_ 
setTarget_�a 
�` 
bool�_ 0 separatoritem separatorItem�o?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY�� �^��]�\bc�[�^  0 backuphandler_ backupHandler_�] �Zd�Z d  �Y�Y 
0 sender  �\  b �X�W�X 
0 sender  �W 0 msg  c ��V��U�T���S
�V 
appr
�U .sysonotfnull��� ��� TEXT
�T .sysodelanull��� ��� nmbr
�S 
cobj�[ �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h �R#�Q�Pef�O�R .0 backupandquithandler_ backupAndQuitHandler_�Q �Ng�N g  �M�M 
0 sender  �P  e �L�K�J�L 
0 sender  �K 	0 reply  �J 0 msg  f 7�I;�H�G�FDG�E�D�C�BSqt�A�@����?���
�I 
appr
�H 
disp�G 0 appicon appIcon
�F 
btns
�E 
dflt�D 
�C .sysodlogaskr        TEXT
�B 
bhit
�A .sysonotfnull��� ��� TEXT
�@ .sysodelanull��� ��� nmbr
�? 
cobj�O �b  f  ���������lv�l� 
�,E�O��  \eEc  
OeEc  Ob  	e  >b  e  ���l Okj Y !b  f  a �a l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h �>��=�<hi�;�>  0 nsfwonhandler_ nsfwOnHandler_�= �:j�: j  �9�9 
0 sender  �<  h �8�8 
0 sender  i ������7� �7 �; >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h �6�5�4kl�3�6 "0 nsfwoffhandler_ nsfwOffHandler_�5 �2m�2 m  �1�1 
0 sender  �4  k �0�0 
0 sender  l &*.25�/BFJNQ�/ �3 >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h �.`�-�,no�+�. 00 notificationonhandler_ notificationOnHandler_�- �*p�* p  �)�) 
0 sender  �,  n �(�( 
0 sender  o tx|���'������' �+ >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h �&��%�$qr�#�& 20 notificationoffhandler_ notificationOffHandler_�% �"s�" s  �!�! 
0 sender  �$  q � �  
0 sender  r ������������ �# >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h ����tu�� 0 quithandler_ quitHandler_� �v� v  �� 
0 sender  �  t ��� 
0 sender  � 	0 reply  u ���������&)�,�CGPS]c� 0 setfocus setFocus
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� .aevtquitnull��� ��� null
� .ascrcmnt****      � ****� �b  e  5)j+  O��������lv�l� �,E�O��  
�j Y 	a j Y Jb  f  ?)j+  Oa �a ���a a lv�l� �,E�O�a   
�j Y 	a j Y h �r�
�	wx�� (0 animatemenubaricon animateMenuBarIcon�
  �	  w ��� 0 	imagepath 	imagePath� 	0 image  x ������� ��������
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr
� 
psxp
�  misccura�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
 �������yz���� $0 resetmenubaricon resetMenuBarIcon��  ��  y ������ 0 	imagepath 	imagePath�� 	0 image  z ���������������������
�� 
alis
�� 
rtyp
�� 
ctxt
�� .earsffdralis        afdr
�� 
psxp
�� misccura�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_�� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
 �������{|���� 0 makestatusbar makeStatusBar��  ��  { �������� 0 bar  �� 0 	imagepath 	imagePath�� 	0 image  | �����������������������������E������
�� misccura�� 0 nsstatusbar NSStatusBar�� "0 systemstatusbar systemStatusBar�� .0 statusitemwithlength_ statusItemWithLength_
�� 
alis
�� 
rtyp
�� 
ctxt
�� .earsffdralis        afdr
�� 
psxp�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_�� 0 nsmenu NSMenu��  0 initwithtitle_ initWithTitle_�� 0 setdelegate_ setDelegate_�� 0 setmenu_ setMenu_�� s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+  ��z����}~���� 0 runonce runOnce��  ��  }  ~ ������ (0 animatemenubaricon animateMenuBarIcon�� 0 	plistinit 	plistInit�� *j+  O*j+  ����������
�� .miscidlenmbr    ��� null��  ��   ���� 0 m  � 
�������������������
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit� �� �b  b   �b  f  vb  f  V*j  �,� F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY b  e  
*j+ Y hY hY hO� �������
� .aevtoappnull  �   � ****� k     ��  ��� ��� ���  �  �  �  � ���� ����
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr� 0 appicon appIcon� 0 makestatusbar makeStatusBar� 0 runonce runOnce� *�)��l �%/E�O)j+ O*j+ ascr  ��ޭ