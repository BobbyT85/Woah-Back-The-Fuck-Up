FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S g�� ��� 0 thelist theList � J   S f � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   Y \ � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   \ _ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   _ b � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   q q � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   q q � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ����� 0 endtime endTime�   �  � � � l     �~�}�|�~  �}  �|   �  � � � l     �{�z�y�{  �z  �y   �  � � � l     �x � ��x   �   Property assignment    � � � � (   P r o p e r t y   a s s i g n m e n t �  � � � j   q ��w ��w 	0 plist   � b   q � � � � n  q � � � � 1   ~ ��v
�v 
psxp � l  q ~ ��u�t � I  q ~�s � �
�s .earsffdralis        afdr � m   q t�r
�r afdrdlib � �q ��p
�q 
from � m   w z�o
�o fldmfldu�p  �u  �t   � m   � � � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � l     �n�m�l�n  �m  �l   �  � � � j   � ��k ��k 0 initialbackup initialBackup � m   � ��j
�j boovtrue �  � � � j   � ��i ��i 0 manualbackup manualBackup � m   � ��h
�h boovfals �  � � � j   � ��g ��g 0 isbackingup isBackingUp � m   � ��f
�f boovfals �  � � � l     �e�d�c�e  �d  �c   �  � � � j   � ��b ��b "0 messagesmissing messagesMissing � J   � � � �  � � � m   � � � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m   � � � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �    m   � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !  m   � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �a m   � �		 �

 � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�a   �  l     �`�_�^�`  �_  �^    j   � ��]�] *0 messagesencouraging messagesEncouraging J   � �  m   � � �  C o m e   o n !  m   � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r !  m   � � � " N o   p a i n !   N o   p a i n !  m   � � �   0 L e t ' s   d o   t h i s   a s   a   t e a m ! !"! m   � �## �$$  W e   c a n   d o   i t !" %&% m   � �'' �((  F r e e e e e e e e e d o m !& )*) m   � �++ �,, 2 A l t o g e t h e r   o r   n o t   a t   a l l !* -.- m   � �// �00 H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !. 1�\1 m   � �22 �33 H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�\   454 l     �[�Z�Y�[  �Z  �Y  5 676 j   � ��X8�X $0 messagescomplete messagesComplete8 J   � �99 :;: m   � �<< �==  N i c e   o n e !; >?> m   � �@@ �AA * Y o u   a b s o l u t e   l e g   e n d !? BCB m   � �DD �EE  G o o d   l a d !C FGF m   � �HH �II  P e r f i c k !G JKJ m   � �LL �MM  H a p p y   d a y s !K NON m   � �PP �QQ  F r e e e e e e e e e d o m !O RSR m   � �TT �UU H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !S VWV m   � �XX �YY B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !W Z�WZ m   � �[[ �\\ d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�W  7 ]^] l     �V�U�T�V  �U  �T  ^ _`_ j   ��Sa�S &0 messagescancelled messagesCancelleda J   �bb cdc m   � �ee �ff  T h a t ' s   a   s h a m ed ghg m   � �ii �jj  A h   m a n ,   u n l u c k yh klk m   � �mm �nn  O h   w e l ll opo m   � �qq �rr @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e np sts m   � �uu �vv , W e l l   t h a t ' s   a   l e t   d o w nt wxw m   � �yy �zz P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?x {|{ m   �}} �~~  A r r o g a n t| �R m  �� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�R  ` ��� l     �Q�P�O�Q  �P  �O  � ��� j  .�N��N 40 messagesalreadybackingup messagesAlreadyBackingUp� J  +�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  �� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  �� ���  D i c k� ��� m  �� ���  F u c k t a r d� ��� m  �� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  !�� ���  A r r o g a n t� ��� m  !$�� ��� $ C h i l l   t h e   f u c k   o u t� ��M� m  $'�� ���  H o l d   f i r e�M  � ��� l     �L�K�J�L  �K  �J  � ��� j  /5�I��I 0 strawake strAwake� m  /2�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  6<�H��H 0 strsleep strSleep� m  69�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  =G�G��G &0 displaysleepstate displaySleepState� I =D�F��E
�F .sysoexecTEXT���     TEXT� m  =@�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�E  � ��� l     �D�C�B�D  �C  �B  � ��� l     �A�@�?�A  �@  �?  � ��� l     �>�=�<�>  �=  �<  � ��� l     �;���;  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �:���:  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �9���9  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �8�7�6�8  �7  �6  � ��� l     �5���5  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     �4���4  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     �3���3  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i  HK��� I      �2�1�0�2 0 	plistinit 	plistInit�1  �0  � k    ��� ��� q      �� �/��/ 0 
foldername 
folderName� �.��. 0 
backupdisk 
backupDisk� �-��-  0 computerfolder computerFolder� �,�+�, 0 
backuptime 
backupTime�+  � ��� r     ��� m     �*�*  � o      �)�) 0 
backuptime 
backupTime� ��� l   �(�'�&�(  �'  �&  � ��� Z   ����%�� l   ��$�#� =    ��� I    �"��!�" 0 itexists itExists� ��� m    �� �    f i l e� �  o    �� 	0 plist  �   �!  � m    �
� boovtrue�$  �#  � O    E k    D  r    $ n    "	
	 1     "�
� 
pcnt
 4     �
� 
plif o    �� 	0 plist   o      �� 0 thecontents theContents  r   % * n   % ( 1   & (�
� 
valL o   % &�� 0 thecontents theContents o      �� 0 thevalue theValue  l  + +����  �  �    r   + 0 n   + . o   , .��  0 foldertobackup FolderToBackup o   + ,�� 0 thevalue theValue o      �� 0 
foldername 
folderName  r   1 6 n   1 4 o   2 4�� 0 backupdrive BackupDrive o   1 2�� 0 thevalue theValue o      �� 0 
backupdisk 
backupDisk  !  r   7 <"#" n   7 :$%$ o   8 :��  0 computerfolder computerFolder% o   7 8�� 0 thevalue theValue# o      ��  0 computerfolder computerFolder! &'& r   = B()( n   = @*+* o   > @�
�
 0 scheduledtime scheduledTime+ o   = >�	�	 0 thevalue theValue) o      �� 0 
backuptime 
backupTime' ,-, l  C C����  �  �  - .�. l  C C�/0�  / . (log {folderName, backupDisk, backupTime}   0 �11 P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�   m    22�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �%  � k   H�33 454 r   H O676 I   H M��� � .0 getcomputeridentifier getComputerIdentifier�  �   7 o      ����  0 computerfolder computerFolder5 898 l  P P��������  ��  ��  9 :;: O   P �<=< t   T �>?> k   V �@@ ABA n  V [CDC I   W [�������� 0 setfocus setFocus��  ��  D  f   V WB EFE l  \ \��������  ��  ��  F GHG r   \ eIJI l  \ cK����K I  \ c����L
�� .sysostflalis    ��� null��  L ��M��
�� 
prmpM m   ^ _NN �OO @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p��  ��  ��  J o      ���� 0 
foldername 
folderNameH PQP r   f uRSR c   f sTUT l  f oV����V I  f o����W
�� .sysostflalis    ��� null��  W ��X��
�� 
prmpX m   h kYY �ZZ R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  U m   o r��
�� 
TEXTS o      ���� 0 
backupdisk 
backupDiskQ [\[ l  v v��������  ��  ��  \ ]^] r   v }_`_ n   v {aba 1   w {��
�� 
psxpb o   v w���� 0 
foldername 
folderName` o      ���� 0 
foldername 
folderName^ cdc r   ~ �efe n   ~ �ghg 1    ���
�� 
psxph o   ~ ���� 0 
backupdisk 
backupDiskf o      ���� 0 
backupdisk 
backupDiskd iji l  � ���������  ��  ��  j klk r   � �mnm J   � �oo pqp m   � �rr �ss 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u rq tut m   � �vv �ww 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u ru x��x m   � �yy �zz , E v e r y   h o u r   o n   t h e   h o u r��  n o      ���� 0 backuptimes backupTimesl {|{ r   � �}~} c   � �� J   � ��� ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  � m   � ���
�� 
TEXT~ o      ���� 0 selectedtime selectedTime| ��� l  � �������  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l  � ���������  ��  ��  � ���� l  � �������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  ? m   T U����  ��= m   P Q���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ; ��� l  � ���������  ��  ��  � ���� O   ����� O   ����� k   ���� ��� I  �"�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   ���
�� 
plii� ����
�� 
insh�  ;  � �����
�� 
prdt� K  
�� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  �� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ���� 0 
foldername 
folderName��  ��  � ��� I #J�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  '*��
�� 
plii� ����
�� 
insh�  ;  -/� �����
�� 
prdt� K  2D�� ����
�� 
kind� m  58��
�� 
TEXT� ����
�� 
pnam� m  ;>�� ���  B a c k u p D r i v e� �����
�� 
valL� o  ?@���� 0 
backupdisk 
backupDisk��  ��  � ��� I Kr�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  OR��
�� 
plii� ����
�� 
insh�  ;  UW� �����
�� 
prdt� K  Zl�� ����
�� 
kind� m  ]`��
�� 
TEXT� ����
�� 
pnam� m  cf�� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  gh����  0 computerfolder computerFolder��  ��  � ���� I s������
�� .corecrel****      � null��  � ����
�� 
kocl� m  wz��
�� 
plii� ����
�� 
insh�  ;  }� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  S c h e d u l e d T i m e� ����
�� 
valL� o  ���~�~ 0 
backuptime 
backupTime�  ��  ��  � l  � ���}�|� I  � ��{�z�
�{ .corecrel****      � null�z  � �y 
�y 
kocl  m   � ��x
�x 
plif �w�v
�w 
prdt K   � � �u�t
�u 
pnam o   � ��s�s 	0 plist  �t  �v  �}  �|  � m   � ��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  �  l ���r�q�p�r  �q  �p   	 r  ��

 o  ���o�o 0 
foldername 
folderName o      �n�n 0 sourcefolder sourceFolder	  r  �� o  ���m�m 0 
backupdisk 
backupDisk o      �l�l "0 destinationdisk destinationDisk  r  �� o  ���k�k  0 computerfolder computerFolder o      �j�j 0 machinefolder machineFolder  r  �� o  ���i�i 0 
backuptime 
backupTime o      �h�h 0 scheduledtime scheduledTime  I ���g�f
�g .ascrcmnt****      � **** J  ��  o  ���e�e 0 sourcefolder sourceFolder  o  ���d�d "0 destinationdisk destinationDisk  !  o  ���c�c 0 machinefolder machineFolder! "�b" o  ���a�a 0 scheduledtime scheduledTime�b  �f   #$# l ���`�_�^�`  �_  �^  $ %�]% I  ���\�[�Z�\ 0 diskinit diskInit�[  �Z  �]  � &'& l     �Y�X�W�Y  �X  �W  ' ()( l     �V�U�T�V  �U  �T  ) *+* l     �S�R�Q�S  �R  �Q  + ,-, l     �P�O�N�P  �O  �N  - ./. l     �M�L�K�M  �L  �K  / 010 l     �J23�J  2 H B Function to detect if the selected hard drive is connected or not   3 �44 �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t1 565 l     �I78�I  7 T N This only happens once a hard drive has been selected and provides a reminder   8 �99 �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r6 :;: i  LO<=< I      �H�G�F�H 0 diskinit diskInit�G  �F  = Z     �>?�E@> l    	A�D�CA =     	BCB I     �BD�A�B 0 itexists itExistsD EFE m    GG �HH  d i s kF I�@I o    �?�? "0 destinationdisk destinationDisk�@  �A  C m    �>
�> boovtrue�D  �C  ? I    �=J�<�= 0 freespaceinit freeSpaceInitJ K�;K m    �:
�: boovfals�;  �<  �E  @ k    �LL MNM I    �9�8�7�9 0 setfocus setFocus�8  �7  N OPO r    $QRQ n    "STS 3     "�6
�6 
cobjT o     �5�5 "0 messagesmissing messagesMissingR o      �4�4 0 msg  P UVU r   % 5WXW l  % 3Y�3�2Y n   % 3Z[Z 1   1 3�1
�1 
bhit[ l  % 1\�0�/\ I  % 1�.]^
�. .sysodlogaskr        TEXT] o   % &�-�- 0 msg  ^ �,_`
�, 
btns_ J   ' +aa bcb m   ' (dd �ee  C a n c e l   B a c k u pc f�+f m   ( )gg �hh  O K�+  ` �*i�)
�* 
dflti m   , -�(�( �)  �0  �/  �3  �2  X o      �'�' 	0 reply  V j�&j Z   6 �kl�%mk l  6 9n�$�#n =   6 9opo o   6 7�"�" 	0 reply  p m   7 8qq �rr  O K�$  �#  l I   < A�!� ��! 0 diskinit diskInit�   �  �%  m Z   D �st��s l  D Ou��u E   D Ovwv o   D I�� &0 displaysleepstate displaySleepStatew o   I N�� 0 strawake strAwake�  �  t k   R �xx yzy r   R [{|{ n   R Y}~} 3   W Y�
� 
cobj~ o   R W�� &0 messagescancelled messagesCancelled| o      �� 0 msg  z � Z   \ ������ l  \ c���� =   \ c��� o   \ a�� 0 notifications  � m   a b�
� boovtrue�  �  � Z   f ������ l  f m���� =   f m��� o   f k�� 0 nsfw  � m   k l�

�
 boovtrue�  �  � I  p y�	��
�	 .sysonotfnull��� ��� TEXT� o   p q�� 0 msg  � ���
� 
appr� m   r u�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  � ��� l  | ����� =   | ���� o   | ��� 0 nsfw  � m   � ��
� boovfals�  �  � ��� I  � �� ��
�  .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  �  �  �  �  �  �  �  �&  ; ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  PS��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � Z     ������� l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    ��� ��� I    �������� 0 setfocus setFocus��  ��  � ��� l   ��������  ��  ��  � ��� Z    M������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r     0��� l    .������ n     .��� 1   , .��
�� 
bhit� l    ,������ I    ,����
�� .sysodlogaskr        TEXT� m     !�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   " &�� ��� m   " #�� ���  Y e s� ���� m   # $�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   ' (���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  � ��� l  3 6������ =   3 6��� o   3 4���� 	0 again  � m   4 5��
�� boovtrue��  ��  � ���� r   9 I��� l  9 G������ n   9 G��� 1   E G��
�� 
bhit� l  9 E������ I  9 E����
�� .sysodlogaskr        TEXT� m   9 :�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   ; ?�� ��� m   ; <�� ���  Y e s� ���� m   < =   �  C a n c e l   B a c k u p��  � ����
�� 
dflt m   @ A���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  ��  ��  �  l  N N��������  ��  ��   �� Z   N ��� l  N S	����	 =   N S

 o   N O���� 
0 reply1   m   O R �  Y e s��  ��   I   V [�������� 0 tidyup tidyUp��  ��  ��   k   ^ �  Z   ^ {���� l  ^ i���� E   ^ i o   ^ c���� &0 displaysleepstate displaySleepState o   c h���� 0 strawake strAwake��  ��   r   l w n   l u 3   q u��
�� 
cobj o   l q���� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��    l  | |��������  ��  ��   �� Z   | ����� l  | ����� =   | � !  o   | ����� 0 notifications  ! m   � ���
�� boovtrue��  ��   Z   � �"#$��" l  � �%����% =   � �&'& o   � ����� 0 nsfw  ' m   � ���
�� boovtrue��  ��  # I  � ���()
�� .sysonotfnull��� ��� TEXT( o   � ����� 0 msg  ) ��*��
�� 
appr* m   � �++ �,, > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  $ -.- l  � �/����/ =   � �010 o   � ����� 0 nsfw  1 m   � ���
�� boovfals��  ��  . 2��2 I  � ���34
�� .sysonotfnull��� ��� TEXT3 m   � �55 �66 , Y o u   s t o p p e d   b a c k i n g   u p4 ��7��
�� 
appr7 m   � �88 �99 
 W B T F U��  ��  ��  ��  ��  ��  ��  � :;: l     ��������  ��  ��  ; <=< l     �������  ��  �  = >?> l     �~�}�|�~  �}  �|  ? @A@ l     �{�z�y�{  �z  �y  A BCB l     �x�w�v�x  �w  �v  C DED l     �uFG�u  F,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   G �HHL   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .E IJI l     �tKL�t  K g a These folders, if required, are then set to their corresponding global variables declared above.   L �MM �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .J NON i  TWPQP I      �s�r�q�s 0 
backupinit 
backupInit�r  �q  Q k     �RR STS r     UVU 4     �pW
�p 
psxfW o    �o�o "0 destinationdisk destinationDiskV o      �n�n 	0 drive  T XYX l   �mZ[�m  Z ' !log (destinationDisk & "Backups")   [ �\\ B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )Y ]^] l   �l�k�j�l  �k  �j  ^ _`_ Z    ,ab�i�ha l   c�g�fc =    ded I    �ef�d�e 0 itexists itExistsf ghg m    	ii �jj  f o l d e rh k�ck b   	 lml o   	 
�b�b "0 destinationdisk destinationDiskm m   
 nn �oo  B a c k u p s�c  �d  e m    �a
�a boovfals�g  �f  b O    (pqp I   '�`�_r
�` .corecrel****      � null�_  r �^st
�^ 
kocls m    �]
�] 
cfolt �\uv
�\ 
inshu o    �[�[ 	0 drive  v �Zw�Y
�Z 
prdtw K    #xx �Xy�W
�X 
pnamy m     !zz �{{  B a c k u p s�W  �Y  q m    ||�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �i  �h  ` }~} l  - -�V�U�T�V  �U  �T  ~ � Z   - d���S�R� l  - >��Q�P� =   - >��� I   - <�O��N�O 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ��M� b   / 8��� b   / 4��� o   / 0�L�L "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�K�K 0 machinefolder machineFolder�M  �N  � m   < =�J
�J boovfals�Q  �P  � O   A `��� I  E _�I�H�
�I .corecrel****      � null�H  � �G��
�G 
kocl� m   G H�F
�F 
cfol� �E��
�E 
insh� n   I T��� 4   O T�D�
�D 
cfol� m   P S�� ���  B a c k u p s� 4   I O�C�
�C 
cdis� o   M N�B�B 	0 drive  � �A��@
�A 
prdt� K   U [�� �?��>
�? 
pnam� o   V Y�=�= 0 machinefolder machineFolder�>  �@  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �S  �R  � ��� l  e e�<�;�:�<  �;  �:  � ��� O   e ���� k   i �� ��� l  i i�9���9  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y�8�
�8 
cfol� o   u x�7�7 0 machinefolder machineFolder� n   i t��� 4   o t�6�
�6 
cfol� m   p s�� ���  B a c k u p s� 4   i o�5�
�5 
cdis� o   m n�4�4 	0 drive  � o      �3�3 0 backupfolder backupFolder� ��2� l  ~ ~�1���1  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�2  � m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��0�/�.�0  �/  �.  � ��� Z   � ����-�� l  � ���,�+� =   � ���� I   � ��*��)�* 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��(� b   � ���� b   � ���� b   � ���� o   � ��'�' "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��&�& 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�(  �)  � m   � ��%
�% boovfals�,  �+  � O   � ���� I  � ��$�#�
�$ .corecrel****      � null�#  � �"��
�" 
kocl� m   � ��!
�! 
cfol� � ��
�  
insh� o   � ��� 0 backupfolder backupFolder� ���
� 
prdt� K   � ��� ���
� 
pnam� m   � ��� ���  L a t e s t�  �  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �-  � r   � ���� m   � ��
� boovfals� o      �� 0 initialbackup initialBackup� ��� l  � �����  �  �  � ��� O   � ���� k   � ��� ��� r   � ���� n   � ���� 4   � ���
� 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ���
� 
cfol� o   � ��� 0 machinefolder machineFolder� n   � ���� 4   � ���
� 
cfol� m   � �   �  B a c k u p s� 4   � ��
� 
cdis o   � ��� 	0 drive  � o      �� 0 latestfolder latestFolder� � l  � ���    log (backupFolder)    � $ l o g   ( b a c k u p F o l d e r )�  � m   � ��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 	 l  � ����
�  �  �
  	 
�	
 I   � ����� 
0 backup  �  �  �	  O  l     ����  �  �    l     ��� �  �  �     l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.    � �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .  l     ����   � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.    ��   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .   l     ��!"��  ! � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   " �##P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .  $%$ l     ��&'��  & � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   ' �((>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .% )*) i  X[+,+ I      �������� 0 tidyup tidyUp��  ��  , k    z-- ./. r     010 4     ��2
�� 
psxf2 o    ���� "0 destinationdisk destinationDisk1 o      ���� 	0 drive  / 343 l   ��������  ��  ��  4 5��5 O   z676 k   y88 9:9 l   ��;<��  ; A ;set creationDates to creation date of items of backupFolder   < �== v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e r: >?> r    @A@ n    BCB 4    ��D
�� 
cfolD o    ���� 0 machinefolder machineFolderC n    EFE 4    ��G
�� 
cfolG m    HH �II  B a c k u p sF 4    ��J
�� 
cdisJ o    ���� 	0 drive  A o      ���� 0 bf bF? KLK r    MNM n    OPO 1    ��
�� 
ascdP n    QRQ 2   ��
�� 
cobjR o    ���� 0 bf bFN o      ���� 0 creationdates creationDatesL STS r     &UVU n     $WXW 4   ! $��Y
�� 
cobjY m   " #���� X o     !���� 0 creationdates creationDatesV o      ���� 0 theoldestdate theOldestDateT Z[Z r   ' *\]\ m   ' (���� ] o      ���� 0 j  [ ^_^ l  + +��������  ��  ��  _ `a` Y   + nb��cd��b k   9 iee fgf r   9 ?hih n   9 =jkj 4   : =��l
�� 
cobjl o   ; <���� 0 i  k o   9 :���� 0 creationdates creationDatesi o      ���� 0 thisdate thisDateg m��m Z   @ ino����n l  @ Hp����p >  @ Hqrq n   @ Fsts 1   D F��
�� 
pnamt 4   @ D��u
�� 
cobju o   B C���� 0 i  r m   F Gvv �ww  L a t e s t��  ��  o Z   K exy����x l  K Nz����z A   K N{|{ o   K L���� 0 thisdate thisDate| o   L M���� 0 theoldestdate theOldestDate��  ��  y k   Q a}} ~~ r   Q T��� o   Q R���� 0 thisdate thisDate� o      ���� 0 theoldestdate theOldestDate ��� r   U X��� o   U V���� 0 i  � o      ���� 0 j  � ��� l  Y Y������  � " log (item j of backupFolder)   � ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )� ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  c m   . /���� d I  / 4�����
�� .corecnte****       ****� o   / 0���� 0 creationdates creationDates��  ��  a ��� l  o o��������  ��  ��  � ��� l  o o������  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� I  o y�����
�� .coredeloobj        obj � n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  � ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l   z z������  � � �set oldestFolder to item j of backupFolder as string
		set oldestFolder to POSIX path of oldestFolder as string
		log ("to delete: " & oldestFolder)
		
		set toDelete to "rm -rf " & oldestFolder
		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g 
 	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g 
 	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r ) 
 	 	 
 	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r 
 	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� I   z �������� 0 setfocus setFocus��  ��  � ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
bhit� l  � ������� I  � �����
�� .sysodlogaskr        TEXT� m   � ��� ���B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .� ����
�� 
btns� J   � ��� ��� m   � ��� ���  E m p t y   T r a s h� ���� m   � ��� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   � ����� ��  ��  ��  ��  ��  � o      ���� 
0 reply2  � ��� Z   ������� l  � ������� =   � ���� o   � ����� 
0 reply2  � m   � ��� ���  E m p t y   T r a s h��  ��  � I  � ������
�� .fndremptnull��� ��� obj � l  � ������ 1   � ��~
�~ 
trsh��  �  ��  ��  � Z   ����}�|� l  � ���{�z� E   � ���� o   � ��y�y &0 displaysleepstate displaySleepState� o   � ��x�x 0 strawake strAwake�{  �z  � k   ��� ��� r   � ���� n   � ���� 3   � ��w
�w 
cobj� o   � ��v�v &0 messagescancelled messagesCancelled� o      �u�u 0 msg  � ��t� Z   ����s�r� l  � ���q�p� =   � ���� o   � ��o�o 0 notifications  � m   � ��n
�n boovtrue�q  �p  � Z   �����m� l  � ���l�k� =   � ���� o   � ��j�j 0 nsfw  � m   � ��i
�i boovtrue�l  �k  � I  � ��h��
�h .sysonotfnull��� ��� TEXT� o   � ��g�g 0 msg  � �f��e
�f 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�e  � ��� l  � ���d�c� =   � ���� o   � ��b�b 0 nsfw  � m   � ��a
�a boovfals�d  �c  � ��`� I  ��_��
�_ .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �^ �]
�^ 
appr  m   �  � 
 W B T F U�]  �`  �m  �s  �r  �t  �}  �|  �  l �\�[�Z�\  �[  �Z    I  �Y�X�Y 0 freespaceinit freeSpaceInit �W m  �V
�V boovtrue�W  �X   	�U	 Z  y
�T�S
 l #�R�Q E  # o  �P�P &0 displaysleepstate displaySleepState o  "�O�O 0 strawake strAwake�R  �Q   Z  &u�N�M l &-�L�K =  &- o  &+�J�J 0 notifications   m  +,�I
�I boovtrue�L  �K   Z  0q�H l 07�G�F =  07 o  05�E�E 0 nsfw   m  56�D
�D boovtrue�G  �F   k  :M  I :G�C
�C .sysonotfnull��� ��� TEXT m  := �   � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . . �B!�A
�B 
appr! m  @C"" �## 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�A   $�@$ I HM�?%�>
�? .sysodelanull��� ��� nmbr% m  HI�=�= �>  �@   &'& l PW(�<�;( =  PW)*) o  PU�:�: 0 nsfw  * m  UV�9
�9 boovfals�<  �;  ' +�8+ k  Zm,, -.- I Zg�7/0
�7 .sysonotfnull��� ��� TEXT/ m  Z]11 �22 t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n0 �63�5
�6 
appr3 m  `c44 �55 
 W B T F U�5  . 6�46 I hm�37�2
�3 .sysodelanull��� ��� nmbr7 m  hi�1�1 �2  �4  �8  �H  �N  �M  �T  �S  �U  7 m    88�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  * 9:9 l     �0�/�.�0  �/  �.  : ;<; l     �-�,�+�-  �,  �+  < =>= l     �*�)�(�*  �)  �(  > ?@? l     �'�&�%�'  �&  �%  @ ABA l     �$�#�"�$  �#  �"  B CDC l     �!EF�!  E M G Function that carries out the backup process and is split in 2 parts.    F �GG �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  D HIH l     � JK�   J*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   K �LLH   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .I MNM l     �OP�  O � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   P �QQ   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .N RSR i  \_TUT I      ���� 
0 backup  �  �  U k    ~VV WXW q      YY �Z� 0 t  Z �[� 0 x  [ ��� "0 containerfolder containerFolder�  X \]\ r     ^_^ I     �`�� 0 gettimestamp getTimestamp` a�a m    �
� boovtrue�  �  _ o      �� 0 t  ] bcb l  	 	����  �  �  c ded r   	 fgf m   	 
�
� boovtrueg o      �� 0 isbackingup isBackingUpe hih l   ����  �  �  i jkj I    �
�	��
 (0 animatemenubaricon animateMenuBarIcon�	  �  k lml I   �n�
� .sysodelanull��� ��� nmbrn m    �� �  m opo l   ����  �  �  p qrq I    #�s� � 0 	stopwatch  s t��t m    uu �vv 
 s t a r t��  �   r wxw l  $ $��������  ��  ��  x yzy O   $u{|{ k   (t}} ~~ l  ( (������  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d ��� l  ( (������  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p� ��� r   ( 0��� c   ( .��� 4   ( ,���
�� 
psxf� o   * +���� 0 sourcefolder sourceFolder� m   , -��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   1 9��� n   1 7��� 1   5 7��
�� 
pnam� 4   1 5���
�� 
cfol� o   3 4���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  : :������  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  : :��������  ��  ��  � ��� Z   : �������� l  : A������ =   : A��� o   : ?���� 0 initialbackup initialBackup� m   ? @��
�� boovfals��  ��  � k   D ��� ��� I  D R�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   F G��
�� 
cfol� ����
�� 
insh� o   H I���� 0 backupfolder backupFolder� �����
�� 
prdt� K   J N�� �����
�� 
pnam� o   K L���� 0 t  ��  ��  � ��� l  S S��������  ��  ��  � ��� r   S ]��� c   S Y��� 4   S W���
�� 
psxf� o   U V���� 0 sourcefolder sourceFolder� m   W X��
�� 
TEXT� o      ���� (0 activesourcefolder activeSourceFolder� ��� r   ^ h��� 4   ^ d���
�� 
cfol� o   ` c���� (0 activesourcefolder activeSourceFolder� o      ���� (0 activesourcefolder activeSourceFolder� ��� l  i i������  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   i ���� n   i ~��� 4   { ~���
�� 
cfol� o   | }���� 0 t  � n   i {��� 4   v {���
�� 
cfol� o   w z���� 0 machinefolder machineFolder� n   i v��� 4   q v���
�� 
cfol� m   r u�� ���  B a c k u p s� 4   i q���
�� 
cdis� o   m p���� 	0 drive  � o      ���� (0 activebackupfolder activeBackupFolder� ��� l  � �������  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ���� I  � ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
cfol� ����
�� 
insh� o   � ����� (0 activebackupfolder activeBackupFolder� �����
�� 
prdt� K   � ��� �����
�� 
pnam� o   � ����� 0 
foldername 
folderName��  ��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �������  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � �������  � D > This means that only new or updated files will be copied over   � ��� |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r� ��� l  � �������  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	� ��� l  � �������  �   Original sync code    � ��� (   O r i g i n a l   s y n c   c o d e  � ��� l  � ���� ��  � z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"     � �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "�  l  � ���������  ��  ��    l  � �����     Original code    �    O r i g i n a l   c o d e 	
	 l  � �����   i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path    � �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h
  l  � �����   x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.    � �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .  l  � �����   p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.    � �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .  l  � �����   � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.    ��   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .  l  � ��� ��   � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.     �!!v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e . "#" l  � ���������  ��  ��  # $%$ l  � ���������  ��  ��  % &��& Z   �t'()��' l  � �*����* =   � �+,+ o   � ����� 0 initialbackup initialBackup, m   � ���
�� boovtrue��  ��  ( k   ��-- ./. r   � �010 n   � �232 1   � ���
�� 
time3 l  � �4����4 I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  1 o      ���� 0 	starttime 	startTime/ 565 Z   �78����7 l  � �9����9 E   � �:;: o   � ����� &0 displaysleepstate displaySleepState; o   � ����� 0 strawake strAwake��  ��  8 Z   �<=����< l  � �>����> =   � �?@? o   � ����� 0 notifications  @ m   � ���
�� boovtrue��  ��  = Z   � �ABC��A l  � �D����D =   � �EFE o   � ����� 0 nsfw  F m   � ���
�� boovtrue��  ��  B I  � ���GH
�� .sysonotfnull��� ��� TEXTG m   � �II �JJ � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .H ��K��
�� 
apprK m   � �LL �MM 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  C NON l  � �P���P =   � �QRQ o   � ��~�~ 0 nsfw  R m   � ��}
�} boovfals��  �  O S�|S I  � ��{TU
�{ .sysonotfnull��� ��� TEXTT m   � �VV �WW \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m eU �zX�y
�z 
apprX m   � �YY �ZZ 
 W B T F U�y  �|  ��  ��  ��  ��  ��  6 [\[ l �x�w�v�x  �w  �v  \ ]^] l �u�t�s�u  �t  �s  ^ _`_ l �r�q�p�r  �q  �p  ` aba l �o�n�m�o  �n  �m  b cdc r  efe c  ghg l i�l�ki b  jkj b  lml b  non b  pqp b  rsr o  	�j�j "0 destinationdisk destinationDisks m  	tt �uu  B a c k u p s /q o  �i�i 0 machinefolder machineFoldero m  vv �ww  / L a t e s t /m o  �h�h 0 
foldername 
folderNamek m  xx �yy  /�l  �k  h m  �g
�g 
TEXTf o      �f�f 0 d  d z{z l   �e|}�e  | A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   } �~~ v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "{ � l   �d�c�b�d  �c  �b  � ��� Q   ?���� k  #6�� ��� l ##�a���a  ���do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings' --exclude='Sanghani.Canary/' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash''" & sourceFolder & "' '" & d & "/'"   � ���t d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S a n g h a n i . C a n a r y / '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h ' ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l ##�`�_�^�`  �_  �^  � ��� l ##�]���]  � S Mdo shell script "rsync -avz --cvs-exclude'" & sourceFolder & "' '" & d & "/'"   � ��� � d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l ##�\�[�Z�\  �[  �Z  � ��Y� I #6�X��W
�X .sysoexecTEXT���     TEXT� b  #2��� b  #.��� b  #,��� b  #(��� m  #&�� ���  d i t t o   '� o  &'�V�V 0 sourcefolder sourceFolder� m  (+�� ���  '   '� o  ,-�U�U 0 d  � m  .1�� ���  / '�W  �Y  � R      �T�S�R
�T .ascrerr ****      � ****�S  �R  � l >>�Q�P�O�Q  �P  �O  � ��� l @@�N�M�L�N  �M  �L  � ��� l @@�K�J�I�K  �J  �I  � ��� l @@�H�G�F�H  �G  �F  � ��� l @@�E�D�C�E  �D  �C  � ��� l @@�B�A�@�B  �A  �@  � ��� r  @G��� m  @A�?
�? boovfals� o      �>�> 0 isbackingup isBackingUp� ��� r  HO��� m  HI�=
�= boovfals� o      �<�< 0 manualbackup manualBackup� ��� l PP�;�:�9�;  �:  �9  � ��� Z  P����8�7� l P[��6�5� E  P[��� o  PU�4�4 &0 displaysleepstate displaySleepState� o  UZ�3�3 0 strawake strAwake�6  �5  � k  ^��� ��� r  ^k��� n  ^g��� 1  cg�2
�2 
time� l ^c��1�0� I ^c�/�.�-
�/ .misccurdldt    ��� null�.  �-  �1  �0  � o      �,�, 0 endtime endTime� ��� r  ls��� n lq��� I  mq�+�*�)�+ 0 getduration getDuration�*  �)  �  f  lm� o      �(�( 0 duration  � ��� r  t��� n  t}��� 3  y}�'
�' 
cobj� o  ty�&�& $0 messagescomplete messagesComplete� o      �%�% 0 msg  � ��� Z  �����$�#� l ����"�!� =  ����� o  ��� �  0 notifications  � m  ���
� boovtrue�"  �!  � Z  ������� l ������ =  ����� o  ���� 0 nsfw  � m  ���
� boovtrue�  �  � k  ���� ��� I �����
� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���� 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ���� 0 msg  � ���
� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  � ��� I �����
� .sysodelanull��� ��� nmbr� m  ���� �  �  � ��� l ������ =  ����� o  ���� 0 nsfw  � m  ���
� boovfals�  �  � ��� k  ���� ��� I �����
� .sysonotfnull��� ��� TEXT� b  ��   b  �� m  �� � 6 F o r   t h e   f i r s t   t i m e   e v e r   i n   o  ���
�
 0 duration   m  �� �    m i n u t e s ! 
� �	�
�	 
appr m  ��		 �

  B a c k e d   u p !�  � � I ����
� .sysodelanull��� ��� nmbr m  ���� �  �  �  �  �$  �#  �  l ������  �  �   �  n �� I  ���������� $0 resetmenubaricon resetMenuBarIcon��  ��    f  ���   �8  �7  �  l ����������  ��  ��   �� Z  ������ l ������ =  �� o  ������  0 backupthenquit backupThenQuit m  ����
�� boovtrue��  ��   I ������
�� .aevtquitnull��� ��� null m  ��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ��  ��  )  l  ���� =     o   ���� 0 initialbackup initialBackup  m  ��
�� boovfals��  ��   !��! k  
p"" #$# r  
)%&% c  
''(' l 
%)����) b  
%*+* b  
!,-, b  
./. b  
010 b  
232 b  
454 b  
676 o  
���� "0 destinationdisk destinationDisk7 m  88 �99  B a c k u p s /5 o  ���� 0 machinefolder machineFolder3 m  :: �;;  /1 o  ���� 0 t  / m  << �==  /- o   ���� 0 
foldername 
folderName+ m  !$>> �??  /��  ��  ( m  %&��
�� 
TEXT& o      ���� 0 c  $ @A@ r  *CBCB c  *ADED l *?F����F b  *?GHG b  *;IJI b  *9KLK b  *5MNM b  *1OPO o  *-���� "0 destinationdisk destinationDiskP m  -0QQ �RR  B a c k u p s /N o  14���� 0 machinefolder machineFolderL m  58SS �TT  / L a t e s t /J o  9:���� 0 
foldername 
folderNameH m  ;>UU �VV  /��  ��  E m  ?@��
�� 
TEXTC o      ���� 0 d  A WXW I DM��Y��
�� .ascrcmnt****      � ****Y l DIZ����Z b  DI[\[ m  DG]] �^^  b a c k i n g   u p   t o :  \ o  GH���� 0 d  ��  ��  ��  X _`_ l NN��������  ��  ��  ` aba Z  N�cd����c l NYe����e E  NYfgf o  NS���� &0 displaysleepstate displaySleepStateg o  SX���� 0 strawake strAwake��  ��  d k  \�hh iji r  \iklk n  \emnm 1  ae��
�� 
timen l \ao����o I \a������
�� .misccurdldt    ��� null��  ��  ��  ��  l o      ���� 0 	starttime 	startTimej pqp r  jursr n  jstut 3  os��
�� 
cobju o  jo���� *0 messagesencouraging messagesEncouragings o      ���� 0 msg  q v��v Z  v�wx����w l v}y����y =  v}z{z o  v{���� 0 notifications  { m  {|��
�� boovtrue��  ��  x Z  ��|}~��| l ������ =  ����� o  ������ 0 nsfw  � m  ����
�� boovtrue��  ��  } k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ~ ��� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� m  ���� ���  B a c k i n g   u p� �����
�� 
appr� m  ���� ��� 
 W B T F U��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  ��  ��  ��  b ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� Q  ������ k  ���� ��� l ��������  ���do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings'  --exclude='Sanghani.Canary/' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' --progress --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   � ���� d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S a n g h a n i . C a n a r y / '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l ����������  ��  ��  � ���� I �������
�� .sysoexecTEXT���     TEXT� b  ����� b  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ��� � r s y n c   - a q z   - - c v s - e x c l u d e   - - e x c l u d e =  . T r a s h    - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e =  . T r a s h e s    - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ������ 0 c  � m  ���� ���  '   '� o  ������ 0 sourcefolder sourceFolder� m  ���� ���  '   '� o  ������ 0 d  � m  ���� ���  / '��  ��  � R      ������
�� .ascrerr ****      � ****��  ��  � l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����~�}�  �~  �}  � ��� l ���|�{�z�|  �{  �z  � ��� l ���y�x�w�y  �x  �w  � ��� r  ����� m  ���v
�v boovfals� o      �u�u 0 isbackingup isBackingUp� ��� r  ����� m  ���t
�t boovfals� o      �s�s 0 manualbackup manualBackup� ��� l ���r�q�p�r  �q  �p  � ��o� Z  �p���n�� = ���� n  �
��� 2 
�m
�m 
cobj� l ���l�k� c  ���� 4  ��j�
�j 
psxf� o   �i�i 0 c  � m  �h
�h 
alis�l  �k  � J  
�g�g  � k  f�� ��� l �f���f  � % delete folder t of backupFolder   � ��� > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r� ��� l �e�d�c�e  �d  �c  � ��� r  ��� c  ��� n  ��� 4  �b�
�b 
cfol� o  �a�a 0 t  � o  �`�` 0 backupfolder backupFolder� m  �_
�_ 
TEXT� o      �^�^ 0 oldestfolder oldestFolder� ��� r  "��� c   	 		  n  			 1  �]
�] 
psxp	 o  �\�\ 0 oldestfolder oldestFolder	 m  �[
�[ 
TEXT� o      �Z�Z 0 oldestfolder oldestFolder� 			 I #,�Y	�X
�Y .ascrcmnt****      � ****	 l #(	�W�V	 b  #(				 m  #&	
	
 �		  t o   d e l e t e :  		 o  &'�U�U 0 oldestfolder oldestFolder�W  �V  �X  	 			 l --�T�S�R�T  �S  �R  	 			 r  -8			 b  -6			 b  -2			 m  -0		 �		  r m   - r f   '	 o  01�Q�Q 0 oldestfolder oldestFolder	 m  25		 �		  '	 o      �P�P 0 todelete toDelete	 			 I 9>�O	�N
�O .sysoexecTEXT���     TEXT	 o  9:�M�M 0 todelete toDelete�N  	 			 l ??�L�K�J�L  �K  �J  	 	�I	 Z  ?f	 	!�H�G	  l ?J	"�F�E	" E  ?J	#	$	# o  ?D�D�D &0 displaysleepstate displaySleepState	$ o  DI�C�C 0 strawake strAwake�F  �E  	! k  Mb	%	% 	&	'	& r  MZ	(	)	( n  MV	*	+	* 1  RV�B
�B 
time	+ l MR	,�A�@	, I MR�?�>�=
�? .misccurdldt    ��� null�>  �=  �A  �@  	) o      �<�< 0 endtime endTime	' 	-	.	- r  [b	/	0	/ n [`	1	2	1 I  \`�;�:�9�; 0 getduration getDuration�:  �9  	2  f  [\	0 o      �8�8 0 duration  	. 	3	4	3 l cc�7	5	6�7  	5 � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   	6 �	7	7 d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "	4 	8	9	8 l cc�6	:	;�6  	:  delay 2   	; �	<	<  d e l a y   2	9 	=	>	= l cc�5�4�3�5  �4  �3  	> 	?	@	? r  cn	A	B	A n  cl	C	D	C 3  hl�2
�2 
cobj	D o  ch�1�1 $0 messagescomplete messagesComplete	B o      �0�0 0 msg  	@ 	E	F	E Z  o\	G	H�/�.	G l ov	I�-�,	I =  ov	J	K	J o  ot�+�+ 0 notifications  	K m  tu�*
�* boovtrue�-  �,  	H k  yX	L	L 	M	N	M Z  y"	O	P�)	Q	O l y~	R�(�'	R =  y~	S	T	S o  yz�&�& 0 duration  	T m  z}	U	U ?�      �(  �'  	P Z  ��	V	W	X�%	V l ��	Y�$�#	Y =  ��	Z	[	Z o  ���"�" 0 nsfw  	[ m  ���!
�! boovtrue�$  �#  	W k  ��	\	\ 	]	^	] I ��� 	_	`
�  .sysonotfnull��� ��� TEXT	_ b  ��	a	b	a b  ��	c	d	c b  ��	e	f	e m  ��	g	g �	h	h  A n d   i n  	f o  ���� 0 duration  	d m  ��	i	i �	j	j    m i n u t e ! 
	b o  ���� 0 msg  	` �	k�
� 
appr	k m  ��	l	l �	m	m : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  	^ 	n�	n I ���	o�
� .sysodelanull��� ��� nmbr	o m  ���� �  �  	X 	p	q	p l ��	r��	r =  ��	s	t	s o  ���� 0 nsfw  	t m  ���
� boovfals�  �  	q 	u�	u k  ��	v	v 	w	x	w I ���	y	z
� .sysonotfnull��� ��� TEXT	y b  ��	{	|	{ b  ��	}	~	} m  ��		 �	�	�  A n d   i n  	~ o  ���� 0 duration  	| m  ��	�	� �	�	�    m i n u t e ! 
	z �	��
� 
appr	� m  ��	�	� �	�	�  B a c k e d   u p !�  	x 	��	� I ���	��
� .sysodelanull��� ��� nmbr	� m  ���� �  �  �  �%  �)  	Q Z  �"	�	�	��
	� l ��	��	�	� =  ��	�	�	� o  ���� 0 nsfw  	� m  ���
� boovtrue�	  �  	� k  ��	�	� 	�	�	� I ���	�	�
� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ���� 0 duration  	� m  ��	�	� �	�	�    m i n u t e s ! 
	� o  ���� 0 msg  	� �	��
� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  	� 	�� 	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  �   	� 	�	�	� l �	�����	� =  �	�	�	� o  � ���� 0 nsfw  	� m   ��
�� boovfals��  ��  	� 	���	� k  	�	� 	�	�	� I ��	�	�
�� .sysonotfnull��� ��� TEXT	� b  	�	�	� b  
	�	�	� m  	�	� �	�	�  A n d   i n  	� o  	���� 0 duration  	� m  
	�	� �	�	�    m i n u t e s ! 
	� ��	���
�� 
appr	� m  	�	� �	�	�  B a c k e d   u p !��  	� 	���	� I ��	���
�� .sysodelanull��� ��� nmbr	� m  ���� ��  ��  ��  �
  	N 	�	�	� l ##��������  ��  ��  	� 	���	� Z  #X	�	�	���	� l #*	�����	� =  #*	�	�	� o  #(���� 0 nsfw  	� m  ()��
�� boovtrue��  ��  	� I -:��	�	�
�� .sysonotfnull��� ��� TEXT	� m  -0	�	� �	�	� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .	� ��	���
�� 
appr	� m  36	�	� �	�	� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  	� 	�	�	� l =D	�����	� =  =D	�	�	� o  =B���� 0 nsfw  	� m  BC��
�� boovfals��  ��  	� 	���	� I GT��	�	�
�� .sysonotfnull��� ��� TEXT	� m  GJ	�	� �	�	� p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d	� ��	���
�� 
appr	� m  MP	�	� �	�	� 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  �/  �.  	F 	�	�	� l ]]��������  ��  ��  	� 	���	� n ]b	�	�	� I  ^b�������� $0 resetmenubaricon resetMenuBarIcon��  ��  	�  f  ]^��  �H  �G  �I  �n  � k  ip	�	� 	�	�	� Z  iT	�	�����	� l it	�����	� E  it	�	�	� o  in���� &0 displaysleepstate displaySleepState	� o  ns���� 0 strawake strAwake��  ��  	� k  wP	�	� 	�	�	� r  w�	�	�	� n  w�	�	�	� 1  |���
�� 
time	� l w|	�����	� I w|������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 endtime endTime	� 	�	�	� r  ��	�	�	� n ��	�	�	� I  ���������� 0 getduration getDuration��  ��  	�  f  ��	� o      ���� 0 duration  	� 	�	�	� r  ��	�	�	� n  ��	�	�	� 3  ����
�� 
cobj	� o  ������ $0 messagescomplete messagesComplete	� o      ���� 0 msg  	� 	���	� Z  �P	�	�����	� l ��	�����	� =  ��	�	�	� o  ������ 0 notifications  	� m  ����
�� boovtrue��  ��  	� Z  �L	�	���
 	� l ��
����
 =  ��


 o  ������ 0 duration  
 m  ��

 ?�      ��  ��  	� Z  ��


��
 l ��
����
 =  ��
	


	 o  ������ 0 nsfw  

 m  ����
�� boovtrue��  ��  
 k  ��

 


 I ����


�� .sysonotfnull��� ��� TEXT
 b  ��


 b  ��


 b  ��


 m  ��

 �

  A n d   i n  
 o  ������ 0 duration  
 m  ��

 �

    m i n u t e ! 

 o  ������ 0 msg  
 ��
��
�� 
appr
 m  ��

 �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  
 

 
 l ��
!����
! =  ��
"
#
" o  ������ 0 nsfw  
# m  ����
�� boovfals��  ��  
  
$��
$ k  ��
%
% 
&
'
& I ����
(
)
�� .sysonotfnull��� ��� TEXT
( b  ��
*
+
* b  ��
,
-
, m  ��
.
. �
/
/  A n d   i n  
- o  ������ 0 duration  
+ m  ��
0
0 �
1
1    m i n u t e ! 

) ��
2��
�� 
appr
2 m  ��
3
3 �
4
4  B a c k e d   u p !��  
' 
5��
5 I ����
6��
�� .sysodelanull��� ��� nmbr
6 m  ������ ��  ��  ��  ��  ��  
  Z  �L
7
8
9��
7 l �
:����
: =  �
;
<
; o  ����� 0 nsfw  
< m  ��
�� boovtrue��  ��  
8 k  "
=
= 
>
?
> I ��
@
A
�� .sysonotfnull��� ��� TEXT
@ b  
B
C
B b  
D
E
D b  
F
G
F m  

H
H �
I
I  A n d   i n  
G o  
���� 0 duration  
E m  
J
J �
K
K    m i n u t e s ! 

C o  ���� 0 msg  
A ��
L��
�� 
appr
L m  
M
M �
N
N : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
? 
O��
O I "��
P��
�� .sysodelanull��� ��� nmbr
P m  ���� ��  ��  
9 
Q
R
Q l %,
S����
S =  %,
T
U
T o  %*���� 0 nsfw  
U m  *+��
�� boovfals��  ��  
R 
V��
V k  /H
W
W 
X
Y
X I /B��
Z
[
�� .sysonotfnull��� ��� TEXT
Z b  /8
\
]
\ b  /4
^
_
^ m  /2
`
` �
a
a  A n d   i n  
_ o  23���� 0 duration  
] m  47
b
b �
c
c    m i n u t e s ! 

[ ��
d��
�� 
appr
d m  ;>
e
e �
f
f  B a c k e d   u p !��  
Y 
g��
g I CH��
h��
�� .sysodelanull��� ��� nmbr
h m  CD���� ��  ��  ��  ��  ��  ��  ��  ��  ��  	� 
i
j
i l UU��~�}�  �~  �}  
j 
k
l
k n UZ
m
n
m I  VZ�|�{�z�| $0 resetmenubaricon resetMenuBarIcon�{  �z  
n  f  UV
l 
o
p
o l [[�y�x�w�y  �x  �w  
p 
q�v
q Z  [p
r
s�u�t
r l [b
t�s�r
t =  [b
u
v
u o  [`�q�q  0 backupthenquit backupThenQuit
v m  `a�p
�p boovtrue�s  �r  
s I el�o
w�n
�o .aevtquitnull��� ��� null
w m  eh
x
x                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �n  �u  �t  �v  �o  ��  ��  ��  | m   $ %
y
y�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  z 
z
{
z l vv�m�l�k�m  �l  �k  
{ 
|�j
| I  v~�i
}�h�i 0 	stopwatch  
} 
~�g
~ m  wz

 �
�
�  f i n i s h�g  �h  �j  S 
�
�
� l     �f�e�d�f  �e  �d  
� 
�
�
� l     �c�b�a�c  �b  �a  
� 
�
�
� l     �`�_�^�`  �_  �^  
� 
�
�
� l     �]�\�[�]  �\  �[  
� 
�
�
� l     �Z�Y�X�Z  �Y  �X  
� 
�
�
� l     �W
�
��W  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �V
�
��V  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �U
�
��U  
� � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   
� �
�
� - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �T�S�R�T  �S  �R  
� 
�
�
� l     �Q
�
��Q  
� � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   
� �
�
�   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .
� 
�
�
� l     �P
�
��P  
� � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   
� �
�
��   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .
� 
�
�
� i  `c
�
�
� I      �O
��N�O 0 itexists itExists
� 
�
�
� o      �M�M 0 
objecttype 
objectType
� 
��L
� o      �K�K 
0 object  �L  �N  
� l    W
�
�
�
� O     W
�
�
� Z    V
�
�
��J
� l   
��I�H
� =    
�
�
� o    �G�G 0 
objecttype 
objectType
� m    
�
� �
�
�  d i s k�I  �H  
� Z   
 
�
��F
�
� I  
 �E
��D
�E .coredoexnull���     ****
� 4   
 �C
�
�C 
cdis
� o    �B�B 
0 object  �D  
� L    
�
� m    �A
�A boovtrue�F  
� L    
�
� m    �@
�@ boovfals
� 
�
�
� l   "
��?�>
� =    "
�
�
� o     �=�= 0 
objecttype 
objectType
� m     !
�
� �
�
�  f i l e�?  �>  
� 
�
�
� Z   % 7
�
��<
�
� I  % -�;
��:
�; .coredoexnull���     ****
� 4   % )�9
�
�9 
file
� o   ' (�8�8 
0 object  �:  
� L   0 2
�
� m   0 1�7
�7 boovtrue�<  
� L   5 7
�
� m   5 6�6
�6 boovfals
� 
�
�
� l  : =
��5�4
� =   : =
�
�
� o   : ;�3�3 0 
objecttype 
objectType
� m   ; <
�
� �
�
�  f o l d e r�5  �4  
� 
��2
� Z   @ R
�
��1
�
� I  @ H�0
��/
�0 .coredoexnull���     ****
� 4   @ D�.
�
�. 
cfol
� o   B C�-�- 
0 object  �/  
� L   K M
�
� m   K L�,
�, boovtrue�1  
� L   P R
�
� m   P Q�+
�+ boovfals�2  �J  
� m     
�
��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  
� "  (string, string) as Boolean   
� �
�
� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n
� 
�
�
� l     �*�)�(�*  �)  �(  
� 
�
�
� l     �'�&�%�'  �&  �%  
� 
�
�
� l     �$�#�"�$  �#  �"  
� 
�
�
� l     �!� ��!  �   �  
� 
�
�
� l     ����  �  �  
� 
�
�
� l     �
�
��  
� � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   
� �
�
�Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .
� 
�
�
� l     �
�
��  
� P J This means that backups can be identified from what machine they're from.   
� �
�
� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .
� 
�
�
� i  dg
�
�
� I      ���� .0 getcomputeridentifier getComputerIdentifier�  �  
� k     
�
� 
�
�
� r     	
�
�
� n        1    �
� 
sicn l    �� I    ���
� .sysosigtsirr   ��� null�  �  �  �  
� o      �� 0 computername computerName
�  r   
  I  
 ��
� .sysoexecTEXT���     TEXT m   
  �		 � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �   o      �� "0 strserialnumber strSerialNumber 

 r     l   �� b     b     o    �
�
 0 computername computerName m     �    -   o    �	�	 "0 strserialnumber strSerialNumber�  �   o      ��  0 identifiername identifierName � L     o    ��  0 identifiername identifierName�  
�  l     ����  �  �    l     ��� �  �  �     l     ��������  ��  ��    l     ��������  ��  ��     l     ��������  ��  ��    !"! l     ��#$��  # � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   $ �%%   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' ." &'& l     ��()��  ( � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   ) �**   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .' +,+ i  hk-.- I      ��/���� 0 gettimestamp getTimestamp/ 0��0 o      ���� 0 isfolder isFolder��  ��  . l   �1231 k    �44 565 l     ��78��  7   Date variables   8 �99    D a t e   v a r i a b l e s6 :;: r     )<=< l     >����> I     ������
�� .misccurdldt    ��� null��  ��  ��  ��  = K    ?? ��@A
�� 
year@ o    ���� 0 y  A ��BC
�� 
mnthB o    ���� 0 m  C ��DE
�� 
day D o    ���� 0 d  E ��F��
�� 
timeF o   	 
���� 0 t  ��  ; GHG r   * 1IJI c   * /KLK l  * -M����M c   * -NON o   * +���� 0 y  O m   + ,��
�� 
long��  ��  L m   - .��
�� 
TEXTJ o      ���� 0 ty tYH PQP r   2 9RSR c   2 7TUT l  2 5V����V c   2 5WXW o   2 3���� 0 y  X m   3 4��
�� 
long��  ��  U m   5 6��
�� 
TEXTS o      ���� 0 ty tYQ YZY r   : A[\[ c   : ?]^] l  : =_����_ c   : =`a` o   : ;���� 0 m  a m   ; <��
�� 
long��  ��  ^ m   = >��
�� 
TEXT\ o      ���� 0 tm tMZ bcb r   B Ided c   B Gfgf l  B Eh����h c   B Eiji o   B C���� 0 d  j m   C D��
�� 
long��  ��  g m   E F��
�� 
TEXTe o      ���� 0 td tDc klk r   J Qmnm c   J Oopo l  J Mq����q c   J Mrsr o   J K���� 0 t  s m   K L��
�� 
long��  ��  p m   M N��
�� 
TEXTn o      ���� 0 tt tTl tut l  R R��������  ��  ��  u vwv l  R R��xy��  x U O Append the month or day with a 0 if the string length is only 1 character long   y �zz �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n gw {|{ r   R [}~} c   R Y� l  R W������ I  R W�����
�� .corecnte****       ****� o   R S���� 0 tm tM��  ��  ��  � m   W X��
�� 
nmbr~ o      ���� 
0 tml tML| ��� r   \ e��� c   \ c��� l  \ a������ I  \ a�����
�� .corecnte****       ****� o   \ ]���� 0 tm tM��  ��  ��  � m   a b��
�� 
nmbr� o      ���� 
0 tdl tDL� ��� l  f f��������  ��  ��  � ��� Z   f u������� l  f i������ =   f i��� o   f g���� 
0 tml tML� m   g h���� ��  ��  � r   l q��� b   l o��� m   l m�� ���  0� o   m n���� 0 tm tM� o      ���� 0 tm tM��  ��  � ��� l  v v��������  ��  ��  � ��� Z   v �������� l  v y������ =   v y��� o   v w���� 
0 tdl tDL� m   w x���� ��  ��  � r   | ���� b   | ���� m   | �� ���  0� o    ����� 0 td tD� o      ���� 0 td tD��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  �   Time variables	   � ���     T i m e   v a r i a b l e s 	� ��� l  � �������  �  	 Get hour   � ���    G e t   h o u r� ��� r   � ���� n   � ���� 1   � ���
�� 
tstr� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 timestr timeStr� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ��� ��  ��  � o   � ��~�~ 0 timestr timeStr� m   � ��}
�} 
TEXT� o      �|�| 0 h  � ��� r   � ���� c   � ���� n   � ���� 7 � ��{��
�{ 
cha � l  � ���z�y� [   � ���� o   � ��x�x 0 pos  � m   � ��w�w �z  �y  �  ;   � �� o   � ��v�v 0 timestr timeStr� m   � ��u
�u 
TEXT� o      �t�t 0 timestr timeStr� ��� l  � ��s�r�q�s  �r  �q  � ��� l  � ��p���p  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ���o�� z�n�m
�n .sysooffslong    ��� null
�m misccura�o  � �l��
�l 
psof� m   � ��� ���  :� �k��j
�k 
psin� o   � ��i�i 0 timestr timeStr�j  � o      �h�h 0 pos  � ��� r   ���� c   ���� n   � ��� 7  � �g��
�g 
cha � m   � ��f�f � l  � ���e�d� \   � �   o   � ��c�c 0 pos   m   � ��b�b �e  �d  � o   � ��a�a 0 timestr timeStr� m   �`
�` 
TEXT� o      �_�_ 0 m  �  r   c   n  	 7�^

�^ 
cha 
 l �]�\ [   o  �[�[ 0 pos   m  �Z�Z �]  �\    ;  	 o  �Y�Y 0 timestr timeStr m  �X
�X 
TEXT o      �W�W 0 timestr timeStr  l �V�U�T�V  �U  �T    l �S�S     Get AM or PM    �    G e t   A M   o r   P M  r  2 I 0�R z�Q�P
�Q .sysooffslong    ��� null
�P misccura�R   �O
�O 
psof m  "% �    �N �M
�N 
psin  o  ()�L�L 0 timestr timeStr�M   o      �K�K 0 pos   !"! r  3E#$# c  3C%&% n  3A'(' 74A�J)*
�J 
cha ) l :>+�I�H+ [  :>,-, o  ;<�G�G 0 pos  - m  <=�F�F �I  �H  *  ;  ?@( o  34�E�E 0 timestr timeStr& m  AB�D
�D 
TEXT$ o      �C�C 0 s  " ./. l FF�B�A�@�B  �A  �@  / 010 l FF�?�>�=�?  �>  �=  1 232 Z  F456�<4 l FI7�;�:7 =  FI898 o  FG�9�9 0 isfolder isFolder9 m  GH�8
�8 boovtrue�;  �:  5 k  La:: ;<; l LL�7=>�7  =   Create folder timestamp   > �?? 0   C r e a t e   f o l d e r   t i m e s t a m p< @�6@ r  LaABA b  L_CDC b  L]EFE b  L[GHG b  LYIJI b  LUKLK b  LSMNM b  LQOPO m  LOQQ �RR  b a c k u p _P o  OP�5�5 0 ty tYN o  QR�4�4 0 tm tML o  ST�3�3 0 td tDJ m  UXSS �TT  _H o  YZ�2�2 0 h  F o  [\�1�1 0 m  D o  ]^�0�0 0 s  B o      �/�/ 0 	timestamp  �6  6 UVU l dgW�.�-W =  dgXYX o  de�,�, 0 isfolder isFolderY m  ef�+
�+ boovfals�.  �-  V Z�*Z k  j{[[ \]\ l jj�)^_�)  ^   Create timestamp   _ �`` "   C r e a t e   t i m e s t a m p] a�(a r  j{bcb b  jyded b  jwfgf b  juhih b  jsjkj b  jolml b  jmnon o  jk�'�' 0 ty tYo o  kl�&�& 0 tm tMm o  mn�%�% 0 td tDk m  orpp �qq  _i o  st�$�$ 0 h  g o  uv�#�# 0 m  e o  wx�"�" 0 s  c o      �!�! 0 	timestamp  �(  �*  �<  3 rsr l ��� ���   �  �  s t�t L  ��uu o  ���� 0 	timestamp  �  2  	(boolean)   3 �vv  ( b o o l e a n ), wxw l     ����  �  �  x yzy l     ����  �  �  z {|{ l     ����  �  �  | }~} l     ����  �  �  ~ � l     ����  �  �  � ��� l     ����  � q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   � ��� �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .� ��� l     ����  � w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   � ��� �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .� ��� i  lo��� I      �
��	�
 0 comparesizes compareSizes� ��� o      �� 
0 source  � ��� o      �� 0 destination  �  �	  � l   ���� k    �� ��� r     ��� m     �
� boovtrue� o      �� 0 fit  � ��� r    
��� 4    ��
� 
psxf� o    �� 0 destination  � o      �� 0 destination  � ��� r    ��� b    ��� b    ��� b    ��� o    � �  "0 destinationdisk destinationDisk� m    �� ���  B a c k u p s /� o    ���� 0 machinefolder machineFolder� m    �� ���  / L a t e s t� o      ���� 0 
templatest 
tempLatest� ��� r    ��� m    ����  � o      ���� $0 latestfoldersize latestFolderSize� ��� r    ��� m    �� ?�      � o      ���� 
0 buffer  � ��� l   ��������  ��  ��  � ��� O    ���� k   ! ��� ��� r   ! ,��� I  ! *�����
�� .sysoexecTEXT���     TEXT� b   ! &��� b   ! $��� m   ! "�� ���  d u   - m s  � o   " #���� 
0 source  � m   $ %�� ���    |   c u t   - f   1��  � o      ���� 0 
foldersize 
folderSize� ��� r   - ?��� ^   - =��� l  - ;������ I  - ;����� z����
�� .sysorondlong        doub
�� misccura� ]   1 6��� l  1 4������ ^   1 4��� o   1 2���� 0 
foldersize 
folderSize� m   2 3���� ��  ��  � m   4 5���� d��  ��  ��  � m   ; <���� d� o      ���� 0 
foldersize 
folderSize� ��� l  @ @��������  ��  ��  � ��� r   @ N��� ^   @ L��� ^   @ J��� ^   @ H��� l  @ F������ l  @ F������ n   @ F��� 1   D F��
�� 
frsp� 4   @ D���
�� 
cdis� o   B C���� 0 destination  ��  ��  ��  ��  � m   F G���� � m   H I���� � m   J K���� � o      ���� 0 	freespace 	freeSpace� ��� r   O _��� ^   O ]��� l  O [������ I  O [����� z����
�� .sysorondlong        doub
�� misccura� l  S V������ ]   S V��� o   S T���� 0 	freespace 	freeSpace� m   T U���� d��  ��  ��  ��  ��  � m   [ \���� d� o      ���� 0 	freespace 	freeSpace� ��� l  ` `��������  ��  ��  � ��� I  ` g�����
�� .ascrcmnt****      � ****� l  ` c������ b   ` c��� o   ` a���� 0 
foldersize 
folderSize� o   a b���� 0 	freespace 	freeSpace��  ��  ��  � ��� l  h h��������  ��  ��  �    Z   h ����� l  h o���� =   h o o   h m���� 0 initialbackup initialBackup m   m n��
�� boovfals��  ��   Z   r ����� l  r }	����	 =   r }

 I   r {������ 0 itexists itExists  m   s v �  f o l d e r �� o   v w���� 0 
templatest 
tempLatest��  ��   m   { |��
�� boovtrue��  ��   k   � �  r   � � I  � �����
�� .sysoexecTEXT���     TEXT b   � � b   � � m   � � �  d u   - m s   ' o   � ����� 0 
templatest 
tempLatest m   � � �  '   |   c u t   - f   1��   o      ���� $0 latestfoldersize latestFolderSize  !  r   � �"#" ^   � �$%$ l  � �&����& I  � �'(��' z����
�� .sysorondlong        doub
�� misccura( ]   � �)*) l  � �+����+ ^   � �,-, o   � ����� $0 latestfoldersize latestFolderSize- m   � ����� ��  ��  * m   � ����� d��  ��  ��  % m   � ����� d# o      ���� $0 latestfoldersize latestFolderSize! ./. l  � ���������  ��  ��  / 0��0 I  � ���1��
�� .ascrcmnt****      � ****1 l  � �2����2 b   � �343 b   � �565 o   � ����� 0 
foldersize 
folderSize6 o   � ����� $0 latestfoldersize latestFolderSize4 o   � ����� 0 	freespace 	freeSpace��  ��  ��  ��  ��  ��  ��  ��   7��7 l  � ���������  ��  ��  ��  � m    88�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 9:9 l  � ���������  ��  ��  : ;<; Z   �=>?��= l  � �@����@ =   � �ABA o   � ����� 0 initialbackup initialBackupB m   � ���
�� boovfals��  ��  > k   � �CC DED r   � �FGF l  � �H����H \   � �IJI o   � ����� $0 latestfoldersize latestFolderSizeJ o   � ����� 0 
foldersize 
folderSize��  ��  G o      ���� 0 tempsize tempSizeE K��K Z   � �LM����L l  � �N����N A   � �OPO o   � ����� 0 tempsize tempSizeP m   � �����  ��  ��  M k   � �QQ RSR l  � ���TU��  T $ if (tempSize > freeSpace) then   U �VV < i f   ( t e m p S i z e   >   f r e e S p a c e )   t h e nS W�W Z   � �XY�~�}X l  � �Z�|�{Z A   � �[\[ o   � ��z�z 0 tempsize tempSize\ l  � �]�y�x] [   � �^_^ o   � ��w�w 0 	freespace 	freeSpace_ o   � ��v�v 
0 buffer  �y  �x  �|  �{  Y r   � �`a` m   � ��u
�u boovfalsa o      �t�t 0 fit  �~  �}  �  ��  ��  ��  ? bcb l  � �d�s�rd =   � �efe o   � ��q�q 0 initialbackup initialBackupf m   � ��p
�p boovtrue�s  �r  c g�og k   � �hh iji l  � ��nkl�n  k &  if (folderSize > freeSpace) then   l �mm @ i f   ( f o l d e r S i z e   >   f r e e S p a c e )   t h e nj n�mn Z   � �op�l�ko l  � �q�j�iq A   � �rsr o   � ��h�h 0 
foldersize 
folderSizes l  � �t�g�ft [   � �uvu o   � ��e�e 0 	freespace 	freeSpacev o   � ��d�d 
0 buffer  �g  �f  �j  �i  p r   � �wxw m   � ��c
�c boovfalsx o      �b�b 0 fit  �l  �k  �m  �o  ��  < yzy l �a�`�_�a  �`  �_  z {�^{ L  || o  �]�] 0 fit  �^  �  (string, string)   � �}}   ( s t r i n g ,   s t r i n g )� ~~ l     �\�[�Z�\  �[  �Z   ��� l     �Y�X�W�Y  �X  �W  � ��� l     �V�U�T�V  �U  �T  � ��� l     �S�R�Q�S  �R  �Q  � ��� l     �P�O�N�P  �O  �N  � ��� l     �M���M  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i  ps��� I      �L��K�L 0 	stopwatch  � ��J� o      �I�I 0 mode  �J  �K  � l    5���� k     5�� ��� q      �� �H�G�H 0 x  �G  � ��� Z     3����F� l    ��E�D� =     ��� o     �C�C 0 mode  � m    �� ��� 
 s t a r t�E  �D  � k    �� ��� r    ��� I    �B��A�B 0 gettimestamp getTimestamp� ��@� m    �?
�? boovfals�@  �A  � o      �>�> 0 x  � ��=� I   �<��;
�< .ascrcmnt****      � ****� l   ��:�9� b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    �8�8 0 x  �:  �9  �;  �=  � ��� l   ��7�6� =    ��� o    �5�5 0 mode  � m    �� ���  f i n i s h�7  �6  � ��4� k    /�� ��� r    '��� I    %�3��2�3 0 gettimestamp getTimestamp� ��1� m     !�0
�0 boovfals�1  �2  � o      �/�/ 0 x  � ��.� I  ( /�-��,
�- .ascrcmnt****      � ****� l  ( +��+�*� b   ( +��� m   ( )�� ��� " b a c k u p   f i n i s h e d :  � o   ) *�)�) 0 x  �+  �*  �,  �.  �4  �F  � ��(� l  4 4�'�&�%�'  �&  �%  �(  �  (string)   � ���  ( s t r i n g )� ��� l     �$�#�"�$  �#  �"  � ��� l     �!� ��!  �   �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  � M G Utility function to get the duration of the backup process in minutes.   � ��� �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .� ��� i  tw��� I      ���� 0 getduration getDuration�  �  � k     �� ��� r     ��� ^     ��� l    ���� \     ��� o     �� 0 endtime endTime� o    �� 0 	starttime 	startTime�  �  � m    �� <� o      �� 0 duration  � ��� r    ��� ^    ��� l   ���
� I   ���	� z��
� .sysorondlong        doub
� misccura� l   ���� ]    ��� o    �� 0 duration  � m    �� d�  �  �	  �  �
  � m    �� d� o      �� 0 duration  � �� � L    �� o    ���� 0 duration  �   � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � � � l     ����   ; 5 Utility function bring WBTFU to the front with focus    � j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s   i  x{ I      �������� 0 setfocus setFocus��  ��   O     
	 I   	������
�� .miscactvnull��� ��� null��  ��  	 m     

                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��    l     ��������  ��  ��    l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��     l     ��������  ��  ��    !"! l     ��#$��  # � �-----------------------------------------------------------------------------------------------------------------------------------------------   $ �%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" &'& l     ��()��  ( � �-----------------------------------------------------------------------------------------------------------------------------------------------   ) �** - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -' +,+ l     ��-.��  - � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   . �// - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -, 010 l     ��������  ��  ��  1 232 i  |454 I      ��6���� $0 menuneedsupdate_ menuNeedsUpdate_6 7��7 l     8����8 m      ��
�� 
cmnu��  ��  ��  ��  5 k     99 :;: l     ��<=��  < J D NSMenu's delegates method, when the menu is clicked this is called.   = �>> �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .; ?@? l     ��AB��  A j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   B �CC �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .@ DED l     ��FG��  F < 6 This means the menu items can be changed dynamically.   G �HH l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .E I��I n    JKJ I    �������� 0 	makemenus 	makeMenus��  ��  K  f     ��  3 LML l     ��������  ��  ��  M NON l     ��������  ��  ��  O PQP l     ��������  ��  ��  Q RSR l     ��������  ��  ��  S TUT l     ��������  ��  ��  U VWV i  ��XYX I      �������� 0 	makemenus 	makeMenus��  ��  Y k    >ZZ [\[ l    	]^_] n    	`a` I    	��������  0 removeallitems removeAllItems��  ��  a o     ���� 0 newmenu newMenu^ !  remove existing menu items   _ �bb 6   r e m o v e   e x i s t i n g   m e n u   i t e m s\ cdc l  
 
��������  ��  ��  d e��e Y   
>f��gh��f k   9ii jkj r    'lml n    %non 4   " %��p
�� 
cobjp o   # $���� 0 i  o o    "���� 0 thelist theListm o      ���� 0 	this_item  k qrq l  ( (��������  ��  ��  r sts Z   ( uvw��u l  ( -x����x =   ( -yzy l  ( +{����{ c   ( +|}| o   ( )���� 0 	this_item  } m   ) *��
�� 
TEXT��  ��  z m   + ,~~ �  B a c k u p   n o w��  ��  v r   0 @��� l  0 >������ n  0 >��� I   7 >������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8���� 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ���� m   9 :�� ���  ��  ��  � n  0 7��� I   3 7�������� 	0 alloc  ��  ��  � n  0 3��� o   1 3���� 0 
nsmenuitem 
NSMenuItem� m   0 1��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItemw ��� l  C H������ =   C H��� l  C F������ c   C F��� o   C D���� 0 	this_item  � m   D E��
�� 
TEXT��  ��  � m   F G�� ��� " B a c k u p   n o w   &   q u i t��  ��  � ��� r   K [��� l  K Y������ n  K Y��� I   R Y������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S���� 0 	this_item  � ��� m   S T�� ��� * b a c k u p A n d Q u i t H a n d l e r :� ���� m   T U�� ���  ��  ��  � n  K R��� I   N R��~�}� 	0 alloc  �~  �}  � n  K N��� o   L N�|�| 0 
nsmenuitem 
NSMenuItem� m   K L�{
�{ misccura��  ��  � o      �z�z 0 thismenuitem thisMenuItem� ��� l  ^ c��y�x� =   ^ c��� l  ^ a��w�v� c   ^ a��� o   ^ _�u�u 0 	this_item  � m   _ `�t
�t 
TEXT�w  �v  � m   a b�� ��� " T u r n   o n   t h e   b a n t s�y  �x  � ��� r   f x��� l  f v��s�r� n  f v��� I   m v�q��p�q J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n�o�o 0 	this_item  � ��� m   n o�� ���  n s f w O n H a n d l e r :� ��n� m   o r�� ���  �n  �p  � n  f m��� I   i m�m�l�k�m 	0 alloc  �l  �k  � n  f i��� o   g i�j�j 0 
nsmenuitem 
NSMenuItem� m   f g�i
�i misccura�s  �r  � o      �h�h 0 thismenuitem thisMenuItem� ��� l  { ���g�f� =   { ���� l  { ~��e�d� c   { ~��� o   { |�c�c 0 	this_item  � m   | }�b
�b 
TEXT�e  �d  � m   ~ ��� ��� $ T u r n   o f f   t h e   b a n t s�g  �f  � ��� r   � ���� l  � ���a�`� n  � ���� I   � ��_��^�_ J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ��]�] 0 	this_item  � ��� m   � ��� ���  n s f w O f f H a n d l e r :� ��\� m   � ��� ���  �\  �^  � n  � ���� I   � ��[�Z�Y�[ 	0 alloc  �Z  �Y  � n  � ���� o   � ��X�X 0 
nsmenuitem 
NSMenuItem� m   � ��W
�W misccura�a  �`  � o      �V�V 0 thismenuitem thisMenuItem� ��� l  � ���U�T� =   � ���� l  � ���S�R� c   � ���� o   � ��Q�Q 0 	this_item  � m   � ��P
�P 
TEXT�S  �R  � m   � ��� ��� * T u r n   o n   n o t i f i c a t i o n s�U  �T  � ��� r   � ���� l  � ���O�N� n  � �� � I   � ��M�L�M J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   � ��K�K 0 	this_item    m   � � � , n o t i f i c a t i o n O n H a n d l e r : �J m   � �		 �

  �J  �L    n  � � I   � ��I�H�G�I 	0 alloc  �H  �G   n  � � o   � ��F�F 0 
nsmenuitem 
NSMenuItem m   � ��E
�E misccura�O  �N  � o      �D�D 0 thismenuitem thisMenuItem�  l  � ��C�B =   � � l  � ��A�@ c   � � o   � ��?�? 0 	this_item   m   � ��>
�> 
TEXT�A  �@   m   � � � , T u r n   o f f   n o t i f i c a t i o n s�C  �B    r   � � l  � ��=�< n  � � I   � ��; �:�; J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  !"! o   � ��9�9 0 	this_item  " #$# m   � �%% �&& . n o t i f i c a t i o n O f f H a n d l e r :$ '�8' m   � �(( �))  �8  �:   n  � �*+* I   � ��7�6�5�7 	0 alloc  �6  �5  + n  � �,-, o   � ��4�4 0 
nsmenuitem 
NSMenuItem- m   � ��3
�3 misccura�=  �<   o      �2�2 0 thismenuitem thisMenuItem ./. l  � �0�1�00 =   � �121 l  � �3�/�.3 c   � �454 o   � ��-�- 0 	this_item  5 m   � ��,
�, 
TEXT�/  �.  2 m   � �66 �77  Q u i t�1  �0  / 8�+8 r   � �9:9 l  � �;�*�); n  � �<=< I   � ��(>�'�( J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_> ?@? o   � ��&�& 0 	this_item  @ ABA m   � �CC �DD  q u i t H a n d l e r :B E�%E m   � �FF �GG  �%  �'  = n  � �HIH I   � ��$�#�"�$ 	0 alloc  �#  �"  I n  � �JKJ o   � ��!�! 0 
nsmenuitem 
NSMenuItemK m   � �� 
�  misccura�*  �)  : o      �� 0 thismenuitem thisMenuItem�+  ��  t LML l ����  �  �  M NON l P��P n QRQ I  �S�� 0 additem_ addItem_S T�T o  �� 0 thismenuitem thisMenuItem�  �  R o  �� 0 newmenu newMenu�  �  O UVU l ����  �  �  V WXW l YZ[Y l \��\ n ]^] I  �_�� 0 
settarget_ 
setTarget__ `�`  f  �  �  ^ o  �� 0 thismenuitem thisMenuItem�  �  Z * $ required for enabling the menu item   [ �aa H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e mX bcb l ��
�	�  �
  �	  c d�d Z  9ef��e G  "ghg l i��i =  jkj o  �� 0 i  k m  �� �  �  h l l�� l =  mnm o  ���� 0 i  n m  ���� �  �   f l %5opqo l %5r����r n %5sts I  *5��u���� 0 additem_ addItem_u v��v l *1w����w n *1xyx o  -1���� 0 separatoritem separatorItemy n *-z{z o  +-���� 0 
nsmenuitem 
NSMenuItem{ m  *+��
�� misccura��  ��  ��  ��  t o  %*���� 0 newmenu newMenu��  ��  p   add a seperator   q �||     a d d   a   s e p e r a t o r�  �  �  �� 0 i  g m    ���� h n    }~} m    ��
�� 
nmbr~ n   � 2   ��
�� 
cobj� o    ���� 0 thelist theList��  ��  W ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������  0 backuphandler_ backupHandler_� ���� o      ���� 
0 sender  ��  ��  � Z     ������� l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � k   
 U�� ��� r   
 ��� m   
 ��
�� boovtrue� o      ���� 0 manualbackup manualBackup� ���� Z    U������� l   ������ =    ��� o    ���� 0 notifications  � m    ��
�� boovtrue��  ��  � Z    Q������ l   #������ =    #��� o    !���� 0 nsfw  � m   ! "��
�� boovtrue��  ��  � k   & 3�� ��� I  & -����
�� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �����
�� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  . 3�����
�� .sysodelanull��� ��� nmbr� m   . /���� ��  ��  � ��� l  6 =������ =   6 =��� o   6 ;���� 0 nsfw  � m   ; <��
�� boovfals��  ��  � ���� k   @ M�� ��� I  @ G����
�� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� �����
�� 
appr� m   B C�� ��� 
 W B T F U��  � ���� I  H M�����
�� .sysodelanull��� ��� nmbr� m   H I���� ��  ��  ��  ��  ��  ��  ��  � ��� l  X _������ =   X _��� o   X ]���� 0 isbackingup isBackingUp� m   ] ^��
�� boovtrue��  ��  � ���� k   b ��� ��� r   b k��� n   b i��� 3   g i��
�� 
cobj� o   b g���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   l �������� l  l s������ =   l s��� o   l q���� 0 notifications  � m   q r��
�� boovtrue��  ��  � Z   v ������� l  v }������ =   v }��� o   v {���� 0 nsfw  � m   { |��
�� boovtrue��  ��  � I  � �����
�� .sysonotfnull��� ��� TEXT� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� .0 backupandquithandler_ backupAndQuitHandler_�  ��  o      ���� 
0 sender  ��  ��  � Z     ��� l    ���� =      o     �� 0 isbackingup isBackingUp m    �~
�~ boovfals��  ��   k   
 � 	 r   
 

 l  
 �}�| n   
  1    �{
�{ 
bhit l  
 �z�y I  
 �x
�x .sysodlogaskr        TEXT m   
  � T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ? �w
�w 
btns J      m     �  Y e s �v m     �  N o�v   �u�t
�u 
dflt m    �s�s �t  �z  �y  �}  �|   o      �r�r 	0 reply  	   l   �q�p�o�q  �p  �o    !�n! Z    �"#$�m" l   %�l�k% =    &'& o    �j�j 	0 reply  ' m    (( �))  Y e s�l  �k  # k   ! t** +,+ r   ! (-.- m   ! "�i
�i boovtrue. o      �h�h  0 backupthenquit backupThenQuit, /0/ r   ) 0121 m   ) *�g
�g boovtrue2 o      �f�f 0 manualbackup manualBackup0 343 l  1 1�e�d�c�e  �d  �c  4 5�b5 Z   1 t67�a�`6 l  1 88�_�^8 =   1 89:9 o   1 6�]�] 0 notifications  : m   6 7�\
�\ boovtrue�_  �^  7 Z   ; p;<=�[; l  ; B>�Z�Y> =   ; B?@? o   ; @�X�X 0 nsfw  @ m   @ A�W
�W boovtrue�Z  �Y  < k   E RAA BCB I  E L�VDE
�V .sysonotfnull��� ��� TEXTD m   E FFF �GG T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u pE �UH�T
�U 
apprH m   G HII �JJ 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�T  C K�SK I  M R�RL�Q
�R .sysodelanull��� ��� nmbrL m   M N�P�P �Q  �S  = MNM l  U \O�O�NO =   U \PQP o   U Z�M�M 0 nsfw  Q m   Z [�L
�L boovfals�O  �N  N R�KR k   _ lSS TUT I  _ f�JVW
�J .sysonotfnull��� ��� TEXTV m   _ `XX �YY   P r e p a r i n g   b a c k u pW �IZ�H
�I 
apprZ m   a b[[ �\\ 
 W B T F U�H  U ]�G] I  g l�F^�E
�F .sysodelanull��� ��� nmbr^ m   g h�D�D �E  �G  �K  �[  �a  �`  �b  $ _`_ l  w |a�C�Ba =   w |bcb o   w x�A�A 	0 reply  c m   x {dd �ee  N o�C  �B  ` f�@f r    �ghg m    ��?
�? boovfalsh o      �>�>  0 backupthenquit backupThenQuit�@  �m  �n   iji l  � �k�=�<k =   � �lml o   � ��;�; 0 isbackingup isBackingUpm m   � ��:
�: boovtrue�=  �<  j n�9n k   � �oo pqp r   � �rsr n   � �tut 3   � ��8
�8 
cobju o   � ��7�7 40 messagesalreadybackingup messagesAlreadyBackingUps o      �6�6 0 msg  q v�5v Z   � �wx�4�3w l  � �y�2�1y =   � �z{z o   � ��0�0 0 notifications  { m   � ��/
�/ boovtrue�2  �1  x Z   � �|}~�.| l  � ��-�, =   � ���� o   � ��+�+ 0 nsfw  � m   � ��*
�* boovtrue�-  �,  } I  � ��)��
�) .sysonotfnull��� ��� TEXT� o   � ��(�( 0 msg  � �'��&
�' 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�&  ~ ��� l  � ���%�$� =   � ���� o   � ��#�# 0 nsfw  � m   � ��"
�" boovfals�%  �$  � ��!� I  � �� ��
�  .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� ���
� 
appr� m   � ��� ��� 
 W B T F U�  �!  �.  �4  �3  �5  �9  ��  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i  ����� I      ����  0 nsfwonhandler_ nsfwOnHandler_� ��� o      �� 
0 sender  �  �  � k     =�� ��� r     ��� m     �

�
 boovtrue� o      �	�	 0 nsfw  � ��� Z    =����� l   ���� =    ��� o    �� 0 notifications  � m    �
� boovtrue�  �  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��� m    �� ���  Q u i t�  � o      �� 0 thelist theList� ��� l  " )�� ��� =   " )��� o   " '���� 0 notifications  � m   ' (��
�� boovfals�   ��  � ���� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   0 1�� ���  Q u i t��  � o      ���� 0 thelist theList��  �  �  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� "0 nsfwoffhandler_ nsfwOffHandler_� ���� o      ���� 
0 sender  ��  ��  � k     =�� ��� r     ��� m     ��
�� boovfals� o      ���� 0 nsfw  � ���� Z    =������ l   ������ =    ��� o    ���� 0 notifications  � m    ��
�� boovtrue��  ��  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� �   " B a c k u p   n o w   &   q u i t�  m     � " T u r n   o n   t h e   b a n t s  m     � , T u r n   o f f   n o t i f i c a t i o n s 	��	 m    

 �  Q u i t��  � o      ���� 0 thelist theList�  l  " )���� =   " ) o   " '���� 0 notifications   m   ' (��
�� boovfals��  ��   �� r   , 9 J   , 3  m   , - �  B a c k u p   n o w  m   - . � " B a c k u p   n o w   &   q u i t  m   . / �   " T u r n   o n   t h e   b a n t s !"! m   / 0## �$$ * T u r n   o n   n o t i f i c a t i o n s" %��% m   0 1&& �''  Q u i t��   o      ���� 0 thelist theList��  ��  ��  � ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��������  ��  ��  - ./. l     ��������  ��  ��  / 010 l     ��������  ��  ��  1 232 i  ��454 I      ��6���� 00 notificationonhandler_ notificationOnHandler_6 7��7 o      ���� 
0 sender  ��  ��  5 k     =88 9:9 r     ;<; m     ��
�� boovtrue< o      ���� 0 notifications  : =��= Z    =>?@��> l   A����A =    BCB o    ���� 0 nsfw  C m    ��
�� boovtrue��  ��  ? r    DED J    FF GHG m    II �JJ  B a c k u p   n o wH KLK m    MM �NN " B a c k u p   n o w   &   q u i tL OPO m    QQ �RR $ T u r n   o f f   t h e   b a n t sP STS m    UU �VV , T u r n   o f f   n o t i f i c a t i o n sT W��W m    XX �YY  Q u i t��  E o      ���� 0 thelist theList@ Z[Z l  " )\����\ =   " )]^] o   " '���� 0 nsfw  ^ m   ' (��
�� boovfals��  ��  [ _��_ r   , 9`a` J   , 3bb cdc m   , -ee �ff  B a c k u p   n o wd ghg m   - .ii �jj " B a c k u p   n o w   &   q u i th klk m   . /mm �nn " T u r n   o n   t h e   b a n t sl opo m   / 0qq �rr , T u r n   o f f   n o t i f i c a t i o n sp s��s m   0 1tt �uu  Q u i t��  a o      ���� 0 thelist theList��  ��  ��  3 vwv l     ��������  ��  ��  w xyx i  ��z{z I      ��|���� 20 notificationoffhandler_ notificationOffHandler_| }��} o      ���� 
0 sender  ��  ��  { k     =~~ � r     ��� m     ��
�� boovfals� o      ���� 0 notifications  � ���� Z    =������ l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m    �� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  " )������ =   " )��� o   " '���� 0 nsfw  � m   ' (��
�� boovfals��  ��  � ���� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   0 1�� ���  Q u i t��  � o      ���� 0 thelist theList��  ��  ��  y ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 0 quithandler_ quitHandler_� ���� o      ���� 
0 sender  ��  ��  � Z     u������ l    ������ =     ��� o     �� 0 isbackingup isBackingUp� m    �
� boovtrue��  ��  � k   
 4�� ��� I   
 ���� 0 setfocus setFocus�  �  � ��� r     ��� l   ���� n    ��� 1    �
� 
bhit� l   ���� I   ���
� .sysodlogaskr        TEXT� m    �� ��� � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?� ���
� 
btns� J    �� ��� m    �� ���  C a n c e l   &   Q u i t� ��� m    �� ���  R e s u m e�  � ���
� 
dflt� m    �� �  �  �  �  �  � o      �� 	0 reply  � ��~� Z   ! 4���}�� l  ! $��|�{� =   ! $��� o   ! "�z�z 	0 reply  � m   " #�� ���  C a n c e l   &   Q u i t�|  �{  � I  ' ,�y��x
�y .aevtquitnull��� ��� null� m   ' (��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �x  �}  � I  / 4�w��v
�w .ascrcmnt****      � ****� l  / 0��u�t� m   / 0�� ���  W B T F U   r e s u m e d�u  �t  �v  �~  � ��� l  7 >��s�r� =   7 >��� o   7 <�q�q 0 isbackingup isBackingUp� m   < =�p
�p boovfals�s  �r  �  �o  k   A q  I   A F�n�m�l�n 0 setfocus setFocus�m  �l    r   G Y l  G W�k�j n   G W	
	 1   U W�i
�i 
bhit
 l  G U�h�g I  G U�f
�f .sysodlogaskr        TEXT m   G H � < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ? �e
�e 
btns J   I O  m   I J �  Q u i t �d m   J M �  R e s u m e�d   �c�b
�c 
dflt m   P Q�a�a �b  �h  �g  �k  �j   o      �`�` 	0 reply   �_ Z   Z q�^ l  Z _�]�\ =   Z _ !  o   Z [�[�[ 	0 reply  ! m   [ ^"" �##  Q u i t�]  �\   I  b g�Z$�Y
�Z .aevtquitnull��� ��� null$ m   b c%%                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �Y  �^   I  j q�X&�W
�X .ascrcmnt****      � ****& l  j m'�V�U' m   j m(( �))  W B T F U   r e s u m e d�V  �U  �W  �_  �o  ��  � *+* l     �T�S�R�T  �S  �R  + ,-, l     �Q�P�O�Q  �P  �O  - ./. l     �N�M�L�N  �M  �L  / 010 l     �K�J�I�K  �J  �I  1 232 l     �H�G�F�H  �G  �F  3 454 i  ��676 I      �E�D�C�E (0 animatemenubaricon animateMenuBarIcon�D  �C  7 k     88 9:9 l     �B;<�B  ; ] Wset imagePath to alias ((path to me as text) & "Contents:Resources:menubar_active.png")   < �== � s e t   i m a g e P a t h   t o   a l i a s   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g " ): >?> l     �A@A�A  @ . (set imagePath to POSIX path of imagePath   A �BB P s e t   i m a g e P a t h   t o   P O S I X   p a t h   o f   i m a g e P a t h? CDC l     �@EF�@  E ] Wset image to current application's NSImage's alloc()'s initWithContentsOfFile:imagePath   F �GG � s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : i m a g e P a t hD HIH r     JKJ n    LML I    �?N�>�? 20 initwithcontentsoffile_ initWithContentsOfFile_N O�=O m    PP �QQ � / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / W B T F U / b l a c k / m e n u b a r _ a c t i v e . p n g�=  �>  M n    RSR I    �<�;�:�< 	0 alloc  �;  �:  S n    TUT o    �9�9 0 nsimage NSImageU m     �8
�8 misccuraK o      �7�7 	0 image  I V�6V n   WXW I    �5Y�4�5 0 	setimage_ 	setImage_Y Z�3Z o    �2�2 	0 image  �3  �4  X o    �1�1 0 
statusitem 
StatusItem�6  5 [\[ l     �0�/�.�0  �/  �.  \ ]^] l     �-�,�+�-  �,  �+  ^ _`_ l     �*�)�(�*  �)  �(  ` aba l     �'�&�%�'  �&  �%  b cdc l     �$�#�"�$  �#  �"  d efe i  ��ghg I      �!� ��! $0 resetmenubaricon resetMenuBarIcon�   �  h k     ii jkj l     �lm�  l ] Wset imagePath to alias ((path to me as text) & "Contents:Resources:menubar_static.png")   m �nn � s e t   i m a g e P a t h   t o   a l i a s   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g " )k opo l     �qr�  q . (set imagePath to POSIX path of imagePath   r �ss P s e t   i m a g e P a t h   t o   P O S I X   p a t h   o f   i m a g e P a t hp tut l     �vw�  v ] Wset image to current application's NSImage's alloc()'s initWithContentsOfFile:imagePath   w �xx � s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : i m a g e P a t hu yzy r     {|{ n    }~} I    ��� 20 initwithcontentsoffile_ initWithContentsOfFile_ ��� m    �� ��� � / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / W B T F U / b l a c k / m e n u b a r _ s t a t i c . p n g�  �  ~ n    ��� I    ���� 	0 alloc  �  �  � n    ��� o    �� 0 nsimage NSImage� m     �
� misccura| o      �� 	0 image  z ��� n   ��� I    ���� 0 	setimage_ 	setImage_� ��� o    �� 	0 image  �  �  � o    �� 0 
statusitem 
StatusItem�  f ��� l     ���
�  �  �
  � ��� l     �	���	  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     � �����   ��  ��  � ��� l     ������  �   create an NSStatusBar   � ��� ,   c r e a t e   a n   N S S t a t u s B a r� ��� i  ����� I      �������� 0 makestatusbar makeStatusBar��  ��  � k     [�� ��� l     ������  �  log ("Make status bar")   � ��� . l o g   ( " M a k e   s t a t u s   b a r " )� ��� r     ��� n    ��� o    ���� "0 systemstatusbar systemStatusBar� n    ��� o    ���� 0 nsstatusbar NSStatusBar� m     ��
�� misccura� o      ���� 0 bar  � ��� l   ��������  ��  ��  � ��� r    ��� n   ��� I   	 ������� .0 statusitemwithlength_ statusItemWithLength_� ���� m   	 
�� ��      ��  ��  � o    	���� 0 bar  � o      ���� 0 
statusitem 
StatusItem� ��� l   ��������  ��  ��  � ��� l   ������  � ] Wset imagePath to alias ((path to me as text) & "Contents:Resources:menubar_static.png")   � ��� � s e t   i m a g e P a t h   t o   a l i a s   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g " )� ��� l   ������  � . (set imagePath to POSIX path of imagePath   � ��� P s e t   i m a g e P a t h   t o   P O S I X   p a t h   o f   i m a g e P a t h� ��� l   ������  � ] Wset image to current application's NSImage's alloc()'s initWithContentsOfFile:imagePath   � ��� � s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : i m a g e P a t h� ��� l   ��������  ��  ��  � ��� r    #��� n   !��� I    !������� 20 initwithcontentsoffile_ initWithContentsOfFile_� ���� m    �� ��� � / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / W B T F U / b l a c k / m e n u b a r _ s t a t i c . p n g��  ��  � n   ��� I    �������� 	0 alloc  ��  ��  � n   ��� o    ���� 0 nsimage NSImage� m    ��
�� misccura� o      ���� 	0 image  � ��� l  $ $��������  ��  ��  � ��� n  $ .��� I   ) .������� 0 	setimage_ 	setImage_� ���� o   ) *���� 	0 image  ��  ��  � o   $ )���� 0 
statusitem 
StatusItem� ��� l  / /��������  ��  ��  � ��� l  / /������  � , & set up the initial NSStatusBars title   � ��� L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e� ��� l  / /������  � # StatusItem's setTitle:"WBTFU"   � ��� : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "� ��� l  / /��������  ��  ��  � ��� l  / /������  � 1 + set up the initial NSMenu of the statusbar   � ��� V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r� ��� r   / A��� n  / ;��� I   6 ;�������  0 initwithtitle_ initWithTitle_� ���� m   6 7�� ���  C u s t o m��  ��  � n  / 6��� I   2 6�������� 	0 alloc  ��  ��  � n  / 2   o   0 2���� 0 nsmenu NSMenu m   / 0��
�� misccura� o      ���� 0 newmenu newMenu�  l  B B������  �  �    l  B B��   � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.    �0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e . 	
	 n  B L I   G L��� 0 setdelegate_ setDelegate_ �  f   G H�  �   o   B G�� 0 newmenu newMenu
  l  M M����  �  �   � n  M [ I   R [��� 0 setmenu_ setMenu_ � o   R W�� 0 newmenu newMenu�  �   o   M R�� 0 
statusitem 
StatusItem�  �  l     ����  �  �    l     ��   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     � �   � �-----------------------------------------------------------------------------------------------------------------------------------------------     �!! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "#" l     ����  �  �  # $%$ l     ����  �  �  % &'& l     ����  �  �  ' ()( l     ����  �  �  ) *+* l     ����  �  �  + ,-, l     ����  �  �  - ./. i  ��010 I      ���� 0 runonce runOnce�  �  1 k     22 343 l      �56�  5 � �if not (current application's NSThread's isMainThread()) as boolean then
		display alert "This script must be run from the main thread." buttons {"Cancel"} as critical
		error number -128
	end if   6 �77� i f   n o t   ( c u r r e n t   a p p l i c a t i o n ' s   N S T h r e a d ' s   i s M a i n T h r e a d ( ) )   a s   b o o l e a n   t h e n 
 	 	 d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l 
 	 	 e r r o r   n u m b e r   - 1 2 8 
 	 e n d   i f4 898 l     ����  �  �  9 :�: I     ���� 0 	plistinit 	plistInit�  �  �  / ;<; l     ����  �  �  < =>= l    ?��? n    @A@ I    ���� 0 makestatusbar makeStatusBar�  �  A  f     �  �  > BCB l   D��D I    ���� 0 runonce runOnce�  �  �  �  C EFE l     ���~�  �  �~  F GHG l     �}�|�{�}  �|  �{  H IJI l     �z�y�x�z  �y  �x  J KLK l     �w�v�u�w  �v  �u  L MNM l     �t�s�r�t  �s  �r  N OPO l     �qQR�q  Q w q Function that is always running in the background. This doesn't need to get called as it is running from the off   R �SS �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f fP TUT l     �pVW�p  V � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   W �XX�   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .U Y�oY i  ��Z[Z I     �n�m�l
�n .miscidlenmbr    ��� null�m  �l  [ k     �\\ ]^] Z     �_`�k�j_ l    a�i�ha E     bcb o     �g�g &0 displaysleepstate displaySleepStatec o    
�f�f 0 strawake strAwake�i  �h  ` Z    �de�e�dd l   f�c�bf =    ghg o    �a�a 0 isbackingup isBackingUph m    �`
�` boovfals�c  �b  e Z    �ijk�_i l   l�^�]l =    mnm o    �\�\ 0 manualbackup manualBackupn m    �[
�[ boovfals�^  �]  j Z   " sop�Z�Yo l  " +q�X�Wq A   " +rsr l  " )t�V�Ut n   " )uvu 1   ' )�T
�T 
hourv l  " 'w�S�Rw I  " '�Q�P�O
�Q .misccurdldt    ��� null�P  �O  �S  �R  �V  �U  s m   ) *�N�N �X  �W  p k   . oxx yzy r   . 7{|{ l  . 5}�M�L} n   . 5~~ 1   3 5�K
�K 
min  l  . 3��J�I� l  . 3��H�G� I  . 3�F�E�D
�F .misccurdldt    ��� null�E  �D  �H  �G  �J  �I  �M  �L  | o      �C�C 0 m  z ��� l  8 8�B�A�@�B  �A  �@  � ��?� Z   8 o����>� G   8 C��� l  8 ;��=�<� =   8 ;��� o   8 9�;�; 0 m  � m   9 :�:�: �=  �<  � l  > A��9�8� =   > A��� o   > ?�7�7 0 m  � m   ? @�6�6 -�9  �8  � Z   F U���5�4� l  F I��3�2� =   F I��� o   F G�1�1 0 scheduledtime scheduledTime� m   G H�0�0 �3  �2  � I   L Q�/�.�-�/ 0 	plistinit 	plistInit�.  �-  �5  �4  � ��� G   X c��� l  X [��,�+� =   X [��� o   X Y�*�* 0 m  � m   Y Z�)�)  �,  �+  � l  ^ a��(�'� =   ^ a��� o   ^ _�&�& 0 m  � m   _ `�%�% �(  �'  � ��$� I   f k�#�"�!�# 0 	plistinit 	plistInit�"  �!  �$  �>  �?  �Z  �Y  k ��� l  v }�� �� =   v }��� o   v {�� 0 manualbackup manualBackup� m   { |�
� boovtrue�   �  � ��� I   � ����� 0 	plistinit 	plistInit�  �  �  �_  �e  �d  �k  �j  ^ ��� l  � �����  �  �  � ��� L   � ��� m   � ��� �  �o       5���� g�������������������������������������������������  � 3��
�	��������� ������������������������������������������������������������������������������
� 
pimr�
 0 
statusitem 
StatusItem�	 0 
thedisplay 
theDisplay� 0 defaults  � $0 internalmenuitem internalMenuItem� $0 externalmenuitem externalMenuItem� 0 newmenu newMenu� 0 thelist theList� 0 nsfw  � 0 notifications  �  0 backupthenquit backupThenQuit�  	0 plist  �� 0 initialbackup initialBackup�� 0 manualbackup manualBackup�� 0 isbackingup isBackingUp�� "0 messagesmissing messagesMissing�� *0 messagesencouraging messagesEncouraging�� $0 messagescomplete messagesComplete�� &0 messagescancelled messagesCancelled�� 40 messagesalreadybackingup messagesAlreadyBackingUp�� 0 strawake strAwake�� 0 strsleep strSleep�� &0 displaysleepstate displaySleepState�� 0 	plistinit 	plistInit�� 0 diskinit diskInit�� 0 freespaceinit freeSpaceInit�� 0 
backupinit 
backupInit�� 0 tidyup tidyUp�� 
0 backup  �� 0 itexists itExists�� .0 getcomputeridentifier getComputerIdentifier�� 0 gettimestamp getTimestamp�� 0 comparesizes compareSizes�� 0 	stopwatch  �� 0 getduration getDuration�� 0 setfocus setFocus�� $0 menuneedsupdate_ menuNeedsUpdate_�� 0 	makemenus 	makeMenus��  0 backuphandler_ backupHandler_�� .0 backupandquithandler_ backupAndQuitHandler_��  0 nsfwonhandler_ nsfwOnHandler_�� "0 nsfwoffhandler_ nsfwOffHandler_�� 00 notificationonhandler_ notificationOnHandler_�� 20 notificationoffhandler_ notificationOffHandler_�� 0 quithandler_ quitHandler_�� (0 animatemenubaricon animateMenuBarIcon�� $0 resetmenubaricon resetMenuBarIcon�� 0 makestatusbar makeStatusBar�� 0 runonce runOnce
�� .miscidlenmbr    ��� null
�� .aevtoappnull  �   � ****� ����� �  ����� �� L��
�� 
vers��  � �����
�� 
cobj� ��   ��
�� 
osax��  � �����
�� 
cobj� ��   �� U
�� 
frmk��  � �����
�� 
cobj� ��   �� [
�� 
frmk��  
� 
msng� �� �����
�� misccura
�� 
pcls� ���  N S U s e r D e f a u l t s� �� �����
�� misccura
�� 
pcls� ���  N S M e n u I t e m� �� �����
�� misccura
�� 
pcls� ���  N S M e n u I t e m� �� �����
�� misccura
�� 
pcls� ���  N S M e n u� ����� �   � � � � �
� boovfals
� boovtrue
� boovfals� ��� � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
� boovtrue
� boovfals
� boovfals� ����� �   � �	� ����� 	� 	 #'+/2� ����� 	� 	 <@DHLPTX[� ����� �  eimquy}�� ��� 	� 	 ���������� ����             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 1 3 8 6 3 4 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 4 1 , " I d l e T i m e r E l a p s e d T i m e " = 3 1 0 0 5 4 7 , " C u r r e n t P o w e r S t a t e " = 4 }� �������� 0 	plistinit 	plistInit�  �  � ��������� 0 
foldername 
folderName� 0 
backupdisk 
backupDisk�  0 computerfolder computerFolder� 0 
backuptime 
backupTime� 0 thecontents theContents� 0 thevalue theValue� 0 backuptimes backupTimes� 0 selectedtime selectedTime� 1��2������������N�Y��rvy��������������������������� 0 itexists itExists
� 
plif
� 
pcnt
� 
valL�  0 foldertobackup FolderToBackup� 0 backupdrive BackupDrive�  0 computerfolder computerFolder� 0 scheduledtime scheduledTime� .0 getcomputeridentifier getComputerIdentifier�  ��� 0 setfocus setFocus
� 
prmp
� .sysostflalis    ��� null
� 
TEXT
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
� .corecrel****      � null
� 
plii
� 
insh
� 
kind� � 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .ascrcmnt****      � ****� 0 diskinit diskInit��jE�O*�b  l+ e  6� .*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYV*j+ 
E�O� ��n)j+ O*��l E�O*�a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a �a  a !b  la " # �*a a $a %*6a  a &a a !a '�a (a ( #O*a a $a %*6a  a &a a !a )�a (a ( #O*a a $a %*6a  a &a a !a *�a (a ( #O*a a $a %*6a  a &a a !a +�a (a ( #UUO�E` ,O�E` -O�E` .O�E�O_ ,_ -_ .�a "vj /O*j+ 0� �=������ 0 diskinit diskInit�  �  � ��� 0 msg  � 	0 reply  � G������dg����q������� "0 destinationdisk destinationDisk� 0 itexists itExists� 0 freespaceinit freeSpaceInit� 0 setfocus setFocus
� 
cobj
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit� 0 diskinit diskInit
� 
appr
� .sysonotfnull��� ��� TEXT� �*��l+ e  *fk+ Y �*j+ Ob  �.E�O����lv�l� �,E�O��  
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� �~��}�|���{�~ 0 freespaceinit freeSpaceInit�} �z��z �  �y�y 	0 again  �|  � �x�w�v�x 	0 again  �w 
0 reply1  �v 0 msg  � �u�t�s�r�q��p���o�n�m�l�� �k�j�i+�h58�u 0 sourcefolder sourceFolder�t "0 destinationdisk destinationDisk�s 0 comparesizes compareSizes�r 0 
backupinit 
backupInit�q 0 setfocus setFocus
�p 
btns
�o 
dflt�n 
�m .sysodlogaskr        TEXT
�l 
bhit�k 0 tidyup tidyUp
�j 
cobj
�i 
appr
�h .sysonotfnull��� ��� TEXT�{ �*��l+ e  
*j+ Y �*j+ O�f  ����lv�l� �,E�Y �e  ����lv�l� �,E�Y hO�a   
*j+ Y ab  b   b  a .E�Y hOb  	e  8b  e  �a a l Y b  f  a a a l Y hY h� �gQ�f�e���d�g 0 
backupinit 
backupInit�f  �e  �  � �c�b�ain�`|�_�^�]�\�[z�Z�Y���X�W���V���� ��U�T
�c 
psxf�b "0 destinationdisk destinationDisk�a 	0 drive  �` 0 itexists itExists
�_ 
kocl
�^ 
cfol
�] 
insh
�\ 
prdt
�[ 
pnam�Z 
�Y .corecrel****      � null�X 0 machinefolder machineFolder
�W 
cdis�V 0 backupfolder backupFolder�U 0 latestfolder latestFolder�T 
0 backup  �d �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` OPUO*j+ � �S,�R�Q���P�S 0 tidyup tidyUp�R  �Q  � �O�N�M�L�K�J�I�H�O 0 bf bF�N 0 creationdates creationDates�M 0 theoldestdate theOldestDate�L 0 j  �K 0 i  �J 0 thisdate thisDate�I 
0 reply2  �H 0 msg  � &�G�F�E8�D�CH�B�A�@�?�>v�=�<�;��:���9�8�7�6��5�4�3��2��1"�014
�G 
psxf�F "0 destinationdisk destinationDisk�E 	0 drive  
�D 
cdis
�C 
cfol�B 0 machinefolder machineFolder
�A 
cobj
�@ 
ascd
�? .corecnte****       ****
�> 
pnam
�= .ascrcmnt****      � ****
�< .coredeloobj        obj �; 0 setfocus setFocus
�: 
btns
�9 
dflt�8 
�7 .sysodlogaskr        TEXT
�6 
bhit
�5 
trsh
�4 .fndremptnull��� ��� obj 
�3 
appr
�2 .sysonotfnull��� ��� TEXT�1 0 freespaceinit freeSpaceInit
�0 .sysodelanull��� ��� nmbr�P{*��/E�O�p*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/j O*j+ Oa a a a lva la  a ,E�O�a   *a ,j Y _b  b   Pb  �.E�Ob  	e  8b  e  �a a l Y b  f  a a a l Y hY hY hO*ek+  Ob  b   Tb  	e  Fb  e  a !a a "l Okj #Y #b  f  a $a a %l Okj #Y hY hY hU� �/U�.�-���,�/ 
0 backup  �.  �-  � 
�+�*�)�(�'�&�%�$�#�"�+ 0 t  �* 0 x  �) "0 containerfolder containerFolder�( 0 
foldername 
folderName�' 0 d  �& 0 duration  �% 0 msg  �$ 0 c  �# 0 oldestfolder oldestFolder�" 0 todelete toDelete� i�!� �u�
y��������������������I�
L�	VY�tvx������������	�� 8:<>QSU]�������������	
			U	g	i	l		�	�	�	�	�	�	�	�	�	�	�	�



.
0
3
H
J
M
`
b
e
�! 0 gettimestamp getTimestamp�  (0 animatemenubaricon animateMenuBarIcon
� .sysodelanull��� ��� nmbr� 0 	stopwatch  
� 
psxf� 0 sourcefolder sourceFolder
� 
TEXT
� 
cfol
� 
pnam
� 
kocl
� 
insh� 0 backupfolder backupFolder
� 
prdt� 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � 0 machinefolder machineFolder� (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
�
 
appr
�	 .sysonotfnull��� ��� TEXT� "0 destinationdisk destinationDisk
� .sysoexecTEXT���     TEXT�  �  � 0 endtime endTime� 0 getduration getDuration
� 
cobj� $0 resetmenubaricon resetMenuBarIcon
�  .aevtquitnull��� ��� null
�� .ascrcmnt****      � ****
�� 
alis
�� 
psxp�,*ek+  E�OeEc  O*j+ Okj O*�k+ O�N*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e `*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_  a !%_ %a "%�%a #%�&E�O a $�%a %%�%a &%j 'W X ( )hOfEc  OfEc  Ob  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  Tb  e   a -�%a .%�%a a /l Okj Y )b  f  a 0�%a 1%a a 2l Okj Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYvb  f k_  a 6%_ %a 7%�%a 8%�%a 9%�&E�O_  a :%_ %a ;%�%a <%�&E�Oa =�%j >Ob  b   l*j a ,E` Ob  a ,.E�Ob  	e  Db  e  �a a ?l Okj Y #b  f  a @a a Al Okj Y hY hY hO a B�%a C%�%a D%�%a E%j 'W X ( )hOfEc  OfEc  O*�/a F&a ,-jv [��/�&E�O�a G,�&E�Oa H�%j >Oa I�%a J%E�O�j 'Ob  b  *j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  �a K  Tb  e   a L�%a M%�%a a Nl Okj Y )b  f  a O�%a P%a a Ql Okj Y hY Qb  e   a R�%a S%�%a a Tl Okj Y )b  f  a U�%a V%a a Wl Okj Y hOb  e  a Xa a Yl Y b  f  a Za a [l Y hY hO)j+ 3Y hY	b  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  ��a K  Tb  e   a \�%a ]%�%a a ^l Okj Y )b  f  a _�%a `%a a al Okj Y hY Qb  e   a b�%a c%�%a a dl Okj Y )b  f  a e�%a f%a a gl Okj Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a hk+ � ��
����� ���� 0 itexists itExists�� ����   ������ 0 
objecttype 
objectType�� 
0 object  ��    ������ 0 
objecttype 
objectType�� 
0 object   
�
�����
���
���
�� 
cdis
�� .coredoexnull���     ****
�� 
file
�� 
cfol�� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU� ��
��������� .0 getcomputeridentifier getComputerIdentifier��  ��   �������� 0 computername computerName�� "0 strserialnumber strSerialNumber��  0 identifiername identifierName ������
�� .sysosigtsirr   ��� null
�� 
sicn
�� .sysoexecTEXT���     TEXT�� *j  �,E�O�j E�O��%�%E�O�� ��.�������� 0 gettimestamp getTimestamp�� ����   ���� 0 isfolder isFolder��   ���������������������������������� 0 isfolder isFolder�� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp   ������������������������������������������QSp
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
� misccura
� 
psof
� 
psin� 
� .sysooffslong    ��� null
� 
cha ���*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�� ����	�� 0 comparesizes compareSizes� �
� 
  ��� 
0 source  � 0 destination  �   	���������� 
0 source  � 0 destination  � 0 fit  � 0 
templatest 
tempLatest� $0 latestfoldersize latestFolderSize� 
0 buffer  � 0 
foldersize 
folderSize� 0 	freespace 	freeSpace� 0 tempsize tempSize	 ������8�����������
� 
psxf� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .sysoexecTEXT���     TEXT
� misccura� � d
� .sysorondlong        doub
� 
cdis
� 
frsp
� .ascrcmnt****      � ****� 0 itexists itExists�eE�O*�/E�O��%�%�%E�OjE�O�E�O� ��%�%j 	E�O� ��!� j U�!E�O*�/�,�!�!�!E�O� 	�� j U�!E�O��%j Ob  f  C*a �l+ e  1a �%a %j 	E�O� ��!� j U�!E�O��%�%j Y hY hOPUOb  f  $��E�O�j ��� fE�Y hY hY b  e  ��� fE�Y hY hO�� ������ 0 	stopwatch  � ��   �� 0 mode  �   ��� 0 mode  � 0 x   ������� 0 gettimestamp getTimestamp
� .ascrcmnt****      � ****� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP� ������ 0 getduration getDuration�  �   �� 0 duration   ������� 0 endtime endTime� 0 	starttime 	startTime� <
� misccura� d
� .sysorondlong        doub� ���!E�O� 	�� j U�!E�O�� ����� 0 setfocus setFocus�  �     
�
� .miscactvnull��� ��� null� � *j U� �5���� $0 menuneedsupdate_ menuNeedsUpdate_� ��   �~
�~ 
cmnu�     �}�} 0 	makemenus 	makeMenus� )j+  � �|Y�{�z�y�| 0 	makemenus 	makeMenus�{  �z   �x�w�v�x 0 i  �w 0 	this_item  �v 0 thismenuitem thisMenuItem "�u�t�s�r~�q�p�o���n����������	%(6CF�m�l�k�j�i�u  0 removeallitems removeAllItems
�t 
cobj
�s 
nmbr
�r 
TEXT
�q misccura�p 0 
nsmenuitem 
NSMenuItem�o 	0 alloc  �n J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�m 0 additem_ addItem_�l 0 
settarget_ 
setTarget_�k 
�j 
bool�i 0 separatoritem separatorItem�y?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY��� �h��g�f�e�h  0 backuphandler_ backupHandler_�g �d�d   �c�c 
0 sender  �f   �b�a�b 
0 sender  �a 0 msg   ��`��_�^���]���
�` 
appr
�_ .sysonotfnull��� ��� TEXT
�^ .sysodelanull��� ��� nmbr
�] 
cobj�e �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h� �\��[�Z�Y�\ .0 backupandquithandler_ backupAndQuitHandler_�[ �X�X   �W�W 
0 sender  �Z   �V�U�T�V 
0 sender  �U 	0 reply  �T 0 msg   �S�R�Q�P�O(F�NI�M�LX[d�K���
�S 
btns
�R 
dflt�Q 
�P .sysodlogaskr        TEXT
�O 
bhit
�N 
appr
�M .sysonotfnull��� ��� TEXT
�L .sysodelanull��� ��� nmbr
�K 
cobj�Y �b  f  �����lv�l� �,E�O��  XeEc  
OeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� �J��I�H�G�J  0 nsfwonhandler_ nsfwOnHandler_�I �F�F   �E�E 
0 sender  �H   �D�D 
0 sender   ������C������C �G >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �B��A�@ !�?�B "0 nsfwoffhandler_ nsfwOffHandler_�A �>"�> "  �=�= 
0 sender  �@    �<�< 
0 sender  ! ��
�;#&�; �? >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �:5�9�8#$�7�: 00 notificationonhandler_ notificationOnHandler_�9 �6%�6 %  �5�5 
0 sender  �8  # �4�4 
0 sender  $ IMQUX�3eimqt�3 �7 >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� �2{�1�0&'�/�2 20 notificationoffhandler_ notificationOffHandler_�1 �.(�. (  �-�- 
0 sender  �0  & �,�, 
0 sender  ' ������+������+ �/ >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� �*��)�()*�'�* 0 quithandler_ quitHandler_�) �&+�& +  �%�% 
0 sender  �(  ) �$�#�$ 
0 sender  �# 	0 reply  * �"��!��� ��������"(�" 0 setfocus setFocus
�! 
btns
�  
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� .aevtquitnull��� ��� null
� .ascrcmnt****      � ****�' vb  e  /*j+  O����lv�l� �,E�O��  
�j Y �j Y @b  f  5*j+  O���a lv�l� �,E�O�a   
�j Y 	a j Y h� �7��,-�� (0 animatemenubaricon animateMenuBarIcon�  �  , �� 	0 image  - ���P��
� misccura� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_� ��,j+ �k+ E�Ob  �k+ � �h��./�� $0 resetmenubaricon resetMenuBarIcon�  �  . �� 	0 image  / ��
�	���
� misccura�
 0 nsimage NSImage�	 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_� ��,j+ �k+ E�Ob  �k+ � ����01�� 0 makestatusbar makeStatusBar�  �  0 ��� 0 bar  � 	0 image  1 � �������������������������
�  misccura�� 0 nsstatusbar NSStatusBar�� "0 systemstatusbar systemStatusBar�� .0 statusitemwithlength_ statusItemWithLength_�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_�� 0 nsmenu NSMenu��  0 initwithtitle_ initWithTitle_�� 0 setdelegate_ setDelegate_�� 0 setmenu_ setMenu_� \��,�,E�O��k+ Ec  O��,j+ �k+ E�Ob  �k+ 	O��,j+ �k+ Ec  Ob  )k+ Ob  b  k+ � ��1����23���� 0 runonce runOnce��  ��  2  3 ���� 0 	plistinit 	plistInit�� *j+  � ��[����45��
�� .miscidlenmbr    ��� null��  ��  4 ���� 0 m  5 
��������������������
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit�� �� �b  b   �b  f  vb  f  V*j  �,� F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY b  e  
*j+ Y hY hY hO�� ��6����78��
�� .aevtoappnull  �   � ****6 k     99 =:: B����  ��  ��  7  8 ������ 0 makestatusbar makeStatusBar�� 0 runonce runOnce�� )j+  O*j+  ascr  ��ޭ