FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S g�� ��� 0 thelist theList � J   S f � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   Y \ � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   \ _ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   _ b � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   q q � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   q q � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ����� 0 endtime endTime�   �  � � � l     �~�}�|�~  �}  �|   �  � � � l     �{�z�y�{  �z  �y   �  � � � l     �x � ��x   �   Property assignment    � � � � (   P r o p e r t y   a s s i g n m e n t �  � � � j   q ��w ��w 	0 plist   � b   q � � � � n  q � � � � 1   ~ ��v
�v 
psxp � l  q ~ ��u�t � I  q ~�s � �
�s .earsffdralis        afdr � m   q t�r
�r afdrdlib � �q ��p
�q 
from � m   w z�o
�o fldmfldu�p  �u  �t   � m   � � � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � l     �n�m�l�n  �m  �l   �  � � � j   � ��k ��k 0 initialbackup initialBackup � m   � ��j
�j boovtrue �  � � � j   � ��i ��i 0 manualbackup manualBackup � m   � ��h
�h boovfals �  � � � j   � ��g ��g 0 isbackingup isBackingUp � m   � ��f
�f boovfals �  � � � l     �e�d�c�e  �d  �c   �  � � � j   � ��b ��b "0 messagesmissing messagesMissing � J   � � � �  � � � m   � � � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m   � � � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �    m   � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !  m   � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �a m   � �		 �

 � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�a   �  l     �`�_�^�`  �_  �^    j   � ��]�] *0 messagesencouraging messagesEncouraging J   � �  m   � � �  C o m e   o n !  m   � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r !  m   � � � " N o   p a i n !   N o   p a i n !  m   � � �   0 L e t ' s   d o   t h i s   a s   a   t e a m ! !"! m   � �## �$$  W e   c a n   d o   i t !" %&% m   � �'' �((  F r e e e e e e e e e d o m !& )*) m   � �++ �,, 2 A l t o g e t h e r   o r   n o t   a t   a l l !* -.- m   � �// �00 H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !. 1�\1 m   � �22 �33 H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�\   454 l     �[�Z�Y�[  �Z  �Y  5 676 j   � ��X8�X $0 messagescomplete messagesComplete8 J   � �99 :;: m   � �<< �==  N i c e   o n e !; >?> m   � �@@ �AA * Y o u   a b s o l u t e   l e g   e n d !? BCB m   � �DD �EE  G o o d   l a d !C FGF m   � �HH �II  P e r f i c k !G JKJ m   � �LL �MM  H a p p y   d a y s !K NON m   � �PP �QQ  F r e e e e e e e e e d o m !O RSR m   � �TT �UU H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !S VWV m   � �XX �YY B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !W Z�WZ m   � �[[ �\\ d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�W  7 ]^] l     �V�U�T�V  �U  �T  ^ _`_ j   ��Sa�S &0 messagescancelled messagesCancelleda J   �bb cdc m   � �ee �ff  T h a t ' s   a   s h a m ed ghg m   � �ii �jj  A h   m a n ,   u n l u c k yh klk m   � �mm �nn  O h   w e l ll opo m   � �qq �rr @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e np sts m   � �uu �vv , W e l l   t h a t ' s   a   l e t   d o w nt wxw m   � �yy �zz P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?x {|{ m   �}} �~~  A r r o g a n t| �R m  �� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�R  ` ��� l     �Q�P�O�Q  �P  �O  � ��� j  .�N��N 40 messagesalreadybackingup messagesAlreadyBackingUp� J  +�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  �� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  �� ���  D i c k� ��� m  �� ���  F u c k t a r d� ��� m  �� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  !�� ���  A r r o g a n t� ��� m  !$�� ��� $ C h i l l   t h e   f u c k   o u t� ��M� m  $'�� ���  H o l d   f i r e�M  � ��� l     �L�K�J�L  �K  �J  � ��� j  /5�I��I 0 strawake strAwake� m  /2�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  6<�H��H 0 strsleep strSleep� m  69�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  =G�G��G &0 displaysleepstate displaySleepState� I =D�F��E
�F .sysoexecTEXT���     TEXT� m  =@�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�E  � ��� l     �D�C�B�D  �C  �B  � ��� l     �A�@�?�A  �@  �?  � ��� l     �>�=�<�>  �=  �<  � ��� l     �;���;  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �:���:  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �9���9  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �8�7�6�8  �7  �6  � ��� l     �5���5  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     �4���4  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     �3���3  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i  HK��� I      �2�1�0�2 0 	plistinit 	plistInit�1  �0  � k    ��� ��� q      �� �/��/ 0 
foldername 
folderName� �.��. 0 
backupdisk 
backupDisk� �-��-  0 computerfolder computerFolder� �,�+�, 0 
backuptime 
backupTime�+  � ��� r     ��� m     �*�*  � o      �)�) 0 
backuptime 
backupTime� ��� l   �(�'�&�(  �'  �&  � ��� Z   ����%�� l   ��$�#� =    ��� I    �"��!�" 0 itexists itExists� ��� m    �� �    f i l e� �  o    �� 	0 plist  �   �!  � m    �
� boovtrue�$  �#  � O    E k    D  r    $ n    "	
	 1     "�
� 
pcnt
 4     �
� 
plif o    �� 	0 plist   o      �� 0 thecontents theContents  r   % * n   % ( 1   & (�
� 
valL o   % &�� 0 thecontents theContents o      �� 0 thevalue theValue  l  + +����  �  �    r   + 0 n   + . o   , .��  0 foldertobackup FolderToBackup o   + ,�� 0 thevalue theValue o      �� 0 
foldername 
folderName  r   1 6 n   1 4 o   2 4�� 0 backupdrive BackupDrive o   1 2�� 0 thevalue theValue o      �� 0 
backupdisk 
backupDisk  !  r   7 <"#" n   7 :$%$ o   8 :��  0 computerfolder computerFolder% o   7 8�� 0 thevalue theValue# o      ��  0 computerfolder computerFolder! &'& r   = B()( n   = @*+* o   > @�
�
 0 scheduledtime scheduledTime+ o   = >�	�	 0 thevalue theValue) o      �� 0 
backuptime 
backupTime' ,-, l  C C����  �  �  - .�. l  C C�/0�  / . (log {folderName, backupDisk, backupTime}   0 �11 P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�   m    22�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �%  � k   H�33 454 r   H O676 I   H M��� � .0 getcomputeridentifier getComputerIdentifier�  �   7 o      ����  0 computerfolder computerFolder5 898 l  P P��������  ��  ��  9 :;: O   P �<=< t   T �>?> k   V �@@ ABA n  V [CDC I   W [�������� 0 setfocus setFocus��  ��  D  f   V WB EFE l  \ \��������  ��  ��  F GHG l  \ \��IJ��  I V Pset folderName to (choose folder with prompt "Please choose a folder to backup")   J �KK � s e t   f o l d e r N a m e   t o   ( c h o o s e   f o l d e r   w i t h   p r o m p t   " P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p " )H LML r   \ cNON I  \ a��P��
�� .earsffdralis        afdrP l  \ ]Q����Q m   \ ]��
�� afdrcusr��  ��  ��  O o      ���� 0 
foldername 
folderNameM RSR r   d uTUT c   d sVWV l  d oX����X I  d o����Y
�� .sysostflalis    ��� null��  Y ��Z��
�� 
prmpZ m   h k[[ �\\ R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  W m   o r��
�� 
TEXTU o      ���� 0 
backupdisk 
backupDiskS ]^] l  v v��������  ��  ��  ^ _`_ r   v }aba n   v {cdc 1   w {��
�� 
psxpd o   v w���� 0 
foldername 
folderNameb o      ���� 0 
foldername 
folderName` efe r   ~ �ghg n   ~ �iji 1    ���
�� 
psxpj o   ~ ���� 0 
backupdisk 
backupDiskh o      ���� 0 
backupdisk 
backupDiskf klk l  � ���������  ��  ��  l mnm r   � �opo J   � �qq rsr m   � �tt �uu 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u rs vwv m   � �xx �yy 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u rw z��z m   � �{{ �|| , E v e r y   h o u r   o n   t h e   h o u r��  p o      ���� 0 backuptimes backupTimesn }~} r   � �� c   � ���� J   � ��� ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  � m   � ���
�� 
TEXT� o      ���� 0 selectedtime selectedTime~ ��� l  � �������  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l  � ���������  ��  ��  � ���� l  � �������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  ? m   T U����  ��= m   P Q���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ; ��� l  � ���������  ��  ��  � ���� O   ����� O   ����� k   ���� ��� I  �$�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  ��
�� 
plii� ����
�� 
insh�  ;  	� �����
�� 
prdt� K  �� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  �� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ���� 0 
foldername 
folderName��  ��  � ��� I %L�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  ),��
�� 
plii� ����
�� 
insh�  ;  /1� �����
�� 
prdt� K  4F�� ����
�� 
kind� m  7:��
�� 
TEXT� ����
�� 
pnam� m  =@�� ���  B a c k u p D r i v e� �����
�� 
valL� o  AB���� 0 
backupdisk 
backupDisk��  ��  � ��� I Mt�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  QT��
�� 
plii� ����
�� 
insh�  ;  WY� �����
�� 
prdt� K  \n�� ����
�� 
kind� m  _b��
�� 
TEXT� ����
�� 
pnam� m  eh�� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  ij����  0 computerfolder computerFolder��  ��  � ���� I u������
�� .corecrel****      � null��  � ����
�� 
kocl� m  y|��
�� 
plii� ����
�� 
insh�  ;  �� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  S c h e d u l e d T i m e� ����
�� 
valL� o  ���~�~ 0 
backuptime 
backupTime�  ��  ��  � l  � � �}�|  I  � ��{�z
�{ .corecrel****      � null�z   �y
�y 
kocl m   � ��x
�x 
plif �w�v
�w 
prdt K   � � �u�t
�u 
pnam o   � ��s�s 	0 plist  �t  �v  �}  �|  � m   � ��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  � 	 l ���r�q�p�r  �q  �p  	 

 r  �� o  ���o�o 0 
foldername 
folderName o      �n�n 0 sourcefolder sourceFolder  r  �� o  ���m�m 0 
backupdisk 
backupDisk o      �l�l "0 destinationdisk destinationDisk  r  �� o  ���k�k  0 computerfolder computerFolder o      �j�j 0 machinefolder machineFolder  r  �� o  ���i�i 0 
backuptime 
backupTime o      �h�h 0 scheduledtime scheduledTime  I ���g�f
�g .ascrcmnt****      � **** J  ��  o  ���e�e 0 sourcefolder sourceFolder  !  o  ���d�d "0 destinationdisk destinationDisk! "#" o  ���c�c 0 machinefolder machineFolder# $�b$ o  ���a�a 0 scheduledtime scheduledTime�b  �f   %&% l ���`�_�^�`  �_  �^  & '�]' I  ���\�[�Z�\ 0 diskinit diskInit�[  �Z  �]  � ()( l     �Y�X�W�Y  �X  �W  ) *+* l     �V�U�T�V  �U  �T  + ,-, l     �S�R�Q�S  �R  �Q  - ./. l     �P�O�N�P  �O  �N  / 010 l     �M�L�K�M  �L  �K  1 232 l     �J45�J  4 H B Function to detect if the selected hard drive is connected or not   5 �66 �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t3 787 l     �I9:�I  9 T N This only happens once a hard drive has been selected and provides a reminder   : �;; �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r8 <=< i  LO>?> I      �H�G�F�H 0 diskinit diskInit�G  �F  ? Z     �@A�EB@ l    	C�D�CC =     	DED I     �BF�A�B 0 itexists itExistsF GHG m    II �JJ  d i s kH K�@K o    �?�? "0 destinationdisk destinationDisk�@  �A  E m    �>
�> boovtrue�D  �C  A I    �=L�<�= 0 freespaceinit freeSpaceInitL M�;M m    �:
�: boovfals�;  �<  �E  B k    �NN OPO n   QRQ I    �9�8�7�9 0 setfocus setFocus�8  �7  R  f    P STS r    $UVU n    "WXW 3     "�6
�6 
cobjX o     �5�5 "0 messagesmissing messagesMissingV o      �4�4 0 msg  T YZY r   % 5[\[ l  % 3]�3�2] n   % 3^_^ 1   1 3�1
�1 
bhit_ l  % 1`�0�/` I  % 1�.ab
�. .sysodlogaskr        TEXTa o   % &�-�- 0 msg  b �,cd
�, 
btnsc J   ' +ee fgf m   ' (hh �ii  C a n c e l   B a c k u pg j�+j m   ( )kk �ll  O K�+  d �*m�)
�* 
dfltm m   , -�(�( �)  �0  �/  �3  �2  \ o      �'�' 	0 reply  Z n�&n Z   6 �op�%qo l  6 9r�$�#r =   6 9sts o   6 7�"�" 	0 reply  t m   7 8uu �vv  O K�$  �#  p I   < A�!� ��! 0 diskinit diskInit�   �  �%  q Z   D �wx��w l  D Oy��y E   D Oz{z o   D I�� &0 displaysleepstate displaySleepState{ o   I N�� 0 strawake strAwake�  �  x k   R �|| }~} r   R [� n   R Y��� 3   W Y�
� 
cobj� o   R W�� &0 messagescancelled messagesCancelled� o      �� 0 msg  ~ ��� Z   \ ������ l  \ c���� =   \ c��� o   \ a�� 0 notifications  � m   a b�
� boovtrue�  �  � Z   f ������ l  f m���� =   f m��� o   f k�� 0 nsfw  � m   k l�

�
 boovtrue�  �  � I  p y�	��
�	 .sysonotfnull��� ��� TEXT� o   p q�� 0 msg  � ���
� 
appr� m   r u�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  � ��� l  | ����� =   | ���� o   | ��� 0 nsfw  � m   � ��
� boovfals�  �  � ��� I  � �� ��
�  .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  �  �  �  �  �  �  �  �&  = ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  PS��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � Z     ������� l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    ��� ��� n   ��� I    �������� 0 setfocus setFocus��  ��  �  f    � ��� l   ��������  ��  ��  � ��� Z    M������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r     0��� l    .������ n     .��� 1   , .��
�� 
bhit� l    ,������ I    ,����
�� .sysodlogaskr        TEXT� m     !�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   " &�� ��� m   " #�� ���  Y e s� ���� m   # $�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   ' (���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ��� l  3 6������ =   3 6��� o   3 4���� 	0 again  � m   4 5��
�� boovtrue��  ��  � ���� r   9 I��� l  9 G������ n   9 G��� 1   E G��
�� 
bhit� l  9 E������ I  9 E����
�� .sysodlogaskr        TEXT� m   9 :�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   ; ?    m   ; < �  Y e s �� m   < = �  C a n c e l   B a c k u p��  � ����
�� 
dflt m   @ A���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  ��  ��  � 	
	 l  N N��������  ��  ��  
 �� Z   N ��� l  N S���� =   N S o   N O���� 	0 reply   m   O R �  Y e s��  ��   I   V [�������� 0 tidyup tidyUp��  ��  ��   k   ^ �  Z   ^ {���� l  ^ i���� E   ^ i o   ^ c���� &0 displaysleepstate displaySleepState o   c h���� 0 strawake strAwake��  ��   r   l w n   l u 3   q u��
�� 
cobj o   l q���� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��    !  l  | |��������  ��  ��  ! "��" Z   | �#$����# l  | �%����% =   | �&'& o   | ����� 0 notifications  ' m   � ���
�� boovtrue��  ��  $ Z   � �()*��( l  � �+����+ =   � �,-, o   � ����� 0 nsfw  - m   � ���
�� boovtrue��  ��  ) I  � ���./
�� .sysonotfnull��� ��� TEXT. o   � ����� 0 msg  / ��0��
�� 
appr0 m   � �11 �22 > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  * 343 l  � �5����5 =   � �676 o   � ����� 0 nsfw  7 m   � ���
�� boovfals��  ��  4 8��8 I  � ���9:
�� .sysonotfnull��� ��� TEXT9 m   � �;; �<< , Y o u   s t o p p e d   b a c k i n g   u p: ��=��
�� 
appr= m   � �>> �?? 
 W B T F U��  ��  ��  ��  ��  ��  ��  � @A@ l     ��������  ��  ��  A BCB l     �������  ��  �  C DED l     �~�}�|�~  �}  �|  E FGF l     �{�z�y�{  �z  �y  G HIH l     �x�w�v�x  �w  �v  I JKJ l     �uLM�u  L,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   M �NNL   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .K OPO l     �tQR�t  Q g a These folders, if required, are then set to their corresponding global variables declared above.   R �SS �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .P TUT i  TWVWV I      �s�r�q�s 0 
backupinit 
backupInit�r  �q  W k     �XX YZY r     [\[ 4     �p]
�p 
psxf] o    �o�o "0 destinationdisk destinationDisk\ o      �n�n 	0 drive  Z ^_^ l   �m`a�m  ` ' !log (destinationDisk & "Backups")   a �bb B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )_ cdc l   �l�k�j�l  �k  �j  d efe Z    ,gh�i�hg l   i�g�fi =    jkj I    �el�d�e 0 itexists itExistsl mnm m    	oo �pp  f o l d e rn q�cq b   	 rsr o   	 
�b�b "0 destinationdisk destinationDisks m   
 tt �uu  B a c k u p s�c  �d  k m    �a
�a boovfals�g  �f  h O    (vwv I   '�`�_x
�` .corecrel****      � null�_  x �^yz
�^ 
kocly m    �]
�] 
cfolz �\{|
�\ 
insh{ o    �[�[ 	0 drive  | �Z}�Y
�Z 
prdt} K    #~~ �X�W
�X 
pnam m     !�� ���  B a c k u p s�W  �Y  w m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �i  �h  f ��� l  - -�V�U�T�V  �U  �T  � ��� Z   - d���S�R� l  - >��Q�P� =   - >��� I   - <�O��N�O 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ��M� b   / 8��� b   / 4��� o   / 0�L�L "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�K�K 0 machinefolder machineFolder�M  �N  � m   < =�J
�J boovfals�Q  �P  � O   A `��� I  E _�I�H�
�I .corecrel****      � null�H  � �G��
�G 
kocl� m   G H�F
�F 
cfol� �E��
�E 
insh� n   I T��� 4   O T�D�
�D 
cfol� m   P S�� ���  B a c k u p s� 4   I O�C�
�C 
cdis� o   M N�B�B 	0 drive  � �A��@
�A 
prdt� K   U [�� �?��>
�? 
pnam� o   V Y�=�= 0 machinefolder machineFolder�>  �@  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �S  �R  � ��� l  e e�<�;�:�<  �;  �:  � ��� O   e ���� k   i �� ��� l  i i�9���9  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y�8�
�8 
cfol� o   u x�7�7 0 machinefolder machineFolder� n   i t��� 4   o t�6�
�6 
cfol� m   p s�� ���  B a c k u p s� 4   i o�5�
�5 
cdis� o   m n�4�4 	0 drive  � o      �3�3 0 backupfolder backupFolder� ��2� l  ~ ~�1���1  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�2  � m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��0�/�.�0  �/  �.  � ��� Z   � ����-�� l  � ���,�+� =   � ���� I   � ��*��)�* 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��(� b   � ���� b   � ���� b   � ���� o   � ��'�' "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��&�& 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�(  �)  � m   � ��%
�% boovfals�,  �+  � O   � ���� I  � ��$�#�
�$ .corecrel****      � null�#  � �"��
�" 
kocl� m   � ��!
�! 
cfol� � ��
�  
insh� o   � ��� 0 backupfolder backupFolder� ���
� 
prdt� K   � ��� ���
� 
pnam� m   � ��� ���  L a t e s t�  �  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �-  � r   � ���� m   � ��
� boovfals� o      �� 0 initialbackup initialBackup� ��� l  � �����  �  �  � ��� O   � ���� k   � ��� ��� r   � ���� n   � ���� 4   � ���
� 
cfol� m   � ��� ���  L a t e s t� n   � �   4   � ��
� 
cfol o   � ��� 0 machinefolder machineFolder n   � � 4   � ��
� 
cfol m   � � �  B a c k u p s 4   � ��
� 
cdis o   � ��� 	0 drive  � o      �� 0 latestfolder latestFolder� 	�	 l  � ��
�  
  log (backupFolder)    � $ l o g   ( b a c k u p F o l d e r )�  � m   � ��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  l  � ����
�  �  �
   �	 I   � ����� 
0 backup  �  �  �	  U  l     ����  �  �    l     ��� �  �  �     l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.    � �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .  !  l     ��"#��  " � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   # �$$�   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .! %&% l     ��'(��  ' � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   ( �))P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .& *+* l     ��,-��  , � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   - �..>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .+ /0/ i  X[121 I      �������� 0 tidyup tidyUp��  ��  2 k    z33 454 r     676 4     ��8
�� 
psxf8 o    ���� "0 destinationdisk destinationDisk7 o      ���� 	0 drive  5 9:9 l   ��������  ��  ��  : ;��; O   z<=< k   y>> ?@? l   ��AB��  A A ;set creationDates to creation date of items of backupFolder   B �CC v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e r@ DED r    FGF n    HIH 4    ��J
�� 
cfolJ o    ���� 0 machinefolder machineFolderI n    KLK 4    ��M
�� 
cfolM m    NN �OO  B a c k u p sL 4    ��P
�� 
cdisP o    ���� 	0 drive  G o      ���� 0 bf bFE QRQ r    STS n    UVU 1    ��
�� 
ascdV n    WXW 2   ��
�� 
cobjX o    ���� 0 bf bFT o      ���� 0 creationdates creationDatesR YZY r     &[\[ n     $]^] 4   ! $��_
�� 
cobj_ m   " #���� ^ o     !���� 0 creationdates creationDates\ o      ���� 0 theoldestdate theOldestDateZ `a` r   ' *bcb m   ' (���� c o      ���� 0 j  a ded l  + +��������  ��  ��  e fgf Y   + nh��ij��h k   9 ikk lml r   9 ?non n   9 =pqp 4   : =��r
�� 
cobjr o   ; <���� 0 i  q o   9 :���� 0 creationdates creationDateso o      ���� 0 thisdate thisDatem s��s Z   @ itu����t l  @ Hv����v >  @ Hwxw n   @ Fyzy 1   D F��
�� 
pnamz 4   @ D��{
�� 
cobj{ o   B C���� 0 i  x m   F G|| �}}  L a t e s t��  ��  u Z   K e~����~ l  K N������ A   K N��� o   K L���� 0 thisdate thisDate� o   L M���� 0 theoldestdate theOldestDate��  ��   k   Q a�� ��� r   Q T��� o   Q R���� 0 thisdate thisDate� o      ���� 0 theoldestdate theOldestDate� ��� r   U X��� o   U V���� 0 i  � o      ���� 0 j  � ��� l  Y Y������  � " log (item j of backupFolder)   � ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )� ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  i m   . /���� j I  / 4�����
�� .corecnte****       ****� o   / 0���� 0 creationdates creationDates��  ��  g ��� l  o o��������  ��  ��  � ��� l  o o������  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� I  o y�����
�� .coredeloobj        obj � n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  � ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l   z z������  � � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� n  z ��� I   { �������� 0 setfocus setFocus��  ��  �  f   z {� ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
bhit� l  � ������� I  � �����
�� .sysodlogaskr        TEXT� m   � ��� ���B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .� ����
�� 
btns� J   � ��� ��� m   � ��� ���  E m p t y   T r a s h� ���� m   � ��� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   � ����� ��  ��  ��  ��  ��  � o      ���� 
0 reply2  � ��� Z   ������� l  � ������� =   � ���� o   � ����� 
0 reply2  � m   � ��� ���  E m p t y   T r a s h��  ��  � I  � ������
�� .fndremptnull��� ��� obj � l  � ������ 1   � ��~
�~ 
trsh��  �  ��  ��  � Z   ����}�|� l  � ���{�z� E   � ���� o   � ��y�y &0 displaysleepstate displaySleepState� o   � ��x�x 0 strawake strAwake�{  �z  � k   ��� ��� r   � ���� n   � ���� 3   � ��w
�w 
cobj� o   � ��v�v &0 messagescancelled messagesCancelled� o      �u�u 0 msg  � ��t� Z   ����s�r� l  � ���q�p� =   � ���� o   � ��o�o 0 notifications  � m   � ��n
�n boovtrue�q  �p  � Z   �����m� l  � ���l�k� =   � ���� o   � ��j�j 0 nsfw  � m   � ��i
�i boovtrue�l  �k  � I  � ��h��
�h .sysonotfnull��� ��� TEXT� o   � ��g�g 0 msg  � �f��e
�f 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�e  � ��� l  � � �d�c  =   � � o   � ��b�b 0 nsfw   m   � ��a
�a boovfals�d  �c  � �` I  ��_
�_ .sysonotfnull��� ��� TEXT m   � � � , Y o u   s t o p p e d   b a c k i n g   u p �^�]
�^ 
appr m   � 		 �

 
 W B T F U�]  �`  �m  �s  �r  �t  �}  �|  �  l �\�[�Z�\  �[  �Z    I  �Y�X�Y 0 freespaceinit freeSpaceInit �W m  �V
�V boovtrue�W  �X   �U Z  y�T�S l #�R�Q E  # o  �P�P &0 displaysleepstate displaySleepState o  "�O�O 0 strawake strAwake�R  �Q   Z  &u�N�M l &-�L�K =  &- o  &+�J�J 0 notifications   m  +,�I
�I boovtrue�L  �K   Z  0q�H l 07�G�F =  07 !  o  05�E�E 0 nsfw  ! m  56�D
�D boovtrue�G  �F   k  :M"" #$# I :G�C%&
�C .sysonotfnull��� ��� TEXT% m  :='' �(( � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .& �B)�A
�B 
appr) m  @C** �++ 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�A  $ ,�@, I HM�?-�>
�? .sysodelanull��� ��� nmbr- m  HI�=�= �>  �@   ./. l PW0�<�;0 =  PW121 o  PU�:�: 0 nsfw  2 m  UV�9
�9 boovfals�<  �;  / 3�83 k  Zm44 565 I Zg�778
�7 .sysonotfnull��� ��� TEXT7 m  Z]99 �:: t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n8 �6;�5
�6 
appr; m  `c<< �== 
 W B T F U�5  6 >�4> I hm�3?�2
�3 .sysodelanull��� ��� nmbr? m  hi�1�1 �2  �4  �8  �H  �N  �M  �T  �S  �U  = m    @@�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  0 ABA l     �0�/�.�0  �/  �.  B CDC l     �-�,�+�-  �,  �+  D EFE l     �*�)�(�*  �)  �(  F GHG l     �'�&�%�'  �&  �%  H IJI l     �$�#�"�$  �#  �"  J KLK l     �!MN�!  M M G Function that carries out the backup process and is split in 2 parts.    N �OO �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  L PQP l     � RS�   R*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   S �TTH   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .Q UVU l     �WX�  W � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   X �YY   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .V Z[Z i  \_\]\ I      ���� 
0 backup  �  �  ] k    �^^ _`_ q      aa �b� 0 t  b �c� 0 x  c ��� "0 containerfolder containerFolder�  ` ded r     fgf I     �h�� 0 gettimestamp getTimestamph i�i m    �
� boovtrue�  �  g o      �� 0 t  e jkj l  	 	����  �  �  k lml r   	 non m   	 
�
� boovtrueo o      �� 0 isbackingup isBackingUpm pqp l   ����  �  �  q rsr I    �
�	��
 (0 animatemenubaricon animateMenuBarIcon�	  �  s tut I   �v�
� .sysodelanull��� ��� nmbrv m    �� �  u wxw l   ����  �  �  x yzy I    #�{� � 0 	stopwatch  { |��| m    }} �~~ 
 s t a r t��  �   z � l  $ $��������  ��  ��  � ��� O   $y��� k   (x�� ��� l  ( (������  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d� ��� l  ( (������  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p� ��� r   ( 0��� c   ( .��� 4   ( ,���
�� 
psxf� o   * +���� 0 sourcefolder sourceFolder� m   , -��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   1 9��� n   1 7��� 1   5 7��
�� 
pnam� 4   1 5���
�� 
cfol� o   3 4���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  : :������  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  : :��������  ��  ��  � ��� Z   : �������� l  : A������ =   : A��� o   : ?���� 0 initialbackup initialBackup� m   ? @��
�� boovfals��  ��  � k   D ��� ��� I  D R�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   F G��
�� 
cfol� ����
�� 
insh� o   H I���� 0 backupfolder backupFolder� �����
�� 
prdt� K   J N�� �����
�� 
pnam� o   K L���� 0 t  ��  ��  � ��� l  S S��������  ��  ��  � ��� r   S ]��� c   S Y��� 4   S W���
�� 
psxf� o   U V���� 0 sourcefolder sourceFolder� m   W X��
�� 
TEXT� o      ���� (0 activesourcefolder activeSourceFolder� ��� r   ^ h��� 4   ^ d���
�� 
cfol� o   ` c���� (0 activesourcefolder activeSourceFolder� o      ���� (0 activesourcefolder activeSourceFolder� ��� l  i i������  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   i ���� n   i ~��� 4   { ~���
�� 
cfol� o   | }���� 0 t  � n   i {��� 4   v {���
�� 
cfol� o   w z���� 0 machinefolder machineFolder� n   i v��� 4   q v���
�� 
cfol� m   r u�� ���  B a c k u p s� 4   i q���
�� 
cdis� o   m p���� 	0 drive  � o      ���� (0 activebackupfolder activeBackupFolder� ��� l  � �������  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ���� I  � ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
cfol� ����
�� 
insh� o   � ����� (0 activebackupfolder activeBackupFolder� �����
�� 
prdt� K   � ��� �����
�� 
pnam� o   � ����� 0 
foldername 
folderName��  ��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �������  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � �������  � D > This means that only new or updated files will be copied over   � ��� |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r� ��� l  � �������  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	�    l  � �����     Original sync code     � (   O r i g i n a l   s y n c   c o d e    l  � �����   z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"    �		 �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' " 

 l  � ���������  ��  ��    l  � �����     Original code    �    O r i g i n a l   c o d e  l  � �����   i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path    � �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h  l  � �����   x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.    � �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .  l  � �����   p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.    � �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .  !  l  � ���"#��  " � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   # �$$�   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .! %&% l  � ���'(��  ' � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   ( �))v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .& *+* l  � ���������  ��  ��  + ,-, l  � ���������  ��  ��  - .��. Z   �x/01��/ l  � �2����2 =   � �343 o   � ����� 0 initialbackup initialBackup4 m   � ���
�� boovtrue��  ��  0 k   ��55 676 r   � �898 n   � �:;: 1   � ���
�� 
time; l  � �<����< I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  9 o      ���� 0 	starttime 	startTime7 =>= Z   �?@����? l  � �A����A E   � �BCB o   � ����� &0 displaysleepstate displaySleepStateC o   � ����� 0 strawake strAwake��  ��  @ Z   �DE����D l  � �F����F =   � �GHG o   � ����� 0 notifications  H m   � ���
�� boovtrue��  ��  E Z   � �IJK��I l  � �L����L =   � �MNM o   � ����� 0 nsfw  N m   � ���
�� boovtrue��  ��  J I  � ���OP
�� .sysonotfnull��� ��� TEXTO m   � �QQ �RR � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .P ��S��
�� 
apprS m   � �TT �UU 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  K VWV l  � �X���X =   � �YZY o   � ��~�~ 0 nsfw  Z m   � ��}
�} boovfals��  �  W [�|[ I  � ��{\]
�{ .sysonotfnull��� ��� TEXT\ m   � �^^ �__ \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e] �z`�y
�z 
appr` m   � �aa �bb 
 W B T F U�y  �|  ��  ��  ��  ��  ��  > cdc l �x�w�v�x  �w  �v  d efe l �u�t�s�u  �t  �s  f ghg l �r�q�p�r  �q  �p  h iji l �o�n�m�o  �n  �m  j klk r  mnm c  opo l q�l�kq b  rsr b  tut b  vwv b  xyx b  z{z o  	�j�j "0 destinationdisk destinationDisk{ m  	|| �}}  B a c k u p s /y o  �i�i 0 machinefolder machineFolderw m  ~~ �  / L a t e s t /u o  �h�h 0 
foldername 
folderNames m  �� ���  /�l  �k  p m  �g
�g 
TEXTn o      �f�f 0 d  l ��� l   �e���e  � A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   � ��� v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l   �d�c�b�d  �c  �b  � ��� Q   A���� k  #8�� ��� l ##�a���a  ���do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' '" & sourceFolder & "' '" & d & "/'"   � ���< d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l ##�`�_�^�`  �_  �^  � ��� l ##�]���]  �   optimised(?)   � ���    o p t i m i s e d ( ? )� ��� I #6�\��[
�\ .sysoexecTEXT���     TEXT� b  #2��� b  #.��� b  #,��� b  #(��� m  #&�� ���	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   '� o  &'�Z�Z 0 sourcefolder sourceFolder� m  (+�� ���  '   '� o  ,-�Y�Y 0 d  � m  .1�� ���  / '�[  � ��� l 77�X�W�V�X  �W  �V  � ��U� l 77�T���T  � U Odo shell script "rsync -aqvz --cvs-exclude '" & sourceFolder & "' '" & d & "/'"   � ��� � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "�U  � R      �S�R�Q
�S .ascrerr ****      � ****�R  �Q  � l @@�P�O�N�P  �O  �N  � ��� l BB�M�L�K�M  �L  �K  � ��� l BB�J�I�H�J  �I  �H  � ��� l BB�G�F�E�G  �F  �E  � ��� l BB�D�C�B�D  �C  �B  � ��� l BB�A�@�?�A  �@  �?  � ��� r  BI��� m  BC�>
�> boovfals� o      �=�= 0 isbackingup isBackingUp� ��� r  JQ��� m  JK�<
�< boovfals� o      �;�; 0 manualbackup manualBackup� ��� l RR�:�9�8�:  �9  �8  � ��� Z  R����7�6� l R]��5�4� E  R]��� o  RW�3�3 &0 displaysleepstate displaySleepState� o  W\�2�2 0 strawake strAwake�5  �4  � k  `��� ��� r  `m��� n  `i��� 1  ei�1
�1 
time� l `e��0�/� I `e�.�-�,
�. .misccurdldt    ��� null�-  �,  �0  �/  � o      �+�+ 0 endtime endTime� ��� r  nu��� n ns��� I  os�*�)�(�* 0 getduration getDuration�)  �(  �  f  no� o      �'�' 0 duration  � ��� r  v���� n  v��� 3  {�&
�& 
cobj� o  v{�%�% $0 messagescomplete messagesComplete� o      �$�$ 0 msg  � ��� Z  �����#�"� l ����!� � =  ����� o  ���� 0 notifications  � m  ���
� boovtrue�!  �   � Z  ������� l ������ =  ����� o  ���� 0 nsfw  � m  ���
� boovtrue�  �  � k  ���� ��� I �����
� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���� 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ���� 0 msg  � ���
� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  �  �  I ����
� .sysodelanull��� ��� nmbr m  ���� �  �  �  l ���� =  �� o  ���� 0 nsfw   m  ���
� boovfals�  �   � k  �� 	
	 I ���

�
 .sysonotfnull��� ��� TEXT b  �� b  �� m  �� � 6 F o r   t h e   f i r s t   t i m e   e v e r   i n   o  ���	�	 0 duration   m  �� �    m i n u t e s ! 
 ��
� 
appr m  �� �  B a c k e d   u p !�  
 � I ����
� .sysodelanull��� ��� nmbr m  ���� �  �  �  �  �#  �"  �  l ����� �  �  �    �� n �� I  ���������� $0 resetmenubaricon resetMenuBarIcon��  ��    f  ����  �7  �6  �   l ����������  ��  ��    !��! Z  ��"#����" l ��$����$ =  ��%&% o  ������  0 backupthenquit backupThenQuit& m  ����
�� boovtrue��  ��  # I ����'��
�� .aevtquitnull��� ��� null' m  ��((                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ��  ��  1 )*) l 	+����+ =  	,-, o  ���� 0 initialbackup initialBackup- m  ��
�� boovfals��  ��  * .��. k  t// 010 r  +232 c  )454 l '6����6 b  '787 b  #9:9 b  !;<; b  =>= b  ?@? b  ABA b  CDC o  ���� "0 destinationdisk destinationDiskD m  EE �FF  B a c k u p s /B o  ���� 0 machinefolder machineFolder@ m  GG �HH  /> o  ���� 0 t  < m   II �JJ  /: o  !"���� 0 
foldername 
folderName8 m  #&KK �LL  /��  ��  5 m  '(��
�� 
TEXT3 o      ���� 0 c  1 MNM r  ,EOPO c  ,CQRQ l ,AS����S b  ,ATUT b  ,=VWV b  ,;XYX b  ,7Z[Z b  ,3\]\ o  ,/���� "0 destinationdisk destinationDisk] m  /2^^ �__  B a c k u p s /[ o  36���� 0 machinefolder machineFolderY m  7:`` �aa  / L a t e s t /W o  ;<���� 0 
foldername 
folderNameU m  =@bb �cc  /��  ��  R m  AB��
�� 
TEXTP o      ���� 0 d  N ded I FO��f��
�� .ascrcmnt****      � ****f l FKg����g b  FKhih m  FIjj �kk  b a c k i n g   u p   t o :  i o  IJ���� 0 d  ��  ��  ��  e lml l PP��������  ��  ��  m non Z  P�pq����p l P[r����r E  P[sts o  PU���� &0 displaysleepstate displaySleepStatet o  UZ���� 0 strawake strAwake��  ��  q k  ^�uu vwv r  ^kxyx n  ^gz{z 1  cg��
�� 
time{ l ^c|����| I ^c������
�� .misccurdldt    ��� null��  ��  ��  ��  y o      ���� 0 	starttime 	startTimew }~} r  lw� n  lu��� 3  qu��
�� 
cobj� o  lq���� *0 messagesencouraging messagesEncouraging� o      ���� 0 msg  ~ ���� Z  x�������� l x������ =  x��� o  x}���� 0 notifications  � m  }~��
�� boovtrue��  ��  � Z  �������� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovtrue��  ��  � k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  � ��� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� m  ���� ���  B a c k i n g   u p� �����
�� 
appr� m  ���� ��� 
 W B T F U��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  ��  ��  ��  o ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� Q  ������ k  ���� ��� l ��������  ���do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings'  --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   � ���� d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l ����������  ��  ��  � ��� l ��������  �   optimised(?)   � ���    o p t i m i s e d ( ? )� ��� I �������
�� .sysoexecTEXT���     TEXT� b  ����� b  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ���
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ������ 0 c  � m  ���� ���  '   '� o  ������ 0 sourcefolder sourceFolder� m  ���� ���  '   '� o  ������ 0 d  � m  ���� ���  / '��  � ��� l ����������  ��  ��  � ���� l ��������  � � zdo shell script "rsync -aqvz --cvs-exclude --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   � ��� � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "��  � R      ������
�� .ascrerr ****      � ****��  ��  � l ����������  ��  ��  � ��� l ����~�}�  �~  �}  � ��� l ���|�{�z�|  �{  �z  � ��� l ���y�x�w�y  �x  �w  � ��� l ���v�u�t�v  �u  �t  � ��� l ���s�r�q�s  �r  �q  � ��� r  ����� m  ���p
�p boovfals� o      �o�o 0 isbackingup isBackingUp� ��� r  ���� m  ���n
�n boovfals� o      �m�m 0 manualbackup manualBackup� ��� l �l�k�j�l  �k  �j  � ��i� Z  t���h�� = ��� n  ��� 2 
�g
�g 
cobj� l 
	 �f�e	  c  
			 4  �d	
�d 
psxf	 o  �c�c 0 c  	 m  	�b
�b 
alis�f  �e  � J  �a�a  � k  j		 			 l �`		�`  	 % delete folder t of backupFolder   	 �				 > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r	 	
		
 l �_�^�]�_  �^  �]  	 			 r  			 c  			 n  			 4  �\	
�\ 
cfol	 o  �[�[ 0 t  	 o  �Z�Z 0 backupfolder backupFolder	 m  �Y
�Y 
TEXT	 o      �X�X 0 oldestfolder oldestFolder	 			 r  &			 c  $			 n  "			 1  "�W
�W 
psxp	 o  �V�V 0 oldestfolder oldestFolder	 m  "#�U
�U 
TEXT	 o      �T�T 0 oldestfolder oldestFolder	 			 I '0�S	�R
�S .ascrcmnt****      � ****	 l ',	 �Q�P	  b  ',	!	"	! m  '*	#	# �	$	$  t o   d e l e t e :  	" o  *+�O�O 0 oldestfolder oldestFolder�Q  �P  �R  	 	%	&	% l 11�N�M�L�N  �M  �L  	& 	'	(	' r  1<	)	*	) b  1:	+	,	+ b  16	-	.	- m  14	/	/ �	0	0  r m   - r f   '	. o  45�K�K 0 oldestfolder oldestFolder	, m  69	1	1 �	2	2  '	* o      �J�J 0 todelete toDelete	( 	3	4	3 I =B�I	5�H
�I .sysoexecTEXT���     TEXT	5 o  =>�G�G 0 todelete toDelete�H  	4 	6	7	6 l CC�F�E�D�F  �E  �D  	7 	8�C	8 Z  Cj	9	:�B�A	9 l CN	;�@�?	; E  CN	<	=	< o  CH�>�> &0 displaysleepstate displaySleepState	= o  HM�=�= 0 strawake strAwake�@  �?  	: k  Qf	>	> 	?	@	? r  Q^	A	B	A n  QZ	C	D	C 1  VZ�<
�< 
time	D l QV	E�;�:	E I QV�9�8�7
�9 .misccurdldt    ��� null�8  �7  �;  �:  	B o      �6�6 0 endtime endTime	@ 	F	G	F r  _f	H	I	H n _d	J	K	J I  `d�5�4�3�5 0 getduration getDuration�4  �3  	K  f  _`	I o      �2�2 0 duration  	G 	L	M	L l gg�1	N	O�1  	N � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   	O �	P	P d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "	M 	Q	R	Q l gg�0	S	T�0  	S  delay 2   	T �	U	U  d e l a y   2	R 	V	W	V l gg�/�.�-�/  �.  �-  	W 	X	Y	X r  gr	Z	[	Z n  gp	\	]	\ 3  lp�,
�, 
cobj	] o  gl�+�+ $0 messagescomplete messagesComplete	[ o      �*�* 0 msg  	Y 	^	_	^ Z  s`	`	a�)�(	` l sz	b�'�&	b =  sz	c	d	c o  sx�%�% 0 notifications  	d m  xy�$
�$ boovtrue�'  �&  	a k  }\	e	e 	f	g	f Z  }&	h	i�#	j	h l }�	k�"�!	k =  }�	l	m	l o  }~� �  0 duration  	m m  ~�	n	n ?�      �"  �!  	i Z  ��	o	p	q�	o l ��	r��	r =  ��	s	t	s o  ���� 0 nsfw  	t m  ���
� boovtrue�  �  	p k  ��	u	u 	v	w	v I ���	x	y
� .sysonotfnull��� ��� TEXT	x b  ��	z	{	z b  ��	|	}	| b  ��	~		~ m  ��	�	� �	�	�  A n d   i n  	 o  ���� 0 duration  	} m  ��	�	� �	�	�    m i n u t e ! 
	{ o  ���� 0 msg  	y �	��
� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  	w 	��	� I ���	��
� .sysodelanull��� ��� nmbr	� m  ���� �  �  	q 	�	�	� l ��	���	� =  ��	�	�	� o  ���� 0 nsfw  	� m  ���
� boovfals�  �  	� 	��	� k  ��	�	� 	�	�	� I ���	�	�
� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ���� 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� �
	��	
�
 
appr	� m  ��	�	� �	�	�  B a c k e d   u p !�	  	� 	��	� I ���	��
� .sysodelanull��� ��� nmbr	� m  ���� �  �  �  �  �#  	j Z  �&	�	�	��	� l ��	���	� =  ��	�	�	� o  ���� 0 nsfw  	� m  ��� 
�  boovtrue�  �  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e s ! 
	� o  ������ 0 msg  	� ��	���
�� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  	� 	�	�	� l �	�����	� =  �	�	�	� o  ����� 0 nsfw  	� m  ��
�� boovfals��  ��  	� 	���	� k  	"	�	� 	�	�	� I 	��	�	�
�� .sysonotfnull��� ��� TEXT	� b  		�	�	� b  		�	�	� m  		�	� �	�	�  A n d   i n  	� o  ���� 0 duration  	� m  	�	� �	�	�    m i n u t e s ! 
	� ��	���
�� 
appr	� m  	�	� �	�	�  B a c k e d   u p !��  	� 	���	� I "��	���
�� .sysodelanull��� ��� nmbr	� m  ���� ��  ��  ��  �  	g 	�	�	� l ''��������  ��  ��  	� 	���	� Z  '\	�	�	���	� l '.	�����	� =  '.	�	�	� o  ',���� 0 nsfw  	� m  ,-��
�� boovtrue��  ��  	� I 1>��	�	�
�� .sysonotfnull��� ��� TEXT	� m  14	�	� �	�	� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .	� ��	���
�� 
appr	� m  7:	�	� �	�	� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  	� 	�	�	� l AH	�����	� =  AH	�	�	� o  AF���� 0 nsfw  	� m  FG��
�� boovfals��  ��  	� 	���	� I KX��	�	�
�� .sysonotfnull��� ��� TEXT	� m  KN	�	� �	�	� p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d	� ��	���
�� 
appr	� m  QT	�	� �	�	� 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  �)  �(  	_ 	�	�	� l aa��������  ��  ��  	� 	���	� n af	�	�	� I  bf�������� $0 resetmenubaricon resetMenuBarIcon��  ��  	�  f  ab��  �B  �A  �C  �h  � k  mt	�	� 	�	�	� Z  mX	�	�����	� l mx	�����	� E  mx	�	�	� o  mr���� &0 displaysleepstate displaySleepState	� o  rw���� 0 strawake strAwake��  ��  	� k  {T	�	� 	�	�	� r  {�
 

  n  {�


 1  ����
�� 
time
 l {�
����
 I {�������
�� .misccurdldt    ��� null��  ��  ��  ��  
 o      ���� 0 endtime endTime	� 


 r  ��


 n ��
	


	 I  ���������� 0 getduration getDuration��  ��  

  f  ��
 o      ���� 0 duration  
 


 r  ��


 n  ��


 3  ����
�� 
cobj
 o  ������ $0 messagescomplete messagesComplete
 o      ���� 0 msg  
 
��
 Z  �T

����
 l ��
����
 =  ��


 o  ������ 0 notifications  
 m  ����
�� boovtrue��  ��  
 Z  �P

��

 l ��
����
 =  ��


 o  ������ 0 duration  
 m  ��

 ?�      ��  ��  
 Z  ��


 ��
 l ��
!����
! =  ��
"
#
" o  ������ 0 nsfw  
# m  ����
�� boovtrue��  ��  
 k  ��
$
$ 
%
&
% I ����
'
(
�� .sysonotfnull��� ��� TEXT
' b  ��
)
*
) b  ��
+
,
+ b  ��
-
.
- m  ��
/
/ �
0
0  A n d   i n  
. o  ������ 0 duration  
, m  ��
1
1 �
2
2    m i n u t e ! 

* o  ������ 0 msg  
( ��
3��
�� 
appr
3 m  ��
4
4 �
5
5 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
& 
6��
6 I ����
7��
�� .sysodelanull��� ��� nmbr
7 m  ������ ��  ��  
  
8
9
8 l ��
:����
: =  ��
;
<
; o  ������ 0 nsfw  
< m  ����
�� boovfals��  ��  
9 
=��
= k  ��
>
> 
?
@
? I ����
A
B
�� .sysonotfnull��� ��� TEXT
A b  ��
C
D
C b  ��
E
F
E m  ��
G
G �
H
H  A n d   i n  
F o  ������ 0 duration  
D m  ��
I
I �
J
J    m i n u t e ! 

B ��
K��
�� 
appr
K m  ��
L
L �
M
M  B a c k e d   u p !��  
@ 
N��
N I ����
O��
�� .sysodelanull��� ��� nmbr
O m  ������ ��  ��  ��  ��  ��  
 Z  P
P
Q
R��
P l 
S����
S =  
T
U
T o  ���� 0 nsfw  
U m  ��
�� boovtrue��  ��  
Q k  &
V
V 
W
X
W I  ��
Y
Z
�� .sysonotfnull��� ��� TEXT
Y b  
[
\
[ b  
]
^
] b  
_
`
_ m  
a
a �
b
b  A n d   i n  
` o  ���� 0 duration  
^ m  
c
c �
d
d    m i n u t e s ! 

\ o  ���� 0 msg  
Z ��
e��
�� 
appr
e m  
f
f �
g
g : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
X 
h��
h I !&��
i��
�� .sysodelanull��� ��� nmbr
i m  !"���� ��  ��  
R 
j
k
j l )0
l����
l =  )0
m
n
m o  ).���� 0 nsfw  
n m  ./��
�� boovfals��  ��  
k 
o��
o k  3L
p
p 
q
r
q I 3F��
s
t
�� .sysonotfnull��� ��� TEXT
s b  3<
u
v
u b  38
w
x
w m  36
y
y �
z
z  A n d   i n  
x o  67���� 0 duration  
v m  8;
{
{ �
|
|    m i n u t e s ! 

t �
}�~
� 
appr
} m  ?B
~
~ �

  B a c k e d   u p !�~  
r 
��}
� I GL�|
��{
�| .sysodelanull��� ��� nmbr
� m  GH�z�z �{  �}  ��  ��  ��  ��  ��  ��  ��  	� 
�
�
� l YY�y�x�w�y  �x  �w  
� 
�
�
� n Y^
�
�
� I  Z^�v�u�t�v $0 resetmenubaricon resetMenuBarIcon�u  �t  
�  f  YZ
� 
�
�
� l __�s�r�q�s  �r  �q  
� 
��p
� Z  _t
�
��o�n
� l _f
��m�l
� =  _f
�
�
� o  _d�k�k  0 backupthenquit backupThenQuit
� m  de�j
�j boovtrue�m  �l  
� I ip�i
��h
�i .aevtquitnull��� ��� null
� m  il
�
�                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �h  �o  �n  �p  �i  ��  ��  ��  � m   $ %
�
��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 
�
�
� l zz�g�f�e�g  �f  �e  
� 
��d
� I  z��c
��b�c 0 	stopwatch  
� 
��a
� m  {~
�
� �
�
�  f i n i s h�a  �b  �d  [ 
�
�
� l     �`�_�^�`  �_  �^  
� 
�
�
� l     �]�\�[�]  �\  �[  
� 
�
�
� l     �Z�Y�X�Z  �Y  �X  
� 
�
�
� l     �W�V�U�W  �V  �U  
� 
�
�
� l     �T�S�R�T  �S  �R  
� 
�
�
� l     �Q
�
��Q  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �P
�
��P  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �O
�
��O  
� � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   
� �
�
� - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
� 
�
�
� l     �N�M�L�N  �M  �L  
� 
�
�
� l     �K
�
��K  
� � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   
� �
�
�   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .
� 
�
�
� l     �J
�
��J  
� � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   
� �
�
��   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .
� 
�
�
� i  `c
�
�
� I      �I
��H�I 0 itexists itExists
� 
�
�
� o      �G�G 0 
objecttype 
objectType
� 
��F
� o      �E�E 
0 object  �F  �H  
� l    W
�
�
�
� O     W
�
�
� Z    V
�
�
��D
� l   
��C�B
� =    
�
�
� o    �A�A 0 
objecttype 
objectType
� m    
�
� �
�
�  d i s k�C  �B  
� Z   
 
�
��@
�
� I  
 �?
��>
�? .coredoexnull���     ****
� 4   
 �=
�
�= 
cdis
� o    �<�< 
0 object  �>  
� L    
�
� m    �;
�; boovtrue�@  
� L    
�
� m    �:
�: boovfals
� 
�
�
� l   "
��9�8
� =    "
�
�
� o     �7�7 0 
objecttype 
objectType
� m     !
�
� �
�
�  f i l e�9  �8  
� 
�
�
� Z   % 7
�
��6
�
� I  % -�5
��4
�5 .coredoexnull���     ****
� 4   % )�3
�
�3 
file
� o   ' (�2�2 
0 object  �4  
� L   0 2
�
� m   0 1�1
�1 boovtrue�6  
� L   5 7
�
� m   5 6�0
�0 boovfals
� 
�
�
� l  : =
��/�.
� =   : =
�
�
� o   : ;�-�- 0 
objecttype 
objectType
� m   ; <
�
� �
�
�  f o l d e r�/  �.  
� 
��,
� Z   @ R
�
��+
�
� I  @ H�*
��)
�* .coredoexnull���     ****
� 4   @ D�(
�
�( 
cfol
� o   B C�'�' 
0 object  �)  
� L   K M
�
� m   K L�&
�& boovtrue�+  
� L   P R
�
� m   P Q�%
�% boovfals�,  �D  
� m     
�
��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  
� "  (string, string) as Boolean   
� �
�
� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n
� 
�
�
� l     �$�#�"�$  �#  �"  
� 
�
�
� l     �!� ��!  �   �  
�    l     ����  �  �    l     ����  �  �    l     ����  �  �    l     �	�   � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   	 �

Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .  l     ��   P J This means that backups can be identified from what machine they're from.    � �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .  i  dg I      ���� .0 getcomputeridentifier getComputerIdentifier�  �   k       r     	 n      1    �
� 
sicn l    �� I    ���
� .sysosigtsirr   ��� null�  �  �  �   o      �
�
 0 computername computerName  r   
  I  
 �	 �
�	 .sysoexecTEXT���     TEXT  m   
 !! �"" � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �   o      �� "0 strserialnumber strSerialNumber #$# r    %&% l   '��' b    ()( b    *+* o    �� 0 computername computerName+ m    ,, �--    -  ) o    �� "0 strserialnumber strSerialNumber�  �  & o      ��  0 identifiername identifierName$ .�. L    // o    � �   0 identifiername identifierName�   010 l     ��������  ��  ��  1 232 l     ��������  ��  ��  3 454 l     ��������  ��  ��  5 676 l     ��������  ��  ��  7 898 l     ��������  ��  ��  9 :;: l     ��<=��  < � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   = �>>   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .; ?@? l     ��AB��  A � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   B �CC   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .@ DED i  hkFGF I      ��H���� 0 gettimestamp getTimestampH I��I o      ���� 0 isfolder isFolder��  ��  G l   �JKLJ k    �MM NON l     ��PQ��  P   Date variables   Q �RR    D a t e   v a r i a b l e sO STS r     )UVU l     W����W I     ������
�� .misccurdldt    ��� null��  ��  ��  ��  V K    XX ��YZ
�� 
yearY o    ���� 0 y  Z ��[\
�� 
mnth[ o    ���� 0 m  \ ��]^
�� 
day ] o    ���� 0 d  ^ ��_��
�� 
time_ o   	 
���� 0 t  ��  T `a` r   * 1bcb c   * /ded l  * -f����f c   * -ghg o   * +���� 0 y  h m   + ,��
�� 
long��  ��  e m   - .��
�� 
TEXTc o      ���� 0 ty tYa iji r   2 9klk c   2 7mnm l  2 5o����o c   2 5pqp o   2 3���� 0 y  q m   3 4��
�� 
long��  ��  n m   5 6��
�� 
TEXTl o      ���� 0 ty tYj rsr r   : Atut c   : ?vwv l  : =x����x c   : =yzy o   : ;���� 0 m  z m   ; <��
�� 
long��  ��  w m   = >��
�� 
TEXTu o      ���� 0 tm tMs {|{ r   B I}~} c   B G� l  B E������ c   B E��� o   B C���� 0 d  � m   C D��
�� 
long��  ��  � m   E F��
�� 
TEXT~ o      ���� 0 td tD| ��� r   J Q��� c   J O��� l  J M������ c   J M��� o   J K���� 0 t  � m   K L��
�� 
long��  ��  � m   M N��
�� 
TEXT� o      ���� 0 tt tT� ��� l  R R��������  ��  ��  � ��� l  R R������  � U O Append the month or day with a 0 if the string length is only 1 character long   � ��� �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g� ��� r   R [��� c   R Y��� l  R W������ I  R W�����
�� .corecnte****       ****� o   R S���� 0 tm tM��  ��  ��  � m   W X��
�� 
nmbr� o      ���� 
0 tml tML� ��� r   \ e��� c   \ c��� l  \ a������ I  \ a�����
�� .corecnte****       ****� o   \ ]���� 0 tm tM��  ��  ��  � m   a b��
�� 
nmbr� o      ���� 
0 tdl tDL� ��� l  f f��������  ��  ��  � ��� Z   f u������� l  f i������ =   f i��� o   f g���� 
0 tml tML� m   g h���� ��  ��  � r   l q��� b   l o��� m   l m�� ���  0� o   m n���� 0 tm tM� o      ���� 0 tm tM��  ��  � ��� l  v v��������  ��  ��  � ��� Z   v �������� l  v y������ =   v y��� o   v w���� 
0 tdl tDL� m   w x���� ��  ��  � r   | ���� b   | ���� m   | �� ���  0� o    ����� 0 td tD� o      ���� 0 td tD��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  �   Time variables	   � ���     T i m e   v a r i a b l e s 	� ��� l  � �������  �  	 Get hour   � ���    G e t   h o u r� ��� r   � ���� n   � ���� 1   � ���
�� 
tstr� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 timestr timeStr� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      �� 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � ��~��
�~ 
cha � m   � ��}�} � l  � ���|�{� \   � ���� o   � ��z�z 0 pos  � m   � ��y�y �|  �{  � o   � ��x�x 0 timestr timeStr� m   � ��w
�w 
TEXT� o      �v�v 0 h  � ��� r   � ���� c   � ���� n   � ���� 7 � ��u��
�u 
cha � l  � ���t�s� [   � ���� o   � ��r�r 0 pos  � m   � ��q�q �t  �s  �  ;   � �� o   � ��p�p 0 timestr timeStr� m   � ��o
�o 
TEXT� o      �n�n 0 timestr timeStr� ��� l  � ��m�l�k�m  �l  �k  � ��� l  � ��j �j      Get minute    �    G e t   m i n u t e�  r   � � I  � ��i z�h�g
�h .sysooffslong    ��� null
�g misccura�i   �f	

�f 
psof	 m   � � �  :
 �e�d
�e 
psin o   � ��c�c 0 timestr timeStr�d   o      �b�b 0 pos    r   � c   � n   �  7  � �a
�a 
cha  m   � ��`�`  l  � ��_�^ \   � � o   � ��]�] 0 pos   m   � ��\�\ �_  �^   o   � ��[�[ 0 timestr timeStr m   �Z
�Z 
TEXT o      �Y�Y 0 m    r   c    n  !"! 7�X#$
�X 
cha # l %�W�V% [  &'& o  �U�U 0 pos  ' m  �T�T �W  �V  $  ;  " o  �S�S 0 timestr timeStr  m  �R
�R 
TEXT o      �Q�Q 0 timestr timeStr ()( l �P�O�N�P  �O  �N  ) *+* l �M,-�M  ,   Get AM or PM   - �..    G e t   A M   o r   P M+ /0/ r  2121 I 03�L43 z�K�J
�K .sysooffslong    ��� null
�J misccura�L  4 �I56
�I 
psof5 m  "%77 �88   6 �H9�G
�H 
psin9 o  ()�F�F 0 timestr timeStr�G  2 o      �E�E 0 pos  0 :;: r  3E<=< c  3C>?> n  3A@A@ 74A�DBC
�D 
cha B l :>D�C�BD [  :>EFE o  ;<�A�A 0 pos  F m  <=�@�@ �C  �B  C  ;  ?@A o  34�?�? 0 timestr timeStr? m  AB�>
�> 
TEXT= o      �=�= 0 s  ; GHG l FF�<�;�:�<  �;  �:  H IJI l FF�9�8�7�9  �8  �7  J KLK Z  FMNO�6M l FIP�5�4P =  FIQRQ o  FG�3�3 0 isfolder isFolderR m  GH�2
�2 boovtrue�5  �4  N k  LaSS TUT l LL�1VW�1  V   Create folder timestamp   W �XX 0   C r e a t e   f o l d e r   t i m e s t a m pU Y�0Y r  LaZ[Z b  L_\]\ b  L]^_^ b  L[`a` b  LYbcb b  LUded b  LSfgf b  LQhih m  LOjj �kk  b a c k u p _i o  OP�/�/ 0 ty tYg o  QR�.�. 0 tm tMe o  ST�-�- 0 td tDc m  UXll �mm  _a o  YZ�,�, 0 h  _ o  [\�+�+ 0 m  ] o  ]^�*�* 0 s  [ o      �)�) 0 	timestamp  �0  O non l dgp�(�'p =  dgqrq o  de�&�& 0 isfolder isFolderr m  ef�%
�% boovfals�(  �'  o s�$s k  j{tt uvu l jj�#wx�#  w   Create timestamp   x �yy "   C r e a t e   t i m e s t a m pv z�"z r  j{{|{ b  jy}~} b  jw� b  ju��� b  js��� b  jo��� b  jm��� o  jk�!�! 0 ty tY� o  kl� �  0 tm tM� o  mn�� 0 td tD� m  or�� ���  _� o  st�� 0 h  � o  uv�� 0 m  ~ o  wx�� 0 s  | o      �� 0 	timestamp  �"  �$  �6  L ��� l ������  �  �  � ��� L  ���� o  ���� 0 	timestamp  �  K  	(boolean)   L ���  ( b o o l e a n )E ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ���
�  �  �
  � ��� l     �	���	  �  �  � ��� l     ����  � q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   � ��� �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .� ��� l     ����  � w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   � ��� �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .� ��� i  lo��� I      ���� 0 comparesizes compareSizes� ��� o      �� 
0 source  � ��� o      � �  0 destination  �  �  � l   ����� k    ��� ��� r     ��� m     ��
�� boovtrue� o      ���� 0 fit  � ��� r    
��� 4    ���
�� 
psxf� o    ���� 0 destination  � o      ���� 0 destination  � ��� l   ��������  ��  ��  � ��� l   ������  � w q Get sizes for Library > Caches, Library > Logs and Library > Mobile Documents then subtract them from folderSize   � ��� �   G e t   s i z e s   f o r   L i b r a r y   >   C a c h e s ,   L i b r a r y   >   L o g s   a n d   L i b r a r y   >   M o b i l e   D o c u m e n t s   t h e n   s u b t r a c t   t h e m   f r o m   f o l d e r S i z e� ��� l   ��������  ��  ��  � ��� r    ��� b    ��� b    ��� b    ��� o    ���� "0 destinationdisk destinationDisk� m    �� ���  B a c k u p s /� o    ���� 0 machinefolder machineFolder� m    �� ���  / L a t e s t� o      ���� 0 
templatest 
tempLatest� ��� r    ��� m    ����  � o      ���� $0 latestfoldersize latestFolderSize� ��� l   ��������  ��  ��  � ��� r    &��� b    $��� n   "��� 1     "��
�� 
psxp� l    ������ I    ����
�� .earsffdralis        afdr� m    ��
�� afdrdlib� �����
�� 
from� m    ��
�� fldmfldu��  ��  ��  � m   " #�� ���  C a c h e s� o      ���� 0 c  � ��� r   ' 4��� b   ' 2��� n  ' 0��� 1   . 0��
�� 
psxp� l  ' .������ I  ' .����
�� .earsffdralis        afdr� m   ' (��
�� afdrdlib� �����
�� 
from� m   ) *��
�� fldmfldu��  ��  ��  � m   0 1�� ���  L o g s� o      ���� 0 l  � ��� r   5 B��� b   5 @��� n  5 >��� 1   < >��
�� 
psxp� l  5 <������ I  5 <����
�� .earsffdralis        afdr� m   5 6��
�� afdrdlib� �����
�� 
from� m   7 8��
�� fldmfldu��  ��  ��  � m   > ?�� ���   M o b i l e   D o c u m e n t s� o      ���� 0 md  �    r   C F m   C D����   o      ���� 0 	cachesize 	cacheSize  r   G J m   G H����   o      ���� 0 logssize logsSize 	 r   K N

 m   K L����   o      ���� 0 mdsize mdSize	  l  O O��������  ��  ��    r   O R m   O P ?�       o      ���� 
0 buffer    l  S S��������  ��  ��    r   S V m   S T����   o      ���� (0 adjustedfoldersize adjustedFolderSize  l  W W��������  ��  ��    O   W� k   [�  !  r   [ h"#" I  [ f��$��
�� .sysoexecTEXT���     TEXT$ b   [ b%&% b   [ ^'(' m   [ \)) �**  d u   - m s   '( o   \ ]���� 
0 source  & m   ^ a++ �,,  '   |   c u t   - f   1��  # o      ���� 0 
foldersize 
folderSize! -.- r   i �/0/ ^   i �121 l  i }3����3 I  i }45��4 z����
�� .sysorondlong        doub
�� misccura5 ]   o x676 l  o t8����8 ^   o t9:9 o   o p���� 0 
foldersize 
folderSize: m   p s���� ��  ��  7 m   t w���� d��  ��  ��  2 m   } ����� d0 o      ���� 0 
foldersize 
folderSize. ;<; l  � ���������  ��  ��  < =>= r   � �?@? ^   � �ABA ^   � �CDC ^   � �EFE l  � �G����G l  � �H����H n   � �IJI 1   � ���
�� 
frspJ 4   � ���K
�� 
cdisK o   � ����� 0 destination  ��  ��  ��  ��  F m   � ����� D m   � ����� B m   � ����� @ o      ���� 0 	freespace 	freeSpace> LML r   � �NON ^   � �PQP l  � �R����R I  � �ST��S z����
�� .sysorondlong        doub
�� misccuraT l  � �U����U ]   � �VWV o   � ����� 0 	freespace 	freeSpaceW m   � ����� d��  ��  ��  ��  ��  Q m   � ����� dO o      ���� 0 	freespace 	freeSpaceM XYX l  � ���������  ��  ��  Y Z[Z r   � �\]\ I  � ���^��
�� .sysoexecTEXT���     TEXT^ b   � �_`_ b   � �aba m   � �cc �dd  d u   - m s   'b o   � ����� 0 c  ` m   � �ee �ff  '   |   c u t   - f   1��  ] o      ���� 0 	cachesize 	cacheSize[ ghg r   � �iji ^   � �klk l  � �m����m I  � �no��n z����
�� .sysorondlong        doub
�� misccurao ]   � �pqp l  � �r����r ^   � �sts o   � ����� 0 	cachesize 	cacheSizet m   � ����� ��  ��  q m   � ����� d��  ��  ��  l m   � ����� dj o      ���� 0 	cachesize 	cacheSizeh uvu l  � ���������  ��  ��  v wxw r   � �yzy I  � ��{�~
� .sysoexecTEXT���     TEXT{ b   � �|}| b   � �~~ m   � ��� ���  d u   - m s   ' o   � ��}�} 0 l  } m   � ��� ���  '   |   c u t   - f   1�~  z o      �|�| 0 logssize logsSizex ��� r   �	��� ^   ���� l  ���{�z� I  ����y� z�x�w
�x .sysorondlong        doub
�w misccura� ]   � ���� l  � ���v�u� ^   � ���� o   � ��t�t 0 logssize logsSize� m   � ��s�s �v  �u  � m   � ��r�r d�y  �{  �z  � m  �q�q d� o      �p�p 0 logssize logsSize� ��� l 

�o�n�m�o  �n  �m  � ��� r  
��� I 
�l��k
�l .sysoexecTEXT���     TEXT� b  
��� b  
��� m  
�� ���  d u   - m s   '� o  �j�j 0 md  � m  �� ���  '   |   c u t   - f   1�k  � o      �i�i 0 mdsize mdSize� ��� r  4��� ^  2��� l .��h�g� I .���f� z�e�d
�e .sysorondlong        doub
�d misccura� ]   )��� l  %��c�b� ^   %��� o   !�a�a 0 mdsize mdSize� m  !$�`�` �c  �b  � m  %(�_�_ d�f  �h  �g  � m  .1�^�^ d� o      �]�] 0 mdsize mdSize� ��� l 55�\�[�Z�\  �[  �Z  � ��� r  5>��� l 5<��Y�X� \  5<��� \  5:��� \  58��� o  56�W�W 0 
foldersize 
folderSize� o  67�V�V 0 	cachesize 	cacheSize� o  89�U�U 0 logssize logsSize� o  :;�T�T 0 mdsize mdSize�Y  �X  � o      �S�S (0 adjustedfoldersize adjustedFolderSize� ��� l ??�R�Q�P�R  �Q  �P  � ��� I ?H�O��N
�O .ascrcmnt****      � ****� l ?D��M�L� b  ?D��� b  ?B��� o  ?@�K�K 0 
foldersize 
folderSize� o  @A�J�J (0 adjustedfoldersize adjustedFolderSize� o  BC�I�I 0 	freespace 	freeSpace�M  �L  �N  � ��� l II�H�G�F�H  �G  �F  � ��E� Z  I����D�C� l IP��B�A� =  IP��� o  IN�@�@ 0 initialbackup initialBackup� m  NO�?
�? boovfals�B  �A  � k  S�� ��� r  Sb��� I S`�>��=
�> .sysoexecTEXT���     TEXT� b  S\��� b  SX��� m  SV�� ���  d u   - m s   '� o  VW�<�< 0 
templatest 
tempLatest� m  X[�� ���  '   |   c u t   - f   1�=  � o      �;�; $0 latestfoldersize latestFolderSize� ��� r  c}��� ^  c{��� l cw��:�9� I cw���8� z�7�6
�7 .sysorondlong        doub
�6 misccura� ]  ir��� l in��5�4� ^  in��� o  ij�3�3 $0 latestfoldersize latestFolderSize� m  jm�2�2 �5  �4  � m  nq�1�1 d�8  �:  �9  � m  wz�0�0 d� o      �/�/ $0 latestfoldersize latestFolderSize� ��.� l ~~�-�,�+�-  �,  �+  �.  �D  �C  �E   m   W X���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��   ��� l ���*�)�(�*  �)  �(  � ��� Z  ������'� l ����&�%� =  ����� o  ���$�$ 0 initialbackup initialBackup� m  ���#
�# boovfals�&  �%  � k  ���� ��� r  ����� \  ����� o  ���"�" (0 adjustedfoldersize adjustedFolderSize� o  ���!�! $0 latestfoldersize latestFolderSize� o      � �  0 temp  � ��� l ������  �  �  � � � Z  ���� l ���� A  �� o  ���� 0 temp   m  ����  �  �   r  �� \  ��	 l ��
��
 o  ���� 0 temp  �  �  	 l ���� ]  �� l ���� o  ���� 0 temp  �  �   m  ���� �  �   o      �� 0 temp  �  �     l �����
�  �  �
   �	 Z  ���� l ���� ?  �� o  ���� 0 temp   l ���� \  �� o  ���� 0 	freespace 	freeSpace o  ��� �  
0 buffer  �  �  �  �   r  �� m  ����
�� boovfals o      ���� 0 fit  �  �  �	  �  l ������ =  ��  o  ������ 0 initialbackup initialBackup  m  ����
�� boovtrue��  ��   !��! Z  ��"#����" l ��$����$ ?  ��%&% o  ������ (0 adjustedfoldersize adjustedFolderSize& l ��'����' \  ��()( o  ������ 0 	freespace 	freeSpace) o  ������ 
0 buffer  ��  ��  ��  ��  # r  ��*+* m  ����
�� boovfals+ o      ���� 0 fit  ��  ��  ��  �'  � ,-, l ����������  ��  ��  - .��. L  ��// o  ������ 0 fit  ��  �  (string, string)   � �00   ( s t r i n g ,   s t r i n g )� 121 l     ��������  ��  ��  2 343 l     ��������  ��  ��  4 565 l     ��������  ��  ��  6 787 l     ��������  ��  ��  8 9:9 l     ��������  ��  ��  : ;<; l     ��=>��  = m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   > �?? �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .< @A@ i  psBCB I      ��D���� 0 	stopwatch  D E��E o      ���� 0 mode  ��  ��  C l    5FGHF k     5II JKJ q      LL ������ 0 x  ��  K MNM Z     3OPQ��O l    R����R =     STS o     ���� 0 mode  T m    UU �VV 
 s t a r t��  ��  P k    WW XYX r    Z[Z I    ��\���� 0 gettimestamp getTimestamp\ ]��] m    ��
�� boovfals��  ��  [ o      ���� 0 x  Y ^��^ I   ��_��
�� .ascrcmnt****      � ****_ l   `����` b    aba m    cc �dd   b a c k u p   s t a r t e d :  b o    ���� 0 x  ��  ��  ��  ��  Q efe l   g����g =    hih o    ���� 0 mode  i m    jj �kk  f i n i s h��  ��  f l��l k    /mm non r    'pqp I    %��r���� 0 gettimestamp getTimestampr s��s m     !��
�� boovfals��  ��  q o      ���� 0 x  o t��t I  ( /��u��
�� .ascrcmnt****      � ****u l  ( +v����v b   ( +wxw m   ( )yy �zz " b a c k u p   f i n i s h e d :  x o   ) *���� 0 x  ��  ��  ��  ��  ��  ��  N {��{ l  4 4��������  ��  ��  ��  G  (string)   H �||  ( s t r i n g )A }~} l     ��������  ��  ��  ~ � l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � M G Utility function to get the duration of the backup process in minutes.   � ��� �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .� ��� i  tw��� I      �������� 0 getduration getDuration��  ��  � k     �� ��� r     ��� ^     ��� l    ������ \     ��� o     ���� 0 endtime endTime� o    ���� 0 	starttime 	startTime��  ��  � m    ���� <� o      ���� 0 duration  � ��� r    ��� ^    ��� l   ������ I   ����� z����
�� .sysorondlong        doub
�� misccura� l   ������ ]    ��� o    ���� 0 duration  � m    ���� d��  ��  ��  ��  ��  � m    ���� d� o      ���� 0 duration  � ���� L    �� o    ���� 0 duration  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     �������  ��  �  � ��� l     �~�}�|�~  �}  �|  � ��� l     �{���{  � ; 5 Utility function bring WBTFU to the front with focus   � ��� j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s� ��� i  x{��� I      �z�y�x�z 0 setfocus setFocus�y  �x  � O     
��� I   	�w�v�u
�w .miscactvnull��� ��� null�v  �u  � m     ��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  � ��� l     �t�s�r�t  �s  �r  � ��� l     �q���q  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �p���p  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �o�n�m�o  �n  �m  � ��� l     �l�k�j�l  �k  �j  � ��� l     �i�h�g�i  �h  �g  � ��� l     �f�e�d�f  �e  �d  � ��� l     �c�b�a�c  �b  �a  � ��� l     �`���`  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �_���_  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �^���^  � � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   � ��� - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �]�\�[�]  �\  �[  � ��� i  |��� I      �Z��Y�Z $0 menuneedsupdate_ menuNeedsUpdate_� ��X� l     ��W�V� m      �U
�U 
cmnu�W  �V  �X  �Y  � k     �� ��� l     �T���T  � J D NSMenu's delegates method, when the menu is clicked this is called.   � ��� �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .� ��� l     �S���S  � j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   � ��� �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .� ��� l     �R���R  � < 6 This means the menu items can be changed dynamically.   � ��� l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .� ��Q� n    ��� I    �P�O�N�P 0 	makemenus 	makeMenus�O  �N  �  f     �Q  � � � l     �M�L�K�M  �L  �K     l     �J�I�H�J  �I  �H    l     �G�F�E�G  �F  �E    l     �D�C�B�D  �C  �B    l     �A�@�?�A  �@  �?   	
	 i  �� I      �>�=�<�> 0 	makemenus 	makeMenus�=  �<   k    >  l    	 n    	 I    	�;�:�9�;  0 removeallitems removeAllItems�:  �9   o     �8�8 0 newmenu newMenu !  remove existing menu items    � 6   r e m o v e   e x i s t i n g   m e n u   i t e m s  l  
 
�7�6�5�7  �6  �5   �4 Y   
>�3�2 k   9  r    '  n    %!"! 4   " %�1#
�1 
cobj# o   # $�0�0 0 i  " o    "�/�/ 0 thelist theList  o      �.�. 0 	this_item   $%$ l  ( (�-�,�+�-  �,  �+  % &'& Z   ( ()*�*( l  ( -+�)�(+ =   ( -,-, l  ( +.�'�&. c   ( +/0/ o   ( )�%�% 0 	this_item  0 m   ) *�$
�$ 
TEXT�'  �&  - m   + ,11 �22  B a c k u p   n o w�)  �(  ) r   0 @343 l  0 >5�#�"5 n  0 >676 I   7 >�!8� �! J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_8 9:9 o   7 8�� 0 	this_item  : ;<; m   8 9== �>>  b a c k u p H a n d l e r :< ?�? m   9 :@@ �AA  �  �   7 n  0 7BCB I   3 7���� 	0 alloc  �  �  C n  0 3DED o   1 3�� 0 
nsmenuitem 
NSMenuItemE m   0 1�
� misccura�#  �"  4 o      �� 0 thismenuitem thisMenuItem* FGF l  C HH��H =   C HIJI l  C FK��K c   C FLML o   C D�� 0 	this_item  M m   D E�
� 
TEXT�  �  J m   F GNN �OO " B a c k u p   n o w   &   q u i t�  �  G PQP r   K [RSR l  K YT��T n  K YUVU I   R Y�W�� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_W XYX o   R S�� 0 	this_item  Y Z[Z m   S T\\ �]] * b a c k u p A n d Q u i t H a n d l e r :[ ^�^ m   T U__ �``  �  �  V n  K Raba I   N R��
�	� 	0 alloc  �
  �	  b n  K Ncdc o   L N�� 0 
nsmenuitem 
NSMenuItemd m   K L�
� misccura�  �  S o      �� 0 thismenuitem thisMenuItemQ efe l  ^ cg��g =   ^ chih l  ^ aj��j c   ^ aklk o   ^ _�� 0 	this_item  l m   _ `� 
�  
TEXT�  �  i m   a bmm �nn " T u r n   o n   t h e   b a n t s�  �  f opo r   f xqrq l  f vs����s n  f vtut I   m v��v���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_v wxw o   m n���� 0 	this_item  x yzy m   n o{{ �||  n s f w O n H a n d l e r :z }��} m   o r~~ �  ��  ��  u n  f m��� I   i m�������� 	0 alloc  ��  ��  � n  f i��� o   g i���� 0 
nsmenuitem 
NSMenuItem� m   f g��
�� misccura��  ��  r o      ���� 0 thismenuitem thisMenuItemp ��� l  { ������� =   { ���� l  { ~������ c   { ~��� o   { |���� 0 	this_item  � m   | }��
�� 
TEXT��  ��  � m   ~ ��� ��� $ T u r n   o f f   t h e   b a n t s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ���  n s f w O f f H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ��� * T u r n   o n   n o t i f i c a t i o n s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ��� , n o t i f i c a t i o n O n H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ��� , T u r n   o f f   n o t i f i c a t i o n s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ��� . n o t i f i c a t i o n O f f H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ���  Q u i t��  ��  � ���� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ���  q u i t H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem��  �*  ' � � l ��������  ��  ��     l ���� n  I  ������ 0 additem_ addItem_ �� o  ���� 0 thismenuitem thisMenuItem��  ��   o  ���� 0 newmenu newMenu��  ��   	 l ��������  ��  ��  	 

 l  l ���� n  I  ������ 0 
settarget_ 
setTarget_ ��  f  ��  ��   o  ���� 0 thismenuitem thisMenuItem��  ��   * $ required for enabling the menu item    � H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m  l ��������  ��  ��   �� Z  9���� G  " l ���� =   o  ���� 0 i   m  ���� ��  ��   l ���� =   !  o  ���� 0 i  ! m  ���� ��  ��   l %5"#$" l %5%����% n %5&'& I  *5��(���� 0 additem_ addItem_( )��) l *1*����* n *1+,+ o  -1���� 0 separatoritem separatorItem, n *--.- o  +-���� 0 
nsmenuitem 
NSMenuItem. m  *+��
�� misccura��  ��  ��  ��  ' o  %*�� 0 newmenu newMenu��  ��  #   add a seperator   $ �//     a d d   a   s e p e r a t o r��  ��  ��  �3 0 i   m    �~�~  n    010 m    �}
�} 
nmbr1 n   232 2   �|
�| 
cobj3 o    �{�{ 0 thelist theList�2  �4  
 454 l     �z�y�x�z  �y  �x  5 676 l     �w�v�u�w  �v  �u  7 898 l     �t�s�r�t  �s  �r  9 :;: l     �q�p�o�q  �p  �o  ; <=< l     �n�m�l�n  �m  �l  = >?> i  ��@A@ I      �kB�j�k  0 backuphandler_ backupHandler_B C�iC o      �h�h 
0 sender  �i  �j  A Z     �DEF�gD l    G�f�eG =     HIH o     �d�d 0 isbackingup isBackingUpI m    �c
�c boovfals�f  �e  E k   
 UJJ KLK r   
 MNM m   
 �b
�b boovtrueN o      �a�a 0 manualbackup manualBackupL O�`O Z    UPQ�_�^P l   R�]�\R =    STS o    �[�[ 0 notifications  T m    �Z
�Z boovtrue�]  �\  Q Z    QUVW�YU l   #X�X�WX =    #YZY o    !�V�V 0 nsfw  Z m   ! "�U
�U boovtrue�X  �W  V k   & 3[[ \]\ I  & -�T^_
�T .sysonotfnull��� ��� TEXT^ m   & '`` �aa T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p_ �Sb�R
�S 
apprb m   ( )cc �dd 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�R  ] e�Qe I  . 3�Pf�O
�P .sysodelanull��� ��� nmbrf m   . /�N�N �O  �Q  W ghg l  6 =i�M�Li =   6 =jkj o   6 ;�K�K 0 nsfw  k m   ; <�J
�J boovfals�M  �L  h l�Il k   @ Mmm non I  @ G�Hpq
�H .sysonotfnull��� ��� TEXTp m   @ Arr �ss   P r e p a r i n g   b a c k u pq �Gt�F
�G 
apprt m   B Cuu �vv 
 W B T F U�F  o w�Ew I  H M�Dx�C
�D .sysodelanull��� ��� nmbrx m   H I�B�B �C  �E  �I  �Y  �_  �^  �`  F yzy l  X _{�A�@{ =   X _|}| o   X ]�?�? 0 isbackingup isBackingUp} m   ] ^�>
�> boovtrue�A  �@  z ~�=~ k   b � ��� r   b k��� n   b i��� 3   g i�<
�< 
cobj� o   b g�;�; 40 messagesalreadybackingup messagesAlreadyBackingUp� o      �:�: 0 msg  � ��9� Z   l ����8�7� l  l s��6�5� =   l s��� o   l q�4�4 0 notifications  � m   q r�3
�3 boovtrue�6  �5  � Z   v �����2� l  v }��1�0� =   v }��� o   v {�/�/ 0 nsfw  � m   { |�.
�. boovtrue�1  �0  � I  � ��-��
�- .sysonotfnull��� ��� TEXT� o   � ��,�, 0 msg  � �+��*
�+ 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�*  � ��� l  � ���)�(� =   � ���� o   � ��'�' 0 nsfw  � m   � ��&
�& boovfals�)  �(  � ��%� I  � ��$��
�$ .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �#��"
�# 
appr� m   � ��� ��� 
 W B T F U�"  �%  �2  �8  �7  �9  �=  �g  ? ��� l     �!� ��!  �   �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i  ����� I      ���� .0 backupandquithandler_ backupAndQuitHandler_� ��� o      �� 
0 sender  �  �  � Z     ������ l    ���� =     ��� o     �� 0 isbackingup isBackingUp� m    �

�
 boovfals�  �  � k   
 ��� ��� r   
 ��� l  
 ��	�� n   
 ��� 1    �
� 
bhit� l  
 ���� I  
 ���
� .sysodlogaskr        TEXT� m   
 �� ��� T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?� ���
� 
btns� J    �� ��� m    �� ���  Y e s� ��� m    �� ���  N o�  � ��� 
� 
dflt� m    ���� �   �  �  �	  �  � o      ���� 	0 reply  � ��� l   ��������  ��  ��  � ���� Z    ������� l   ������ =    ��� o    ���� 	0 reply  � m    �� ���  Y e s��  ��  � k   ! t�� ��� r   ! (��� m   ! "��
�� boovtrue� o      ����  0 backupthenquit backupThenQuit� ��� r   ) 0��� m   ) *��
�� boovtrue� o      ���� 0 manualbackup manualBackup� ��� l  1 1��������  ��  ��  � ���� Z   1 t������� l  1 8������ =   1 8��� o   1 6���� 0 notifications  � m   6 7��
�� boovtrue��  ��  � Z   ; p������ l  ; B������ =   ; B��� o   ; @���� 0 nsfw  � m   @ A��
�� boovtrue��  ��  � k   E R�� ��� I  E L����
�� .sysonotfnull��� ��� TEXT� m   E F�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �����
�� 
appr� m   G H�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  M R�����
�� .sysodelanull��� ��� nmbr� m   M N���� ��  ��  �    l  U \���� =   U \ o   U Z���� 0 nsfw   m   Z [��
�� boovfals��  ��   �� k   _ l  I  _ f��	

�� .sysonotfnull��� ��� TEXT	 m   _ ` �   P r e p a r i n g   b a c k u p
 ����
�� 
appr m   a b � 
 W B T F U��   �� I  g l����
�� .sysodelanull��� ��� nmbr m   g h���� ��  ��  ��  ��  ��  ��  ��  �  l  w |���� =   w | o   w x���� 	0 reply   m   x { �  N o��  ��   �� r    � m    ���
�� boovfals o      ����  0 backupthenquit backupThenQuit��  ��  ��  �  l  � ����� =   � �  o   � ����� 0 isbackingup isBackingUp  m   � ���
�� boovtrue��  ��   !��! k   � �"" #$# r   � �%&% n   � �'(' 3   � ���
�� 
cobj( o   � ����� 40 messagesalreadybackingup messagesAlreadyBackingUp& o      ���� 0 msg  $ )��) Z   � �*+����* l  � �,����, =   � �-.- o   � ����� 0 notifications  . m   � ���
�� boovtrue��  ��  + Z   � �/01��/ l  � �2����2 =   � �343 o   � ����� 0 nsfw  4 m   � ���
�� boovtrue��  ��  0 I  � ���56
�� .sysonotfnull��� ��� TEXT5 o   � ����� 0 msg  6 ��7��
�� 
appr7 m   � �88 �99 P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  1 :;: l  � �<����< =   � �=>= o   � ����� 0 nsfw  > m   � ���
�� boovfals��  ��  ; ?��? I  � ���@A
�� .sysonotfnull��� ��� TEXT@ m   � �BB �CC 2 Y o u ' r e   a l r e a d y   b a c k i n g   u pA ��D��
�� 
apprD m   � �EE �FF 
 W B T F U��  ��  ��  ��  ��  ��  ��  �  � GHG l     ��������  ��  ��  H IJI l     ��������  ��  ��  J KLK l     ��������  ��  ��  L MNM l     ��������  ��  ��  N OPO l     ��������  ��  ��  P QRQ i  ��STS I      ��U����  0 nsfwonhandler_ nsfwOnHandler_U V��V o      ���� 
0 sender  ��  ��  T k     =WW XYX r     Z[Z m     ��
�� boovtrue[ o      ���� 0 nsfw  Y \��\ Z    =]^_��] l   `����` =    aba o    ���� 0 notifications  b m    �
� boovtrue��  ��  ^ r    cdc J    ee fgf m    hh �ii  B a c k u p   n o wg jkj m    ll �mm " B a c k u p   n o w   &   q u i tk non m    pp �qq $ T u r n   o f f   t h e   b a n t so rsr m    tt �uu , T u r n   o f f   n o t i f i c a t i o n ss v�v m    ww �xx  Q u i t�  d o      �� 0 thelist theList_ yzy l  " ){��{ =   " )|}| o   " '�� 0 notifications  } m   ' (�
� boovfals�  �  z ~�~ r   , 9� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��� m   0 1�� ���  Q u i t�  � o      �� 0 thelist theList�  ��  ��  R ��� l     ����  �  �  � ��� i  ����� I      ���� "0 nsfwoffhandler_ nsfwOffHandler_� ��� o      �� 
0 sender  �  �  � k     =�� ��� r     ��� m     �~
�~ boovfals� o      �}�} 0 nsfw  � ��|� Z    =����{� l   ��z�y� =    ��� o    �x�x 0 notifications  � m    �w
�w boovtrue�z  �y  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� " T u r n   o n   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��v� m    �� ���  Q u i t�v  � o      �u�u 0 thelist theList� ��� l  " )��t�s� =   " )��� o   " '�r�r 0 notifications  � m   ' (�q
�q boovfals�t  �s  � ��p� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��o� m   0 1�� ���  Q u i t�o  � o      �n�n 0 thelist theList�p  �{  �|  � ��� l     �m�l�k�m  �l  �k  � ��� l     �j�i�h�j  �i  �h  � ��� l     �g�f�e�g  �f  �e  � ��� l     �d�c�b�d  �c  �b  � ��� l     �a�`�_�a  �`  �_  � ��� i  ����� I      �^��]�^ 00 notificationonhandler_ notificationOnHandler_� ��\� o      �[�[ 
0 sender  �\  �]  � k     =�� ��� r     ��� m     �Z
�Z boovtrue� o      �Y�Y 0 notifications  � ��X� Z    =����W� l   ��V�U� =    ��� o    �T�T 0 nsfw  � m    �S
�S boovtrue�V  �U  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m       � " B a c k u p   n o w   &   q u i t�  m     � $ T u r n   o f f   t h e   b a n t s  m     �		 , T u r n   o f f   n o t i f i c a t i o n s 
�R
 m     �  Q u i t�R  � o      �Q�Q 0 thelist theList�  l  " )�P�O =   " ) o   " '�N�N 0 nsfw   m   ' (�M
�M boovfals�P  �O   �L r   , 9 J   , 3  m   , - �  B a c k u p   n o w  m   - . � " B a c k u p   n o w   &   q u i t  m   . /   �!! " T u r n   o n   t h e   b a n t s "#" m   / 0$$ �%% , T u r n   o f f   n o t i f i c a t i o n s# &�K& m   0 1'' �((  Q u i t�K   o      �J�J 0 thelist theList�L  �W  �X  � )*) l     �I�H�G�I  �H  �G  * +,+ i  ��-.- I      �F/�E�F 20 notificationoffhandler_ notificationOffHandler_/ 0�D0 o      �C�C 
0 sender  �D  �E  . k     =11 232 r     454 m     �B
�B boovfals5 o      �A�A 0 notifications  3 6�@6 Z    =789�?7 l   :�>�=: =    ;<; o    �<�< 0 nsfw  < m    �;
�; boovtrue�>  �=  8 r    =>= J    ?? @A@ m    BB �CC  B a c k u p   n o wA DED m    FF �GG " B a c k u p   n o w   &   q u i tE HIH m    JJ �KK $ T u r n   o f f   t h e   b a n t sI LML m    NN �OO * T u r n   o n   n o t i f i c a t i o n sM P�:P m    QQ �RR  Q u i t�:  > o      �9�9 0 thelist theList9 STS l  " )U�8�7U =   " )VWV o   " '�6�6 0 nsfw  W m   ' (�5
�5 boovfals�8  �7  T X�4X r   , 9YZY J   , 3[[ \]\ m   , -^^ �__  B a c k u p   n o w] `a` m   - .bb �cc " B a c k u p   n o w   &   q u i ta ded m   . /ff �gg " T u r n   o n   t h e   b a n t se hih m   / 0jj �kk * T u r n   o n   n o t i f i c a t i o n si l�3l m   0 1mm �nn  Q u i t�3  Z o      �2�2 0 thelist theList�4  �?  �@  , opo l     �1�0�/�1  �0  �/  p qrq l     �.�-�,�.  �-  �,  r sts l     �+�*�)�+  �*  �)  t uvu l     �(�'�&�(  �'  �&  v wxw l     �%�$�#�%  �$  �#  x yzy i  ��{|{ I      �"}�!�" 0 quithandler_ quitHandler_} ~� ~ o      �� 
0 sender  �   �!  | Z     u��� l    ���� =     ��� o     �� 0 isbackingup isBackingUp� m    �
� boovtrue�  �  � k   
 4�� ��� n  
 ��� I    ���� 0 setfocus setFocus�  �  �  f   
 � ��� r     ��� l   ���� n    ��� 1    �
� 
bhit� l   ���� I   ���
� .sysodlogaskr        TEXT� m    �� ��� � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?� ���
� 
btns� J    �� ��� m    �� ���  C a n c e l   &   Q u i t� ��� m    �� ���  R e s u m e�  � ���
� 
dflt� m    �� �  �  �  �  �  � o      �� 	0 reply  � ��
� Z   ! 4���	�� l  ! $���� =   ! $��� o   ! "�� 	0 reply  � m   " #�� ���  C a n c e l   &   Q u i t�  �  � I  ' ,���
� .aevtquitnull��� ��� null� m   ' (��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  �	  � I  / 4���
� .ascrcmnt****      � ****� l  / 0��� � m   / 0�� ���  W B T F U   r e s u m e d�  �   �  �
  � ��� l  7 >������ =   7 >��� o   7 <���� 0 isbackingup isBackingUp� m   < =��
�� boovfals��  ��  � ���� k   A q�� ��� n  A F��� I   B F�������� 0 setfocus setFocus��  ��  �  f   A B� ��� r   G Y��� l  G W������ n   G W��� 1   U W��
�� 
bhit� l  G U������ I  G U����
�� .sysodlogaskr        TEXT� m   G H�� ��� < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?� ����
�� 
btns� J   I O�� ��� m   I J�� ���  Q u i t� ���� m   J M�� ���  R e s u m e��  � �����
�� 
dflt� m   P Q���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ���� Z   Z q������ l  Z _������ =   Z _��� o   Z [���� 	0 reply  � m   [ ^�� ���  Q u i t��  ��  � I  b g�����
�� .aevtquitnull��� ��� null� m   b c��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  � I  j q�����
�� .ascrcmnt****      � ****� l  j m������ m   j m�� ���  W B T F U   r e s u m e d��  ��  ��  ��  ��  �  z ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� (0 animatemenubaricon animateMenuBarIcon��  ��  � k     .�� ��� r     ��� 4     ���
�� 
alis� l   ������ b    ��� l   	������ I   	����
�� .earsffdralis        afdr�  f    � �����
�� 
rtyp� m    ��
�� 
ctxt��  ��  ��  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g��  ��  � o      ���� 0 	imagepath 	imagePath� ��� r       n     1    ��
�� 
psxp o    ���� 0 	imagepath 	imagePath o      ���� 0 	imagepath 	imagePath�  r    # n   !	 I    !��
���� 20 initwithcontentsoffile_ initWithContentsOfFile_
 � o    �� 0 	imagepath 	imagePath�  ��  	 n    I    ���� 	0 alloc  �  �   n    o    �� 0 nsimage NSImage m    �
� misccura o      �� 	0 image   � n  $ . I   ) .��� 0 	setimage_ 	setImage_ � o   ) *�� 	0 image  �  �   o   $ )�� 0 
statusitem 
StatusItem�  �  l     ����  �  �    l     ����  �  �    l     ����  �  �    l     ����  �  �    l     ����  �  �     i  ��!"! I      ���� $0 resetmenubaricon resetMenuBarIcon�  �  " k     .## $%$ r     &'& 4     �(
� 
alis( l   )��) b    *+* l   	,��, I   	�-.
� .earsffdralis        afdr-  f    . �/�
� 
rtyp/ m    �
� 
ctxt�  �  �  + m   	 
00 �11 J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�  �  ' o      �� 0 	imagepath 	imagePath% 232 r    454 n    676 1    �
� 
psxp7 o    �� 0 	imagepath 	imagePath5 o      �� 0 	imagepath 	imagePath3 898 r    #:;: n   !<=< I    !�>�� 20 initwithcontentsoffile_ initWithContentsOfFile_> ?�? o    �� 0 	imagepath 	imagePath�  �  = n   @A@ I    ���� 	0 alloc  �  �  A n   BCB o    �� 0 nsimage NSImageC m    �
� misccura; o      �� 	0 image  9 D�D n  $ .EFE I   ) .�G�� 0 	setimage_ 	setImage_G H�H o   ) *�� 	0 image  �  �  F o   $ )�� 0 
statusitem 
StatusItem�    IJI l     ����  �  �  J KLK l     ��~�}�  �~  �}  L MNM l     �|�{�z�|  �{  �z  N OPO l     �y�x�w�y  �x  �w  P QRQ l     �v�u�t�v  �u  �t  R STS l     �sUV�s  U   create an NSStatusBar   V �WW ,   c r e a t e   a n   N S S t a t u s B a rT XYX i  ��Z[Z I      �r�q�p�r 0 makestatusbar makeStatusBar�q  �p  [ k     r\\ ]^] l     �o_`�o  _  log ("Make status bar")   ` �aa . l o g   ( " M a k e   s t a t u s   b a r " )^ bcb r     ded n    fgf o    �n�n "0 systemstatusbar systemStatusBarg n    hih o    �m�m 0 nsstatusbar NSStatusBari m     �l
�l misccurae o      �k�k 0 bar  c jkj l   �j�i�h�j  �i  �h  k lml r    non n   pqp I   	 �gr�f�g .0 statusitemwithlength_ statusItemWithLength_r s�es m   	 
tt ��      �e  �f  q o    	�d�d 0 bar  o o      �c�c 0 
statusitem 
StatusItemm uvu l   �b�a�`�b  �a  �`  v wxw r    #yzy 4    !�_{
�_ 
alis{ l    |�^�]| b     }~} l   �\�[ I   �Z��
�Z .earsffdralis        afdr�  f    � �Y��X
�Y 
rtyp� m    �W
�W 
ctxt�X  �\  �[  ~ m    �� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�^  �]  z o      �V�V 0 	imagepath 	imagePathx ��� r   $ )��� n   $ '��� 1   % '�U
�U 
psxp� o   $ %�T�T 0 	imagepath 	imagePath� o      �S�S 0 	imagepath 	imagePath� ��� r   * 8��� n  * 6��� I   1 6�R��Q�R 20 initwithcontentsoffile_ initWithContentsOfFile_� ��P� o   1 2�O�O 0 	imagepath 	imagePath�P  �Q  � n  * 1��� I   - 1�N�M�L�N 	0 alloc  �M  �L  � n  * -��� o   + -�K�K 0 nsimage NSImage� m   * +�J
�J misccura� o      �I�I 	0 image  � ��� l  9 9�H�G�F�H  �G  �F  � ��� l  9 9�E���E  � � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   � ��� s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "� ��� l  9 9�D�C�B�D  �C  �B  � ��� n  9 C��� I   > C�A��@�A 0 	setimage_ 	setImage_� ��?� o   > ?�>�> 	0 image  �?  �@  � o   9 >�=�= 0 
statusitem 
StatusItem� ��� l  D D�<�;�:�<  �;  �:  � ��� l  D D�9���9  � , & set up the initial NSStatusBars title   � ��� L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e� ��� l  D D�8���8  � # StatusItem's setTitle:"WBTFU"   � ��� : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "� ��� l  D D�7�6�5�7  �6  �5  � ��� l  D D�4���4  � 1 + set up the initial NSMenu of the statusbar   � ��� V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r� ��� r   D X��� n  D R��� I   K R�3��2�3  0 initwithtitle_ initWithTitle_� ��1� m   K N�� ���  C u s t o m�1  �2  � n  D K��� I   G K�0�/�.�0 	0 alloc  �/  �.  � n  D G��� o   E G�-�- 0 nsmenu NSMenu� m   D E�,
�, misccura� o      �+�+ 0 newmenu newMenu� ��� l  Y Y�*�)�(�*  �)  �(  � ��� l  Y Y�'���'  � � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   � ���0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .� ��� n  Y c��� I   ^ c�&��%�& 0 setdelegate_ setDelegate_� ��$�  f   ^ _�$  �%  � o   Y ^�#�# 0 newmenu newMenu� ��� l  d d�"�!� �"  �!  �   � ��� n  d r��� I   i r���� 0 setmenu_ setMenu_� ��� o   i n�� 0 newmenu newMenu�  �  � o   d i�� 0 
statusitem 
StatusItem�  Y ��� l     ����  �  �  � ��� l     ����  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ��
�	�  �
  �	  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i  ����� I      ��� � 0 runonce runOnce�  �   � k     �� ��� l      ������  � � �if not (current application's NSThread's isMainThread()) as boolean then		display alert "This script must be run from the main thread." buttons {"Cancel"} as critical		error number -128	end if   � ���� i f   n o t   ( c u r r e n t   a p p l i c a t i o n ' s   N S T h r e a d ' s   i s M a i n T h r e a d ( ) )   a s   b o o l e a n   t h e n  	 	 d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l  	 	 e r r o r   n u m b e r   - 1 2 8  	 e n d   i f� ��� l     ��������  ��  ��  � � � I     �������� (0 animatemenubaricon animateMenuBarIcon��  ��    �� I    �������� 0 	plistinit 	plistInit��  ��  ��  �  l     ��������  ��  ��    l    ���� n     I    �������� 0 makestatusbar makeStatusBar��  ��    f     ��  ��   	
	 l   ���� I    �������� 0 runonce runOnce��  ��  ��  ��  
  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   w q Function that is always running in the background. This doesn't need to get called as it is running from the off    � �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f  l     ����   � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.    ��   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .  ��  i  ��!"! I     ������
�� .miscidlenmbr    ��� null��  ��  " k     �## $%$ Z     �&'����& l    (����( E     )*) o     ���� &0 displaysleepstate displaySleepState* o    
���� 0 strawake strAwake��  ��  ' Z    �+,����+ l   -����- =    ./. o    ���� 0 isbackingup isBackingUp/ m    ��
�� boovfals��  ��  , Z    �012��0 l   3����3 =    454 o    ���� 0 manualbackup manualBackup5 m    ��
�� boovfals��  ��  1 Z   " s67����6 l  " +8��8 A   " +9:9 l  " );��; n   " )<=< 1   ' )�
� 
hour= l  " '>��> I  " '���
� .misccurdldt    ��� null�  �  �  �  �  �  : m   ) *�� �  �  7 k   . o?? @A@ r   . 7BCB l  . 5D��D n   . 5EFE 1   3 5�
� 
min F l  . 3G��G l  . 3H��H I  . 3���
� .misccurdldt    ��� null�  �  �  �  �  �  �  �  C o      �� 0 m  A IJI l  8 8����  �  �  J K�K Z   8 oLMN�L G   8 COPO l  8 ;Q��Q =   8 ;RSR o   8 9�� 0 m  S m   9 :�� �  �  P l  > AT��T =   > AUVU o   > ?�� 0 m  V m   ? @�� -�  �  M Z   F UWX��W l  F IY��Y =   F IZ[Z o   F G�� 0 scheduledtime scheduledTime[ m   G H�� �  �  X I   L Q���� 0 	plistinit 	plistInit�  �  �  �  N \]\ G   X c^_^ l  X [`��` =   X [aba o   X Y�� 0 m  b m   Y Z��  �  �  _ l  ^ ac��c =   ^ aded o   ^ _�� 0 m  e m   _ `�� �  �  ] f�f I   f k���� 0 	plistinit 	plistInit�  �  �  �  �  ��  ��  2 ghg l  v }i��i =   v }jkj o   v {�� 0 manualbackup manualBackupk m   { |�
� boovtrue�  �  h l�l I   � ����� 0 	plistinit 	plistInit�  �  �  ��  ��  ��  ��  ��  % mnm l  � ���~�}�  �~  �}  n o�|o L   � �pp m   � ��{�{ �|  ��       5�zqr�y gstuvw�x�w�vx�u�t�syz{|}��~����������������������������z  q 3�r�q�p�o�n�m�l�k�j�i�h�g�f�e�d�c�b�a�`�_�^�]�\�[�Z�Y�X�W�V�U�T�S�R�Q�P�O�N�M�L�K�J�I�H�G�F�E�D�C�B�A�@
�r 
pimr�q 0 
statusitem 
StatusItem�p 0 
thedisplay 
theDisplay�o 0 defaults  �n $0 internalmenuitem internalMenuItem�m $0 externalmenuitem externalMenuItem�l 0 newmenu newMenu�k 0 thelist theList�j 0 nsfw  �i 0 notifications  �h  0 backupthenquit backupThenQuit�g 	0 plist  �f 0 initialbackup initialBackup�e 0 manualbackup manualBackup�d 0 isbackingup isBackingUp�c "0 messagesmissing messagesMissing�b *0 messagesencouraging messagesEncouraging�a $0 messagescomplete messagesComplete�` &0 messagescancelled messagesCancelled�_ 40 messagesalreadybackingup messagesAlreadyBackingUp�^ 0 strawake strAwake�] 0 strsleep strSleep�\ &0 displaysleepstate displaySleepState�[ 0 	plistinit 	plistInit�Z 0 diskinit diskInit�Y 0 freespaceinit freeSpaceInit�X 0 
backupinit 
backupInit�W 0 tidyup tidyUp�V 
0 backup  �U 0 itexists itExists�T .0 getcomputeridentifier getComputerIdentifier�S 0 gettimestamp getTimestamp�R 0 comparesizes compareSizes�Q 0 	stopwatch  �P 0 getduration getDuration�O 0 setfocus setFocus�N $0 menuneedsupdate_ menuNeedsUpdate_�M 0 	makemenus 	makeMenus�L  0 backuphandler_ backupHandler_�K .0 backupandquithandler_ backupAndQuitHandler_�J  0 nsfwonhandler_ nsfwOnHandler_�I "0 nsfwoffhandler_ nsfwOffHandler_�H 00 notificationonhandler_ notificationOnHandler_�G 20 notificationoffhandler_ notificationOffHandler_�F 0 quithandler_ quitHandler_�E (0 animatemenubaricon animateMenuBarIcon�D $0 resetmenubaricon resetMenuBarIcon�C 0 makestatusbar makeStatusBar�B 0 runonce runOnce
�A .miscidlenmbr    ��� null
�@ .aevtoappnull  �   � ****r �?��? �  ����� �> L�=
�> 
vers�=  � �<��;
�< 
cobj� ��   �:
�: 
osax�;  � �9��8
�9 
cobj� ��   �7 U
�7 
frmk�8  � �6��5
�6 
cobj� ��   �4 [
�4 
frmk�5  
�y 
msngs �� �3�2�
�3 misccura
�2 
pcls� ���  N S U s e r D e f a u l t st �� �1�0�
�1 misccura
�0 
pcls� ���  N S M e n u I t e mu �� �/�.�
�/ misccura
�. 
pcls� ���  N S M e n u I t e mv �� �-�,�
�- misccura
�, 
pcls� ���  N S M e n uw �+��+ �   � � � � �
�x boovfals
�w boovtrue
�v boovfalsx ��� � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�u boovtrue
�t boovfals
�s boovfalsy �*��* �   � �	z �)��) 	� 	 #'+/2{ �(��( 	� 	 <@DHLPTX[| �'��' �  eimquy}�} �&��& 	� 	 ���������~ ����             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 3 0 1 7 3 5 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 4 8 7 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 0 0 , " I d l e T i m e r E l a p s e d T i m e " = 2 5 8 6 8 7 0 , " C u r r e n t P o w e r S t a t e " = 4 } �%��$�#���"�% 0 	plistinit 	plistInit�$  �#  � �!� �������! 0 
foldername 
folderName�  0 
backupdisk 
backupDisk�  0 computerfolder computerFolder� 0 
backuptime 
backupTime� 0 thecontents theContents� 0 thevalue theValue� 0 backuptimes backupTimes� 0 selectedtime selectedTime� 2��2��������������[��
�	tx{������������� ����������������������� 0 itexists itExists
� 
plif
� 
pcnt
� 
valL�  0 foldertobackup FolderToBackup� 0 backupdrive BackupDrive�  0 computerfolder computerFolder� 0 scheduledtime scheduledTime� .0 getcomputeridentifier getComputerIdentifier�  ��� 0 setfocus setFocus
� afdrcusr
� .earsffdralis        afdr
� 
prmp
� .sysostflalis    ��� null
�
 
TEXT
�	 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
�  .corecrel****      � null
�� 
plii
�� 
insh
�� 
kind�� �� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 machinefolder machineFolder
�� .ascrcmnt****      � ****�� 0 diskinit diskInit�"�jE�O*�b  l+ e  6� .*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYX*j+ 
E�O� ��n)j+ O�j E�O*a a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O�a a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a  �a !a "b  la # $ �*a  a %a &*6a !a 'a a "a (�a )a ) $O*a  a %a &*6a !a 'a a "a *�a )a ) $O*a  a %a &*6a !a 'a a "a +�a )a ) $O*a  a %a &*6a !a 'a a "a ,�a )a ) $UUO�E` -O�E` .O�E` /O�E�O_ -_ ._ /�a #vj 0O*j+ 1� ��?���������� 0 diskinit diskInit��  ��  � ������ 0 msg  �� 	0 reply  � I������������hk��������u����������� "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit�� 0 setfocus setFocus
�� 
cobj
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit
�� 
appr
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  *fk+ Y �)j+ Ob  �.E�O����lv�l� �,E�O��  
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� ������������� 0 freespaceinit freeSpaceInit�� ����� �  ���� 	0 again  ��  � �������� 	0 again  �� 	0 reply  �� 0 msg  � ������������������������������1��;>�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes�� 0 
backupinit 
backupInit�� 0 setfocus setFocus
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp
�� 
cobj
�� 
appr
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  
*j+ Y �)j+ O�f  ����lv�l� �,E�Y �e  ����lv�l� �,E�Y hO�a   
*j+ Y ab  b   b  a .E�Y hOb  	e  8b  e  �a a l Y b  f  a a a l Y hY h� ��W���������� 0 
backupinit 
backupInit��  ��  �  � ������ot������������������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  �� 0 itexists itExists
�� 
kocl
�� 
cfol
�� 
insh
�� 
prdt
�� 
pnam� 
� .corecrel****      � null� 0 machinefolder machineFolder
� 
cdis� 0 backupfolder backupFolder� 0 latestfolder latestFolder� 
0 backup  �� �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` OPUO*j+ � �2������ 0 tidyup tidyUp�  �  � ��������� 0 bf bF� 0 creationdates creationDates� 0 theoldestdate theOldestDate� 0 j  � 0 i  � 0 thisdate thisDate� 
0 reply2  � 0 msg  � &���@��N�����|�����������������	�'*�9<
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  
� 
cdis
� 
cfol� 0 machinefolder machineFolder
� 
cobj
� 
ascd
� .corecnte****       ****
� 
pnam
� .ascrcmnt****      � ****
� .coredeloobj        obj � 0 setfocus setFocus
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� 
trsh
� .fndremptnull��� ��� obj 
� 
appr
� .sysonotfnull��� ��� TEXT� 0 freespaceinit freeSpaceInit
� .sysodelanull��� ��� nmbr�{*��/E�O�p*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/j O)j+ Oa a a a lva la  a ,E�O�a   *a ,j Y _b  b   Pb  �.E�Ob  	e  8b  e  �a a l Y b  f  a a a l Y hY hY hO*ek+  Ob  b   Tb  	e  Fb  e  a !a a "l Okj #Y #b  f  a $a a %l Okj #Y hY hY hU� �]������ 
0 backup  �  �  � 
����������� 0 t  � 0 x  � "0 containerfolder containerFolder� 0 
foldername 
folderName� 0 d  � 0 duration  � 0 msg  � 0 c  � 0 oldestfolder oldestFolder� 0 todelete toDelete� i���}�
������~�}�|�{�z�y�x�w�v�u��t�s�r�q�pQ�oT�n^a�m|~�����l�k�j�i�h�g����f(�eEGIK^`bj�d��������c�b	#	/	1	n	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�
/
1
4
G
I
L
a
c
f
y
{
~
�� 0 gettimestamp getTimestamp� (0 animatemenubaricon animateMenuBarIcon
� .sysodelanull��� ��� nmbr� 0 	stopwatch  
� 
psxf� 0 sourcefolder sourceFolder
� 
TEXT
� 
cfol
�~ 
pnam
�} 
kocl
�| 
insh�{ 0 backupfolder backupFolder
�z 
prdt�y 
�x .corecrel****      � null�w (0 activesourcefolder activeSourceFolder
�v 
cdis�u 	0 drive  �t 0 machinefolder machineFolder�s (0 activebackupfolder activeBackupFolder
�r .misccurdldt    ��� null
�q 
time�p 0 	starttime 	startTime
�o 
appr
�n .sysonotfnull��� ��� TEXT�m "0 destinationdisk destinationDisk
�l .sysoexecTEXT���     TEXT�k  �j  �i 0 endtime endTime�h 0 getduration getDuration
�g 
cobj�f $0 resetmenubaricon resetMenuBarIcon
�e .aevtquitnull��� ��� null
�d .ascrcmnt****      � ****
�c 
alis
�b 
psxp��*ek+  E�OeEc  O*j+ Okj O*�k+ O�R*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e b*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_  a !%_ %a "%�%a #%�&E�O a $�%a %%�%a &%j 'OPW X ( )hOfEc  OfEc  Ob  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  Tb  e   a -�%a .%�%a a /l Okj Y )b  f  a 0�%a 1%a a 2l Okj Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYxb  f m_  a 6%_ %a 7%�%a 8%�%a 9%�&E�O_  a :%_ %a ;%�%a <%�&E�Oa =�%j >Ob  b   l*j a ,E` Ob  a ,.E�Ob  	e  Db  e  �a a ?l Okj Y #b  f  a @a a Al Okj Y hY hY hO  a B�%a C%�%a D%�%a E%j 'OPW X ( )hOfEc  OfEc  O*�/a F&a ,-jv [��/�&E�O�a G,�&E�Oa H�%j >Oa I�%a J%E�O�j 'Ob  b  *j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  �a K  Tb  e   a L�%a M%�%a a Nl Okj Y )b  f  a O�%a P%a a Ql Okj Y hY Qb  e   a R�%a S%�%a a Tl Okj Y )b  f  a U�%a V%a a Wl Okj Y hOb  e  a Xa a Yl Y b  f  a Za a [l Y hY hO)j+ 3Y hY	b  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  ��a K  Tb  e   a \�%a ]%�%a a ^l Okj Y )b  f  a _�%a `%a a al Okj Y hY Qb  e   a b�%a c%�%a a dl Okj Y )b  f  a e�%a f%a a gl Okj Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a hk+ � �a
��`�_���^�a 0 itexists itExists�` �]��] �  �\�[�\ 0 
objecttype 
objectType�[ 
0 object  �_  � �Z�Y�Z 0 
objecttype 
objectType�Y 
0 object  � 
�
��X�W
��V
��U
�X 
cdis
�W .coredoexnull���     ****
�V 
file
�U 
cfol�^ X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU� �T�S�R���Q�T .0 getcomputeridentifier getComputerIdentifier�S  �R  � �P�O�N�P 0 computername computerName�O "0 strserialnumber strSerialNumber�N  0 identifiername identifierName� �M�L!�K,
�M .sysosigtsirr   ��� null
�L 
sicn
�K .sysoexecTEXT���     TEXT�Q *j  �,E�O�j E�O��%�%E�O�� �JG�I�H���G�J 0 gettimestamp getTimestamp�I �F��F �  �E�E 0 isfolder isFolder�H  � �D�C�B�A�@�?�>�=�<�;�:�9�8�7�6�5�D 0 isfolder isFolder�C 0 y  �B 0 m  �A 0 d  �@ 0 t  �? 0 ty tY�> 0 tm tM�= 0 td tD�< 0 tt tT�; 
0 tml tML�: 
0 tdl tDL�9 0 timestr timeStr�8 0 pos  �7 0 h  �6 0 s  �5 0 	timestamp  � �4�3�2�1�0�/�.�-�,�+�*�)�(�'�&���%�$�#��"�!� �7jl�
�4 
Krtn
�3 
year�2 0 y  
�1 
mnth�0 0 m  
�/ 
day �. 0 d  
�- 
time�, 0 t  �+ 
�* .misccurdldt    ��� null
�) 
long
�( 
TEXT
�' .corecnte****       ****
�& 
nmbr
�% 
tstr
�$ misccura
�# 
psof
�" 
psin�! 
�  .sysooffslong    ��� null
� 
cha �G�*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�� �������� 0 comparesizes compareSizes� ��� �  ��� 
0 source  � 0 destination  �  � ��������������
�	�� 
0 source  � 0 destination  � 0 fit  � 0 
templatest 
tempLatest� $0 latestfoldersize latestFolderSize� 0 c  � 0 l  � 0 md  � 0 	cachesize 	cacheSize� 0 logssize logsSize� 0 mdsize mdSize� 
0 buffer  � (0 adjustedfoldersize adjustedFolderSize�
 0 
foldersize 
folderSize�	 0 	freespace 	freeSpace� 0 temp  � !���������� ����)+��������������ce��������
� 
psxf� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� afdrdlib
� 
from
� fldmfldu
� .earsffdralis        afdr
�  
psxp
�� .sysoexecTEXT���     TEXT
�� misccura�� �� d
�� .sysorondlong        doub
�� 
cdis
�� 
frsp
�� .ascrcmnt****      � ****��eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O�*�%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�O��%�%j Ob  f  1a �%a  %j E�Oa  �a !a  j Ua !E�OPY hUOb  f  ,��E�O�j ��l E�Y hO��� fE�Y hY b  e  ��� fE�Y hY hO�� ��C���������� 0 	stopwatch  �� ����� �  ���� 0 mode  ��  � ������ 0 mode  �� 0 x  � U��c��jy�� 0 gettimestamp getTimestamp
�� .ascrcmnt****      � ****�� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP� ������������� 0 getduration getDuration��  ��  � ���� 0 duration  � �������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
�� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�� ������������� 0 setfocus setFocus��  ��  �  � ���
�� .miscactvnull��� ��� null�� � *j U� ������������� $0 menuneedsupdate_ menuNeedsUpdate_�� ����� �  ��
�� 
cmnu��  �  � ���� 0 	makemenus 	makeMenus�� )j+  � ������������ 0 	makemenus 	makeMenus��  ��  � �������� 0 i  �� 0 	this_item  �� 0 thismenuitem thisMenuItem� "��������1������=@��N\_m{~������������������������  0 removeallitems removeAllItems
�� 
cobj
�� 
nmbr
�� 
TEXT
�� misccura�� 0 
nsmenuitem 
NSMenuItem�� 	0 alloc  �� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�� 0 additem_ addItem_�� 0 
settarget_ 
setTarget_�� 
�� 
bool�� 0 separatoritem separatorItem��?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY��� ��A���������  0 backuphandler_ backupHandler_�� ��� �  �� 
0 sender  ��  � ��� 
0 sender  � 0 msg  � `�c��ru����
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h� �������� .0 backupandquithandler_ backupAndQuitHandler_� ��� �  �� 
0 sender  �  � ���� 
0 sender  � 	0 reply  � 0 msg  � ���������������8BE
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� �b  f  �����lv�l� �,E�O��  XeEc  
OeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� �T������  0 nsfwonhandler_ nsfwOnHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � hlptw������� � >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �������� "0 nsfwoffhandler_ nsfwOffHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � ������������ � >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �������� 00 notificationonhandler_ notificationOnHandler_� ��� �  ���� 
0 sender  �  � ���� 
0 sender  � � �� $'�� � >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� ��.���������� 20 notificationoffhandler_ notificationOffHandler_�� ����� �  ���� 
0 sender  ��  � ���� 
0 sender  � BFJNQ��^bfjm�� �� >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� ��|���������� 0 quithandler_ quitHandler_�� ����� �  �� 
0 sender  ��  � �~�}�~ 
0 sender  �} 	0 reply  � �|��{���z�y�x�w���v��u������| 0 setfocus setFocus
�{ 
btns
�z 
dflt�y 
�x .sysodlogaskr        TEXT
�w 
bhit
�v .aevtquitnull��� ��� null
�u .ascrcmnt****      � ****�� vb  e  /)j+  O����lv�l� �,E�O��  
�j Y �j Y @b  f  5)j+  O���a lv�l� �,E�O�a   
�j Y 	a j Y h� �t��s�r���q�t (0 animatemenubaricon animateMenuBarIcon�s  �r  � �p�o�p 0 	imagepath 	imagePath�o 	0 image  � �n�m�l�k��j�i�h�g�f�e
�n 
alis
�m 
rtyp
�l 
ctxt
�k .earsffdralis        afdr
�j 
psxp
�i misccura�h 0 nsimage NSImage�g 	0 alloc  �f 20 initwithcontentsoffile_ initWithContentsOfFile_�e 0 	setimage_ 	setImage_�q /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
� �d"�c�b���a�d $0 resetmenubaricon resetMenuBarIcon�c  �b  � �`�_�` 0 	imagepath 	imagePath�_ 	0 image  � �^�]�\�[0�Z�Y�X�W�V�U
�^ 
alis
�] 
rtyp
�\ 
ctxt
�[ .earsffdralis        afdr
�Z 
psxp
�Y misccura�X 0 nsimage NSImage�W 	0 alloc  �V 20 initwithcontentsoffile_ initWithContentsOfFile_�U 0 	setimage_ 	setImage_�a /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
� �T[�S�R���Q�T 0 makestatusbar makeStatusBar�S  �R  � �P�O�N�P 0 bar  �O 0 	imagepath 	imagePath�N 	0 image  � �M�L�Kt�J�I�H�G�F��E�D�C�B�A�@��?�>�=
�M misccura�L 0 nsstatusbar NSStatusBar�K "0 systemstatusbar systemStatusBar�J .0 statusitemwithlength_ statusItemWithLength_
�I 
alis
�H 
rtyp
�G 
ctxt
�F .earsffdralis        afdr
�E 
psxp�D 0 nsimage NSImage�C 	0 alloc  �B 20 initwithcontentsoffile_ initWithContentsOfFile_�A 0 	setimage_ 	setImage_�@ 0 nsmenu NSMenu�?  0 initwithtitle_ initWithTitle_�> 0 setdelegate_ setDelegate_�= 0 setmenu_ setMenu_�Q s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ � �<��;�:���9�< 0 runonce runOnce�;  �:  �  � �8�7�8 (0 animatemenubaricon animateMenuBarIcon�7 0 	plistinit 	plistInit�9 *j+  O*j+ � �6"�5�4���3
�6 .miscidlenmbr    ��� null�5  �4  � �2�2 0 m  � 
�1�0�/�.�-�,�+�*�)�(
�1 .misccurdldt    ��� null
�0 
hour�/ 
�. 
min �- �, -
�+ 
bool�* 0 scheduledtime scheduledTime�) 0 	plistinit 	plistInit�( �3 �b  b   �b  f  vb  f  V*j  �,� F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY b  e  
*j+ Y hY hY hO�� �'��&�%���$
�' .aevtoappnull  �   � ****� k         	�#�#  �&  �%  �  � �"�!�" 0 makestatusbar makeStatusBar�! 0 runonce runOnce�$ )j+  O*j+  ascr  ��ޭ