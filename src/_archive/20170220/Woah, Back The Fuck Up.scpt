FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 A ; Global variable declaration for use in different functions    6 � 7 7 v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s 4  8 9 8 p       : : �� ;�� 	0 plist   ; �� <�� 0 sourcefolder sourceFolder < �� =�� "0 destinationdisk destinationDisk = �� >�� 	0 drive   > �� ?�� 0 machinefolder machineFolder ? �� @�� 0 backupfolder backupFolder @ �� A�� (0 activesourcefolder activeSourceFolder A �� B�� (0 activebackupfolder activeBackupFolder B �� C�� 0 latestfolder latestFolder C ������ 0 initialbackup initialBackup��   9  D E D p       F F �� G�� 0 scheduledtime scheduledTime G �� H�� 0 	starttime 	startTime H ������ 0 endtime endTime��   E  I J I p       K K �� L�� 0 isbackingup isBackingUp L �� M�� "0 messagesmissing messagesMissing M �� N�� *0 messagesencouraging messagesEncouraging N �� O�� $0 messagescomplete messagesComplete O ������ &0 messagescancelled messagesCancelled��   J  P Q P l     ��������  ��  ��   Q  R S R l     ��������  ��  ��   S  T U T l     �� V W��   V !  Global variable assignment    W � X X 6   G l o b a l   v a r i a b l e   a s s i g n m e n t U  Y Z Y l     [���� [ r      \ ] \ b      ^ _ ^ n    	 ` a ` 1    	��
�� 
psxp a l     b���� b I    �� c d
�� .earsffdralis        afdr c m     ��
�� afdrdlib d �� e��
�� 
from e m    ��
�� fldmfldu��  ��  ��   _ m   	 
 f f � g g f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t ] o      ���� 	0 plist  ��  ��   Z  h i h l     ��������  ��  ��   i  j k j l    l���� l r     m n m m    ��
�� boovtrue n o      ���� 0 initialbackup initialBackup��  ��   k  o p o l    q���� q r     r s r m    ��
�� boovfals s o      ���� 0 isbackingup isBackingUp��  ��   p  t u t l     ��������  ��  ��   u  v w v l    x���� x r     y z y J     { {  | } | m     ~ ~ �   t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . }  � � � m     � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �  � � � m     � � � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d ! �  � � � m     � � � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �  ��� � m     � � � � � � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .��   z o      ���� "0 messagesmissing messagesMissing��  ��   w  � � � l     ��������  ��  ��   �  � � � l    C ����� � r     C � � � J     ? � �  � � � m     # � � � � �  C o m e   o n ! �  � � � m   # & � � � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r ! �  � � � m   & ) � � � � � " N o   p a i n !   N o   p a i n ! �  � � � m   ) , � � � � � 0 L e t ' s   d o   t h i s   a s   a   t e a m ! �  � � � m   , / � � � � �  W e   c a n   d o   i t ! �  � � � m   / 2 � � � � �  F r e e e e e e e e e d o m ! �  � � � m   2 5 � � � � � 2 A l t o g e t h e r   o r   n o t   a t   a l l ! �  � � � m   5 8 � � � � � H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! ! �  ��� � m   8 ; � � � � � H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !��   � o      ���� *0 messagesencouraging messagesEncouraging��  ��   �  � � � l     ��������  ��  ��   �  � � � l  D g ����� � r   D g � � � J   D c � �  � � � m   D G � � � � �  N i c e   o n e ! �  � � � m   G J � � � � � * Y o u   a b s o l u t e   l e g   e n d ! �  � � � m   J M � � � � �  G o o d   l a d ! �  � � � m   M P � � � � �  P e r f i c k ! �  � � � m   P S � � � � �  H a p p y   d a y s ! �  � � � m   S V � � � � �  F r e e e e e e e e e d o m ! �  � � � m   V Y � � � � � H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! ! �  � � � m   Y \ � � � � � B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s ! �  ��� � m   \ _ � � � � � d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !��   � o      ���� $0 messagescomplete messagesComplete��  ��   �  � � � l     ��������  ��  ��   �  � � � l  h � ����� � r   h � � � � J   h � � �  � � � m   h k � � � � �  T h a t ' s   a   s h a m e �  � � � m   k n � � � � �  A h   m a n ,   u n l u c k y �  � � � m   n q � � � � �  O h   w e l l �  � � � m   q t � � � � � @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e n �  � � � m   t w � � �   , W e l l   t h a t ' s   a   l e t   d o w n �  m   w z � P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?  m   z } �  A r r o g a n t 	��	 m   } �

 � > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0��   � o      ���� &0 messagescancelled messagesCancelled��  ��   �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  !  l     ��"#��  " � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   # �$$ - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -! %&% l     ��~�}�  �~  �}  & '(' l     �|)*�|  ) 6 0 Function to check for a .plist preferences file   * �++ `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e( ,-, l     �{./�{  . m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   / �00 �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e- 121 l     �z34�z  3 b \ If a .plist file is present, it assigns preferences to their corresponding global variables   4 �55 �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s2 676 i     898 I      �y�x�w�y 0 	plistinit 	plistInit�x  �w  9 k    �:: ;<; q      == �v>�v 0 
foldername 
folderName> �u?�u 0 
backupdisk 
backupDisk? �t@�t  0 computerfolder computerFolder@ �s�r�s 0 
backuptime 
backupTime�r  < ABA r     CDC m     �q�q  D o      �p�p 0 
backuptime 
backupTimeB EFE l   �o�n�m�o  �n  �m  F GHG Z   �IJ�lKI l   L�k�jL =    MNM I    �iO�h�i 0 itexists itExistsO PQP m    RR �SS  f i l eQ T�gT o    �f�f 	0 plist  �g  �h  N m    �e
�e boovtrue�k  �j  J O    =UVU k    <WW XYX r    Z[Z n    \]\ 1    �d
�d 
pcnt] 4    �c^
�c 
plif^ o    �b�b 	0 plist  [ o      �a�a 0 thecontents theContentsY _`_ r    "aba n     cdc 1     �`
�` 
valLd o    �_�_ 0 thecontents theContentsb o      �^�^ 0 thevalue theValue` efe l  # #�]�\�[�]  �\  �[  f ghg r   # (iji n   # &klk o   $ &�Z�Z  0 foldertobackup FolderToBackupl o   # $�Y�Y 0 thevalue theValuej o      �X�X 0 
foldername 
folderNameh mnm r   ) .opo n   ) ,qrq o   * ,�W�W 0 backupdrive BackupDriver o   ) *�V�V 0 thevalue theValuep o      �U�U 0 
backupdisk 
backupDiskn sts r   / 4uvu n   / 2wxw o   0 2�T�T  0 computerfolder computerFolderx o   / 0�S�S 0 thevalue theValuev o      �R�R  0 computerfolder computerFoldert yzy r   5 :{|{ n   5 8}~} o   6 8�Q�Q 0 scheduledtime scheduledTime~ o   5 6�P�P 0 thevalue theValue| o      �O�O 0 
backuptime 
backupTimez � l  ; ;�N�M�L�N  �M  �L  � ��K� l  ; ;�J���J  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�K  V m    ���                                                                                  sevs  alis    �  Macintosh HD               �e�H+  ��1System Events.app                                              ��e����        ����  	                CoreServices    �e��      ����    ��1��0��/  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  �l  K k   @��� ��� r   @ G��� I   @ E�I�H�G�I .0 getcomputeridentifier getComputerIdentifier�H  �G  � o      �F�F  0 computerfolder computerFolder� ��� l  H H�E�D�C�E  �D  �C  � ��� O   H ���� t   L ���� k   N ��� ��� r   N W��� l  N U��B�A� I  N U�@�?�
�@ .sysostflalis    ��� null�?  � �>��=
�> 
prmp� m   P Q�� ��� @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p�=  �B  �A  � o      �<�< 0 
foldername 
folderName� ��� r   X g��� c   X e��� l  X a��;�:� I  X a�9�8�
�9 .sysostflalis    ��� null�8  � �7��6
�7 
prmp� m   Z ]�� ��� R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o�6  �;  �:  � m   a d�5
�5 
TEXT� o      �4�4 0 
backupdisk 
backupDisk� ��� l  h h�3�2�1�3  �2  �1  � ��� r   h o��� n   h m��� 1   i m�0
�0 
psxp� o   h i�/�/ 0 
foldername 
folderName� o      �.�. 0 
foldername 
folderName� ��� r   p w��� n   p u��� 1   q u�-
�- 
psxp� o   p q�,�, 0 
backupdisk 
backupDisk� o      �+�+ 0 
backupdisk 
backupDisk� ��� l  x x�*�)�(�*  �)  �(  � ��� r   x ���� J   x ��� ��� m   x {�� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r� ��� m   { ~�� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r� ��'� m   ~ ��� ��� , E v e r y   h o u r   o n   t h e   h o u r�'  � o      �&�& 0 backuptimes backupTimes� ��� r   � ���� c   � ���� J   � ��� ��%� I  � ��$��
�$ .gtqpchltns    @   @ ns  � o   � ��#�# 0 backuptimes backupTimes� �"��!
�" 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?�!  �%  � m   � �� 
�  
TEXT� o      �� 0 selectedtime selectedTime� ��� l  � �����  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � �����  �  �  � ��� Z   � ������ l  � ����� =   � ���� o   � ��� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r�  �  � r   � ���� m   � ��� � o      �� 0 
backuptime 
backupTime� ��� l  � ����� =   � ���� o   � ��� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r�  �  � ��� r   � ���� m   � ��� � o      �� 0 
backuptime 
backupTime� ��� l  � ����� =   � ���� o   � ��� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r�  �  � ��� r   � ���� m   � ��� <� o      �
�
 0 
backuptime 
backupTime�  �  � ��� l  � ��	���	  �  �  � ��� l  � �� �    . (log {folderName, backupDisk, backupTime}    � P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�  � m   L M��  ��� m   H I�                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  �  l  � �����  �  �   �  O   �� O   ��	
	 k   ��  I  �����
�� .corecrel****      � null��   ��
�� 
kocl m   � ���
�� 
plii ��
�� 
insh  ;   � � ����
�� 
prdt K   �
 ��
�� 
kind m   � ���
�� 
TEXT ��
�� 
pnam m   �  F o l d e r T o B a c k u p ����
�� 
valL o  ���� 0 
foldername 
folderName��  ��    I 8����
�� .corecrel****      � null��   �� 
�� 
kocl m  ��
�� 
plii  ��!"
�� 
insh!  ;  " ��#��
�� 
prdt# K   2$$ ��%&
�� 
kind% m  #&��
�� 
TEXT& ��'(
�� 
pnam' m  ),)) �**  B a c k u p D r i v e( ��+��
�� 
valL+ o  -.���� 0 
backupdisk 
backupDisk��  ��   ,-, I 9`����.
�� .corecrel****      � null��  . ��/0
�� 
kocl/ m  =@��
�� 
plii0 ��12
�� 
insh1  ;  CE2 ��3��
�� 
prdt3 K  HZ44 ��56
�� 
kind5 m  KN��
�� 
TEXT6 ��78
�� 
pnam7 m  QT99 �::  C o m p u t e r F o l d e r8 ��;��
�� 
valL; o  UV����  0 computerfolder computerFolder��  ��  - <��< I a�����=
�� .corecrel****      � null��  = ��>?
�� 
kocl> m  eh��
�� 
plii? ��@A
�� 
insh@  ;  kmA ��B��
�� 
prdtB K  p�CC ��DE
�� 
kindD m  sv��
�� 
TEXTE ��FG
�� 
pnamF m  y|HH �II  S c h e d u l e d T i m eG ��J��
�� 
valLJ o  }~���� 0 
backuptime 
backupTime��  ��  ��  
 l  � �K����K I  � �����L
�� .corecrel****      � null��  L ��MN
�� 
koclM m   � ���
�� 
plifN ��O��
�� 
prdtO K   � �PP ��Q��
�� 
pnamQ o   � ����� 	0 plist  ��  ��  ��  ��   m   � �RR�                                                                                  sevs  alis    �  Macintosh HD               �e�H+  ��1System Events.app                                              ��e����        ����  	                CoreServices    �e��      ����    ��1��0��/  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  �   H STS l ����������  ��  ��  T UVU r  ��WXW o  ������ 0 
foldername 
folderNameX o      ���� 0 sourcefolder sourceFolderV YZY r  ��[\[ o  ������ 0 
backupdisk 
backupDisk\ o      ���� "0 destinationdisk destinationDiskZ ]^] r  ��_`_ o  ������  0 computerfolder computerFolder` o      ���� 0 machinefolder machineFolder^ aba r  ��cdc o  ������ 0 
backuptime 
backupTimed o      ���� 0 scheduledtime scheduledTimeb efe I ����g��
�� .ascrcmnt****      � ****g J  ��hh iji o  ������ 0 sourcefolder sourceFolderj klk o  ������ "0 destinationdisk destinationDiskl mnm o  ������ 0 machinefolder machineFoldern o��o o  ������ 0 scheduledtime scheduledTime��  ��  f pqp l ����������  ��  ��  q r��r I  ���������� 0 diskinit diskInit��  ��  ��  7 sts l     ��������  ��  ��  t uvu l     ��������  ��  ��  v wxw l     ��������  ��  ��  x yzy l     ��������  ��  ��  z {|{ l     ��������  ��  ��  | }~} l     �����   H B Function to detect if the selected hard drive is connected or not   � ��� �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t~ ��� l     ������  � T N This only happens once a hard drive has been selected and provides a reminder   � ��� �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r� ��� i    ��� I      �������� 0 diskinit diskInit��  ��  � Z     K������ l    	������ =     	��� I     ������� 0 itexists itExists� ��� m    �� ���  d i s k� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    ������� 0 freespaceinit freeSpaceInit� ���� m    ��
�� boovfals��  ��  ��  � k    K�� ��� r    ��� n    ��� 3    ��
�� 
cobj� o    ���� "0 messagesmissing messagesMissing� o      ���� 0 msg  � ��� r    +��� l   )������ n    )��� 1   ' )��
�� 
bhit� l   '����� I   '�~��
�~ .sysodlogaskr        TEXT� o    �}�} 0 msg  � �|��
�| 
btns� J    !�� ��� m    �� ���  C a n c e l   B a c k u p� ��{� m    �� ���  O K�{  � �z��y
�z 
dflt� m   " #�x�x �y  ��  �  ��  ��  � o      �w�w 	0 reply  � ��v� Z   , K���u�� l  , /��t�s� =   , /��� o   , -�r�r 	0 reply  � m   - .�� ���  O K�t  �s  � I   2 7�q�p�o�q 0 diskinit diskInit�p  �o  �u  � k   : K�� ��� r   : ?��� n   : =��� 3   ; =�n
�n 
cobj� o   : ;�m�m &0 messagescancelled messagesCancelled� o      �l�l 0 msg  � ��k� I  @ K�j��
�j .sysonotfnull��� ��� TEXT� o   @ A�i�i 0 msg  � �h��g
�h 
appr� m   D G�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�g  �k  �v  � ��� l     �f�e�d�f  �e  �d  � ��� l     �c�b�a�c  �b  �a  � ��� l     �`�_�^�`  �_  �^  � ��� l     �]�\�[�]  �\  �[  � ��� l     �Z�Y�X�Z  �Y  �X  � ��� l     �W���W  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     �V���V  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     �U���U  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i    ��� I      �T��S�T 0 freespaceinit freeSpaceInit� ��R� o      �Q�Q 	0 again  �R  �S  � Z     k���P�� l    	��O�N� =     	��� I     �M��L�M 0 comparesizes compareSizes� ��� o    �K�K 0 sourcefolder sourceFolder� ��J� o    �I�I "0 destinationdisk destinationDisk�J  �L  � m    �H
�H boovtrue�O  �N  � I    �G�F�E�G 0 
backupinit 
backupInit�F  �E  �P  � k    k�� ��� Z    G����D� l   ��C�B� =    ��� o    �A�A 	0 again  � m    �@
�@ boovfals�C  �B  � r    *��� l   (��?�>� n    (   1   & (�=
�= 
bhit l   &�<�; I   &�:
�: .sysodlogaskr        TEXT m     � � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ? �9
�9 
btns J     		 

 m     �  Y e s �8 m     �  C a n c e l   B a c k u p�8   �7�6
�7 
dflt m   ! "�5�5 �6  �<  �;  �?  �>  � o      �4�4 
0 reply1  �  l  - 0�3�2 =   - 0 o   - .�1�1 	0 again   m   . /�0
�0 boovtrue�3  �2   �/ r   3 C l  3 A�.�- n   3 A 1   ? A�,
�, 
bhit l  3 ?�+�* I  3 ?�)
�) .sysodlogaskr        TEXT m   3 4   �!! � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ? �("#
�( 
btns" J   5 9$$ %&% m   5 6'' �((  Y e s& )�') m   6 7** �++  C a n c e l   B a c k u p�'  # �&,�%
�& 
dflt, m   : ;�$�$ �%  �+  �*  �.  �-   o      �#�# 
0 reply1  �/  �D  � -.- l  H H�"�!� �"  �!  �   . /�/ Z   H k01�20 l  H K3��3 =   H K454 o   H I�� 
0 reply1  5 m   I J66 �77  Y e s�  �  1 I   N S���� 0 tidyup tidyUp�  �  �  2 k   V k88 9:9 r   V _;<; n   V ]=>= 3   Y ]�
� 
cobj> o   V Y�� &0 messagescancelled messagesCancelled< o      �� 0 msg  : ?�? I  ` k�@A
� .sysonotfnull��� ��� TEXT@ o   ` a�� 0 msg  A �B�
� 
apprB m   d gCC �DD > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  �  �  � EFE l     ����  �  �  F GHG l     ���
�  �  �
  H IJI l     �	���	  �  �  J KLK l     ����  �  �  L MNM l     ����  �  �  N OPO l     � QR�   Q,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   R �SSL   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .P TUT l     ��VW��  V g a These folders, if required, are then set to their corresponding global variables declared above.   W �XX �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .U YZY i    [\[ I      �������� 0 
backupinit 
backupInit��  ��  \ k     �]] ^_^ r     `a` 4     ��b
�� 
psxfb o    ���� "0 destinationdisk destinationDiska o      ���� 	0 drive  _ cdc l   ��ef��  e ' !log (destinationDisk & "Backups")   f �gg B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )d hih l   ��������  ��  ��  i jkj Z    ,lm����l l   n����n =    opo I    ��q���� 0 itexists itExistsq rsr m    	tt �uu  f o l d e rs v��v b   	 wxw o   	 
���� "0 destinationdisk destinationDiskx m   
 yy �zz  B a c k u p s��  ��  p m    ��
�� boovfals��  ��  m O    ({|{ I   '����}
�� .corecrel****      � null��  } ��~
�� 
kocl~ m    ��
�� 
cfol ����
�� 
insh� o    ���� 	0 drive  � �����
�� 
prdt� K    #�� �����
�� 
pnam� m     !�� ���  B a c k u p s��  ��  | m    ���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  ��  ��  k ��� l  - -��������  ��  ��  � ��� Z   - d������� l  - >������ =   - >��� I   - <������� 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ���� b   / 8��� b   / 4��� o   / 0���� "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7���� 0 machinefolder machineFolder��  ��  � m   < =��
�� boovfals��  ��  � O   A `��� I  E _�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   G H��
�� 
cfol� ����
�� 
insh� n   I T��� 4   O T���
�� 
cfol� m   P S�� ���  B a c k u p s� 4   I O���
�� 
cdis� o   M N���� 	0 drive  � �����
�� 
prdt� K   U [�� �����
�� 
pnam� o   V Y���� 0 machinefolder machineFolder��  ��  � m   A B���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  ��  ��  � ��� l  e e��������  ��  ��  � ��� O   e ���� k   i �� ��� l  i i������  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y���
�� 
cfol� o   u x���� 0 machinefolder machineFolder� n   i t��� 4   o t���
�� 
cfol� m   p s�� ���  B a c k u p s� 4   i o���
�� 
cdis� o   m n���� 	0 drive  � o      ���� 0 backupfolder backupFolder� ���� l  ~ ~������  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )��  � m   e f���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� I   � �������� 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ���� b   � ���� b   � ���� b   � ���� o   � ����� "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ����� 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t��  ��  � m   � ���
�� boovfals��  ��  � O   � ���� I  � ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
cfol� ����
�� 
insh� o   � ����� 0 backupfolder backupFolder� �����
�� 
prdt� K   � ��� �����
�� 
pnam� m   � ��� ���  L a t e s t��  ��  � m   � ����                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  ��  � r   � ���� m   � ���
�� boovfals� o      ���� 0 initialbackup initialBackup� ��� l  � ���������  ��  ��  � ��� O   � ���� k   � ��� ��� r   � ���� n   � �   4   � ���
�� 
cfol m   � � �  L a t e s t n   � � 4   � ���
�� 
cfol m   � � �		  B a c k u p s 4   � ���

�� 
cdis
 o   � ����� 	0 drive  � o      ���� 0 latestfolder latestFolder� �� l  � �����    log (backupFolder)    � $ l o g   ( b a c k u p F o l d e r )��  � m   � ��                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  �  l  � ���������  ��  ��   �� I   � ��������� 
0 backup  ��  ��  ��  Z  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     �� ��   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.     �!! �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h . "#" l     ��$%��  $ � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   % �&&�   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .# '(' l     ��)*��  ) � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   * �++P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .( ,-, l     ��./��  . � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   / �00>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .- 121 i    343 I      ��~�}� 0 tidyup tidyUp�~  �}  4 O     �565 k    �77 898 r    :;: n    	<=< 1    	�|
�| 
ascd= n    >?> 2   �{
�{ 
cobj? o    �z�z 0 backupfolder backupFolder; o      �y�y 0 creationdates creationDates9 @A@ r    BCB n    DED 4    �xF
�x 
cobjF m    �w�w E o    �v�v 0 creationdates creationDatesC o      �u�u 0 theoldestdate theOldestDateA GHG r    IJI m    �t�t J o      �s�s 0 j  H KLK l   �r�q�p�r  �q  �p  L MNM Y    KO�oPQ�nO k   % FRR STS r   % +UVU n   % )WXW 4   & )�mY
�m 
cobjY o   ' (�l�l 0 i  X o   % &�k�k 0 creationdates creationDatesV o      �j�j 0 thisdate thisDateT Z�iZ Z   , F[\�h�g[ l  , /]�f�e] A   , /^_^ o   , -�d�d 0 thisdate thisDate_ o   - .�c�c 0 theoldestdate theOldestDate�f  �e  \ k   2 B`` aba r   2 5cdc o   2 3�b�b 0 thisdate thisDated o      �a�a 0 theoldestdate theOldestDateb efe r   6 9ghg o   6 7�`�` 0 i  h o      �_�_ 0 j  f i�^i I  : B�]j�\
�] .ascrcmnt****      � ****j l  : >k�[�Zk n   : >lml 4   ; >�Yn
�Y 
cobjn o   < =�X�X 0 j  m o   : ;�W�W 0 backupfolder backupFolder�[  �Z  �\  �^  �h  �g  �i  �o 0 i  P m    �V�V Q I    �Uo�T
�U .corecnte****       ****o o    �S�S 0 creationdates creationDates�T  �n  N pqp l  L L�R�Q�P�R  �Q  �P  q rsr l  L L�Otu�O  t ! -- Delete the oldest folder   u �vv 6 - -   D e l e t e   t h e   o l d e s t   f o l d e rs wxw I  L T�Ny�M
�N .coredeloobj        obj y n   L Pz{z 4   M P�L|
�L 
cobj| o   N O�K�K 0 j  { o   L M�J�J 0 backupfolder backupFolder�M  x }~} l  U U�I�H�G�I  �H  �G  ~ � l  U U�F�E�D�F  �E  �D  � ��� l   U U�C���C  � � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  U U�B�A�@�B  �A  �@  � ��� l  U U�?�>�=�?  �>  �=  � ��� r   U e��� l  U c��<�;� n   U c��� 1   a c�:
�: 
bhit� l  U a��9�8� I  U a�7��
�7 .sysodlogaskr        TEXT� m   U V�� ���D Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .  � �6��
�6 
btns� J   W [�� ��� m   W X�� ���  E m p t y   T r a s h� ��5� m   X Y�� ���  C a n c e l   B a c k u p�5  � �4��3
�4 
dflt� m   \ ]�2�2 �3  �9  �8  �<  �;  � o      �1�1 
0 reply2  � ��� Z   f ����0�� l  f i��/�.� =   f i��� o   f g�-�- 
0 reply2  � m   g h�� ���  E m p t y   T r a s h�/  �.  � I  l u�,��+
�, .fndremptnull��� ��� obj � l  l q��*�)� 1   l q�(
�( 
trsh�*  �)  �+  �0  � k   x ��� ��� r   x ��� n   x }��� 3   { }�'
�' 
cobj� o   x {�&�& &0 messagescancelled messagesCancelled� o      �%�% 0 msg  � ��$� I  � ��#��
�# .sysonotfnull��� ��� TEXT� o   � ��"�" 0 msg  � �!�� 
�! 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�   �$  � ��� l  � �����  �  �  � ��� I   � ����� 0 freespaceinit freeSpaceInit� ��� m   � ��
� boovtrue�  �  � ��� I  � ����
� .sysonotfnull��� ��� TEXT� m   � ��� ��� � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .� ���
� 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�  � ��� I  � ����
� .sysodelanull��� ��� nmbr� m   � ��� �  �  6 m     ���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  2 ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ��
�	�  �
  �	  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  � M G Function that carries out the backup process and is split in 2 parts.    � ��� �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  � ��� l     ����  �*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   � ���H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .� ��� l     � ���   � � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   � ���   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .� ��� i    ��� I      �������� 
0 backup  ��  ��  � k    f�� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovtrue��  ��  � o      ���� 0 t  � ��� l  	 	��������  ��  ��  � ��� r   	 ��� m   	 
��
�� boovtrue� o      ���� 0 isbackingup isBackingUp� ��� l   ��������  ��  ��  � ��� I    ������� 0 	stopwatch  �  ��  m     � 
 s t a r t��  ��  �  l   ��������  ��  ��    O   ] k   \		 

 l   ����   f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created    � �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d  l   ����   x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up    � �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p  r      c     4    ��
�� 
psxf o    ���� 0 sourcefolder sourceFolder m    ��
�� 
TEXT o      ���� 0 
foldername 
folderName  r   ! ) n   ! '  1   % '��
�� 
pnam  4   ! %��!
�� 
cfol! o   # $���� 0 
foldername 
folderName o      ���� 0 
foldername 
folderName "#" l  * *��$%��  $  log (folderName)		   % �&& $ l o g   ( f o l d e r N a m e ) 	 	# '(' l  * *��������  ��  ��  ( )*) Z   * �+,����+ l  * --����- =   * -./. o   * +���� 0 initialbackup initialBackup/ m   + ,��
�� boovfals��  ��  , k   0 00 121 I  0 >����3
�� .corecrel****      � null��  3 ��45
�� 
kocl4 m   2 3��
�� 
cfol5 ��67
�� 
insh6 o   4 5���� 0 backupfolder backupFolder7 ��8��
�� 
prdt8 K   6 :99 ��:��
�� 
pnam: o   7 8���� 0 t  ��  ��  2 ;<; l  ? ?��������  ��  ��  < =>= r   ? I?@? c   ? EABA 4   ? C��C
�� 
psxfC o   A B���� 0 sourcefolder sourceFolderB m   C D��
�� 
TEXT@ o      ���� (0 activesourcefolder activeSourceFolder> DED r   J TFGF 4   J P��H
�� 
cfolH o   L O���� (0 activesourcefolder activeSourceFolderG o      ���� (0 activesourcefolder activeSourceFolderE IJI l  U U��KL��  K  log (activeSourceFolder)   L �MM 0 l o g   ( a c t i v e S o u r c e F o l d e r )J NON r   U nPQP n   U jRSR 4   g j��T
�� 
cfolT o   h i���� 0 t  S n   U gUVU 4   b g��W
�� 
cfolW o   c f���� 0 machinefolder machineFolderV n   U bXYX 4   ] b��Z
�� 
cfolZ m   ^ a[[ �\\  B a c k u p sY 4   U ]��]
�� 
cdis] o   Y \���� 	0 drive  Q o      ���� (0 activebackupfolder activeBackupFolderO ^_^ l  o o��`a��  `  log (activeBackupFolder)   a �bb 0 l o g   ( a c t i v e B a c k u p F o l d e r )_ c��c I  o ����d
�� .corecrel****      � null��  d ��ef
�� 
kocle m   q r��
�� 
cfolf ��gh
�� 
inshg o   s v���� (0 activebackupfolder activeBackupFolderh ��i��
�� 
prdti K   w {jj ��k��
�� 
pnamk o   x y���� 0 
foldername 
folderName��  ��  ��  ��  ��  * lml l  � ���������  ��  ��  m non l  � ���������  ��  ��  o pqp l  � ���rs��  r o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   s �tt �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m eq uvu l  � ���wx��  w q k An atomic version would just to do a straight backup without versioning to see the function at full effect   x �yy �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c tv z{z l  � ���|}��  | D > This means that only new or updated files will be copied over   } �~~ |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r{ � l  � �������  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	� ��� l  � �������  �   Original sync code    � ��� (   O r i g i n a l   s y n c   c o d e  � ��� l  � �������  � z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   � ��� �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Original code   � ���    O r i g i n a l   c o d e� ��� l  � �������  � i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   � ��� �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h� ��� l  � �������  � x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   � ��� �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .� ��� l  � �������  � p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   � ��� �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .� ��� l  � �������  � � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   � ����   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .� ��� l  � �������  � � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   � ���v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .� ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ���� Z   �\������ l  � ������� =   � ���� o   � ����� 0 initialbackup initialBackup� m   � ���
�� boovtrue��  ��  � k   ��� ��� r   � ���� n   � ���� 1   � ���
�� 
time� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      �� 0 	starttime 	startTime� ��� I  � ��~��
�~ .sysonotfnull��� ��� TEXT� m   � ��� ��� � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .� �}��|
�} 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�|  � ��� l  � ��{�z�y�{  �z  �y  � ��� r   � ���� c   � ���� l  � ���x�w� b   � ���� b   � ���� b   � ���� b   � ���� b   � ���� o   � ��v�v "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��u�u 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t /� o   � ��t�t 0 
foldername 
folderName� m   � ��� ���  /�x  �w  � m   � ��s
�s 
TEXT� o      �r�r 0 d  � ��� I  � ��q��p
�q .sysoexecTEXT���     TEXT� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  d i t t o   '� o   � ��o�o 0 sourcefolder sourceFolder� m   � ��� ���  '   '� o   � ��n�n 0 d  � m   � ��� ���  / '�p  � ��� l  � ��m�l�k�m  �l  �k  � ��� r   � ���� m   � ��j
�j boovfals� o      �i�i 0 isbackingup isBackingUp� ��� l  � ��h�g�f�h  �g  �f  � ��� r   � ���� n   � �   1   � ��e
�e 
time l  � ��d�c I  � ��b�a�`
�b .misccurdldt    ��� null�a  �`  �d  �c  � o      �_�_ 0 endtime endTime�  r   � � n  � � I   � ��^�]�\�^ 0 getduration getDuration�]  �\    f   � � o      �[�[ 0 duration   	
	 r   � � n   � � 3   � ��Z
�Z 
cobj o   � ��Y�Y $0 messagescomplete messagesComplete o      �X�X 0 msg  
  I  ��W
�W .sysonotfnull��� ��� TEXT b   � b   � b   � � m   � � � 6 F o r   t h e   f i r s t   t i m e   e v e r   i n   o   � ��V�V 0 duration   m   �  �    m i n u t e s ! 
 o  �U�U 0 msg   �T�S
�T 
appr m  	 � : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�S    �R  I �Q!�P
�Q .sysodelanull��� ��� nmbr! m  �O�O �P  �R  � "#" l $�N�M$ =  %&% o  �L�L 0 initialbackup initialBackup& m  �K
�K boovfals�N  �M  # '�J' k  X(( )*) r  ;+,+ c  9-.- l 7/�I�H/ b  7010 b  3232 b  1454 b  -676 b  +898 b  ':;: b  #<=< o  �G�G "0 destinationdisk destinationDisk= m  ">> �??  B a c k u p s /; o  #&�F�F 0 machinefolder machineFolder9 m  '*@@ �AA  /7 o  +,�E�E 0 t  5 m  -0BB �CC  /3 o  12�D�D 0 
foldername 
folderName1 m  36DD �EE  /�I  �H  . m  78�C
�C 
TEXT, o      �B�B 0 c  * FGF r  <UHIH c  <SJKJ l <QL�A�@L b  <QMNM b  <MOPO b  <KQRQ b  <GSTS b  <CUVU o  <?�?�? "0 destinationdisk destinationDiskV m  ?BWW �XX  B a c k u p s /T o  CF�>�> 0 machinefolder machineFolderR m  GJYY �ZZ  / L a t e s t /P o  KL�=�= 0 
foldername 
folderNameN m  MP[[ �\\  /�A  �@  K m  QR�<
�< 
TEXTI o      �;�; 0 d  G ]^] I V_�:_�9
�: .ascrcmnt****      � ****_ l V[`�8�7` b  V[aba m  VYcc �dd  b a c k i n g   u p   t o :  b o  YZ�6�6 0 d  �8  �7  �9  ^ efe l ``�5�4�3�5  �4  �3  f ghg r  `miji n  `iklk 1  ei�2
�2 
timel l `em�1�0m I `e�/�.�-
�/ .misccurdldt    ��� null�.  �-  �1  �0  j o      �,�, 0 	starttime 	startTimeh non r  nwpqp n  nursr 3  qu�+
�+ 
cobjs o  nq�*�* *0 messagesencouraging messagesEncouragingq o      �)�) 0 msg  o tut I x��(vw
�( .sysonotfnull��� ��� TEXTv o  xy�'�' 0 msg  w �&x�%
�& 
apprx m  |yy �zz 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�%  u {|{ l ���$�#�"�$  �#  �"  | }~} I ���!� 
�! .sysoexecTEXT���     TEXT b  ����� b  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ��� V r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ���� 0 c  � m  ���� ���  '   '� o  ���� 0 sourcefolder sourceFolder� m  ���� ���  '   '� o  ���� 0 d  � m  ���� ���  / '�   ~ ��� l ������  �  �  � ��� r  ����� m  ���
� boovfals� o      �� 0 isbackingup isBackingUp� ��� l ������  �  �  � ��� Z  �X����� = ����� n  ����� 2 ���
� 
cobj� l ������ c  ����� 4  ����
� 
psxf� o  ���� 0 c  � m  ���
� 
alis�  �  � J  ����  � k  ��� ��� l ������  � % delete folder t of backupFolder   � ��� > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r� ��� l ���
�	��
  �	  �  � ��� r  ����� c  ����� n  ����� 4  ����
� 
cfol� o  ���� 0 t  � o  ���� 0 backupfolder backupFolder� m  ���
� 
TEXT� o      �� 0 oldestfolder oldestFolder� ��� r  ����� c  ����� n  ����� 1  ���
� 
psxp� o  ���� 0 oldestfolder oldestFolder� m  ��� 
�  
TEXT� o      ���� 0 oldestfolder oldestFolder� ��� I �������
�� .ascrcmnt****      � ****� l �������� b  ����� m  ���� ���  t o   d e l e t e :  � o  ������ 0 oldestfolder oldestFolder��  ��  ��  � ��� l ����������  ��  ��  � ��� r  ����� b  ����� m  ���� ���  r m   - r f  � o  ������ 0 oldestfolder oldestFolder� o      ���� 0 todelete toDelete� ��� I �������
�� .sysoexecTEXT���     TEXT� o  ������ 0 todelete toDelete��  � ��� l ����������  ��  ��  � ��� r  ����� n  ����� 1  ����
�� 
time� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 endtime endTime� ��� r  ����� n ����� I  ���������� 0 getduration getDuration��  ��  �  f  ��� o      ���� 0 duration  � ��� l ��������  � � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   � ��� d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "� ��� l ��������  �  delay 2   � ���  d e l a y   2� ��� l ����������  ��  ��  � ��� r  ����� n  ����� 3  ����
�� 
cobj� o  ������ $0 messagescomplete messagesComplete� o      ���� 0 msg  � ��� I �����
�� .sysonotfnull��� ��� TEXT� b  �
��� b  ���� b  �� � m  � �  A n d   i n    o  ���� 0 duration  � m   �    m i n u t e s ! 
� o  	���� 0 msg  � ����
�� 
appr m   � : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � �� I ��	��
�� .sysodelanull��� ��� nmbr	 m  ���� ��  ��  �  � k  X

  r  * n  & 1  "&��
�� 
time l "���� I "������
�� .misccurdldt    ��� null��  ��  ��  ��   o      ���� 0 endtime endTime  r  +2 n +0 I  ,0�������� 0 getduration getDuration��  ��    f  +, o      ���� 0 duration    r  3< n  3: 3  6:��
�� 
cobj o  36���� $0 messagescomplete messagesComplete o      ���� 0 msg    I =R�� !
�� .sysonotfnull��� ��� TEXT  b  =H"#" b  =F$%$ b  =B&'& m  =@(( �))  A n d   i n  ' o  @A���� 0 duration  % m  BE** �++    m i n u t e s ! 
# o  FG���� 0 msg  ! ��,��
�� 
appr, m  KN-- �.. : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��   /��/ I SX��0��
�� .sysodelanull��� ��� nmbr0 m  ST���� ��  ��  �  �J  ��  ��   m    11�                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��   232 l ^^��������  ��  ��  3 4��4 I  ^f��5���� 0 	stopwatch  5 6��6 m  _b77 �88  f i n i s h��  ��  ��  � 9:9 l     ��������  ��  ��  : ;<; l     ��������  ��  ��  < =>= l     ��������  ��  ��  > ?@? l     ��������  ��  ��  @ ABA l     ��������  ��  ��  B CDC l     ��EF��  E � �-----------------------------------------------------------------------------------------------------------------------------------------------   F �GG - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -D HIH l     ��JK��  J � �-----------------------------------------------------------------------------------------------------------------------------------------------   K �LL - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -I MNM l     ��OP��  O � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   P �QQ - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -N RSR l     ��������  ��  ��  S TUT l     ��VW��  V � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   W �XX   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .U YZY l     ��[\��  [ � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   \ �]]�   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .Z ^_^ i    `a` I      ��b���� 0 itexists itExistsb cdc o      ���� 0 
objecttype 
objectTyped e��e o      ���� 
0 object  ��  ��  a l    Wfghf O     Wiji Z    Vklm��k l   n����n =    opo o    ���� 0 
objecttype 
objectTypep m    qq �rr  d i s k��  ��  l Z   
 st��us I  
 ��v��
�� .coredoexnull���     ****v 4   
 ��w
�� 
cdisw o    ���� 
0 object  ��  t L    xx m    ��
�� boovtrue��  u L    yy m    ��
�� boovfalsm z{z l   "|����| =    "}~} o     ���� 0 
objecttype 
objectType~ m     ! ���  f i l e��  ��  { ��� Z   % 7������ I  % -�����
�� .coredoexnull���     ****� 4   % )���
�� 
file� o   ' (���� 
0 object  ��  � L   0 2�� m   0 1��
�� boovtrue��  � L   5 7�� m   5 6��
�� boovfals� ��� l  : =������ =   : =��� o   : ;���� 0 
objecttype 
objectType� m   ; <�� ���  f o l d e r��  ��  � ���� Z   @ R����� I  @ H�~��}
�~ .coredoexnull���     ****� 4   @ D�|�
�| 
cfol� o   B C�{�{ 
0 object  �}  � L   K M�� m   K L�z
�z boovtrue�  � L   P R�� m   P Q�y
�y boovfals��  ��  j m     ���                                                                                  sevs  alis    �  Macintosh HD               �e�H+  ��1System Events.app                                              ��e����        ����  	                CoreServices    �e��      ����    ��1��0��/  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  g "  (string, string) as Boolean   h ��� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n_ ��� l     �x�w�v�x  �w  �v  � ��� l     �u�t�s�u  �t  �s  � ��� l     �r�q�p�r  �q  �p  � ��� l     �o�n�m�o  �n  �m  � ��� l     �l�k�j�l  �k  �j  � ��� l     �i���i  � � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   � ���Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .� ��� l     �h���h  � P J This means that backups can be identified from what machine they're from.   � ��� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .� ��� l     �g���g  � F @ Copy to bookmarks http://macscripter.net/viewtopic.php?id=33547   � ��� �   C o p y   t o   b o o k m a r k s   h t t p : / / m a c s c r i p t e r . n e t / v i e w t o p i c . p h p ? i d = 3 3 5 4 7� ��� i    ��� I      �f�e�d�f .0 getcomputeridentifier getComputerIdentifier�e  �d  � k     �� ��� r     	��� n     ��� 1    �c
�c 
sicn� l    ��b�a� I    �`�_�^
�` .sysosigtsirr   ��� null�_  �^  �b  �a  � o      �]�] 0 computername computerName� ��� r   
 ��� I  
 �\��[
�\ .sysoexecTEXT���     TEXT� m   
 �� ��� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �[  � o      �Z�Z "0 strserialnumber strSerialNumber� ��� r    ��� l   ��Y�X� b    ��� b    ��� o    �W�W 0 computername computerName� m    �� ���    -  � o    �V�V "0 strserialnumber strSerialNumber�Y  �X  � o      �U�U  0 identifiername identifierName� ��T� L    �� o    �S�S  0 identifiername identifierName�T  � ��� l     �R�Q�P�R  �Q  �P  � ��� l     �O�N�M�O  �N  �M  � ��� l     �L�K�J�L  �K  �J  � ��� l     �I�H�G�I  �H  �G  � ��� l     �F�E�D�F  �E  �D  � ��� l     �C���C  � � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   � ���   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .� ��� l     �B���B  � � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   � ���   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .� ��� i     #��� I      �A��@�A 0 gettimestamp getTimestamp� ��?� o      �>�> 0 isfolder isFolder�?  �@  � l   m���� k    m�� ��� l     �=���=  �   Date variables   � ���    D a t e   v a r i a b l e s� ��� r     )��� l     ��<�;� I     �:�9�8
�: .misccurdldt    ��� null�9  �8  �<  �;  � K    �� �7��
�7 
year� o    �6�6 0 y  � �5� 
�5 
mnth� o    �4�4 0 m    �3
�3 
day  o    �2�2 0 d   �1�0
�1 
time o   	 
�/�/ 0 t  �0  �  r   * 1 c   * /	 l  * -
�.�-
 c   * - o   * +�,�, 0 y   m   + ,�+
�+ 
long�.  �-  	 m   - .�*
�* 
TEXT o      �)�) 0 ty tY  r   2 9 c   2 7 l  2 5�(�' c   2 5 o   2 3�&�& 0 y   m   3 4�%
�% 
long�(  �'   m   5 6�$
�$ 
TEXT o      �#�# 0 ty tY  r   : A c   : ? l  : =�"�! c   : = o   : ;� �  0 m   m   ; <�
� 
long�"  �!   m   = >�
� 
TEXT o      �� 0 tm tM   r   B I!"! c   B G#$# l  B E%��% c   B E&'& o   B C�� 0 d  ' m   C D�
� 
long�  �  $ m   E F�
� 
TEXT" o      �� 0 td tD  ()( r   J Q*+* c   J O,-, l  J M.��. c   J M/0/ o   J K�� 0 t  0 m   K L�
� 
long�  �  - m   M N�
� 
TEXT+ o      �� 0 tt tT) 121 l  R R����  �  �  2 343 l  R R�56�  5 U O Append the month or day with a 0 if the string length is only 1 character long   6 �77 �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g4 898 r   R [:;: c   R Y<=< l  R W>��> I  R W�
?�	
�
 .corecnte****       ****? o   R S�� 0 tm tM�	  �  �  = m   W X�
� 
nmbr; o      �� 
0 tml tML9 @A@ r   \ eBCB c   \ cDED l  \ aF��F I  \ a�G�
� .corecnte****       ****G o   \ ]�� 0 tm tM�  �  �  E m   a b� 
�  
nmbrC o      ���� 
0 tdl tDLA HIH l  f f��������  ��  ��  I JKJ Z   f uLM����L l  f iN����N =   f iOPO o   f g���� 
0 tml tMLP m   g h���� ��  ��  M r   l qQRQ b   l oSTS m   l mUU �VV  0T o   m n���� 0 tm tMR o      ���� 0 tm tM��  ��  K WXW l  v v��������  ��  ��  X YZY Z   v �[\����[ l  v y]����] =   v y^_^ o   v w���� 
0 tdl tDL_ m   w x���� ��  ��  \ r   | �`a` b   | �bcb m   | dd �ee  0c o    ����� 0 td tDa o      ���� 0 td tD��  ��  Z fgf l  � ���������  ��  ��  g hih l  � ���������  ��  ��  i jkj l  � ���lm��  l   Time variables	   m �nn     T i m e   v a r i a b l e s 	k opo l  � ���qr��  q  	 Get hour   r �ss    G e t   h o u rp tut r   � �vwv n   � �xyx 1   � ���
�� 
tstry l  � �z����z I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  w o      ���� 0 timestr timeStru {|{ r   � �}~} I  � �����
�� .sysooffslong    ��� null��   ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  ~ o      ���� 0 pos  | ��� r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 h  � ��� r   � ���� c   � ���� n   � ���� 7 � �����
�� 
cha � l  � ������� [   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  �  ;   � �� o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ������
�� .sysooffslong    ��� null��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 m  � ��� r   �	��� c   ���� n   ���� 7 �����
�� 
cha � l  ������� [   ���� o   � ���� 0 pos  � m   ���� ��  ��  �  ;  � o   � ����� 0 timestr timeStr� m  ��
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l 

��������  ��  ��  � ��� l 

������  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  
��� I 
�����
�� .sysooffslong    ��� null��  � ����
�� 
psof� m  �� ���   � �����
�� 
psin� o  ���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  0��� c  .��� n  ,��� 7,����
�� 
cha � l %)������ [  %)��� o  &'���� 0 pos  � m  '(���� ��  ��  �  ;  *+� o  ���� 0 timestr timeStr� m  ,-��
�� 
TEXT� o      ���� 0 s  � ��� l 11��������  ��  ��  � ��� l 11��������  ��  ��  � ��� Z  1j������ l 14������ =  14��� o  12���� 0 isfolder isFolder� m  23��
�� boovtrue��  ��  � k  7L�� ��� l 77������  �   Create folder timestamp   � ��� 0   C r e a t e   f o l d e r   t i m e s t a m p� ���� r  7L��� b  7J��� b  7H�	 � b  7F			 b  7D			 b  7@			 b  7>			 b  7<			
		 m  7:		 �		  b a c k u p _	
 o  :;���� 0 ty tY	 o  <=���� 0 tm tM	 o  >?���� 0 td tD	 m  @C		 �		  _	 o  DE���� 0 h  	  o  FG���� 0 m  � o  HI���� 0 s  � o      �� 0 	timestamp  ��  � 			 l OR	�~�}	 =  OR			 o  OP�|�| 0 isfolder isFolder	 m  PQ�{
�{ boovfals�~  �}  	 	�z	 k  Uf		 			 l UU�y		�y  	   Create timestamp   	 �		 "   C r e a t e   t i m e s t a m p	 	�x	 r  Uf			 b  Ud			 b  Ub	 	!	  b  U`	"	#	" b  U^	$	%	$ b  UZ	&	'	& b  UX	(	)	( o  UV�w�w 0 ty tY	) o  VW�v�v 0 tm tM	' o  XY�u�u 0 td tD	% m  Z]	*	* �	+	+  _	# o  ^_�t�t 0 h  	! o  `a�s�s 0 m  	 o  bc�r�r 0 s  	 o      �q�q 0 	timestamp  �x  �z  ��  � 	,	-	, l kk�p�o�n�p  �o  �n  	- 	.�m	. L  km	/	/ o  kl�l�l 0 	timestamp  �m  �  	(boolean)   � �	0	0  ( b o o l e a n )� 	1	2	1 l     �k�j�i�k  �j  �i  	2 	3	4	3 l     �h�g�f�h  �g  �f  	4 	5	6	5 l     �e�d�c�e  �d  �c  	6 	7	8	7 l     �b�a�`�b  �a  �`  	8 	9	:	9 l     �_�^�]�_  �^  �]  	: 	;	<	; l     �\	=	>�\  	= q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   	> �	?	? �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .	< 	@	A	@ l     �[	B	C�[  	B w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   	C �	D	D �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .	A 	E	F	E i   $ '	G	H	G I      �Z	I�Y�Z 0 comparesizes compareSizes	I 	J	K	J o      �X�X 
0 source  	K 	L�W	L o      �V�V 0 destination  �W  �Y  	H l    ^	M	N	O	M k     ^	P	P 	Q	R	Q r     	S	T	S m     �U
�U boovtrue	T o      �T�T 0 fit  	R 	U	V	U r    
	W	X	W 4    �S	Y
�S 
psxf	Y o    �R�R 0 destination  	X o      �Q�Q 0 destination  	V 	Z	[	Z l   �P�O�N�P  �O  �N  	[ 	\	]	\ O    L	^	_	^ k    K	`	` 	a	b	a r    	c	d	c I   �M	e�L
�M .sysoexecTEXT���     TEXT	e b    	f	g	f b    	h	i	h m    	j	j �	k	k  d u   - m s  	i o    �K�K 
0 source  	g m    	l	l �	m	m    |   c u t   - f   1�L  	d o      �J�J 0 
foldersize 
folderSize	b 	n	o	n r    (	p	q	p ^    &	r	s	r l   $	t�I�H	t I   $�G	u�F
�G .sysorondlong        doub	u ]     	v	w	v l   	x�E�D	x ^    	y	z	y o    �C�C 0 
foldersize 
folderSize	z m    �B�B �E  �D  	w m    �A�A d�F  �I  �H  	s m   $ %�@�@ d	q o      �?�? 0 
foldersize 
folderSize	o 	{	|	{ l  ) )�>�=�<�>  �=  �<  	| 	}	~	} r   ) 7		�	 ^   ) 5	�	�	� ^   ) 3	�	�	� ^   ) 1	�	�	� l  ) /	��;�:	� l  ) /	��9�8	� n   ) /	�	�	� 1   - /�7
�7 
frsp	� 4   ) -�6	�
�6 
cdis	� o   + ,�5�5 0 destination  �9  �8  �;  �:  	� m   / 0�4�4 	� m   1 2�3�3 	� m   3 4�2�2 	� o      �1�1 0 	freespace 	freeSpace	~ 	�	�	� r   8 C	�	�	� ^   8 A	�	�	� l  8 ?	��0�/	� I  8 ?�.	��-
�. .sysorondlong        doub	� l  8 ;	��,�+	� ]   8 ;	�	�	� o   8 9�*�* 0 	freespace 	freeSpace	� m   9 :�)�) d�,  �+  �-  �0  �/  	� m   ? @�(�( d	� o      �'�' 0 	freespace 	freeSpace	� 	�	�	� l  D D�&�%�$�&  �%  �$  	� 	��#	� I  D K�"	��!
�" .ascrcmnt****      � ****	� l  D G	�� �	� b   D G	�	�	� o   D E�� 0 
foldersize 
folderSize	� o   E F�� 0 	freespace 	freeSpace�   �  �!  �#  	_ m    	�	��                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  	] 	�	�	� l  M M����  �  �  	� 	�	�	� Z   M [	�	���	� H   M Q	�	� l  M P	���	� A   M P	�	�	� o   M N�� 0 
foldersize 
folderSize	� o   N O�� 0 	freespace 	freeSpace�  �  	� r   T W	�	�	� m   T U�
� boovfals	� o      �� 0 fit  �  �  	� 	�	�	� l  \ \����  �  �  	� 	��	� L   \ ^	�	� o   \ ]�� 0 fit  �  	N  (string, string)   	O �	�	�   ( s t r i n g ,   s t r i n g )	F 	�	�	� l     ���
�  �  �
  	� 	�	�	� l     �	���	  �  �  	� 	�	�	� l     ����  �  �  	� 	�	�	� l     ����  �  �  	� 	�	�	� l     � �����   ��  ��  	� 	�	�	� l     ��	�	���  	� m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   	� �	�	� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .	� 	�	�	� i   ( +	�	�	� I      ��	����� 0 	stopwatch  	� 	���	� o      ���� 0 mode  ��  ��  	� l    5	�	�	�	� k     5	�	� 	�	�	� q      	�	� ������ 0 x  ��  	� 	�	�	� Z     3	�	�	���	� l    	�����	� =     	�	�	� o     ���� 0 mode  	� m    	�	� �	�	� 
 s t a r t��  ��  	� k    	�	� 	�	�	� r    	�	�	� I    ��	����� 0 gettimestamp getTimestamp	� 	���	� m    ��
�� boovfals��  ��  	� o      ���� 0 x  	� 	���	� I   ��	���
�� .ascrcmnt****      � ****	� l   	�����	� b    	�	�	� m    	�	� �	�	�   b a c k u p   s t a r t e d :  	� o    ���� 0 x  ��  ��  ��  ��  	� 	�	�	� l   	�����	� =    	�	�	� o    ���� 0 mode  	� m    	�	� �	�	�  f i n i s h��  ��  	� 	���	� k    /	�	� 	�	�	� r    '	�	�	� I    %��	����� 0 gettimestamp getTimestamp	� 	���	� m     !��
�� boovfals��  ��  	� o      ���� 0 x  	� 	���	� I  ( /��	���
�� .ascrcmnt****      � ****	� l  ( +	�����	� b   ( +	�	�	� m   ( )	�	� �	�	� " b a c k u p   f i n i s h e d :  	� o   ) *���� 0 x  ��  ��  ��  ��  ��  ��  	� 	���	� l  4 4��������  ��  ��  ��  	�  (string)   	� �	�	�  ( s t r i n g )	� 	�	�	� l     ��������  ��  ��  	� 	�	�	� l     ��������  ��  ��  	� 
 

  l     ��������  ��  ��  
 


 l     ��������  ��  ��  
 


 l     ��������  ��  ��  
 


 l     ��

	��  
 M G Utility function to get the duration of the backup process in minutes.   
	 �



 �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .
 


 i   , /


 I      �������� 0 getduration getDuration��  ��  
 k     

 


 r     


 ^     


 l    
����
 \     


 o     ���� 0 endtime endTime
 o    ���� 0 	starttime 	startTime��  ��  
 m    ���� <
 o      ���� 0 duration  
 


 r    


 ^    


 l   
����
 I   ��
 ��
�� .sysorondlong        doub
  l   
!����
! ]    
"
#
" o    	���� 0 duration  
# m   	 
���� d��  ��  ��  ��  ��  
 m    ���� d
 o      ���� 0 duration  
 
$��
$ L    
%
% o    ���� 0 duration  ��  
 
&
'
& l     ��������  ��  ��  
' 
(
)
( l     ��
*
+��  
* � �-----------------------------------------------------------------------------------------------------------------------------------------------   
+ �
,
, - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
) 
-
.
- l     ��
/
0��  
/ � �-----------------------------------------------------------------------------------------------------------------------------------------------   
0 �
1
1 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
. 
2
3
2 l     ��������  ��  ��  
3 
4
5
4 l     ��������  ��  ��  
5 
6
7
6 l     ��������  ��  ��  
7 
8
9
8 l     ��������  ��  ��  
9 
:
;
: l     ��������  ��  ��  
; 
<
=
< i   0 3
>
?
> I      �������� 0 runonce runOnce��  ��  
? I     �������� 0 	plistinit 	plistInit��  ��  
= 
@
A
@ l     ��������  ��  ��  
A 
B
C
B l  � �
D����
D I   � ��������� 0 runonce runOnce��  ��  ��  ��  
C 
E
F
E l     ��������  ��  ��  
F 
G
H
G l     ��������  ��  ��  
H 
I
J
I l     ��
K
L��  
K w q Function that is always running in the background. This doesn't need to get called as it is running from the off   
L �
M
M �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f
J 
N
O
N l     ��
P
Q��  
P � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   
Q �
R
R�   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .
O 
S��
S i   4 7
T
U
T I     ������
�� .miscidlenmbr    ��� null��  ��  
U k     ^
V
V 
W
X
W Z     [
Y
Z����
Y l    
[��~
[ =     
\
]
\ o     �}�} 0 isbackingup isBackingUp
] m    �|
�| boovfals�  �~  
Z Z    W
^
_�{�z
^ l   
`�y�x
` A    
a
b
a l   
c�w�v
c n    
d
e
d 1    �u
�u 
hour
e l   
f�t�s
f I   �r�q�p
�r .misccurdldt    ��� null�q  �p  �t  �s  �w  �v  
b m    �o�o �y  �x  
_ k    S
g
g 
h
i
h r    
j
k
j l   
l�n�m
l n    
m
n
m 1    �l
�l 
min 
n l   
o�k�j
o l   
p�i�h
p I   �g�f�e
�g .misccurdldt    ��� null�f  �e  �i  �h  �k  �j  �n  �m  
k o      �d�d 0 m  
i 
q
r
q l   �c�b�a�c  �b  �a  
r 
s�`
s Z    S
t
u
v�_
t G    '
w
x
w l   
y�^�]
y =    
z
{
z o    �\�\ 0 m  
{ m    �[�[ �^  �]  
x l  " %
|�Z�Y
| =   " %
}
~
} o   " #�X�X 0 m  
~ m   # $�W�W -�Z  �Y  
u Z   * 9

��V�U
 l  * -
��T�S
� =   * -
�
�
� o   * +�R�R 0 scheduledtime scheduledTime
� m   + ,�Q�Q �T  �S  
� I   0 5�P�O�N�P 0 	plistinit 	plistInit�O  �N  �V  �U  
v 
�
�
� G   < G
�
�
� l  < ?
��M�L
� =   < ?
�
�
� o   < =�K�K 0 m  
� m   = >�J�J  �M  �L  
� l  B E
��I�H
� =   B E
�
�
� o   B C�G�G 0 m  
� m   C D�F�F �I  �H  
� 
��E
� I   J O�D�C�B�D 0 	plistinit 	plistInit�C  �B  �E  �_  �`  �{  �z  ��  ��  
X 
�
�
� l  \ \�A�@�?�A  �@  �?  
� 
��>
� L   \ ^
�
� m   \ ]�=�= <�>  ��       �<
�
�
�
�
�
�
�
�
�
�
�
�
�
�
�
��<  
� �;�:�9�8�7�6�5�4�3�2�1�0�/�.�-�; 0 	plistinit 	plistInit�: 0 diskinit diskInit�9 0 freespaceinit freeSpaceInit�8 0 
backupinit 
backupInit�7 0 tidyup tidyUp�6 
0 backup  �5 0 itexists itExists�4 .0 getcomputeridentifier getComputerIdentifier�3 0 gettimestamp getTimestamp�2 0 comparesizes compareSizes�1 0 	stopwatch  �0 0 getduration getDuration�/ 0 runonce runOnce
�. .miscidlenmbr    ��� null
�- .aevtoappnull  �   � ****
� �,9�+�*
�
��)�, 0 	plistinit 	plistInit�+  �*  
� �(�'�&�%�$�#�"�!�( 0 
foldername 
folderName�' 0 
backupdisk 
backupDisk�&  0 computerfolder computerFolder�% 0 
backuptime 
backupTime�$ 0 thecontents theContents�# 0 thevalue theValue�" 0 backuptimes backupTimes�! 0 selectedtime selectedTime
� 1R� ��������������������������������
�	����)9H����� �  	0 plist  � 0 itexists itExists
� 
plif
� 
pcnt
� 
valL�  0 foldertobackup FolderToBackup� 0 backupdrive BackupDrive�  0 computerfolder computerFolder� 0 scheduledtime scheduledTime� .0 getcomputeridentifier getComputerIdentifier�  ��
� 
prmp
� .sysostflalis    ��� null
� 
TEXT
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam�
 
�	 .corecrel****      � null
� 
plii
� 
insh
� 
kind� � 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .ascrcmnt****      � ****�  0 diskinit diskInit�)�jE�O*��l+ e  2� **��/�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYL*j+ E�O� ��n*��l E�O*�a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a �a  a !�la " # �*a a $a %*6a  a &a a !a '�a (a ( #O*a a $a %*6a  a &a a !a )�a (a ( #O*a a $a %*6a  a &a a !a *�a (a ( #O*a a $a %*6a  a &a a !a +�a (a ( #UUO�E` ,O�E` -O�E` .O�E�O_ ,_ -_ .�a "vj /O*j+ 0
� �������
�
����� 0 diskinit diskInit��  ��  
� ������ 0 msg  �� 	0 reply  
� ����������������������������������� "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit�� "0 messagesmissing messagesMissing
�� 
cobj
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit�� &0 messagescancelled messagesCancelled
�� 
appr
�� .sysonotfnull��� ��� TEXT�� L*��l+ e  *fk+ Y 8��.E�O����lv�l� �,E�O��  
*j+ Y ��.E�O�a a l 
� �������
�
����� 0 freespaceinit freeSpaceInit�� ��
��� 
�  ���� 	0 again  ��  
� �������� 	0 again  �� 
0 reply1  �� 0 msg  
� ������������������ '*6��������C���� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes�� 0 
backupinit 
backupInit
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp�� &0 messagescancelled messagesCancelled
�� 
cobj
�� 
appr
�� .sysonotfnull��� ��� TEXT�� l*��l+ e  
*j+ Y Y�f  ����lv�l� 
�,E�Y �e  ����lv�l� 
�,E�Y hO��  
*j+ Y _ a .E�O�a a l 
� ��\����
�
����� 0 
backupinit 
backupInit��  ��  
�  
� ������ty��������������������������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  �� 0 itexists itExists
�� 
kocl
�� 
cfol
�� 
insh
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null�� 0 machinefolder machineFolder
�� 
cdis�� 0 backupfolder backupFolder�� 0 initialbackup initialBackup�� 0 latestfolder latestFolder�� 
0 backup  �� �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY fE` O� *a �/�a /�a /E` OPUO*j+ 
� ��4����
�
����� 0 tidyup tidyUp��  ��  
� ���������������� 0 creationdates creationDates�� 0 theoldestdate theOldestDate�� 0 j  �� 0 i  �� 0 thisdate thisDate�� 
0 reply2  �� 0 msg  
� ���������������������������������������������� 0 backupfolder backupFolder
�� 
cobj
�� 
ascd
�� .corecnte****       ****
�� .ascrcmnt****      � ****
�� .coredeloobj        obj 
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit
�� 
trsh
�� .fndremptnull��� ��� obj �� &0 messagescancelled messagesCancelled
�� 
appr
�� .sysonotfnull��� ��� TEXT�� 0 freespaceinit freeSpaceInit
�� .sysodelanull��� ��� nmbr�� �� ���-�,E�O��k/E�OkE�O 3k�j kh ��/E�O�� �E�O�E�O��/j Y h[OY��O��/j O����lv�l� �,E�O��  *a ,j Y _ �.E�O�a a l O*ek+ Oa a a l Okj U
� �������
�
����� 
0 backup  ��  ��  
� 
���������������������� 0 t  �� 0 x  �� "0 containerfolder containerFolder�� 0 
foldername 
folderName�� 0 d  �� 0 duration  �� 0 msg  �� 0 c  �� 0 oldestfolder oldestFolder�� 0 todelete toDelete
� H������1������������������������������[��������~��}��|�{�������z�y�x�w�v�u>@BDWY[c�t�sy�����r�q��(*-7�� 0 gettimestamp getTimestamp�� 0 isbackingup isBackingUp�� 0 	stopwatch  
�� 
psxf�� 0 sourcefolder sourceFolder
�� 
TEXT
�� 
cfol
�� 
pnam�� 0 initialbackup initialBackup
�� 
kocl
�� 
insh�� 0 backupfolder backupFolder
�� 
prdt�� 
�� .corecrel****      � null�� (0 activesourcefolder activeSourceFolder
�� 
cdis�� 	0 drive  �� 0 machinefolder machineFolder�� (0 activebackupfolder activeBackupFolder
�� .misccurdldt    ��� null
� 
time�~ 0 	starttime 	startTime
�} 
appr
�| .sysonotfnull��� ��� TEXT�{ "0 destinationdisk destinationDisk
�z .sysoexecTEXT���     TEXT�y 0 endtime endTime�x 0 getduration getDuration�w $0 messagescomplete messagesComplete
�v 
cobj
�u .sysodelanull��� ��� nmbr
�t .ascrcmnt****      � ****�s *0 messagesencouraging messagesEncouraging
�r 
alis
�q 
psxp��g*ek+  E�OeE�O*�k+ O�F*��/�&E�O*�/�,E�O�f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hO�e  �*j a ,E` Oa a a l O_ a %_ %a  %�%a !%�&E�Oa "�%a #%�%a $%j %OfE�O*j a ,E` &O)j+ 'E�O_ (a ).E�Oa *�%a +%�%a a ,l Okj -YH�f A_ a .%_ %a /%�%a 0%�%a 1%�&E�O_ a 2%_ %a 3%�%a 4%�&E�Oa 5�%j 6O*j a ,E` O_ 7a ).E�O�a a 8l Oa 9�%a :%�%a ;%�%a <%j %OfE�O*�/a =&a )-jv  k��/�&E�O�a >,�&E�Oa ?�%j 6Oa @�%E�O�j %O*j a ,E` &O)j+ 'E�O_ (a ).E�Oa A�%a B%�%a a Cl Okj -Y =*j a ,E` &O)j+ 'E�O_ (a ).E�Oa D�%a E%�%a a Fl Okj -Y hUO*a Gk+ 
� �pa�o�n
�
��m�p 0 itexists itExists�o �l
��l 
�  �k�j�k 0 
objecttype 
objectType�j 
0 object  �n  
� �i�h�i 0 
objecttype 
objectType�h 
0 object  
� �q�g�f�e��d
�g 
cdis
�f .coredoexnull���     ****
�e 
file
�d 
cfol�m X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU
� �c��b�a
�
��`�c .0 getcomputeridentifier getComputerIdentifier�b  �a  
� �_�^�]�_ 0 computername computerName�^ "0 strserialnumber strSerialNumber�]  0 identifiername identifierName
� �\�[��Z�
�\ .sysosigtsirr   ��� null
�[ 
sicn
�Z .sysoexecTEXT���     TEXT�` *j  �,E�O�j E�O��%�%E�O�
� �Y��X�W
�
��V�Y 0 gettimestamp getTimestamp�X �U
��U 
�  �T�T 0 isfolder isFolder�W  
� �S�R�Q�P�O�N�M�L�K�J�I�H�G�F�E�D�S 0 isfolder isFolder�R 0 y  �Q 0 m  �P 0 d  �O 0 t  �N 0 ty tY�M 0 tm tM�L 0 td tD�K 0 tt tT�J 
0 tml tML�I 
0 tdl tDL�H 0 timestr timeStr�G 0 pos  �F 0 h  �E 0 s  �D 0 	timestamp  
� �C�B�A�@�?�>�=�<�;�:�9�8�7�6�5Ud�4�3��2�1�0�/��			*
�C 
Krtn
�B 
year�A 0 y  
�@ 
mnth�? 0 m  
�> 
day �= 0 d  
�< 
time�; 0 t  �: 
�9 .misccurdldt    ��� null
�8 
long
�7 
TEXT
�6 .corecnte****       ****
�5 
nmbr
�4 
tstr
�3 
psof
�2 
psin�1 
�0 .sysooffslong    ��� null
�/ 
cha �Vn*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�
� �.	H�-�,
�
��+�. 0 comparesizes compareSizes�- �*
��* 
�  �)�(�) 
0 source  �( 0 destination  �,  
� �'�&�%�$�#�' 
0 source  �& 0 destination  �% 0 fit  �$ 0 
foldersize 
folderSize�# 0 	freespace 	freeSpace
� �"	�	j	l�!� �����
�" 
psxf
�! .sysoexecTEXT���     TEXT�  � d
� .sysorondlong        doub
� 
cdis
� 
frsp
� .ascrcmnt****      � ****�+ _eE�O*�/E�O� >�%�%j E�O��!� j �!E�O*�/�,�!�!�!E�O�� j �!E�O��%j 
UO�� fE�Y hO�
� �	���
�
��� 0 	stopwatch  � �
�� 
�  �� 0 mode  �  
� ��� 0 mode  � 0 x  
� 	��	��	�	�� 0 gettimestamp getTimestamp
� .ascrcmnt****      � ****� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP
� �
��
�
��� 0 getduration getDuration�  �  
� �� 0 duration  
� ��
�	��� 0 endtime endTime�
 0 	starttime 	startTime�	 <� d
� .sysorondlong        doub� ���!E�O�� j �!E�O�
� �
?��
�
��� 0 runonce runOnce�  �  
�  
� �� 0 	plistinit 	plistInit� *j+  
� �
U� ��
�
���
� .miscidlenmbr    ��� null�   ��  
� ���� 0 m  
� �������������������������� 0 isbackingup isBackingUp
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit�� �� <�� _�f  V*j �,� F*j �,E�O�� 
 �� �& ��  
*j+ 	Y hY �j 
 �� �& 
*j+ 	Y hY hY hO�
� ��
�����
�
���
�� .aevtoappnull  �   � ****
� k     �
�
�  Y
�
�  j
�
�  o
�
�  v
�
�  �
�
�  �
�
�  �
�
� 
B����  ��  ��  
�  
� 0���������� f������ ~ � � � ����� � � � � � � � � ����� � � � � � � � � ��� � � � � �
������
�� afdrdlib
�� 
from
�� fldmfldu
�� .earsffdralis        afdr
�� 
psxp�� 	0 plist  �� 0 initialbackup initialBackup�� 0 isbackingup isBackingUp�� �� "0 messagesmissing messagesMissing�� 	�� *0 messagesencouraging messagesEncouraging�� $0 messagescomplete messagesComplete�� �� &0 messagescancelled messagesCancelled�� 0 runonce runOnce�� ����l �,�%E�OeE�OfE�O������vE�Oa a a a a a a a a a vE` Oa a a a a a  a !a "a #a vE` $Oa %a &a 'a (a )a *a +a ,a -vE` .O*j+ /ascr  ��ޭ