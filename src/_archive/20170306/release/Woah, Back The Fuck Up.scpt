FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��  ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.     �   �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .      l     ��   !��     � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ! � " "   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > .   # $ # l     ��������  ��  ��   $  % & % l     �� ' (��   ' ^ X Donkey icon/logo taken from http://thedeciderist.com/2013/01/29/the-party-of-less-evil/    ( � ) ) �   D o n k e y   i c o n / l o g o   t a k e n   f r o m   h t t p : / / t h e d e c i d e r i s t . c o m / 2 0 1 3 / 0 1 / 2 9 / t h e - p a r t y - o f - l e s s - e v i l / &  * + * l     ��������  ��  ��   +  , - , x     �� . /��   . 1      ��
�� 
ascr / �� 0��
�� 
minv 0 m       1 1 � 2 2  2 . 4��   -  3 4 3 x    �� 5����   5 2  	 ��
�� 
osax��   4  6 7 6 x     �� 8����   8 4    �� 9
�� 
frmk 9 m     : : � ; ;  F o u n d a t i o n��   7  < = < x     -�� >����   > 4   " &�� ?
�� 
frmk ? m   $ % @ @ � A A  A p p K i t��   =  B C B l     ��������  ��  ��   C  D E D j   - /�� F�� 0 
statusitem 
StatusItem F m   - .��
�� 
msng E  G H G l     ��������  ��  ��   H  I J I j   0 2�� K�� 0 
thedisplay 
theDisplay K m   0 1 L L � M M   J  N O N j   3 9�� P�� 0 defaults   P 4   3 8�� Q
�� 
pcls Q m   5 6 R R � S S  N S U s e r D e f a u l t s O  T U T j   : @�� V�� $0 internalmenuitem internalMenuItem V 4   : ?�� W
�� 
pcls W m   < = X X � Y Y  N S M e n u I t e m U  Z [ Z j   A I�� \�� $0 externalmenuitem externalMenuItem \ 4   A H�� ]
�� 
pcls ] m   C F ^ ^ � _ _  N S M e n u I t e m [  ` a ` j   J R�� b�� 0 newmenu newMenu b 4   J Q�� c
�� 
pcls c m   L O d d � e e  N S M e n u a  f g f j   S g�� h�� 0 thelist theList h J   S f i i  j k j m   S V l l � m m  B a c k u p   n o w k  n o n m   V Y p p � q q " B a c k u p   n o w   &   q u i t o  r s r m   Y \ t t � u u " T u r n   o n   t h e   b a n t s s  v w v m   \ _ x x � y y , T u r n   o f f   n o t i f i c a t i o n s w  z�� z m   _ b { { � | |  Q u i t��   g  } ~ } l     ��������  ��  ��   ~   �  j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � p   q q � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   q q � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ������ 0 endtime endTime��   �  � � � p   q q � � ������ 0 appicon appIcon��   �  � � � l     ����� � r      � � � 4     �� �
�� 
alis � l    ����� � b     � � � l   	 ����� � I   	�� � �
�� .earsffdralis        afdr �  f     � �� ���
�� 
rtyp � m    ��
�� 
ctxt��  ��  ��   � m   	 
 � � � � � < C o n t e n t s : R e s o u r c e s : a p p l e t . i c n s��  ��   � o      ���� 0 appicon appIcon��  ��   �  � � � l     ��������  ��  ��   �  � � � j   q ��� ��� 	0 plist   � b   q � � � � n  q � � � � 1   ~ ���
�� 
psxp � l  q ~ ����� � I  q ~�� � �
�� .earsffdralis        afdr � m   q t��
�� afdrdlib � �� ���
�� 
from � m   w z��
�� fldmfldu��  ��  ��   � m   � � � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � l     ��������  ��  ��   �  � � � j   � ��� ��� 0 initialbackup initialBackup � m   � ���
�� boovtrue �  � � � j   � ��� ��� 0 manualbackup manualBackup � m   � ���
�� boovfals �  � � � j   � ��� ��� 0 isbackingup isBackingUp � m   � ���
�� boovfals �  � � � l     ��������  ��  ��   �  � � � j   � ��� ��� "0 messagesmissing messagesMissing � J   � � � �  � � � m   � � � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m   � � � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �  � � � m   � � � � � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d ! �  � � � m   � � � � � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �  ��� � m   � � � � � � � � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .��   �  � � � j   � ��� ��� *0 messagesencouraging messagesEncouraging � J   � � � �  � � � m   � � � � � � �  C o m e   o n ! �  � � � m   � � � � � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r ! �  � � � m   � � � � � � � " N o   p a i n !   N o   p a i n ! �  � � � m   � � � � � � � 0 L e t ' s   d o   t h i s   a s   a   t e a m ! �  � � � m   � � � � � � �  W e   c a n   d o   i t ! �  � � � m   � � � � � � �  F r e e e e e e e e e d o m ! �  � � � m   � � � � � � � 2 A l t o g e t h e r   o r   n o t   a t   a l l ! �  � � � m   � �   � H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! ! �  m   � � � H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e ! �� m   � � �  Y o u   s e x y   f u c k !��   � 	
	 j   � ����� $0 messagescomplete messagesComplete J   � �  m   � � �  N i c e   o n e !  m   � � � * Y o u   a b s o l u t e   l e g   e n d !  m   � � �  G o o d   l a d !  m   � � �  P e r f i c k !  m   � � �    H a p p y   d a y s ! !"! m   � �## �$$  F r e e e e e e e e e d o m !" %&% m   � �'' �(( H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !& )*) m   � �++ �,, B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !* -.- m   � �// �00 d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !. 1��1 m   � �22 �33  Y o u   s e x y   f u c k !��  
 454 j   ���6�� &0 messagescancelled messagesCancelled6 J   �77 898 m   � �:: �;;  T h a t ' s   a   s h a m e9 <=< m   � �>> �??  A h   m a n ,   u n l u c k y= @A@ m   � �BB �CC  O h   w e l lA DED m   � �FF �GG @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e nE HIH m   �JJ �KK , W e l l   t h a t ' s   a   l e t   d o w nI LML m  NN �OO P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?M PQP m  RR �SS  A r r o g a n tQ TUT m  
VV �WW > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0U X��X m  
YY �ZZ  E l l   b e n d��  5 [\[ j  7��]�� 40 messagesalreadybackingup messagesAlreadyBackingUp] J  4^^ _`_ m  aa �bb  T h a t ' s   a   s h a m e` cdc m  ee �ff . T h o u g h t   y o u   w e r e   c l e v e rd ghg m  ii �jj > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a th klk m  !mm �nn  D i c kl opo m  !$qq �rr  F u c k t a r dp sts m  $'uu �vv J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n st wxw m  '*yy �zz  A r r o g a n tx {|{ m  *-}} �~~ $ C h i l l   t h e   f u c k   o u t| �� m  -0�� ���  H o l d   f i r e��  \ ��� l     ��������  ��  ��  � ��� j  8>����� 0 strawake strAwake� m  8;�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  ?E����� 0 strsleep strSleep� m  ?B�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  FP����� &0 displaysleepstate displaySleepState� I FM�����
�� .sysoexecTEXT���     TEXT� m  FI�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t��  � ��� l     ����~��  �  �~  � ��� l     �}���}  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� i  QT��� I      �|�{�z�| 0 	plistinit 	plistInit�{  �z  � k    �� ��� q      �� �y��y 0 
foldername 
folderName� �x��x 0 
backupdisk 
backupDisk� �w��w  0 computerfolder computerFolder� �v�u�v 0 
backuptime 
backupTime�u  � ��� r     ��� m     �t�t  � o      �s�s 0 
backuptime 
backupTime� ��� l   �r�q�p�r  �q  �p  � ��� Z   ����o�� l   ��n�m� =    ��� I    �l��k�l 0 itexists itExists� ��� m    �� ���  f i l e� ��j� o    �i�i 	0 plist  �j  �k  � m    �h
�h boovtrue�n  �m  � O    C��� k    B�� ��� r    $��� n    "��� 1     "�g
�g 
pcnt� 4     �f�
�f 
plif� o    �e�e 	0 plist  � o      �d�d 0 thecontents theContents� ��� r   % *��� n   % (��� 1   & (�c
�c 
valL� o   % &�b�b 0 thecontents theContents� o      �a�a 0 thevalue theValue� ��� l  + +�`�_�^�`  �_  �^  � ��� r   + 0��� n   + .��� o   , .�]�]  0 foldertobackup FolderToBackup� o   + ,�\�\ 0 thevalue theValue� o      �[�[ 0 
foldername 
folderName� ��� r   1 6��� n   1 4��� o   2 4�Z�Z 0 backupdrive BackupDrive� o   1 2�Y�Y 0 thevalue theValue� o      �X�X 0 
backupdisk 
backupDisk� ��� r   7 <��� n   7 :��� o   8 :�W�W  0 computerfolder computerFolder� o   7 8�V�V 0 thevalue theValue� o      �U�U  0 computerfolder computerFolder� ��T� r   = B��� n   = @��� o   > @�S�S 0 scheduledtime scheduledTime� o   = >�R�R 0 thevalue theValue� o      �Q�Q 0 
backuptime 
backupTime�T  � m    ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �o  � k   F��� ��� r   F M��� I   F K�P�O�N�P .0 getcomputeridentifier getComputerIdentifier�O  �N  � o      �M�M  0 computerfolder computerFolder� ��� l  N N�L�K�J�L  �K  �J  � ��� O   N9��� t   R8��� k   T7�� ��� n  T Y��� I   U Y�I�H�G�I 0 setfocus setFocus�H  �G  �  f   T U� ��� l  Z Z�F�E�D�F  �E  �D  � ��� r   Z ]��� m   Z [�� ���L T h i s   p r o g r a m   a u t o m a t i c a l l y   b a c k s   u p   y o u r   H o m e   f o l d e r   b u t   e x c l u d e s   t h e   C a c h e s ,   L o g s   a n d   M o b i l e   D o c u m e n t s   f o l d e r s   f r o m   y o u r   L i b r a r y . 
 
 A l l   y o u   n e e d   t o   d o   i s   s e l e c t   t h e   e x t e r n a l   d r i v e   t o   b a c k u p   t o   a t   t h e   n e x t   p r o m p t . 
 
 Y o u r   b a c k u p s   w i l l   b e   s a v e d   i n : 
 [ E x t e r n a l   H a r d   D r i v e ]   >   B a c k u p s   >   [ C o m p u t e r   N a m e ]� o      �C�C 0 welcome  � ��� l  ^ ^�B�A�@�B  �A  �@  �    Z   ^ ��? l  ^ e�>�= =   ^ e o   ^ c�<�< 0 nsfw   m   c d�;
�; boovtrue�>  �=   r   h k	 m   h i

 � , W o a h ,   B a c k   T h e   F u c k   U p	 o      �:�: 0 	temptitle 	tempTitle  l  n u�9�8 =   n u o   n s�7�7 0 nsfw   m   s t�6
�6 boovfals�9  �8   �5 r   x } m   x { � 
 W B T F U o      �4�4 0 	temptitle 	tempTitle�5  �?    l  � ��3�2�1�3  �2  �1    r   � � l  � ��0�/ n   � � 1   � ��.
�. 
bhit l  � ��-�, I  � ��+ !
�+ .sysodlogaskr        TEXT  o   � ��*�* 0 welcome  ! �)"#
�) 
appr" o   � ��(�( 0 	temptitle 	tempTitle# �'$%
�' 
disp$ o   � ��&�& 0 appicon appIcon% �%&'
�% 
btns& J   � �(( )�$) m   � �** �++  O K ,   g o t   i t�$  ' �#,�"
�# 
dflt, m   � ��!�! �"  �-  �,  �0  �/   o      � �  	0 reply   -.- l  � �����  �  �  . /0/ r   � �121 I  � ��3�
� .earsffdralis        afdr3 l  � �4��4 m   � ��
� afdrcusr�  �  �  2 o      �� 0 
foldername 
folderName0 565 l  � �����  �  �  6 787 Q   � �9:;9 r   � �<=< c   � �>?> l  � �@��@ I  � ���A
� .sysostflalis    ��� null�  A �B�
� 
prmpB m   � �CC �DD R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o�  �  �  ? m   � ��
� 
TEXT= o      �� 0 
backupdisk 
backupDisk: R      ��
�	
� .ascrerr ****      � ****�
  �	  ; I  � ��E�
� .aevtquitnull��� ��� nullE m   � �FF                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  8 GHG l  � �����  �  �  H IJI r   � �KLK n   � �MNM 1   � ��
� 
psxpN o   � ��� 0 
foldername 
folderNameL o      �� 0 
foldername 
folderNameJ OPO r   � �QRQ n   � �STS 1   � �� 
�  
psxpT o   � ����� 0 
backupdisk 
backupDiskR o      ���� 0 
backupdisk 
backupDiskP UVU l  � ���������  ��  ��  V WXW r   � �YZY J   � �[[ \]\ m   � �^^ �__ 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r] `a` m   � �bb �cc 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u ra d��d m   � �ee �ff , E v e r y   h o u r   o n   t h e   h o u r��  Z o      ���� 0 backuptimes backupTimesX ghg r   �iji c   �klk J   � �mm n��n I  � ���op
�� .gtqpchltns    @   @ ns  o o   � ����� 0 backuptimes backupTimesp ��q��
�� 
prmpq m   � �rr �ss 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  l m   ���
�� 
TEXTj o      ���� 0 selectedtime selectedTimeh tut l ��������  ��  ��  u v��v Z  7wxy��w l z����z =  {|{ o  ���� 0 selectedtime selectedTime| m  
}} �~~ 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  x r  � m  ���� � o      ���� 0 
backuptime 
backupTimey ��� l ������ =  ��� o  ���� 0 selectedtime selectedTime� m  �� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r  #��� m  !���� � o      ���� 0 
backuptime 
backupTime� ��� l &+������ =  &+��� o  &'���� 0 selectedtime selectedTime� m  '*�� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r  .3��� m  .1���� <� o      ���� 0 
backuptime 
backupTime��  ��  ��  � m   R S����  ��� m   N O���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l ::��������  ��  ��  � ���� O  :���� O  >���� k  Y��� ��� I Y������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ]`��
�� 
plii� ����
�� 
insh�  ;  ce� �����
�� 
prdt� K  hz�� ����
�� 
kind� m  kn��
�� 
TEXT� ����
�� 
pnam� m  qt�� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  uv���� 0 
foldername 
folderName��  ��  � ��� I �������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ����
�� 
plii� ����
�� 
insh�  ;  ��� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  B a c k u p D r i v e� �����
�� 
valL� o  ������ 0 
backupdisk 
backupDisk��  ��  � ��� I �������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ����
�� 
plii� ����
�� 
insh�  ;  ��� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  ������  0 computerfolder computerFolder��  ��  � ���� I �������
�� .corecrel****      � null��  � ����
�� 
kocl� m  ����
�� 
plii� ����
�� 
insh�  ;  ��� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  S c h e d u l e d T i m e� �����
�� 
valL� o  ������ 0 
backuptime 
backupTime��  ��  ��  � l >V������ I >V�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  BC��
�� 
plif� �����
�� 
prdt� K  FP�� �����
�� 
pnam� o  IN���� 	0 plist  ��  ��  ��  ��  � m  :;���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  � ��� l ����������  ��  ��  � ��� r  � ��� o  ������ 0 
foldername 
folderName� o      ���� 0 sourcefolder sourceFolder� ��� r  ��� o  ���� 0 
backupdisk 
backupDisk� o      ���� "0 destinationdisk destinationDisk� ��� r  ��� o  ����  0 computerfolder computerFolder� o      ���� 0 machinefolder machineFolder� ��� r  ��� o  ���� 0 
backuptime 
backupTime� o      ���� 0 scheduledtime scheduledTime� ��� l ��������  ��  ��  � ���� I  �������� 0 diskinit diskInit��  ��  ��  � ��� l     ��������  ��  ��  � ��� i  UX� � I      �������� 0 diskinit diskInit��  ��    Z     �� l    	�~�} =     	 I     �|�{�| 0 itexists itExists 	 m    

 �  d i s k	 �z o    �y�y "0 destinationdisk destinationDisk�z  �{   m    �x
�x boovtrue�~  �}   I    �w�v�w 0 freespaceinit freeSpaceInit �u m    �t
�t boovfals�u  �v  �   k    �  n    I    �s�r�q�s 0 setfocus setFocus�r  �q    f      r    $ n    " 3     "�p
�p 
cobj o     �o�o "0 messagesmissing messagesMissing o      �n�n 0 msg    l  % %�m�l�k�m  �l  �k    Z   % F �j l  % ,!�i�h! =   % ,"#" o   % *�g�g 0 nsfw  # m   * +�f
�f boovtrue�i  �h   r   / 2$%$ m   / 0&& �'' , W o a h ,   B a c k   T h e   F u c k   U p% o      �e�e 0 	temptitle 	tempTitle  ()( l  5 <*�d�c* =   5 <+,+ o   5 :�b�b 0 nsfw  , m   : ;�a
�a boovfals�d  �c  ) -�`- r   ? B./. m   ? @00 �11 
 W B T F U/ o      �_�_ 0 	temptitle 	tempTitle�`  �j   232 l  G G�^�]�\�^  �]  �\  3 454 r   G ]676 l  G [8�[�Z8 n   G [9:9 1   W [�Y
�Y 
bhit: l  G W;�X�W; I  G W�V<=
�V .sysodlogaskr        TEXT< o   G H�U�U 0 msg  = �T>?
�T 
appr> o   I J�S�S 0 	temptitle 	tempTitle? �R@A
�R 
disp@ o   K L�Q�Q 0 appicon appIconA �PBC
�P 
btnsB J   M QDD EFE m   M NGG �HH  C a n c e l   B a c k u pF I�OI m   N OJJ �KK  O K�O  C �NL�M
�N 
dfltL m   R S�L�L �M  �X  �W  �[  �Z  7 o      �K�K 	0 reply  5 M�JM Z   ^ �NO�IPN l  ^ cQ�H�GQ =   ^ cRSR o   ^ _�F�F 	0 reply  S m   _ bTT �UU  O K�H  �G  O I   f k�E�D�C�E 0 diskinit diskInit�D  �C  �I  P Z   n �VW�B�AV l  n yX�@�?X E   n yYZY o   n s�>�> &0 displaysleepstate displaySleepStateZ o   s x�=�= 0 strawake strAwake�@  �?  W k   | �[[ \]\ r   | �^_^ n   | �`a` 3   � ��<
�< 
cobja o   | ��;�; &0 messagescancelled messagesCancelled_ o      �:�: 0 msg  ] b�9b Z   � �cd�8�7c l  � �e�6�5e =   � �fgf o   � ��4�4 0 notifications  g m   � ��3
�3 boovtrue�6  �5  d Z   � �hij�2h l  � �k�1�0k =   � �lml o   � ��/�/ 0 nsfw  m m   � ��.
�. boovtrue�1  �0  i I  � ��-no
�- .sysonotfnull��� ��� TEXTn o   � ��,�, 0 msg  o �+p�*
�+ 
apprp m   � �qq �rr > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�*  j sts l  � �u�)�(u =   � �vwv o   � ��'�' 0 nsfw  w m   � ��&
�& boovfals�)  �(  t x�%x I  � ��$yz
�$ .sysonotfnull��� ��� TEXTy m   � �{{ �|| , Y o u   s t o p p e d   b a c k i n g   u pz �#}�"
�# 
appr} m   � �~~ � 
 W B T F U�"  �%  �2  �8  �7  �9  �B  �A  �J  � ��� l     �!� ��!  �   �  � ��� i  Y\��� I      ���� 0 freespaceinit freeSpaceInit� ��� o      �� 	0 again  �  �  � Z     ������ l    	���� =     	��� I     ���� 0 comparesizes compareSizes� ��� o    �� 0 sourcefolder sourceFolder� ��� o    �� "0 destinationdisk destinationDisk�  �  � m    �
� boovtrue�  �  � I    ���� 0 
backupinit 
backupInit�  �  �  � k    ��� ��� n   ��� I    ���� 0 setfocus setFocus�  �  �  f    � ��� l   ��
�	�  �
  �	  � ��� Z    ;����� l   !���� =    !��� o    �� 0 nsfw  � m     �
� boovtrue�  �  � r   $ '��� m   $ %�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      �� 0 	temptitle 	tempTitle� ��� l  * 1���� =   * 1��� o   * /� �  0 nsfw  � m   / 0��
�� boovfals�  �  � ���� r   4 7��� m   4 5�� ��� 
 W B T F U� o      ���� 0 	temptitle 	tempTitle��  �  � ��� l  < <��������  ��  ��  � ��� Z   < ������� l  < ?������ =   < ?��� o   < =���� 	0 again  � m   = >��
�� boovfals��  ��  � r   B X��� l  B V������ n   B V��� 1   R V��
�� 
bhit� l  B R������ I  B R����
�� .sysodlogaskr        TEXT� m   B C�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
appr� o   D E���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   F G���� 0 appicon appIcon� ����
�� 
btns� J   H L�� ��� m   H I�� ���  Y e s� ���� m   I J�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   M N���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ��� l  [ ^������ =   [ ^��� o   [ \���� 	0 again  � m   \ ]��
�� boovtrue��  ��  � ���� r   a }��� l  a {������ n   a {��� 1   w {��
�� 
bhit� l  a w������ I  a w����
�� .sysodlogaskr        TEXT� m   a d�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
appr� o   e f���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   g h���� 0 appicon appIcon� ����
�� 
btns� J   i q�� ��� m   i l�� ���  Y e s� ���� m   l o�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   r s���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  ��  ��  � ��� l  � ���������  ��  ��  � ���� Z   � ������� l  � ������� =   � ���� o   � ����� 	0 reply  � m   � ��� ���  Y e s��  ��  � I   � ��������� 0 tidyup tidyUp��  ��  ��  � k   � ��� ��� Z   � �� ����� l  � ����� E   � � o   � ����� &0 displaysleepstate displaySleepState o   � ����� 0 strawake strAwake��  ��    r   � � n   � � 3   � ���
�� 
cobj o   � ����� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��  � 	 l  � ���������  ��  ��  	 
��
 Z   � ����� l  � ����� =   � � o   � ����� 0 notifications   m   � ���
�� boovtrue��  ��   Z   � ��� l  � ����� =   � � o   � ����� 0 nsfw   m   � ���
�� boovtrue��  ��   I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � � � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��    l  � ����� =   � � o   � ����� 0 nsfw   m   � ���
�� boovfals��  ��    ��  I  � ���!"
�� .sysonotfnull��� ��� TEXT! m   � �## �$$ , Y o u   s t o p p e d   b a c k i n g   u p" ��%��
�� 
appr% m   � �&& �'' 
 W B T F U��  ��  ��  ��  ��  ��  ��  � ()( l     ��������  ��  ��  ) *+* i  ]`,-, I      �������� 0 
backupinit 
backupInit��  ��  - k     �.. /0/ r     121 4     ��3
�� 
psxf3 o    ���� "0 destinationdisk destinationDisk2 o      ���� 	0 drive  0 454 l   ��������  ��  ��  5 676 Z    ,89����8 l   :����: =    ;<; I    ��=���� 0 itexists itExists= >?> m    	@@ �AA  f o l d e r? B��B b   	 CDC o   	 
���� "0 destinationdisk destinationDiskD m   
 EE �FF  B a c k u p s��  ��  < m    ��
�� boovfals��  ��  9 O    (GHG I   '����I
�� .corecrel****      � null��  I ��JK
�� 
koclJ m    ��
�� 
cfolK ��LM
�� 
inshL o    ���� 	0 drive  M ��N��
�� 
prdtN K    #OO ��P��
�� 
pnamP m     !QQ �RR  B a c k u p s��  ��  H m    SS�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ��  7 TUT l  - -�������  ��  �  U VWV Z   - dXY�~�}X l  - >Z�|�{Z =   - >[\[ I   - <�z]�y�z 0 itexists itExists] ^_^ m   . /`` �aa  f o l d e r_ b�xb b   / 8cdc b   / 4efe o   / 0�w�w "0 destinationdisk destinationDiskf m   0 3gg �hh  B a c k u p s /d o   4 7�v�v 0 machinefolder machineFolder�x  �y  \ m   < =�u
�u boovfals�|  �{  Y O   A `iji I  E _�t�sk
�t .corecrel****      � null�s  k �rlm
�r 
kocll m   G H�q
�q 
cfolm �pno
�p 
inshn n   I Tpqp 4   O T�or
�o 
cfolr m   P Sss �tt  B a c k u p sq 4   I O�nu
�n 
cdisu o   M N�m�m 	0 drive  o �lv�k
�l 
prdtv K   U [ww �jx�i
�j 
pnamx o   V Y�h�h 0 machinefolder machineFolder�i  �k  j m   A Byy�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �~  �}  W z{z l  e e�g�f�e�g  �f  �e  { |}| O   e ~~~ r   i }��� n   i y��� 4   t y�d�
�d 
cfol� o   u x�c�c 0 machinefolder machineFolder� n   i t��� 4   o t�b�
�b 
cfol� m   p s�� ���  B a c k u p s� 4   i o�a�
�a 
cdis� o   m n�`�` 	0 drive  � o      �_�_ 0 backupfolder backupFolder m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  } ��� l   �^�]�\�^  �]  �\  � ��� Z    ����[�� l   ���Z�Y� =    ���� I    ��X��W�X 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��V� b   � ���� b   � ���� b   � ���� o   � ��U�U "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��T�T 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�V  �W  � m   � ��S
�S boovfals�Z  �Y  � O   � ���� I  � ��R�Q�
�R .corecrel****      � null�Q  � �P��
�P 
kocl� m   � ��O
�O 
cfol� �N��
�N 
insh� o   � ��M�M 0 backupfolder backupFolder� �L��K
�L 
prdt� K   � ��� �J��I
�J 
pnam� m   � ��� ���  L a t e s t�I  �K  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �[  � r   � ���� m   � ��H
�H boovfals� o      �G�G 0 initialbackup initialBackup� ��� l  � ��F�E�D�F  �E  �D  � ��� O   � ���� r   � ���� n   � ���� 4   � ��C�
�C 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ��B�
�B 
cfol� o   � ��A�A 0 machinefolder machineFolder� n   � ���� 4   � ��@�
�@ 
cfol� m   � ��� ���  B a c k u p s� 4   � ��?�
�? 
cdis� o   � ��>�> 	0 drive  � o      �=�= 0 latestfolder latestFolder� m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��<�;�:�<  �;  �:  � ��9� I   � ��8�7�6�8 
0 backup  �7  �6  �9  + ��� l     �5�4�3�5  �4  �3  � ��� i  ad��� I      �2�1�0�2 0 tidyup tidyUp�1  �0  � k     ��� ��� r     ��� 4     �/�
�/ 
psxf� o    �.�. "0 destinationdisk destinationDisk� o      �-�- 	0 drive  � ��� l   �,�+�*�,  �+  �*  � ��)� O    ���� k    ��� ��� r    ��� n    ��� 4    �(�
�( 
cfol� o    �'�' 0 machinefolder machineFolder� n    ��� 4    �&�
�& 
cfol� m    �� ���  B a c k u p s� 4    �%�
�% 
cdis� o    �$�$ 	0 drive  � o      �#�# 0 bf bF� ��� r    ��� n    ��� 1    �"
�" 
ascd� n    ��� 2   �!
�! 
cobj� o    � �  0 bf bF� o      �� 0 creationdates creationDates� ��� r     &��� n     $��� 4   ! $��
� 
cobj� m   " #�� � o     !�� 0 creationdates creationDates� o      �� 0 theoldestdate theOldestDate� ��� r   ' *� � m   ' (��   o      �� 0 j  �  l  + +����  �  �    Y   + e�� k   9 ` 	
	 r   9 ? n   9 = 4   : =�
� 
cobj o   ; <�� 0 i   o   9 :�� 0 creationdates creationDates o      �� 0 thisdate thisDate
 � Z   @ `�� l  @ H�� >  @ H n   @ F 1   D F�

�
 
pnam 4   @ D�	
�	 
cobj o   B C�� 0 i   m   F G �  L a t e s t�  �   Z   K \�� l  K N�� A   K N o   K L�� 0 thisdate thisDate o   L M�� 0 theoldestdate theOldestDate�  �   k   Q X   !"! r   Q T#$# o   Q R�� 0 thisdate thisDate$ o      � �  0 theoldestdate theOldestDate" %��% r   U X&'& o   U V���� 0 i  ' o      ���� 0 j  ��  �  �  �  �  �  � 0 i   m   . /����  I  / 4��(��
�� .corecnte****       ****( o   / 0���� 0 creationdates creationDates��  �   )*) l  f f��������  ��  ��  * +,+ r   f p-.- c   f n/0/ l  f l1����1 n   f l232 4   g l��4
�� 
cobj4 l  h k5����5 [   h k676 o   h i���� 0 j  7 m   i j���� ��  ��  3 o   f g���� 0 bf bF��  ��  0 m   l m��
�� 
TEXT. o      ����  0 foldertodelete folderToDelete, 898 r   q x:;: c   q v<=< n   q t>?> 1   r t��
�� 
psxp? o   q r����  0 foldertodelete folderToDelete= m   t u��
�� 
TEXT; o      ����  0 foldertodelete folderToDelete9 @A@ l  y y��������  ��  ��  A BCB r   y �DED b   y �FGF b   y |HIH m   y zJJ �KK  r m   - r f   'I o   z {����  0 foldertodelete folderToDeleteG m   | LL �MM  'E o      ���� 0 todelete toDeleteC NON I  � ���P��
�� .sysoexecTEXT���     TEXTP o   � ����� 0 todelete toDelete��  O QRQ l  � ���������  ��  ��  R STS Z   � �UV����U l  � �W����W E   � �XYX o   � ����� &0 displaysleepstate displaySleepStateY o   � ����� 0 strawake strAwake��  ��  V Z   � �Z[����Z l  � �\����\ =   � �]^] o   � ����� 0 notifications  ^ m   � ���
�� boovtrue��  ��  [ Z   � �_`a��_ l  � �b����b =   � �cdc o   � ����� 0 nsfw  d m   � ���
�� boovtrue��  ��  ` k   � �ee fgf I  � ���hi
�� .sysonotfnull��� ��� TEXTh m   � �jj �kk � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .i ��l��
�� 
apprl m   � �mm �nn 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  g o��o I  � ���p��
�� .sysodelanull��� ��� nmbrp m   � ����� ��  ��  a qrq l  � �s����s =   � �tut o   � ����� 0 nsfw  u m   � ���
�� boovfals��  ��  r v��v k   � �ww xyx I  � ���z{
�� .sysonotfnull��� ��� TEXTz m   � �|| �}} t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n{ ��~��
�� 
appr~ m   � � ��� 
 W B T F U��  y ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  ��  ��  ��  ��  ��  ��  T ��� l  � ���������  ��  ��  � ���� I   � �������� 0 freespaceinit freeSpaceInit� ���� m   � ���
�� boovtrue��  ��  ��  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �)  � ��� l     ��������  ��  ��  � ��� i  eh��� I      �������� 
0 backup  ��  ��  � k    j�� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovtrue��  ��  � o      ���� 0 t  � ��� l  	 	��������  ��  ��  � ��� r   	 ��� m   	 
��
�� boovtrue� o      ���� 0 isbackingup isBackingUp� ��� l   ��������  ��  ��  � ��� I    �������� (0 animatemenubaricon animateMenuBarIcon��  ��  � ��� I   �����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ��� l   ��������  ��  ��  � ��� I    #������� 0 	stopwatch  � ���� m    �� ��� 
 s t a r t��  ��  � ��� l  $ $��������  ��  ��  � ��� O   $a��� k   (`�� ��� r   ( 0��� c   ( .��� 4   ( ,���
�� 
psxf� o   * +���� 0 sourcefolder sourceFolder� m   , -��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   1 9��� n   1 7��� 1   5 7��
�� 
pnam� 4   1 5���
�� 
cfol� o   3 4���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  : :��������  ��  ��  � ��� Z   : �����~� l  : A��}�|� =   : A��� o   : ?�{�{ 0 initialbackup initialBackup� m   ? @�z
�z boovfals�}  �|  � k   D ��� ��� I  D R�y�x�
�y .corecrel****      � null�x  � �w��
�w 
kocl� m   F G�v
�v 
cfol� �u��
�u 
insh� o   H I�t�t 0 backupfolder backupFolder� �s��r
�s 
prdt� K   J N�� �q��p
�q 
pnam� o   K L�o�o 0 t  �p  �r  � ��� l  S S�n�m�l�n  �m  �l  � ��� r   S ]��� c   S Y��� 4   S W�k�
�k 
psxf� o   U V�j�j 0 sourcefolder sourceFolder� m   W X�i
�i 
TEXT� o      �h�h (0 activesourcefolder activeSourceFolder� ��� r   ^ h��� 4   ^ d�g�
�g 
cfol� o   ` c�f�f (0 activesourcefolder activeSourceFolder� o      �e�e (0 activesourcefolder activeSourceFolder� ��� r   i ���� n   i ~��� 4   { ~�d�
�d 
cfol� o   | }�c�c 0 t  � n   i {��� 4   v {�b�
�b 
cfol� o   w z�a�a 0 machinefolder machineFolder� n   i v��� 4   q v�`�
�` 
cfol� m   r u�� ���  B a c k u p s� 4   i q�_�
�_ 
cdis� o   m p�^�^ 	0 drive  � o      �]�] (0 activebackupfolder activeBackupFolder� ��\� I  � ��[�Z�
�[ .corecrel****      � null�Z  � �Y��
�Y 
kocl� m   � ��X
�X 
cfol� �W��
�W 
insh� o   � ��V�V (0 activebackupfolder activeBackupFolder� �U��T
�U 
prdt� K   � ��� �S��R
�S 
pnam� o   � ��Q�Q 0 
foldername 
folderName�R  �T  �\  �  �~  �    l  � ��P�O�N�P  �O  �N   �M Z   �`�L l  � ��K�J =   � � o   � ��I�I 0 initialbackup initialBackup m   � ��H
�H boovtrue�K  �J   k   ��		 

 r   � � n   � � 1   � ��G
�G 
time l  � ��F�E I  � ��D�C�B
�D .misccurdldt    ��� null�C  �B  �F  �E   o      �A�A 0 	starttime 	startTime  Z   ��@�? l  � ��>�= E   � � o   � ��<�< &0 displaysleepstate displaySleepState o   � ��;�; 0 strawake strAwake�>  �=   Z   ��:�9 l  � ��8�7 =   � � o   � ��6�6 0 notifications   m   � ��5
�5 boovtrue�8  �7   Z   � ��4 l  � � �3�2  =   � �!"! o   � ��1�1 0 nsfw  " m   � ��0
�0 boovtrue�3  �2   I  � ��/#$
�/ .sysonotfnull��� ��� TEXT# m   � �%% �&& � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .$ �.'�-
�. 
appr' m   � �(( �)) 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�-   *+* l  � �,�,�+, =   � �-.- o   � ��*�* 0 nsfw  . m   � ��)
�) boovfals�,  �+  + /�(/ I  � ��'01
�' .sysonotfnull��� ��� TEXT0 m   � �22 �33 \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e1 �&4�%
�& 
appr4 m   � �55 �66 
 W B T F U�%  �(  �4  �:  �9  �@  �?   787 l �$�#�"�$  �#  �"  8 9:9 r  ;<; c  =>= l ?�!� ? b  @A@ b  BCB b  DED b  FGF b  HIH o  	�� "0 destinationdisk destinationDiskI m  	JJ �KK  B a c k u p s /G o  �� 0 machinefolder machineFolderE m  LL �MM  / L a t e s t /C o  �� 0 
foldername 
folderNameA m  NN �OO  /�!  �   > m  �
� 
TEXT< o      �� 0 d  : PQP l   ����  �  �  Q RSR Q   ?TUVT I #6�W�
� .sysoexecTEXT���     TEXTW b  #2XYX b  #.Z[Z b  #,\]\ b  #(^_^ m  #&`` �aa	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   '_ o  &'�� 0 sourcefolder sourceFolder] m  (+bb �cc  '   '[ o  ,-�� 0 d  Y m  .1dd �ee  / '�  U R      ���
� .ascrerr ****      � ****�  �  V l >>����  �  �  S fgf l @@����  �  �  g hih r  @Gjkj m  @A�

�
 boovfalsk o      �	�	 0 isbackingup isBackingUpi lml r  HOnon m  HI�
� boovfalso o      �� 0 manualbackup manualBackupm pqp l PP����  �  �  q rsr Z  P�tu��t l P[v�� v E  P[wxw o  PU���� &0 displaysleepstate displaySleepStatex o  UZ���� 0 strawake strAwake�  �   u k  ^�yy z{z r  ^k|}| n  ^g~~ 1  cg��
�� 
time l ^c������ I ^c������
�� .misccurdldt    ��� null��  ��  ��  ��  } o      ���� 0 endtime endTime{ ��� r  ls��� n lq��� I  mq�������� 0 getduration getDuration��  ��  �  f  lm� o      ���� 0 duration  � ��� r  t��� n  t}��� 3  y}��
�� 
cobj� o  ty���� $0 messagescomplete messagesComplete� o      ���� 0 msg  � ��� Z  ��������� l �������� =  ����� o  ������ 0 notifications  � m  ����
�� boovtrue��  ��  � Z  �������� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovtrue��  ��  � k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  � ��� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
� �����
�� 
appr� m  ���� ���  B a c k e d   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  � ��� l ����������  ��  ��  � ���� n ����� I  ���������� $0 resetmenubaricon resetMenuBarIcon��  ��  �  f  ����  �  �  s ��� l ����������  ��  ��  � ���� Z  ��������� l �������� =  ����� o  ������  0 backupthenquit backupThenQuit� m  ����
�� boovtrue��  ��  � I �������
�� .aevtquitnull��� ��� null� m  ����                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ��  ��   ��� l  ������ =   ��� o   ���� 0 initialbackup initialBackup� m  ��
�� boovfals��  ��  � ���� k  
\�� ��� r  
)��� c  
'��� l 
%������ b  
%��� b  
!��� b  
��� b  
��� b  
��� b  
��� b  
��� o  
���� "0 destinationdisk destinationDisk� m  �� ���  B a c k u p s /� o  ���� 0 machinefolder machineFolder� m  �� ���  /� o  ���� 0 t  � m  �� ���  /� o   ���� 0 
foldername 
folderName� m  !$�� ���  /��  ��  � m  %&��
�� 
TEXT� o      ���� 0 c  � ��� r  *C��� c  *A��� l *?������ b  *?   b  *; b  *9 b  *5 b  *1	 o  *-���� "0 destinationdisk destinationDisk	 m  -0

 �  B a c k u p s / o  14���� 0 machinefolder machineFolder m  58 �  / L a t e s t / o  9:���� 0 
foldername 
folderName m  ;> �  /��  ��  � m  ?@��
�� 
TEXT� o      ���� 0 d  �  l DD��������  ��  ��    Z  D����� l DO���� E  DO o  DI���� &0 displaysleepstate displaySleepState o  IN���� 0 strawake strAwake��  ��   k  R�  r  R_ n  R[ 1  W[��
�� 
time l RW ����  I RW������
�� .misccurdldt    ��� null��  ��  ��  ��   o      ���� 0 	starttime 	startTime !"! r  `k#$# n  `i%&% 3  ei��
�� 
cobj& o  `e���� *0 messagesencouraging messagesEncouraging$ o      ���� 0 msg  " '��' Z  l�()����( l ls*����* =  ls+,+ o  lq���� 0 notifications  , m  qr��
�� boovtrue��  ��  ) Z  v�-./��- l v}0����0 =  v}121 o  v{���� 0 nsfw  2 m  {|��
�� boovtrue��  ��  . k  ��33 454 I ����67
�� .sysonotfnull��� ��� TEXT6 o  ������ 0 msg  7 ��8��
�� 
appr8 m  ��99 �:: 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  5 ;��; I ����<��
�� .sysodelanull��� ��� nmbr< m  ������ ��  ��  / =>= l ��?���? =  ��@A@ o  ���~�~ 0 nsfw  A m  ���}
�} boovfals��  �  > B�|B k  ��CC DED I ���{FG
�{ .sysonotfnull��� ��� TEXTF m  ��HH �II  B a c k i n g   u pG �zJ�y
�z 
apprJ m  ��KK �LL 
 W B T F U�y  E M�xM I ���wN�v
�w .sysodelanull��� ��� nmbrN m  ���u�u �v  �x  �|  ��  ��  ��  ��  ��  ��   OPO l ���t�s�r�t  �s  �r  P QRQ Q  ��STUS I ���qV�p
�q .sysoexecTEXT���     TEXTV b  ��WXW b  ��YZY b  ��[\[ b  ��]^] b  ��_`_ b  ��aba m  ��cc �dd
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = 'b o  ���o�o 0 c  ` m  ��ee �ff  '   '^ o  ���n�n 0 sourcefolder sourceFolder\ m  ��gg �hh  '   'Z o  ���m�m 0 d  X m  ��ii �jj  / '�p  T R      �l�k�j
�l .ascrerr ****      � ****�k  �j  U l ���i�h�g�i  �h  �g  R klk l ���f�e�d�f  �e  �d  l mnm r  ��opo m  ���c
�c boovfalsp o      �b�b 0 isbackingup isBackingUpn qrq r  ��sts m  ���a
�a boovfalst o      �`�` 0 manualbackup manualBackupr uvu l ���_�^�]�_  �^  �]  v w�\w Z  �\xy�[zx = �{|{ n  � }~} 2 � �Z
�Z 
cobj~ l ���Y�X c  ����� 4  ���W�
�W 
psxf� o  ���V�V 0 c  � m  ���U
�U 
alis�Y  �X  | J   �T�T  y k  R�� ��� r  ��� c  ��� n  
��� 4  
�S�
�S 
cfol� o  	�R�R 0 t  � o  �Q�Q 0 backupfolder backupFolder� m  
�P
�P 
TEXT� o      �O�O 0 oldestfolder oldestFolder� ��� r  ��� c  ��� n  ��� 1  �N
�N 
psxp� o  �M�M 0 oldestfolder oldestFolder� m  �L
�L 
TEXT� o      �K�K 0 oldestfolder oldestFolder� ��� l �J�I�H�J  �I  �H  � ��� r  $��� b  "��� b  ��� m  �� ���  r m   - r f   '� o  �G�G 0 oldestfolder oldestFolder� m  !�� ���  '� o      �F�F 0 todelete toDelete� ��� I %*�E��D
�E .sysoexecTEXT���     TEXT� o  %&�C�C 0 todelete toDelete�D  � ��� l ++�B�A�@�B  �A  �@  � ��?� Z  +R���>�=� l +6��<�;� E  +6��� o  +0�:�: &0 displaysleepstate displaySleepState� o  05�9�9 0 strawake strAwake�<  �;  � k  9N�� ��� r  9F��� n  9B��� 1  >B�8
�8 
time� l 9>��7�6� I 9>�5�4�3
�5 .misccurdldt    ��� null�4  �3  �7  �6  � o      �2�2 0 endtime endTime� ��� r  GN��� n GL��� I  HL�1�0�/�1 0 getduration getDuration�0  �/  �  f  GH� o      �.�. 0 duration  � ��� l OO�-�,�+�-  �,  �+  � ��� r  OZ��� n  OX��� 3  TX�*
�* 
cobj� o  OT�)�) $0 messagescomplete messagesComplete� o      �(�( 0 msg  � ��� Z  [H���'�&� l [b��%�$� =  [b��� o  [`�#�# 0 notifications  � m  `a�"
�" boovtrue�%  �$  � k  eD�� ��� Z  e���!�� l ej�� �� =  ej��� o  ef�� 0 duration  � m  fi�� ?�      �   �  � Z  m������ l mt���� =  mt��� o  mr�� 0 nsfw  � m  rs�
� boovtrue�  �  � k  w��� ��� I w����
� .sysonotfnull��� ��� TEXT� b  w���� b  w���� b  w|��� m  wz�� ���  A n d   i n  � o  z{�� 0 duration  � m  |�� ���    m i n u t e ! 
� o  ���� 0 msg  � ���
� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  � ��� I �����
� .sysodelanull��� ��� nmbr� m  ���� �  �  � ��� l ������ =  ����� o  ���� 0 nsfw  � m  ���
� boovfals�  �  � ��� k  ���� ��� I ���
��
�
 .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ���	�	 0 duration  � m  ��   �    m i n u t e ! 
� ��
� 
appr m  �� �  B a c k e d   u p !�  � � I ����
� .sysodelanull��� ��� nmbr m  ���� �  �  �  �  �!  � Z  �	� l ��
�� 
 =  �� o  ������ 0 nsfw   m  ����
�� boovtrue�  �    k  ��  I ����
�� .sysonotfnull��� ��� TEXT b  �� b  �� b  �� m  �� �  A n d   i n   o  ������ 0 duration   m  �� �    m i n u t e s ! 
 o  ������ 0 msg   ����
�� 
appr m  �� � : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��   �� I ���� ��
�� .sysodelanull��� ��� nmbr  m  ������ ��  ��  	 !"! l ��#����# =  ��$%$ o  ������ 0 nsfw  % m  ����
�� boovfals��  ��  " &��& k  �
'' ()( I ���*+
�� .sysonotfnull��� ��� TEXT* b  ��,-, b  ��./. m  ��00 �11  A n d   i n  / o  ������ 0 duration  - m  ��22 �33    m i n u t e s ! 
+ ��4��
�� 
appr4 m  � 55 �66  B a c k e d   u p !��  ) 7��7 I 
��8��
�� .sysodelanull��� ��� nmbr8 m  ���� ��  ��  ��  �  � 9:9 l ��������  ��  ��  : ;��; Z  D<=>��< l ?����? =  @A@ o  ���� 0 nsfw  A m  ��
�� boovtrue��  ��  = I &��BC
�� .sysonotfnull��� ��� TEXTB m  DD �EE � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .C ��F��
�� 
apprF m  "GG �HH @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  > IJI l )0K����K =  )0LML o  ).���� 0 nsfw  M m  ./��
�� boovfals��  ��  J N��N I 3@��OP
�� .sysonotfnull��� ��� TEXTO m  36QQ �RR p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e dP ��S��
�� 
apprS m  9<TT �UU 2 D e l e t e d   n e w   b a c k u p   f o l d e r��  ��  ��  ��  �'  �&  � VWV l II��������  ��  ��  W X��X n INYZY I  JN�������� $0 resetmenubaricon resetMenuBarIcon��  ��  Z  f  IJ��  �>  �=  �?  �[  z k  U\[[ \]\ Z  U@^_����^ l U``����` E  U`aba o  UZ���� &0 displaysleepstate displaySleepStateb o  Z_���� 0 strawake strAwake��  ��  _ k  c<cc ded r  cpfgf n  clhih 1  hl��
�� 
timei l chj����j I ch������
�� .misccurdldt    ��� null��  ��  ��  ��  g o      ���� 0 endtime endTimee klk r  qxmnm n qvopo I  rv�������� 0 getduration getDuration��  ��  p  f  qrn o      ���� 0 duration  l qrq r  y�sts n  y�uvu 3  ~���
�� 
cobjv o  y~���� $0 messagescomplete messagesCompletet o      ���� 0 msg  r w��w Z  �<xy����x l ��z����z =  ��{|{ o  ������ 0 notifications  | m  ����
�� boovtrue��  ��  y Z  �8}~��} l �������� =  ����� o  ������ 0 duration  � m  ���� ?�      ��  ��  ~ Z  �������� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovtrue��  ��  � k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e ! 
� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  � ��� l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovfals��  ��  � ���� k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e ! 
� �����
�� 
appr� m  ���� ���  B a c k e d   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��   Z  �8������ l �������� =  ����� o  ������ 0 nsfw  � m  ����
�� boovtrue��  ��  � k  ��� ��� I �����
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ������ 0 msg  � �����
�� 
appr� m  �� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I 	�����
�� .sysodelanull��� ��� nmbr� m  	
���� ��  ��  � ��� l ������ =  ��� o  ���� 0 nsfw  � m  ��
�� boovfals��  ��  � ���� k  4�� ��� I .���
� .sysonotfnull��� ��� TEXT� b  $��� b   ��� m  �� ���  A n d   i n  � o  �~�~ 0 duration  � m   #�� ���    m i n u t e s ! 
� �}��|
�} 
appr� m  '*�� ���  B a c k e d   u p !�|  � ��{� I /4�z��y
�z .sysodelanull��� ��� nmbr� m  /0�x�x �y  �{  ��  ��  ��  ��  ��  ��  ��  ] ��� l AA�w�v�u�w  �v  �u  � ��� n AF��� I  BF�t�s�r�t $0 resetmenubaricon resetMenuBarIcon�s  �r  �  f  AB� ��� l GG�q�p�o�q  �p  �o  � ��n� Z  G\���m�l� l GN��k�j� =  GN��� o  GL�i�i  0 backupthenquit backupThenQuit� m  LM�h
�h boovtrue�k  �j  � I QX�g��f
�g .aevtquitnull��� ��� null� m  QT��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �f  �m  �l  �n  �\  ��  �L  �M  � m   $ %���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l bb�e�d�c�e  �d  �c  � ��b� I  bj�a��`�a 0 	stopwatch  � ��_� m  cf�� ���  f i n i s h�_  �`  �b  � 	 		  l     �^�]�\�^  �]  �\  	 			 l     �[		�[  	 � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   	 �		 - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	 			 i  il			
		 I      �Z	�Y�Z 0 itexists itExists	 			 o      �X�X 0 
objecttype 
objectType	 	�W	 o      �V�V 
0 object  �W  �Y  	
 O     W			 Z    V			�U	 l   	�T�S	 =    			 o    �R�R 0 
objecttype 
objectType	 m    		 �		  d i s k�T  �S  	 Z   
 		�Q		 I  
 �P	�O
�P .coredoexnull���     ****	 4   
 �N	
�N 
cdis	 o    �M�M 
0 object  �O  	 L    		 m    �L
�L boovtrue�Q  	 L    		 m    �K
�K boovfals	 	 	!	  l   "	"�J�I	" =    "	#	$	# o     �H�H 0 
objecttype 
objectType	$ m     !	%	% �	&	&  f i l e�J  �I  	! 	'	(	' Z   % 7	)	*�G	+	) I  % -�F	,�E
�F .coredoexnull���     ****	, 4   % )�D	-
�D 
file	- o   ' (�C�C 
0 object  �E  	* L   0 2	.	. m   0 1�B
�B boovtrue�G  	+ L   5 7	/	/ m   5 6�A
�A boovfals	( 	0	1	0 l  : =	2�@�?	2 =   : =	3	4	3 o   : ;�>�> 0 
objecttype 
objectType	4 m   ; <	5	5 �	6	6  f o l d e r�@  �?  	1 	7�=	7 Z   @ R	8	9�<	:	8 I  @ H�;	;�:
�; .coredoexnull���     ****	; 4   @ D�9	<
�9 
cfol	< o   B C�8�8 
0 object  �:  	9 L   K M	=	= m   K L�7
�7 boovtrue�<  	: L   P R	>	> m   P Q�6
�6 boovfals�=  �U  	 m     	?	?�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  	 	@	A	@ l     �5�4�3�5  �4  �3  	A 	B	C	B i  mp	D	E	D I      �2�1�0�2 .0 getcomputeridentifier getComputerIdentifier�1  �0  	E k     	F	F 	G	H	G r     		I	J	I n     	K	L	K 1    �/
�/ 
sicn	L l    	M�.�-	M I    �,�+�*
�, .sysosigtsirr   ��� null�+  �*  �.  �-  	J o      �)�) 0 computername computerName	H 	N	O	N r   
 	P	Q	P I  
 �(	R�'
�( .sysoexecTEXT���     TEXT	R m   
 	S	S �	T	T � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �'  	Q o      �&�& "0 strserialnumber strSerialNumber	O 	U	V	U r    	W	X	W l   	Y�%�$	Y b    	Z	[	Z b    	\	]	\ o    �#�# 0 computername computerName	] m    	^	^ �	_	_    -  	[ o    �"�" "0 strserialnumber strSerialNumber�%  �$  	X o      �!�!  0 identifiername identifierName	V 	`� 	` L    	a	a o    ��  0 identifiername identifierName�   	C 	b	c	b l     ����  �  �  	c 	d	e	d i  qt	f	g	f I      �	h�� 0 gettimestamp getTimestamp	h 	i�	i o      �� 0 isfolder isFolder�  �  	g k    �	j	j 	k	l	k r     )	m	n	m l     	o��	o I     ���
� .misccurdldt    ��� null�  �  �  �  	n K    	p	p �	q	r
� 
year	q o    �� 0 y  	r �	s	t
� 
mnth	s o    �� 0 m  	t �	u	v
� 
day 	u o    �� 0 d  	v �	w�
� 
time	w o   	 
�
�
 0 t  �  	l 	x	y	x r   * 1	z	{	z c   * /	|	}	| l  * -	~�	�	~ c   * -		�	 o   * +�� 0 y  	� m   + ,�
� 
long�	  �  	} m   - .�
� 
TEXT	{ o      �� 0 ty tY	y 	�	�	� r   2 9	�	�	� c   2 7	�	�	� l  2 5	���	� c   2 5	�	�	� o   2 3�� 0 y  	� m   3 4� 
�  
long�  �  	� m   5 6��
�� 
TEXT	� o      ���� 0 ty tY	� 	�	�	� r   : A	�	�	� c   : ?	�	�	� l  : =	�����	� c   : =	�	�	� o   : ;���� 0 m  	� m   ; <��
�� 
long��  ��  	� m   = >��
�� 
TEXT	� o      ���� 0 tm tM	� 	�	�	� r   B I	�	�	� c   B G	�	�	� l  B E	�����	� c   B E	�	�	� o   B C���� 0 d  	� m   C D��
�� 
long��  ��  	� m   E F��
�� 
TEXT	� o      ���� 0 td tD	� 	�	�	� r   J Q	�	�	� c   J O	�	�	� l  J M	�����	� c   J M	�	�	� o   J K���� 0 t  	� m   K L��
�� 
long��  ��  	� m   M N��
�� 
TEXT	� o      ���� 0 tt tT	� 	�	�	� l  R R��������  ��  ��  	� 	�	�	� r   R [	�	�	� c   R Y	�	�	� l  R W	�����	� I  R W��	���
�� .corecnte****       ****	� o   R S���� 0 tm tM��  ��  ��  	� m   W X��
�� 
nmbr	� o      ���� 
0 tml tML	� 	�	�	� r   \ e	�	�	� c   \ c	�	�	� l  \ a	�����	� I  \ a��	���
�� .corecnte****       ****	� o   \ ]���� 0 tm tM��  ��  ��  	� m   a b��
�� 
nmbr	� o      ���� 
0 tdl tDL	� 	�	�	� l  f f��������  ��  ��  	� 	�	�	� Z   f u	�	�����	� l  f i	�����	� =   f i	�	�	� o   f g���� 
0 tml tML	� m   g h���� ��  ��  	� r   l q	�	�	� b   l o	�	�	� m   l m	�	� �	�	�  0	� o   m n���� 0 tm tM	� o      ���� 0 tm tM��  ��  	� 	�	�	� l  v v��������  ��  ��  	� 	�	�	� Z   v �	�	�����	� l  v y	�����	� =   v y	�	�	� o   v w���� 
0 tdl tDL	� m   w x���� ��  ��  	� r   | �	�	�	� b   | �	�	�	� m   | 	�	� �	�	�  0	� o    ����� 0 td tD	� o      ���� 0 td tD��  ��  	� 	�	�	� l  � ���������  ��  ��  	� 	�	�	� r   � �	�	�	� n   � �	�	�	� 1   � ���
�� 
tstr	� l  � �	�����	� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 timestr timeStr	� 	�	�	� r   � �	�	�	� I  � �	���	�	� z����
�� .sysooffslong    ��� null
�� misccura��  	� ��	�	�
�� 
psof	� m   � �	�	� �	�	�  :	� ��	���
�� 
psin	� o   � ����� 0 timestr timeStr��  	� o      ���� 0 pos  	� 	�	�	� r   � �	�	�	� c   � �	�	�	� n   � �	�	�	� 7  � ���	�	�
�� 
cha 	� m   � ����� 	� l  � �	�����	� \   � �	�	�	� o   � ����� 0 pos  	� m   � ����� ��  ��  	� o   � ����� 0 timestr timeStr	� m   � ���
�� 
TEXT	� o      ���� 0 h  	� 	�	�	� r   � �	�	�	� c   � �	�	�	� n   � �	�	�	� 7 � ���	�	�
�� 
cha 	� l  � �
 ����
  [   � �


 o   � ����� 0 pos  
 m   � ����� ��  ��  	�  ;   � �	� o   � ����� 0 timestr timeStr	� m   � ���
�� 
TEXT	� o      ���� 0 timestr timeStr	� 


 l  � ���������  ��  ��  
 


 r   � �


 I  � �
	��


	 z����
�� .sysooffslong    ��� null
�� misccura��  

 ��


�� 
psof
 m   � �

 �

  :
 ��
��
�� 
psin
 o   � ����� 0 timestr timeStr��  
 o      ���� 0 pos  
 


 r   �


 c   �


 n   � 


 7  � ��


�� 
cha 
 m   � ����� 
 l  � �
����
 \   � �


 o   � ����� 0 pos  
 m   � ����� ��  ��  
 o   � ����� 0 timestr timeStr
 m   ��
�� 
TEXT
 o      ���� 0 m  
 


 r  

 
 c  
!
"
! n  
#
$
# 7��
%
&
�� 
cha 
% l 
'����
' [  
(
)
( o  ���� 0 pos  
) m  ���� ��  ��  
&  ;  
$ o  ���� 0 timestr timeStr
" m  ��
�� 
TEXT
  o      ���� 0 timestr timeStr
 
*
+
* l ��������  ��  ��  
+ 
,
-
, r  2
.
/
. I 0
0��
1
0 z����
�� .sysooffslong    ��� null
�� misccura��  
1 �
2
3
� 
psof
2 m  "%
4
4 �
5
5   
3 �~
6�}
�~ 
psin
6 o  ()�|�| 0 timestr timeStr�}  
/ o      �{�{ 0 pos  
- 
7
8
7 r  3E
9
:
9 c  3C
;
<
; n  3A
=
>
= 74A�z
?
@
�z 
cha 
? l :>
A�y�x
A [  :>
B
C
B o  ;<�w�w 0 pos  
C m  <=�v�v �y  �x  
@  ;  ?@
> o  34�u�u 0 timestr timeStr
< m  AB�t
�t 
TEXT
: o      �s�s 0 s  
8 
D
E
D l FF�r�q�p�r  �q  �p  
E 
F
G
F Z  F
H
I
J�o
H l FI
K�n�m
K =  FI
L
M
L o  FG�l�l 0 isfolder isFolder
M m  GH�k
�k boovtrue�n  �m  
I r  La
N
O
N b  L_
P
Q
P b  L]
R
S
R b  L[
T
U
T b  LY
V
W
V b  LU
X
Y
X b  LS
Z
[
Z b  LQ
\
]
\ m  LO
^
^ �
_
_  b a c k u p _
] o  OP�j�j 0 ty tY
[ o  QR�i�i 0 tm tM
Y o  ST�h�h 0 td tD
W m  UX
`
` �
a
a  _
U o  YZ�g�g 0 h  
S o  [\�f�f 0 m  
Q o  ]^�e�e 0 s  
O o      �d�d 0 	timestamp  
J 
b
c
b l dg
d�c�b
d =  dg
e
f
e o  de�a�a 0 isfolder isFolder
f m  ef�`
�` boovfals�c  �b  
c 
g�_
g r  j{
h
i
h b  jy
j
k
j b  jw
l
m
l b  ju
n
o
n b  js
p
q
p b  jo
r
s
r b  jm
t
u
t o  jk�^�^ 0 ty tY
u o  kl�]�] 0 tm tM
s o  mn�\�\ 0 td tD
q m  or
v
v �
w
w  _
o o  st�[�[ 0 h  
m o  uv�Z�Z 0 m  
k o  wx�Y�Y 0 s  
i o      �X�X 0 	timestamp  �_  �o  
G 
x
y
x l ���W�V�U�W  �V  �U  
y 
z�T
z L  ��
{
{ o  ���S�S 0 	timestamp  �T  	e 
|
}
| l     �R�Q�P�R  �Q  �P  
} 
~

~ i  ux
�
�
� I      �O
��N�O 0 comparesizes compareSizes
� 
�
�
� o      �M�M 
0 source  
� 
��L
� o      �K�K 0 destination  �L  �N  
� k    �
�
� 
�
�
� r     
�
�
� m     �J
�J boovtrue
� o      �I�I 0 fit  
� 
�
�
� r    

�
�
� 4    �H
�
�H 
psxf
� o    �G�G 0 destination  
� o      �F�F 0 destination  
� 
�
�
� l   �E�D�C�E  �D  �C  
� 
�
�
� r    
�
�
� b    
�
�
� b    
�
�
� b    
�
�
� o    �B�B "0 destinationdisk destinationDisk
� m    
�
� �
�
�  B a c k u p s /
� o    �A�A 0 machinefolder machineFolder
� m    
�
� �
�
�  / L a t e s t
� o      �@�@ 0 
templatest 
tempLatest
� 
�
�
� r    
�
�
� m    �?�?  
� o      �>�> $0 latestfoldersize latestFolderSize
� 
�
�
� l   �=�<�;�=  �<  �;  
� 
�
�
� r    &
�
�
� b    $
�
�
� n   "
�
�
� 1     "�:
�: 
psxp
� l    
��9�8
� I    �7
�
�
�7 .earsffdralis        afdr
� m    �6
�6 afdrdlib
� �5
��4
�5 
from
� m    �3
�3 fldmfldu�4  �9  �8  
� m   " #
�
� �
�
�  C a c h e s
� o      �2�2 0 c  
� 
�
�
� r   ' 4
�
�
� b   ' 2
�
�
� n  ' 0
�
�
� 1   . 0�1
�1 
psxp
� l  ' .
��0�/
� I  ' .�.
�
�
�. .earsffdralis        afdr
� m   ' (�-
�- afdrdlib
� �,
��+
�, 
from
� m   ) *�*
�* fldmfldu�+  �0  �/  
� m   0 1
�
� �
�
�  L o g s
� o      �)�) 0 l  
� 
�
�
� r   5 B
�
�
� b   5 @
�
�
� n  5 >
�
�
� 1   < >�(
�( 
psxp
� l  5 <
��'�&
� I  5 <�%
�
�
�% .earsffdralis        afdr
� m   5 6�$
�$ afdrdlib
� �#
��"
�# 
from
� m   7 8�!
�! fldmfldu�"  �'  �&  
� m   > ?
�
� �
�
�   M o b i l e   D o c u m e n t s
� o      � �  0 md  
� 
�
�
� r   C F
�
�
� m   C D��  
� o      �� 0 	cachesize 	cacheSize
� 
�
�
� r   G J
�
�
� m   G H��  
� o      �� 0 logssize logsSize
� 
�
�
� r   K N
�
�
� m   K L��  
� o      �� 0 mdsize mdSize
� 
�
�
� l  O O����  �  �  
� 
�
�
� r   O R
�
�
� m   O P
�
� ?�      
� o      �� 
0 buffer  
� 
�
�
� l  S S����  �  �  
� 
�
�
� r   S V
�
�
� m   S T��  
� o      �� (0 adjustedfoldersize adjustedFolderSize
� 
�
�
� l  W W����  �  �  
� 
�
�
� O   Wx
�
�
� k   [w
�
� 
�
�
� r   [ h
�
�
� I  [ f�
��
� .sysoexecTEXT���     TEXT
� b   [ b
�
�
� b   [ ^
�
�
� m   [ \
�
� �
�
�  d u   - m s   '
� o   \ ]�� 
0 source  
� m   ^ a
�
� �
�
�  '   |   c u t   - f   1�  
� o      �
�
 0 
foldersize 
folderSize
� 
�
�
� r   i �
� 
� ^   i � l  i }�	� I  i }� z��
� .sysorondlong        doub
� misccura ]   o x l  o t�� ^   o t	
	 o   o p�� 0 
foldersize 
folderSize
 m   p s�� �  �   m   t w� �  d�  �	  �   m   } ����� d  o      ���� 0 
foldersize 
folderSize
�  l  � ���������  ��  ��    r   � � ^   � � ^   � � ^   � � l  � ����� l  � ����� n   � � 1   � ���
�� 
frsp 4   � ���
�� 
cdis o   � ����� 0 destination  ��  ��  ��  ��   m   � �����  m   � �����  m   � �����  o      ���� 0 	freespace 	freeSpace  r   � � ^   � � !  l  � �"����" I  � �#$��# z����
�� .sysorondlong        doub
�� misccura$ l  � �%����% ]   � �&'& o   � ����� 0 	freespace 	freeSpace' m   � ����� d��  ��  ��  ��  ��  ! m   � ����� d o      ���� 0 	freespace 	freeSpace ()( l  � ���������  ��  ��  ) *+* r   � �,-, I  � ���.��
�� .sysoexecTEXT���     TEXT. b   � �/0/ b   � �121 m   � �33 �44  d u   - m s   '2 o   � ����� 0 c  0 m   � �55 �66  '   |   c u t   - f   1��  - o      ���� 0 	cachesize 	cacheSize+ 787 r   � �9:9 ^   � �;<; l  � �=����= I  � �>?��> z����
�� .sysorondlong        doub
�� misccura? ]   � �@A@ l  � �B����B ^   � �CDC o   � ����� 0 	cachesize 	cacheSizeD m   � ����� ��  ��  A m   � ����� d��  ��  ��  < m   � ����� d: o      ���� 0 	cachesize 	cacheSize8 EFE l  � ���������  ��  ��  F GHG r   � �IJI I  � ���K��
�� .sysoexecTEXT���     TEXTK b   � �LML b   � �NON m   � �PP �QQ  d u   - m s   'O o   � ����� 0 l  M m   � �RR �SS  '   |   c u t   - f   1��  J o      ���� 0 logssize logsSizeH TUT r   �	VWV ^   �XYX l  �Z����Z I  �[\��[ z����
�� .sysorondlong        doub
�� misccura\ ]   � �]^] l  � �_����_ ^   � �`a` o   � ����� 0 logssize logsSizea m   � ����� ��  ��  ^ m   � ����� d��  ��  ��  Y m  ���� dW o      ���� 0 logssize logsSizeU bcb l 

��������  ��  ��  c ded r  
fgf I 
��h��
�� .sysoexecTEXT���     TEXTh b  
iji b  
klk m  
mm �nn  d u   - m s   'l o  ���� 0 md  j m  oo �pp  '   |   c u t   - f   1��  g o      ���� 0 mdsize mdSizee qrq r  4sts ^  2uvu l .w����w I .xy��x z����
�� .sysorondlong        doub
�� misccuray ]   )z{z l  %|����| ^   %}~} o   !���� 0 mdsize mdSize~ m  !$���� ��  ��  { m  %(���� d��  ��  ��  v m  .1���� dt o      ���� 0 mdsize mdSizer � l 55��������  ��  ��  � ��� r  5>��� l 5<������ \  5<��� \  5:��� \  58��� o  56���� 0 
foldersize 
folderSize� o  67���� 0 	cachesize 	cacheSize� o  89���� 0 logssize logsSize� o  :;���� 0 mdsize mdSize��  ��  � o      ���� (0 adjustedfoldersize adjustedFolderSize� ��� l ??��������  ��  ��  � ���� Z  ?w������� l ?F������ =  ?F��� o  ?D���� 0 initialbackup initialBackup� m  DE��
�� boovfals��  ��  � k  Is�� ��� r  IX��� I IV�����
�� .sysoexecTEXT���     TEXT� b  IR��� b  IN��� m  IL�� ���  d u   - m s   '� o  LM���� 0 
templatest 
tempLatest� m  NQ�� ���  '   |   c u t   - f   1��  � o      ���� $0 latestfoldersize latestFolderSize� ���� r  Ys��� ^  Yq��� l Ym������ I Ym����� z����
�� .sysorondlong        doub
�� misccura� ]  _h��� l _d������ ^  _d��� o  _`���� $0 latestfoldersize latestFolderSize� m  `c���� ��  ��  � m  dg���� d��  ��  ��  � m  mp���� d� o      ���� $0 latestfoldersize latestFolderSize��  ��  ��  ��  
� m   W X���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  
� ��� l yy��������  ��  ��  � ��� Z  y������� l y������� =  y���� o  y~���� 0 initialbackup initialBackup� m  ~�
� boovfals��  ��  � k  ���� ��� r  ����� \  ����� o  ���~�~ (0 adjustedfoldersize adjustedFolderSize� o  ���}�} $0 latestfoldersize latestFolderSize� o      �|�| 0 temp  � ��� l ���{�z�y�{  �z  �y  � ��� Z  �����x�w� l ����v�u� A  ����� o  ���t�t 0 temp  � m  ���s�s  �v  �u  � r  ����� \  ����� l ����r�q� o  ���p�p 0 temp  �r  �q  � l ����o�n� ]  ����� l ����m�l� o  ���k�k 0 temp  �m  �l  � m  ���j�j �o  �n  � o      �i�i 0 temp  �x  �w  � ��� l ���h�g�f�h  �g  �f  � ��e� Z  �����d�c� l ����b�a� ?  ����� o  ���`�` 0 temp  � l ����_�^� \  ����� o  ���]�] 0 	freespace 	freeSpace� o  ���\�\ 
0 buffer  �_  �^  �b  �a  � r  ����� m  ���[
�[ boovfals� o      �Z�Z 0 fit  �d  �c  �e  � ��� l ����Y�X� =  ����� o  ���W�W 0 initialbackup initialBackup� m  ���V
�V boovtrue�Y  �X  � ��U� Z  �����T�S� l ����R�Q� ?  ����� o  ���P�P (0 adjustedfoldersize adjustedFolderSize� l ����O�N� \  ����� o  ���M�M 0 	freespace 	freeSpace� o  ���L�L 
0 buffer  �O  �N  �R  �Q  � r  ����� m  ���K
�K boovfals� o      �J�J 0 fit  �T  �S  �U  ��  � ��� l ���I�H�G�I  �H  �G  � ��F� L  ���� o  ���E�E 0 fit  �F  
 ��� l     �D�C�B�D  �C  �B  � ��� i  y|��� I      �A��@�A 0 	stopwatch  � ��?� o      �>�> 0 mode  �?  �@  � k     #�� ��� q      �� �=�<�= 0 x  �<  �  �;  Z     #�: l    �9�8 =      o     �7�7 0 mode   m     � 
 s t a r t�9  �8   r    	
	 I    �6�5�6 0 gettimestamp getTimestamp �4 m    �3
�3 boovfals�4  �5  
 o      �2�2 0 x    l   �1�0 =     o    �/�/ 0 mode   m     �  f i n i s h�1  �0   �. r     I    �-�,�- 0 gettimestamp getTimestamp �+ m    �*
�* boovfals�+  �,   o      �)�) 0 x  �.  �:  �;  �  l     �(�'�&�(  �'  �&    i  }� I      �%�$�#�% 0 getduration getDuration�$  �#   k       !  r     "#" ^     $%$ l    &�"�!& \     '(' o     � �  0 endtime endTime( o    �� 0 	starttime 	startTime�"  �!  % m    �� <# o      �� 0 duration  ! )*) r    +,+ ^    -.- l   /��/ I   01�0 z��
� .sysorondlong        doub
� misccura1 l   2��2 ]    343 o    �� 0 duration  4 m    �� d�  �  �  �  �  . m    �� d, o      �� 0 duration  * 5�5 L    66 o    �� 0 duration  �   787 l     ����  �  �  8 9:9 i  ��;<; I      ���
� 0 setfocus setFocus�  �
  < O     
=>= I   	�	��
�	 .miscactvnull��� ��� null�  �  > m     ??                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  : @A@ l     ����  �  �  A BCB l     �DE�  D � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   E �FF - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -C GHG i  ��IJI I      �K�� $0 menuneedsupdate_ menuNeedsUpdate_K L� L l     M����M m      ��
�� 
cmnu��  ��  �   �  J n    NON I    �������� 0 	makemenus 	makeMenus��  ��  O  f     H PQP l     ��������  ��  ��  Q RSR i  ��TUT I      �������� 0 	makemenus 	makeMenus��  ��  U k    >VV WXW n    	YZY I    	��������  0 removeallitems removeAllItems��  ��  Z o     ���� 0 newmenu newMenuX [\[ l  
 
��������  ��  ��  \ ]��] Y   
>^��_`��^ k   9aa bcb r    'ded n    %fgf 4   " %��h
�� 
cobjh o   # $���� 0 i  g o    "���� 0 thelist theListe o      ���� 0 	this_item  c iji l  ( (��������  ��  ��  j klk Z   ( mno��m l  ( -p����p =   ( -qrq l  ( +s����s c   ( +tut o   ( )���� 0 	this_item  u m   ) *��
�� 
TEXT��  ��  r m   + ,vv �ww  B a c k u p   n o w��  ��  n r   0 @xyx l  0 >z����z n  0 >{|{ I   7 >��}���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_} ~~ o   7 8���� 0 	this_item   ��� m   8 9�� ���  b a c k u p H a n d l e r :� ���� m   9 :�� ���  ��  ��  | n  0 7��� I   3 7�������� 	0 alloc  ��  ��  � n  0 3��� o   1 3���� 0 
nsmenuitem 
NSMenuItem� m   0 1��
�� misccura��  ��  y o      ���� 0 thismenuitem thisMenuItemo ��� l  C H������ =   C H��� l  C F������ c   C F��� o   C D���� 0 	this_item  � m   D E��
�� 
TEXT��  ��  � m   F G�� ��� " B a c k u p   n o w   &   q u i t��  ��  � ��� r   K [��� l  K Y������ n  K Y��� I   R Y������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S���� 0 	this_item  � ��� m   S T�� ��� * b a c k u p A n d Q u i t H a n d l e r :� ���� m   T U�� ���  ��  ��  � n  K R��� I   N R�������� 	0 alloc  ��  ��  � n  K N��� o   L N���� 0 
nsmenuitem 
NSMenuItem� m   K L��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  ^ c������ =   ^ c��� l  ^ a������ c   ^ a��� o   ^ _���� 0 	this_item  � m   _ `��
�� 
TEXT��  ��  � m   a b�� ��� " T u r n   o n   t h e   b a n t s��  ��  � ��� r   f x��� l  f v������ n  f v��� I   m v������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n���� 0 	this_item  � ��� m   n o�� ���  n s f w O n H a n d l e r :� ���� m   o r�� ���  ��  ��  � n  f m��� I   i m�������� 	0 alloc  ��  ��  � n  f i��� o   g i���� 0 
nsmenuitem 
NSMenuItem� m   f g��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  { ������� =   { ���� l  { ~������ c   { ~��� o   { |���� 0 	this_item  � m   | }��
�� 
TEXT��  ��  � m   ~ ��� ��� $ T u r n   o f f   t h e   b a n t s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ���  n s f w O f f H a n d l e r :� ���� m   � ��� ���  ��  ��  � n  � ���� I   � ��������� 	0 alloc  ��  ��  � n  � ���� o   � ����� 0 
nsmenuitem 
NSMenuItem� m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  � ������� =   � ���� l  � ������� c   � ���� o   � ����� 0 	this_item  � m   � ���
�� 
TEXT��  ��  � m   � ��� ��� * T u r n   o n   n o t i f i c a t i o n s��  ��  � ��� r   � ���� l  � ������� n  � ���� I   � �������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   � ����� 0 	this_item  � ��� m   � ��� ��� , n o t i f i c a t i o n O n H a n d l e r :�  ��  m   � � �  ��  ��  � n  � � I   � ��������� 	0 alloc  ��  ��   n  � � o   � ����� 0 
nsmenuitem 
NSMenuItem m   � ���
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem�  l  � �	����	 =   � �

 l  � ����� c   � � o   � ����� 0 	this_item   m   � ���
�� 
TEXT��  ��   m   � � � , T u r n   o f f   n o t i f i c a t i o n s��  ��    r   � � l  � ����� n  � � I   � ���~� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   � ��}�} 0 	this_item    m   � � � . n o t i f i c a t i o n O f f H a n d l e r : �| m   � �   �!!  �|  �~   n  � �"#" I   � ��{�z�y�{ 	0 alloc  �z  �y  # n  � �$%$ o   � ��x�x 0 
nsmenuitem 
NSMenuItem% m   � ��w
�w misccura��  ��   o      �v�v 0 thismenuitem thisMenuItem &'& l  � �(�u�t( =   � �)*) l  � �+�s�r+ c   � �,-, o   � ��q�q 0 	this_item  - m   � ��p
�p 
TEXT�s  �r  * m   � �.. �//  Q u i t�u  �t  ' 0�o0 r   � �121 l  � �3�n�m3 n  � �454 I   � ��l6�k�l J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_6 787 o   � ��j�j 0 	this_item  8 9:9 m   � �;; �<<  q u i t H a n d l e r :: =�i= m   � �>> �??  �i  �k  5 n  � �@A@ I   � ��h�g�f�h 	0 alloc  �g  �f  A n  � �BCB o   � ��e�e 0 
nsmenuitem 
NSMenuItemC m   � ��d
�d misccura�n  �m  2 o      �c�c 0 thismenuitem thisMenuItem�o  ��  l DED l �b�a�`�b  �a  �`  E FGF l H�_�^H n IJI I  �]K�\�] 0 additem_ addItem_K L�[L o  �Z�Z 0 thismenuitem thisMenuItem�[  �\  J o  �Y�Y 0 newmenu newMenu�_  �^  G MNM l �X�W�V�X  �W  �V  N OPO l Q�U�TQ n RSR I  �ST�R�S 0 
settarget_ 
setTarget_T U�QU  f  �Q  �R  S o  �P�P 0 thismenuitem thisMenuItem�U  �T  P VWV l �O�N�M�O  �N  �M  W X�LX Z  9YZ�K�JY G  "[\[ l ]�I�H] =  ^_^ o  �G�G 0 i  _ m  �F�F �I  �H  \ l `�E�D` =  aba o  �C�C 0 i  b m  �B�B �E  �D  Z l %5c�A�@c n %5ded I  *5�?f�>�? 0 additem_ addItem_f g�=g l *1h�<�;h n *1iji o  -1�:�: 0 separatoritem separatorItemj n *-klk o  +-�9�9 0 
nsmenuitem 
NSMenuIteml m  *+�8
�8 misccura�<  �;  �=  �>  e o  %*�7�7 0 newmenu newMenu�A  �@  �K  �J  �L  �� 0 i  _ m    �6�6 ` n    mnm m    �5
�5 
nmbrn n   opo 2   �4
�4 
cobjp o    �3�3 0 thelist theList��  ��  S qrq l     �2�1�0�2  �1  �0  r sts i  ��uvu I      �/w�.�/  0 backuphandler_ backupHandler_w x�-x o      �,�, 
0 sender  �-  �.  v Z     �yz{�+y l    |�*�)| =     }~} o     �(�( 0 isbackingup isBackingUp~ m    �'
�' boovfals�*  �)  z k   
 U ��� r   
 ��� m   
 �&
�& boovtrue� o      �%�% 0 manualbackup manualBackup� ��$� Z    U���#�"� l   ��!� � =    ��� o    �� 0 notifications  � m    �
� boovtrue�!  �   � Z    Q����� l   #���� =    #��� o    !�� 0 nsfw  � m   ! "�
� boovtrue�  �  � k   & 3�� ��� I  & -���
� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� ���
� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�  � ��� I  . 3���
� .sysodelanull��� ��� nmbr� m   . /�� �  �  � ��� l  6 =���� =   6 =��� o   6 ;�� 0 nsfw  � m   ; <�
� boovfals�  �  � ��� k   @ M�� ��� I  @ G���
� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� ���

� 
appr� m   B C�� ��� 
 W B T F U�
  � ��	� I  H M���
� .sysodelanull��� ��� nmbr� m   H I�� �  �	  �  �  �#  �"  �$  { ��� l  X _���� =   X _��� o   X ]�� 0 isbackingup isBackingUp� m   ] ^�
� boovtrue�  �  � ��� k   b ��� ��� r   b k��� n   b i��� 3   g i� 
�  
cobj� o   b g���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   l �������� l  l s������ =   l s��� o   l q���� 0 notifications  � m   q r��
�� boovtrue��  ��  � Z   v ������� l  v }������ =   v }��� o   v {���� 0 nsfw  � m   { |��
�� boovtrue��  ��  � I  � �����
�� .sysonotfnull��� ��� TEXT� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  ��  ��  ��  ��  �  �+  t ��� l     ��������  ��  ��  � ��� i  ����� I      ������� .0 backupandquithandler_ backupAndQuitHandler_� ���� o      ���� 
0 sender  ��  ��  � Z    ������ l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � k   
 ��� ��� Z   
 +������ l  
 ������ =   
 ��� o   
 ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      ���� 0 	temptitle 	tempTitle� ��� l   !������ =    !��� o    ���� 0 nsfw  � m     ��
�� boovfals��  ��  � ���� r   $ '��� m   $ %�� ��� 
 W B T F U� o      ���� 0 	temptitle 	tempTitle��  ��  � ��� l  , ,��������  ��  ��  �    r   , @ l  , >���� n   , > 1   < >��
�� 
bhit l  , <���� I  , <��	
�� .sysodlogaskr        TEXT m   , -

 � T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?	 ��
�� 
appr o   . /���� 0 	temptitle 	tempTitle ��
�� 
disp o   0 1���� 0 appicon appIcon ��
�� 
btns J   2 6  m   2 3 �  Y e s �� m   3 4 �  N o��   ����
�� 
dflt m   7 8���� ��  ��  ��  ��  ��   o      ���� 	0 reply    l  A A��������  ��  ��   �� Z   A � �� l  A D!����! =   A D"#" o   A B���� 	0 reply  # m   B C$$ �%%  Y e s��  ��   k   G �&& '(' r   G N)*) m   G H��
�� boovtrue* o      ����  0 backupthenquit backupThenQuit( +,+ r   O V-.- m   O P��
�� boovtrue. o      ���� 0 manualbackup manualBackup, /0/ l  W W��������  ��  ��  0 1��1 Z   W �23����2 l  W ^4����4 =   W ^565 o   W \���� 0 notifications  6 m   \ ]��
�� boovtrue��  ��  3 Z   a �789��7 l  a h:����: =   a h;<; o   a f���� 0 nsfw  < m   f g��
�� boovtrue��  ��  8 k   k x== >?> I  k r��@A
�� .sysonotfnull��� ��� TEXT@ m   k lBB �CC T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u pA ��D��
�� 
apprD m   m nEE �FF 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  ? G��G I  s x��H��
�� .sysodelanull��� ��� nmbrH m   s t���� ��  ��  9 IJI l  { �K����K =   { �LML o   { ����� 0 nsfw  M m   � ���
�� boovfals��  ��  J N��N k   � �OO PQP I  � ���RS
�� .sysonotfnull��� ��� TEXTR m   � �TT �UU   P r e p a r i n g   b a c k u pS ��V��
�� 
apprV m   � �WW �XX 
 W B T F U��  Q Y��Y I  � ���Z��
�� .sysodelanull��� ��� nmbrZ m   � ����� ��  ��  ��  ��  ��  ��  ��    [\[ l  � �]����] =   � �^_^ o   � ����� 	0 reply  _ m   � �`` �aa  N o��  ��  \ b��b r   � �cdc m   � ���
�� boovfalsd o      ����  0 backupthenquit backupThenQuit��  ��  ��  � efe l  � �g����g =   � �hih o   � ����� 0 isbackingup isBackingUpi m   � ���
�� boovtrue��  ��  f j��j k   �
kk lml r   � �non n   � �pqp 3   � ���
�� 
cobjq o   � ����� 40 messagesalreadybackingup messagesAlreadyBackingUpo o      �� 0 msg  m r�~r Z   �
st�}�|s l  � �u�{�zu =   � �vwv o   � ��y�y 0 notifications  w m   � ��x
�x boovtrue�{  �z  t Z   �xyz�wx l  � �{�v�u{ =   � �|}| o   � ��t�t 0 nsfw  } m   � ��s
�s boovtrue�v  �u  y I  � ��r~
�r .sysonotfnull��� ��� TEXT~ o   � ��q�q 0 msg   �p��o
�p 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�o  z ��� l  � ���n�m� =   � ���� o   � ��l�l 0 nsfw  � m   � ��k
�k boovfals�n  �m  � ��j� I  ��i��
�i .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �h��g
�h 
appr� m   � ��� ��� 
 W B T F U�g  �j  �w  �}  �|  �~  ��  ��  � ��� l     �f�e�d�f  �e  �d  � ��� i  ����� I      �c��b�c  0 nsfwonhandler_ nsfwOnHandler_� ��a� o      �`�` 
0 sender  �a  �b  � k     =�� ��� r     ��� m     �_
�_ boovtrue� o      �^�^ 0 nsfw  � ��]� Z    =����\� l   ��[�Z� =    ��� o    �Y�Y 0 notifications  � m    �X
�X boovtrue�[  �Z  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��W� m    �� ���  Q u i t�W  � o      �V�V 0 thelist theList� ��� l  " )��U�T� =   " )��� o   " '�S�S 0 notifications  � m   ' (�R
�R boovfals�U  �T  � ��Q� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� $ T u r n   o f f   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��P� m   0 1�� ���  Q u i t�P  � o      �O�O 0 thelist theList�Q  �\  �]  � ��� l     �N�M�L�N  �M  �L  � ��� i  ����� I      �K��J�K "0 nsfwoffhandler_ nsfwOffHandler_� ��I� o      �H�H 
0 sender  �I  �J  � k     =�� ��� r     ��� m     �G
�G boovfals� o      �F�F 0 nsfw  � ��E� Z    =����D� l   ��C�B� =    ��� o    �A�A 0 notifications  � m    �@
�@ boovtrue�C  �B  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� " T u r n   o n   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��?� m    �� ���  Q u i t�?  � o      �>�> 0 thelist theList�    l  " )�=�< =   " ) o   " '�;�; 0 notifications   m   ' (�:
�: boovfals�=  �<   �9 r   , 9 J   , 3 	
	 m   , - �  B a c k u p   n o w
  m   - . � " B a c k u p   n o w   &   q u i t  m   . / � " T u r n   o n   t h e   b a n t s  m   / 0 � * T u r n   o n   n o t i f i c a t i o n s �8 m   0 1 �  Q u i t�8   o      �7�7 0 thelist theList�9  �D  �E  �  l     �6�5�4�6  �5  �4    i  �� !  I      �3"�2�3 00 notificationonhandler_ notificationOnHandler_" #�1# o      �0�0 
0 sender  �1  �2  ! k     =$$ %&% r     '(' m     �/
�/ boovtrue( o      �.�. 0 notifications  & )�-) Z    =*+,�,* l   -�+�*- =    ./. o    �)�) 0 nsfw  / m    �(
�( boovtrue�+  �*  + r    010 J    22 343 m    55 �66  B a c k u p   n o w4 787 m    99 �:: " B a c k u p   n o w   &   q u i t8 ;<; m    == �>> $ T u r n   o f f   t h e   b a n t s< ?@? m    AA �BB , T u r n   o f f   n o t i f i c a t i o n s@ C�'C m    DD �EE  Q u i t�'  1 o      �&�& 0 thelist theList, FGF l  " )H�%�$H =   " )IJI o   " '�#�# 0 nsfw  J m   ' (�"
�" boovfals�%  �$  G K�!K r   , 9LML J   , 3NN OPO m   , -QQ �RR  B a c k u p   n o wP STS m   - .UU �VV " B a c k u p   n o w   &   q u i tT WXW m   . /YY �ZZ " T u r n   o n   t h e   b a n t sX [\[ m   / 0]] �^^ , T u r n   o f f   n o t i f i c a t i o n s\ _� _ m   0 1`` �aa  Q u i t�   M o      �� 0 thelist theList�!  �,  �-   bcb l     ����  �  �  c ded i  ��fgf I      �h�� 20 notificationoffhandler_ notificationOffHandler_h i�i o      �� 
0 sender  �  �  g k     =jj klk r     mnm m     �
� boovfalsn o      �� 0 notifications  l o�o Z    =pqr�p l   s��s =    tut o    �� 0 nsfw  u m    �
� boovtrue�  �  q r    vwv J    xx yzy m    {{ �||  B a c k u p   n o wz }~} m     ��� " B a c k u p   n o w   &   q u i t~ ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ��� m    �� ���  Q u i t�  w o      �� 0 thelist theListr ��� l  " )���� =   " )��� o   " '�� 0 nsfw  � m   ' (�

�
 boovfals�  �  � ��	� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��� m   0 1�� ���  Q u i t�  � o      �� 0 thelist theList�	  �  �  e ��� l     ����  �  �  � ��� i  ����� I      ���� 0 quithandler_ quitHandler_� ��� o      � �  
0 sender  �  �  � Z     ������� l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovtrue��  ��  � k   
 V�� ��� n  
 ��� I    �������� 0 setfocus setFocus��  ��  �  f   
 � ��� l   ��������  ��  ��  � ��� Z    1������ l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� m    �� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      ���� 0 	temptitle 	tempTitle� ��� l    '������ =     '��� o     %���� 0 nsfw  � m   % &��
�� boovfals��  ��  � ���� r   * -��� m   * +�� ��� 
 W B T F U� o      ���� 0 	temptitle 	tempTitle��  ��  � ��� l  2 2��������  ��  ��  � ��� r   2 F��� l  2 D������ n   2 D��� 1   B D��
�� 
bhit� l  2 B������ I  2 B����
�� .sysodlogaskr        TEXT� m   2 3�� ��� � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?� ����
�� 
appr� o   4 5���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   6 7���� 0 appicon appIcon� ����
�� 
btns� J   8 <�� ��� m   8 9�� ���  C a n c e l   &   Q u i t� ���� m   9 :�� ���  R e s u m e��  � �����
�� 
dflt� m   = >���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ���� Z   G V������ l  G J������ =   G J��� o   G H���� 	0 reply  � m   H I�� ���  C a n c e l   &   Q u i t��  ��  � I  M R�����
�� .aevtquitnull��� ��� null� m   M N��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  � l  U U��������  ��  ��  ��  � ��� l  Y `������ =   Y `��� o   Y ^���� 0 isbackingup isBackingUp� m   ^ _��
�� boovfals��  ��  �  ��  k   c �  n  c h I   d h�������� 0 setfocus setFocus��  ��    f   c d  l  i i��������  ��  ��   	 Z   i �
��
 l  i p���� =   i p o   i n���� 0 nsfw   m   n o��
�� boovtrue��  ��   r   s x m   s v � , W o a h ,   B a c k   T h e   F u c k   U p o      ���� 0 	temptitle 	tempTitle  l  { ����� =   { � o   { ����� 0 nsfw   m   � ���
�� boovfals��  ��   �� r   � � m   � � � 
 W B T F U o      ���� 0 	temptitle 	tempTitle��  ��  	  l  � ���������  ��  ��    !  r   � �"#" l  � �$����$ n   � �%&% 1   � ���
�� 
bhit& l  � �'����' I  � ���()
�� .sysodlogaskr        TEXT( m   � �** �++ < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?) ��,-
�� 
appr, o   � ����� 0 	temptitle 	tempTitle- ��./
�� 
disp. o   � ����� 0 appicon appIcon/ ��01
�� 
btns0 J   � �22 343 m   � �55 �66  Q u i t4 7��7 m   � �88 �99  R e s u m e��  1 ��:��
�� 
dflt: m   � ����� ��  ��  ��  ��  ��  # o      ���� 	0 reply  ! ;��; Z   � �<=��>< l  � �?����? =   � �@A@ o   � ����� 	0 reply  A m   � �BB �CC  Q u i t��  ��  = I  � ���D��
�� .aevtquitnull��� ��� nullD m   � �EE                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  > l  � ���������  ��  ��  ��  ��  ��  � FGF l     ��������  ��  ��  G HIH i  ��JKJ I      �������� (0 animatemenubaricon animateMenuBarIcon��  ��  K k     .LL MNM r     OPO 4     ��Q
�� 
alisQ l   R����R b    STS l   	U����U I   	��VW
�� .earsffdralis        afdrV  f    W ��X��
�� 
rtypX m    ��
�� 
ctxt��  ��  ��  T m   	 
YY �ZZ J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g��  ��  P o      ���� 0 	imagepath 	imagePathN [\[ r    ]^] n    _`_ 1    ��
�� 
psxp` o    ���� 0 	imagepath 	imagePath^ o      ���� 0 	imagepath 	imagePath\ aba r    #cdc n   !efe I    !��g���� 20 initwithcontentsoffile_ initWithContentsOfFile_g h��h o    ���� 0 	imagepath 	imagePath��  ��  f n   iji I    ����~�� 	0 alloc  �  �~  j n   klk o    �}�} 0 nsimage NSImagel m    �|
�| misccurad o      �{�{ 	0 image  b m�zm n  $ .non I   ) .�yp�x�y 0 	setimage_ 	setImage_p q�wq o   ) *�v�v 	0 image  �w  �x  o o   $ )�u�u 0 
statusitem 
StatusItem�z  I rsr l     �t�s�r�t  �s  �r  s tut i  ��vwv I      �q�p�o�q $0 resetmenubaricon resetMenuBarIcon�p  �o  w k     .xx yzy r     {|{ 4     �n}
�n 
alis} l   ~�m�l~ b    � l   	��k�j� I   	�i��
�i .earsffdralis        afdr�  f    � �h��g
�h 
rtyp� m    �f
�f 
ctxt�g  �k  �j  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�m  �l  | o      �e�e 0 	imagepath 	imagePathz ��� r    ��� n    ��� 1    �d
�d 
psxp� o    �c�c 0 	imagepath 	imagePath� o      �b�b 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�a��`�a 20 initwithcontentsoffile_ initWithContentsOfFile_� ��_� o    �^�^ 0 	imagepath 	imagePath�_  �`  � n   ��� I    �]�\�[�] 	0 alloc  �\  �[  � n   ��� o    �Z�Z 0 nsimage NSImage� m    �Y
�Y misccura� o      �X�X 	0 image  � ��W� n  $ .��� I   ) .�V��U�V 0 	setimage_ 	setImage_� ��T� o   ) *�S�S 	0 image  �T  �U  � o   $ )�R�R 0 
statusitem 
StatusItem�W  u ��� l     �Q�P�O�Q  �P  �O  � ��� i  ����� I      �N�M�L�N 0 makestatusbar makeStatusBar�M  �L  � k     r�� ��� r     ��� n    ��� o    �K�K "0 systemstatusbar systemStatusBar� n    ��� o    �J�J 0 nsstatusbar NSStatusBar� m     �I
�I misccura� o      �H�H 0 bar  � ��� l   �G�F�E�G  �F  �E  � ��� r    ��� n   ��� I   	 �D��C�D .0 statusitemwithlength_ statusItemWithLength_� ��B� m   	 
�� ��      �B  �C  � o    	�A�A 0 bar  � o      �@�@ 0 
statusitem 
StatusItem� ��� l   �?�>�=�?  �>  �=  � ��� r    #��� 4    !�<�
�< 
alis� l    ��;�:� b     ��� l   ��9�8� I   �7��
�7 .earsffdralis        afdr�  f    � �6��5
�6 
rtyp� m    �4
�4 
ctxt�5  �9  �8  � m    �� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�;  �:  � o      �3�3 0 	imagepath 	imagePath� ��� r   $ )��� n   $ '��� 1   % '�2
�2 
psxp� o   $ %�1�1 0 	imagepath 	imagePath� o      �0�0 0 	imagepath 	imagePath� ��� r   * 8��� n  * 6��� I   1 6�/��.�/ 20 initwithcontentsoffile_ initWithContentsOfFile_� ��-� o   1 2�,�, 0 	imagepath 	imagePath�-  �.  � n  * 1��� I   - 1�+�*�)�+ 	0 alloc  �*  �)  � n  * -��� o   + -�(�( 0 nsimage NSImage� m   * +�'
�' misccura� o      �&�& 	0 image  � ��� n  9 C��� I   > C�%��$�% 0 	setimage_ 	setImage_� ��#� o   > ?�"�" 	0 image  �#  �$  � o   9 >�!�! 0 
statusitem 
StatusItem� ��� l  D D� ���   �  �  � ��� r   D X��� n  D R��� I   K R����  0 initwithtitle_ initWithTitle_� ��� m   K N�� ���  C u s t o m�  �  � n  D K��� I   G K���� 	0 alloc  �  �  � n  D G��� o   E G�� 0 nsmenu NSMenu� m   D E�
� misccura� o      �� 0 newmenu newMenu� ��� l  Y Y����  �  �  � ��� n  Y c��� I   ^ c���� 0 setdelegate_ setDelegate_� ���  f   ^ _�  �  � o   Y ^�� 0 newmenu newMenu� ��� l  d d����  �  �  � ��
� n  d r��� I   i r�	���	 0 setmenu_ setMenu_� ��� o   i n�� 0 newmenu newMenu�  �  � o   d i�� 0 
statusitem 
StatusItem�
  � � � l     ����  �  �     l     ��   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  i  ��	 I      � �����  0 runonce runOnce��  ��  	 k     

  I     �������� (0 animatemenubaricon animateMenuBarIcon��  ��   �� I    �������� 0 	plistinit 	plistInit��  ��  ��    l     ��������  ��  ��    l   ���� n    I    �������� 0 makestatusbar makeStatusBar��  ��    f    ��  ��    l   ���� I    �������� 0 runonce runOnce��  ��  ��  ��    l     ��������  ��  ��   �� i  �� I     ������
�� .miscidlenmbr    ��� null��  ��   k     �  Z     � !����  l    "����" E     #$# o     ���� &0 displaysleepstate displaySleepState$ o    
���� 0 strawake strAwake��  ��  ! Z    }%&����% l   '����' =    ()( o    ���� 0 isbackingup isBackingUp) m    ��
�� boovfals��  ��  & Z    y*+,��* l   -����- =    ./. o    ���� 0 manualbackup manualBackup/ m    ��
�� boovfals��  ��  + k   " c00 121 r   " +343 l  " )5����5 n   " )676 1   ' )��
�� 
min 7 l  " '8����8 l  " '9����9 I  " '������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  ��  ��  4 o      ���� 0 m  2 :;: l  , ,��������  ��  ��  ; <��< Z   , c=>?��= G   , 7@A@ l  , /B����B =   , /CDC o   , -���� 0 m  D m   - .���� ��  ��  A l  2 5E����E =   2 5FGF o   2 3���� 0 m  G m   3 4���� -��  ��  > Z   : IHI����H l  : =J����J =   : =KLK o   : ;���� 0 scheduledtime scheduledTimeL m   ; <���� ��  ��  I I   @ E�������� 0 	plistinit 	plistInit��  ��  ��  ��  ? MNM G   L WOPO l  L OQ����Q =   L ORSR o   L M���� 0 m  S m   M N����  ��  ��  P l  R UT����T =   R UUVU o   R S���� 0 m  V m   S T���� ��  ��  N W��W I   Z _�������� 0 	plistinit 	plistInit��  ��  ��  ��  ��  , XYX l  f mZ����Z =   f m[\[ o   f k���� 0 manualbackup manualBackup\ m   k l��
�� boovtrue��  ��  Y ]��] I   p u�������� 0 	plistinit 	plistInit��  ��  ��  ��  ��  ��  ��  ��   ^_^ l  � ���������  ��  ��  _ `��` L   � �aa m   � ����� ��  ��       5��bc�� Ldefgh������i������jklmn��opqrstuvwxyz{|}~��������������  b 3������������������~�}�|�{�z�y�x�w�v�u�t�s�r�q�p�o�n�m�l�k�j�i�h�g�f�e�d�c�b�a�`�_�^�]
� 
pimr� 0 
statusitem 
StatusItem� 0 
thedisplay 
theDisplay� 0 defaults  � $0 internalmenuitem internalMenuItem� $0 externalmenuitem externalMenuItem� 0 newmenu newMenu� 0 thelist theList� 0 nsfw  � 0 notifications  �  0 backupthenquit backupThenQuit� 	0 plist  � 0 initialbackup initialBackup� 0 manualbackup manualBackup� 0 isbackingup isBackingUp� "0 messagesmissing messagesMissing� *0 messagesencouraging messagesEncouraging�~ $0 messagescomplete messagesComplete�} &0 messagescancelled messagesCancelled�| 40 messagesalreadybackingup messagesAlreadyBackingUp�{ 0 strawake strAwake�z 0 strsleep strSleep�y &0 displaysleepstate displaySleepState�x 0 	plistinit 	plistInit�w 0 diskinit diskInit�v 0 freespaceinit freeSpaceInit�u 0 
backupinit 
backupInit�t 0 tidyup tidyUp�s 
0 backup  �r 0 itexists itExists�q .0 getcomputeridentifier getComputerIdentifier�p 0 gettimestamp getTimestamp�o 0 comparesizes compareSizes�n 0 	stopwatch  �m 0 getduration getDuration�l 0 setfocus setFocus�k $0 menuneedsupdate_ menuNeedsUpdate_�j 0 	makemenus 	makeMenus�i  0 backuphandler_ backupHandler_�h .0 backupandquithandler_ backupAndQuitHandler_�g  0 nsfwonhandler_ nsfwOnHandler_�f "0 nsfwoffhandler_ nsfwOffHandler_�e 00 notificationonhandler_ notificationOnHandler_�d 20 notificationoffhandler_ notificationOffHandler_�c 0 quithandler_ quitHandler_�b (0 animatemenubaricon animateMenuBarIcon�a $0 resetmenubaricon resetMenuBarIcon�` 0 makestatusbar makeStatusBar�_ 0 runonce runOnce
�^ .miscidlenmbr    ��� null
�] .aevtoappnull  �   � ****c �\��\ �  ����� �[ 1�Z
�[ 
vers�Z  � �Y��X
�Y 
cobj� ��   �W
�W 
osax�X  � �V��U
�V 
cobj� ��   �T :
�T 
frmk�U  � �S��R
�S 
cobj� ��   �Q @
�Q 
frmk�R  
�� 
msngd �� �P�O�
�P misccura
�O 
pcls� ���  N S U s e r D e f a u l t se �� �N�M�
�N misccura
�M 
pcls� ���  N S M e n u I t e mf �� �L�K�
�L misccura
�K 
pcls� ���  N S M e n u I t e mg �� �J�I�
�J misccura
�I 
pcls� ���  N S M e n uh �H��H �   l p t x {
�� boovfals
�� boovtrue
�� boovfalsi ��� � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�� boovtrue
�� boovfals
�� boovfalsj �G��G �   � � � � �k �F��F 
� 
  � � � � � � � l �E��E 
� 
 #'+/2m �D��D 	� 	 :>BFJNRVYn �C��C 	� 	 aeimquy}�o ����             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 3 0 6 1 8 9 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 2 1 , " I d l e T i m e r E l a p s e d T i m e " = 7 3 9 3 4 2 , " C u r r e n t P o w e r S t a t e " = 4 }p �B��A�@���?�B 0 	plistinit 	plistInit�A  �@  � �>�=�<�;�:�9�8�7�6�5�4�> 0 
foldername 
folderName�= 0 
backupdisk 
backupDisk�<  0 computerfolder computerFolder�; 0 
backuptime 
backupTime�: 0 thecontents theContents�9 0 thevalue theValue�8 0 welcome  �7 0 	temptitle 	tempTitle�6 	0 reply  �5 0 backuptimes backupTimes�4 0 selectedtime selectedTime� A��3��2�1�0�/�.�-�,�+��*�)�
�(�'�&�%*�$�#�"�!� ��C����F��^ber�}�������������������
�	���3 0 itexists itExists
�2 
plif
�1 
pcnt
�0 
valL�/  0 foldertobackup FolderToBackup�. 0 backupdrive BackupDrive�-  0 computerfolder computerFolder�, 0 scheduledtime scheduledTime�+ .0 getcomputeridentifier getComputerIdentifier�*  ���) 0 setfocus setFocus
�( 
appr
�' 
disp�& 0 appicon appIcon
�% 
btns
�$ 
dflt�# 
�" .sysodlogaskr        TEXT
�! 
bhit
�  afdrcusr
� .earsffdralis        afdr
� 
prmp
� .sysostflalis    ��� null
� 
TEXT�  �  
� .aevtquitnull��� ��� null
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
� .corecrel****      � null
� 
plii
� 
insh
� 
kind� �
 0 sourcefolder sourceFolder�	 "0 destinationdisk destinationDisk� 0 machinefolder machineFolder� 0 diskinit diskInit�?jE�O*�b  l+ e  4� ,*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�UY�*j+ 
E�O� ��n)j+ O�E�Ob  e  �E�Y b  f  
a E�Y hO�a �a _ a a kva ka  a ,E�Oa j E�O *a a l a &E�W X   !a "j #O�a $,E�O�a $,E�Oa %a &a 'mvE�O�a a (l )kva &E�O�a *  
a +E�Y #�a ,  
a -E�Y �a .  
a /E�Y hoUO� �*a 0�a 1a 2b  la 3 4 �*a 0a 5a 6*6a 1a 7a a 2a 8�a 9a 9 4O*a 0a 5a 6*6a 1a 7a a 2a :�a 9a 9 4O*a 0a 5a 6*6a 1a 7a a 2a ;�a 9a 9 4O*a 0a 5a 6*6a 1a 7a a 2a <�a 9a 9 4UUO�E` =O�E` >O�E` ?O�E�O*j+ @q � ������ 0 diskinit diskInit�  �  � ��� � 0 msg  � 0 	temptitle 	tempTitle�  	0 reply  � 
����������&0��������GJ��������T��q��{~�� "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit�� 0 setfocus setFocus
�� 
cobj
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit
�� .sysonotfnull��� ��� TEXT� �*��l+ e  *fk+ Y �)j+ Ob  �.E�Ob  e  �E�Y b  f  �E�Y hO�������lv�l� a ,E�O�a   
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY hr ������������� 0 freespaceinit freeSpaceInit�� ����� �  ���� 	0 again  ��  � ���������� 	0 again  �� 0 	temptitle 	tempTitle�� 	0 reply  �� 0 msg  � �����������������������������������������#&�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes�� 0 
backupinit 
backupInit�� 0 setfocus setFocus
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp
�� 
cobj
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  
*j+ Y �)j+ Ob  e  �E�Y b  f  �E�Y hO�f  �������lv�l� a ,E�Y (�e  !a ����a a lv�l� a ,E�Y hO�a   
*j+ Y ]b  b   b  a .E�Y hOb  	e  4b  e  ��a l Y b  f  a �a l Y hY hs ��-���������� 0 
backupinit 
backupInit��  ��  �  � ������@E��S����������Q����`g����s�������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  �� 0 itexists itExists
�� 
kocl
�� 
cfol
�� 
insh
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null�� 0 machinefolder machineFolder
�� 
cdis�� 0 backupfolder backupFolder�� 0 latestfolder latestFolder�� 
0 backup  �� �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` UO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` UO*j+ t ������������ 0 tidyup tidyUp��  ��  � ��������� 0 bf bF� 0 creationdates creationDates� 0 theoldestdate theOldestDate� 0 j  � 0 i  � 0 thisdate thisDate�  0 foldertodelete folderToDelete� 0 todelete toDelete� ��������������JL�j�m��|�
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  
� 
cdis
� 
cfol� 0 machinefolder machineFolder
� 
cobj
� 
ascd
� .corecnte****       ****
� 
pnam
� 
TEXT
� 
psxp
� .sysoexecTEXT���     TEXT
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr� 0 freespaceinit freeSpaceInit� �*��/E�O� �*��/��/��/E�O��-�,E�O��k/E�OkE�O 9l�j 
kh ��/E�O*�/�,� �� �E�O�E�Y hY h[OY��O��k/�&E�O��,�&E�O�%a %E�O�j Ob  b   Tb  	e  Fb  e  a a a l Okj Y #b  f  a a a l Okj Y hY hY hO*ek+ Uu �������� 
0 backup  �  �  � 
����������� 0 t  � 0 x  � "0 containerfolder containerFolder� 0 
foldername 
folderName� 0 d  � 0 duration  � 0 msg  � 0 c  � 0 oldestfolder oldestFolder� 0 todelete toDelete� f��������������������������%�(�25�~JLN`bd�}�|�{�z�y�x�������w��v����
9HKcegi�u�t������� 025DGQT�������������� 0 gettimestamp getTimestamp� (0 animatemenubaricon animateMenuBarIcon
� .sysodelanull��� ��� nmbr� 0 	stopwatch  
� 
psxf� 0 sourcefolder sourceFolder
� 
TEXT
� 
cfol
� 
pnam
� 
kocl
� 
insh� 0 backupfolder backupFolder
� 
prdt� 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � 0 machinefolder machineFolder� (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
� 
appr
� .sysonotfnull��� ��� TEXT�~ "0 destinationdisk destinationDisk
�} .sysoexecTEXT���     TEXT�|  �{  �z 0 endtime endTime�y 0 getduration getDuration
�x 
cobj�w $0 resetmenubaricon resetMenuBarIcon
�v .aevtquitnull��� ��� null
�u 
alis
�t 
psxp�k*ek+  E�OeEc  O*j+ Okj O*�k+ O�:*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e `*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_  a !%_ %a "%�%a #%�&E�O a $�%a %%�%a &%j 'W X ( )hOfEc  OfEc  Ob  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  Tb  e   a -�%a .%�%a a /l Okj Y )b  f  a 0�%a 1%a a 2l Okj Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYbb  f W_  a 6%_ %a 7%�%a 8%�%a 9%�&E�O_  a :%_ %a ;%�%a <%�&E�Ob  b   l*j a ,E` Ob  a ,.E�Ob  	e  Db  e  �a a =l Okj Y #b  f  a >a a ?l Okj Y hY hY hO a @�%a A%�%a B%�%a C%j 'W X ( )hOfEc  OfEc  O*�/a D&a ,-jv Q��/�&E�O�a E,�&E�Oa F�%a G%E�O�j 'Ob  b  *j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  �a H  Tb  e   a I�%a J%�%a a Kl Okj Y )b  f  a L�%a M%a a Nl Okj Y hY Qb  e   a O�%a P%�%a a Ql Okj Y )b  f  a R�%a S%a a Tl Okj Y hOb  e  a Ua a Vl Y b  f  a Wa a Xl Y hY hO)j+ 3Y hY	b  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  ��a H  Tb  e   a Y�%a Z%�%a a [l Okj Y )b  f  a \�%a ]%a a ^l Okj Y hY Qb  e   a _�%a `%�%a a al Okj Y )b  f  a b�%a c%a a dl Okj Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a ek+ v �s	
�r�q���p�s 0 itexists itExists�r �o��o �  �n�m�n 0 
objecttype 
objectType�m 
0 object  �q  � �l�k�l 0 
objecttype 
objectType�k 
0 object  � 	?	�j�i	%�h	5�g
�j 
cdis
�i .coredoexnull���     ****
�h 
file
�g 
cfol�p X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hUw �f	E�e�d���c�f .0 getcomputeridentifier getComputerIdentifier�e  �d  � �b�a�`�b 0 computername computerName�a "0 strserialnumber strSerialNumber�`  0 identifiername identifierName� �_�^	S�]	^
�_ .sysosigtsirr   ��� null
�^ 
sicn
�] .sysoexecTEXT���     TEXT�c *j  �,E�O�j E�O��%�%E�O�x �\	g�[�Z���Y�\ 0 gettimestamp getTimestamp�[ �X��X �  �W�W 0 isfolder isFolder�Z  � �V�U�T�S�R�Q�P�O�N�M�L�K�J�I�H�G�V 0 isfolder isFolder�U 0 y  �T 0 m  �S 0 d  �R 0 t  �Q 0 ty tY�P 0 tm tM�O 0 td tD�N 0 tt tT�M 
0 tml tML�L 
0 tdl tDL�K 0 timestr timeStr�J 0 pos  �I 0 h  �H 0 s  �G 0 	timestamp  � �F�E�D�C�B�A�@�?�>�=�<�;�:�9�8	�	��7�6�5	��4�3�2�1

4
^
`
v
�F 
Krtn
�E 
year�D 0 y  
�C 
mnth�B 0 m  
�A 
day �@ 0 d  
�? 
time�> 0 t  �= 
�< .misccurdldt    ��� null
�; 
long
�: 
TEXT
�9 .corecnte****       ****
�8 
nmbr
�7 
tstr
�6 misccura
�5 
psof
�4 
psin�3 
�2 .sysooffslong    ��� null
�1 
cha �Y�*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�y �0
��/�.���-�0 0 comparesizes compareSizes�/ �,��, �  �+�*�+ 
0 source  �* 0 destination  �.  � �)�(�'�&�%�$�#�"�!� �������) 
0 source  �( 0 destination  �' 0 fit  �& 0 
templatest 
tempLatest�% $0 latestfoldersize latestFolderSize�$ 0 c  �# 0 l  �" 0 md  �! 0 	cachesize 	cacheSize�  0 logssize logsSize� 0 mdsize mdSize� 
0 buffer  � (0 adjustedfoldersize adjustedFolderSize� 0 
foldersize 
folderSize� 0 	freespace 	freeSpace� 0 temp  �  ��
��
������
�
�
�
��
�
��������35PRmo��
� 
psxf� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� afdrdlib
� 
from
� fldmfldu
� .earsffdralis        afdr
� 
psxp
� .sysoexecTEXT���     TEXT
� misccura� � d
� .sysorondlong        doub
� 
cdis
� 
frsp�-�eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O��%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�Ob  f  /a �%a %j E�Oa  �a !a  j Ua !E�Y hUOb  f  ,��E�O�j ��l E�Y hO��� fE�Y hY b  e  ��� fE�Y hY hO�z �
��	�����
 0 	stopwatch  �	 ��� �  �� 0 mode  �  � ��� 0 mode  � 0 x  � �� 0 gettimestamp getTimestamp� $��  *fk+ E�Y ��  *fk+ E�Y h{ �� ������� 0 getduration getDuration�   ��  � ���� 0 duration  � �������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
�� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�| ��<���������� 0 setfocus setFocus��  ��  �  � ?��
�� .miscactvnull��� ��� null�� � *j U} ��J���������� $0 menuneedsupdate_ menuNeedsUpdate_�� ����� �  ��
�� 
cmnu��  �  � ���� 0 	makemenus 	makeMenus�� )j+  ~ ��U���������� 0 	makemenus 	makeMenus��  ��  � �������� 0 i  �� 0 	this_item  �� 0 thismenuitem thisMenuItem� "��������v��������������������� .;>������������  0 removeallitems removeAllItems
�� 
cobj
�� 
nmbr
�� 
TEXT
�� misccura�� 0 
nsmenuitem 
NSMenuItem�� 	0 alloc  �� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�� 0 additem_ addItem_�� 0 
settarget_ 
setTarget_�� 
�� 
bool�� 0 separatoritem separatorItem��?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY�� ��v����������  0 backuphandler_ backupHandler_�� ����� �  ���� 
0 sender  ��  � ������ 
0 sender  �� 0 msg  � ���������������
�� 
appr
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr
�� 
cobj�� �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h� ������������� .0 backupandquithandler_ backupAndQuitHandler_�� ����� �  ���� 
0 sender  ��  � ���������� 
0 sender  �� 0 	temptitle 	tempTitle�� 	0 reply  �� 0 msg  � ��
���������$BE��TW`����
�� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj��b  f  �b  e  �E�Y b  f  �E�Y hO�������lv�l� �,E�O��  \eEc  
OeEc  Ob  	e  >b  e  ���l Okj Y !b  f  a �a l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h� ��������  0 nsfwonhandler_ nsfwOnHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � ������������ � >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �������� "0 nsfwoffhandler_ nsfwOffHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � ������� � >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y h� �!������ 00 notificationonhandler_ notificationOnHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � 59=AD�QUY]`� � >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� �g������ 20 notificationoffhandler_ notificationOffHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � {���������� � >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y h� �������� 0 quithandler_ quitHandler_� ��� �  �� 
0 sender  �  � ���� 
0 sender  � 0 	temptitle 	tempTitle� 	0 reply  � �����������������*58B� 0 setfocus setFocus
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit
� .aevtquitnull��� ��� null� �b  e  Q)j+  Ob  e  �E�Y b  f  �E�Y hO�������lv�l� �,E�O��  
�j Y hY hb  f  ])j+  Ob  e  
a E�Y b  f  
a E�Y hOa ����a a lv�l� �,E�O�a   
�j Y hY h� �K������ (0 animatemenubaricon animateMenuBarIcon�  �  � �~�}�~ 0 	imagepath 	imagePath�} 	0 image  � �|�{�z�yY�x�w�v�u�t�s
�| 
alis
�{ 
rtyp
�z 
ctxt
�y .earsffdralis        afdr
�x 
psxp
�w misccura�v 0 nsimage NSImage�u 	0 alloc  �t 20 initwithcontentsoffile_ initWithContentsOfFile_�s 0 	setimage_ 	setImage_� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
� �rw�q�p���o�r $0 resetmenubaricon resetMenuBarIcon�q  �p  � �n�m�n 0 	imagepath 	imagePath�m 	0 image  � �l�k�j�i��h�g�f�e�d�c
�l 
alis
�k 
rtyp
�j 
ctxt
�i .earsffdralis        afdr
�h 
psxp
�g misccura�f 0 nsimage NSImage�e 	0 alloc  �d 20 initwithcontentsoffile_ initWithContentsOfFile_�c 0 	setimage_ 	setImage_�o /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
� �b��a�`���_�b 0 makestatusbar makeStatusBar�a  �`  � �^�]�\�^ 0 bar  �] 0 	imagepath 	imagePath�\ 	0 image  � �[�Z�Y��X�W�V�U�T��S�R�Q�P�O�N��M�L�K
�[ misccura�Z 0 nsstatusbar NSStatusBar�Y "0 systemstatusbar systemStatusBar�X .0 statusitemwithlength_ statusItemWithLength_
�W 
alis
�V 
rtyp
�U 
ctxt
�T .earsffdralis        afdr
�S 
psxp�R 0 nsimage NSImage�Q 	0 alloc  �P 20 initwithcontentsoffile_ initWithContentsOfFile_�O 0 	setimage_ 	setImage_�N 0 nsmenu NSMenu�M  0 initwithtitle_ initWithTitle_�L 0 setdelegate_ setDelegate_�K 0 setmenu_ setMenu_�_ s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ � �J	�I�H���G�J 0 runonce runOnce�I  �H  �  � �F�E�F (0 animatemenubaricon animateMenuBarIcon�E 0 	plistinit 	plistInit�G *j+  O*j+ � �D�C�B���A
�D .miscidlenmbr    ��� null�C  �B  � �@�@ 0 m  � �?�>�=�<�;�:�9�8
�? .misccurdldt    ��� null
�> 
min �= �< -
�; 
bool�: 0 scheduledtime scheduledTime�9 0 	plistinit 	plistInit�8 �A �b  b   tb  f  fb  f  F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY b  e  
*j+ Y hY hY hO�� �7��6�5���4
�7 .aevtoappnull  �   � ****� k     ��  ��� �� �3�3  �6  �5  �  � �2�1�0�/ ��.�-�,
�2 
alis
�1 
rtyp
�0 
ctxt
�/ .earsffdralis        afdr�. 0 appicon appIcon�- 0 makestatusbar makeStatusBar�, 0 runonce runOnce�4 *�)��l �%/E�O)j+ O*j+  ascr  ��ޭ