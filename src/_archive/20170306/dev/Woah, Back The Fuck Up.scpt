FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 ^ X Donkey icon/logo taken from http://thedeciderist.com/2013/01/29/the-party-of-less-evil/    6 � 7 7 �   D o n k e y   i c o n / l o g o   t a k e n   f r o m   h t t p : / / t h e d e c i d e r i s t . c o m / 2 0 1 3 / 0 1 / 2 9 / t h e - p a r t y - o f - l e s s - e v i l / 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    E � F F   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x C  G H G l     ��������  ��  ��   H  I J I l     ��������  ��  ��   J  K L K l     ��������  ��  ��   L  M N M l     ��������  ��  ��   N  O P O l     ��������  ��  ��   P  Q R Q l     �� S T��   S . ( Properties and includes for the menubar    T � U U P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r R  V W V x     �� X Y��   X 1      ��
�� 
ascr Y �� Z��
�� 
minv Z m       [ [ � \ \  2 . 4��   W  ] ^ ] x    �� _����   _ 2  	 ��
�� 
osax��   ^  ` a ` x     �� b����   b 4    �� c
�� 
frmk c m     d d � e e  F o u n d a t i o n��   a  f g f x     -�� h����   h 4   " &�� i
�� 
frmk i m   $ % j j � k k  A p p K i t��   g  l m l l     ��������  ��  ��   m  n o n j   - /�� p�� 0 
statusitem 
StatusItem p m   - .��
�� 
msng o  q r q l     ��������  ��  ��   r  s t s j   0 2�� u�� 0 
thedisplay 
theDisplay u m   0 1 v v � w w   t  x y x j   3 9�� z�� 0 defaults   z 4   3 8�� {
�� 
pcls { m   5 6 | | � } }  N S U s e r D e f a u l t s y  ~  ~ j   : @�� ��� $0 internalmenuitem internalMenuItem � 4   : ?�� �
�� 
pcls � m   < = � � � � �  N S M e n u I t e m   � � � j   A I�� ��� $0 externalmenuitem externalMenuItem � 4   A H�� �
�� 
pcls � m   C F � � � � �  N S M e n u I t e m �  � � � j   J R�� ��� 0 newmenu newMenu � 4   J Q�� �
�� 
pcls � m   L O � � � � �  N S M e n u �  � � � l     ��������  ��  ��   �  � � � j   S g�� ��� 0 thelist theList � J   S f � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " B a c k u p   n o w   &   q u i t �  � � � m   Y \ � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   \ _ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   _ b � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   h j�� ��� 0 nsfw   � m   h i��
�� boovfals �  � � � j   k m�� ��� 0 notifications   � m   k l��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � j   n p�� ���  0 backupthenquit backupThenQuit � m   n o��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��~�}�  �~  �}   �  � � � l     �| � ��|   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   q q � � �{ ��{ 0 sourcefolder sourceFolder � �z ��z "0 destinationdisk destinationDisk � �y ��y 	0 drive   � �x ��x 0 machinefolder machineFolder � �w ��w 0 backupfolder backupFolder � �v ��v (0 activesourcefolder activeSourceFolder � �u ��u (0 activebackupfolder activeBackupFolder � �t�s�t 0 latestfolder latestFolder�s   �  � � � p   q q � � �r ��r 0 scheduledtime scheduledTime � �q ��q 0 	starttime 	startTime � �p�o�p 0 endtime endTime�o   �  � � � p   q q � � �n�m�n 0 appicon appIcon�m   �  � � � l     �l�k�j�l  �k  �j   �  � � � l     ��i�h � r      � � � 4     �g �
�g 
alis � l    ��f�e � b     � � � l   	 ��d�c � I   	�b � �
�b .earsffdralis        afdr �  f     � �a ��`
�a 
rtyp � m    �_
�_ 
ctxt�`  �d  �c   � m   	 
 � � � � � < C o n t e n t s : R e s o u r c e s : a p p l e t . i c n s�f  �e   � o      �^�^ 0 appicon appIcon�i  �h   �  � � � l     �]�\�[�]  �\  �[   �  � � � l     �Z�Y�X�Z  �Y  �X   �  � � � l     �W�V�U�W  �V  �U   �  � � � l     �T�S�R�T  �S  �R   �  � � � l     �Q�P�O�Q  �P  �O   �  � � � l     �N �N      Property assignment    � (   P r o p e r t y   a s s i g n m e n t �  j   q ��M�M 	0 plist   b   q � n  q �	 1   ~ ��L
�L 
psxp	 l  q ~
�K�J
 I  q ~�I
�I .earsffdralis        afdr m   q t�H
�H afdrdlib �G�F
�G 
from m   w z�E
�E fldmfldu�F  �K  �J   m   � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t  l     �D�C�B�D  �C  �B    j   � ��A�A 0 initialbackup initialBackup m   � ��@
�@ boovtrue  j   � ��?�? 0 manualbackup manualBackup m   � ��>
�> boovfals  j   � ��=�= 0 isbackingup isBackingUp m   � ��<
�< boovfals  l     �;�:�9�;  �:  �9    j   � ��8�8 "0 messagesmissing messagesMissing J   � �   !"! m   � �## �$$ t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . ." %&% m   � �'' �(( V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . .& )*) m   � �++ �,, ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !* -.- m   � �// �00 < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ?. 1�71 m   � �22 �33 � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�7   454 l     �6�5�4�6  �5  �4  5 676 j   � ��38�3 *0 messagesencouraging messagesEncouraging8 J   � �99 :;: m   � �<< �==  C o m e   o n !; >?> m   � �@@ �AA 4 C o m e   o n !   E y e   o f   t h e   t i g e r !? BCB m   � �DD �EE " N o   p a i n !   N o   p a i n !C FGF m   � �HH �II 0 L e t ' s   d o   t h i s   a s   a   t e a m !G JKJ m   � �LL �MM  W e   c a n   d o   i t !K NON m   � �PP �QQ  F r e e e e e e e e e d o m !O RSR m   � �TT �UU 2 A l t o g e t h e r   o r   n o t   a t   a l l !S VWV m   � �XX �YY H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !W Z[Z m   � �\\ �]] H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e ![ ^�2^ m   � �__ �``  Y o u   s e x y   f u c k !�2  7 aba l     �1�0�/�1  �0  �/  b cdc j   � ��.e�. $0 messagescomplete messagesCompletee J   � �ff ghg m   � �ii �jj  N i c e   o n e !h klk m   � �mm �nn * Y o u   a b s o l u t e   l e g   e n d !l opo m   � �qq �rr  G o o d   l a d !p sts m   � �uu �vv  P e r f i c k !t wxw m   � �yy �zz  H a p p y   d a y s !x {|{ m   � �}} �~~  F r e e e e e e e e e d o m !| � m   � ��� ��� H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !� ��� m   � ��� ��� B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !� ��� m   � ��� ��� d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !� ��-� m   � ��� ���  Y o u   s e x y   f u c k !�-  d ��� l     �,�+�*�,  �+  �*  � ��� j   ��)��) &0 messagescancelled messagesCancelled� J   ��� ��� m   � ��� ���  T h a t ' s   a   s h a m e� ��� m   � ��� ���  A h   m a n ,   u n l u c k y� ��� m   � ��� ���  O h   w e l l� ��� m   � ��� ��� @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e n� ��� m   ��� ��� , W e l l   t h a t ' s   a   l e t   d o w n� ��� m  �� ��� P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?� ��� m  �� ���  A r r o g a n t� ��� m  
�� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0� ��(� m  
�� ���  E l l   b e n d�(  � ��� l     �'�&�%�'  �&  �%  � ��� j  7�$��$ 40 messagesalreadybackingup messagesAlreadyBackingUp� J  4�� ��� m  �� ���  T h a t ' s   a   s h a m e� ��� m  �� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  �� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  !�� ���  D i c k� ��� m  !$�� ���  F u c k t a r d� ��� m  $'�� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  '*�� ���  A r r o g a n t� ��� m  *-�� ��� $ C h i l l   t h e   f u c k   o u t� ��#� m  -0�� ���  H o l d   f i r e�#  � ��� l     �"�!� �"  �!  �   � ��� j  8>��� 0 strawake strAwake� m  8;�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  ?E��� 0 strsleep strSleep� m  ?B�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  FP��� &0 displaysleepstate displaySleepState� I FM���
� .sysoexecTEXT���     TEXT� m  FI�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � �   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -�  l     �
�
   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     �		�	   � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   	 �

 - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����  �  �    l     ��   6 0 Function to check for a .plist preferences file    � `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e  l     ��   m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file    � �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e  l     ��   b \ If a .plist file is present, it assigns preferences to their corresponding global variables    � �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s  i  QT I      ��� � 0 	plistinit 	plistInit�  �    k    -   !"! q      ## ��$�� 0 
foldername 
folderName$ ��%�� 0 
backupdisk 
backupDisk% ��&��  0 computerfolder computerFolder& ������ 0 
backuptime 
backupTime��  " '(' r     )*) m     ����  * o      ���� 0 
backuptime 
backupTime( +,+ l   ��������  ��  ��  , -.- Z   �/0��1/ l   2����2 =    343 I    ��5���� 0 itexists itExists5 676 m    88 �99  f i l e7 :��: o    ���� 	0 plist  ��  ��  4 m    ��
�� boovtrue��  ��  0 O    E;<; k    D== >?> r    $@A@ n    "BCB 1     "��
�� 
pcntC 4     ��D
�� 
plifD o    ���� 	0 plist  A o      ���� 0 thecontents theContents? EFE r   % *GHG n   % (IJI 1   & (��
�� 
valLJ o   % &���� 0 thecontents theContentsH o      ���� 0 thevalue theValueF KLK l  + +��������  ��  ��  L MNM r   + 0OPO n   + .QRQ o   , .����  0 foldertobackup FolderToBackupR o   + ,���� 0 thevalue theValueP o      ���� 0 
foldername 
folderNameN STS r   1 6UVU n   1 4WXW o   2 4���� 0 backupdrive BackupDriveX o   1 2���� 0 thevalue theValueV o      ���� 0 
backupdisk 
backupDiskT YZY r   7 <[\[ n   7 :]^] o   8 :����  0 computerfolder computerFolder^ o   7 8���� 0 thevalue theValue\ o      ����  0 computerfolder computerFolderZ _`_ r   = Baba n   = @cdc o   > @���� 0 scheduledtime scheduledTimed o   = >���� 0 thevalue theValueb o      ���� 0 
backuptime 
backupTime` efe l  C C��������  ��  ��  f g��g l  C C��hi��  h . (log {folderName, backupDisk, backupTime}   i �jj P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  < m    kk�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  1 k   H�ll mnm r   H Oopo I   H M�������� .0 getcomputeridentifier getComputerIdentifier��  ��  p o      ����  0 computerfolder computerFoldern qrq l  P P��������  ��  ��  r sts O   P=uvu t   T<wxw k   V;yy z{z n  V [|}| I   W [�������� 0 setfocus setFocus��  ��  }  f   V W{ ~~ l  \ \��������  ��  ��   ��� r   \ _��� m   \ ]�� ���L T h i s   p r o g r a m   a u t o m a t i c a l l y   b a c k s   u p   y o u r   H o m e   f o l d e r   b u t   e x c l u d e s   t h e   C a c h e s ,   L o g s   a n d   M o b i l e   D o c u m e n t s   f o l d e r s   f r o m   y o u r   L i b r a r y . 
 
 A l l   y o u   n e e d   t o   d o   i s   s e l e c t   t h e   e x t e r n a l   d r i v e   t o   b a c k u p   t o   a t   t h e   n e x t   p r o m p t . 
 
 Y o u r   b a c k u p s   w i l l   b e   s a v e d   i n : 
 [ E x t e r n a l   H a r d   D r i v e ]   >   B a c k u p s   >   [ C o m p u t e r   N a m e ]� o      ���� 0 welcome  � ��� l  ` `��������  ��  ��  � ��� Z   ` ������� l  ` g������ =   ` g��� o   ` e���� 0 nsfw  � m   e f��
�� boovtrue��  ��  � r   j m��� m   j k�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      ���� 0 	temptitle 	tempTitle� ��� l  p w������ =   p w��� o   p u���� 0 nsfw  � m   u v��
�� boovfals��  ��  � ���� r   z ��� m   z }�� ��� 
 W B T F U� o      ���� 0 	temptitle 	tempTitle��  ��  � ��� l  � ���������  ��  ��  � ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
bhit� l  � ������� I  � �����
�� .sysodlogaskr        TEXT� o   � ����� 0 welcome  � ����
�� 
appr� o   � ����� 0 	temptitle 	tempTitle� ����
�� 
disp� o   � ����� 0 appicon appIcon� ����
�� 
btns� J   � ��� ���� m   � ��� ���  O K ,   g o t   i t��  � �����
�� 
dflt� m   � ����� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ��� l  � ���������  ��  ��  � ��� r   � ���� I  � ������
�� .earsffdralis        afdr� l  � ������� m   � ���
�� afdrcusr��  ��  ��  � o      ���� 0 
foldername 
folderName� ��� l  � ���������  ��  ��  � ��� Q   � ����� r   � ���� c   � ���� l  � ������� I  � ������
�� .sysostflalis    ��� null��  � �����
�� 
prmp� m   � ��� ��� R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  � m   � ���
�� 
TEXT� o      ���� 0 
backupdisk 
backupDisk� R      ������
�� .ascrerr ****      � ****��  ��  � I  � ������
�� .sysoexecTEXT���     TEXT� m   � ��� ��� : k i l l   ' W o a h ,   B a c k   T h e   F u c k   U p '��  � ��� l  � ���������  ��  ��  � ��� r   � ���� n   � ���� 1   � ���
�� 
psxp� o   � ����� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� r   � ���� n   � ���� 1   � ���
�� 
psxp� o   � ����� 0 
backupdisk 
backupDisk� o      ���� 0 
backupdisk 
backupDisk� ��� l  � ���~�}�  �~  �}  � ��� r   � ���� J   � ��� ��� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r� ��� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r� ��|� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r�|  � o      �{�{ 0 backuptimes backupTimes� ��� r   ���� c   ���� J   ��� ��z� I  � ��y��
�y .gtqpchltns    @   @ ns  � o   � ��x�x 0 backuptimes backupTimes� �w��v
�w 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?�v  �z  � m  �u
�u 
TEXT� o      �t�t 0 selectedtime selectedTime� ��� l �s�r�q�s  �r  �q  � � � Z  9�p l �o�n =   o  	�m�m 0 selectedtime selectedTime m  	 � 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r�o  �n   r  	
	 m  �l�l 
 o      �k�k 0 
backuptime 
backupTime  l �j�i =   o  �h�h 0 selectedtime selectedTime m   � 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r�j  �i    r   % m   #�g�g  o      �f�f 0 
backuptime 
backupTime  l (-�e�d =  (- o  ()�c�c 0 selectedtime selectedTime m  ), � , E v e r y   h o u r   o n   t h e   h o u r�e  �d   �b r  05 m  03�a�a < o      �`�` 0 
backuptime 
backupTime�b  �p     !  l ::�_�^�]�_  �^  �]  ! "�\" l ::�[#$�[  # . (log {folderName, backupDisk, backupTime}   $ �%% P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�\  x m   T U�Z�Z  ��v m   P Q&&�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  t '(' l >>�Y�X�W�Y  �X  �W  ( )�V) O  >�*+* O  B�,-, k  ]�.. /0/ I ]��U�T1
�U .corecrel****      � null�T  1 �S23
�S 
kocl2 m  ad�R
�R 
plii3 �Q45
�Q 
insh4  ;  gi5 �P6�O
�P 
prdt6 K  l~77 �N89
�N 
kind8 m  or�M
�M 
TEXT9 �L:;
�L 
pnam: m  ux<< �==  F o l d e r T o B a c k u p; �K>�J
�K 
valL> o  yz�I�I 0 
foldername 
folderName�J  �O  0 ?@? I ���H�GA
�H .corecrel****      � null�G  A �FBC
�F 
koclB m  ���E
�E 
pliiC �DDE
�D 
inshD  ;  ��E �CF�B
�C 
prdtF K  ��GG �AHI
�A 
kindH m  ���@
�@ 
TEXTI �?JK
�? 
pnamJ m  ��LL �MM  B a c k u p D r i v eK �>N�=
�> 
valLN o  ���<�< 0 
backupdisk 
backupDisk�=  �B  @ OPO I ���;�:Q
�; .corecrel****      � null�:  Q �9RS
�9 
koclR m  ���8
�8 
pliiS �7TU
�7 
inshT  ;  ��U �6V�5
�6 
prdtV K  ��WW �4XY
�4 
kindX m  ���3
�3 
TEXTY �2Z[
�2 
pnamZ m  ��\\ �]]  C o m p u t e r F o l d e r[ �1^�0
�1 
valL^ o  ���/�/  0 computerfolder computerFolder�0  �5  P _�._ I ���-�,`
�- .corecrel****      � null�,  ` �+ab
�+ 
kocla m  ���*
�* 
pliib �)cd
�) 
inshc  ;  ��d �(e�'
�( 
prdte K  ��ff �&gh
�& 
kindg m  ���%
�% 
TEXTh �$ij
�$ 
pnami m  ��kk �ll  S c h e d u l e d T i m ej �#m�"
�# 
valLm o  ���!�! 0 
backuptime 
backupTime�"  �'  �.  - l BZn� �n I BZ��o
� .corecrel****      � null�  o �pq
� 
koclp m  FG�
� 
plifq �r�
� 
prdtr K  JTss �t�
� 
pnamt o  MR�� 	0 plist  �  �  �   �  + m  >?uu�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �V  . vwv l ������  �  �  w xyx r  �z{z o  � �� 0 
foldername 
folderName{ o      �� 0 sourcefolder sourceFoldery |}| r  
~~ o  �� 0 
backupdisk 
backupDisk o      �� "0 destinationdisk destinationDisk} ��� r  ��� o  ��  0 computerfolder computerFolder� o      �� 0 machinefolder machineFolder� ��� r  ��� o  �� 0 
backuptime 
backupTime� o      �� 0 scheduledtime scheduledTime� ��� I '�
��	
�
 .ascrcmnt****      � ****� J  #�� ��� o  �� 0 sourcefolder sourceFolder� ��� o  �� "0 destinationdisk destinationDisk� ��� o  �� 0 machinefolder machineFolder� ��� o  �� 0 scheduledtime scheduledTime�  �	  � ��� l ((����  �  �  � �� � I  (-�������� 0 diskinit diskInit��  ��  �    ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � H B Function to detect if the selected hard drive is connected or not   � ��� �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t� ��� l     ������  � T N This only happens once a hard drive has been selected and provides a reminder   � ��� �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r� ��� i  UX��� I      �������� 0 diskinit diskInit��  ��  � Z     ������� l    	������ =     	��� I     ������� 0 itexists itExists� ��� m    �� ���  d i s k� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    ������� 0 freespaceinit freeSpaceInit� ���� m    ��
�� boovfals��  ��  ��  � k    ��� ��� n   ��� I    �������� 0 setfocus setFocus��  ��  �  f    � ��� r    $��� n    "��� 3     "��
�� 
cobj� o     ���� "0 messagesmissing messagesMissing� o      ���� 0 msg  � ��� l  % %��������  ��  ��  � ��� Z   % F������ l  % ,������ =   % ,��� o   % *���� 0 nsfw  � m   * +��
�� boovtrue��  ��  � r   / 2��� m   / 0�� ��� , W o a h ,   B a c k   T h e   F u c k   U p� o      ���� 0 	temptitle 	tempTitle� ��� l  5 <������ =   5 <��� o   5 :���� 0 nsfw  � m   : ;��
�� boovfals��  ��  � ���� r   ? B��� m   ? @�� ��� 
 W B T F U� o      ���� 0 	temptitle 	tempTitle��  ��  � ��� l  G G��������  ��  ��  � ��� r   G ]��� l  G [������ n   G [��� 1   W [��
�� 
bhit� l  G W������ I  G W����
�� .sysodlogaskr        TEXT� o   G H���� 0 msg  � ����
�� 
appr� o   I J���� 0 	temptitle 	tempTitle� ����
�� 
disp� o   K L���� 0 appicon appIcon� ����
�� 
btns� J   M Q�� ��� m   M N�� ���  C a n c e l   B a c k u p� ���� m   N O�� ���  O K��  � �����
�� 
dflt� m   R S���� ��  ��  ��  ��  ��  � o      ���� 	0 reply  � ���� Z   ^ ������� l  ^ c������ =   ^ c� � o   ^ _���� 	0 reply    m   _ b �  O K��  ��  � I   f k�������� 0 diskinit diskInit��  ��  ��  � Z   n ����� l  n y���� E   n y o   n s���� &0 displaysleepstate displaySleepState o   s x���� 0 strawake strAwake��  ��   k   | � 	
	 r   | � n   | � 3   � ���
�� 
cobj o   | ����� &0 messagescancelled messagesCancelled o      ���� 0 msg  
 �� Z   � ����� l  � ����� =   � � o   � ����� 0 notifications   m   � ���
�� boovtrue��  ��   Z   � ��� l  � ����� =   � � o   � ����� 0 nsfw   m   � ���
�� boovtrue��  ��   I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � � � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��    !  l  � �"����" =   � �#$# o   � ����� 0 nsfw  $ m   � ���
�� boovfals��  ��  ! %��% I  � ���&'
�� .sysonotfnull��� ��� TEXT& m   � �(( �)) , Y o u   s t o p p e d   b a c k i n g   u p' ��*��
�� 
appr* m   � �++ �,, 
 W B T F U��  ��  ��  ��  ��  ��  ��  ��  ��  � -.- l     ��������  ��  ��  . /0/ l     ��������  ��  ��  0 121 l     ��������  ��  ��  2 343 l     �������  ��  �  4 565 l     �~�}�|�~  �}  �|  6 787 l     �{9:�{  9 � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   : �;;   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y8 <=< l     �z>?�z  > s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   ? �@@ �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x= ABA l     �yCD�y  C � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   D �EE�   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l yB FGF i  Y\HIH I      �xJ�w�x 0 freespaceinit freeSpaceInitJ K�vK o      �u�u 	0 again  �v  �w  I Z     �LM�tNL l    	O�s�rO =     	PQP I     �qR�p�q 0 comparesizes compareSizesR STS o    �o�o 0 sourcefolder sourceFolderT U�nU o    �m�m "0 destinationdisk destinationDisk�n  �p  Q m    �l
�l boovtrue�s  �r  M I    �k�j�i�k 0 
backupinit 
backupInit�j  �i  �t  N k    �VV WXW n   YZY I    �h�g�f�h 0 setfocus setFocus�g  �f  Z  f    X [\[ l   �e�d�c�e  �d  �c  \ ]^] Z    ;_`a�b_ l   !b�a�`b =    !cdc o    �_�_ 0 nsfw  d m     �^
�^ boovtrue�a  �`  ` r   $ 'efe m   $ %gg �hh , W o a h ,   B a c k   T h e   F u c k   U pf o      �]�] 0 	temptitle 	tempTitlea iji l  * 1k�\�[k =   * 1lml o   * /�Z�Z 0 nsfw  m m   / 0�Y
�Y boovfals�\  �[  j n�Xn r   4 7opo m   4 5qq �rr 
 W B T F Up o      �W�W 0 	temptitle 	tempTitle�X  �b  ^ sts l  < <�V�U�T�V  �U  �T  t uvu Z   < �wxy�Sw l  < ?z�R�Qz =   < ?{|{ o   < =�P�P 	0 again  | m   = >�O
�O boovfals�R  �Q  x r   B X}~} l  B V�N�M n   B V��� 1   R V�L
�L 
bhit� l  B R��K�J� I  B R�I��
�I .sysodlogaskr        TEXT� m   B C�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� �H��
�H 
appr� o   D E�G�G 0 	temptitle 	tempTitle� �F��
�F 
disp� o   F G�E�E 0 appicon appIcon� �D��
�D 
btns� J   H L�� ��� m   H I�� ���  Y e s� ��C� m   I J�� ���  C a n c e l   B a c k u p�C  � �B��A
�B 
dflt� m   M N�@�@ �A  �K  �J  �N  �M  ~ o      �?�? 	0 reply  y ��� l  [ ^��>�=� =   [ ^��� o   [ \�<�< 	0 again  � m   \ ]�;
�; boovtrue�>  �=  � ��:� r   a }��� l  a {��9�8� n   a {��� 1   w {�7
�7 
bhit� l  a w��6�5� I  a w�4��
�4 .sysodlogaskr        TEXT� m   a d�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   p e r m a n e n t l y   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� �3��
�3 
appr� o   e f�2�2 0 	temptitle 	tempTitle� �1��
�1 
disp� o   g h�0�0 0 appicon appIcon� �/��
�/ 
btns� J   i q�� ��� m   i l�� ���  Y e s� ��.� m   l o�� ���  C a n c e l   B a c k u p�.  � �-��,
�- 
dflt� m   r s�+�+ �,  �6  �5  �9  �8  � o      �*�* 	0 reply  �:  �S  v ��� l  � ��)�(�'�)  �(  �'  � ��&� Z   � ����%�� l  � ���$�#� =   � ���� o   � ��"�" 	0 reply  � m   � ��� ���  Y e s�$  �#  � I   � ��!� ��! 0 tidyup tidyUp�   �  �%  � k   � ��� ��� Z   � ������ l  � ����� E   � ���� o   � ��� &0 displaysleepstate displaySleepState� o   � ��� 0 strawake strAwake�  �  � r   � ���� n   � ���� 3   � ��
� 
cobj� o   � ��� &0 messagescancelled messagesCancelled� o      �� 0 msg  �  �  � ��� l  � �����  �  �  � ��� Z   � ������ l  � ����� =   � ���� o   � ��� 0 notifications  � m   � ��
� boovtrue�  �  � Z   � ������ l  � ���
�	� =   � ���� o   � ��� 0 nsfw  � m   � ��
� boovtrue�
  �	  � I  � ����
� .sysonotfnull��� ��� TEXT� o   � ��� 0 msg  � ���
� 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  � ��� l  � ����� =   � ���� o   � �� �  0 nsfw  � m   � ���
�� boovfals�  �  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  ��  �  �  �  �  �&  G ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  �,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   � ���L   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .� ��� l     ������  � g a These folders, if required, are then set to their corresponding global variables declared above.   � ��� �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .�    i  ]` I      �������� 0 
backupinit 
backupInit��  ��   k     �  r      4     ��	
�� 
psxf	 o    ���� "0 destinationdisk destinationDisk o      ���� 	0 drive   

 l   ����   ' !log (destinationDisk & "Backups")    � B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )  l   ��������  ��  ��    Z    ,���� l   ���� =     I    ������ 0 itexists itExists  m    	 �  f o l d e r �� b   	  o   	 
���� "0 destinationdisk destinationDisk m   
    �!!  B a c k u p s��  ��   m    ��
�� boovfals��  ��   O    ("#" I   '����$
�� .corecrel****      � null��  $ ��%&
�� 
kocl% m    ��
�� 
cfol& ��'(
�� 
insh' o    ���� 	0 drive  ( ��)��
�� 
prdt) K    #** ��+��
�� 
pnam+ m     !,, �--  B a c k u p s��  ��  # m    ..�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ��   /0/ l  - -��������  ��  ��  0 121 Z   - d34����3 l  - >5����5 =   - >676 I   - <��8���� 0 itexists itExists8 9:9 m   . /;; �<<  f o l d e r: =��= b   / 8>?> b   / 4@A@ o   / 0���� "0 destinationdisk destinationDiskA m   0 3BB �CC  B a c k u p s /? o   4 7���� 0 machinefolder machineFolder��  ��  7 m   < =��
�� boovfals��  ��  4 O   A `DED I  E _����F
�� .corecrel****      � null��  F ��GH
�� 
koclG m   G H��
�� 
cfolH ��IJ
�� 
inshI n   I TKLK 4   O T��M
�� 
cfolM m   P SNN �OO  B a c k u p sL 4   I O��P
�� 
cdisP o   M N���� 	0 drive  J ��Q��
�� 
prdtQ K   U [RR ��S��
�� 
pnamS o   V Y���� 0 machinefolder machineFolder��  ��  E m   A BTT�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  ��  2 UVU l  e e��������  ��  ��  V WXW O   e ~YZY r   i }[\[ n   i y]^] 4   t y��_
�� 
cfol_ o   u x���� 0 machinefolder machineFolder^ n   i t`a` 4   o t��b
�� 
cfolb m   p scc �dd  B a c k u p sa 4   i o��e
�� 
cdise o   m n���� 	0 drive  \ o      ���� 0 backupfolder backupFolderZ m   e fff�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  X ghg l   ��������  ��  ��  h iji Z    �kl��mk l   �n����n =    �opo I    ���q���� 0 itexists itExistsq rsr m   � �tt �uu  f o l d e rs v��v b   � �wxw b   � �yzy b   � �{|{ o   � ����� "0 destinationdisk destinationDisk| m   � �}} �~~  B a c k u p s /z o   � ����� 0 machinefolder machineFolderx m   � � ���  / L a t e s t��  ��  p m   � ���
�� boovfals��  ��  l O   � ���� I  � ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
cfol� ����
�� 
insh� o   � ����� 0 backupfolder backupFolder� �����
�� 
prdt� K   � ��� �����
�� 
pnam� m   � ��� ���  L a t e s t��  ��  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  m r   � ���� m   � ���
�� boovfals� o      ���� 0 initialbackup initialBackupj ��� l  � ���������  ��  ��  � ��� O   � ���� r   � ���� n   � ���� 4   � ����
�� 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ����
�� 
cfol� o   � ����� 0 machinefolder machineFolder� n   � ���� 4   � ����
�� 
cfol� m   � ��� ���  B a c k u p s� 4   � ����
�� 
cdis� o   � ����� 	0 drive  � o      ���� 0 latestfolder latestFolder� m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ���������  ��  ��  � ���� I   � ��������� 
0 backup  ��  ��  ��   ��� l     ����~��  �  �~  � ��� l     �}�|�{�}  �|  �{  � ��� l     �z�y�x�z  �y  �x  � ��� l     �w�v�u�w  �v  �u  � ��� l     �t�s�r�t  �s  �r  � ��� l     �q���q  � j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.   � ��� �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .� ��� l     �p���p  � � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   � ����   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .� ��� l     �o���o  � � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   � ���P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .� ��� l     �n���n  � � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   � ���>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .� ��� i  ad��� I      �m�l�k�m 0 tidyup tidyUp�l  �k  � k    �� ��� r     ��� 4     �j�
�j 
psxf� o    �i�i "0 destinationdisk destinationDisk� o      �h�h 	0 drive  � ��� l   �g�f�e�g  �f  �e  � ��d� O   ��� k   �� ��� r    ��� n    ��� 4    �c�
�c 
cfol� o    �b�b 0 machinefolder machineFolder� n    ��� 4    �a�
�a 
cfol� m    �� ���  B a c k u p s� 4    �`�
�` 
cdis� o    �_�_ 	0 drive  � o      �^�^ 0 bf bF� ��� r    ��� n    ��� 1    �]
�] 
ascd� n    ��� 2   �\
�\ 
cobj� o    �[�[ 0 bf bF� o      �Z�Z 0 creationdates creationDates� ��� r     &��� n     $��� 4   ! $�Y�
�Y 
cobj� m   " #�X�X � o     !�W�W 0 creationdates creationDates� o      �V�V 0 theoldestdate theOldestDate� ��� r   ' *��� m   ' (�U�U � o      �T�T 0 j  � ��� l  + +�S�R�Q�S  �R  �Q  � ��� Y   + n��P���O� k   9 i��    r   9 ? n   9 = 4   : =�N
�N 
cobj o   ; <�M�M 0 i   o   9 :�L�L 0 creationdates creationDates o      �K�K 0 thisdate thisDate �J Z   @ i	�I�H l  @ H
�G�F
 >  @ H n   @ F 1   D F�E
�E 
pnam 4   @ D�D
�D 
cobj o   B C�C�C 0 i   m   F G �  L a t e s t�G  �F  	 Z   K e�B�A l  K N�@�? A   K N o   K L�>�> 0 thisdate thisDate o   L M�=�= 0 theoldestdate theOldestDate�@  �?   k   Q a  r   Q T o   Q R�<�< 0 thisdate thisDate o      �;�; 0 theoldestdate theOldestDate  r   U X o   U V�:�: 0 i   o      �9�9 0 j    !  l  Y Y�8"#�8  " " log (item j of backupFolder)   # �$$ 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )! %�7% I  Y a�6&�5
�6 .ascrcmnt****      � ****& l  Y ]'�4�3' n   Y ]()( 4   Z ]�2*
�2 
cobj* o   [ \�1�1 0 j  ) o   Y Z�0�0 0 bf bF�4  �3  �5  �7  �B  �A  �I  �H  �J  �P 0 i  � m   . /�/�/ � I  / 4�.+�-
�. .corecnte****       ****+ o   / 0�,�, 0 creationdates creationDates�-  �O  � ,-, l  o o�+�*�)�+  �*  �)  - ./. l  o o�(01�(  0 ! -- Delete the oldest folder   1 �22 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r/ 343 l  o o�'56�'  5 # delete item j of backupFolder   6 �77 : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r4 898 l  o o�&:;�&  :  delete item (j + 1) of bF   ; �<< 2 d e l e t e   i t e m   ( j   +   1 )   o f   b F9 =>= l  o o�%�$�#�%  �$  �#  > ?@? r   o yABA c   o wCDC l  o uE�"�!E n   o uFGF 4   p u� H
�  
cobjH l  q tI��I [   q tJKJ o   q r�� 0 j  K m   r s�� �  �  G o   o p�� 0 bf bF�"  �!  D m   u v�
� 
TEXTB o      ��  0 foldertodelete folderToDelete@ LML r   z �NON c   z PQP n   z }RSR 1   { }�
� 
psxpS o   z {��  0 foldertodelete folderToDeleteQ m   } ~�
� 
TEXTO o      ��  0 foldertodelete folderToDeleteM TUT I  � ��V�
� .ascrcmnt****      � ****V l  � �W��W b   � �XYX m   � �ZZ �[[ $ f o l d e r   t o   d e l e t e :  Y o   � ���  0 foldertodelete folderToDelete�  �  �  U \]\ l  � �����  �  �  ] ^_^ l  � ����
�  �  �
  _ `a` r   � �bcb b   � �ded b   � �fgf m   � �hh �ii  r m   - r f   'g o   � ��	�	  0 foldertodelete folderToDeletee m   � �jj �kk  'c o      �� 0 todelete toDeletea lml I  � ��n�
� .sysoexecTEXT���     TEXTn o   � ��� 0 todelete toDelete�  m opo l  � �����  �  �  p qrq l  � ��� ���  �   ��  r sts n  � �uvu I   � ��������� 0 setfocus setFocus��  ��  v  f   � �t wxw l  � ���������  ��  ��  x yzy l   � ���{|��  {�}if (nsfw = true) then			set tempTitle to "Woah, Back The Fuck Up"		else if (nsfw = false) then			set tempTitle to "WBTFU"		end if				set reply2 to the button returned of (display dialog "You need to empty your trash first before a backup can happen.
Click Empty Trash to do this automatically and continue backing up, or Cancel to stop the process." with title tempTitle with icon appIcon buttons {"Empty Trash", "Cancel Backup"} default button 2)		if (reply2 = "Empty Trash") then			empty the trash		else			if (displaySleepState contains strAwake) then				set msg to some item of messagesCancelled				if (notifications = true) then					if (nsfw = true) then						display notification msg with title "You stopped backing the fuck up"					else if (nsfw = false) then						display notification "You stopped backing up" with title "WBTFU"					end if				end if			end if		end if   | �}}� i f   ( n s f w   =   t r u e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W o a h ,   B a c k   T h e   F u c k   U p "  	 	 e l s e   i f   ( n s f w   =   f a l s e )   t h e n  	 	 	 s e t   t e m p T i t l e   t o   " W B T F U "  	 	 e n d   i f  	 	  	 	 s e t   r e p l y 2   t o   t h e   b u t t o n   r e t u r n e d   o f   ( d i s p l a y   d i a l o g   " Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s . "   w i t h   t i t l e   t e m p T i t l e   w i t h   i c o n   a p p I c o n   b u t t o n s   { " E m p t y   T r a s h " ,   " C a n c e l   B a c k u p " }   d e f a u l t   b u t t o n   2 )  	 	 i f   ( r e p l y 2   =   " E m p t y   T r a s h " )   t h e n  	 	 	 e m p t y   t h e   t r a s h  	 	 e l s e  	 	 	 i f   ( d i s p l a y S l e e p S t a t e   c o n t a i n s   s t r A w a k e )   t h e n  	 	 	 	 s e t   m s g   t o   s o m e   i t e m   o f   m e s s a g e s C a n c e l l e d  	 	 	 	 i f   ( n o t i f i c a t i o n s   =   t r u e )   t h e n  	 	 	 	 	 i f   ( n s f w   =   t r u e )   t h e n  	 	 	 	 	 	 d i s p l a y   n o t i f i c a t i o n   m s g   w i t h   t i t l e   " Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p "  	 	 	 	 	 e l s e   i f   ( n s f w   =   f a l s e )   t h e n  	 	 	 	 	 	 d i s p l a y   n o t i f i c a t i o n   " Y o u   s t o p p e d   b a c k i n g   u p "   w i t h   t i t l e   " W B T F U "  	 	 	 	 	 e n d   i f  	 	 	 	 e n d   i f  	 	 	 e n d   i f  	 	 e n d   i fz ~~ l  � ���������  ��  ��   ��� Z   �������� l  � ������� E   � ���� o   � ����� &0 displaysleepstate displaySleepState� o   � ����� 0 strawake strAwake��  ��  � Z   �������� l  � ������� =   � ���� o   � ����� 0 notifications  � m   � ���
�� boovtrue��  ��  � Z   � ������� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovtrue��  ��  � k   � ��� ��� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .� �����
�� 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  � ��� l  � ������� =   � ���� o   � ����� 0 nsfw  � m   � ���
�� boovfals��  ��  � ���� k   � ��� ��� I  � �����
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n� �����
�� 
appr� m   � ��� ��� 
 W B T F U��  � ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  ��  ��  ��  ��  ��  ��  � ��� l ��������  ��  ��  � ���� I  ������� 0 freespaceinit freeSpaceInit� ���� m  ��
�� boovtrue��  ��  ��  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �d  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � M G Function that carries out the backup process and is split in 2 parts.    � ��� �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  � ��� l     ������  �*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   � ���H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .� ��� l     ������  � � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   � ���   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .� ��� i  eh��� I      �������� 
0 backup  ��  ��  � k    ��� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovtrue��  ��  � o      ���� 0 t  � ��� l  	 	��������  ��  ��  � ��� r   	 ��� m   	 
��
�� boovtrue� o      ���� 0 isbackingup isBackingUp� ��� l   ��������  ��  ��  � ��� I    �������� (0 animatemenubaricon animateMenuBarIcon��  ��  � ��� I   �����
�� .sysodelanull��� ��� nmbr� m    ���� ��  � ��� l   ��������  ��  ��  � ��� I    #������� 0 	stopwatch  � ���� m    �� ��� 
 s t a r t��  ��  � ��� l  $ $��������  ��  ��  � ��� O   $y��� k   (x�� ��� l  ( (������  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d�    l  ( (����   x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up    � �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p  r   ( 0 c   ( .	
	 4   ( ,��
�� 
psxf o   * +���� 0 sourcefolder sourceFolder
 m   , -��
�� 
TEXT o      ���� 0 
foldername 
folderName  r   1 9 n   1 7 1   5 7��
�� 
pnam 4   1 5��
�� 
cfol o   3 4���� 0 
foldername 
folderName o      ���� 0 
foldername 
folderName  l  : :����    log (folderName)		    � $ l o g   ( f o l d e r N a m e ) 	 	  l  : :��������  ��  ��    Z   : ����� l  : A���� =   : A  o   : ?���� 0 initialbackup initialBackup  m   ? @��
�� boovfals��  ��   k   D �!! "#" I  D R��~$
� .corecrel****      � null�~  $ �}%&
�} 
kocl% m   F G�|
�| 
cfol& �{'(
�{ 
insh' o   H I�z�z 0 backupfolder backupFolder( �y)�x
�y 
prdt) K   J N** �w+�v
�w 
pnam+ o   K L�u�u 0 t  �v  �x  # ,-, l  S S�t�s�r�t  �s  �r  - ./. r   S ]010 c   S Y232 4   S W�q4
�q 
psxf4 o   U V�p�p 0 sourcefolder sourceFolder3 m   W X�o
�o 
TEXT1 o      �n�n (0 activesourcefolder activeSourceFolder/ 565 r   ^ h787 4   ^ d�m9
�m 
cfol9 o   ` c�l�l (0 activesourcefolder activeSourceFolder8 o      �k�k (0 activesourcefolder activeSourceFolder6 :;: r   i �<=< n   i ~>?> 4   { ~�j@
�j 
cfol@ o   | }�i�i 0 t  ? n   i {ABA 4   v {�hC
�h 
cfolC o   w z�g�g 0 machinefolder machineFolderB n   i vDED 4   q v�fF
�f 
cfolF m   r uGG �HH  B a c k u p sE 4   i q�eI
�e 
cdisI o   m p�d�d 	0 drive  = o      �c�c (0 activebackupfolder activeBackupFolder; JKJ l  � ��bLM�b  L 3 -log (activeSourceFolder & activeBackupFolder)   M �NN Z l o g   ( a c t i v e S o u r c e F o l d e r   &   a c t i v e B a c k u p F o l d e r )K O�aO I  � ��`�_P
�` .corecrel****      � null�_  P �^QR
�^ 
koclQ m   � ��]
�] 
cfolR �\ST
�\ 
inshS o   � ��[�[ (0 activebackupfolder activeBackupFolderT �ZU�Y
�Z 
prdtU K   � �VV �XW�W
�X 
pnamW o   � ��V�V 0 
foldername 
folderName�W  �Y  �a  ��  ��   XYX l  � ��U�T�S�U  �T  �S  Y Z[Z l  � ��R�Q�P�R  �Q  �P  [ \]\ l  � ��O^_�O  ^ o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   _ �`` �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e] aba l  � ��Ncd�N  c q k An atomic version would just to do a straight backup without versioning to see the function at full effect   d �ee �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c tb fgf l  � ��Mhi�M  h D > This means that only new or updated files will be copied over   i �jj |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e rg klk l  � ��Lmn�L  m ] W Any deleted files in the source folder will be deleted in the destination folder too		   n �oo �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	l pqp l  � ��Krs�K  r   Original sync code    s �tt (   O r i g i n a l   s y n c   c o d e  q uvu l  � ��Jwx�J  w z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   x �yy �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "v z{z l  � ��I�H�G�I  �H  �G  { |}| l  � ��F~�F  ~   Original code    ���    O r i g i n a l   c o d e} ��� l  � ��E���E  � i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   � ��� �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h� ��� l  � ��D���D  � x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   � ��� �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .� ��� l  � ��C���C  � p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   � ��� �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .� ��� l  � ��B���B  � � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   � ����   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .� ��� l  � ��A���A  � � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   � ���v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .� ��� l  � ��@�?�>�@  �?  �>  � ��� l  � ��=�<�;�=  �<  �;  � ��:� Z   �x����9� l  � ���8�7� =   � ���� o   � ��6�6 0 initialbackup initialBackup� m   � ��5
�5 boovtrue�8  �7  � k   ���� ��� r   � ���� n   � ���� 1   � ��4
�4 
time� l  � ���3�2� I  � ��1�0�/
�1 .misccurdldt    ��� null�0  �/  �3  �2  � o      �.�. 0 	starttime 	startTime� ��� Z   ����-�,� l  � ���+�*� E   � ���� o   � ��)�) &0 displaysleepstate displaySleepState� o   � ��(�( 0 strawake strAwake�+  �*  � Z   ����'�&� l  � ���%�$� =   � ���� o   � ��#�# 0 notifications  � m   � ��"
�" boovtrue�%  �$  � Z   � �����!� l  � ��� �� =   � ���� o   � ��� 0 nsfw  � m   � ��
� boovtrue�   �  � I  � ����
� .sysonotfnull��� ��� TEXT� m   � ��� ��� � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .� ���
� 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�  � ��� l  � ����� =   � ���� o   � ��� 0 nsfw  � m   � ��
� boovfals�  �  � ��� I  � ����
� .sysonotfnull��� ��� TEXT� m   � ��� ��� \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e� ���
� 
appr� m   � ��� ��� 
 W B T F U�  �  �!  �'  �&  �-  �,  � ��� l ����  �  �  � ��� l ����  �  �  � ��� l ��
�	�  �
  �	  � ��� l ����  �  �  � ��� r  ��� c  ��� l ���� b  ��� b  ��� b  ��� b  ��� b  ��� o  	�� "0 destinationdisk destinationDisk� m  	�� ���  B a c k u p s /� o  �� 0 machinefolder machineFolder� m  �� ���  / L a t e s t /� o  �� 0 
foldername 
folderName� m  �� ���  /�  �  � m  � 
�  
TEXT� o      ���� 0 d  � ��� l   ������  � A ;do shell script "ditto '" & sourceFolder & "' '" & d & "/'"   � ��� v d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "� ��� l   ��������  ��  ��  � ��� Q   A���� k  #8�� � � l ##����  ��do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings' --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' '" & sourceFolder & "' '" & d & "/'"    �< d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "   l ##��������  ��  ��    l ##��	��     optimised(?)   	 �

    o p t i m i s e d ( ? )  I #6����
�� .sysoexecTEXT���     TEXT b  #2 b  #. b  #, b  #( m  #& �	� r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   ' o  &'���� 0 sourcefolder sourceFolder m  (+ �  '   ' o  ,-���� 0 d   m  .1 �  / '��    l 77��������  ��  ��   �� l 77�� ��   U Odo shell script "rsync -aqvz --cvs-exclude '" & sourceFolder & "' '" & d & "/'"     �!! � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "��  � R      ������
�� .ascrerr ****      � ****��  ��  � l @@��������  ��  ��  � "#" l BB��������  ��  ��  # $%$ l BB��������  ��  ��  % &'& l BB��������  ��  ��  ' ()( l BB��������  ��  ��  ) *+* l BB��������  ��  ��  + ,-, r  BI./. m  BC��
�� boovfals/ o      ���� 0 isbackingup isBackingUp- 010 r  JQ232 m  JK��
�� boovfals3 o      ���� 0 manualbackup manualBackup1 454 l RR��������  ��  ��  5 676 Z  R�89����8 l R]:����: E  R];<; o  RW���� &0 displaysleepstate displaySleepState< o  W\���� 0 strawake strAwake��  ��  9 k  `�== >?> r  `m@A@ n  `iBCB 1  ei��
�� 
timeC l `eD����D I `e������
�� .misccurdldt    ��� null��  ��  ��  ��  A o      ���� 0 endtime endTime? EFE r  nuGHG n nsIJI I  os�������� 0 getduration getDuration��  ��  J  f  noH o      ���� 0 duration  F KLK r  v�MNM n  vOPO 3  {��
�� 
cobjP o  v{���� $0 messagescomplete messagesCompleteN o      ���� 0 msg  L QRQ Z  ��ST����S l ��U����U =  ��VWV o  ������ 0 notifications  W m  ����
�� boovtrue��  ��  T Z  ��XYZ��X l ��[����[ =  ��\]\ o  ������ 0 nsfw  ] m  ����
�� boovtrue��  ��  Y k  ��^^ _`_ I ����ab
�� .sysonotfnull��� ��� TEXTa b  ��cdc b  ��efe b  ��ghg m  ��ii �jj 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  h o  ������ 0 duration  f m  ��kk �ll    m i n u t e s ! 
d o  ������ 0 msg  b ��m��
�� 
apprm m  ��nn �oo : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  ` p��p I ����q��
�� .sysodelanull��� ��� nmbrq m  ������ ��  ��  Z rsr l ��t����t =  ��uvu o  ������ 0 nsfw  v m  ����
�� boovfals��  ��  s w��w k  ��xx yzy I ����{|
�� .sysonotfnull��� ��� TEXT{ b  ��}~} b  ��� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ������ 0 duration  ~ m  ���� ���    m i n u t e s ! 
| �����
�� 
appr� m  ���� ���  B a c k e d   u p !��  z ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  ��  ��  ��  R ��� l ����������  ��  ��  � ���� n ����� I  ���������� $0 resetmenubaricon resetMenuBarIcon��  ��  �  f  ����  ��  ��  7 ��� l ����������  ��  ��  � ���� Z  ��������� l �������� =  ����� o  ������  0 backupthenquit backupThenQuit� m  ����
�� boovtrue��  ��  � I �������
�� .aevtquitnull��� ��� null� m  ����                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ��  ��  � ��� l 	������ =  	��� o  ���� 0 initialbackup initialBackup� m  ��
�� boovfals��  ��  � ���� k  t�� ��� r  +��� c  )��� l '������ b  '��� b  #��� b  !��� b  ��� b  ��� b  ��� b  ��� o  ���� "0 destinationdisk destinationDisk� m  �� ���  B a c k u p s /� o  ���� 0 machinefolder machineFolder� m  �� ���  /� o  �� 0 t  � m   �� ���  /� o  !"�~�~ 0 
foldername 
folderName� m  #&�� ���  /��  ��  � m  '(�}
�} 
TEXT� o      �|�| 0 c  � ��� r  ,E��� c  ,C��� l ,A��{�z� b  ,A��� b  ,=��� b  ,;��� b  ,7��� b  ,3��� o  ,/�y�y "0 destinationdisk destinationDisk� m  /2�� ���  B a c k u p s /� o  36�x�x 0 machinefolder machineFolder� m  7:�� ���  / L a t e s t /� o  ;<�w�w 0 
foldername 
folderName� m  =@�� ���  /�{  �z  � m  AB�v
�v 
TEXT� o      �u�u 0 d  � ��� I FO�t��s
�t .ascrcmnt****      � ****� l FK��r�q� b  FK��� m  FI�� ���  b a c k i n g   u p   t o :  � o  IJ�p�p 0 d  �r  �q  �s  � ��� l PP�o�n�m�o  �n  �m  � ��� Z  P����l�k� l P[��j�i� E  P[��� o  PU�h�h &0 displaysleepstate displaySleepState� o  UZ�g�g 0 strawake strAwake�j  �i  � k  ^��� ��� r  ^k��� n  ^g��� 1  cg�f
�f 
time� l ^c��e�d� I ^c�c�b�a
�c .misccurdldt    ��� null�b  �a  �e  �d  � o      �`�` 0 	starttime 	startTime� ��� r  lw��� n  lu��� 3  qu�_
�_ 
cobj� o  lq�^�^ *0 messagesencouraging messagesEncouraging� o      �]�] 0 msg  � ��\� Z  x����[�Z� l x��Y�X� =  x��� o  x}�W�W 0 notifications  � m  }~�V
�V boovtrue�Y  �X  � Z  ������U� l ����T�S� =  ����� o  ���R�R 0 nsfw  � m  ���Q
�Q boovtrue�T  �S  � k  ���� 	 		  I ���P		
�P .sysonotfnull��� ��� TEXT	 o  ���O�O 0 msg  	 �N	�M
�N 
appr	 m  ��		 �		 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�M  	 	�L	 I ���K	�J
�K .sysodelanull��� ��� nmbr	 m  ���I�I �J  �L  � 			
		 l ��	�H�G	 =  ��			 o  ���F�F 0 nsfw  	 m  ���E
�E boovfals�H  �G  	
 	�D	 k  ��		 			 I ���C		
�C .sysonotfnull��� ��� TEXT	 m  ��		 �		  B a c k i n g   u p	 �B	�A
�B 
appr	 m  ��		 �		 
 W B T F U�A  	 	�@	 I ���?	�>
�? .sysodelanull��� ��� nmbr	 m  ���=�= �>  �@  �D  �U  �[  �Z  �\  �l  �k  � 			 l ���<�;�:�<  �;  �:  	 			 l ���9�8�7�9  �8  �7  	 		 	 l ���6�5�4�6  �5  �4  	  	!	"	! l ���3�2�1�3  �2  �1  	" 	#	$	# l ���0�/�.�0  �/  �.  	$ 	%	&	% Q  ��	'	(	)	' k  ��	*	* 	+	,	+ l ���-	-	.�-  	-��do shell script "rsync -avz --cvs-exclude --include='com.google.Chrome.savedState/' --exclude='com.apple.loginwindow.plist' --exclude='com.apple.finder.plist.*' --exclude='*.plist.*' --exclude='com.apple.NetInfoManager.plist' --exclude='.localized' --exclude='.FBC' --exclude='.DS' --exclude='com.apple.nsurlsessiond/' --exclude='Logs/' --exclude='saved-telemetry-pings'  --exclude='Session Storage/' --exclude='Cache/' --exclude='Caches/' --exclude='com.apple.finder.savedState/' --exclude='Saved Application State' --exclude='Mobile Documents/' --exclude='Mobile Documents.*' --exclude='.webtmp' --exclude='*.waf' --exclude='.Trash' --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	. �	/	/� d o   s h e l l   s c r i p t   " r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '     - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "	, 	0	1	0 l ���,�+�*�,  �+  �*  	1 	2	3	2 l ���)	4	5�)  	4   optimised(?)   	5 �	6	6    o p t i m i s e d ( ? )	3 	7	8	7 I ���(	9�'
�( .sysoexecTEXT���     TEXT	9 b  ��	:	;	: b  ��	<	=	< b  ��	>	?	> b  ��	@	A	@ b  ��	B	C	B b  ��	D	E	D m  ��	F	F �	G	G
 r s y n c   - a q v z   - - c v s - e x c l u d e   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' . T r a s h '   - - e x c l u d e = ' . T r a s h * '   - - e x c l u d e = ' . T r a s h e s '   - - e x c l u d e = ' / B a c k u p s . b a c k u p d b '   - - e x c l u d e = ' / . M o b i l e B a c k u p s '   - - e x c l u d e = ' / . M o b i l e B a c k u p s . t r a s h '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' $ R E C Y C L E . B I N '   - - e x c l u d e = ' $ R e c y c l e . B i n '   - - e x c l u d e = ' . A p p l e D B '   - - e x c l u d e = ' . A p p l e D e s k t o p '   - - e x c l u d e = ' . A p p l e D o u b l e '   - - e x c l u d e = ' . c o m . a p p l e . t i m e m a c h i n e . s u p p o r t e d '   - - e x c l u d e = ' . d b f s e v e n t s d '   - - e x c l u d e = ' . D o c u m e n t R e v i s i o n s - V 1 0 0 * '   - - e x c l u d e = ' . D S _ S t o r e '   - - e x c l u d e = ' . f s e v e n t s d '   - - e x c l u d e = ' . P K I n s t a l l S a n d b o x M a n a g e r '   - - e x c l u d e = ' . S p o t l i g h t * '   - - e x c l u d e = ' . S y m A V * '   - - e x c l u d e = ' . s y m S c h e d S c a n L o c k x z '   - - e x c l u d e = ' . T e m p o r a r y I t e m s '   - - e x c l u d e = ' . v o l '   - - e x c l u d e = ' . V o l u m e I c o n . i c n s '   - - e x c l u d e = ' D e s k t o p   D B '   - - e x c l u d e = ' D e s k t o p   D F '   - - e x c l u d e = ' h i b e r f i l . s y s '   - - e x c l u d e = ' l o s t + f o u n d '   - - e x c l u d e = ' N e t w o r k   T r a s h   F o l d e r '   - - e x c l u d e = ' p a g e f i l e . s y s '   - - e x c l u d e = ' R e c y c l e d '   - - e x c l u d e = ' R E C Y C L E R '   - - e x c l u d e = ' S y s t e m   V o l u m e   I n f o r m a t i o n '   - - e x c l u d e = ' T e m p o r a r y   I t e m s '   - - e x c l u d e = ' T h u m b s . d b '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' . w e b t m p '   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '	E o  ���&�& 0 c  	C m  ��	H	H �	I	I  '   '	A o  ���%�% 0 sourcefolder sourceFolder	? m  ��	J	J �	K	K  '   '	= o  ���$�$ 0 d  	; m  ��	L	L �	M	M  / '�'  	8 	N	O	N l ���#�"�!�#  �"  �!  	O 	P� 	P l ���	Q	R�  	Q � zdo shell script "rsync -aqvz --cvs-exclude --delete --backup --backup-dir='" & c & "' '" & sourceFolder & "' '" & d & "/'"   	R �	S	S � d o   s h e l l   s c r i p t   " r s y n c   - a q v z   - - c v s - e x c l u d e   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' "   &   c   &   " '   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' "�   	( R      ���
� .ascrerr ****      � ****�  �  	) l ������  �  �  	& 	T	U	T l ������  �  �  	U 	V	W	V l ������  �  �  	W 	X	Y	X l ������  �  �  	Y 	Z	[	Z l ������  �  �  	[ 	\	]	\ l �����
�  �  �
  	] 	^	_	^ r  ��	`	a	` m  ���	
�	 boovfals	a o      �� 0 isbackingup isBackingUp	_ 	b	c	b r  �	d	e	d m  ���
� boovfals	e o      �� 0 manualbackup manualBackup	c 	f	g	f l ����  �  �  	g 	h�	h Z  t	i	j�	k	i = 	l	m	l n  	n	o	n 2 
� 
�  
cobj	o l 
	p����	p c  
	q	r	q 4  ��	s
�� 
psxf	s o  ���� 0 c  	r m  	��
�� 
alis��  ��  	m J  ����  	j k  j	t	t 	u	v	u r  	w	x	w c  	y	z	y n  	{	|	{ 4  ��	}
�� 
cfol	} o  ���� 0 t  	| o  ���� 0 backupfolder backupFolder	z m  ��
�� 
TEXT	x o      ���� 0 oldestfolder oldestFolder	v 	~		~ r  &	�	�	� c  $	�	�	� n  "	�	�	� 1  "��
�� 
psxp	� o  ���� 0 oldestfolder oldestFolder	� m  "#��
�� 
TEXT	� o      ���� 0 oldestfolder oldestFolder	 	�	�	� I '0��	���
�� .ascrcmnt****      � ****	� l ',	�����	� b  ',	�	�	� m  '*	�	� �	�	�  t o   d e l e t e :  	� o  *+���� 0 oldestfolder oldestFolder��  ��  ��  	� 	�	�	� l 11��������  ��  ��  	� 	�	�	� r  1<	�	�	� b  1:	�	�	� b  16	�	�	� m  14	�	� �	�	�  r m   - r f   '	� o  45���� 0 oldestfolder oldestFolder	� m  69	�	� �	�	�  '	� o      ���� 0 todelete toDelete	� 	�	�	� I =B��	���
�� .sysoexecTEXT���     TEXT	� o  =>���� 0 todelete toDelete��  	� 	�	�	� l CC��������  ��  ��  	� 	���	� Z  Cj	�	�����	� l CN	�����	� E  CN	�	�	� o  CH���� &0 displaysleepstate displaySleepState	� o  HM���� 0 strawake strAwake��  ��  	� k  Qf	�	� 	�	�	� r  Q^	�	�	� n  QZ	�	�	� 1  VZ��
�� 
time	� l QV	�����	� I QV������
�� .misccurdldt    ��� null��  ��  ��  ��  	� o      ���� 0 endtime endTime	� 	�	�	� r  _f	�	�	� n _d	�	�	� I  `d�������� 0 getduration getDuration��  ��  	�  f  _`	� o      ���� 0 duration  	� 	�	�	� l gg��������  ��  ��  	� 	�	�	� r  gr	�	�	� n  gp	�	�	� 3  lp��
�� 
cobj	� o  gl���� $0 messagescomplete messagesComplete	� o      ���� 0 msg  	� 	�	�	� Z  s`	�	�����	� l sz	�����	� =  sz	�	�	� o  sx���� 0 notifications  	� m  xy��
�� boovtrue��  ��  	� k  }\	�	� 	�	�	� Z  }&	�	���	�	� l }�	�����	� =  }�	�	�	� o  }~���� 0 duration  	� m  ~�	�	� ?�      ��  ��  	� Z  ��	�	�	���	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� o  ������ 0 msg  	� ��	���
�� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  	� 	�	�	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovfals��  ��  	� 	���	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� ��	���
�� 
appr	� m  ��	�	� �	�	�  B a c k e d   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  ��  ��  ��  	� Z  �&
 

��
  l ��
����
 =  ��


 o  ������ 0 nsfw  
 m  ����
�� boovtrue��  ��  
 k  ��

 


 I ����
	


�� .sysonotfnull��� ��� TEXT
	 b  ��


 b  ��


 b  ��


 m  ��

 �

  A n d   i n  
 o  ������ 0 duration  
 m  ��

 �

    m i n u t e s ! 

 o  ������ 0 msg  

 ��
��
�� 
appr
 m  ��

 �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  
 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  
 


 l �
����
 =  �


 o  ����� 0 nsfw  
 m  ��
�� boovfals��  ��  
 
��
 k  	"
 
  
!
"
! I 	��
#
$
�� .sysonotfnull��� ��� TEXT
# b  	
%
&
% b  	
'
(
' m  	
)
) �
*
*  A n d   i n  
( o  ���� 0 duration  
& m  
+
+ �
,
,    m i n u t e s ! 

$ ��
-��
�� 
appr
- m  
.
. �
/
/  B a c k e d   u p !��  
" 
0��
0 I "��
1��
�� .sysodelanull��� ��� nmbr
1 m  ���� ��  ��  ��  ��  	� 
2
3
2 l ''��������  ��  ��  
3 
4��
4 Z  '\
5
6
7��
5 l '.
8����
8 =  '.
9
:
9 o  ',���� 0 nsfw  
: m  ,-��
�� boovtrue��  ��  
6 I 1>�
;
<
� .sysonotfnull��� ��� TEXT
; m  14
=
= �
>
> � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .
< �~
?�}
�~ 
appr
? m  7:
@
@ �
A
A @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r�}  
7 
B
C
B l AH
D�|�{
D =  AH
E
F
E o  AF�z�z 0 nsfw  
F m  FG�y
�y boovfals�|  �{  
C 
G�x
G I KX�w
H
I
�w .sysonotfnull��� ��� TEXT
H m  KN
J
J �
K
K p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d
I �v
L�u
�v 
appr
L m  QT
M
M �
N
N 2 D e l e t e d   n e w   b a c k u p   f o l d e r�u  �x  ��  ��  ��  ��  	� 
O
P
O l aa�t�s�r�t  �s  �r  
P 
Q�q
Q n af
R
S
R I  bf�p�o�n�p $0 resetmenubaricon resetMenuBarIcon�o  �n  
S  f  ab�q  ��  ��  ��  �  	k k  mt
T
T 
U
V
U Z  mX
W
X�m�l
W l mx
Y�k�j
Y E  mx
Z
[
Z o  mr�i�i &0 displaysleepstate displaySleepState
[ o  rw�h�h 0 strawake strAwake�k  �j  
X k  {T
\
\ 
]
^
] r  {�
_
`
_ n  {�
a
b
a 1  ���g
�g 
time
b l {�
c�f�e
c I {��d�c�b
�d .misccurdldt    ��� null�c  �b  �f  �e  
` o      �a�a 0 endtime endTime
^ 
d
e
d r  ��
f
g
f n ��
h
i
h I  ���`�_�^�` 0 getduration getDuration�_  �^  
i  f  ��
g o      �]�] 0 duration  
e 
j
k
j r  ��
l
m
l n  ��
n
o
n 3  ���\
�\ 
cobj
o o  ���[�[ $0 messagescomplete messagesComplete
m o      �Z�Z 0 msg  
k 
p�Y
p Z  �T
q
r�X�W
q l ��
s�V�U
s =  ��
t
u
t o  ���T�T 0 notifications  
u m  ���S
�S boovtrue�V  �U  
r Z  �P
v
w�R
x
v l ��
y�Q�P
y =  ��
z
{
z o  ���O�O 0 duration  
{ m  ��
|
| ?�      �Q  �P  
w Z  ��
}
~
�N
} l ��
��M�L
� =  ��
�
�
� o  ���K�K 0 nsfw  
� m  ���J
�J boovtrue�M  �L  
~ k  ��
�
� 
�
�
� I ���I
�
�
�I .sysonotfnull��� ��� TEXT
� b  ��
�
�
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  A n d   i n  
� o  ���H�H 0 duration  
� m  ��
�
� �
�
�    m i n u t e ! 

� o  ���G�G 0 msg  
� �F
��E
�F 
appr
� m  ��
�
� �
�
� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�E  
� 
��D
� I ���C
��B
�C .sysodelanull��� ��� nmbr
� m  ���A�A �B  �D  
 
�
�
� l ��
��@�?
� =  ��
�
�
� o  ���>�> 0 nsfw  
� m  ���=
�= boovfals�@  �?  
� 
��<
� k  ��
�
� 
�
�
� I ���;
�
�
�; .sysonotfnull��� ��� TEXT
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  A n d   i n  
� o  ���:�: 0 duration  
� m  ��
�
� �
�
�    m i n u t e ! 

� �9
��8
�9 
appr
� m  ��
�
� �
�
�  B a c k e d   u p !�8  
� 
��7
� I ���6
��5
�6 .sysodelanull��� ��� nmbr
� m  ���4�4 �5  �7  �<  �N  �R  
x Z  P
�
�
��3
� l 
��2�1
� =  
�
�
� o  �0�0 0 nsfw  
� m  �/
�/ boovtrue�2  �1  
� k  &
�
� 
�
�
� I  �.
�
�
�. .sysonotfnull��� ��� TEXT
� b  
�
�
� b  
�
�
� b  
�
�
� m  
�
� �
�
�  A n d   i n  
� o  �-�- 0 duration  
� m  
�
� �
�
�    m i n u t e s ! 

� o  �,�, 0 msg  
� �+
��*
�+ 
appr
� m  
�
� �
�
� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�*  
� 
��)
� I !&�(
��'
�( .sysodelanull��� ��� nmbr
� m  !"�&�& �'  �)  
� 
�
�
� l )0
��%�$
� =  )0
�
�
� o  ).�#�# 0 nsfw  
� m  ./�"
�" boovfals�%  �$  
� 
��!
� k  3L
�
� 
�
�
� I 3F� 
�
�
�  .sysonotfnull��� ��� TEXT
� b  3<
�
�
� b  38
�
�
� m  36
�
� �
�
�  A n d   i n  
� o  67�� 0 duration  
� m  8;
�
� �
�
�    m i n u t e s ! 

� �
��
� 
appr
� m  ?B
�
� �
�
�  B a c k e d   u p !�  
� 
��
� I GL�
��
� .sysodelanull��� ��� nmbr
� m  GH�� �  �  �!  �3  �X  �W  �Y  �m  �l  
V 
�
�
� l YY����  �  �  
� 
�
�
� n Y^
�
�
� I  Z^���� $0 resetmenubaricon resetMenuBarIcon�  �  
�  f  YZ
� 
�
�
� l __����  �  �  
� 
��
� Z  _t
�
���
� l _f
���
� =  _f
�
�
� o  _d�
�
  0 backupthenquit backupThenQuit
� m  de�	
�	 boovtrue�  �  
� I ip�
��
� .aevtquitnull��� ��� null
� m  il
�
�                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �  �  �  �  �  ��  �9  �:  � m   $ %
�
��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 
�
�
� l zz����  �  �  
� 
��
� I  z��
��� 0 	stopwatch  
� 
�� 
� m  {~
�
� �
�
�  f i n i s h�   �  �  � 
�
�
� l     ��������  ��  ��  
� 
�
�
� l     ��
�
���  
� � �-----------------------------------------------------------------------------------------------------------------------------------------------   
� �
�
� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
�    l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��������  ��  ��    l     ��������  ��  ��   	
	 l     ��������  ��  ��  
  l     ��������  ��  ��    l     ��������  ��  ��    l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����   � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------    � - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��������  ��  ��    !  l     ��"#��  " � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   # �$$   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .! %&% l     ��'(��  ' � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   ( �))�   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .& *+* i  il,-, I      ��.���� 0 itexists itExists. /0/ o      ���� 0 
objecttype 
objectType0 1��1 o      ���� 
0 object  ��  ��  - l    W2342 O     W565 Z    V789��7 l   :����: =    ;<; o    ���� 0 
objecttype 
objectType< m    == �>>  d i s k��  ��  8 Z   
 ?@��A? I  
 ��B��
�� .coredoexnull���     ****B 4   
 ��C
�� 
cdisC o    ���� 
0 object  ��  @ L    DD m    ��
�� boovtrue��  A L    EE m    ��
�� boovfals9 FGF l   "H����H =    "IJI o     ���� 0 
objecttype 
objectTypeJ m     !KK �LL  f i l e��  ��  G MNM Z   % 7OP��QO I  % -��R��
�� .coredoexnull���     ****R 4   % )��S
�� 
fileS o   ' (���� 
0 object  ��  P L   0 2TT m   0 1��
�� boovtrue��  Q L   5 7UU m   5 6��
�� boovfalsN VWV l  : =X����X =   : =YZY o   : ;���� 0 
objecttype 
objectTypeZ m   ; <[[ �\\  f o l d e r��  ��  W ]��] Z   @ R^_��`^ I  @ H��a��
�� .coredoexnull���     ****a 4   @ D��b
�� 
cfolb o   B C���� 
0 object  ��  _ L   K Mcc m   K L��
�� boovtrue��  ` L   P Rdd m   P Q��
�� boovfals��  ��  6 m     ee�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  3 "  (string, string) as Boolean   4 �ff 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n+ ghg l     ��������  ��  ��  h iji l     ��������  ��  ��  j klk l     ��������  ��  ��  l mnm l     ��������  ��  ��  n opo l     ��������  ��  ��  p qrq l     ��st��  s � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   t �uuZ   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .r vwv l     ��xy��  x P J This means that backups can be identified from what machine they're from.   y �zz �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .w {|{ i  mp}~} I      �������� .0 getcomputeridentifier getComputerIdentifier��  ��  ~ k      ��� r     	��� n     ��� 1    ��
�� 
sicn� l    ������ I    ������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 computername computerName� ��� r   
 ��� I  
 �����
�� .sysoexecTEXT���     TEXT� m   
 �� ��� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  ��  � o      ���� "0 strserialnumber strSerialNumber� ��� r    ��� l   ������ b    ��� b    ��� o    ���� 0 computername computerName� m    �� ���    -  � o    ���� "0 strserialnumber strSerialNumber��  ��  � o      ����  0 identifiername identifierName� ���� L    �� o    ����  0 identifiername identifierName��  | ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   � ���   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .� ��� l     ������  � � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   � ���   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .� ��� i  qt��� I      ������� 0 gettimestamp getTimestamp� ���� o      ���� 0 isfolder isFolder��  ��  � l   ����� k    ��� ��� l     ������  �   Date variables   � ���    D a t e   v a r i a b l e s� ��� r     )��� l     ������ I     �����
�� .misccurdldt    ��� null��  �  ��  ��  � K    �� �~��
�~ 
year� o    �}�} 0 y  � �|��
�| 
mnth� o    �{�{ 0 m  � �z��
�z 
day � o    �y�y 0 d  � �x��w
�x 
time� o   	 
�v�v 0 t  �w  � ��� r   * 1��� c   * /��� l  * -��u�t� c   * -��� o   * +�s�s 0 y  � m   + ,�r
�r 
long�u  �t  � m   - .�q
�q 
TEXT� o      �p�p 0 ty tY� ��� r   2 9��� c   2 7��� l  2 5��o�n� c   2 5��� o   2 3�m�m 0 y  � m   3 4�l
�l 
long�o  �n  � m   5 6�k
�k 
TEXT� o      �j�j 0 ty tY� ��� r   : A��� c   : ?��� l  : =��i�h� c   : =��� o   : ;�g�g 0 m  � m   ; <�f
�f 
long�i  �h  � m   = >�e
�e 
TEXT� o      �d�d 0 tm tM� ��� r   B I��� c   B G��� l  B E��c�b� c   B E��� o   B C�a�a 0 d  � m   C D�`
�` 
long�c  �b  � m   E F�_
�_ 
TEXT� o      �^�^ 0 td tD� ��� r   J Q��� c   J O��� l  J M��]�\� c   J M��� o   J K�[�[ 0 t  � m   K L�Z
�Z 
long�]  �\  � m   M N�Y
�Y 
TEXT� o      �X�X 0 tt tT� ��� l  R R�W�V�U�W  �V  �U  � ��� l  R R�T���T  � U O Append the month or day with a 0 if the string length is only 1 character long   � ��� �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g� � � r   R [ c   R Y l  R W�S�R I  R W�Q�P
�Q .corecnte****       **** o   R S�O�O 0 tm tM�P  �S  �R   m   W X�N
�N 
nmbr o      �M�M 
0 tml tML   r   \ e	
	 c   \ c l  \ a�L�K I  \ a�J�I
�J .corecnte****       **** o   \ ]�H�H 0 tm tM�I  �L  �K   m   a b�G
�G 
nmbr
 o      �F�F 
0 tdl tDL  l  f f�E�D�C�E  �D  �C    Z   f u�B�A l  f i�@�? =   f i o   f g�>�> 
0 tml tML m   g h�=�= �@  �?   r   l q b   l o m   l m �  0 o   m n�<�< 0 tm tM o      �;�; 0 tm tM�B  �A    l  v v�:�9�8�:  �9  �8    !  Z   v �"#�7�6" l  v y$�5�4$ =   v y%&% o   v w�3�3 
0 tdl tDL& m   w x�2�2 �5  �4  # r   | �'(' b   | �)*) m   | ++ �,,  0* o    ��1�1 0 td tD( o      �0�0 0 td tD�7  �6  ! -.- l  � ��/�.�-�/  �.  �-  . /0/ l  � ��,�+�*�,  �+  �*  0 121 l  � ��)34�)  3   Time variables	   4 �55     T i m e   v a r i a b l e s 	2 676 l  � ��(89�(  8  	 Get hour   9 �::    G e t   h o u r7 ;<; r   � �=>= n   � �?@? 1   � ��'
�' 
tstr@ l  � �A�&�%A I  � ��$�#�"
�$ .misccurdldt    ��� null�#  �"  �&  �%  > o      �!�! 0 timestr timeStr< BCB r   � �DED I  � �F� GF z��
� .sysooffslong    ��� null
� misccura�   G �HI
� 
psofH m   � �JJ �KK  :I �L�
� 
psinL o   � ��� 0 timestr timeStr�  E o      �� 0 pos  C MNM r   � �OPO c   � �QRQ n   � �STS 7  � ��UV
� 
cha U m   � ��� V l  � �W��W \   � �XYX o   � ��� 0 pos  Y m   � ��� �  �  T o   � ��� 0 timestr timeStrR m   � ��
� 
TEXTP o      �� 0 h  N Z[Z r   � �\]\ c   � �^_^ n   � �`a` 7 � ��bc
� 
cha b l  � �d��d [   � �efe o   � ��� 0 pos  f m   � ��� �  �  c  ;   � �a o   � ��
�
 0 timestr timeStr_ m   � ��	
�	 
TEXT] o      �� 0 timestr timeStr[ ghg l  � �����  �  �  h iji l  � ��kl�  k   Get minute   l �mm    G e t   m i n u t ej non r   � �pqp I  � �r�sr z��
� .sysooffslong    ��� null
� misccura�  s � tu
�  
psoft m   � �vv �ww  :u ��x��
�� 
psinx o   � ����� 0 timestr timeStr��  q o      ���� 0 pos  o yzy r   �{|{ c   �}~} n   � � 7  � ����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr~ m   ��
�� 
TEXT| o      ���� 0 m  z ��� r  ��� c  ��� n  ��� 7����
�� 
cha � l ������ [  ��� o  ���� 0 pos  � m  ���� ��  ��  �  ;  � o  ���� 0 timestr timeStr� m  ��
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l ��������  ��  ��  � ��� l ������  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  2��� I 0����� z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m  "%�� ���   � �����
�� 
psin� o  ()���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  3E��� c  3C��� n  3A��� 74A����
�� 
cha � l :>������ [  :>��� o  ;<���� 0 pos  � m  <=���� ��  ��  �  ;  ?@� o  34���� 0 timestr timeStr� m  AB��
�� 
TEXT� o      ���� 0 s  � ��� l FF��������  ��  ��  � ��� l FF��������  ��  ��  � ��� Z  F������ l FI������ =  FI��� o  FG���� 0 isfolder isFolder� m  GH��
�� boovtrue��  ��  � k  La�� ��� l LL������  �   Create folder timestamp   � ��� 0   C r e a t e   f o l d e r   t i m e s t a m p� ���� r  La��� b  L_��� b  L]��� b  L[��� b  LY��� b  LU��� b  LS��� b  LQ��� m  LO�� ���  b a c k u p _� o  OP���� 0 ty tY� o  QR���� 0 tm tM� o  ST���� 0 td tD� m  UX�� ���  _� o  YZ���� 0 h  � o  [\���� 0 m  � o  ]^���� 0 s  � o      ���� 0 	timestamp  ��  � ��� l dg������ =  dg��� o  de���� 0 isfolder isFolder� m  ef��
�� boovfals��  ��  � ���� k  j{�� ��� l jj������  �   Create timestamp   � ��� "   C r e a t e   t i m e s t a m p� ���� r  j{��� b  jy��� b  jw��� b  ju��� b  js��� b  jo��� b  jm��� o  jk���� 0 ty tY� o  kl���� 0 tm tM� o  mn���� 0 td tD� m  or�� ���  _� o  st���� 0 h  � o  uv���� 0 m  � o  wx���� 0 s  � o      ���� 0 	timestamp  ��  ��  ��  � ��� l ����������  ��  ��  � ���� L  ���� o  ������ 0 	timestamp  ��  �  	(boolean)   � ���  ( b o o l e a n )� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � � � l     ��������  ��  ��     l     ��������  ��  ��    l     ��������  ��  ��    l     ����   q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.    �		 �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e . 

 l     ����   w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.    � �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .  i  ux I      ������ 0 comparesizes compareSizes  o      ���� 
0 source   �� o      ���� 0 destination  ��  ��   l   � k    �  r      m     ��
�� boovtrue o      ���� 0 fit     r    
!"! 4    ��#
�� 
psxf# o    ���� 0 destination  " o      ���� 0 destination    $%$ l   ��������  ��  ��  % &'& r    ()( b    *+* b    ,-, b    ./. o    ���� "0 destinationdisk destinationDisk/ m    00 �11  B a c k u p s /- o    ���� 0 machinefolder machineFolder+ m    22 �33  / L a t e s t) o      ���� 0 
templatest 
tempLatest' 454 r    676 m    ����  7 o      ���� $0 latestfoldersize latestFolderSize5 898 l   ��������  ��  ��  9 :;: r    &<=< b    $>?> n   "@A@ 1     "��
�� 
psxpA l    B����B I    ��CD
�� .earsffdralis        afdrC m    ��
�� afdrdlibD ��E��
�� 
fromE m    ��
�� fldmfldu��  ��  ��  ? m   " #FF �GG  C a c h e s= o      ���� 0 c  ; HIH r   ' 4JKJ b   ' 2LML n  ' 0NON 1   . 0��
�� 
psxpO l  ' .P��~P I  ' .�}QR
�} .earsffdralis        afdrQ m   ' (�|
�| afdrdlibR �{S�z
�{ 
fromS m   ) *�y
�y fldmfldu�z  �  �~  M m   0 1TT �UU  L o g sK o      �x�x 0 l  I VWV r   5 BXYX b   5 @Z[Z n  5 >\]\ 1   < >�w
�w 
psxp] l  5 <^�v�u^ I  5 <�t_`
�t .earsffdralis        afdr_ m   5 6�s
�s afdrdlib` �ra�q
�r 
froma m   7 8�p
�p fldmfldu�q  �v  �u  [ m   > ?bb �cc   M o b i l e   D o c u m e n t sY o      �o�o 0 md  W ded r   C Ffgf m   C D�n�n  g o      �m�m 0 	cachesize 	cacheSizee hih r   G Jjkj m   G H�l�l  k o      �k�k 0 logssize logsSizei lml r   K Nnon m   K L�j�j  o o      �i�i 0 mdsize mdSizem pqp l  O O�h�g�f�h  �g  �f  q rsr r   O Rtut m   O Pvv ?�      u o      �e�e 
0 buffer  s wxw l  S S�d�c�b�d  �c  �b  x yzy r   S V{|{ m   S T�a�a  | o      �`�` (0 adjustedfoldersize adjustedFolderSizez }~} l  W W�_�^�]�_  �^  �]  ~ � O   W���� k   [��� ��� r   [ h��� I  [ f�\��[
�\ .sysoexecTEXT���     TEXT� b   [ b��� b   [ ^��� m   [ \�� ���  d u   - m s   '� o   \ ]�Z�Z 
0 source  � m   ^ a�� ���  '   |   c u t   - f   1�[  � o      �Y�Y 0 
foldersize 
folderSize� ��� r   i ���� ^   i ���� l  i }��X�W� I  i }���V� z�U�T
�U .sysorondlong        doub
�T misccura� ]   o x��� l  o t��S�R� ^   o t��� o   o p�Q�Q 0 
foldersize 
folderSize� m   p s�P�P �S  �R  � m   t w�O�O d�V  �X  �W  � m   } ��N�N d� o      �M�M 0 
foldersize 
folderSize� ��� l  � ��L�K�J�L  �K  �J  � ��� r   � ���� ^   � ���� ^   � ���� ^   � ���� l  � ���I�H� l  � ���G�F� n   � ���� 1   � ��E
�E 
frsp� 4   � ��D�
�D 
cdis� o   � ��C�C 0 destination  �G  �F  �I  �H  � m   � ��B�B � m   � ��A�A � m   � ��@�@ � o      �?�? 0 	freespace 	freeSpace� ��� r   � ���� ^   � ���� l  � ���>�=� I  � ����<� z�;�:
�; .sysorondlong        doub
�: misccura� l  � ���9�8� ]   � ���� o   � ��7�7 0 	freespace 	freeSpace� m   � ��6�6 d�9  �8  �<  �>  �=  � m   � ��5�5 d� o      �4�4 0 	freespace 	freeSpace� ��� l  � ��3�2�1�3  �2  �1  � ��� r   � ���� I  � ��0��/
�0 .sysoexecTEXT���     TEXT� b   � ���� b   � ���� m   � ��� ���  d u   - m s   '� o   � ��.�. 0 c  � m   � ��� ���  '   |   c u t   - f   1�/  � o      �-�- 0 	cachesize 	cacheSize� ��� r   � ���� ^   � ���� l  � ���,�+� I  � ����*� z�)�(
�) .sysorondlong        doub
�( misccura� ]   � ���� l  � ���'�&� ^   � ���� o   � ��%�% 0 	cachesize 	cacheSize� m   � ��$�$ �'  �&  � m   � ��#�# d�*  �,  �+  � m   � ��"�" d� o      �!�! 0 	cachesize 	cacheSize� ��� l  � �� ���   �  �  � ��� r   � ���� I  � ����
� .sysoexecTEXT���     TEXT� b   � ���� b   � ���� m   � ��� ���  d u   - m s   '� o   � ��� 0 l  � m   � ��� ���  '   |   c u t   - f   1�  � o      �� 0 logssize logsSize� ��� r   �	��� ^   ���� l  ����� I  ����� z��
� .sysorondlong        doub
� misccura� ]   � ���� l  � ����� ^   � ���� o   � ��� 0 logssize logsSize� m   � ��� �  �  � m   � ��� d�  �  �  � m  �� d� o      �� 0 logssize logsSize� ��� l 

����  �  �  � ��� r  
��� I 
�
��	
�
 .sysoexecTEXT���     TEXT� b  
��� b  
� � m  
 �  d u   - m s   '  o  �� 0 md  � m   �  '   |   c u t   - f   1�	  � o      �� 0 mdsize mdSize�  r  4 ^  2	
	 l .�� I .� z��
� .sysorondlong        doub
� misccura ]   ) l  %��  ^   % o   !���� 0 mdsize mdSize m  !$���� �  �    m  %(���� d�  �  �  
 m  .1���� d o      ���� 0 mdsize mdSize  l 55��������  ��  ��    r  5> l 5<���� \  5< \  5: \  58 o  56���� 0 
foldersize 
folderSize o  67���� 0 	cachesize 	cacheSize o  89���� 0 logssize logsSize o  :;���� 0 mdsize mdSize��  ��   o      ���� (0 adjustedfoldersize adjustedFolderSize  !  l ??��������  ��  ��  ! "#" I ?H��$��
�� .ascrcmnt****      � ****$ l ?D%����% b  ?D&'& b  ?B()( o  ?@���� 0 
foldersize 
folderSize) o  @A���� (0 adjustedfoldersize adjustedFolderSize' o  BC���� 0 	freespace 	freeSpace��  ��  ��  # *+* l II��������  ��  ��  + ,��, Z  I�-.����- l IP/����/ =  IP010 o  IN���� 0 initialbackup initialBackup1 m  NO��
�� boovfals��  ��  . k  S22 343 r  Sb565 I S`��7��
�� .sysoexecTEXT���     TEXT7 b  S\898 b  SX:;: m  SV<< �==  d u   - m s   '; o  VW���� 0 
templatest 
tempLatest9 m  X[>> �??  '   |   c u t   - f   1��  6 o      ���� $0 latestfoldersize latestFolderSize4 @A@ r  c}BCB ^  c{DED l cwF����F I cwGH��G z����
�� .sysorondlong        doub
�� misccuraH ]  irIJI l inK����K ^  inLML o  ij���� $0 latestfoldersize latestFolderSizeM m  jm���� ��  ��  J m  nq���� d��  ��  ��  E m  wz���� dC o      ���� $0 latestfoldersize latestFolderSizeA N��N l ~~��������  ��  ��  ��  ��  ��  ��  � m   W XOO�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � PQP l ����������  ��  ��  Q RSR Z  ��TUV��T l ��W����W =  ��XYX o  ������ 0 initialbackup initialBackupY m  ����
�� boovfals��  ��  U k  ��ZZ [\[ r  ��]^] \  ��_`_ o  ������ (0 adjustedfoldersize adjustedFolderSize` o  ������ $0 latestfoldersize latestFolderSize^ o      ���� 0 temp  \ aba l ����������  ��  ��  b cdc Z  ��ef����e l ��g����g A  ��hih o  ������ 0 temp  i m  ������  ��  ��  f r  ��jkj \  ��lml l ��n����n o  ������ 0 temp  ��  ��  m l ��o����o ]  ��pqp l ��r����r o  ������ 0 temp  ��  ��  q m  ������ ��  ��  k o      ���� 0 temp  ��  ��  d sts l ����������  ��  ��  t u��u Z  ��vw����v l ��x����x ?  ��yzy o  ������ 0 temp  z l ��{����{ \  ��|}| o  ������ 0 	freespace 	freeSpace} o  ������ 
0 buffer  ��  ��  ��  ��  w r  ��~~ m  ����
�� boovfals o      ���� 0 fit  ��  ��  ��  V ��� l �������� =  ����� o  ������ 0 initialbackup initialBackup� m  ����
�� boovtrue��  ��  � ���� Z  ��������� l �������� ?  ����� o  ������ (0 adjustedfoldersize adjustedFolderSize� l �������� \  ����� o  ������ 0 	freespace 	freeSpace� o  ������ 
0 buffer  ��  ��  ��  ��  � r  ����� m  ����
�� boovfals� o      ���� 0 fit  ��  ��  ��  ��  S ��� l ����������  ��  ��  � ���� L  ���� o  ������ 0 fit  ��    (string, string)    ���   ( s t r i n g ,   s t r i n g ) ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ����~��  �  �~  � ��� l     �}�|�{�}  �|  �{  � ��� l     �z�y�x�z  �y  �x  � ��� l     �w���w  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i  y|��� I      �v��u�v 0 	stopwatch  � ��t� o      �s�s 0 mode  �t  �u  � l    3���� k     3�� ��� q      �� �r�q�r 0 x  �q  � ��p� Z     3����o� l    ��n�m� =     ��� o     �l�l 0 mode  � m    �� ��� 
 s t a r t�n  �m  � k    �� ��� r    ��� I    �k��j�k 0 gettimestamp getTimestamp� ��i� m    �h
�h boovfals�i  �j  � o      �g�g 0 x  � ��f� I   �e��d
�e .ascrcmnt****      � ****� l   ��c�b� b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    �a�a 0 x  �c  �b  �d  �f  � ��� l   ��`�_� =    ��� o    �^�^ 0 mode  � m    �� ���  f i n i s h�`  �_  � ��]� k    /�� ��� r    '��� I    %�\��[�\ 0 gettimestamp getTimestamp� ��Z� m     !�Y
�Y boovfals�Z  �[  � o      �X�X 0 x  � ��W� I  ( /�V��U
�V .ascrcmnt****      � ****� l  ( +��T�S� b   ( +��� m   ( )�� ��� " b a c k u p   f i n i s h e d :  � o   ) *�R�R 0 x  �T  �S  �U  �W  �]  �o  �p  �  (string)   � ���  ( s t r i n g )� ��� l     �Q�P�O�Q  �P  �O  � ��� l     �N�M�L�N  �M  �L  � ��� l     �K�J�I�K  �J  �I  � ��� l     �H�G�F�H  �G  �F  � ��� l     �E�D�C�E  �D  �C  � ��� l     �B���B  � M G Utility function to get the duration of the backup process in minutes.   � ��� �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .� ��� i  }���� I      �A�@�?�A 0 getduration getDuration�@  �?  � k     �� ��� r     ��� ^     ��� l    ��>�=� \     ��� o     �<�< 0 endtime endTime� o    �;�; 0 	starttime 	startTime�>  �=  � m    �:�: <� o      �9�9 0 duration  � ��� r    ��� ^       l   �8�7 I   �6 z�5�4
�5 .sysorondlong        doub
�4 misccura l   �3�2 ]     o    �1�1 0 duration   m    �0�0 d�3  �2  �6  �8  �7   m    �/�/ d� o      �.�. 0 duration  � �- L    		 o    �,�, 0 duration  �-  � 

 l     �+�*�)�+  �*  �)    l     �(�'�&�(  �'  �&    l     �%�$�#�%  �$  �#    l     �"�!� �"  �!  �     l     ����  �  �    l     ��   ; 5 Utility function bring WBTFU to the front with focus    � j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s  i  �� I      ���� 0 setfocus setFocus�  �   k       O     
 !  I   	���
� .miscactvnull��� ��� null�  �  ! m     ""                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��   #$# l   ����  �  �  $ %&% l   �'(�  ' 8 2do shell script "open -a 'Woah, Back The Fuck Up'"   ( �)) d d o   s h e l l   s c r i p t   " o p e n   - a   ' W o a h ,   B a c k   T h e   F u c k   U p ' "& *�* l   �+,�  + ( "do shell script "open -a 'Finder'"   , �-- D d o   s h e l l   s c r i p t   " o p e n   - a   ' F i n d e r ' "�   ./. l     ����  �  �  / 010 l     �23�  2 � �-----------------------------------------------------------------------------------------------------------------------------------------------   3 �44 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -1 565 l     �78�  7 � �-----------------------------------------------------------------------------------------------------------------------------------------------   8 �99 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -6 :;: l     �
�	��
  �	  �  ; <=< l     ����  �  �  = >?> l     ����  �  �  ? @A@ l     �� ���  �   ��  A BCB l     ��������  ��  ��  C DED l     ��FG��  F � �-----------------------------------------------------------------------------------------------------------------------------------------------   G �HH - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -E IJI l     ��KL��  K � �-----------------------------------------------------------------------------------------------------------------------------------------------   L �MM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -J NON l     ��PQ��  P � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   Q �RR - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -O STS l     ��������  ��  ��  T UVU i  ��WXW I      ��Y���� $0 menuneedsupdate_ menuNeedsUpdate_Y Z��Z l     [����[ m      ��
�� 
cmnu��  ��  ��  ��  X k     \\ ]^] l     ��_`��  _ J D NSMenu's delegates method, when the menu is clicked this is called.   ` �aa �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .^ bcb l     ��de��  d j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   e �ff �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .c ghg l     ��ij��  i < 6 This means the menu items can be changed dynamically.   j �kk l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .h l��l n    mnm I    �������� 0 	makemenus 	makeMenus��  ��  n  f     ��  V opo l     ��������  ��  ��  p qrq l     ��������  ��  ��  r sts l     ��������  ��  ��  t uvu l     ��������  ��  ��  v wxw l     ��������  ��  ��  x yzy i  ��{|{ I      �������� 0 	makemenus 	makeMenus��  ��  | k    >}} ~~ l    	���� n    	��� I    	��������  0 removeallitems removeAllItems��  ��  � o     ���� 0 newmenu newMenu� !  remove existing menu items   � ��� 6   r e m o v e   e x i s t i n g   m e n u   i t e m s ��� l  
 
��������  ��  ��  � ���� Y   
>�������� k   9�� ��� r    '��� n    %��� 4   " %���
�� 
cobj� o   # $���� 0 i  � o    "���� 0 thelist theList� o      ���� 0 	this_item  � ��� l  ( (��������  ��  ��  � ��� Z   ( ������ l  ( -������ =   ( -��� l  ( +������ c   ( +��� o   ( )���� 0 	this_item  � m   ) *��
�� 
TEXT��  ��  � m   + ,�� ���  B a c k u p   n o w��  ��  � r   0 @��� l  0 >������ n  0 >��� I   7 >������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8���� 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ���� m   9 :�� ���  ��  ��  � n  0 7��� I   3 7�������� 	0 alloc  ��  ��  � n  0 3��� o   1 3���� 0 
nsmenuitem 
NSMenuItem� m   0 1��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  C H������ =   C H��� l  C F������ c   C F��� o   C D���� 0 	this_item  � m   D E��
�� 
TEXT��  ��  � m   F G�� ��� " B a c k u p   n o w   &   q u i t��  ��  � ��� r   K [��� l  K Y������ n  K Y��� I   R Y������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S���� 0 	this_item  � ��� m   S T�� ��� * b a c k u p A n d Q u i t H a n d l e r :� ���� m   T U�� ���  ��  ��  � n  K R��� I   N R�������� 	0 alloc  ��  ��  � n  K N��� o   L N���� 0 
nsmenuitem 
NSMenuItem� m   K L��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  ^ c������ =   ^ c��� l  ^ a������ c   ^ a��� o   ^ _���� 0 	this_item  � m   _ `��
�� 
TEXT��  ��  � m   a b�� ��� " T u r n   o n   t h e   b a n t s��  ��  � ��� r   f x��� l  f v������ n  f v��� I   m v������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n���� 0 	this_item  � ��� m   n o�� ���  n s f w O n H a n d l e r :� ���� m   o r�� ���  ��  ��  � n  f m��� I   i m�������� 	0 alloc  ��  ��  � n  f i��� o   g i���� 0 
nsmenuitem 
NSMenuItem� m   f g��
�� misccura��  ��  � o      ���� 0 thismenuitem thisMenuItem� ��� l  { ������� =   { ���� l  { ~������ c   { ~��� o   { |���� 0 	this_item  � m   | }��
�� 
TEXT��  ��  � m   ~ ��� ��� $ T u r n   o f f   t h e   b a n t s��  ��  � ��� r   � �   l  � ����� n  � � I   � ������� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   � ����� 0 	this_item   	 m   � �

 �  n s f w O f f H a n d l e r :	 �� m   � � �  ��  ��   n  � � I   � ��������� 	0 alloc  ��  ��   n  � � o   � ��� 0 
nsmenuitem 
NSMenuItem m   � ��~
�~ misccura��  ��   o      �}�} 0 thismenuitem thisMenuItem�  l  � ��|�{ =   � � l  � ��z�y c   � � o   � ��x�x 0 	this_item   m   � ��w
�w 
TEXT�z  �y   m   � � � * T u r n   o n   n o t i f i c a t i o n s�|  �{    r   � �  l  � �!�v�u! n  � �"#" I   � ��t$�s�t J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_$ %&% o   � ��r�r 0 	this_item  & '(' m   � �)) �** , n o t i f i c a t i o n O n H a n d l e r :( +�q+ m   � �,, �--  �q  �s  # n  � �./. I   � ��p�o�n�p 	0 alloc  �o  �n  / n  � �010 o   � ��m�m 0 
nsmenuitem 
NSMenuItem1 m   � ��l
�l misccura�v  �u    o      �k�k 0 thismenuitem thisMenuItem 232 l  � �4�j�i4 =   � �565 l  � �7�h�g7 c   � �898 o   � ��f�f 0 	this_item  9 m   � ��e
�e 
TEXT�h  �g  6 m   � �:: �;; , T u r n   o f f   n o t i f i c a t i o n s�j  �i  3 <=< r   � �>?> l  � �@�d�c@ n  � �ABA I   � ��bC�a�b J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_C DED o   � ��`�` 0 	this_item  E FGF m   � �HH �II . n o t i f i c a t i o n O f f H a n d l e r :G J�_J m   � �KK �LL  �_  �a  B n  � �MNM I   � ��^�]�\�^ 	0 alloc  �]  �\  N n  � �OPO o   � ��[�[ 0 
nsmenuitem 
NSMenuItemP m   � ��Z
�Z misccura�d  �c  ? o      �Y�Y 0 thismenuitem thisMenuItem= QRQ l  � �S�X�WS =   � �TUT l  � �V�V�UV c   � �WXW o   � ��T�T 0 	this_item  X m   � ��S
�S 
TEXT�V  �U  U m   � �YY �ZZ  Q u i t�X  �W  R [�R[ r   � �\]\ l  � �^�Q�P^ n  � �_`_ I   � ��Oa�N�O J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_a bcb o   � ��M�M 0 	this_item  c ded m   � �ff �gg  q u i t H a n d l e r :e h�Lh m   � �ii �jj  �L  �N  ` n  � �klk I   � ��K�J�I�K 	0 alloc  �J  �I  l n  � �mnm o   � ��H�H 0 
nsmenuitem 
NSMenuItemn m   � ��G
�G misccura�Q  �P  ] o      �F�F 0 thismenuitem thisMenuItem�R  ��  � opo l �E�D�C�E  �D  �C  p qrq l s�B�As n tut I  �@v�?�@ 0 additem_ addItem_v w�>w o  �=�= 0 thismenuitem thisMenuItem�>  �?  u o  �<�< 0 newmenu newMenu�B  �A  r xyx l �;�:�9�;  �:  �9  y z{z l |}~| l �8�7 n ��� I  �6��5�6 0 
settarget_ 
setTarget_� ��4�  f  �4  �5  � o  �3�3 0 thismenuitem thisMenuItem�8  �7  } * $ required for enabling the menu item   ~ ��� H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m{ ��� l �2�1�0�2  �1  �0  � ��/� Z  9���.�-� G  "��� l ��,�+� =  ��� o  �*�* 0 i  � m  �)�) �,  �+  � l ��(�'� =  ��� o  �&�& 0 i  � m  �%�% �(  �'  � l %5���� l %5��$�#� n %5��� I  *5�"��!�" 0 additem_ addItem_� �� � l *1���� n *1��� o  -1�� 0 separatoritem separatorItem� n *-��� o  +-�� 0 
nsmenuitem 
NSMenuItem� m  *+�
� misccura�  �  �   �!  � o  %*�� 0 newmenu newMenu�$  �#  �   add a seperator   � ���     a d d   a   s e p e r a t o r�.  �-  �/  �� 0 i  � m    �� � n    ��� m    �
� 
nmbr� n   ��� 2   �
� 
cobj� o    �� 0 thelist theList��  ��  z ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ���
�  �  �
  � ��� l     �	���	  �  �  � ��� i  ����� I      ����  0 backuphandler_ backupHandler_� ��� o      �� 
0 sender  �  �  � Z     ������ l    ��� � =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovfals�  �   � k   
 U�� ��� r   
 ��� m   
 ��
�� boovtrue� o      ���� 0 manualbackup manualBackup� ���� Z    U������� l   ������ =    ��� o    ���� 0 notifications  � m    ��
�� boovtrue��  ��  � Z    Q������ l   #������ =    #��� o    !���� 0 nsfw  � m   ! "��
�� boovtrue��  ��  � k   & 3�� ��� I  & -����
�� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �����
�� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  . 3�����
�� .sysodelanull��� ��� nmbr� m   . /���� ��  ��  � ��� l  6 =������ =   6 =��� o   6 ;���� 0 nsfw  � m   ; <��
�� boovfals��  ��  � ���� k   @ M�� ��� I  @ G����
�� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� �����
�� 
appr� m   B C�� ��� 
 W B T F U��  � ���� I  H M�����
�� .sysodelanull��� ��� nmbr� m   H I���� ��  ��  ��  ��  ��  ��  ��  � ��� l  X _������ =   X _��� o   X ]���� 0 isbackingup isBackingUp� m   ] ^��
�� boovtrue��  ��  � ���� k   b ��� ��� r   b k��� n   b i��� 3   g i��
�� 
cobj� o   b g���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   l �������� l  l s������ =   l s��� o   l q���� 0 notifications  � m   q r��
�� boovtrue��  ��  � Z   v ������� l  v }������ =   v }   o   v {���� 0 nsfw   m   { |��
�� boovtrue��  ��  � I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � � � P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  �  l  � �	����	 =   � �

 o   � ����� 0 nsfw   m   � ���
�� boovfals��  ��   �� I  � ���
�� .sysonotfnull��� ��� TEXT m   � � � 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p ����
�� 
appr m   � � � 
 W B T F U��  ��  ��  ��  ��  ��  ��  �  �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    i  �� !  I      ��"���� .0 backupandquithandler_ backupAndQuitHandler_" #��# o      ���� 
0 sender  ��  ��  ! Z    $%&��$ l    '����' =     ()( o     ���� 0 isbackingup isBackingUp) m    ��
�� boovfals��  ��  % k   
 �** +,+ Z   
 +-./��- l  
 0����0 =   
 121 o   
 ���� 0 nsfw  2 m    ��
�� boovtrue��  ��  . r    343 m    55 �66 , W o a h ,   B a c k   T h e   F u c k   U p4 o      ���� 0 	temptitle 	tempTitle/ 787 l   !9����9 =    !:;: o    ���� 0 nsfw  ; m     ��
�� boovfals��  ��  8 <��< r   $ '=>= m   $ %?? �@@ 
 W B T F U> o      ���� 0 	temptitle 	tempTitle��  ��  , ABA l  , ,��������  ��  ��  B CDC r   , @EFE l  , >G����G n   , >HIH 1   < >��
�� 
bhitI l  , <J����J I  , <��KL
�� .sysodlogaskr        TEXTK m   , -MM �NN T A r e   y o u   s u r e   y o u   w a n t   t o   b a c k u p   t h e n   q u i t ?L �OP
� 
apprO o   . /�� 0 	temptitle 	tempTitleP �QR
� 
dispQ o   0 1�� 0 appicon appIconR �ST
� 
btnsS J   2 6UU VWV m   2 3XX �YY  Y e sW Z�Z m   3 4[[ �\\  N o�  T �]�
� 
dflt] m   7 8�� �  ��  ��  ��  ��  F o      �� 	0 reply  D ^_^ l  A A����  �  �  _ `�` Z   A �abc�a l  A Dd��d =   A Defe o   A B�~�~ 	0 reply  f m   B Cgg �hh  Y e s�  �  b k   G �ii jkj r   G Nlml m   G H�}
�} boovtruem o      �|�|  0 backupthenquit backupThenQuitk non r   O Vpqp m   O P�{
�{ boovtrueq o      �z�z 0 manualbackup manualBackupo rsr l  W W�y�x�w�y  �x  �w  s t�vt Z   W �uv�u�tu l  W ^w�s�rw =   W ^xyx o   W \�q�q 0 notifications  y m   \ ]�p
�p boovtrue�s  �r  v Z   a �z{|�oz l  a h}�n�m} =   a h~~ o   a f�l�l 0 nsfw   m   f g�k
�k boovtrue�n  �m  { k   k x�� ��� I  k r�j��
�j .sysonotfnull��� ��� TEXT� m   k l�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �i��h
�i 
appr� m   m n�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�h  � ��g� I  s x�f��e
�f .sysodelanull��� ��� nmbr� m   s t�d�d �e  �g  | ��� l  { ���c�b� =   { ���� o   { ��a�a 0 nsfw  � m   � ��`
�` boovfals�c  �b  � ��_� k   � ��� ��� I  � ��^��
�^ .sysonotfnull��� ��� TEXT� m   � ��� ���   P r e p a r i n g   b a c k u p� �]��\
�] 
appr� m   � ��� ��� 
 W B T F U�\  � ��[� I  � ��Z��Y
�Z .sysodelanull��� ��� nmbr� m   � ��X�X �Y  �[  �_  �o  �u  �t  �v  c ��� l  � ���W�V� =   � ���� o   � ��U�U 	0 reply  � m   � ��� ���  N o�W  �V  � ��T� r   � ���� m   � ��S
�S boovfals� o      �R�R  0 backupthenquit backupThenQuit�T  �  �  & ��� l  � ���Q�P� =   � ���� o   � ��O�O 0 isbackingup isBackingUp� m   � ��N
�N boovtrue�Q  �P  � ��M� k   �
�� ��� r   � ���� n   � ���� 3   � ��L
�L 
cobj� o   � ��K�K 40 messagesalreadybackingup messagesAlreadyBackingUp� o      �J�J 0 msg  � ��I� Z   �
���H�G� l  � ���F�E� =   � ���� o   � ��D�D 0 notifications  � m   � ��C
�C boovtrue�F  �E  � Z   �����B� l  � ���A�@� =   � ���� o   � ��?�? 0 nsfw  � m   � ��>
�> boovtrue�A  �@  � I  � ��=��
�= .sysonotfnull��� ��� TEXT� o   � ��<�< 0 msg  � �;��:
�; 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�:  � ��� l  � ���9�8� =   � ���� o   � ��7�7 0 nsfw  � m   � ��6
�6 boovfals�9  �8  � ��5� I  ��4��
�4 .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �3��2
�3 
appr� m   � ��� ��� 
 W B T F U�2  �5  �B  �H  �G  �I  �M  ��   ��� l     �1�0�/�1  �0  �/  � ��� l     �.�-�,�.  �-  �,  � ��� l     �+�*�)�+  �*  �)  � ��� l     �(�'�&�(  �'  �&  � ��� l     �%�$�#�%  �$  �#  � ��� i  ����� I      �"��!�"  0 nsfwonhandler_ nsfwOnHandler_� �� � o      �� 
0 sender  �   �!  � k     =�� ��� r     ��� m     �
� boovtrue� o      �� 0 nsfw  � ��� Z    =����� l   ���� =    ��� o    �� 0 notifications  � m    �
� boovtrue�  �  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m       � , T u r n   o f f   n o t i f i c a t i o n s� � m     �  Q u i t�  � o      �� 0 thelist theList�  l  " )�� =   " )	 o   " '�� 0 notifications  	 m   ' (�
� boovfals�  �   
�
 r   , 9 J   , 3  m   , - �  B a c k u p   n o w  m   - . � " B a c k u p   n o w   &   q u i t  m   . / � $ T u r n   o f f   t h e   b a n t s  m   / 0 � * T u r n   o n   n o t i f i c a t i o n s � m   0 1 �    Q u i t�   o      �� 0 thelist theList�  �  �  � !"! l     ����  �  �  " #$# i  ��%&% I      �
'�	�
 "0 nsfwoffhandler_ nsfwOffHandler_' (�( o      �� 
0 sender  �  �	  & k     =)) *+* r     ,-, m     �
� boovfals- o      �� 0 nsfw  + .�. Z    =/01�/ l   2��2 =    343 o    � �  0 notifications  4 m    ��
�� boovtrue�  �  0 r    565 J    77 898 m    :: �;;  B a c k u p   n o w9 <=< m    >> �?? " B a c k u p   n o w   &   q u i t= @A@ m    BB �CC " T u r n   o n   t h e   b a n t sA DED m    FF �GG , T u r n   o f f   n o t i f i c a t i o n sE H��H m    II �JJ  Q u i t��  6 o      ���� 0 thelist theList1 KLK l  " )M����M =   " )NON o   " '���� 0 notifications  O m   ' (��
�� boovfals��  ��  L P��P r   , 9QRQ J   , 3SS TUT m   , -VV �WW  B a c k u p   n o wU XYX m   - .ZZ �[[ " B a c k u p   n o w   &   q u i tY \]\ m   . /^^ �__ " T u r n   o n   t h e   b a n t s] `a` m   / 0bb �cc * T u r n   o n   n o t i f i c a t i o n sa d��d m   0 1ee �ff  Q u i t��  R o      ���� 0 thelist theList��  �  �  $ ghg l     ��������  ��  ��  h iji l     ��������  ��  ��  j klk l     ��������  ��  ��  l mnm l     ��������  ��  ��  n opo l     ��������  ��  ��  p qrq i  ��sts I      ��u���� 00 notificationonhandler_ notificationOnHandler_u v��v o      ���� 
0 sender  ��  ��  t k     =ww xyx r     z{z m     ��
�� boovtrue{ o      ���� 0 notifications  y |��| Z    =}~��} l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  ~ r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ���� m    �� ���  Q u i t��  � o      ���� 0 thelist theList ��� l  " )������ =   " )��� o   " '���� 0 nsfw  � m   ' (��
�� boovfals��  ��  � ���� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ���� m   0 1�� ���  Q u i t��  � o      ���� 0 thelist theList��  ��  ��  r ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 20 notificationoffhandler_ notificationOffHandler_� ���� o      ���� 
0 sender  ��  ��  � k     =�� ��� r     ��� m     ��
�� boovfals� o      ���� 0 notifications  � ���� Z    =������ l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� " B a c k u p   n o w   &   q u i t� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m    �� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  " )����� =   " )��� o   " '�� 0 nsfw  � m   ' (�
� boovfals��  �  � ��� r   , 9��� J   , 3�� ��� m   , -�� ���  B a c k u p   n o w� ��� m   - .�� ��� " B a c k u p   n o w   &   q u i t� ��� m   . /�� ��� " T u r n   o n   t h e   b a n t s� ��� m   / 0�� ��� * T u r n   o n   n o t i f i c a t i o n s� ��� m   0 1�� ���  Q u i t�  � o      �� 0 thelist theList�  ��  ��  � ��� l     ����  �  �  � ��� l     ����  �  �  � � � l     ����  �  �     l     ����  �  �    l     ����  �  �    i  �� I      �	�� 0 quithandler_ quitHandler_	 
�
 o      �� 
0 sender  �  �   Z     �� l    �� =      o     �� 0 isbackingup isBackingUp m    �
� boovtrue�  �   k   
 \  n  
  I    ���� 0 setfocus setFocus�  �    f   
   l   ����  �  �    Z    1� l   �� =     o    �� 0 nsfw   m    �
� boovtrue�  �   r     !  m    "" �## , W o a h ,   B a c k   T h e   F u c k   U p! o      �� 0 	temptitle 	tempTitle $%$ l    '&��& =     ''(' o     %�� 0 nsfw  ( m   % &�
� boovfals�  �  % )�) r   * -*+* m   * +,, �-- 
 W B T F U+ o      �� 0 	temptitle 	tempTitle�  �   ./. l  2 2����  �  �  / 010 r   2 F232 l  2 D4��4 n   2 D565 1   B D�
� 
bhit6 l  2 B7��7 I  2 B�89
� .sysodlogaskr        TEXT8 m   2 3:: �;; � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?9 �<=
� 
appr< o   4 5�� 0 	temptitle 	tempTitle= �>?
� 
disp> o   6 7�� 0 appicon appIcon? �@A
� 
btns@ J   8 <BB CDC m   8 9EE �FF  C a n c e l   &   Q u i tD G�G m   9 :HH �II  R e s u m e�  A �J�
� 
dfltJ m   = >�~�~ �  �  �  �  �  3 o      �}�} 	0 reply  1 K�|K Z   G \LM�{NL l  G JO�z�yO =   G JPQP o   G H�x�x 	0 reply  Q m   H IRR �SS  C a n c e l   &   Q u i t�z  �y  M k   M RTT UVU l  M M�wWX�w  W / )quit application "Woah, Back The Fuck Up"   X �YY R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "V Z�vZ I  M R�u[�t
�u .sysoexecTEXT���     TEXT[ m   M N\\ �]] : k i l l   ' W o a h ,   B a c k   T h e   F u c k   U p '�t  �v  �{  N I  U \�s^�r
�s .ascrcmnt****      � ****^ l  U X_�q�p_ m   U X`` �aa  W B T F U   r e s u m e d�q  �p  �r  �|   bcb l  _ fd�o�nd =   _ fefe o   _ d�m�m 0 isbackingup isBackingUpf m   d e�l
�l boovfals�o  �n  c g�kg k   i �hh iji n  i nklk I   j n�j�i�h�j 0 setfocus setFocus�i  �h  l  f   i jj mnm l  o o�g�f�e�g  �f  �e  n opo Z   o �qrs�dq l  o vt�c�bt =   o vuvu o   o t�a�a 0 nsfw  v m   t u�`
�` boovtrue�c  �b  r r   y ~wxw m   y |yy �zz , W o a h ,   B a c k   T h e   F u c k   U px o      �_�_ 0 	temptitle 	tempTitles {|{ l  � �}�^�]} =   � �~~ o   � ��\�\ 0 nsfw   m   � ��[
�[ boovfals�^  �]  | ��Z� r   � ���� m   � ��� ��� 
 W B T F U� o      �Y�Y 0 	temptitle 	tempTitle�Z  �d  p ��� l  � ��X�W�V�X  �W  �V  � ��� r   � ���� l  � ���U�T� n   � ���� 1   � ��S
�S 
bhit� l  � ���R�Q� I  � ��P��
�P .sysodlogaskr        TEXT� m   � ��� ��� < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?� �O��
�O 
appr� o   � ��N�N 0 	temptitle 	tempTitle� �M��
�M 
disp� o   � ��L�L 0 appicon appIcon� �K��
�K 
btns� J   � ��� ��� m   � ��� ���  Q u i t� ��J� m   � ��� ���  R e s u m e�J  � �I��H
�I 
dflt� m   � ��G�G �H  �R  �Q  �U  �T  � o      �F�F 	0 reply  � ��E� Z   � ����D�� l  � ���C�B� =   � ���� o   � ��A�A 	0 reply  � m   � ��� ���  Q u i t�C  �B  � k   � ��� ��� l  � ��@���@  � / )quit application "Woah, Back The Fuck Up"   � ��� R q u i t   a p p l i c a t i o n   " W o a h ,   B a c k   T h e   F u c k   U p "� ��?� I  � ��>��=
�> .sysoexecTEXT���     TEXT� m   � ��� ��� : k i l l   ' W o a h ,   B a c k   T h e   F u c k   U p '�=  �?  �D  � I  � ��<��;
�< .ascrcmnt****      � ****� l  � ���:�9� m   � ��� ���  W B T F U   r e s u m e d�:  �9  �;  �E  �k  �   ��� l     �8�7�6�8  �7  �6  � ��� l     �5�4�3�5  �4  �3  � ��� l     �2�1�0�2  �1  �0  � ��� l     �/�.�-�/  �.  �-  � ��� l     �,�+�*�,  �+  �*  � ��� i  ����� I      �)�(�'�) (0 animatemenubaricon animateMenuBarIcon�(  �'  � k     .�� ��� r     ��� 4     �&�
�& 
alis� l   ��%�$� b    ��� l   	��#�"� I   	�!��
�! .earsffdralis        afdr�  f    � � ��
�  
rtyp� m    �
� 
ctxt�  �#  �"  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g�%  �$  � o      �� 0 	imagepath 	imagePath� ��� r    ��� n    ��� 1    �
� 
psxp� o    �� 0 	imagepath 	imagePath� o      �� 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !���� 20 initwithcontentsoffile_ initWithContentsOfFile_� ��� o    �� 0 	imagepath 	imagePath�  �  � n   ��� I    ���� 	0 alloc  �  �  � n   ��� o    �� 0 nsimage NSImage� m    �
� misccura� o      �� 	0 image  � ��� n  $ .��� I   ) .���� 0 	setimage_ 	setImage_� ��� o   ) *�� 	0 image  �  �  � o   $ )�
�
 0 
statusitem 
StatusItem�  � ��� l     �	���	  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     � �����   ��  ��  � ��� l     ��������  ��  ��  � ��� i  ����� I      �������� $0 resetmenubaricon resetMenuBarIcon��  ��  � k     .�� ��� r     ��� 4     �� 
�� 
alis  l   ���� b     l   	���� I   	��
�� .earsffdralis        afdr  f     ����
�� 
rtyp m    ��
�� 
ctxt��  ��  ��   m   	 
 �		 J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g��  ��  � o      ���� 0 	imagepath 	imagePath� 

 r     n     1    ��
�� 
psxp o    ���� 0 	imagepath 	imagePath o      ���� 0 	imagepath 	imagePath  r    # n   ! I    !������ 20 initwithcontentsoffile_ initWithContentsOfFile_ �� o    ���� 0 	imagepath 	imagePath��  ��   n    I    �������� 	0 alloc  ��  ��   n    o    ���� 0 nsimage NSImage m    ��
�� misccura o      ���� 	0 image   �� n  $ . I   ) .������ 0 	setimage_ 	setImage_  ��  o   ) *���� 	0 image  ��  ��   o   $ )���� 0 
statusitem 
StatusItem��  � !"! l     ��������  ��  ��  " #$# l     ��������  ��  ��  $ %&% l     ��������  ��  ��  & '(' l     ��������  ��  ��  ( )*) l     ��������  ��  ��  * +,+ l     ��-.��  -   create an NSStatusBar   . �// ,   c r e a t e   a n   N S S t a t u s B a r, 010 i  ��232 I      �������� 0 makestatusbar makeStatusBar��  ��  3 k     r44 565 l     ��78��  7  log ("Make status bar")   8 �99 . l o g   ( " M a k e   s t a t u s   b a r " )6 :;: r     <=< n    >?> o    ���� "0 systemstatusbar systemStatusBar? n    @A@ o    ���� 0 nsstatusbar NSStatusBarA m     ��
�� misccura= o      ���� 0 bar  ; BCB l   ��������  ��  ��  C DED r    FGF n   HIH I   	 �J�� .0 statusitemwithlength_ statusItemWithLength_J K�K m   	 
LL ��      �  �  I o    	�� 0 bar  G o      �� 0 
statusitem 
StatusItemE MNM l   ����  �  �  N OPO r    #QRQ 4    !�S
� 
alisS l    T��T b     UVU l   W��W I   �XY
� .earsffdralis        afdrX  f    Y �Z�
� 
rtypZ m    �
� 
ctxt�  �  �  V m    [[ �\\ J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�  �  R o      �� 0 	imagepath 	imagePathP ]^] r   $ )_`_ n   $ 'aba 1   % '�
� 
psxpb o   $ %�� 0 	imagepath 	imagePath` o      �� 0 	imagepath 	imagePath^ cdc r   * 8efe n  * 6ghg I   1 6�i�� 20 initwithcontentsoffile_ initWithContentsOfFile_i j�j o   1 2�� 0 	imagepath 	imagePath�  �  h n  * 1klk I   - 1���� 	0 alloc  �  �  l n  * -mnm o   + -�� 0 nsimage NSImagen m   * +�
� misccuraf o      �� 	0 image  d opo l  9 9����  �  �  p qrq l  9 9�st�  s � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   t �uu s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "r vwv l  9 9����  �  �  w xyx n  9 Cz{z I   > C�|�� 0 	setimage_ 	setImage_| }�} o   > ?�� 	0 image  �  �  { o   9 >�� 0 
statusitem 
StatusItemy ~~ l  D D����  �  �   ��� l  D D����  � , & set up the initial NSStatusBars title   � ��� L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e� ��� l  D D����  � # StatusItem's setTitle:"WBTFU"   � ��� : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "� ��� l  D D����  �  �  � ��� l  D D����  � 1 + set up the initial NSMenu of the statusbar   � ��� V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r� ��� r   D X��� n  D R��� I   K R����  0 initwithtitle_ initWithTitle_� ��� m   K N�� ���  C u s t o m�  �  � n  D K��� I   G K���� 	0 alloc  �  �  � n  D G��� o   E G�� 0 nsmenu NSMenu� m   D E�
� misccura� o      �� 0 newmenu newMenu� ��� l  Y Y����  �  �  � ��� l  Y Y����  � � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   � ���0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .� ��� n  Y c��� I   ^ c�~��}�~ 0 setdelegate_ setDelegate_� ��|�  f   ^ _�|  �}  � o   Y ^�{�{ 0 newmenu newMenu� ��� l  d d�z�y�x�z  �y  �x  � ��w� n  d r��� I   i r�v��u�v 0 setmenu_ setMenu_� ��t� o   i n�s�s 0 newmenu newMenu�t  �u  � o   d i�r�r 0 
statusitem 
StatusItem�w  1 ��� l     �q�p�o�q  �p  �o  � ��� l     �n���n  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �m���m  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �l�k�j�l  �k  �j  � ��� l     �i�h�g�i  �h  �g  � ��� l     �f�e�d�f  �e  �d  � ��� l     �c�b�a�c  �b  �a  � ��� l     �`�_�^�`  �_  �^  � ��� l     �]�\�[�]  �\  �[  � ��� i  ����� I      �Z�Y�X�Z 0 runonce runOnce�Y  �X  � k     �� ��� I     �W�V�U�W (0 animatemenubaricon animateMenuBarIcon�V  �U  � ��T� I    �S�R�Q�S 0 	plistinit 	plistInit�R  �Q  �T  � ��� l     �P�O�N�P  �O  �N  � ��� l   ��M�L� n   ��� I    �K�J�I�K 0 makestatusbar makeStatusBar�J  �I  �  f    �M  �L  � ��� l   ��H�G� I    �F�E�D�F 0 runonce runOnce�E  �D  �H  �G  � ��� l     �C�B�A�C  �B  �A  � ��� l     �@�?�>�@  �?  �>  � ��� l     �=�<�;�=  �<  �;  � ��� l     �:�9�8�:  �9  �8  � ��� l     �7�6�5�7  �6  �5  � ��� l     �4���4  � w q Function that is always running in the background. This doesn't need to get called as it is running from the off   � ��� �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f� ��� l     �3���3  � � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   � ����   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .� ��2� i  ����� I     �1�0�/
�1 .miscidlenmbr    ��� null�0  �/  � k     ��� ��� Z     ����.�-� l    ��,�+� E     ��� o     �*�* &0 displaysleepstate displaySleepState� o    
�)�) 0 strawake strAwake�,  �+  � Z    ���(�'� l   ��&�%� =    � � o    �$�$ 0 isbackingup isBackingUp  m    �#
�# boovfals�&  �%  � Z    {�" l   �!�  =     o    �� 0 manualbackup manualBackup m    �
� boovfals�!  �    k   " e 	 l  " "�
�  
 . (if ((hours of (current date)) < 18) then    � P i f   ( ( h o u r s   o f   ( c u r r e n t   d a t e ) )   <   1 8 )   t h e n	  r   " + l  " )�� n   " ) 1   ' )�
� 
min  l  " '�� l  " '�� I  " '���
� .misccurdldt    ��� null�  �  �  �  �  �  �  �   o      �� 0 m    l  , ,����  �  �    Z   , c� G   , 7 l  , /�� =   , / !  o   , -�� 0 m  ! m   - .�
�
 �  �   l  2 5"�	�" =   2 5#$# o   2 3�� 0 m  $ m   3 4�� -�	  �   Z   : I%&��% l  : ='��' =   : =()( o   : ;�� 0 scheduledtime scheduledTime) m   ; <� �  �  �  & I   @ E�������� 0 	plistinit 	plistInit��  ��  �  �   *+* G   L W,-, l  L O.����. =   L O/0/ o   L M���� 0 m  0 m   M N����  ��  ��  - l  R U1����1 =   R U232 o   R S���� 0 m  3 m   S T���� ��  ��  + 4��4 I   Z _�������� 0 	plistinit 	plistInit��  ��  ��  �   5��5 l  d d��67��  6  end if   7 �88  e n d   i f��   9:9 l  h o;����; =   h o<=< o   h m���� 0 manualbackup manualBackup= m   m n��
�� boovtrue��  ��  : >��> I   r w�������� 0 	plistinit 	plistInit��  ��  ��  �"  �(  �'  �.  �-  � ?@? l  � ���������  ��  ��  @ A��A L   � �BB m   � ����� ��  �2       5��CD�� vEFGHI������J������KLMNO��PQRSTUVWXYZ[\]^_`abcdefghijkl��  C 3�����������������������������������������������������������������������������
�� 
pimr�� 0 
statusitem 
StatusItem�� 0 
thedisplay 
theDisplay�� 0 defaults  �� $0 internalmenuitem internalMenuItem�� $0 externalmenuitem externalMenuItem�� 0 newmenu newMenu�� 0 thelist theList�� 0 nsfw  �� 0 notifications  ��  0 backupthenquit backupThenQuit�� 	0 plist  �� 0 initialbackup initialBackup�� 0 manualbackup manualBackup�� 0 isbackingup isBackingUp�� "0 messagesmissing messagesMissing�� *0 messagesencouraging messagesEncouraging�� $0 messagescomplete messagesComplete�� &0 messagescancelled messagesCancelled�� 40 messagesalreadybackingup messagesAlreadyBackingUp�� 0 strawake strAwake�� 0 strsleep strSleep�� &0 displaysleepstate displaySleepState�� 0 	plistinit 	plistInit�� 0 diskinit diskInit�� 0 freespaceinit freeSpaceInit� 0 
backupinit 
backupInit� 0 tidyup tidyUp� 
0 backup  � 0 itexists itExists� .0 getcomputeridentifier getComputerIdentifier� 0 gettimestamp getTimestamp� 0 comparesizes compareSizes� 0 	stopwatch  � 0 getduration getDuration� 0 setfocus setFocus� $0 menuneedsupdate_ menuNeedsUpdate_� 0 	makemenus 	makeMenus�  0 backuphandler_ backupHandler_� .0 backupandquithandler_ backupAndQuitHandler_�  0 nsfwonhandler_ nsfwOnHandler_� "0 nsfwoffhandler_ nsfwOffHandler_� 00 notificationonhandler_ notificationOnHandler_� 20 notificationoffhandler_ notificationOffHandler_� 0 quithandler_ quitHandler_� (0 animatemenubaricon animateMenuBarIcon� $0 resetmenubaricon resetMenuBarIcon� 0 makestatusbar makeStatusBar� 0 runonce runOnce
� .miscidlenmbr    ��� null
� .aevtoappnull  �   � ****D �m� m  nopqn � [�
� 
vers�  o �r�
� 
cobjr ss   �
� 
osax�  p �t�
� 
cobjt uu   � d
� 
frmk�  q �v�
� 
cobjv ww   � j
� 
frmk�  
�� 
msngE xx ��y
� misccura
� 
pclsy �zz  N S U s e r D e f a u l t sF {{ ��|
� misccura
� 
pcls| �}}  N S M e n u I t e mG ~~ ��
� misccura
� 
pcls ���  N S M e n u I t e mH �� ���
� misccura
� 
pcls� ���  N S M e n uI ��� �   � � � � �
�� boovfals
�� boovtrue
�� boovfalsJ ��� � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
�� boovtrue
�� boovfals
�� boovfalsK ��� �  #'+/2L ��� 
� 
 <@DHLPTX\_M ��� 
� 
 imquy}����N ��� 	� 	 ���������O ��� 	� 	 ���������P ����             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 3 0 6 3 3 3 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 8 , " I d l e T i m e r E l a p s e d T i m e " = 7 4 5 4 0 0 , " C u r r e n t P o w e r S t a t e " = 4 }Q ������� 0 	plistinit 	plistInit�  �  � �����������~� 0 
foldername 
folderName� 0 
backupdisk 
backupDisk�  0 computerfolder computerFolder� 0 
backuptime 
backupTime� 0 thecontents theContents� 0 thevalue theValue� 0 welcome  � 0 	temptitle 	tempTitle� 	0 reply  � 0 backuptimes backupTimes�~ 0 selectedtime selectedTime� B8�}k�|�{�z�y�x�w�v�u&�t�s����r�q�p�o��n�m�l�k�j�i�h��g�f�e�d��c�b�����a�`�_�^�]�\�[�Z�Y�X�W�V<�UL\k�T�S�R�Q�P�} 0 itexists itExists
�| 
plif
�{ 
pcnt
�z 
valL�y  0 foldertobackup FolderToBackup�x 0 backupdrive BackupDrive�w  0 computerfolder computerFolder�v 0 scheduledtime scheduledTime�u .0 getcomputeridentifier getComputerIdentifier�t  ���s 0 setfocus setFocus
�r 
appr
�q 
disp�p 0 appicon appIcon
�o 
btns
�n 
dflt�m 
�l .sysodlogaskr        TEXT
�k 
bhit
�j afdrcusr
�i .earsffdralis        afdr
�h 
prmp
�g .sysostflalis    ��� null
�f 
TEXT�e  �d  
�c .sysoexecTEXT���     TEXT
�b 
psxp
�a .gtqpchltns    @   @ ns  �` �_ �^ <
�] 
kocl
�\ 
prdt
�[ 
pnam�Z 
�Y .corecrel****      � null
�X 
plii
�W 
insh
�V 
kind�U �T 0 sourcefolder sourceFolder�S "0 destinationdisk destinationDisk�R 0 machinefolder machineFolder
�Q .ascrcmnt****      � ****�P 0 diskinit diskInit�.jE�O*�b  l+ e  6� .*�b  /�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUY�*j+ 
E�O� ��n)j+ O�E�Ob  e  �E�Y b  f  
a E�Y hO�a �a _ a a kva ka  a ,E�Oa j E�O *a a l a &E�W X   !a "j #O�a $,E�O�a $,E�Oa %a &a 'mvE�O�a a (l )kva &E�O�a *  
a +E�Y #�a ,  
a -E�Y �a .  
a /E�Y hOPoUO� �*a 0�a 1a 2b  la 3 4 �*a 0a 5a 6*6a 1a 7a a 2a 8�a 9a 9 4O*a 0a 5a 6*6a 1a 7a a 2a :�a 9a 9 4O*a 0a 5a 6*6a 1a 7a a 2a ;�a 9a 9 4O*a 0a 5a 6*6a 1a 7a a 2a <�a 9a 9 4UUO�E` =O�E` >O�E` ?O�E�O_ =_ >_ ?�a 3vj @O*j+ AR �O��N�M���L�O 0 diskinit diskInit�N  �M  � �K�J�I�K 0 msg  �J 0 	temptitle 	tempTitle�I 	0 reply  � ��H�G�F�E�D���C�B�A�@���?�>�=�<�;�:(+�H "0 destinationdisk destinationDisk�G 0 itexists itExists�F 0 freespaceinit freeSpaceInit�E 0 setfocus setFocus
�D 
cobj
�C 
appr
�B 
disp�A 0 appicon appIcon
�@ 
btns
�? 
dflt�> 
�= .sysodlogaskr        TEXT
�< 
bhit�; 0 diskinit diskInit
�: .sysonotfnull��� ��� TEXT�L �*��l+ e  *fk+ Y �)j+ Ob  �.E�Ob  e  �E�Y b  f  �E�Y hO�������lv�l� a ,E�O�a   
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY hS �9I�8�7���6�9 0 freespaceinit freeSpaceInit�8 �5��5 �  �4�4 	0 again  �7  � �3�2�1�0�3 	0 again  �2 0 	temptitle 	tempTitle�1 	0 reply  �0 0 msg  � �/�.�-�,�+gq��*�)�(�'���&�%�$�#�����"�!�� ���/ 0 sourcefolder sourceFolder�. "0 destinationdisk destinationDisk�- 0 comparesizes compareSizes�, 0 
backupinit 
backupInit�+ 0 setfocus setFocus
�* 
appr
�) 
disp�( 0 appicon appIcon
�' 
btns
�& 
dflt�% 
�$ .sysodlogaskr        TEXT
�# 
bhit�" 0 tidyup tidyUp
�! 
cobj
�  .sysonotfnull��� ��� TEXT�6 �*��l+ e  
*j+ Y �)j+ Ob  e  �E�Y b  f  �E�Y hO�f  �������lv�l� a ,E�Y (�e  !a ����a a lv�l� a ,E�Y hO�a   
*j+ Y ]b  b   b  a .E�Y hOb  	e  4b  e  ��a l Y b  f  a �a l Y hY hT ������� 0 
backupinit 
backupInit�  �  �  � ��� �.�����,��;B��Nc�t}�����
� 
psxf� "0 destinationdisk destinationDisk� 	0 drive  � 0 itexists itExists
� 
kocl
� 
cfol
� 
insh
� 
prdt
� 
pnam� 
� .corecrel****      � null� 0 machinefolder machineFolder
� 
cdis� 0 backupfolder backupFolder� 0 latestfolder latestFolder� 
0 backup  � �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` UO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` UO*j+ U ���
�	���� 0 tidyup tidyUp�
  �	  � �������� � 0 bf bF� 0 creationdates creationDates� 0 theoldestdate theOldestDate� 0 j  � 0 i  � 0 thisdate thisDate�  0 foldertodelete folderToDelete�  0 todelete toDelete� ����������������������������Zhj����������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  
�� 
cdis
�� 
cfol�� 0 machinefolder machineFolder
�� 
cobj
�� 
ascd
�� .corecnte****       ****
�� 
pnam
�� .ascrcmnt****      � ****
�� 
TEXT
�� 
psxp
�� .sysoexecTEXT���     TEXT�� 0 setfocus setFocus
�� 
appr
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr�� 0 freespaceinit freeSpaceInit�*��/E�O�*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/�&E�O��,�&E�Oa �%j Oa �%a %E�O�j O)j+ Ob  b   Tb  	e  Fb  e  a a a l Okj Y #b  f  a a a l Okj Y hY hY hO*ek+ UV ������������� 
0 backup  ��  ��  � 
���������������������� 0 t  �� 0 x  �� "0 containerfolder containerFolder�� 0 
foldername 
folderName�� 0 d  �� 0 duration  �� 0 msg  �� 0 c  �� 0 oldestfolder oldestFolder�� 0 todelete toDelete� i���������
�����������������������������G����������������������������������ikn���������������				F	H	J	L��	�	�	�	�	�	�	�	�	�	�



)
+
.
=
@
J
M
�
�
�
�
�
�
�
�
�
�
�
�
��� 0 gettimestamp getTimestamp�� (0 animatemenubaricon animateMenuBarIcon
�� .sysodelanull��� ��� nmbr�� 0 	stopwatch  
�� 
psxf�� 0 sourcefolder sourceFolder
�� 
TEXT
�� 
cfol
�� 
pnam
�� 
kocl
�� 
insh�� 0 backupfolder backupFolder
�� 
prdt�� 
�� .corecrel****      � null�� (0 activesourcefolder activeSourceFolder
�� 
cdis�� 	0 drive  �� 0 machinefolder machineFolder�� (0 activebackupfolder activeBackupFolder
�� .misccurdldt    ��� null
�� 
time�� 0 	starttime 	startTime
�� 
appr
�� .sysonotfnull��� ��� TEXT�� "0 destinationdisk destinationDisk
�� .sysoexecTEXT���     TEXT��  ��  �� 0 endtime endTime�� 0 getduration getDuration
� 
cobj� $0 resetmenubaricon resetMenuBarIcon
� .aevtquitnull��� ��� null
� .ascrcmnt****      � ****
� 
alis
� 
psxp���*ek+  E�OeEc  O*j+ Okj O*�k+ O�R*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e b*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_  a !%_ %a "%�%a #%�&E�O a $�%a %%�%a &%j 'OPW X ( )hOfEc  OfEc  Ob  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  Tb  e   a -�%a .%�%a a /l Okj Y )b  f  a 0�%a 1%a a 2l Okj Y hY hO)j+ 3Y hOb  
e  a 4j 5Y hYxb  f m_  a 6%_ %a 7%�%a 8%�%a 9%�&E�O_  a :%_ %a ;%�%a <%�&E�Oa =�%j >Ob  b   l*j a ,E` Ob  a ,.E�Ob  	e  Db  e  �a a ?l Okj Y #b  f  a @a a Al Okj Y hY hY hO  a B�%a C%�%a D%�%a E%j 'OPW X ( )hOfEc  OfEc  O*�/a F&a ,-jv [��/�&E�O�a G,�&E�Oa H�%j >Oa I�%a J%E�O�j 'Ob  b  *j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  �a K  Tb  e   a L�%a M%�%a a Nl Okj Y )b  f  a O�%a P%a a Ql Okj Y hY Qb  e   a R�%a S%�%a a Tl Okj Y )b  f  a U�%a V%a a Wl Okj Y hOb  e  a Xa a Yl Y b  f  a Za a [l Y hY hO)j+ 3Y hY	b  b   �*j a ,E` *O)j+ +E�Ob  a ,.E�Ob  	e  ��a K  Tb  e   a \�%a ]%�%a a ^l Okj Y )b  f  a _�%a `%a a al Okj Y hY Qb  e   a b�%a c%�%a a dl Okj Y )b  f  a e�%a f%a a gl Okj Y hY hY hO)j+ 3Ob  
e  a 4j 5Y hY hUO*a hk+ W �-������ 0 itexists itExists� ��� �  ��� 0 
objecttype 
objectType� 
0 object  �  � ��� 0 
objecttype 
objectType� 
0 object  � e=��K�[�
� 
cdis
� .coredoexnull���     ****
� 
file
� 
cfol� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hUX �~������ .0 getcomputeridentifier getComputerIdentifier�  �  � ���� 0 computername computerName� "0 strserialnumber strSerialNumber�  0 identifiername identifierName� �����
� .sysosigtsirr   ��� null
� 
sicn
� .sysoexecTEXT���     TEXT� *j  �,E�O�j E�O��%�%E�O�Y �������� 0 gettimestamp getTimestamp� ��� �  �� 0 isfolder isFolder�  � �������������������� 0 isfolder isFolder� 0 y  � 0 m  � 0 d  � 0 t  � 0 ty tY� 0 tm tM� 0 td tD� 0 tt tT� 
0 tml tML� 
0 tdl tDL� 0 timestr timeStr� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  � ����������������������������~+�}�|�{J�z�y�x�wv����
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
� .corecnte****       ****
�~ 
nmbr
�} 
tstr
�| misccura
�{ 
psof
�z 
psin�y 
�x .sysooffslong    ��� null
�w 
cha ��*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�Z �v�u�t���s�v 0 comparesizes compareSizes�u �r��r �  �q�p�q 
0 source  �p 0 destination  �t  � �o�n�m�l�k�j�i�h�g�f�e�d�c�b�a�`�o 
0 source  �n 0 destination  �m 0 fit  �l 0 
templatest 
tempLatest�k $0 latestfoldersize latestFolderSize�j 0 c  �i 0 l  �h 0 md  �g 0 	cachesize 	cacheSize�f 0 logssize logsSize�e 0 mdsize mdSize�d 
0 buffer  �c (0 adjustedfoldersize adjustedFolderSize�b 0 
foldersize 
folderSize�a 0 	freespace 	freeSpace�` 0 temp  � !�_�^0�]2�\�[�Z�Y�XFTbvO���W�V�U�T�S�R�Q�����P<>
�_ 
psxf�^ "0 destinationdisk destinationDisk�] 0 machinefolder machineFolder
�\ afdrdlib
�[ 
from
�Z fldmfldu
�Y .earsffdralis        afdr
�X 
psxp
�W .sysoexecTEXT���     TEXT
�V misccura�U �T d
�S .sysorondlong        doub
�R 
cdis
�Q 
frsp
�P .ascrcmnt****      � ****�s�eE�O*�/E�O��%�%�%E�OjE�O���l �,�%E�O���l �,�%E�O���l �,�%E�OjE�OjE�OjE�O�E�OjE�O�*�%a %j E�Oa  �a !a  j Ua !E�O*a �/a ,a !a !a !E�Oa  �a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�Oa �%a %j E�Oa  �a !a  j Ua !E�O����E�O��%�%j Ob  f  1a �%a  %j E�Oa  �a !a  j Ua !E�OPY hUOb  f  ,��E�O�j ��l E�Y hO��� fE�Y hY b  e  ��� fE�Y hY hO�[ �O��N�M���L�O 0 	stopwatch  �N �K��K �  �J�J 0 mode  �M  � �I�H�I 0 mode  �H 0 x  � ��G��F���G 0 gettimestamp getTimestamp
�F .ascrcmnt****      � ****�L 4��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y h\ �E��D�C���B�E 0 getduration getDuration�D  �C  � �A�A 0 duration  � �@�?�>�=�<�;�@ 0 endtime endTime�? 0 	starttime 	startTime�> <
�= misccura�< d
�; .sysorondlong        doub�B ���!E�O� 	�� j U�!E�O�] �:�9�8���7�: 0 setfocus setFocus�9  �8  �  � "�6
�6 .miscactvnull��� ��� null�7 � *j UOP^ �5X�4�3���2�5 $0 menuneedsupdate_ menuNeedsUpdate_�4 �1��1 �  �0
�0 
cmnu�3  �  � �/�/ 0 	makemenus 	makeMenus�2 )j+  _ �.|�-�,���+�. 0 	makemenus 	makeMenus�-  �,  � �*�)�(�* 0 i  �) 0 	this_item  �( 0 thismenuitem thisMenuItem� "�'�&�%�$��#�"�!��� �������
),:HKYfi������'  0 removeallitems removeAllItems
�& 
cobj
�% 
nmbr
�$ 
TEXT
�# misccura�" 0 
nsmenuitem 
NSMenuItem�! 	0 alloc  �  J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� 0 additem_ addItem_� 0 
settarget_ 
setTarget_� 
� 
bool� 0 separatoritem separatorItem�+?b  j+  O3kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y ���&a   ��,j+ �a a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ O�l 
 �a  a  & b  ��,a !,k+ Y h[OY��` ��������  0 backuphandler_ backupHandler_� ��� �  �� 
0 sender  �  � ��� 
0 sender  � 0 msg  � ��������
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY ha �!������ .0 backupandquithandler_ backupAndQuitHandler_� �
��
 �  �	�	 
0 sender  �  � ����� 
0 sender  � 0 	temptitle 	tempTitle� 	0 reply  � 0 msg  � 5?M����X[� ������g��������������
� 
appr
� 
disp� 0 appicon appIcon
� 
btns
�  
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr
�� 
cobj�b  f  �b  e  �E�Y b  f  �E�Y hO�������lv�l� �,E�O��  \eEc  
OeEc  Ob  	e  >b  e  ���l Okj Y !b  f  a �a l Okj Y hY hY �a   fEc  
Y hY Yb  e  Nb  a .E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY hb �������������  0 nsfwonhandler_ nsfwOnHandler_�� ����� �  ���� 
0 sender  ��  � ���� 
0 sender  � ��� ���� �� >eEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y hc ��&���������� "0 nsfwoffhandler_ nsfwOffHandler_�� ����� �  ���� 
0 sender  ��  � ���� 
0 sender  � :>BFI��VZ^be�� �� >fEc  Ob  	e  ������vEc  Y b  	f  ������vEc  Y hd ��t���������� 00 notificationonhandler_ notificationOnHandler_�� ����� �  ���� 
0 sender  ��  � ���� 
0 sender  � �������������� �� >eEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y he ������������� 20 notificationoffhandler_ notificationOffHandler_�� ����� �  ���� 
0 sender  ��  � ���� 
0 sender  � �������������� �� >fEc  	Ob  e  ������vEc  Y b  f  ������vEc  Y hf ������������ 0 quithandler_ quitHandler_�� ����� �  ���� 
0 sender  ��  � �������� 
0 sender  �� 0 	temptitle 	tempTitle�� 	0 reply  � ��",:��������EH��������R\��`��y��������� 0 setfocus setFocus
�� 
appr
�� 
disp�� 0 appicon appIcon
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit
�� .sysoexecTEXT���     TEXT
�� .ascrcmnt****      � ****�� �b  e  W)j+  Ob  e  �E�Y b  f  �E�Y hO�������lv�l� �,E�O��  
�j Y 	a j Y pb  f  e)j+  Ob  e  
a E�Y b  f  
a E�Y hOa ����a a lv�l� �,E�O�a   a j Y 	a j Y hg ������������� (0 animatemenubaricon animateMenuBarIcon��  ��  � ������ 0 	imagepath 	imagePath�� 	0 image  � �����������
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr
� 
psxp
� misccura� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_�� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
h �������� $0 resetmenubaricon resetMenuBarIcon�  �  � ��� 0 	imagepath 	imagePath� 	0 image  � ����������
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr
� 
psxp
� misccura� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_� /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
i �3������ 0 makestatusbar makeStatusBar�  �  � ���� 0 bar  � 0 	imagepath 	imagePath� 	0 image  � ���L�����[����������
� misccura� 0 nsstatusbar NSStatusBar� "0 systemstatusbar systemStatusBar� .0 statusitemwithlength_ statusItemWithLength_
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr
� 
psxp� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_� 0 nsmenu NSMenu�  0 initwithtitle_ initWithTitle_� 0 setdelegate_ setDelegate_� 0 setmenu_ setMenu_� s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ j �������� 0 runonce runOnce�  �  �  � ��� (0 animatemenubaricon animateMenuBarIcon� 0 	plistinit 	plistInit� *j+  O*j+ k �������
� .miscidlenmbr    ��� null�  �  � �� 0 m  � �����~�}�|�{
� .misccurdldt    ��� null
� 
min � � -
�~ 
bool�} 0 scheduledtime scheduledTime�| 0 	plistinit 	plistInit�{ � �b  b   vb  f  hb  f  H*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hOPY b  e  
*j+ Y hY hY hO�l �z��y�x���w
�z .aevtoappnull  �   � ****� k     ��  ��� ��� ��v�v  �y  �x  �  � �u�t�s�r ��q�p�o
�u 
alis
�t 
rtyp
�s 
ctxt
�r .earsffdralis        afdr�q 0 appicon appIcon�p 0 makestatusbar makeStatusBar�o 0 runonce runOnce�w *�)��l �%/E�O)j+ O*j+ ascr  ��ޭ