FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S \�� ��� 0 thelist theList � J   S [ � �  � � � m   S V � � � � �  B a c k u p   n o w �  ��� � m   V Y � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   ] ] � � �� ��� 	0 plist   � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � �� ��� 0 latestfolder latestFolder � ������ 0 initialbackup initialBackup��   �  � � � p   ] ] � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ������ 0 endtime endTime��   �  � � � p   ] ] � � ������ 0 manualbackup manualBackup��   �  � � � p   ] ] � � �� ��� 0 isbackingup isBackingUp � �� ��� "0 messagesmissing messagesMissing � �� ��� *0 messagesencouraging messagesEncouraging � �� ��� $0 messagescomplete messagesComplete � ������ &0 messagescancelled messagesCancelled��   �  � � � p   ] ] � � �� ��� 0 strawake strAwake � �� ��� 0 strsleep strSleep � ������ &0 displaysleepstate displaySleepState��   �  � � � l     �������  ��  �   �  � � � l     �~�}�|�~  �}  �|   �  � � � l     �{ � ��{   � !  Global variable assignment    � � � � 6   G l o b a l   v a r i a b l e   a s s i g n m e n t �  � � � l     ��z�y � r      � � � b      � � � n    	 � � � 1    	�x
�x 
psxp � l     ��w�v � I    �u � �
�u .earsffdralis        afdr � m     �t
�t afdrdlib � �s ��r
�s 
from � m    �q
�q fldmfldu�r  �w  �v   � m   	 
 � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t � o      �p�p 	0 plist  �z  �y   �  � � � l     �o�n�m�o  �n  �m   �  � � � l    ��l�k � r     � � � m    �j
�j boovtrue � o      �i�i 0 initialbackup initialBackup�l  �k   �  � � � l    ��h�g � r     � � � m    �f
�f boovfals � o      �e�e 0 manualbackup manualBackup�h  �g   �  � � � l    ��d�c � r     � � � m    �b
�b boovfals � o      �a�a 0 isbackingup isBackingUp�d  �c   �  � � � l     �`�_�^�`  �_  �^   �  � � � l   % ��]�\ � r    % � � � J    ! � �  � � � m     � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m     � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �  � � � m     � � � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d ! �  � � � m     � � � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �  ��[ � m     � � �   � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�[   � o      �Z�Z "0 messagesmissing messagesMissing�]  �\   �  l     �Y�X�W�Y  �X  �W    l  & I�V�U r   & I J   & E 	
	 m   & ) �  C o m e   o n !
  m   ) , � 4 C o m e   o n !   E y e   o f   t h e   t i g e r !  m   , / � " N o   p a i n !   N o   p a i n !  m   / 2 � 0 L e t ' s   d o   t h i s   a s   a   t e a m !  m   2 5 �  W e   c a n   d o   i t !  m   5 8 �    F r e e e e e e e e e d o m ! !"! m   8 ;## �$$ 2 A l t o g e t h e r   o r   n o t   a t   a l l !" %&% m   ; >'' �(( H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !& )�T) m   > A** �++ H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�T   o      �S�S *0 messagesencouraging messagesEncouraging�V  �U   ,-, l     �R�Q�P�R  �Q  �P  - ./. l  J m0�O�N0 r   J m121 J   J i33 454 m   J M66 �77  N i c e   o n e !5 898 m   M P:: �;; * Y o u   a b s o l u t e   l e g   e n d !9 <=< m   P S>> �??  G o o d   l a d != @A@ m   S VBB �CC  P e r f i c k !A DED m   V YFF �GG  H a p p y   d a y s !E HIH m   Y \JJ �KK  F r e e e e e e e e e d o m !I LML m   \ _NN �OO H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !M PQP m   _ bRR �SS B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !Q T�MT m   b eUU �VV d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�M  2 o      �L�L $0 messagescomplete messagesComplete�O  �N  / WXW l     �K�J�I�K  �J  �I  X YZY l  n �[�H�G[ r   n �\]\ J   n �^^ _`_ m   n qaa �bb  T h a t ' s   a   s h a m e` cdc m   q tee �ff  A h   m a n ,   u n l u c k yd ghg m   t wii �jj  O h   w e l lh klk m   w zmm �nn @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e nl opo m   z }qq �rr , W e l l   t h a t ' s   a   l e t   d o w np sts m   } �uu �vv P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?t wxw m   � �yy �zz  A r r o g a n tx {�F{ m   � �|| �}} > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�F  ] o      �E�E &0 messagescancelled messagesCancelled�H  �G  Z ~~ l     �D�C�B�D  �C  �B   ��� l  � ���A�@� r   � ���� J   � ��� ��� m   � ��� ���  T h a t ' s   a   s h a m e� ��� m   � ��� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m   � ��� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m   � ��� ���  D i c k� ��� m   � ��� ���  F u c k t a r d� ��� m   � ��� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m   � ��� ���  A r r o g a n t� ��� m   � ��� ��� $ C h i l l   t h e   f u c k   o u t� ��?� m   � ��� ���  H o l d   f i r e�?  � o      �>�> 40 messagesalreadybackingup messagesAlreadyBackingUp�A  �@  � ��� l     �=�<�;�=  �<  �;  � ��� l  � ���:�9� r   � ���� m   � ��� ��� * " C u r r e n t P o w e r S t a t e " = 4� o      �8�8 0 strawake strAwake�:  �9  � ��� l  � ���7�6� r   � ���� m   � ��� ��� * " C u r r e n t P o w e r S t a t e " = 1� o      �5�5 0 strsleep strSleep�7  �6  � ��� l  � ���4�3� r   � ���� I  � ��2��1
�2 .sysoexecTEXT���     TEXT� m   � ��� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�1  � o      �0�0 &0 displaysleepstate displaySleepState�4  �3  � ��� l     �/�.�-�/  �.  �-  � ��� l     �,�+�*�,  �+  �*  � ��� l     �)�(�'�)  �(  �'  � ��� l     �&���&  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �%���%  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �$���$  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �#�"�!�#  �"  �!  � ��� l     � ���   � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     ����  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     ����  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i   ] `��� I      ���� 0 	plistinit 	plistInit�  �  � k    ��� ��� l     ����  �   display dialog "PLISTINIT"   � ��� 4 d i s p l a y   d i a l o g   " P L I S T I N I T "� ��� q      �� ��� 0 
foldername 
folderName� ��� 0 
backupdisk 
backupDisk� ���  0 computerfolder computerFolder� ��� 0 
backuptime 
backupTime�  � ��� r     ��� m     ��  � o      �� 0 
backuptime 
backupTime� ��� l   ����  �  �  � ��� Z   �� �� l   �� =     I    ��� 0 itexists itExists  m     �		  f i l e 
�

 o    �	�	 	0 plist  �
  �   m    �
� boovtrue�  �    O    = k    <  r     n     1    �
� 
pcnt 4    �
� 
plif o    �� 	0 plist   o      �� 0 thecontents theContents  r    " n      1     �
� 
valL o    �� 0 thecontents theContents o      �� 0 thevalue theValue  l  # #� �����   ��  ��    r   # (  n   # &!"! o   $ &����  0 foldertobackup FolderToBackup" o   # $���� 0 thevalue theValue  o      ���� 0 
foldername 
folderName #$# r   ) .%&% n   ) ,'(' o   * ,���� 0 backupdrive BackupDrive( o   ) *���� 0 thevalue theValue& o      ���� 0 
backupdisk 
backupDisk$ )*) r   / 4+,+ n   / 2-.- o   0 2����  0 computerfolder computerFolder. o   / 0���� 0 thevalue theValue, o      ����  0 computerfolder computerFolder* /0/ r   5 :121 n   5 8343 o   6 8���� 0 scheduledtime scheduledTime4 o   5 6���� 0 thevalue theValue2 o      ���� 0 
backuptime 
backupTime0 565 l  ; ;��������  ��  ��  6 7��7 l  ; ;��89��  8 . (log {folderName, backupDisk, backupTime}   9 �:: P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��   m    ;;�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �   k   @�<< =>= r   @ G?@? I   @ E�������� .0 getcomputeridentifier getComputerIdentifier��  ��  @ o      ����  0 computerfolder computerFolder> ABA l  H H��������  ��  ��  B CDC O   H �EFE t   L �GHG k   N �II JKJ l  N N��LM��  L  
setFocus()   M �NN  s e t F o c u s ( )K OPO l  N N��������  ��  ��  P QRQ r   N WSTS l  N UU����U I  N U����V
�� .sysostflalis    ��� null��  V ��W��
�� 
prmpW m   P QXX �YY @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p��  ��  ��  T o      ���� 0 
foldername 
folderNameR Z[Z r   X g\]\ c   X e^_^ l  X a`����` I  X a����a
�� .sysostflalis    ��� null��  a ��b��
�� 
prmpb m   Z ]cc �dd R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  _ m   a d��
�� 
TEXT] o      ���� 0 
backupdisk 
backupDisk[ efe l  h h��������  ��  ��  f ghg r   h oiji n   h mklk 1   i m��
�� 
psxpl o   h i���� 0 
foldername 
folderNamej o      ���� 0 
foldername 
folderNameh mnm r   p wopo n   p uqrq 1   q u��
�� 
psxpr o   p q���� 0 
backupdisk 
backupDiskp o      ���� 0 
backupdisk 
backupDiskn sts l  x x��������  ��  ��  t uvu r   x �wxw J   x �yy z{z m   x {|| �}} 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r{ ~~ m   { ~�� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r ���� m   ~ ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  x o      ���� 0 backuptimes backupTimesv ��� r   � ���� c   � ���� J   � ��� ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  � m   � ���
�� 
TEXT� o      ���� 0 selectedtime selectedTime� ��� l  � �������  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l  � ���������  ��  ��  � ���� l  � �������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  H m   L M����  ��F m   H I���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  D ��� l  � ���������  ��  ��  � ���� O   ����� O   ����� k   ���� ��� I  ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
plii� ����
�� 
insh�  ;   � �� �����
�� 
prdt� K   �
�� ����
�� 
kind� m   � ���
�� 
TEXT� ����
�� 
pnam� m  �� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ���� 0 
foldername 
folderName��  ��  � ��� I 8�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  ��
�� 
plii� ����
�� 
insh�  ;  � �����
�� 
prdt� K   2�� ����
�� 
kind� m  #&��
�� 
TEXT� ����
�� 
pnam� m  ),�� ���  B a c k u p D r i v e� �����
�� 
valL� o  -.���� 0 
backupdisk 
backupDisk��  ��  � ��� I 9`�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  =@��
�� 
plii� ����
�� 
insh�  ;  CE� ���~
� 
prdt� K  HZ�� �}��
�} 
kind� m  KN�|
�| 
TEXT� �{��
�{ 
pnam� m  QT�� ���  C o m p u t e r F o l d e r� �z��y
�z 
valL� o  UV�x�x  0 computerfolder computerFolder�y  �~  � ��w� I a��v�u�
�v .corecrel****      � null�u  � �t��
�t 
kocl� m  eh�s
�s 
plii� �r��
�r 
insh�  ;  km� �q��p
�q 
prdt� K  p�   �o
�o 
kind m  sv�n
�n 
TEXT �m
�m 
pnam m  y| �  S c h e d u l e d T i m e �l�k
�l 
valL o  }~�j�j 0 
backuptime 
backupTime�k  �p  �w  � l  � ��i�h I  � ��g�f	
�g .corecrel****      � null�f  	 �e

�e 
kocl
 m   � ��d
�d 
plif �c�b
�c 
prdt K   � � �a�`
�a 
pnam o   � ��_�_ 	0 plist  �`  �b  �i  �h  � m   � ��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  �  l ���^�]�\�^  �]  �\    r  �� o  ���[�[ 0 
foldername 
folderName o      �Z�Z 0 sourcefolder sourceFolder  r  �� o  ���Y�Y 0 
backupdisk 
backupDisk o      �X�X "0 destinationdisk destinationDisk  r  �� o  ���W�W  0 computerfolder computerFolder o      �V�V 0 machinefolder machineFolder  r  �� !  o  ���U�U 0 
backuptime 
backupTime! o      �T�T 0 scheduledtime scheduledTime "#" I ���S$�R
�S .ascrcmnt****      � ****$ J  ��%% &'& o  ���Q�Q 0 sourcefolder sourceFolder' ()( o  ���P�P "0 destinationdisk destinationDisk) *+* o  ���O�O 0 machinefolder machineFolder+ ,�N, o  ���M�M 0 scheduledtime scheduledTime�N  �R  # -.- l ���L�K�J�L  �K  �J  . /�I/ I  ���H�G�F�H 0 diskinit diskInit�G  �F  �I  � 010 l     �E�D�C�E  �D  �C  1 232 l     �B�A�@�B  �A  �@  3 454 l     �?�>�=�?  �>  �=  5 676 l     �<�;�:�<  �;  �:  7 898 l     �9�8�7�9  �8  �7  9 :;: l     �6<=�6  < H B Function to detect if the selected hard drive is connected or not   = �>> �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t; ?@? l     �5AB�5  A T N This only happens once a hard drive has been selected and provides a reminder   B �CC �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r@ DED i   a dFGF I      �4�3�2�4 0 diskinit diskInit�3  �2  G k     YHH IJI l     �1KL�1  K  display dialog "DISKINIT"   L �MM 2 d i s p l a y   d i a l o g   " D I S K I N I T "J N�0N Z     YOP�/QO l    	R�.�-R =     	STS I     �,U�+�, 0 itexists itExistsU VWV m    XX �YY  d i s kW Z�*Z o    �)�) "0 destinationdisk destinationDisk�*  �+  T m    �(
�( boovtrue�.  �-  P I    �'[�&�' 0 freespaceinit freeSpaceInit[ \�%\ m    �$
�$ boovfals�%  �&  �/  Q k    Y]] ^_^ l   �#`a�#  `  
setFocus()   a �bb  s e t F o c u s ( )_ cdc r    efe n    ghg 3    �"
�" 
cobjh o    �!�! "0 messagesmissing messagesMissingf o      � �  0 msg  d iji r    +klk l   )m��m n    )non 1   ' )�
� 
bhito l   'p��p I   '�qr
� .sysodlogaskr        TEXTq o    �� 0 msg  r �st
� 
btnss J    !uu vwv m    xx �yy  C a n c e l   B a c k u pw z�z m    {{ �||  O K�  t �}�
� 
dflt} m   " #�� �  �  �  �  �  l o      �� 	0 reply  j ~�~ Z   , Y��� l  , /���� =   , /��� o   , -�� 	0 reply  � m   - .�� ���  O K�  �  � I   2 7���� 0 diskinit diskInit�  �  �  � Z   : Y���
�	� l  : ?���� E   : ?��� o   : ;�� &0 displaysleepstate displaySleepState� o   ; >�� 0 strawake strAwake�  �  � k   B U�� ��� r   B I��� n   B G��� 3   E G�
� 
cobj� o   B E�� &0 messagescancelled messagesCancelled� o      �� 0 msg  � ��� I  J U� ��
�  .sysonotfnull��� ��� TEXT� o   J K���� 0 msg  � �����
�� 
appr� m   N Q�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  �  �
  �	  �  �0  E ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i   e h��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � k     y�� ��� l     ������  � $ display dialog "FREESPACEINIT"   � ��� < d i s p l a y   d i a l o g   " F R E E S P A C E I N I T "� ���� Z     y������ l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    y�� ��� l   ������  �  
setFocus()   � ���  s e t F o c u s ( )� ��� l   ��������  ��  ��  � ��� Z    G������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r    *��� l   (������ n    (��� 1   & (��
�� 
bhit� l   &������ I   &����
�� .sysodlogaskr        TEXT� m    �� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J     �� ��� m    �� ���  Y e s� ���� m    �� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   ! "���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  � ��� l  - 0������ =   - 0��� o   - .���� 	0 again  � m   . /��
�� boovtrue��  ��  � ���� r   3 C��� l  3 A������ n   3 A��� 1   ? A��
�� 
bhit� l  3 ?������ I  3 ?����
�� .sysodlogaskr        TEXT� m   3 4�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   5 9    m   5 6 �  Y e s �� m   6 7 �  C a n c e l   B a c k u p��  � ����
�� 
dflt m   : ;���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  ��  ��  � 	
	 l  H H��������  ��  ��  
 �� Z   H y�� l  H K���� =   H K o   H I���� 
0 reply1   m   I J �  Y e s��  ��   I   N S�������� 0 tidyup tidyUp��  ��  ��   k   V y  Z   V m���� l  V ]���� E   V ] o   V Y���� &0 displaysleepstate displaySleepState o   Y \���� 0 strawake strAwake��  ��   r   ` i n   ` g 3   c g��
�� 
cobj o   ` c���� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��    ��  I  n y��!"
�� .sysonotfnull��� ��� TEXT! o   n o���� 0 msg  " ��#��
�� 
appr# m   r u$$ �%% > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  ��  ��  ��  � &'& l     ��������  ��  ��  ' ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��������  ��  ��  - ./. l     ��������  ��  ��  / 010 l     ��23��  2,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   3 �44L   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .1 565 l     ��78��  7 g a These folders, if required, are then set to their corresponding global variables declared above.   8 �99 �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .6 :;: i   i l<=< I      �������� 0 
backupinit 
backupInit��  ��  = k     �>> ?@? l     ��AB��  A ! display dialog "BACKUPINIT"   B �CC 6 d i s p l a y   d i a l o g   " B A C K U P I N I T "@ DED r     FGF 4     ��H
�� 
psxfH o    ���� "0 destinationdisk destinationDiskG o      ���� 	0 drive  E IJI l   ��KL��  K ' !log (destinationDisk & "Backups")   L �MM B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )J NON l   ����~��  �  �~  O PQP Z    ,RS�}�|R l   T�{�zT =    UVU I    �yW�x�y 0 itexists itExistsW XYX m    	ZZ �[[  f o l d e rY \�w\ b   	 ]^] o   	 
�v�v "0 destinationdisk destinationDisk^ m   
 __ �``  B a c k u p s�w  �x  V m    �u
�u boovfals�{  �z  S O    (aba I   '�t�sc
�t .corecrel****      � null�s  c �rde
�r 
kocld m    �q
�q 
cfole �pfg
�p 
inshf o    �o�o 	0 drive  g �nh�m
�n 
prdth K    #ii �lj�k
�l 
pnamj m     !kk �ll  B a c k u p s�k  �m  b m    mm�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �}  �|  Q non l  - -�j�i�h�j  �i  �h  o pqp Z   - drs�g�fr l  - >t�e�dt =   - >uvu I   - <�cw�b�c 0 itexists itExistsw xyx m   . /zz �{{  f o l d e ry |�a| b   / 8}~} b   / 4� o   / 0�`�` "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /~ o   4 7�_�_ 0 machinefolder machineFolder�a  �b  v m   < =�^
�^ boovfals�e  �d  s O   A `��� I  E _�]�\�
�] .corecrel****      � null�\  � �[��
�[ 
kocl� m   G H�Z
�Z 
cfol� �Y��
�Y 
insh� n   I T��� 4   O T�X�
�X 
cfol� m   P S�� ���  B a c k u p s� 4   I O�W�
�W 
cdis� o   M N�V�V 	0 drive  � �U��T
�U 
prdt� K   U [�� �S��R
�S 
pnam� o   V Y�Q�Q 0 machinefolder machineFolder�R  �T  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �g  �f  q ��� l  e e�P�O�N�P  �O  �N  � ��� O   e ���� k   i �� ��� l  i i�M���M  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y�L�
�L 
cfol� o   u x�K�K 0 machinefolder machineFolder� n   i t��� 4   o t�J�
�J 
cfol� m   p s�� ���  B a c k u p s� 4   i o�I�
�I 
cdis� o   m n�H�H 	0 drive  � o      �G�G 0 backupfolder backupFolder� ��F� l  ~ ~�E���E  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�F  � m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��D�C�B�D  �C  �B  � ��� Z   � ����A�� l  � ���@�?� =   � ���� I   � ��>��=�> 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��<� b   � ���� b   � ���� b   � ���� o   � ��;�; "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��:�: 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�<  �=  � m   � ��9
�9 boovfals�@  �?  � O   � ���� I  � ��8�7�
�8 .corecrel****      � null�7  � �6��
�6 
kocl� m   � ��5
�5 
cfol� �4��
�4 
insh� o   � ��3�3 0 backupfolder backupFolder� �2��1
�2 
prdt� K   � ��� �0��/
�0 
pnam� m   � ��� ���  L a t e s t�/  �1  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �A  � r   � ���� m   � ��.
�. boovfals� o      �-�- 0 initialbackup initialBackup� ��� l  � ��,�+�*�,  �+  �*  � ��� O   � ���� k   � ��� ��� r   � ���� n   � ���� 4   � ��)�
�) 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ��(�
�( 
cfol� o   � ��'�' 0 machinefolder machineFolder� n   � ���� 4   � ��&�
�& 
cfol� m   � ��� ���  B a c k u p s� 4   � ��%�
�% 
cdis� o   � ��$�$ 	0 drive  � o      �#�# 0 latestfolder latestFolder� ��"� l  � ��!���!  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�"  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � �� ���   �  �  � ��� I   � ����� 
0 backup  �  �  �  ; ��� l     ����  �  �  � ��� l     ����  �  �  �    l     ����  �  �    l     ����  �  �    l     ����  �  �    l     �
	�
   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.   	 �

 �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .  l     �	�	   � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.    ��   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .  l     ��   � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.    �P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .  l     ��   � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.    �>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .  i   m p I      ���� 0 tidyup tidyUp�  �   k     �   l     �!"�  !  display dialog "TIDYUP"   " �## . d i s p l a y   d i a l o g   " T I D Y U P "  $%$ r     &'& 4     �(
� 
psxf( o    �� "0 destinationdisk destinationDisk' o      � �  	0 drive  % )*) l   ��������  ��  ��  * +��+ O    �,-, k    �.. /0/ l   ��12��  1 A ;set creationDates to creation date of items of backupFolder   2 �33 v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e r0 454 r    676 n    898 4    ��:
�� 
cfol: o    ���� 0 machinefolder machineFolder9 n    ;<; 4    ��=
�� 
cfol= m    >> �??  B a c k u p s< 4    ��@
�� 
cdis@ o    ���� 	0 drive  7 o      ���� 0 bf bF5 ABA r    CDC n    EFE 1    ��
�� 
ascdF n    GHG 2   ��
�� 
cobjH o    ���� 0 bf bFD o      ���� 0 creationdates creationDatesB IJI r     &KLK n     $MNM 4   ! $��O
�� 
cobjO m   " #���� N o     !���� 0 creationdates creationDatesL o      ���� 0 theoldestdate theOldestDateJ PQP r   ' *RSR m   ' (���� S o      ���� 0 j  Q TUT l  + +��������  ��  ��  U VWV Y   + nX��YZ��X k   9 i[[ \]\ r   9 ?^_^ n   9 =`a` 4   : =��b
�� 
cobjb o   ; <���� 0 i  a o   9 :���� 0 creationdates creationDates_ o      ���� 0 thisdate thisDate] c��c Z   @ ide����d l  @ Hf����f >  @ Hghg n   @ Fiji 1   D F��
�� 
pnamj 4   @ D��k
�� 
cobjk o   B C���� 0 i  h m   F Gll �mm  L a t e s t��  ��  e Z   K eno����n l  K Np����p A   K Nqrq o   K L���� 0 thisdate thisDater o   L M���� 0 theoldestdate theOldestDate��  ��  o k   Q ass tut r   Q Tvwv o   Q R���� 0 thisdate thisDatew o      ���� 0 theoldestdate theOldestDateu xyx r   U Xz{z o   U V���� 0 i  { o      ���� 0 j  y |}| l  Y Y��~��  ~ " log (item j of backupFolder)    ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )} ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  Y m   . /���� Z I  / 4�����
�� .corecnte****       ****� o   / 0���� 0 creationdates creationDates��  ��  W ��� l  o o��������  ��  ��  � ��� l  o o������  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� I  o y�����
�� .coredeloobj        obj � n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  � ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l   z z������  � � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l  z z������  �  
setFocus()   � ���  s e t F o c u s ( )� ��� r   z ���� l  z ������� n   z ���� 1   � ���
�� 
bhit� l  z ������� I  z �����
�� .sysodlogaskr        TEXT� m   z {�� ���B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .� ����
�� 
btns� J   ~ ��� ��� m   ~ ��� ���  E m p t y   T r a s h� ���� m   � ��� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   � ����� ��  ��  ��  ��  ��  � o      ���� 
0 reply2  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 
0 reply2  � m   � ��� ���  E m p t y   T r a s h��  ��  � I  � ������
�� .fndremptnull��� ��� obj � l  � ������� 1   � ���
�� 
trsh��  ��  ��  ��  � Z   � �������� l  � ������� E   � ���� o   � ����� &0 displaysleepstate displaySleepState� o   � ����� 0 strawake strAwake��  ��  � k   � ��� ��� r   � ���� n   � ���� 3   � ���
�� 
cobj� o   � ����� &0 messagescancelled messagesCancelled� o      ���� 0 msg  � ���� I  � �����
�� .sysonotfnull��� ��� TEXT� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� I   � �������� 0 freespaceinit freeSpaceInit� ��� m   � ��~
�~ boovtrue�  ��  � ��}� Z   � ����|�{� l  � ���z�y� E   � ���� o   � ��x�x &0 displaysleepstate displaySleepState� o   � ��w�w 0 strawake strAwake�z  �y  � k   � ��� ��� I  � ��v��
�v .sysonotfnull��� ��� TEXT� m   � ��� ��� � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .� �u��t
�u 
appr� m   � ��� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�t  � ��s� I  � ��r��q
�r .sysodelanull��� ��� nmbr� m   � ��p�p �q  �s  �|  �{  �}  - m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��   ��� l     �o�n�m�o  �n  �m  � � � l     �l�k�j�l  �k  �j     l     �i�h�g�i  �h  �g    l     �f�e�d�f  �e  �d    l     �c�b�a�c  �b  �a    l     �`	
�`  	 M G Function that carries out the backup process and is split in 2 parts.    
 � �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .    l     �_�_  *$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.    �H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .  l     �^�^   � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.    �   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .  i   q t I      �]�\�[�] 
0 backup  �\  �[   k      l     �Z�Z    display dialog "BACKUP"    � . d i s p l a y   d i a l o g   " B A C K U P "  !  q      "" �Y#�Y 0 t  # �X$�X 0 x  $ �W�V�W "0 containerfolder containerFolder�V  ! %&% r     '(' I     �U)�T�U 0 gettimestamp getTimestamp) *�S* m    �R
�R boovtrue�S  �T  ( o      �Q�Q 0 t  & +,+ l  	 	�P�O�N�P  �O  �N  , -.- r   	 /0/ m   	 
�M
�M boovtrue0 o      �L�L 0 isbackingup isBackingUp. 121 l   �K�J�I�K  �J  �I  2 343 I    �H5�G�H 0 	stopwatch  5 6�F6 m    77 �88 
 s t a r t�F  �G  4 9:9 l   �E�D�C�E  �D  �C  : ;<; O   =>= k   ?? @A@ l   �BBC�B  B f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   C �DD �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e dA EFE l   �AGH�A  G x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   H �II �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u pF JKJ r     LML c    NON 4    �@P
�@ 
psxfP o    �?�? 0 sourcefolder sourceFolderO m    �>
�> 
TEXTM o      �=�= 0 
foldername 
folderNameK QRQ r   ! )STS n   ! 'UVU 1   % '�<
�< 
pnamV 4   ! %�;W
�; 
cfolW o   # $�:�: 0 
foldername 
folderNameT o      �9�9 0 
foldername 
folderNameR XYX l  * *�8Z[�8  Z  log (folderName)		   [ �\\ $ l o g   ( f o l d e r N a m e ) 	 	Y ]^] l  * *�7�6�5�7  �6  �5  ^ _`_ Z   * �ab�4�3a l  * -c�2�1c =   * -ded o   * +�0�0 0 initialbackup initialBackupe m   + ,�/
�/ boovfals�2  �1  b k   0 ff ghg I  0 >�.�-i
�. .corecrel****      � null�-  i �,jk
�, 
koclj m   2 3�+
�+ 
cfolk �*lm
�* 
inshl o   4 5�)�) 0 backupfolder backupFolderm �(n�'
�( 
prdtn K   6 :oo �&p�%
�& 
pnamp o   7 8�$�$ 0 t  �%  �'  h qrq l  ? ?�#�"�!�#  �"  �!  r sts r   ? Iuvu c   ? Ewxw 4   ? C� y
�  
psxfy o   A B�� 0 sourcefolder sourceFolderx m   C D�
� 
TEXTv o      �� (0 activesourcefolder activeSourceFoldert z{z r   J T|}| 4   J P�~
� 
cfol~ o   L O�� (0 activesourcefolder activeSourceFolder} o      �� (0 activesourcefolder activeSourceFolder{ � l  U U����  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   U n��� n   U j��� 4   g j��
� 
cfol� o   h i�� 0 t  � n   U g��� 4   b g��
� 
cfol� o   c f�� 0 machinefolder machineFolder� n   U b��� 4   ] b��
� 
cfol� m   ^ a�� ���  B a c k u p s� 4   U ]��
� 
cdis� o   Y \�� 	0 drive  � o      �� (0 activebackupfolder activeBackupFolder� ��� l  o o����  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ��� I  o ���
� .corecrel****      � null�  � ���
� 
kocl� m   q r�
� 
cfol� �
��
�
 
insh� o   s v�	�	 (0 activebackupfolder activeBackupFolder� ���
� 
prdt� K   w {�� ���
� 
pnam� o   x y�� 0 
foldername 
folderName�  �  �  �4  �3  ` ��� l  � �����  �  �  � ��� l  � �� �����   ��  ��  � ��� l  � �������  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �������  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � �������  � D > This means that only new or updated files will be copied over   � ��� |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r� ��� l  � �������  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	� ��� l  � �������  �   Original sync code    � ��� (   O r i g i n a l   s y n c   c o d e  � ��� l  � �������  � z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   � ��� �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Original code   � ���    O r i g i n a l   c o d e� ��� l  � �������  � i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   � ��� �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h� ��� l  � �������  � x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   � ��� �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .� ��� l  � �������  � p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   � ��� �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .� ��� l  � �������  � � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   � ����   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .� ��� l  � �������  � � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   � ���v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .� ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ���� Z   ������� l  � ������� =   � ���� o   � ����� 0 initialbackup initialBackup� m   � ���
�� boovtrue��  ��  � k   �5�� ��� r   � ���� n   � ���� 1   � ���
�� 
time� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 	starttime 	startTime� ��� Z   � �������� l  � ������� E   � ���� o   � ����� &0 displaysleepstate displaySleepState� o   � ����� 0 strawake strAwake��  ��  � I  � �����
�� .sysonotfnull��� ��� TEXT� m   � �   � � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .� ����
�� 
appr m   � � � 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  ��  ��  �  l  � ���������  ��  ��    l  � ���������  ��  ��   	
	 l  � ���������  ��  ��  
  l  � ���������  ��  ��    r   � � c   � � l  � ����� b   � � b   � � b   � � b   � � b   � � o   � ����� "0 destinationdisk destinationDisk m   � � �  B a c k u p s / o   � ����� 0 machinefolder machineFolder m   � �   �!!  / L a t e s t / o   � ����� 0 
foldername 
folderName m   � �"" �##  /��  ��   m   � ���
�� 
TEXT o      ���� 0 d   $%$ l  � ���&'��  & D >do shell script "ditto '" & sourceFolder & "' '" & d & "/'"			   ' �(( | d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' " 	 	 	% )*) I  � ���+��
�� .sysoexecTEXT���     TEXT+ b   � �,-, b   � �./. b   � �010 b   � �232 m   � �44 �55� r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   '3 o   � ����� 0 sourcefolder sourceFolder1 m   � �66 �77  '   '/ o   � ����� 0 d  - m   � �88 �99  / '��  * :;: l  � ���������  ��  ��  ; <=< l  � ���������  ��  ��  = >?> l  � ���������  ��  ��  ? @A@ l  � ���������  ��  ��  A BCB r   � �DED m   � ���
�� boovfalsE o      ���� 0 isbackingup isBackingUpC FGF r   � �HIH m   � ���
�� boovfalsI o      ���� 0 manualbackup manualBackupG JKJ l  � ���������  ��  ��  K L��L Z   �5MN����M l  � �O����O E   � �PQP o   � ����� &0 displaysleepstate displaySleepStateQ o   � ����� 0 strawake strAwake��  ��  N k   �1RR STS r   �UVU n   � �WXW 1   � ���
�� 
timeX l  � �Y����Y I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  V o      ���� 0 endtime endTimeT Z[Z r  \]\ n 	^_^ I  	�������� 0 getduration getDuration��  ��  _  f  ] o      ���� 0 duration  [ `a` r  bcb n  ded 3  ��
�� 
cobje o  ���� $0 messagescomplete messagesCompletec o      ���� 0 msg  a fgf I +��hi
�� .sysonotfnull��� ��� TEXTh b  !jkj b  lml b  non m  pp �qq 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  o o  ���� 0 duration  m m  rr �ss    m i n u t e s ! 
k o   ���� 0 msg  i ��t��
�� 
apprt m  $'uu �vv : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  g w��w I ,1��x��
�� .sysodelanull��� ��� nmbrx m  ,-���� ��  ��  ��  ��  ��  � yzy l 8;{����{ =  8;|}| o  89���� 0 initialbackup initialBackup} m  9:��
�� boovfals��  ��  z ~��~ k  > ��� r  >]��� c  >[��� l >Y������ b  >Y��� b  >U��� b  >S��� b  >O��� b  >M��� b  >I��� b  >E��� o  >A���� "0 destinationdisk destinationDisk� m  AD�� ���  B a c k u p s /� o  EH���� 0 machinefolder machineFolder� m  IL�� ���  /� o  MN���� 0 t  � m  OR�� ���  /� o  ST�� 0 
foldername 
folderName� m  UX�� ���  /��  ��  � m  YZ�~
�~ 
TEXT� o      �}�} 0 c  � ��� r  ^w��� c  ^u��� l ^s��|�{� b  ^s��� b  ^o��� b  ^m��� b  ^i��� b  ^e��� o  ^a�z�z "0 destinationdisk destinationDisk� m  ad�� ���  B a c k u p s /� o  eh�y�y 0 machinefolder machineFolder� m  il�� ���  / L a t e s t /� o  mn�x�x 0 
foldername 
folderName� m  or�� ���  /�|  �{  � m  st�w
�w 
TEXT� o      �v�v 0 d  � ��� I x��u��t
�u .ascrcmnt****      � ****� l x}��s�r� b  x}��� m  x{�� ���  b a c k i n g   u p   t o :  � o  {|�q�q 0 d  �s  �r  �t  � ��� l ���p�o�n�p  �o  �n  � ��� Z  �����m�l� l ����k�j� E  ����� o  ���i�i &0 displaysleepstate displaySleepState� o  ���h�h 0 strawake strAwake�k  �j  � k  ���� ��� r  ����� n  ����� 1  ���g
�g 
time� l ����f�e� I ���d�c�b
�d .misccurdldt    ��� null�c  �b  �f  �e  � o      �a�a 0 	starttime 	startTime� ��� r  ����� n  ����� 3  ���`
�` 
cobj� o  ���_�_ *0 messagesencouraging messagesEncouraging� o      �^�^ 0 msg  � ��� I ���]��
�] .sysonotfnull��� ��� TEXT� o  ���\�\ 0 msg  � �[��Z
�[ 
appr� m  ���� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�Z  � ��Y� I ���X��W
�X .sysodelanull��� ��� nmbr� m  ���V�V �W  �Y  �m  �l  � ��� l ���U�T�S�U  �T  �S  � ��� l ���R�Q�P�R  �Q  �P  � ��� l ���O�N�M�O  �N  �M  � ��� l ���L�K�J�L  �K  �J  � ��� I ���I��H
�I .sysoexecTEXT���     TEXT� b  ����� b  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ���( r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ���G�G 0 c  � m  ���� ���  '   '� o  ���F�F 0 sourcefolder sourceFolder� m  ���� ���  '   '� o  ���E�E 0 d  � m  ���� ���  / '�H  � ��� l ���D�C�B�D  �C  �B  � ��� l ���A�@�?�A  �@  �?  � � � l ���>�=�<�>  �=  �<     l ���;�:�9�;  �:  �9    r  �� m  ���8
�8 boovfals o      �7�7 0 isbackingup isBackingUp  r  ��	
	 m  ���6
�6 boovfals
 o      �5�5 0 manualbackup manualBackup  l ���4�3�2�4  �3  �2   �1 Z  ��0 = �� n  �� 2 ���/
�/ 
cobj l ���.�- c  �� 4  ���,
�, 
psxf o  ���+�+ 0 c   m  ���*
�* 
alis�.  �-   J  ���)�)   k  ��  l ���(�(   % delete folder t of backupFolder    � > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r   l ���'�&�%�'  �&  �%    !"! r  ��#$# c  ��%&% n  ��'(' 4  ���$)
�$ 
cfol) o  ���#�# 0 t  ( o  ���"�" 0 backupfolder backupFolder& m  ���!
�! 
TEXT$ o      � �  0 oldestfolder oldestFolder" *+* r  �,-, c  � ./. n  ��010 1  ���
� 
psxp1 o  ���� 0 oldestfolder oldestFolder/ m  ���
� 
TEXT- o      �� 0 oldestfolder oldestFolder+ 232 I �4�
� .ascrcmnt****      � ****4 l 5��5 b  676 m  88 �99  t o   d e l e t e :  7 o  �� 0 oldestfolder oldestFolder�  �  �  3 :;: l ����  �  �  ; <=< r  >?> b  @A@ b  BCB m  DD �EE  r m   - r f   'C o  �� 0 oldestfolder oldestFolderA m  FF �GG  '? o      �� 0 todelete toDelete= HIH I �J�
� .sysoexecTEXT���     TEXTJ o  �� 0 todelete toDelete�  I KLK l ����  �  �  L M�M Z  �NO�
�	N l &P��P E  &QRQ o  "�� &0 displaysleepstate displaySleepStateR o  "%�� 0 strawake strAwake�  �  O k  )�SS TUT r  )6VWV n  )2XYX 1  .2�
� 
timeY l ).Z��Z I ).�� ��
� .misccurdldt    ��� null�   ��  �  �  W o      ���� 0 endtime endTimeU [\[ r  7>]^] n 7<_`_ I  8<�������� 0 getduration getDuration��  ��  `  f  78^ o      ���� 0 duration  \ aba l ??��cd��  c � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   d �ee d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "b fgf l ??��hi��  h  delay 2   i �jj  d e l a y   2g klk l ??��������  ��  ��  l mnm r  ?Hopo n  ?Fqrq 3  BF��
�� 
cobjr o  ?B���� $0 messagescomplete messagesCompletep o      ���� 0 msg  n sts Z  I�uv��wu l INx����x =  INyzy o  IJ���� 0 duration  z m  JM{{ ?�      ��  ��  v k  Ql|| }~} I Qf���
�� .sysonotfnull��� ��� TEXT b  Q\��� b  QZ��� b  QV��� m  QT�� ���  A n d   i n  � o  TU���� 0 duration  � m  VY�� ���    m i n u t e ! 
� o  Z[���� 0 msg  � �����
�� 
appr� m  _b�� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  ~ ���� I gl�����
�� .sysodelanull��� ��� nmbr� m  gh���� ��  ��  ��  w k  o��� ��� I o�����
�� .sysonotfnull��� ��� TEXT� b  oz��� b  ox��� b  ot��� m  or�� ���  A n d   i n  � o  rs���� 0 duration  � m  tw�� ���    m i n u t e s ! 
� o  xy���� 0 msg  � �����
�� 
appr� m  }��� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  t ��� l ����������  ��  ��  � ���� I ������
�� .sysonotfnull��� ��� TEXT� m  ���� ��� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .� �����
�� 
appr� m  ���� ��� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r��  ��  �
  �	  �  �0   Z  �������� l �������� E  ����� o  ������ &0 displaysleepstate displaySleepState� o  ������ 0 strawake strAwake��  ��  � k  �
�� ��� r  ����� n  ����� 1  ����
�� 
time� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 endtime endTime� ��� r  ����� n ����� I  ���������� 0 getduration getDuration��  ��  �  f  ��� o      ���� 0 duration  � ��� r  ����� n  ����� 3  ����
�� 
cobj� o  ������ $0 messagescomplete messagesComplete� o      ���� 0 msg  � ���� Z  �
������ l �������� =  ����� o  ������ 0 duration  � m  ���� ?�      ��  ��  � k  ���� ��� I ������
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e ! 
� o  ������ 0 msg  � �����
�� 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �������
�� .sysodelanull��� ��� nmbr� m  ������ ��  ��  ��  � k  �
�� ��� I �����
�� .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ���  A n d   i n  � o  ������ 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ������ 0 msg  � �����
�� 
appr� m  � �� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I 
�����
�� .sysodelanull��� ��� nmbr� m  ���� ��  ��  ��  ��  ��  �1  ��  ��  ��  > m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  < ��� l ��������  ��  ��  � ���� I  ������� 0 	stopwatch  � ���� m  �� ���  f i n i s h��  ��  ��   �	 � l     ��������  ��  ��  	  			 l     ��������  ��  ��  	 			 l     ��������  ��  ��  	 			 l     ��������  ��  ��  	 			 l     ��������  ��  ��  	 			
		 l     ��		��  	 � �-----------------------------------------------------------------------------------------------------------------------------------------------   	 �		 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	
 			 l     ��		��  	 � �-----------------------------------------------------------------------------------------------------------------------------------------------   	 �		 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	 			 l     ��		��  	 � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   	 �		 - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	 			 l     ��������  ��  ��  	 			 l     ��		��  	 � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   	 �		   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .	 		 	 l     ��	!	"��  	! � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   	" �	#	#�   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .	  	$	%	$ i   u x	&	'	& I      ��	(���� 0 itexists itExists	( 	)	*	) o      ���� 0 
objecttype 
objectType	* 	+��	+ o      ���� 
0 object  ��  ��  	' l    W	,	-	.	, O     W	/	0	/ Z    V	1	2	3��	1 l   	4����	4 =    	5	6	5 o    ���� 0 
objecttype 
objectType	6 m    	7	7 �	8	8  d i s k��  ��  	2 Z   
 	9	:��	;	9 I  
 ��	<��
�� .coredoexnull���     ****	< 4   
 �	=
� 
cdis	= o    �~�~ 
0 object  ��  	: L    	>	> m    �}
�} boovtrue��  	; L    	?	? m    �|
�| boovfals	3 	@	A	@ l   "	B�{�z	B =    "	C	D	C o     �y�y 0 
objecttype 
objectType	D m     !	E	E �	F	F  f i l e�{  �z  	A 	G	H	G Z   % 7	I	J�x	K	I I  % -�w	L�v
�w .coredoexnull���     ****	L 4   % )�u	M
�u 
file	M o   ' (�t�t 
0 object  �v  	J L   0 2	N	N m   0 1�s
�s boovtrue�x  	K L   5 7	O	O m   5 6�r
�r boovfals	H 	P	Q	P l  : =	R�q�p	R =   : =	S	T	S o   : ;�o�o 0 
objecttype 
objectType	T m   ; <	U	U �	V	V  f o l d e r�q  �p  	Q 	W�n	W Z   @ R	X	Y�m	Z	X I  @ H�l	[�k
�l .coredoexnull���     ****	[ 4   @ D�j	\
�j 
cfol	\ o   B C�i�i 
0 object  �k  	Y L   K M	]	] m   K L�h
�h boovtrue�m  	Z L   P R	^	^ m   P Q�g
�g boovfals�n  ��  	0 m     	_	_�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  	- "  (string, string) as Boolean   	. �	`	` 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n	% 	a	b	a l     �f�e�d�f  �e  �d  	b 	c	d	c l     �c�b�a�c  �b  �a  	d 	e	f	e l     �`�_�^�`  �_  �^  	f 	g	h	g l     �]�\�[�]  �\  �[  	h 	i	j	i l     �Z�Y�X�Z  �Y  �X  	j 	k	l	k l     �W	m	n�W  	m � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   	n �	o	oZ   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .	l 	p	q	p l     �V	r	s�V  	r P J This means that backups can be identified from what machine they're from.   	s �	t	t �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .	q 	u	v	u i   y |	w	x	w I      �U�T�S�U .0 getcomputeridentifier getComputerIdentifier�T  �S  	x k     	y	y 	z	{	z r     		|	}	| n     	~		~ 1    �R
�R 
sicn	 l    	��Q�P	� I    �O�N�M
�O .sysosigtsirr   ��� null�N  �M  �Q  �P  	} o      �L�L 0 computername computerName	{ 	�	�	� r   
 	�	�	� I  
 �K	��J
�K .sysoexecTEXT���     TEXT	� m   
 	�	� �	�	� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �J  	� o      �I�I "0 strserialnumber strSerialNumber	� 	�	�	� r    	�	�	� l   	��H�G	� b    	�	�	� b    	�	�	� o    �F�F 0 computername computerName	� m    	�	� �	�	�    -  	� o    �E�E "0 strserialnumber strSerialNumber�H  �G  	� o      �D�D  0 identifiername identifierName	� 	��C	� L    	�	� o    �B�B  0 identifiername identifierName�C  	v 	�	�	� l     �A�@�?�A  �@  �?  	� 	�	�	� l     �>�=�<�>  �=  �<  	� 	�	�	� l     �;�:�9�;  �:  �9  	� 	�	�	� l     �8�7�6�8  �7  �6  	� 	�	�	� l     �5�4�3�5  �4  �3  	� 	�	�	� l     �2	�	��2  	� � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   	� �	�	�   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .	� 	�	�	� l     �1	�	��1  	� � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   	� �	�	�   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .	� 	�	�	� i   } �	�	�	� I      �0	��/�0 0 gettimestamp getTimestamp	� 	��.	� o      �-�- 0 isfolder isFolder�.  �/  	� l   �	�	�	�	� k    �	�	� 	�	�	� l     �,	�	��,  	�   Date variables   	� �	�	�    D a t e   v a r i a b l e s	� 	�	�	� r     )	�	�	� l     	��+�*	� I     �)�(�'
�) .misccurdldt    ��� null�(  �'  �+  �*  	� K    	�	� �&	�	�
�& 
year	� o    �%�% 0 y  	� �$	�	�
�$ 
mnth	� o    �#�# 0 m  	� �"	�	�
�" 
day 	� o    �!�! 0 d  	� � 	��
�  
time	� o   	 
�� 0 t  �  	� 	�	�	� r   * 1	�	�	� c   * /	�	�	� l  * -	���	� c   * -	�	�	� o   * +�� 0 y  	� m   + ,�
� 
long�  �  	� m   - .�
� 
TEXT	� o      �� 0 ty tY	� 	�	�	� r   2 9	�	�	� c   2 7	�	�	� l  2 5	���	� c   2 5	�	�	� o   2 3�� 0 y  	� m   3 4�
� 
long�  �  	� m   5 6�
� 
TEXT	� o      �� 0 ty tY	� 	�	�	� r   : A	�	�	� c   : ?	�	�	� l  : =	���	� c   : =	�	�	� o   : ;�� 0 m  	� m   ; <�
� 
long�  �  	� m   = >�
� 
TEXT	� o      �� 0 tm tM	� 	�	�	� r   B I	�	�	� c   B G	�	�	� l  B E	���
	� c   B E	�	�	� o   B C�	�	 0 d  	� m   C D�
� 
long�  �
  	� m   E F�
� 
TEXT	� o      �� 0 td tD	� 	�	�	� r   J Q	�	�	� c   J O	�	�	� l  J M	���	� c   J M	�	�	� o   J K�� 0 t  	� m   K L�
� 
long�  �  	� m   M N�
� 
TEXT	� o      � �  0 tt tT	� 	�	�	� l  R R��������  ��  ��  	� 	�	�	� l  R R��	�	���  	� U O Append the month or day with a 0 if the string length is only 1 character long   	� �	�	� �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g	� 	�	�	� r   R [	�	�	� c   R Y	�	�	� l  R W	�����	� I  R W��
 ��
�� .corecnte****       ****
  o   R S���� 0 tm tM��  ��  ��  	� m   W X��
�� 
nmbr	� o      ���� 
0 tml tML	� 


 r   \ e


 c   \ c


 l  \ a
����
 I  \ a��
��
�� .corecnte****       ****
 o   \ ]���� 0 tm tM��  ��  ��  
 m   a b��
�� 
nmbr
 o      ���� 
0 tdl tDL
 
	


	 l  f f��������  ��  ��  

 


 Z   f u

����
 l  f i
����
 =   f i


 o   f g���� 
0 tml tML
 m   g h���� ��  ��  
 r   l q


 b   l o


 m   l m

 �

  0
 o   m n���� 0 tm tM
 o      ���� 0 tm tM��  ��  
 


 l  v v��������  ��  ��  
 


 Z   v �

����
 l  v y
����
 =   v y

 
 o   v w���� 
0 tdl tDL
  m   w x���� ��  ��  
 r   | �
!
"
! b   | �
#
$
# m   | 
%
% �
&
&  0
$ o    ����� 0 td tD
" o      ���� 0 td tD��  ��  
 
'
(
' l  � ���������  ��  ��  
( 
)
*
) l  � ���������  ��  ��  
* 
+
,
+ l  � ���
-
.��  
-   Time variables	   
. �
/
/     T i m e   v a r i a b l e s 	
, 
0
1
0 l  � ���
2
3��  
2  	 Get hour   
3 �
4
4    G e t   h o u r
1 
5
6
5 r   � �
7
8
7 n   � �
9
:
9 1   � ���
�� 
tstr
: l  � �
;����
; I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  
8 o      ���� 0 timestr timeStr
6 
<
=
< r   � �
>
?
> I  � �
@��
A
@ z����
�� .sysooffslong    ��� null
�� misccura��  
A ��
B
C
�� 
psof
B m   � �
D
D �
E
E  :
C ��
F��
�� 
psin
F o   � ����� 0 timestr timeStr��  
? o      ���� 0 pos  
= 
G
H
G r   � �
I
J
I c   � �
K
L
K n   � �
M
N
M 7  � ���
O
P
�� 
cha 
O m   � ����� 
P l  � �
Q����
Q \   � �
R
S
R o   � ����� 0 pos  
S m   � ����� ��  ��  
N o   � ����� 0 timestr timeStr
L m   � ���
�� 
TEXT
J o      ���� 0 h  
H 
T
U
T r   � �
V
W
V c   � �
X
Y
X n   � �
Z
[
Z 7 � ���
\
]
�� 
cha 
\ l  � �
^����
^ [   � �
_
`
_ o   � ����� 0 pos  
` m   � ����� ��  ��  
]  ;   � �
[ o   � ����� 0 timestr timeStr
Y m   � ���
�� 
TEXT
W o      ���� 0 timestr timeStr
U 
a
b
a l  � ���������  ��  ��  
b 
c
d
c l  � ���
e
f��  
e   Get minute   
f �
g
g    G e t   m i n u t e
d 
h
i
h r   � �
j
k
j I  � �
l��
m
l z����
�� .sysooffslong    ��� null
�� misccura��  
m ��
n
o
�� 
psof
n m   � �
p
p �
q
q  :
o ��
r��
�� 
psin
r o   � ����� 0 timestr timeStr��  
k o      ���� 0 pos  
i 
s
t
s r   �
u
v
u c   �
w
x
w n   � 
y
z
y 7  � ��
{
|
�� 
cha 
{ m   � ����� 
| l  � �
}����
} \   � �
~

~ o   � ����� 0 pos  
 m   � ����� ��  ��  
z o   � ����� 0 timestr timeStr
x m   ��
�� 
TEXT
v o      ���� 0 m  
t 
�
�
� r  
�
�
� c  
�
�
� n  
�
�
� 7��
�
�
�� 
cha 
� l 
�����
� [  
�
�
� o  ���� 0 pos  
� m  ���� ��  ��  
�  ;  
� o  ���� 0 timestr timeStr
� m  ��
�� 
TEXT
� o      ���� 0 timestr timeStr
� 
�
�
� l ��������  ��  ��  
� 
�
�
� l ��
�
���  
�   Get AM or PM   
� �
�
�    G e t   A M   o r   P M
� 
�
�
� r  2
�
�
� I 0
���
�
� z����
�� .sysooffslong    ��� null
�� misccura��  
� ��
�
�
�� 
psof
� m  "%
�
� �
�
�   
� ��
���
�� 
psin
� o  ()���� 0 timestr timeStr��  
� o      ���� 0 pos  
� 
�
�
� r  3E
�
�
� c  3C
�
�
� n  3A
�
�
� 74A��
�
�
�� 
cha 
� l :>
�����
� [  :>
�
�
� o  ;<���� 0 pos  
� m  <=���� ��  ��  
�  ;  ?@
� o  34���� 0 timestr timeStr
� m  AB��
�� 
TEXT
� o      �� 0 s  
� 
�
�
� l FF�~�}�|�~  �}  �|  
� 
�
�
� l FF�{�z�y�{  �z  �y  
� 
�
�
� Z  F
�
�
��x
� l FI
��w�v
� =  FI
�
�
� o  FG�u�u 0 isfolder isFolder
� m  GH�t
�t boovtrue�w  �v  
� k  La
�
� 
�
�
� l LL�s
�
��s  
�   Create folder timestamp   
� �
�
� 0   C r e a t e   f o l d e r   t i m e s t a m p
� 
��r
� r  La
�
�
� b  L_
�
�
� b  L]
�
�
� b  L[
�
�
� b  LY
�
�
� b  LU
�
�
� b  LS
�
�
� b  LQ
�
�
� m  LO
�
� �
�
�  b a c k u p _
� o  OP�q�q 0 ty tY
� o  QR�p�p 0 tm tM
� o  ST�o�o 0 td tD
� m  UX
�
� �
�
�  _
� o  YZ�n�n 0 h  
� o  [\�m�m 0 m  
� o  ]^�l�l 0 s  
� o      �k�k 0 	timestamp  �r  
� 
�
�
� l dg
��j�i
� =  dg
�
�
� o  de�h�h 0 isfolder isFolder
� m  ef�g
�g boovfals�j  �i  
� 
��f
� k  j{
�
� 
�
�
� l jj�e
�
��e  
�   Create timestamp   
� �
�
� "   C r e a t e   t i m e s t a m p
� 
��d
� r  j{
�
�
� b  jy
�
�
� b  jw
�
�
� b  ju
�
�
� b  js
�
�
� b  jo
�
�
� b  jm
�
�
� o  jk�c�c 0 ty tY
� o  kl�b�b 0 tm tM
� o  mn�a�a 0 td tD
� m  or
�
� �
�
�  _
� o  st�`�` 0 h  
� o  uv�_�_ 0 m  
� o  wx�^�^ 0 s  
� o      �]�] 0 	timestamp  �d  �f  �x  
� 
�
�
� l ���\�[�Z�\  �[  �Z  
� 
��Y
� L  ��
�
� o  ���X�X 0 	timestamp  �Y  	�  	(boolean)   	� �
�
�  ( b o o l e a n )	� 
�
�
� l     �W�V�U�W  �V  �U  
� 
�
�
� l     �T�S�R�T  �S  �R  
� 
�
�
� l     �Q�P�O�Q  �P  �O  
� 
�
�
� l     �N�M�L�N  �M  �L  
� 
�
�
� l     �K�J�I�K  �J  �I  
� 
� 
� l     �H�H   q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.    � �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .   l     �G�G   w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.    � �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s . 	
	 i   � � I      �F�E�F 0 comparesizes compareSizes  o      �D�D 
0 source   �C o      �B�B 0 destination  �C  �E   l    j k     j  l     �A�A   # display dialog "COMPARESIZES"    � : d i s p l a y   d i a l o g   " C O M P A R E S I Z E S "  r      m     �@
�@ boovtrue o      �?�? 0 fit    r    
 !  4    �>"
�> 
psxf" o    �=�= 0 destination  ! o      �<�< 0 destination   #$# l   �;�:�9�;  �:  �9  $ %&% O    X'(' k    W)) *+* r    ,-, I   �8.�7
�8 .sysoexecTEXT���     TEXT. b    /0/ b    121 m    33 �44  d u   - m s  2 o    �6�6 
0 source  0 m    55 �66    |   c u t   - f   1�7  - o      �5�5 0 
foldersize 
folderSize+ 787 r    -9:9 ^    +;<; l   )=�4�3= I   )>?�2> z�1�0
�1 .sysorondlong        doub
�0 misccura? ]    $@A@ l   "B�/�.B ^    "CDC o     �-�- 0 
foldersize 
folderSizeD m     !�,�, �/  �.  A m   " #�+�+ d�2  �4  �3  < m   ) *�*�* d: o      �)�) 0 
foldersize 
folderSize8 EFE l  . .�(GH�(  G ) #display dialog folderSize as string   H �II F d i s p l a y   d i a l o g   f o l d e r S i z e   a s   s t r i n gF JKJ l  . .�'�&�%�'  �&  �%  K LML r   . <NON ^   . :PQP ^   . 8RSR ^   . 6TUT l  . 4V�$�#V l  . 4W�"�!W n   . 4XYX 1   2 4� 
�  
frspY 4   . 2�Z
� 
cdisZ o   0 1�� 0 destination  �"  �!  �$  �#  U m   4 5�� S m   6 7�� Q m   8 9�� O o      �� 0 	freespace 	freeSpaceM [\[ r   = M]^] ^   = K_`_ l  = Ia��a I  = Ibc�b z��
� .sysorondlong        doub
� misccurac l  A Dd��d ]   A Defe o   A B�� 0 	freespace 	freeSpacef m   B C�� d�  �  �  �  �  ` m   I J�� d^ o      �� 0 	freespace 	freeSpace\ ghg l  N N�ij�  i ( "display dialog freeSpace as string   j �kk D d i s p l a y   d i a l o g   f r e e S p a c e   a s   s t r i n gh lml l  N N����  �  �  m non I  N U�
p�	
�
 .ascrcmnt****      � ****p l  N Qq��q b   N Qrsr o   N O�� 0 
foldersize 
folderSizes o   O P�� 0 	freespace 	freeSpace�  �  �	  o t�t l  V V�uv�  u K Edisplay dialog ((folderSize) as string) & " " & (freeSpace) as string   v �ww � d i s p l a y   d i a l o g   ( ( f o l d e r S i z e )   a s   s t r i n g )   &   "   "   &   ( f r e e S p a c e )   a s   s t r i n g�  ( m    xx�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  & yzy l  Y Y��� �  �  �   z {|{ Z   Y g}~����} H   Y ] l  Y \������ A   Y \��� o   Y Z���� 0 
foldersize 
folderSize� o   Z [���� 0 	freespace 	freeSpace��  ��  ~ r   ` c��� m   ` a��
�� boovfals� o      ���� 0 fit  ��  ��  | ��� l  h h��������  ��  ��  � ��� l  h h������  � " display dialog "FIT: " & fit   � ��� 8 d i s p l a y   d i a l o g   " F I T :   "   &   f i t� ��� l  h h��������  ��  ��  � ���� L   h j�� o   h i���� 0 fit  ��    (string, string)    ���   ( s t r i n g ,   s t r i n g )
 ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i   � ���� I      ������� 0 	stopwatch  � ���� o      ���� 0 mode  ��  ��  � l    5���� k     5�� ��� q      �� ������ 0 x  ��  � ��� Z     3������ l    ������ =     ��� o     ���� 0 mode  � m    �� ��� 
 s t a r t��  ��  � k    �� ��� r    ��� I    ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovfals��  ��  � o      ���� 0 x  � ���� I   �����
�� .ascrcmnt****      � ****� l   ������ b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    ���� 0 x  ��  ��  ��  ��  � ��� l   ������ =    ��� o    ���� 0 mode  � m    �� ���  f i n i s h��  ��  � ���� k    /�� ��� r    '��� I    %������� 0 gettimestamp getTimestamp� ���� m     !��
�� boovfals��  ��  � o      ���� 0 x  � ���� I  ( /�����
�� .ascrcmnt****      � ****� l  ( +������ b   ( +��� m   ( )�� ��� " b a c k u p   f i n i s h e d :  � o   ) *���� 0 x  ��  ��  ��  ��  ��  ��  � ���� l  4 4��������  ��  ��  ��  �  (string)   � ���  ( s t r i n g )� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � M G Utility function to get the duration of the backup process in minutes.   � ��� �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .� ��� i   � ���� I      �������� 0 getduration getDuration��  ��  � k     �� ��� r     ��� ^     ��� l    ������ \     ��� o     ���� 0 endtime endTime� o    ���� 0 	starttime 	startTime��  ��  � m    ���� <� o      ���� 0 duration  � ��� r    ��� ^    ��� l    ����  I   �� z����
�� .sysorondlong        doub
�� misccura l   ���� ]     o    ���� 0 duration   m    ���� d��  ��  ��  ��  ��  � m    ���� d� o      ���� 0 duration  � �� L     o    ���� 0 duration  ��  � 	 l     ��������  ��  ��  	 

 l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ����   ; 5 Utility function bring WBTFU to the front with focus    � j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s  i   � � I      ����~�� 0 setfocus setFocus�  �~   O     
 I   	�}�|�{
�} .miscactvnull��� ��� null�|  �{   m                                                                                           @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��    l     �z�y�x�z  �y  �x    !  l     �w"#�w  " � �-----------------------------------------------------------------------------------------------------------------------------------------------   # �$$ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -! %&% l     �v'(�v  ' � �-----------------------------------------------------------------------------------------------------------------------------------------------   ( �)) - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -& *+* l     �u�t�s�u  �t  �s  + ,-, l     �r�q�p�r  �q  �p  - ./. l     �o�n�m�o  �n  �m  / 010 l     �l�k�j�l  �k  �j  1 232 l     �i�h�g�i  �h  �g  3 454 l     �f67�f  6 � �-----------------------------------------------------------------------------------------------------------------------------------------------   7 �88 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -5 9:9 l     �e;<�e  ; � �-----------------------------------------------------------------------------------------------------------------------------------------------   < �== - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -: >?> l     �d@A�d  @ � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   A �BB - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -? CDC l     �c�b�a�c  �b  �a  D EFE i   � �GHG I      �`I�_�` $0 menuneedsupdate_ menuNeedsUpdate_I J�^J l     K�]�\K m      �[
�[ 
cmnu�]  �\  �^  �_  H k     LL MNM l     �ZOP�Z  O J D NSMenu's delegates method, when the menu is clicked this is called.   P �QQ �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .N RSR l     �YTU�Y  T j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   U �VV �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .S WXW l     �XYZ�X  Y < 6 This means the menu items can be changed dynamically.   Z �[[ l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .X \�W\ n    ]^] I    �V�U�T�V 0 	makemenus 	makeMenus�U  �T  ^  f     �W  F _`_ l     �S�R�Q�S  �R  �Q  ` aba l     �P�O�N�P  �O  �N  b cdc l     �M�L�K�M  �L  �K  d efe l     �J�I�H�J  �I  �H  f ghg l     �G�F�E�G  �F  �E  h iji i   � �klk I      �D�C�B�D 0 	makemenus 	makeMenus�C  �B  l k     vmm non l    	pqrp n    	sts I    	�A�@�?�A  0 removeallitems removeAllItems�@  �?  t o     �>�> 0 newmenu newMenuq !  remove existing menu items   r �uu 6   r e m o v e   e x i s t i n g   m e n u   i t e m so vwv l  
 
�=�<�;�=  �<  �;  w x�:x Y   
 vy�9z{�8y k    q|| }~} r    '� n    %��� 4   " %�7�
�7 
cobj� o   # $�6�6 0 i  � o    "�5�5 0 thelist theList� o      �4�4 0 	this_item  ~ ��� l  ( (�3�2�1�3  �2  �1  � ��� Z   ( _����0� l  ( -��/�.� =   ( -��� l  ( +��-�,� c   ( +��� o   ( )�+�+ 0 	this_item  � m   ) *�*
�* 
TEXT�-  �,  � m   + ,�� ���  B a c k u p   n o w�/  �.  � r   0 @��� l  0 >��)�(� n  0 >��� I   7 >�'��&�' J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8�%�% 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ��$� m   9 :�� ���  �$  �&  � n  0 7��� I   3 7�#�"�!�# 	0 alloc  �"  �!  � n  0 3��� o   1 3� �  0 
nsmenuitem 
NSMenuItem� m   0 1�
� misccura�)  �(  � o      �� 0 thismenuitem thisMenuItem� ��� l  C H���� =   C H��� l  C F���� c   C F��� o   C D�� 0 	this_item  � m   D E�
� 
TEXT�  �  � m   F G�� ���  Q u i t�  �  � ��� r   K [��� l  K Y���� n  K Y��� I   R Y���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S�� 0 	this_item  � ��� m   S T�� ���  q u i t H a n d l e r :� ��� m   T U�� ���  �  �  � n  K R��� I   N R���� 	0 alloc  �  �  � n  K N��� o   L N�� 0 
nsmenuitem 
NSMenuItem� m   K L�
� misccura�  �  � o      �� 0 thismenuitem thisMenuItem�  �0  � ��� l  ` `�
�	��
  �	  �  � ��� l  ` j���� n  ` j��� I   e j���� 0 additem_ addItem_� ��� o   e f�� 0 thismenuitem thisMenuItem�  �  � o   ` e�� 0 newmenu newMenu�  �  � ��� l  k k� �����   ��  ��  � ���� l  k q���� l  k q������ n  k q��� I   l q������� 0 
settarget_ 
setTarget_� ����  f   l m��  ��  � o   k l���� 0 thismenuitem thisMenuItem��  ��  � * $ required for enabling the menu item   � ��� H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m��  �9 0 i  z m    ���� { n    ��� m    ��
�� 
nmbr� n   ��� 2   ��
�� 
cobj� o    ���� 0 thelist theList�8  �:  j ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������  0 backuphandler_ backupHandler_� ���� o      ���� 
0 sender  ��  ��  � Z     3������ l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � k    �� ��� l   ������  � % display dialog "BACKUP HANDLER"   � ��� > d i s p l a y   d i a l o g   " B A C K U P   H A N D L E R "� ��� r    	��� m    ��
�� boovtrue� o      ���� 0 manualbackup manualBackup� ��� I  
 ��� 
�� .sysonotfnull��� ��� TEXT� m   
  � T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p  ����
�� 
appr m     � 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  �  I   ����
�� .sysodelanull��� ��� nmbr m    ���� ��   	��	 l   ��
��  
 ! display dialog manualBackup    � 6 d i s p l a y   d i a l o g   m a n u a l B a c k u p��  �  l   ���� =     o    ���� 0 isbackingup isBackingUp m    ��
�� boovtrue��  ��   �� k   " /  r   " ' n   " % 3   # %��
�� 
cobj o   " #���� 40 messagesalreadybackingup messagesAlreadyBackingUp o      ���� 0 msg   �� I  ( /��
�� .sysonotfnull��� ��� TEXT o   ( )���� 0 msg   ����
�� 
appr m   * + � P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  ��  ��  ��  �  !  l     ��������  ��  ��  ! "#" l     ��������  ��  ��  # $%$ l     ��������  ��  ��  % &'& l     ��������  ��  ��  ' ()( l     ��������  ��  ��  ) *+* i   � �,-, I      ��.���� 0 quithandler_ quitHandler_. /��/ o      ���� 
0 sender  ��  ��  - Z     a012��0 l    3����3 =     454 o     ���� 0 isbackingup isBackingUp5 m    ��
�� boovtrue��  ��  1 k    *66 787 l   ��9:��  9  
setFocus()   : �;;  s e t F o c u s ( )8 <=< r    >?> l   @����@ n    ABA 1    ��
�� 
bhitB l   C����C I   ��DE
�� .sysodlogaskr        TEXTD m    FF �GG � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ?E ��HI
�� 
btnsH J    JJ KLK m    	MM �NN  C a n c e l   &   Q u i tL O��O m   	 
PP �QQ  R e s u m e��  I ��R��
�� 
dfltR m    ���� ��  ��  ��  ��  ��  ? o      ���� 	0 reply  = S��S Z    *TU��VT l   W����W =    XYX o    ���� 	0 reply  Y m    ZZ �[[  C a n c e l   &   Q u i t��  ��  U I   "��\��
�� .aevtquitnull��� ��� null\ m    ]]                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  V I  % *��^��
�� .ascrcmnt****      � ****^ l  % &_����_ m   % &`` �aa  W B T F U   r e s u m e d��  ��  ��  ��  2 bcb l  - 0d����d =   - 0efe o   - .���� 0 isbackingup isBackingUpf m   . /��
�� boovfals��  ��  c g��g k   3 ]hh iji l  3 3��kl��  k  
setFocus()   l �mm  s e t F o c u s ( )j non r   3 Epqp l  3 Cr����r n   3 Csts 1   A C��
�� 
bhitt l  3 Au����u I  3 A��vw
�� .sysodlogaskr        TEXTv m   3 4xx �yy < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?w ��z{
�� 
btnsz J   5 ;|| }~} m   5 6 ���  Q u i t~ ���� m   6 9�� ���  R e s u m e��  { �����
�� 
dflt� m   < =���� ��  ��  ��  ��  ��  q o      ���� 	0 reply  o ���� Z   F ]����� l  F K��~�}� =   F K��� o   F G�|�| 	0 reply  � m   G J�� ���  Q u i t�~  �}  � I  N S�{��z
�{ .aevtquitnull��� ��� null� m   N O��                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �z  �  � I  V ]�y��x
�y .ascrcmnt****      � ****� l  V Y��w�v� m   V Y�� ���  W B T F U   r e s u m e d�w  �v  �x  ��  ��  ��  + ��� l     �u�t�s�u  �t  �s  � ��� l     �r�q�p�r  �q  �p  � ��� l     �o�n�m�o  �n  �m  � ��� l     �l�k�j�l  �k  �j  � ��� l     �i�h�g�i  �h  �g  � ��� l     �f���f  �   create an NSStatusBar   � ��� ,   c r e a t e   a n   N S S t a t u s B a r� ��� i   � ���� I      �e�d�c�e 0 makestatusbar makeStatusBar�d  �c  � k     r�� ��� l     �b���b  �  log ("Make status bar")   � ��� . l o g   ( " M a k e   s t a t u s   b a r " )� ��� r     ��� n    ��� o    �a�a "0 systemstatusbar systemStatusBar� n    ��� o    �`�` 0 nsstatusbar NSStatusBar� m     �_
�_ misccura� o      �^�^ 0 bar  � ��� l   �]�\�[�]  �\  �[  � ��� r    ��� n   ��� I   	 �Z��Y�Z .0 statusitemwithlength_ statusItemWithLength_� ��X� m   	 
�� ��      �X  �Y  � o    	�W�W 0 bar  � o      �V�V 0 
statusitem 
StatusItem� ��� l   �U�T�S�U  �T  �S  � ��� r    #��� 4    !�R�
�R 
alis� l    ��Q�P� b     ��� l   ��O�N� I   �M��
�M .earsffdralis        afdr�  f    � �L��K
�L 
rtyp� m    �J
�J 
ctxt�K  �O  �N  � m    �� ��� < C o n t e n t s : R e s o u r c e s : m e n u b a r . p n g�Q  �P  � o      �I�I 0 	imagepath 	imagePath� ��� r   $ )��� n   $ '��� 1   % '�H
�H 
psxp� o   $ %�G�G 0 	imagepath 	imagePath� o      �F�F 0 	imagepath 	imagePath� ��� r   * 8��� n  * 6��� I   1 6�E��D�E 20 initwithcontentsoffile_ initWithContentsOfFile_� ��C� o   1 2�B�B 0 	imagepath 	imagePath�C  �D  � n  * 1��� I   - 1�A�@�?�A 	0 alloc  �@  �?  � n  * -��� o   + -�>�> 0 nsimage NSImage� m   * +�=
�= misccura� o      �<�< 	0 image  � ��� l  9 9�;�:�9�;  �:  �9  � ��� l  9 9�8���8  � � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   � ��� s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "� ��� l  9 9�7�6�5�7  �6  �5  � ��� n  9 C��� I   > C�4��3�4 0 	setimage_ 	setImage_� ��2� o   > ?�1�1 	0 image  �2  �3  � o   9 >�0�0 0 
statusitem 
StatusItem� ��� l  D D�/�.�-�/  �.  �-  � ��� l  D D�,���,  � , & set up the initial NSStatusBars title   � ��� L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e� ��� l  D D�+���+  � # StatusItem's setTitle:"WBTFU"   � ��� : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "� ��� l  D D�*�)�(�*  �)  �(  � � � l  D D�'�'   1 + set up the initial NSMenu of the statusbar    � V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r   r   D X n  D R	 I   K R�&
�%�&  0 initwithtitle_ initWithTitle_
 �$ m   K N �  C u s t o m�$  �%  	 n  D K I   G K�#�"�!�# 	0 alloc  �"  �!   n  D G o   E G� �  0 nsmenu NSMenu m   D E�
� misccura o      �� 0 newmenu newMenu  l  Y Y����  �  �    n  Y c I   ^ c��� 0 setdelegate_ setDelegate_ �  f   ^ _�  �   o   Y ^�� 0 newmenu newMenu  l  d d��   � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.    �0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .   l  d d����  �  �    !�! n  d r"#" I   i r�$�� 0 setmenu_ setMenu_$ %�% o   i n�� 0 newmenu newMenu�  �  # o   d i�� 0 
statusitem 
StatusItem�  � &'& l     ���
�  �  �
  ' ()( l     �	*+�	  * � �-----------------------------------------------------------------------------------------------------------------------------------------------   + �,, - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -) -.- l     �/0�  / � �-----------------------------------------------------------------------------------------------------------------------------------------------   0 �11 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -. 232 l     ����  �  �  3 454 l     ����  �  �  5 676 l     �� ���  �   ��  7 898 l     ��������  ��  ��  9 :;: l     ��������  ��  ��  ; <=< l     ��������  ��  ��  = >?> i   � �@A@ I      �������� 0 runonce runOnce��  ��  A k     BB CDC Z     EF����E H     
GG c     	HIH l    J����J n    KLK I    �������� 0 ismainthread isMainThread��  ��  L n    MNM o    ���� 0 nsthread NSThreadN m     ��
�� misccura��  ��  I m    ��
�� 
boolF k    OO PQP l   ��RS��  R b \display alert "This script must be run from the main thread." buttons {"Cancel"} as critical   S �TT � d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a lQ U��U l   ��VW��  V  error number -128   W �XX " e r r o r   n u m b e r   - 1 2 8��  ��  ��  D YZY l   ��������  ��  ��  Z [��[ I    �������� 0 	plistinit 	plistInit��  ��  ��  ? \]\ l     ��������  ��  ��  ] ^_^ l  � �`����` n  � �aba I   � ��������� 0 makestatusbar makeStatusBar��  ��  b  f   � ���  ��  _ cdc l  � �e����e I   � ��������� 0 runonce runOnce��  ��  ��  ��  d fgf l     ��������  ��  ��  g hih l     ��������  ��  ��  i jkj l     ��������  ��  ��  k lml l     ��������  ��  ��  m non l     ��������  ��  ��  o pqp l     ��rs��  r w q Function that is always running in the background. This doesn't need to get called as it is running from the off   s �tt �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f fq uvu l     ��wx��  w � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   x �yy�   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .v z��z i   � �{|{ I     ������
�� .miscidlenmbr    ��� null��  ��  | k     �}} ~~ Z     }������� l    ������ E     ��� o     ���� &0 displaysleepstate displaySleepState� o    ���� 0 strawake strAwake��  ��  � Z    y������� l   	������ =    	��� o    ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � Z    u������ l   ������ =    ��� o    ���� 0 manualbackup manualBackup� m    ��
�� boovfals��  ��  � Z    c������� l   ������ A    ��� l   ������ n    ��� 1    ��
�� 
hour� l   ������ I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  � m    ���� ��  ��  � k    _�� ��� r    '��� l   %������ n    %��� 1   # %��
�� 
min � l   #������ l   #������ I   #������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  ��  ��  � o      ���� 0 m  � ��� l  ( (��������  ��  ��  � ���� Z   ( _������ G   ( 3��� l  ( +������ =   ( +��� o   ( )���� 0 m  � m   ) *���� ��  ��  � l  . 1������ =   . 1��� o   . /���� 0 m  � m   / 0���� -��  ��  � Z   6 E������� l  6 9������ =   6 9��� o   6 7���� 0 scheduledtime scheduledTime� m   7 8���� ��  ��  � I   < A����~�� 0 	plistinit 	plistInit�  �~  ��  ��  � ��� G   H S��� l  H K��}�|� =   H K��� o   H I�{�{ 0 m  � m   I J�z�z  �}  �|  � l  N Q��y�x� =   N Q��� o   N O�w�w 0 m  � m   O P�v�v �y  �x  � ��u� I   V [�t�s�r�t 0 	plistinit 	plistInit�s  �r  �u  ��  ��  ��  ��  � ��� l  f i��q�p� =   f i��� o   f g�o�o 0 manualbackup manualBackup� m   g h�n
�n boovtrue�q  �p  � ��m� I   l q�l�k�j�l 0 	plistinit 	plistInit�k  �j  �m  ��  ��  ��  ��  ��   ��� l  ~ ~�i�h�g�i  �h  �g  � ��f� L   ~ ��� m   ~ �e�e <�f  ��       �d���c g���������������������������d  � �b�a�`�_�^�]�\�[�Z�Y�X�W�V�U�T�S�R�Q�P�O�N�M�L�K�J�I�H�G�F
�b 
pimr�a 0 
statusitem 
StatusItem�` 0 
thedisplay 
theDisplay�_ 0 defaults  �^ $0 internalmenuitem internalMenuItem�] $0 externalmenuitem externalMenuItem�\ 0 newmenu newMenu�[ 0 thelist theList�Z 0 	plistinit 	plistInit�Y 0 diskinit diskInit�X 0 freespaceinit freeSpaceInit�W 0 
backupinit 
backupInit�V 0 tidyup tidyUp�U 
0 backup  �T 0 itexists itExists�S .0 getcomputeridentifier getComputerIdentifier�R 0 gettimestamp getTimestamp�Q 0 comparesizes compareSizes�P 0 	stopwatch  �O 0 getduration getDuration�N 0 setfocus setFocus�M $0 menuneedsupdate_ menuNeedsUpdate_�L 0 	makemenus 	makeMenus�K  0 backuphandler_ backupHandler_�J 0 quithandler_ quitHandler_�I 0 makestatusbar makeStatusBar�H 0 runonce runOnce
�G .miscidlenmbr    ��� null
�F .aevtoappnull  �   � ****� �E��E �  ����� �D L�C
�D 
vers�C  � �B��A
�B 
cobj� ��   �@
�@ 
osax�A  � �?��>
�? 
cobj� ��   �= U
�= 
frmk�>  � �<��;
�< 
cobj� ��   �: [
�: 
frmk�;  
�c 
msng� �� �9�8�
�9 misccura
�8 
pcls� ���  N S U s e r D e f a u l t s� �� �7�6�
�7 misccura
�6 
pcls� ���  N S M e n u I t e m� �� �5�4�
�5 misccura
�4 
pcls� ���  N S M e n u I t e m� �� �3�2�
�3 misccura
�2 
pcls� ���  N S M e n u� �1��1 �   � �� �0��/�.� �-�0 0 	plistinit 	plistInit�/  �.  � �,�+�*�)�(�'�&�%�, 0 
foldername 
folderName�+ 0 
backupdisk 
backupDisk�*  0 computerfolder computerFolder�) 0 
backuptime 
backupTime�( 0 thecontents theContents�' 0 thevalue theValue�& 0 backuptimes backupTimes�% 0 selectedtime selectedTime  1�$�#;�"�!� ��������X�c��|������������������
��	��������$ 	0 plist  �# 0 itexists itExists
�" 
plif
�! 
pcnt
�  
valL�  0 foldertobackup FolderToBackup� 0 backupdrive BackupDrive�  0 computerfolder computerFolder� 0 scheduledtime scheduledTime� .0 getcomputeridentifier getComputerIdentifier�  ��
� 
prmp
� .sysostflalis    ��� null
� 
TEXT
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
� .corecrel****      � null
� 
plii
� 
insh
�
 
kind�	 � 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .ascrcmnt****      � ****� 0 diskinit diskInit�-�jE�O*��l+ e  2� **��/�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYL*j+ E�O� ��n*��l E�O*�a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a �a  a !�la " # �*a a $a %*6a  a &a a !a '�a (a ( #O*a a $a %*6a  a &a a !a )�a (a ( #O*a a $a %*6a  a &a a !a *�a (a ( #O*a a $a %*6a  a &a a !a +�a (a ( #UUO�E` ,O�E` -O�E` .O�E�O_ ,_ -_ .�a "vj /O*j+ 0� �G��� � 0 diskinit diskInit�  �   ������ 0 msg  �� 	0 reply   X������������x{������������������������ "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit�� "0 messagesmissing messagesMissing
�� 
cobj
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit�� &0 displaysleepstate displaySleepState�� 0 strawake strAwake�� &0 messagescancelled messagesCancelled
�� 
appr
�� .sysonotfnull��� ��� TEXT�  Z*��l+ e  *fk+ Y F��.E�O����lv�l� �,E�O��  
*j+ Y !�_  _ �.E�O�a a l Y h� ����������� 0 freespaceinit freeSpaceInit�� ����   ���� 	0 again  ��   �������� 	0 again  �� 
0 reply1  �� 0 msg   ����������������������������������$���� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes�� 0 
backupinit 
backupInit
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp�� &0 displaysleepstate displaySleepState�� 0 strawake strAwake�� &0 messagescancelled messagesCancelled
�� 
cobj
�� 
appr
�� .sysonotfnull��� ��� TEXT�� z*��l+ e  
*j+ Y g�f  ����lv�l� 
�,E�Y �e  ����lv�l� 
�,E�Y hO��  
*j+ Y %_ _  _ a .E�Y hO�a a l � ��=�������� 0 
backupinit 
backupInit��  ��     ������Z_��m����������k����z���������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  �� 0 itexists itExists
�� 
kocl
�� 
cfol
�� 
insh
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null�� 0 machinefolder machineFolder
�� 
cdis�� 0 backupfolder backupFolder�� 0 initialbackup initialBackup�� 0 latestfolder latestFolder�� 
0 backup  �� �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY fE` O� *a �/�a /�_ /�a /E` OPUO*j+ � ������	���� 0 tidyup tidyUp��  ��   ������������������ 0 bf bF�� 0 creationdates creationDates�� 0 theoldestdate theOldestDate�� 0 j  �� 0 i  �� 0 thisdate thisDate�� 
0 reply2  �� 0 msg  	 $�����������>����������l���������������������������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  
�� 
cdis
�� 
cfol�� 0 machinefolder machineFolder
�� 
cobj
�� 
ascd
�� .corecnte****       ****
�� 
pnam
�� .ascrcmnt****      � ****
�� .coredeloobj        obj 
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit
�� 
trsh
�� .fndremptnull��� ��� obj �� &0 displaysleepstate displaySleepState�� 0 strawake strAwake�� &0 messagescancelled messagesCancelled
�� 
appr
�� .sysonotfnull��� ��� TEXT�� 0 freespaceinit freeSpaceInit
�� .sysodelanull��� ��� nmbr�� �*��/E�O� �*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/j O�a a a lva la  a ,E�O�a   *a ,j Y #_ _  _ �.E�O�a a l Y hO*ek+  O_ _  a !a a "l Okj #Y hU� ������
���� 
0 backup  ��  ��  
 
���������������������� 0 t  �� 0 x  �� "0 containerfolder containerFolder�� 0 
foldername 
folderName�� 0 d  �� 0 duration  �� 0 msg  �� 0 c  �� 0 oldestfolder oldestFolder�� 0 todelete toDelete U����7�����������������������~�}�|�{�z��y�x�w�v�u�t�s �r�q�p "468�o�n�m�l�k�jpru�i���������h�g������f�e8DF{����������������� 0 gettimestamp getTimestamp�� 0 isbackingup isBackingUp�� 0 	stopwatch  
�� 
psxf�� 0 sourcefolder sourceFolder
�� 
TEXT
�� 
cfol
�� 
pnam�� 0 initialbackup initialBackup
�� 
kocl
�� 
insh�� 0 backupfolder backupFolder
� 
prdt�~ 
�} .corecrel****      � null�| (0 activesourcefolder activeSourceFolder
�{ 
cdis�z 	0 drive  �y 0 machinefolder machineFolder�x (0 activebackupfolder activeBackupFolder
�w .misccurdldt    ��� null
�v 
time�u 0 	starttime 	startTime�t &0 displaysleepstate displaySleepState�s 0 strawake strAwake
�r 
appr
�q .sysonotfnull��� ��� TEXT�p "0 destinationdisk destinationDisk
�o .sysoexecTEXT���     TEXT�n 0 manualbackup manualBackup�m 0 endtime endTime�l 0 getduration getDuration�k $0 messagescomplete messagesComplete
�j 
cobj
�i .sysodelanull��� ��� nmbr
�h .ascrcmnt****      � ****�g *0 messagesencouraging messagesEncouraging
�f 
alis
�e 
psxp��*ek+  E�OeE�O*�k+ O��*��/�&E�O*�/�,E�O�f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hO�e  �*j a ,E` O_ _  a a a l Y hO_  a !%_ %a "%�%a #%�&E�Oa $�%a %%�%a &%j 'OfE�OfE` (O_ _  @*j a ,E` )O)j+ *E�O_ +a ,.E�Oa -�%a .%�%a a /l Okj 0Y hY��f �_  a 1%_ %a 2%�%a 3%�%a 4%�&E�O_  a 5%_ %a 6%�%a 7%�&E�Oa 8�%j 9O_ _  .*j a ,E` O_ :a ,.E�O�a a ;l Okj 0Y hOa <�%a =%�%a >%�%a ?%j 'OfE�OfE` (O*�/a @&a ,-jv  ���/�&E�O�a A,�&E�Oa B�%j 9Oa C�%a D%E�O�j 'O_ _  t*j a ,E` )O)j+ *E�O_ +a ,.E�O�a E   a F�%a G%�%a a Hl Okj 0Y a I�%a J%�%a a Kl Okj 0Oa La a Ml Y hY q_ _  f*j a ,E` )O)j+ *E�O_ +a ,.E�O�a E   a N�%a O%�%a a Pl Okj 0Y a Q�%a R%�%a a Sl Okj 0Y hY hUO*a Tk+ � �d	'�c�b�a�d 0 itexists itExists�c �`�`   �_�^�_ 0 
objecttype 
objectType�^ 
0 object  �b   �]�\�] 0 
objecttype 
objectType�\ 
0 object   	_	7�[�Z	E�Y	U�X
�[ 
cdis
�Z .coredoexnull���     ****
�Y 
file
�X 
cfol�a X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU� �W	x�V�U�T�W .0 getcomputeridentifier getComputerIdentifier�V  �U   �S�R�Q�S 0 computername computerName�R "0 strserialnumber strSerialNumber�Q  0 identifiername identifierName �P�O	��N	�
�P .sysosigtsirr   ��� null
�O 
sicn
�N .sysoexecTEXT���     TEXT�T *j  �,E�O�j E�O��%�%E�O�� �M	��L�K�J�M 0 gettimestamp getTimestamp�L �I�I   �H�H 0 isfolder isFolder�K   �G�F�E�D�C�B�A�@�?�>�=�<�;�:�9�8�G 0 isfolder isFolder�F 0 y  �E 0 m  �D 0 d  �C 0 t  �B 0 ty tY�A 0 tm tM�@ 0 td tD�? 0 tt tT�> 
0 tml tML�= 
0 tdl tDL�< 0 timestr timeStr�; 0 pos  �: 0 h  �9 0 s  �8 0 	timestamp   �7�6�5�4�3�2�1�0�/�.�-�,�+�*�)

%�(�'�&
D�%�$�#�"
p
�
�
�
�
�7 
Krtn
�6 
year�5 0 y  
�4 
mnth�3 0 m  
�2 
day �1 0 d  
�0 
time�/ 0 t  �. 
�- .misccurdldt    ��� null
�, 
long
�+ 
TEXT
�* .corecnte****       ****
�) 
nmbr
�( 
tstr
�' misccura
�& 
psof
�% 
psin�$ 
�# .sysooffslong    ��� null
�" 
cha �J�*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�� �!� ���! 0 comparesizes compareSizes�  ��   ��� 
0 source  � 0 destination  �   ������ 
0 source  � 0 destination  � 0 fit  � 0 
foldersize 
folderSize� 0 	freespace 	freeSpace �x35��������
� 
psxf
� .sysoexecTEXT���     TEXT
� misccura� � d
� .sysorondlong        doub
� 
cdis
� 
frsp
� .ascrcmnt****      � ****� keE�O*�/E�O� J�%�%j E�O� ��!� j U�!E�O*�/�,�!�!�!E�O� 	�� j U�!E�O��%j OPUO�� fE�Y hO�� ����
�	� 0 	stopwatch  � ��   �� 0 mode  �
   ��� 0 mode  � 0 x   ������� 0 gettimestamp getTimestamp
� .ascrcmnt****      � ****�	 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP� ���� ��� 0 getduration getDuration�  �    ���� 0 duration   �������������� 0 endtime endTime�� 0 	starttime 	startTime�� <
�� misccura�� d
�� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�� ���������� 0 setfocus setFocus��  ��     ��
�� .miscactvnull��� ��� null�� � *j U� ��H�������� $0 menuneedsupdate_ menuNeedsUpdate_�� �� ��    ��
�� 
cmnu��     ���� 0 	makemenus 	makeMenus�� )j+  � ��l����!"���� 0 	makemenus 	makeMenus��  ��  ! �������� 0 i  �� 0 	this_item  �� 0 thismenuitem thisMenuItem" ����������������������������  0 removeallitems removeAllItems
�� 
cobj
�� 
nmbr
�� 
TEXT
�� misccura�� 0 
nsmenuitem 
NSMenuItem�� 	0 alloc  �� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�� 0 additem_ addItem_�� 0 
settarget_ 
setTarget_�� wb  j+  O kkb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ��&�  ��,j+ ���m+ 
E�Y hOb  �k+ O�)k+ [OY��� �������#$����  0 backuphandler_ backupHandler_�� ��%�� %  ���� 
0 sender  ��  # �������� 
0 sender  �� 40 messagesalreadybackingup messagesAlreadyBackingUp�� 0 msg  $ 	�������������� 0 isbackingup isBackingUp�� 0 manualbackup manualBackup
�� 
appr
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr
�� 
cobj�� 4�f  eE�O���l Okj OPY �e  ��.E�O���l Y h� ��-����&'���� 0 quithandler_ quitHandler_�� ��(�� (  ���� 
0 sender  ��  & ������ 
0 sender  �� 	0 reply  ' ��F��MP��������Z]��`��x����� 0 isbackingup isBackingUp
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit
�� .aevtquitnull��� ��� null
�� .ascrcmnt****      � ****�� b�e  )����lv�l� �,E�O��  
�j Y �j Y 6�f  /���a lv�l� �,E�O�a   
�j Y 	a j Y h� �������)*���� 0 makestatusbar makeStatusBar��  ��  ) �������� 0 bar  �� 0 	imagepath 	imagePath�� 	0 image  * ������������������������������������
�� misccura�� 0 nsstatusbar NSStatusBar�� "0 systemstatusbar systemStatusBar�� .0 statusitemwithlength_ statusItemWithLength_
�� 
alis
�� 
rtyp
�� 
ctxt
�� .earsffdralis        afdr
�� 
psxp�� 0 nsimage NSImage�� 	0 alloc  �� 20 initwithcontentsoffile_ initWithContentsOfFile_�� 0 	setimage_ 	setImage_�� 0 nsmenu NSMenu��  0 initwithtitle_ initWithTitle_�� 0 setdelegate_ setDelegate_�� 0 setmenu_ setMenu_�� s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ � ��A����+,���� 0 runonce runOnce��  ��  +  , ����������
�� misccura�� 0 nsthread NSThread�� 0 ismainthread isMainThread
�� 
bool�� 0 	plistinit 	plistInit�� ��,j+ �& hY hO*j+ � ��|����-.��
�� .miscidlenmbr    ��� null��  ��  - ���� 0 m  . ����������������������� &0 displaysleepstate displaySleepState�� 0 strawake strAwake�� 0 isbackingup isBackingUp�� 0 manualbackup manualBackup
�� .misccurdldt    ��� null
�� 
hour� 
� 
min � � -
� 
bool� 0 scheduledtime scheduledTime� 0 	plistinit 	plistInit� � <�� ��� x�f  n�f  V*j �,� F*j �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY �e  
*j+ Y hY hY hO�� �/��01�
� .aevtoappnull  �   � ****/ k     �22  �33  �44  �55  �66  �77 88 .99 Y:: �;; �<< �== �>> ^?? c��  �  �  0  1 C����~�} ��|�{�z�y � � � � ��x�w#'*�v�u6:>BFJNRU�taeimquy|�s�r����������q��p��o��n�m�l�k
� afdrdlib
� 
from
� fldmfldu
�~ .earsffdralis        afdr
�} 
psxp�| 	0 plist  �{ 0 initialbackup initialBackup�z 0 manualbackup manualBackup�y 0 isbackingup isBackingUp�x �w "0 messagesmissing messagesMissing�v 	�u *0 messagesencouraging messagesEncouraging�t $0 messagescomplete messagesComplete�s �r &0 messagescancelled messagesCancelled�q 40 messagesalreadybackingup messagesAlreadyBackingUp�p 0 strawake strAwake�o 0 strsleep strSleep
�n .sysoexecTEXT���     TEXT�m &0 displaysleepstate displaySleepState�l 0 makestatusbar makeStatusBar�k 0 runonce runOnce� ����l �,�%E�OeE�OfE�OfE�O������vE` Oa a a a a a a a a a vE` Oa a a a a  a !a "a #a $a vE` %Oa &a 'a (a )a *a +a ,a -a .vE` /Oa 0a 1a 2a 3a 4a 5a 6a 7a 8a vE` 9Oa :E` ;Oa <E` =Oa >j ?E` @O)j+ AO*j+ Bascr  ��ޭ