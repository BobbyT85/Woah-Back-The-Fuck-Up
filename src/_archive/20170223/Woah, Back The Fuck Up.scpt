FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S _�� ��� 0 thelist theList � J   S ^ � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " T u r n   o n   t h e   b a n t s �  ��� � m   Y \ � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   ` b�� ��� 0 nsfw   � m   ` a��
�� boovfals �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   c c � � �� ��� 	0 plist   � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � �� ��� 0 latestfolder latestFolder � ������ 0 initialbackup initialBackup��   �  � � � p   c c � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ������ 0 endtime endTime��   �  � � � p   c c � � ������ 0 manualbackup manualBackup��   �  � � � p   c c � � �� ��� 0 isbackingup isBackingUp � �� ��� "0 messagesmissing messagesMissing � �� ��� *0 messagesencouraging messagesEncouraging � �� ��� $0 messagescomplete messagesComplete � ������ &0 messagescancelled messagesCancelled��   �  � � � p   c c � � �� ��� 0 strawake strAwake � � �� 0 strsleep strSleep � �~�}�~ &0 displaysleepstate displaySleepState�}   �  � � � l     �|�{�z�|  �{  �z   �  � � � l     �y�x�w�y  �x  �w   �  � � � l     �v � ��v   � !  Global variable assignment    � � � � 6   G l o b a l   v a r i a b l e   a s s i g n m e n t �  � � � l     ��u�t � r      � � � b      � � � n    	 � � � 1    	�s
�s 
psxp � l     ��r�q � I    �p � �
�p .earsffdralis        afdr � m     �o
�o afdrdlib � �n ��m
�n 
from � m    �l
�l fldmfldu�m  �r  �q   � m   	 
 � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t � o      �k�k 	0 plist  �u  �t   �  � � � l     �j�i�h�j  �i  �h   �  � � � l    ��g�f � r     � � � m    �e
�e boovtrue � o      �d�d 0 initialbackup initialBackup�g  �f   �  � � � l    ��c�b � r     � � � m    �a
�a boovfals � o      �`�` 0 manualbackup manualBackup�c  �b   �  � � � l    ��_�^ � r     � � � m    �]
�] boovfals � o      �\�\ 0 isbackingup isBackingUp�_  �^   �  � � � l     �[�Z�Y�[  �Z  �Y   �  � � � l   % ��X�W � r    % � � � J    ! � �  � � � m     � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m     � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �  �  � m     � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d !   m     � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �V m     �		 � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�V   � o      �U�U "0 messagesmissing messagesMissing�X  �W   � 

 l     �T�S�R�T  �S  �R    l  & I�Q�P r   & I J   & E  m   & ) �  C o m e   o n !  m   ) , � 4 C o m e   o n !   E y e   o f   t h e   t i g e r !  m   , / � " N o   p a i n !   N o   p a i n !  m   / 2   �!! 0 L e t ' s   d o   t h i s   a s   a   t e a m ! "#" m   2 5$$ �%%  W e   c a n   d o   i t !# &'& m   5 8(( �))  F r e e e e e e e e e d o m !' *+* m   8 ;,, �-- 2 A l t o g e t h e r   o r   n o t   a t   a l l !+ ./. m   ; >00 �11 H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !/ 2�O2 m   > A33 �44 H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�O   o      �N�N *0 messagesencouraging messagesEncouraging�Q  �P   565 l     �M�L�K�M  �L  �K  6 787 l  J m9�J�I9 r   J m:;: J   J i<< =>= m   J M?? �@@  N i c e   o n e !> ABA m   M PCC �DD * Y o u   a b s o l u t e   l e g   e n d !B EFE m   P SGG �HH  G o o d   l a d !F IJI m   S VKK �LL  P e r f i c k !J MNM m   V YOO �PP  H a p p y   d a y s !N QRQ m   Y \SS �TT  F r e e e e e e e e e d o m !R UVU m   \ _WW �XX H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !V YZY m   _ b[[ �\\ B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !Z ]�H] m   b e^^ �__ d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�H  ; o      �G�G $0 messagescomplete messagesComplete�J  �I  8 `a` l     �F�E�D�F  �E  �D  a bcb l  n �d�C�Bd r   n �efe J   n �gg hih m   n qjj �kk  T h a t ' s   a   s h a m ei lml m   q tnn �oo  A h   m a n ,   u n l u c k ym pqp m   t wrr �ss  O h   w e l lq tut m   w zvv �ww @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e nu xyx m   z }zz �{{ , W e l l   t h a t ' s   a   l e t   d o w ny |}| m   } �~~ � P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?} ��� m   � ��� ���  A r r o g a n t� ��A� m   � ��� ��� > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�A  f o      �@�@ &0 messagescancelled messagesCancelled�C  �B  c ��� l     �?�>�=�?  �>  �=  � ��� l  � ���<�;� r   � ���� J   � ��� ��� m   � ��� ���  T h a t ' s   a   s h a m e� ��� m   � ��� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m   � ��� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m   � ��� ���  D i c k� ��� m   � ��� ���  F u c k t a r d� ��� m   � ��� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m   � ��� ���  A r r o g a n t� ��� m   � ��� ��� $ C h i l l   t h e   f u c k   o u t� ��:� m   � ��� ���  H o l d   f i r e�:  � o      �9�9 40 messagesalreadybackingup messagesAlreadyBackingUp�<  �;  � ��� l     �8�7�6�8  �7  �6  � ��� l  � ���5�4� r   � ���� m   � ��� ��� * " C u r r e n t P o w e r S t a t e " = 4� o      �3�3 0 strawake strAwake�5  �4  � ��� l  � ���2�1� r   � ���� m   � ��� ��� * " C u r r e n t P o w e r S t a t e " = 1� o      �0�0 0 strsleep strSleep�2  �1  � ��� l  � ���/�.� r   � ���� I  � ��-��,
�- .sysoexecTEXT���     TEXT� m   � ��� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�,  � o      �+�+ &0 displaysleepstate displaySleepState�/  �.  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� l     �$�#�"�$  �#  �"  � ��� l     �!���!  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     � ���   � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ����  �  �  � ��� l     ����  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     ����  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     ����  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i   c f��� I      ���� 0 	plistinit 	plistInit�  �  � k    ��� ��� l     ����  �   display dialog "PLISTINIT"   � ��� 4 d i s p l a y   d i a l o g   " P L I S T I N I T "� ��� q      �� ��� 0 
foldername 
folderName� ��� 0 
backupdisk 
backupDisk� ���  0 computerfolder computerFolder� ��� 0 
backuptime 
backupTime�  �    r      m     ��   o      �� 0 
backuptime 
backupTime  l   ����  �  �    Z   �	�

 l   �	� =     I    ��� 0 itexists itExists  m     �  f i l e � o    �� 	0 plist  �  �   m    �
� boovtrue�	  �  	 O    = k    <  r     n     1    �
� 
pcnt 4    �
� 
plif o    � �  	0 plist   o      ���� 0 thecontents theContents  r    " !  n     "#" 1     ��
�� 
valL# o    ���� 0 thecontents theContents! o      ���� 0 thevalue theValue $%$ l  # #��������  ��  ��  % &'& r   # (()( n   # &*+* o   $ &����  0 foldertobackup FolderToBackup+ o   # $���� 0 thevalue theValue) o      ���� 0 
foldername 
folderName' ,-, r   ) ../. n   ) ,010 o   * ,���� 0 backupdrive BackupDrive1 o   ) *���� 0 thevalue theValue/ o      ���� 0 
backupdisk 
backupDisk- 232 r   / 4454 n   / 2676 o   0 2����  0 computerfolder computerFolder7 o   / 0���� 0 thevalue theValue5 o      ����  0 computerfolder computerFolder3 898 r   5 ::;: n   5 8<=< o   6 8���� 0 scheduledtime scheduledTime= o   5 6���� 0 thevalue theValue; o      ���� 0 
backuptime 
backupTime9 >?> l  ; ;��������  ��  ��  ? @��@ l  ; ;��AB��  A . (log {folderName, backupDisk, backupTime}   B �CC P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��   m    DD�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �
  
 k   @�EE FGF r   @ GHIH I   @ E�������� .0 getcomputeridentifier getComputerIdentifier��  ��  I o      ����  0 computerfolder computerFolderG JKJ l  H H��������  ��  ��  K LML O   H �NON t   L �PQP k   N �RR STS I   N S�������� 0 setfocus setFocus��  ��  T UVU l  T T��������  ��  ��  V WXW r   T _YZY l  T ][����[ I  T ]����\
�� .sysostflalis    ��� null��  \ ��]��
�� 
prmp] m   V Y^^ �__ @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p��  ��  ��  Z o      ���� 0 
foldername 
folderNameX `a` r   ` obcb c   ` mded l  ` if����f I  ` i����g
�� .sysostflalis    ��� null��  g ��h��
�� 
prmph m   b eii �jj R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  e m   i l��
�� 
TEXTc o      ���� 0 
backupdisk 
backupDiska klk l  p p��������  ��  ��  l mnm r   p wopo n   p uqrq 1   q u��
�� 
psxpr o   p q���� 0 
foldername 
folderNamep o      ���� 0 
foldername 
folderNamen sts r   x uvu n   x }wxw 1   y }��
�� 
psxpx o   x y���� 0 
backupdisk 
backupDiskv o      ���� 0 
backupdisk 
backupDiskt yzy l  � ���������  ��  ��  z {|{ r   � �}~} J   � � ��� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r� ��� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r� ���� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ~ o      ���� 0 backuptimes backupTimes| ��� r   � ���� c   � ���� J   � ��� ���� I  � �����
�� .gtqpchltns    @   @ ns  � o   � ����� 0 backuptimes backupTimes� �����
�� 
prmp� m   � ��� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  � m   � ���
�� 
TEXT� o      ���� 0 selectedtime selectedTime� ��� l  � �������  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l  � ���������  ��  ��  � ���� l  � �������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  Q m   L M����  ��O m   H I���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  M ��� l  � ���������  ��  ��  � ���� O   ����� O   ����� k   ���� ��� I  ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
plii� ����
�� 
insh�  ;   � �� �����
�� 
prdt� K   �� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  	�� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ���� 0 
foldername 
folderName��  ��  � ��� I @�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   ��
�� 
plii� ����
�� 
insh�  ;  #%� �����
�� 
prdt� K  (:�� ����
�� 
kind� m  +.��
�� 
TEXT� ����
�� 
pnam� m  14�� ���  B a c k u p D r i v e� ����
�� 
valL� o  56�~�~ 0 
backupdisk 
backupDisk�  ��  � ��� I Ah�}�|�
�} .corecrel****      � null�|  � �{��
�{ 
kocl� m  EH�z
�z 
plii� �y��
�y 
insh�  ;  KM� �x��w
�x 
prdt� K  Pb�� �v��
�v 
kind� m  SV�u
�u 
TEXT� �t��
�t 
pnam� m  Y\�� ���  C o m p u t e r F o l d e r� �s��r
�s 
valL� o  ]^�q�q  0 computerfolder computerFolder�r  �w  � ��p� I i��o�n 
�o .corecrel****      � null�n    �m
�m 
kocl m  mp�l
�l 
plii �k
�k 
insh  ;  su �j�i
�j 
prdt K  x� �h
�h 
kind m  {~�g
�g 
TEXT �f	

�f 
pnam	 m  �� �  S c h e d u l e d T i m e
 �e�d
�e 
valL o  ���c�c 0 
backuptime 
backupTime�d  �i  �p  � l  � ��b�a I  � ��`�_
�` .corecrel****      � null�_   �^
�^ 
kocl m   � ��]
�] 
plif �\�[
�\ 
prdt K   � � �Z�Y
�Z 
pnam o   � ��X�X 	0 plist  �Y  �[  �b  �a  � m   � ��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��    l ���W�V�U�W  �V  �U    r  �� o  ���T�T 0 
foldername 
folderName o      �S�S 0 sourcefolder sourceFolder  r  �� o  ���R�R 0 
backupdisk 
backupDisk o      �Q�Q "0 destinationdisk destinationDisk  !  r  ��"#" o  ���P�P  0 computerfolder computerFolder# o      �O�O 0 machinefolder machineFolder! $%$ r  ��&'& o  ���N�N 0 
backuptime 
backupTime' o      �M�M 0 scheduledtime scheduledTime% ()( I ���L*�K
�L .ascrcmnt****      � ***** J  ��++ ,-, o  ���J�J 0 sourcefolder sourceFolder- ./. o  ���I�I "0 destinationdisk destinationDisk/ 010 o  ���H�H 0 machinefolder machineFolder1 2�G2 o  ���F�F 0 scheduledtime scheduledTime�G  �K  ) 343 l ���E�D�C�E  �D  �C  4 5�B5 I  ���A�@�?�A 0 diskinit diskInit�@  �?  �B  � 676 l     �>�=�<�>  �=  �<  7 898 l     �;�:�9�;  �:  �9  9 :;: l     �8�7�6�8  �7  �6  ; <=< l     �5�4�3�5  �4  �3  = >?> l     �2�1�0�2  �1  �0  ? @A@ l     �/BC�/  B H B Function to detect if the selected hard drive is connected or not   C �DD �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o tA EFE l     �.GH�.  G T N This only happens once a hard drive has been selected and provides a reminder   H �II �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e rF JKJ i   g jLML I      �-�,�+�- 0 diskinit diskInit�,  �+  M k     �NN OPO l     �*QR�*  Q  display dialog "DISKINIT"   R �SS 2 d i s p l a y   d i a l o g   " D I S K I N I T "P T�)T Z     �UV�(WU l    	X�'�&X =     	YZY I     �%[�$�% 0 itexists itExists[ \]\ m    ^^ �__  d i s k] `�#` o    �"�" "0 destinationdisk destinationDisk�#  �$  Z m    �!
�! boovtrue�'  �&  V I    � a��  0 freespaceinit freeSpaceInita b�b m    �
� boovfals�  �  �(  W k    �cc ded I    ���� 0 setfocus setFocus�  �  e fgf r     hih n    jkj 3    �
� 
cobjk o    �� "0 messagesmissing messagesMissingi o      �� 0 msg  g lml r   ! 1non l  ! /p��p n   ! /qrq 1   - /�
� 
bhitr l  ! -s��s I  ! -�tu
� .sysodlogaskr        TEXTt o   ! "�� 0 msg  u �vw
� 
btnsv J   # 'xx yzy m   # ${{ �||  C a n c e l   B a c k u pz }�} m   $ %~~ �  O K�  w ���
� 
dflt� m   ( )�� �  �  �  �  �  o o      �
�
 	0 reply  m ��	� Z   2 ������ l  2 5���� =   2 5��� o   2 3�� 	0 reply  � m   3 4�� ���  O K�  �  � I   8 =���� 0 diskinit diskInit�  �  �  � Z   @ ����� � l  @ G������ E   @ G��� o   @ C���� &0 displaysleepstate displaySleepState� o   C F���� 0 strawake strAwake��  ��  � k   J ��� ��� r   J Q��� n   J O��� 3   M O��
�� 
cobj� o   J M���� &0 messagescancelled messagesCancelled� o      ���� 0 msg  � ���� Z   R ������� l  R Y������ =   R Y��� o   R W���� 0 nsfw  � m   W X��
�� boovtrue��  ��  � I  \ g����
�� .sysonotfnull��� ��� TEXT� o   \ ]���� 0 msg  � �����
�� 
appr� m   ` c�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  � ��� l  j q������ =   j q��� o   j o���� 0 nsfw  � m   o p��
�� boovfals��  ��  � ���� I  t �����
�� .sysonotfnull��� ��� TEXT� m   t w�� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �����
�� 
appr� m   z }�� ��� 
 W B T F U��  ��  ��  ��  �  �   �	  �)  K ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i   k n��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � k     ��� ��� l     ������  � $ display dialog "FREESPACEINIT"   � ��� < d i s p l a y   d i a l o g   " F R E E S P A C E I N I T "� ���� Z     ������� l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    ��� ��� I    �������� 0 setfocus setFocus��  ��  � ��� l   ��������  ��  ��  � ��� Z    M������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r     0��� l    .������ n     .��� 1   , .��
�� 
bhit� l    ,������ I    ,����
�� .sysodlogaskr        TEXT� m     !�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   " &�� ��� m   " #�� ���  Y e s� ���� m   # $�� ���  C a n c e l   B a c k u p��  � �� ��
�� 
dflt  m   ' (���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  �  l  3 6���� =   3 6 o   3 4���� 	0 again   m   4 5��
�� boovtrue��  ��   �� r   9 I l  9 G	����	 n   9 G

 1   E G��
�� 
bhit l  9 E���� I  9 E��
�� .sysodlogaskr        TEXT m   9 : � � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ? ��
�� 
btns J   ; ?  m   ; < �  Y e s �� m   < = �  C a n c e l   B a c k u p��   ����
�� 
dflt m   @ A���� ��  ��  ��  ��  ��   o      ���� 
0 reply1  ��  ��  �  l  N N��������  ��  ��   �� Z   N � ��! l  N S"����" =   N S#$# o   N O���� 
0 reply1  $ m   O R%% �&&  Y e s��  ��    I   V [�������� 0 tidyup tidyUp��  ��  ��  ! k   ^ �'' ()( Z   ^ u*+����* l  ^ e,����, E   ^ e-.- o   ^ a���� &0 displaysleepstate displaySleepState. o   a d���� 0 strawake strAwake��  ��  + r   h q/0/ n   h o121 3   k o��
�� 
cobj2 o   h k���� &0 messagescancelled messagesCancelled0 o      ���� 0 msg  ��  ��  ) 3��3 Z   v �456��4 l  v }7����7 =   v }898 o   v {���� 0 nsfw  9 m   { |��
�� boovtrue��  ��  5 I  � ���:;
�� .sysonotfnull��� ��� TEXT: o   � ��� 0 msg  ; �~<�}
�~ 
appr< m   � �== �>> > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�}  6 ?@? l  � �A�|�{A =   � �BCB o   � ��z�z 0 nsfw  C m   � ��y
�y boovfals�|  �{  @ D�xD I  � ��wEF
�w .sysonotfnull��� ��� TEXTE m   � �GG �HH , Y o u   s t o p p e d   b a c k i n g   u pF �vI�u
�v 
apprI m   � �JJ �KK 
 W B T F U�u  �x  ��  ��  ��  ��  � LML l     �t�s�r�t  �s  �r  M NON l     �q�p�o�q  �p  �o  O PQP l     �n�m�l�n  �m  �l  Q RSR l     �k�j�i�k  �j  �i  S TUT l     �h�g�f�h  �g  �f  U VWV l     �eXY�e  X,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   Y �ZZL   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .W [\[ l     �d]^�d  ] g a These folders, if required, are then set to their corresponding global variables declared above.   ^ �__ �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .\ `a` i   o rbcb I      �c�b�a�c 0 
backupinit 
backupInit�b  �a  c k     �dd efe l     �`gh�`  g ! display dialog "BACKUPINIT"   h �ii 6 d i s p l a y   d i a l o g   " B A C K U P I N I T "f jkj r     lml 4     �_n
�_ 
psxfn o    �^�^ "0 destinationdisk destinationDiskm o      �]�] 	0 drive  k opo l   �\qr�\  q ' !log (destinationDisk & "Backups")   r �ss B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )p tut l   �[�Z�Y�[  �Z  �Y  u vwv Z    ,xy�X�Wx l   z�V�Uz =    {|{ I    �T}�S�T 0 itexists itExists} ~~ m    	�� ���  f o l d e r ��R� b   	 ��� o   	 
�Q�Q "0 destinationdisk destinationDisk� m   
 �� ���  B a c k u p s�R  �S  | m    �P
�P boovfals�V  �U  y O    (��� I   '�O�N�
�O .corecrel****      � null�N  � �M��
�M 
kocl� m    �L
�L 
cfol� �K��
�K 
insh� o    �J�J 	0 drive  � �I��H
�I 
prdt� K    #�� �G��F
�G 
pnam� m     !�� ���  B a c k u p s�F  �H  � m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �X  �W  w ��� l  - -�E�D�C�E  �D  �C  � ��� Z   - d���B�A� l  - >��@�?� =   - >��� I   - <�>��=�> 0 itexists itExists� ��� m   . /�� ���  f o l d e r� ��<� b   / 8��� b   / 4��� o   / 0�;�; "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�:�: 0 machinefolder machineFolder�<  �=  � m   < =�9
�9 boovfals�@  �?  � O   A `��� I  E _�8�7�
�8 .corecrel****      � null�7  � �6��
�6 
kocl� m   G H�5
�5 
cfol� �4��
�4 
insh� n   I T��� 4   O T�3�
�3 
cfol� m   P S�� ���  B a c k u p s� 4   I O�2�
�2 
cdis� o   M N�1�1 	0 drive  � �0��/
�0 
prdt� K   U [�� �.��-
�. 
pnam� o   V Y�,�, 0 machinefolder machineFolder�-  �/  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �B  �A  � ��� l  e e�+�*�)�+  �*  �)  � ��� O   e ���� k   i �� ��� l  i i�(���(  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y�'�
�' 
cfol� o   u x�&�& 0 machinefolder machineFolder� n   i t��� 4   o t�%�
�% 
cfol� m   p s�� ���  B a c k u p s� 4   i o�$�
�$ 
cdis� o   m n�#�# 	0 drive  � o      �"�" 0 backupfolder backupFolder� ��!� l  ~ ~� ���   �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�!  � m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � �����  �  �  � ��� Z   � ������ l  � ����� =   � ���� I   � ����� 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��� b   � ���� b   � ���� b   � ���� o   � ��� "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��� 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�  �  � m   � ��
� boovfals�  �  � O   � ���� I  � ����
� .corecrel****      � null�  � ���
� 
kocl� m   � ��
� 
cfol� ���
� 
insh� o   � ��� 0 backupfolder backupFolder� ���
� 
prdt� K   � ��� ���

� 
pnam� m   � ��� ���  L a t e s t�
  �  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �  � r   � �� � m   � ��	
�	 boovfals  o      �� 0 initialbackup initialBackup�  l  � �����  �  �    O   � � k   � � 	 r   � �

 n   � � 4   � ��
� 
cfol m   � � �  L a t e s t n   � � 4   � ��
� 
cfol o   � ��� 0 machinefolder machineFolder n   � � 4   � ��
� 
cfol m   � � �  B a c k u p s 4   � �� 
�  
cdis o   � ����� 	0 drive   o      ���� 0 latestfolder latestFolder	 �� l  � �����    log (backupFolder)    � $ l o g   ( b a c k u p F o l d e r )��   m   � ��                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��     l  � ���������  ��  ��    !��! I   � ��������� 
0 backup  ��  ��  ��  a "#" l     ��������  ��  ��  # $%$ l     ��������  ��  ��  % &'& l     ��������  ��  ��  ' ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��./��  . j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.   / �00 �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .- 121 l     ��34��  3 � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   4 �55�   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .2 676 l     ��89��  8 � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   9 �::P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .7 ;<; l     ��=>��  = � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   > �??>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .< @A@ i   s vBCB I      �������� 0 tidyup tidyUp��  ��  C k    TDD EFE l     ��GH��  G  display dialog "TIDYUP"   H �II . d i s p l a y   d i a l o g   " T I D Y U P "F JKJ r     LML 4     ��N
�� 
psxfN o    ���� "0 destinationdisk destinationDiskM o      ���� 	0 drive  K OPO l   ��������  ��  ��  P Q��Q O   TRSR k   STT UVU l   ��WX��  W A ;set creationDates to creation date of items of backupFolder   X �YY v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e rV Z[Z r    \]\ n    ^_^ 4    ��`
�� 
cfol` o    ���� 0 machinefolder machineFolder_ n    aba 4    ��c
�� 
cfolc m    dd �ee  B a c k u p sb 4    ��f
�� 
cdisf o    ���� 	0 drive  ] o      ���� 0 bf bF[ ghg r    iji n    klk 1    ��
�� 
ascdl n    mnm 2   ��
�� 
cobjn o    ���� 0 bf bFj o      ���� 0 creationdates creationDatesh opo r     &qrq n     $sts 4   ! $��u
�� 
cobju m   " #���� t o     !���� 0 creationdates creationDatesr o      ���� 0 theoldestdate theOldestDatep vwv r   ' *xyx m   ' (���� y o      ���� 0 j  w z{z l  + +��������  ��  ��  { |}| Y   + n~�����~ k   9 i�� ��� r   9 ?��� n   9 =��� 4   : =���
�� 
cobj� o   ; <���� 0 i  � o   9 :���� 0 creationdates creationDates� o      ���� 0 thisdate thisDate� ���� Z   @ i������� l  @ H������ >  @ H��� n   @ F��� 1   D F��
�� 
pnam� 4   @ D���
�� 
cobj� o   B C���� 0 i  � m   F G�� ���  L a t e s t��  ��  � Z   K e������� l  K N������ A   K N��� o   K L���� 0 thisdate thisDate� o   L M���� 0 theoldestdate theOldestDate��  ��  � k   Q a�� ��� r   Q T��� o   Q R���� 0 thisdate thisDate� o      ���� 0 theoldestdate theOldestDate� ��� r   U X��� o   U V���� 0 i  � o      ���� 0 j  � ��� l  Y Y������  � " log (item j of backupFolder)   � ��� 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )� ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i   m   . /���� � I  / 4�����
�� .corecnte****       ****� o   / 0���� 0 creationdates creationDates��  ��  } ��� l  o o��������  ��  ��  � ��� l  o o������  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� I  o y�����
�� .coredeloobj        obj � n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  � ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l   z z������  � � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� I   z �������� 0 setfocus setFocus��  ��  � ��� r   � ���� l  � ������ n   � ���� 1   � ��~
�~ 
bhit� l  � ���}�|� I  � ��{��
�{ .sysodlogaskr        TEXT� m   � ��� ���B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .� �z��
�z 
btns� J   � ��� ��� m   � ��� ���  E m p t y   T r a s h� ��y� m   � ��� ���  C a n c e l   B a c k u p�y  � �x��w
�x 
dflt� m   � ��v�v �w  �}  �|  ��  �  � o      �u�u 
0 reply2  � ��� Z   � ����t�� l  � ���s�r� =   � ���� o   � ��q�q 
0 reply2  � m   � ��� ���  E m p t y   T r a s h�s  �r  � I  � ��p��o
�p .fndremptnull��� ��� obj � l  � ���n�m� 1   � ��l
�l 
trsh�n  �m  �o  �t  � Z   � ����k�j� l  � ���i�h� E   � ���� o   � ��g�g &0 displaysleepstate displaySleepState� o   � ��f�f 0 strawake strAwake�i  �h  � k   � ��� ��� r   � ���� n   � �� � 3   � ��e
�e 
cobj  o   � ��d�d &0 messagescancelled messagesCancelled� o      �c�c 0 msg  � �b Z   � ��a l  � ��`�_ =   � � o   � ��^�^ 0 nsfw   m   � ��]
�] boovtrue�`  �_   I  � ��\	
�\ .sysonotfnull��� ��� TEXT o   � ��[�[ 0 msg  	 �Z
�Y
�Z 
appr
 m   � � � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�Y    l  � ��X�W =   � � o   � ��V�V 0 nsfw   m   � ��U
�U boovfals�X  �W   �T I  � ��S
�S .sysonotfnull��� ��� TEXT m   � � � , Y o u   s t o p p e d   b a c k i n g   u p �R�Q
�R 
appr m   � � � 
 W B T F U�Q  �T  �a  �b  �k  �j  �  l  � ��P�O�N�P  �O  �N    I   ��M�L�M 0 freespaceinit freeSpaceInit �K m   � ��J
�J boovtrue�K  �L    �I  Z  S!"�H�G! l #�F�E# E  $%$ o  �D�D &0 displaysleepstate displaySleepState% o  
�C�C 0 strawake strAwake�F  �E  " Z  O&'(�B& l )�A�@) =  *+* o  �?�? 0 nsfw  + m  �>
�> boovtrue�A  �@  ' k  +,, -.- I %�=/0
�= .sysonotfnull��� ��� TEXT/ m  11 �22 � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .0 �<3�;
�< 
appr3 m  !44 �55 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�;  . 6�:6 I &+�97�8
�9 .sysodelanull��� ��� nmbr7 m  &'�7�7 �8  �:  ( 898 l .5:�6�5: =  .5;<; o  .3�4�4 0 nsfw  < m  34�3
�3 boovfals�6  �5  9 =�2= k  8K>> ?@? I 8E�1AB
�1 .sysonotfnull��� ��� TEXTA m  8;CC �DD t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i nB �0E�/
�0 
apprE m  >AFF �GG 
 W B T F U�/  @ H�.H I FK�-I�,
�- .sysodelanull��� ��� nmbrI m  FG�+�+ �,  �.  �2  �B  �H  �G  �I  S m    JJ�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��  A KLK l     �*�)�(�*  �)  �(  L MNM l     �'�&�%�'  �&  �%  N OPO l     �$�#�"�$  �#  �"  P QRQ l     �!� ��!  �   �  R STS l     ����  �  �  T UVU l     �WX�  W M G Function that carries out the backup process and is split in 2 parts.    X �YY �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  V Z[Z l     �\]�  \*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   ] �^^H   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .[ _`_ l     �ab�  a � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   b �cc   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .` ded i   w zfgf I      ���� 
0 backup  �  �  g k    �hh iji l     �kl�  k  display dialog "BACKUP"   l �mm . d i s p l a y   d i a l o g   " B A C K U P "j non q      pp �q� 0 t  q �r� 0 x  r ��� "0 containerfolder containerFolder�  o sts r     uvu I     �w�� 0 gettimestamp getTimestampw x�x m    �
� boovtrue�  �  v o      �� 0 t  t yzy l  	 	��
�	�  �
  �	  z {|{ r   	 }~} m   	 
�
� boovtrue~ o      �� 0 isbackingup isBackingUp| � l   ����  �  �  � ��� I    ���� 0 	stopwatch  � ��� m    �� ��� 
 s t a r t�  �  � ��� l   � �����   ��  ��  � ��� O   ���� k   ��� ��� l   ������  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d� ��� l   ������  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p� ��� r     ��� c    ��� 4    ���
�� 
psxf� o    ���� 0 sourcefolder sourceFolder� m    ��
�� 
TEXT� o      ���� 0 
foldername 
folderName� ��� r   ! )��� n   ! '��� 1   % '��
�� 
pnam� 4   ! %���
�� 
cfol� o   # $���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  * *������  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  * *��������  ��  ��  � ��� Z   * �������� l  * -������ =   * -��� o   * +���� 0 initialbackup initialBackup� m   + ,��
�� boovfals��  ��  � k   0 �� ��� I  0 >�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   2 3��
�� 
cfol� ����
�� 
insh� o   4 5���� 0 backupfolder backupFolder� �����
�� 
prdt� K   6 :�� �����
�� 
pnam� o   7 8���� 0 t  ��  ��  � ��� l  ? ?��������  ��  ��  � ��� r   ? I��� c   ? E��� 4   ? C���
�� 
psxf� o   A B���� 0 sourcefolder sourceFolder� m   C D��
�� 
TEXT� o      ���� (0 activesourcefolder activeSourceFolder� ��� r   J T��� 4   J P���
�� 
cfol� o   L O���� (0 activesourcefolder activeSourceFolder� o      ���� (0 activesourcefolder activeSourceFolder� ��� l  U U������  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   U n��� n   U j��� 4   g j���
�� 
cfol� o   h i���� 0 t  � n   U g��� 4   b g���
�� 
cfol� o   c f���� 0 machinefolder machineFolder� n   U b��� 4   ] b���
�� 
cfol� m   ^ a�� ���  B a c k u p s� 4   U ]���
�� 
cdis� o   Y \���� 	0 drive  � o      ���� (0 activebackupfolder activeBackupFolder� ��� l  o o������  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ���� I  o �����
�� .corecrel****      � null��  � ����
�� 
kocl� m   q r��
�� 
cfol� ����
�� 
insh� o   s v���� (0 activebackupfolder activeBackupFolder� �����
�� 
prdt� K   w {�� �����
�� 
pnam� o   x y���� 0 
foldername 
folderName��  ��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �������  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � ��� ��    D > This means that only new or updated files will be copied over    � |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r�  l  � �����   ] W Any deleted files in the source folder will be deleted in the destination folder too		    � �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	 	 l  � ���
��  
   Original sync code     � (   O r i g i n a l   s y n c   c o d e  	  l  � �����   z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"    � �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "  l  � ���������  ��  ��    l  � �����     Original code    �    O r i g i n a l   c o d e  l  � �����   i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path    � �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h  l  � ��� !��    x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   ! �"" �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d . #$# l  � ���%&��  % p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   & �'' �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .$ ()( l  � ���*+��  * � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   + �,,�   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .) -.- l  � ���/0��  / � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   0 �11v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .. 232 l  � ���������  ��  ��  3 454 l  � ���������  ��  ��  5 6��6 Z   ��789��7 l  � �:����: =   � �;<; o   � ����� 0 initialbackup initialBackup< m   � ���
�� boovtrue��  ��  8 k   ��== >?> r   � �@A@ n   � �BCB 1   � ���
�� 
timeC l  � �D����D I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  A o      ���� 0 	starttime 	startTime? EFE Z   � �GH����G l  � �I����I E   � �JKJ o   � ����� &0 displaysleepstate displaySleepStateK o   � ����� 0 strawake strAwake��  ��  H Z   � �LMN��L l  � �O����O =   � �PQP o   � ����� 0 nsfw  Q m   � ���
�� boovtrue��  ��  M I  � ���RS
�� .sysonotfnull��� ��� TEXTR m   � �TT �UU � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .S ��V��
�� 
apprV m   � �WW �XX 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  N YZY l  � �[����[ =   � �\]\ o   � ����� 0 nsfw  ] m   � ���
�� boovfals��  ��  Z ^��^ I  � ���_`
�� .sysonotfnull��� ��� TEXT_ m   � �aa �bb \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e` ��c��
�� 
apprc m   � �dd �ee 
 W B T F U��  ��  ��  ��  ��  F fgf l  � �����~��  �  �~  g hih l  � ��}�|�{�}  �|  �{  i jkj l  � ��z�y�x�z  �y  �x  k lml l  � ��w�v�u�w  �v  �u  m non r   � �pqp c   � �rsr l  � �t�t�st b   � �uvu b   � �wxw b   � �yzy b   � �{|{ b   � �}~} o   � ��r�r "0 destinationdisk destinationDisk~ m   � � ���  B a c k u p s /| o   � ��q�q 0 machinefolder machineFolderz m   � ��� ���  / L a t e s t /x o   � ��p�p 0 
foldername 
folderNamev m   � ��� ���  /�t  �s  s m   � ��o
�o 
TEXTq o      �n�n 0 d  o ��� l  � ��m���m  � D >do shell script "ditto '" & sourceFolder & "' '" & d & "/'"			   � ��� | d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' " 	 	 	� ��� I  �	�l��k
�l .sysoexecTEXT���     TEXT� b   ���� b   ���� b   � ���� b   � ���� m   � ��� ���� r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   '� o   � ��j�j 0 sourcefolder sourceFolder� m   � ��� ���  '   '� o   � �i�i 0 d  � m  �� ���  / '�k  � ��� l 

�h�g�f�h  �g  �f  � ��� l 

�e�d�c�e  �d  �c  � ��� l 

�b�a�`�b  �a  �`  � ��� l 

�_�^�]�_  �^  �]  � ��� r  
��� m  
�\
�\ boovfals� o      �[�[ 0 isbackingup isBackingUp� ��� r  ��� m  �Z
�Z boovfals� o      �Y�Y 0 manualbackup manualBackup� ��� l �X�W�V�X  �W  �V  � ��U� Z  ����T�S� l ��R�Q� E  ��� o  �P�P &0 displaysleepstate displaySleepState� o  �O�O 0 strawake strAwake�R  �Q  � k  ��� ��� r  +��� n  '��� 1  #'�N
�N 
time� l #��M�L� I #�K�J�I
�K .misccurdldt    ��� null�J  �I  �M  �L  � o      �H�H 0 endtime endTime� ��� r  ,3��� n ,1��� I  -1�G�F�E�G 0 getduration getDuration�F  �E  �  f  ,-� o      �D�D 0 duration  � ��� r  4=��� n  4;��� 3  7;�C
�C 
cobj� o  47�B�B $0 messagescomplete messagesComplete� o      �A�A 0 msg  � ��@� Z  >�����?� l >E��>�=� =  >E��� o  >C�<�< 0 nsfw  � m  CD�;
�; boovtrue�>  �=  � k  Hc�� ��� I H]�:��
�: .sysonotfnull��� ��� TEXT� b  HS��� b  HQ��� b  HM��� m  HK�� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  KL�9�9 0 duration  � m  MP�� ���    m i n u t e s ! 
� o  QR�8�8 0 msg  � �7��6
�7 
appr� m  VY�� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�6  � ��5� I ^c�4��3
�4 .sysodelanull��� ��� nmbr� m  ^_�2�2 �3  �5  � ��� l fm��1�0� =  fm��� o  fk�/�/ 0 nsfw  � m  kl�.
�. boovfals�1  �0  � ��-� k  p��� ��� I p��,��
�, .sysonotfnull��� ��� TEXT� b  py��� b  pu��� m  ps�� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  st�+�+ 0 duration  � m  ux�� ���    m i n u t e s ! 
� �*��)
�* 
appr� m  |�� ���  B a c k e d   u p !�)  � ��(� I ���'��&
�' .sysodelanull��� ��� nmbr� m  ���%�% �&  �(  �-  �?  �@  �T  �S  �U  9 ��� l ����$�#� =  ����� o  ���"�" 0 initialbackup initialBackup� m  ���!
�! boovfals�$  �#  � �� � k  ��    r  �� c  �� l ���� b  ��	 b  ��

 b  �� b  �� b  �� b  �� b  �� o  ���� "0 destinationdisk destinationDisk m  �� �  B a c k u p s / o  ���� 0 machinefolder machineFolder m  �� �  / o  ���� 0 t   m  �� �  / o  ���� 0 
foldername 
folderName	 m  �� �  /�  �   m  ���
� 
TEXT o      �� 0 c    r  �� !  c  ��"#" l ��$��$ b  ��%&% b  ��'(' b  ��)*) b  ��+,+ b  ��-.- o  ���� "0 destinationdisk destinationDisk. m  ��// �00  B a c k u p s /, o  ���� 0 machinefolder machineFolder* m  ��11 �22  / L a t e s t /( o  ���� 0 
foldername 
folderName& m  ��33 �44  /�  �  # m  ���
� 
TEXT! o      �� 0 d   565 I ���7�
� .ascrcmnt****      � ****7 l ��8��8 b  ��9:9 m  ��;; �<<  b a c k i n g   u p   t o :  : o  ���� 0 d  �  �  �  6 =>= l ����
�	�  �
  �	  > ?@? Z  �CAB��A l ��C��C E  ��DED o  ���� &0 displaysleepstate displaySleepStateE o  ���� 0 strawake strAwake�  �  B k  �?FF GHG r  ��IJI n  ��KLK 1  ���
� 
timeL l ��M�� M I ��������
�� .misccurdldt    ��� null��  ��  �  �   J o      ���� 0 	starttime 	startTimeH NON r  ��PQP n  ��RSR 3  ����
�� 
cobjS o  ������ *0 messagesencouraging messagesEncouragingQ o      ���� 0 msg  O T��T Z   ?UVW��U l  X����X =   YZY o   ���� 0 nsfw  Z m  ��
�� boovtrue��  ��  V k  
[[ \]\ I 
��^_
�� .sysonotfnull��� ��� TEXT^ o  
���� 0 msg  _ ��`��
�� 
appr` m  aa �bb 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  ] c��c I ��d��
�� .sysodelanull��� ��� nmbrd m  ���� ��  ��  W efe l %g����g =  %hih o  #���� 0 nsfw  i m  #$��
�� boovfals��  ��  f j��j k  (;kk lml I (5��no
�� .sysonotfnull��� ��� TEXTn m  (+pp �qq  B a c k i n g   u po ��r��
�� 
apprr m  .1ss �tt 
 W B T F U��  m u��u I 6;��v��
�� .sysodelanull��� ��� nmbrv m  67���� ��  ��  ��  ��  ��  �  �  @ wxw l DD��������  ��  ��  x yzy l DD��������  ��  ��  z {|{ l DD��������  ��  ��  | }~} l DD��������  ��  ��  ~ � I D]�����
�� .sysoexecTEXT���     TEXT� b  DY��� b  DU��� b  DS��� b  DO��� b  DM��� b  DI��� m  DG�� ���( r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  GH���� 0 c  � m  IL�� ���  '   '� o  MN���� 0 sourcefolder sourceFolder� m  OR�� ���  '   '� o  ST���� 0 d  � m  UX�� ���  / '��  � ��� l ^^��������  ��  ��  � ��� l ^^��������  ��  ��  � ��� l ^^��������  ��  ��  � ��� l ^^��������  ��  ��  � ��� r  ^a��� m  ^_��
�� boovfals� o      ���� 0 isbackingup isBackingUp� ��� r  bg��� m  bc��
�� boovfals� o      ���� 0 manualbackup manualBackup� ��� l hh��������  ��  ��  � ���� Z  h������� = hw��� n  ht��� 2 pt��
�� 
cobj� l hp������ c  hp��� 4  hl���
�� 
psxf� o  jk���� 0 c  � m  lo��
�� 
alis��  ��  � J  tv����  � k  z��� ��� l zz������  � % delete folder t of backupFolder   � ��� > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r� ��� l zz��������  ��  ��  � ��� r  z���� c  z���� n  z~��� 4  {~���
�� 
cfol� o  |}���� 0 t  � o  z{���� 0 backupfolder backupFolder� m  ~��
�� 
TEXT� o      ���� 0 oldestfolder oldestFolder� ��� r  ����� c  ����� n  ����� 1  ����
�� 
psxp� o  ������ 0 oldestfolder oldestFolder� m  ����
�� 
TEXT� o      ���� 0 oldestfolder oldestFolder� ��� I �������
�� .ascrcmnt****      � ****� l �������� b  ����� m  ���� ���  t o   d e l e t e :  � o  ������ 0 oldestfolder oldestFolder��  ��  ��  � ��� l ����������  ��  ��  � ��� r  ����� b  ����� b  ����� m  ���� ���  r m   - r f   '� o  ������ 0 oldestfolder oldestFolder� m  ���� ���  '� o      ���� 0 todelete toDelete� ��� I �������
�� .sysoexecTEXT���     TEXT� o  ������ 0 todelete toDelete��  � ��� l ����������  ��  ��  � ���� Z  ��������� l �������� E  ����� o  ������ &0 displaysleepstate displaySleepState� o  ������ 0 strawake strAwake��  ��  � k  ���� ��� r  ����� n  ����� 1  ����
�� 
time� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 endtime endTime� ��� r  ����� n ����� I  ���������� 0 getduration getDuration��  ��  �  f  ��� o      ���� 0 duration  � ��� l ��������  � � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   � �	 	  d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "� 			 l ����		��  	  delay 2   	 �		  d e l a y   2	 			 l ������~��  �  �~  	 				 r  ��	
		
 n  ��			 3  ���}
�} 
cobj	 o  ���|�| $0 messagescomplete messagesComplete	 o      �{�{ 0 msg  		 			 Z  �|		�z		 l ��	�y�x	 =  ��			 o  ���w�w 0 duration  	 m  ��		 ?�      �y  �x  	 Z  �*			�v	 l ��	�u�t	 =  ��			 o  ���s�s 0 nsfw  	 m  ���r
�r boovtrue�u  �t  	 k  � 		 			 I ���q	 	!
�q .sysonotfnull��� ��� TEXT	  b  ��	"	#	" b  ��	$	%	$ b  ��	&	'	& m  ��	(	( �	)	)  A n d   i n  	' o  ���p�p 0 duration  	% m  ��	*	* �	+	+    m i n u t e ! 
	# o  ���o�o 0 msg  	! �n	,�m
�n 
appr	, m  ��	-	- �	.	. : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�m  	 	/�l	/ I � �k	0�j
�k .sysodelanull��� ��� nmbr	0 m  ���i�i �j  �l  	 	1	2	1 l 
	3�h�g	3 =  
	4	5	4 o  �f�f 0 nsfw  	5 m  	�e
�e boovfals�h  �g  	2 	6�d	6 k  &	7	7 	8	9	8 I  �c	:	;
�c .sysonotfnull��� ��� TEXT	: b  	<	=	< b  	>	?	> m  	@	@ �	A	A  A n d   i n  	? o  �b�b 0 duration  	= m  	B	B �	C	C    m i n u t e ! 
	; �a	D�`
�a 
appr	D m  	E	E �	F	F  B a c k e d   u p !�`  	9 	G�_	G I !&�^	H�]
�^ .sysodelanull��� ��� nmbr	H m  !"�\�\ �]  �_  �d  �v  �z  	 Z  -|	I	J	K�[	I l -4	L�Z�Y	L =  -4	M	N	M o  -2�X�X 0 nsfw  	N m  23�W
�W boovtrue�Z  �Y  	J k  7R	O	O 	P	Q	P I 7L�V	R	S
�V .sysonotfnull��� ��� TEXT	R b  7B	T	U	T b  7@	V	W	V b  7<	X	Y	X m  7:	Z	Z �	[	[  A n d   i n  	Y o  :;�U�U 0 duration  	W m  <?	\	\ �	]	]    m i n u t e s ! 
	U o  @A�T�T 0 msg  	S �S	^�R
�S 
appr	^ m  EH	_	_ �	`	` : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�R  	Q 	a�Q	a I MR�P	b�O
�P .sysodelanull��� ��� nmbr	b m  MN�N�N �O  �Q  	K 	c	d	c l U\	e�M�L	e =  U\	f	g	f o  UZ�K�K 0 nsfw  	g m  Z[�J
�J boovfals�M  �L  	d 	h�I	h k  _x	i	i 	j	k	j I _r�H	l	m
�H .sysonotfnull��� ��� TEXT	l b  _h	n	o	n b  _d	p	q	p m  _b	r	r �	s	s  A n d   i n  	q o  bc�G�G 0 duration  	o m  dg	t	t �	u	u    m i n u t e s ! 
	m �F	v�E
�F 
appr	v m  kn	w	w �	x	x  B a c k e d   u p !�E  	k 	y�D	y I sx�C	z�B
�C .sysodelanull��� ��� nmbr	z m  st�A�A �B  �D  �I  �[  	 	{	|	{ l }}�@�?�>�@  �?  �>  	| 	}�=	} Z  }�	~		��<	~ l }�	��;�:	� =  }�	�	�	� o  }��9�9 0 nsfw  	� m  ���8
�8 boovtrue�;  �:  	 I ���7	�	�
�7 .sysonotfnull��� ��� TEXT	� m  ��	�	� �	�	� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .	� �6	��5
�6 
appr	� m  ��	�	� �	�	� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r�5  	� 	�	�	� l ��	��4�3	� =  ��	�	�	� o  ���2�2 0 nsfw  	� m  ���1
�1 boovfals�4  �3  	� 	��0	� I ���/	�	�
�/ .sysonotfnull��� ��� TEXT	� m  ��	�	� �	�	� p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d	� �.	��-
�. 
appr	� m  ��	�	� �	�	� 2 D e l e t e d   n e w   b a c k u p   f o l d e r�-  �0  �<  �=  ��  ��  ��  ��  � Z  ��	�	��,�+	� l ��	��*�)	� E  ��	�	�	� o  ���(�( &0 displaysleepstate displaySleepState	� o  ���'�' 0 strawake strAwake�*  �)  	� k  ��	�	� 	�	�	� r  ��	�	�	� n  ��	�	�	� 1  ���&
�& 
time	� l ��	��%�$	� I ���#�"�!
�# .misccurdldt    ��� null�"  �!  �%  �$  	� o      � �  0 endtime endTime	� 	�	�	� r  ��	�	�	� n ��	�	�	� I  ������ 0 getduration getDuration�  �  	�  f  ��	� o      �� 0 duration  	� 	�	�	� r  ��	�	�	� n  ��	�	�	� 3  ���
� 
cobj	� o  ���� $0 messagescomplete messagesComplete	� o      �� 0 msg  	� 	��	� Z  ��	�	��	�	� l ��	���	� =  ��	�	�	� o  ���� 0 duration  	� m  ��	�	� ?�      �  �  	� Z  �:	�	�	��	� l ��	���	� =  ��	�	�	� o  ���� 0 nsfw  	� m  ���
� boovtrue�  �  	� k  �	�	� 	�	�	� I �
�	�	�
� .sysonotfnull��� ��� TEXT	� b  � 	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ���� 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� o  ���� 0 msg  	� �	��

� 
appr	� m  	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�
  	� 	��		� I �	��
� .sysodelanull��� ��� nmbr	� m  �� �  �	  	� 	�	�	� l 	���	� =  	�	�	� o  �� 0 nsfw  	� m  �
� boovfals�  �  	� 	��	� k  6	�	� 	�	�	� I 0� 	�	�
�  .sysonotfnull��� ��� TEXT	� b  &	�	�	� b  "	�	�	� m   	�	� �	�	�  A n d   i n  	� o   !���� 0 duration  	� m  "%	�	� �	�	�    m i n u t e ! 
	� ��	���
�� 
appr	� m  ),	�	� �	�	�  B a c k e d   u p !��  	� 	���	� I 16��	���
�� .sysodelanull��� ��� nmbr	� m  12���� ��  ��  �  �  �  	� Z  =�	�	�	���	� l =D	�����	� =  =D	�	�	� o  =B���� 0 nsfw  	� m  BC��
�� boovtrue��  ��  	� k  Gb	�	� 	�	�	� I G\��	�	�
�� .sysonotfnull��� ��� TEXT	� b  GR	�	�	� b  GP	�	�	� b  GL	�	�	� m  GJ	�	� �	�	�  A n d   i n  	� o  JK���� 0 duration  	� m  LO	�	� �	�	�    m i n u t e s ! 
	� o  PQ���� 0 msg  	� ��
 ��
�� 
appr
  m  UX

 �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 
��
 I ]b��
��
�� .sysodelanull��� ��� nmbr
 m  ]^���� ��  ��  	� 


 l el
����
 =  el

	
 o  ej���� 0 nsfw  
	 m  jk��
�� boovfals��  ��  
 

��

 k  o�

 


 I o���


�� .sysonotfnull��� ��� TEXT
 b  ox


 b  ot


 m  or

 �

  A n d   i n  
 o  rs���� 0 duration  
 m  tw

 �

    m i n u t e s ! 

 ��
��
�� 
appr
 m  {~

 �

  B a c k e d   u p !��  
 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  ��  ��  �  �,  �+  ��  �   ��  ��  � m    

�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 


 l ����������  ��  ��  
 
 ��
  I  ����
!���� 0 	stopwatch  
! 
"��
" m  ��
#
# �
$
$  f i n i s h��  ��  ��  e 
%
&
% l     ��������  ��  ��  
& 
'
(
' l     ��������  ��  ��  
( 
)
*
) l     ��������  ��  ��  
* 
+
,
+ l     ��������  ��  ��  
, 
-
.
- l     ��������  ��  ��  
. 
/
0
/ l     ��
1
2��  
1 � �-----------------------------------------------------------------------------------------------------------------------------------------------   
2 �
3
3 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
0 
4
5
4 l     ��
6
7��  
6 � �-----------------------------------------------------------------------------------------------------------------------------------------------   
7 �
8
8 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
5 
9
:
9 l     ��
;
<��  
; � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   
< �
=
= - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
: 
>
?
> l     ��������  ��  ��  
? 
@
A
@ l     ��
B
C��  
B � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   
C �
D
D   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .
A 
E
F
E l     ��
G
H��  
G � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   
H �
I
I�   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .
F 
J
K
J i   { ~
L
M
L I      ��
N���� 0 itexists itExists
N 
O
P
O o      ���� 0 
objecttype 
objectType
P 
Q��
Q o      ���� 
0 object  ��  ��  
M l    W
R
S
T
R O     W
U
V
U Z    V
W
X
Y��
W l   
Z����
Z =    
[
\
[ o    ���� 0 
objecttype 
objectType
\ m    
]
] �
^
^  d i s k��  ��  
X Z   
 
_
`��
a
_ I  
 ��
b��
�� .coredoexnull���     ****
b 4   
 ��
c
�� 
cdis
c o    ���� 
0 object  ��  
` L    
d
d m    ��
�� boovtrue��  
a L    
e
e m    ��
�� boovfals
Y 
f
g
f l   "
h����
h =    "
i
j
i o     ���� 0 
objecttype 
objectType
j m     !
k
k �
l
l  f i l e��  ��  
g 
m
n
m Z   % 7
o
p��
q
o I  % -��
r��
�� .coredoexnull���     ****
r 4   % )��
s
�� 
file
s o   ' (���� 
0 object  ��  
p L   0 2
t
t m   0 1��
�� boovtrue��  
q L   5 7
u
u m   5 6��
�� boovfals
n 
v
w
v l  : =
x����
x =   : =
y
z
y o   : ;���� 0 
objecttype 
objectType
z m   ; <
{
{ �
|
|  f o l d e r��  ��  
w 
}��
} Z   @ R
~
��
�
~ I  @ H��
���
�� .coredoexnull���     ****
� 4   @ D��
�
�� 
cfol
� o   B C���� 
0 object  ��  
 L   K M
�
� m   K L��
�� boovtrue��  
� L   P R
�
� m   P Q��
�� boovfals��  ��  
V m     
�
��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  
S "  (string, string) as Boolean   
T �
�
� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n
K 
�
�
� l     ��������  ��  ��  
� 
�
�
� l     ��������  ��  ��  
� 
�
�
� l     ��������  ��  ��  
� 
�
�
� l     ��������  ��  ��  
� 
�
�
� l     ��������  ��  ��  
� 
�
�
� l     ��
�
���  
� � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   
� �
�
�Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .
� 
�
�
� l     ��
�
���  
� P J This means that backups can be identified from what machine they're from.   
� �
�
� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .
� 
�
�
� i    �
�
�
� I      �������� .0 getcomputeridentifier getComputerIdentifier��  ��  
� k     
�
� 
�
�
� r     	
�
�
� n     
�
�
� 1    ��
�� 
sicn
� l    
�����
� I    ������
�� .sysosigtsirr   ��� null��  ��  ��  ��  
� o      ���� 0 computername computerName
� 
�
�
� r   
 
�
�
� I  
 �
��~
� .sysoexecTEXT���     TEXT
� m   
 
�
� �
�
� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �~  
� o      �}�} "0 strserialnumber strSerialNumber
� 
�
�
� r    
�
�
� l   
��|�{
� b    
�
�
� b    
�
�
� o    �z�z 0 computername computerName
� m    
�
� �
�
�    -  
� o    �y�y "0 strserialnumber strSerialNumber�|  �{  
� o      �x�x  0 identifiername identifierName
� 
��w
� L    
�
� o    �v�v  0 identifiername identifierName�w  
� 
�
�
� l     �u�t�s�u  �t  �s  
� 
�
�
� l     �r�q�p�r  �q  �p  
� 
�
�
� l     �o�n�m�o  �n  �m  
� 
�
�
� l     �l�k�j�l  �k  �j  
� 
�
�
� l     �i�h�g�i  �h  �g  
� 
�
�
� l     �f
�
��f  
� � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   
� �
�
�   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .
� 
�
�
� l     �e
�
��e  
� � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   
� �
�
�   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .
� 
�
�
� i   � �
�
�
� I      �d
��c�d 0 gettimestamp getTimestamp
� 
��b
� o      �a�a 0 isfolder isFolder�b  �c  
� l   �
�
�
�
� k    �
�
� 
�
�
� l     �`
�
��`  
�   Date variables   
� �
�
�    D a t e   v a r i a b l e s
� 
�
�
� r     )
�
�
� l     
��_�^
� I     �]�\�[
�] .misccurdldt    ��� null�\  �[  �_  �^  
� K    
�
� �Z
�
�
�Z 
year
� o    �Y�Y 0 y  
� �X
�
�
�X 
mnth
� o    �W�W 0 m  
� �V
�
�
�V 
day 
� o    �U�U 0 d  
� �T
��S
�T 
time
� o   	 
�R�R 0 t  �S  
� 
�
�
� r   * 1
�
�
� c   * /
�
�
� l  * -
��Q�P
� c   * -
�
�
� o   * +�O�O 0 y  
� m   + ,�N
�N 
long�Q  �P  
� m   - .�M
�M 
TEXT
� o      �L�L 0 ty tY
� 
�
�
� r   2 9
�
�
� c   2 7
�
�
� l  2 5
��K�J
� c   2 5
�
�
� o   2 3�I�I 0 y  
� m   3 4�H
�H 
long�K  �J  
� m   5 6�G
�G 
TEXT
� o      �F�F 0 ty tY
� 
�
�
� r   : A
� 
� c   : ? l  : =�E�D c   : = o   : ;�C�C 0 m   m   ; <�B
�B 
long�E  �D   m   = >�A
�A 
TEXT  o      �@�@ 0 tm tM
�  r   B I	 c   B G

 l  B E�?�> c   B E o   B C�=�= 0 d   m   C D�<
�< 
long�?  �>   m   E F�;
�; 
TEXT	 o      �:�: 0 td tD  r   J Q c   J O l  J M�9�8 c   J M o   J K�7�7 0 t   m   K L�6
�6 
long�9  �8   m   M N�5
�5 
TEXT o      �4�4 0 tt tT  l  R R�3�2�1�3  �2  �1    l  R R�0�0   U O Append the month or day with a 0 if the string length is only 1 character long    � �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g   r   R [!"! c   R Y#$# l  R W%�/�.% I  R W�-&�,
�- .corecnte****       ****& o   R S�+�+ 0 tm tM�,  �/  �.  $ m   W X�*
�* 
nmbr" o      �)�) 
0 tml tML  '(' r   \ e)*) c   \ c+,+ l  \ a-�(�'- I  \ a�&.�%
�& .corecnte****       ****. o   \ ]�$�$ 0 tm tM�%  �(  �'  , m   a b�#
�# 
nmbr* o      �"�" 
0 tdl tDL( /0/ l  f f�!� ��!  �   �  0 121 Z   f u34��3 l  f i5��5 =   f i676 o   f g�� 
0 tml tML7 m   g h�� �  �  4 r   l q898 b   l o:;: m   l m<< �==  0; o   m n�� 0 tm tM9 o      �� 0 tm tM�  �  2 >?> l  v v����  �  �  ? @A@ Z   v �BC��B l  v yD��D =   v yEFE o   v w�� 
0 tdl tDLF m   w x�� �  �  C r   | �GHG b   | �IJI m   | KK �LL  0J o    ��� 0 td tDH o      �� 0 td tD�  �  A MNM l  � ���
�	�  �
  �	  N OPO l  � �����  �  �  P QRQ l  � ��ST�  S   Time variables	   T �UU     T i m e   v a r i a b l e s 	R VWV l  � ��XY�  X  	 Get hour   Y �ZZ    G e t   h o u rW [\[ r   � �]^] n   � �_`_ 1   � ��
� 
tstr` l  � �a��a I  � �� ����
�  .misccurdldt    ��� null��  ��  �  �  ^ o      ���� 0 timestr timeStr\ bcb r   � �ded I  � �f��gf z����
�� .sysooffslong    ��� null
�� misccura��  g ��hi
�� 
psofh m   � �jj �kk  :i ��l��
�� 
psinl o   � ����� 0 timestr timeStr��  e o      ���� 0 pos  c mnm r   � �opo c   � �qrq n   � �sts 7  � ���uv
�� 
cha u m   � ����� v l  � �w����w \   � �xyx o   � ����� 0 pos  y m   � ����� ��  ��  t o   � ����� 0 timestr timeStrr m   � ���
�� 
TEXTp o      ���� 0 h  n z{z r   � �|}| c   � �~~ n   � ���� 7 � �����
�� 
cha � l  � ������� [   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  �  ;   � �� o   � ����� 0 timestr timeStr m   � ���
�� 
TEXT} o      ���� 0 timestr timeStr{ ��� l  � ���������  ��  ��  � ��� l  � �������  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   ���� c   ���� n   � ��� 7  � ����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   ��
�� 
TEXT� o      ���� 0 m  � ��� r  ��� c  ��� n  ��� 7����
�� 
cha � l ������ [  ��� o  ���� 0 pos  � m  ���� ��  ��  �  ;  � o  ���� 0 timestr timeStr� m  ��
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l ��������  ��  ��  � ��� l ������  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  2��� I 0����� z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m  "%�� ���   � �����
�� 
psin� o  ()���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  3E��� c  3C��� n  3A��� 74A����
�� 
cha � l :>������ [  :>��� o  ;<���� 0 pos  � m  <=���� ��  ��  �  ;  ?@� o  34���� 0 timestr timeStr� m  AB��
�� 
TEXT� o      ���� 0 s  � ��� l FF��������  ��  ��  � ��� l FF��������  ��  ��  � ��� Z  F������ l FI������ =  FI��� o  FG���� 0 isfolder isFolder� m  GH��
�� boovtrue��  ��  � k  La�� ��� l LL������  �   Create folder timestamp   � ��� 0   C r e a t e   f o l d e r   t i m e s t a m p� ���� r  La��� b  L_��� b  L]��� b  L[��� b  LY��� b  LU��� b  LS��� b  LQ��� m  LO�� ���  b a c k u p _� o  OP���� 0 ty tY� o  QR���� 0 tm tM� o  ST���� 0 td tD� m  UX�� ���  _� o  YZ���� 0 h  � o  [\���� 0 m  � o  ]^���� 0 s  � o      ���� 0 	timestamp  ��  � ��� l dg������ =  dg��� o  de���� 0 isfolder isFolder� m  ef��
�� boovfals��  ��  � ���� k  j{��    l jj����     Create timestamp    � "   C r e a t e   t i m e s t a m p �� r  j{ b  jy	 b  jw

 b  ju b  js b  jo b  jm o  jk���� 0 ty tY o  kl���� 0 tm tM o  mn���� 0 td tD m  or �  _ o  st���� 0 h   o  uv���� 0 m  	 o  wx���� 0 s   o      ���� 0 	timestamp  ��  ��  ��  �  l ����������  ��  ��   �� L  �� o  ������ 0 	timestamp  ��  
�  	(boolean)   
� �  ( b o o l e a n )
�  l     ��������  ��  ��    l     ��������  ��  ��     l     ��������  ��  ��    !"! l     ��������  ��  ��  " #$# l     ��~�}�  �~  �}  $ %&% l     �|'(�|  ' q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   ( �)) �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .& *+* l     �{,-�{  , w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   - �.. �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .+ /0/ i   � �121 I      �z3�y�z 0 comparesizes compareSizes3 454 o      �x�x 
0 source  5 6�w6 o      �v�v 0 destination  �w  �y  2 l    j7897 k     j:: ;<; l     �u=>�u  = # display dialog "COMPARESIZES"   > �?? : d i s p l a y   d i a l o g   " C O M P A R E S I Z E S "< @A@ r     BCB m     �t
�t boovtrueC o      �s�s 0 fit  A DED r    
FGF 4    �rH
�r 
psxfH o    �q�q 0 destination  G o      �p�p 0 destination  E IJI l   �o�n�m�o  �n  �m  J KLK O    XMNM k    WOO PQP r    RSR I   �lT�k
�l .sysoexecTEXT���     TEXTT b    UVU b    WXW m    YY �ZZ  d u   - m s  X o    �j�j 
0 source  V m    [[ �\\    |   c u t   - f   1�k  S o      �i�i 0 
foldersize 
folderSizeQ ]^] r    -_`_ ^    +aba l   )c�h�gc I   )de�fd z�e�d
�e .sysorondlong        doub
�d misccurae ]    $fgf l   "h�c�bh ^    "iji o     �a�a 0 
foldersize 
folderSizej m     !�`�` �c  �b  g m   " #�_�_ d�f  �h  �g  b m   ) *�^�^ d` o      �]�] 0 
foldersize 
folderSize^ klk l  . .�\mn�\  m ) #display dialog folderSize as string   n �oo F d i s p l a y   d i a l o g   f o l d e r S i z e   a s   s t r i n gl pqp l  . .�[�Z�Y�[  �Z  �Y  q rsr r   . <tut ^   . :vwv ^   . 8xyx ^   . 6z{z l  . 4|�X�W| l  . 4}�V�U} n   . 4~~ 1   2 4�T
�T 
frsp 4   . 2�S�
�S 
cdis� o   0 1�R�R 0 destination  �V  �U  �X  �W  { m   4 5�Q�Q y m   6 7�P�P w m   8 9�O�O u o      �N�N 0 	freespace 	freeSpaces ��� r   = M��� ^   = K��� l  = I��M�L� I  = I���K� z�J�I
�J .sysorondlong        doub
�I misccura� l  A D��H�G� ]   A D��� o   A B�F�F 0 	freespace 	freeSpace� m   B C�E�E d�H  �G  �K  �M  �L  � m   I J�D�D d� o      �C�C 0 	freespace 	freeSpace� ��� l  N N�B���B  � ( "display dialog freeSpace as string   � ��� D d i s p l a y   d i a l o g   f r e e S p a c e   a s   s t r i n g� ��� l  N N�A�@�?�A  �@  �?  � ��� I  N U�>��=
�> .ascrcmnt****      � ****� l  N Q��<�;� b   N Q��� o   N O�:�: 0 
foldersize 
folderSize� o   O P�9�9 0 	freespace 	freeSpace�<  �;  �=  � ��8� l  V V�7���7  � K Edisplay dialog ((folderSize) as string) & " " & (freeSpace) as string   � ��� � d i s p l a y   d i a l o g   ( ( f o l d e r S i z e )   a s   s t r i n g )   &   "   "   &   ( f r e e S p a c e )   a s   s t r i n g�8  N m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  L ��� l  Y Y�6�5�4�6  �5  �4  � ��� Z   Y g���3�2� H   Y ]�� l  Y \��1�0� A   Y \��� o   Y Z�/�/ 0 
foldersize 
folderSize� o   Z [�.�. 0 	freespace 	freeSpace�1  �0  � r   ` c��� m   ` a�-
�- boovfals� o      �,�, 0 fit  �3  �2  � ��� l  h h�+�*�)�+  �*  �)  � ��� l  h h�(���(  � " display dialog "FIT: " & fit   � ��� 8 d i s p l a y   d i a l o g   " F I T :   "   &   f i t� ��� l  h h�'�&�%�'  �&  �%  � ��$� L   h j�� o   h i�#�# 0 fit  �$  8  (string, string)   9 ���   ( s t r i n g ,   s t r i n g )0 ��� l     �"�!� �"  �!  �   � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i   � ���� I      ���� 0 	stopwatch  � ��� o      �� 0 mode  �  �  � l    5���� k     5�� ��� q      �� ��� 0 x  �  � ��� Z     3����� l    ���
� =     ��� o     �	�	 0 mode  � m    �� ��� 
 s t a r t�  �
  � k    �� ��� r    ��� I    ���� 0 gettimestamp getTimestamp� ��� m    �
� boovfals�  �  � o      �� 0 x  � ��� I   ���
� .ascrcmnt****      � ****� l   �� ��� b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    ���� 0 x  �   ��  �  �  � ��� l   ������ =    ��� o    ���� 0 mode  � m    �� ���  f i n i s h��  ��  � ���� k    /�� ��� r    '��� I    %������� 0 gettimestamp getTimestamp� ���� m     !��
�� boovfals��  ��  � o      ���� 0 x  � ���� I  ( /�����
�� .ascrcmnt****      � ****� l  ( +������ b   ( +��� m   ( )�� �   " b a c k u p   f i n i s h e d :  � o   ) *���� 0 x  ��  ��  ��  ��  ��  �  � �� l  4 4��������  ��  ��  ��  �  (string)   � �  ( s t r i n g )�  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��   	
	 l     ��������  ��  ��  
  l     ��������  ��  ��    l     ����   M G Utility function to get the duration of the backup process in minutes.    � �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .  i   � � I      �������� 0 getduration getDuration��  ��   k       r      ^      l    ���� \      o     ���� 0 endtime endTime o    ���� 0 	starttime 	startTime��  ��   m    ���� < o      ���� 0 duration    !  r    "#" ^    $%$ l   &����& I   '(��' z����
�� .sysorondlong        doub
�� misccura( l   )����) ]    *+* o    ���� 0 duration  + m    ���� d��  ��  ��  ��  ��  % m    ���� d# o      ���� 0 duration  ! ,��, L    -- o    ���� 0 duration  ��   ./. l     ��������  ��  ��  / 010 l     ��������  ��  ��  1 232 l     ��������  ��  ��  3 454 l     ��������  ��  ��  5 676 l     ��������  ��  ��  7 898 l     ��:;��  : ; 5 Utility function bring WBTFU to the front with focus   ; �<< j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s9 =>= i   � �?@? I      �������� 0 setfocus setFocus��  ��  @ O     
ABA I   	������
�� .miscactvnull��� ��� null��  ��  B m     CC                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  > DED l     ��������  ��  ��  E FGF l     ��HI��  H � �-----------------------------------------------------------------------------------------------------------------------------------------------   I �JJ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -G KLK l     ��MN��  M � �-----------------------------------------------------------------------------------------------------------------------------------------------   N �OO - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -L PQP l     ��������  ��  ��  Q RSR l     ��������  ��  ��  S TUT l     ��������  ��  ��  U VWV l     ��������  ��  ��  W XYX l     ��������  ��  ��  Y Z[Z l     ��\]��  \ � �-----------------------------------------------------------------------------------------------------------------------------------------------   ] �^^ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -[ _`_ l     ��ab��  a � �-----------------------------------------------------------------------------------------------------------------------------------------------   b �cc - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -` ded l     ��fg��  f � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   g �hh - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -e iji l     ��������  ��  ��  j klk i   � �mnm I      ��o���� $0 menuneedsupdate_ menuNeedsUpdate_o p��p l     q����q m      ��
�� 
cmnu��  ��  ��  ��  n k     rr sts l     ��uv��  u J D NSMenu's delegates method, when the menu is clicked this is called.   v �ww �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .t xyx l     ��z{��  z j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   { �|| �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .y }~} l     �����   < 6 This means the menu items can be changed dynamically.   � ��� l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .~ ���� n    ��� I    �������� 0 	makemenus 	makeMenus��  ��  �  f     ��  l ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     �������  ��  �  � ��� l     �~�}�|�~  �}  �|  � ��� l     �{�z�y�{  �z  �y  � ��� i   � ���� I      �x�w�v�x 0 	makemenus 	makeMenus�w  �v  � k     ��� ��� l    	���� n    	��� I    	�u�t�s�u  0 removeallitems removeAllItems�t  �s  � o     �r�r 0 newmenu newMenu� !  remove existing menu items   � ��� 6   r e m o v e   e x i s t i n g   m e n u   i t e m s� ��� l  
 
�q�p�o�q  �p  �o  � ��n� Y   
 ���m���l� k    ��� ��� r    '��� n    %��� 4   " %�k�
�k 
cobj� o   # $�j�j 0 i  � o    "�i�i 0 thelist theList� o      �h�h 0 	this_item  � ��� l  ( (�g�f�e�g  �f  �e  � ��� Z   ( �����d� l  ( -��c�b� =   ( -��� l  ( +��a�`� c   ( +��� o   ( )�_�_ 0 	this_item  � m   ) *�^
�^ 
TEXT�a  �`  � m   + ,�� ���  B a c k u p   n o w�c  �b  � r   0 @��� l  0 >��]�\� n  0 >��� I   7 >�[��Z�[ J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8�Y�Y 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ��X� m   9 :�� ���  �X  �Z  � n  0 7��� I   3 7�W�V�U�W 	0 alloc  �V  �U  � n  0 3��� o   1 3�T�T 0 
nsmenuitem 
NSMenuItem� m   0 1�S
�S misccura�]  �\  � o      �R�R 0 thismenuitem thisMenuItem� ��� l  C H��Q�P� =   C H��� l  C F��O�N� c   C F��� o   C D�M�M 0 	this_item  � m   D E�L
�L 
TEXT�O  �N  � m   F G�� ��� " T u r n   o n   t h e   b a n t s�Q  �P  � ��� r   K [��� l  K Y��K�J� n  K Y��� I   R Y�I��H�I J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S�G�G 0 	this_item  � ��� m   S T�� ���  n s f w O n H a n d l e r :� ��F� m   T U�� ���  �F  �H  � n  K R��� I   N R�E�D�C�E 	0 alloc  �D  �C  � n  K N��� o   L N�B�B 0 
nsmenuitem 
NSMenuItem� m   K L�A
�A misccura�K  �J  � o      �@�@ 0 thismenuitem thisMenuItem� ��� l  ^ c��?�>� =   ^ c��� l  ^ a��=�<� c   ^ a��� o   ^ _�;�; 0 	this_item  � m   _ `�:
�: 
TEXT�=  �<  � m   a b�� ��� $ T u r n   o f f   t h e   b a n t s�?  �>  � ��� r   f x��� l  f v��9�8� n  f v��� I   m v�7��6�7 J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n�5�5 0 	this_item  � � � m   n o �  n s f w O f f H a n d l e r :  �4 m   o r �  �4  �6  � n  f m I   i m�3�2�1�3 	0 alloc  �2  �1   n  f i	 o   g i�0�0 0 
nsmenuitem 
NSMenuItem	 m   f g�/
�/ misccura�9  �8  � o      �.�. 0 thismenuitem thisMenuItem� 

 l  { ��-�, =   { � l  { ~�+�* c   { ~ o   { |�)�) 0 	this_item   m   | }�(
�( 
TEXT�+  �*   m   ~ � �  Q u i t�-  �,   �' r   � � l  � ��&�% n  � � I   � ��$�#�$ J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   � ��"�" 0 	this_item    m   � � �    q u i t H a n d l e r : !�!! m   � �"" �##  �!  �#   n  � �$%$ I   � �� ���  	0 alloc  �  �  % n  � �&'& o   � ��� 0 
nsmenuitem 
NSMenuItem' m   � ��
� misccura�&  �%   o      �� 0 thismenuitem thisMenuItem�'  �d  � ()( l  � �����  �  �  ) *+* l  � �,��, n  � �-.- I   � ��/�� 0 additem_ addItem_/ 0�0 o   � ��� 0 thismenuitem thisMenuItem�  �  . o   � ��� 0 newmenu newMenu�  �  + 121 l  � �����  �  �  2 3�3 l  � �4564 l  � �7��7 n  � �898 I   � ��
:�	�
 0 
settarget_ 
setTarget_: ;�;  f   � ��  �	  9 o   � ��� 0 thismenuitem thisMenuItem�  �  5 * $ required for enabling the menu item   6 �<< H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m�  �m 0 i  � m    �� � n    =>= m    �
� 
nmbr> n   ?@? 2   �
� 
cobj@ o    �� 0 thelist theList�l  �n  � ABA l     ��� �  �  �   B CDC l     ��������  ��  ��  D EFE l     ��������  ��  ��  F GHG l     ��������  ��  ��  H IJI l     ��������  ��  ��  J KLK i   � �MNM I      ��O����  0 backuphandler_ backupHandler_O P��P o      ���� 
0 sender  ��  ��  N Z     }QRS��Q l    T����T =     UVU o     ���� 0 isbackingup isBackingUpV m    ��
�� boovfals��  ��  R k    AWW XYX l   ��Z[��  Z % display dialog "BACKUP HANDLER"   [ �\\ > d i s p l a y   d i a l o g   " B A C K U P   H A N D L E R "Y ]^] r    	_`_ m    ��
�� boovtrue` o      ���� 0 manualbackup manualBackup^ aba Z   
 ?cde��c l  
 f����f =   
 ghg o   
 ���� 0 nsfw  h m    ��
�� boovtrue��  ��  d k    !ii jkj I   ��lm
�� .sysonotfnull��� ��� TEXTl m    nn �oo T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u pm ��p��
�� 
apprp m    qq �rr 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  k s��s I   !��t��
�� .sysodelanull��� ��� nmbrt m    ���� ��  ��  e uvu l  $ +w����w =   $ +xyx o   $ )���� 0 nsfw  y m   ) *��
�� boovfals��  ��  v z��z k   . ;{{ |}| I  . 5��~
�� .sysonotfnull��� ��� TEXT~ m   . /�� ���   P r e p a r i n g   b a c k u p �����
�� 
appr� m   0 1�� ��� 
 W B T F U��  } ���� I  6 ;�����
�� .sysodelanull��� ��� nmbr� m   6 7���� ��  ��  ��  ��  b ���� l  @ @������  � ! display dialog manualBackup   � ��� 6 d i s p l a y   d i a l o g   m a n u a l B a c k u p��  S ��� l  D G������ =   D G��� o   D E���� 0 isbackingup isBackingUp� m   E F��
�� boovtrue��  ��  � ���� k   J y�� ��� r   J O��� n   J M��� 3   K M��
�� 
cobj� o   J K���� 40 messagesalreadybackingup messagesAlreadyBackingUp� o      ���� 0 msg  � ���� Z   P y������ l  P W������ =   P W��� o   P U���� 0 nsfw  � m   U V��
�� boovtrue��  ��  � I  Z a����
�� .sysonotfnull��� ��� TEXT� o   Z [���� 0 msg  � �����
�� 
appr� m   \ ]�� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p��  � ��� l  d k������ =   d k��� o   d i���� 0 nsfw  � m   i j��
�� boovfals��  ��  � ���� I  n u����
�� .sysonotfnull��� ��� TEXT� m   n o�� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �����
�� 
appr� m   p q�� ��� 
 W B T F U��  ��  ��  ��  ��  ��  L ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������  0 nsfwonhandler_ nsfwOnHandler_� ���� o      ���� 
0 sender  ��  ��  � k     �� ��� r     ��� m     ��
�� boovtrue� o      ���� 0 nsfw  � ��� r    ��� J    �� ��� m    	�� ���  B a c k u p   n o w� ��� m   	 
�� ��� $ T u r n   o f f   t h e   b a n t s� ���� m   
 �� ���  Q u i t��  � o      ���� 0 thelist theList� ���� l   ������  � ) #set a to {choose from list theList}   � ��� F s e t   a   t o   { c h o o s e   f r o m   l i s t   t h e L i s t }��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� "0 nsfwoffhandler_ nsfwOffHandler_� ���� o      ���� 
0 sender  ��  ��  � k     �� ��� r     ��� m     ��
�� boovfals� o      ���� 0 nsfw  � ��� r    ��� J    �� ��� m    	�� ���  B a c k u p   n o w� ��� m   	 
�� ��� " T u r n   o n   t h e   b a n t s� ���� m   
 �� ���  Q u i t��  � o      ���� 0 thelist theList� ���� l   ������  � ) #set a to {choose from list theList}   � ��� F s e t   a   t o   { c h o o s e   f r o m   l i s t   t h e L i s t }��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � � � l     ��������  ��  ��     l     ��������  ��  ��    i   � � I      ������ 0 quithandler_ quitHandler_ � o      �~�~ 
0 sender  �  ��   Z     o	
�}	 l    �|�{ =      o     �z�z 0 isbackingup isBackingUp m    �y
�y boovtrue�|  �{  
 k    0  I    �x�w�v�x 0 setfocus setFocus�w  �v    r     l   �u�t n     1    �s
�s 
bhit l   �r�q I   �p
�p .sysodlogaskr        TEXT m     � � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ? �o
�o 
btns J       !"! m    ## �$$  C a n c e l   &   Q u i t" %�n% m    && �''  R e s u m e�n   �m(�l
�m 
dflt( m    �k�k �l  �r  �q  �u  �t   o      �j�j 	0 reply   )�i) Z    0*+�h,* l    -�g�f- =     ./. o    �e�e 	0 reply  / m    00 �11  C a n c e l   &   Q u i t�g  �f  + I  # (�d2�c
�d .aevtquitnull��� ��� null2 m   # $33                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �c  �h  , I  + 0�b4�a
�b .ascrcmnt****      � ****4 l  + ,5�`�_5 m   + ,66 �77  W B T F U   r e s u m e d�`  �_  �a  �i   898 l  3 6:�^�]: =   3 6;<; o   3 4�\�\ 0 isbackingup isBackingUp< m   4 5�[
�[ boovfals�^  �]  9 =�Z= k   9 k>> ?@? I   9 >�Y�X�W�Y 0 setfocus setFocus�X  �W  @ ABA r   ? SCDC l  ? QE�V�UE n   ? QFGF 1   O Q�T
�T 
bhitG l  ? OH�S�RH I  ? O�QIJ
�Q .sysodlogaskr        TEXTI m   ? @KK �LL < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?J �PMN
�P 
btnsM J   A IOO PQP m   A DRR �SS  Q u i tQ T�OT m   D GUU �VV  R e s u m e�O  N �NW�M
�N 
dfltW m   J K�L�L �M  �S  �R  �V  �U  D o      �K�K 	0 reply  B X�JX Z   T kYZ�I[Y l  T Y\�H�G\ =   T Y]^] o   T U�F�F 	0 reply  ^ m   U X__ �``  Q u i t�H  �G  Z I  \ a�Ea�D
�E .aevtquitnull��� ��� nulla m   \ ]bb                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  �D  �I  [ I  d k�Cc�B
�C .ascrcmnt****      � ****c l  d gd�A�@d m   d gee �ff  W B T F U   r e s u m e d�A  �@  �B  �J  �Z  �}   ghg l     �?�>�=�?  �>  �=  h iji l     �<�;�:�<  �;  �:  j klk l     �9�8�7�9  �8  �7  l mnm l     �6�5�4�6  �5  �4  n opo l     �3�2�1�3  �2  �1  p qrq l     �0st�0  s   create an NSStatusBar   t �uu ,   c r e a t e   a n   N S S t a t u s B a rr vwv i   � �xyx I      �/�.�-�/ 0 makestatusbar makeStatusBar�.  �-  y k     rzz {|{ l     �,}~�,  }  log ("Make status bar")   ~ � . l o g   ( " M a k e   s t a t u s   b a r " )| ��� r     ��� n    ��� o    �+�+ "0 systemstatusbar systemStatusBar� n    ��� o    �*�* 0 nsstatusbar NSStatusBar� m     �)
�) misccura� o      �(�( 0 bar  � ��� l   �'�&�%�'  �&  �%  � ��� r    ��� n   ��� I   	 �$��#�$ .0 statusitemwithlength_ statusItemWithLength_� ��"� m   	 
�� ��      �"  �#  � o    	�!�! 0 bar  � o      � �  0 
statusitem 
StatusItem� ��� l   ����  �  �  � ��� r    #��� 4    !��
� 
alis� l    ���� b     ��� l   ���� I   ���
� .earsffdralis        afdr�  f    � ���
� 
rtyp� m    �
� 
ctxt�  �  �  � m    �� ��� < C o n t e n t s : R e s o u r c e s : m e n u b a r . p n g�  �  � o      �� 0 	imagepath 	imagePath� ��� r   $ )��� n   $ '��� 1   % '�
� 
psxp� o   $ %�� 0 	imagepath 	imagePath� o      �� 0 	imagepath 	imagePath� ��� r   * 8��� n  * 6��� I   1 6���� 20 initwithcontentsoffile_ initWithContentsOfFile_� ��� o   1 2�� 0 	imagepath 	imagePath�  �  � n  * 1��� I   - 1��
�	� 	0 alloc  �
  �	  � n  * -��� o   + -�� 0 nsimage NSImage� m   * +�
� misccura� o      �� 	0 image  � ��� l  9 9����  �  �  � ��� l  9 9����  � � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   � ��� s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "� ��� l  9 9�� ���  �   ��  � ��� n  9 C��� I   > C������� 0 	setimage_ 	setImage_� ���� o   > ?���� 	0 image  ��  ��  � o   9 >���� 0 
statusitem 
StatusItem� ��� l  D D��������  ��  ��  � ��� l  D D������  � , & set up the initial NSStatusBars title   � ��� L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e� ��� l  D D������  � # StatusItem's setTitle:"WBTFU"   � ��� : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "� ��� l  D D��������  ��  ��  � ��� l  D D������  � 1 + set up the initial NSMenu of the statusbar   � ��� V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r� ��� r   D X��� n  D R��� I   K R�������  0 initwithtitle_ initWithTitle_� ���� m   K N�� ���  C u s t o m��  ��  � n  D K��� I   G K�������� 	0 alloc  ��  ��  � n  D G��� o   E G���� 0 nsmenu NSMenu� m   D E��
�� misccura� o      ���� 0 newmenu newMenu� ��� l  Y Y��������  ��  ��  � ��� n  Y c��� I   ^ c������� 0 setdelegate_ setDelegate_� ����  f   ^ _��  ��  � o   Y ^���� 0 newmenu newMenu� ��� l  d d������  � � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   � ���0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .� ��� l  d d��������  ��  ��  � ���� n  d r��� I   i r������� 0 setmenu_ setMenu_� ���� o   i n���� 0 newmenu newMenu��  ��  � o   d i���� 0 
statusitem 
StatusItem��  w ��� l     ��������  ��  ��  � ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -�    l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��������  ��  ��    l     ��������  ��  ��   	
	 l     ��������  ��  ��  
  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    i   � � I      �������� 0 runonce runOnce��  ��   k       Z     ���� H     
 c     	 l    ���� n     I    �������� 0 ismainthread isMainThread��  ��   n     !  o    ���� 0 nsthread NSThread! m     ��
�� misccura��  ��   m    ��
�� 
bool k    "" #$# l   ��%&��  % b \display alert "This script must be run from the main thread." buttons {"Cancel"} as critical   & �'' � d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l$ (��( l   ��)*��  )  error number -128   * �++ " e r r o r   n u m b e r   - 1 2 8��  ��  ��   ,-, l   ��������  ��  ��  - .��. I    �������� 0 	plistinit 	plistInit��  ��  ��   /0/ l     ��������  ��  ��  0 121 l  � �3����3 n  � �454 I   � ��������� 0 makestatusbar makeStatusBar��  ��  5  f   � ���  ��  2 676 l  � �8����8 I   � ��������� 0 runonce runOnce��  ��  ��  ��  7 9:9 l     ��������  ��  ��  : ;<; l     ��������  ��  ��  < =>= l     ��������  ��  ��  > ?@? l     ��������  ��  ��  @ ABA l     ��������  ��  ��  B CDC l     ��EF��  E w q Function that is always running in the background. This doesn't need to get called as it is running from the off   F �GG �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f fD HIH l     ��JK��  J � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   K �LL�   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .I M��M i   � �NON I     ������
�� .miscidlenmbr    ��� null��  ��  O k     �PP QRQ Z     }ST����S l    U����U E     VWV o     ���� &0 displaysleepstate displaySleepStateW o    ���� 0 strawake strAwake��  ��  T Z    yXY���X l   	Z�~�}Z =    	[\[ o    �|�| 0 isbackingup isBackingUp\ m    �{
�{ boovfals�~  �}  Y Z    u]^_�z] l   `�y�x` =    aba o    �w�w 0 manualbackup manualBackupb m    �v
�v boovfals�y  �x  ^ Z    ccd�u�tc l   e�s�re A    fgf l   h�q�ph n    iji 1    �o
�o 
hourj l   k�n�mk I   �l�k�j
�l .misccurdldt    ��� null�k  �j  �n  �m  �q  �p  g m    �i�i �s  �r  d k    _ll mnm r    'opo l   %q�h�gq n    %rsr 1   # %�f
�f 
min s l   #t�e�dt l   #u�c�bu I   #�a�`�_
�a .misccurdldt    ��� null�`  �_  �c  �b  �e  �d  �h  �g  p o      �^�^ 0 m  n vwv l  ( (�]�\�[�]  �\  �[  w x�Zx Z   ( _yz{�Yy G   ( 3|}| l  ( +~�X�W~ =   ( +� o   ( )�V�V 0 m  � m   ) *�U�U �X  �W  } l  . 1��T�S� =   . 1��� o   . /�R�R 0 m  � m   / 0�Q�Q -�T  �S  z Z   6 E���P�O� l  6 9��N�M� =   6 9��� o   6 7�L�L 0 scheduledtime scheduledTime� m   7 8�K�K �N  �M  � I   < A�J�I�H�J 0 	plistinit 	plistInit�I  �H  �P  �O  { ��� G   H S��� l  H K��G�F� =   H K��� o   H I�E�E 0 m  � m   I J�D�D  �G  �F  � l  N Q��C�B� =   N Q��� o   N O�A�A 0 m  � m   O P�@�@ �C  �B  � ��?� I   V [�>�=�<�> 0 	plistinit 	plistInit�=  �<  �?  �Y  �Z  �u  �t  _ ��� l  f i��;�:� =   f i��� o   f g�9�9 0 manualbackup manualBackup� m   g h�8
�8 boovtrue�;  �:  � ��7� I   l q�6�5�4�6 0 	plistinit 	plistInit�5  �4  �7  �z  ��  �  ��  ��  R ��� l  ~ ~�3�2�1�3  �2  �1  � ��0� L   ~ ��� m   ~ �/�/ <�0  ��       "�.���- g������,������������������������.  �  �+�*�)�(�'�&�%�$�#�"�!� ��������������������
�+ 
pimr�* 0 
statusitem 
StatusItem�) 0 
thedisplay 
theDisplay�( 0 defaults  �' $0 internalmenuitem internalMenuItem�& $0 externalmenuitem externalMenuItem�% 0 newmenu newMenu�$ 0 thelist theList�# 0 nsfw  �" 0 	plistinit 	plistInit�! 0 diskinit diskInit�  0 freespaceinit freeSpaceInit� 0 
backupinit 
backupInit� 0 tidyup tidyUp� 
0 backup  � 0 itexists itExists� .0 getcomputeridentifier getComputerIdentifier� 0 gettimestamp getTimestamp� 0 comparesizes compareSizes� 0 	stopwatch  � 0 getduration getDuration� 0 setfocus setFocus� $0 menuneedsupdate_ menuNeedsUpdate_� 0 	makemenus 	makeMenus�  0 backuphandler_ backupHandler_�  0 nsfwonhandler_ nsfwOnHandler_� "0 nsfwoffhandler_ nsfwOffHandler_� 0 quithandler_ quitHandler_� 0 makestatusbar makeStatusBar� 0 runonce runOnce
� .miscidlenmbr    ��� null
� .aevtoappnull  �   � ****� ��� �  ����� �
 L�	
�
 
vers�	  � ���
� 
cobj� ��   �
� 
osax�  � ���
� 
cobj� ��   � U
� 
frmk�  � ���
� 
cobj� ��   �  [
�  
frmk�  
�- 
msng� �� �����
�� misccura
�� 
pcls� ���  N S U s e r D e f a u l t s� �� �����
�� misccura
�� 
pcls� ���  N S M e n u I t e m� �� �����
�� misccura
�� 
pcls� ���  N S M e n u I t e m� �� �����
�� misccura
�� 
pcls� ���  N S M e n u� ����� �   � � �
�, boovfals� ������������� 0 	plistinit 	plistInit��  ��  � ������������������ 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk��  0 computerfolder computerFolder�� 0 
backuptime 
backupTime�� 0 thecontents theContents�� 0 thevalue theValue�� 0 backuptimes backupTimes�� 0 selectedtime selectedTime� 2����D�����������������������^��i���������������������������������������������������� 	0 plist  �� 0 itexists itExists
�� 
plif
�� 
pcnt
�� 
valL��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive��  0 computerfolder computerFolder�� 0 scheduledtime scheduledTime�� .0 getcomputeridentifier getComputerIdentifier��  ���� 0 setfocus setFocus
�� 
prmp
�� .sysostflalis    ��� null
�� 
TEXT
�� 
psxp
�� .gtqpchltns    @   @ ns  �� �� �� <
�� 
kocl
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null
�� 
plii
�� 
insh
�� 
kind�� �� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 machinefolder machineFolder
�� .ascrcmnt****      � ****�� 0 diskinit diskInit���jE�O*��l+ e  2� **��/�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYT*j+ E�O� ��n*j+ O*�a l E�O*�a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a  �a !a "�la # $ �*a  a %a &*6a !a 'a a "a (�a )a ) $O*a  a %a &*6a !a 'a a "a *�a )a ) $O*a  a %a &*6a !a 'a a "a +�a )a ) $O*a  a %a &*6a !a 'a a "a ,�a )a ) $UUO�E` -O�E` .O�E` /O�E�O_ -_ ._ /�a #vj 0O*j+ 1� ��M���������� 0 diskinit diskInit��  ��  � ������ 0 msg  �� 	0 reply  � ^��������������{~�������������������������� "0 destinationdisk destinationDisk�� 0 itexists itExists�� 0 freespaceinit freeSpaceInit�� 0 setfocus setFocus�� "0 messagesmissing messagesMissing
�� 
cobj
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 diskinit diskInit�� &0 displaysleepstate displaySleepState�� 0 strawake strAwake�� &0 messagescancelled messagesCancelled
�� 
appr
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  *fk+ Y v*j+ O��.E�O����lv�l� �,E�O��  
*j+ Y K_ _  @_ �.E�Ob  e  �a a l Y b  f  a a a l Y hY h� ������������� 0 freespaceinit freeSpaceInit�� ����� �  ���� 	0 again  ��  � �������� 	0 again  �� 
0 reply1  �� 0 msg  � �����������������������%������������=��GJ�� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 comparesizes compareSizes�� 0 
backupinit 
backupInit�� 0 setfocus setFocus
�� 
btns
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
bhit�� 0 tidyup tidyUp�� &0 displaysleepstate displaySleepState�� 0 strawake strAwake�� &0 messagescancelled messagesCancelled
�� 
cobj
�� 
appr
�� .sysonotfnull��� ��� TEXT�� �*��l+ e  
*j+ Y �*j+ O�f  ����lv�l� �,E�Y �e  ����lv�l� �,E�Y hO�a   
*j+ Y M_ _  _ a .E�Y hOb  e  �a a l Y b  f  a a a l Y h� ��c���������� 0 
backupinit 
backupInit��  ��  �  � ���������������������������������
�� 
psxf�� "0 destinationdisk destinationDisk�� 	0 drive  �� 0 itexists itExists
� 
kocl
� 
cfol
� 
insh
� 
prdt
� 
pnam� 
� .corecrel****      � null� 0 machinefolder machineFolder
� 
cdis� 0 backupfolder backupFolder� 0 initialbackup initialBackup� 0 latestfolder latestFolder� 
0 backup  �� �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY fE` O� *a �/�a /�_ /�a /E` OPUO*j+ � �C������ 0 tidyup tidyUp�  �  � �~�}�|�{�z�y�x�w�~ 0 bf bF�} 0 creationdates creationDates�| 0 theoldestdate theOldestDate�{ 0 j  �z 0 i  �y 0 thisdate thisDate�x 
0 reply2  �w 0 msg  � )�v�u�tJ�s�rd�q�p�o�n�m��l�k�j��i���h�g�f�e��d�c�b�a�`�_�^�]14�\CF
�v 
psxf�u "0 destinationdisk destinationDisk�t 	0 drive  
�s 
cdis
�r 
cfol�q 0 machinefolder machineFolder
�p 
cobj
�o 
ascd
�n .corecnte****       ****
�m 
pnam
�l .ascrcmnt****      � ****
�k .coredeloobj        obj �j 0 setfocus setFocus
�i 
btns
�h 
dflt�g 
�f .sysodlogaskr        TEXT
�e 
bhit
�d 
trsh
�c .fndremptnull��� ��� obj �b &0 displaysleepstate displaySleepState�a 0 strawake strAwake�` &0 messagescancelled messagesCancelled
�_ 
appr
�^ .sysonotfnull��� ��� TEXT�] 0 freespaceinit freeSpaceInit
�\ .sysodelanull��� ��� nmbr�U*��/E�O�J*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/j O*j+ Oa a a a lva la  a ,E�O�a   *a ,j Y K_ _  @_ �.E�Ob  e  �a a l  Y b  f  a !a a "l  Y hY hO*ek+ #O_ _  Fb  e  a $a a %l  Okj &Y #b  f  a 'a a (l  Okj &Y hY hU� �[g�Z�Y���X�[ 
0 backup  �Z  �Y  � 
�W�V�U�T�S�R�Q�P�O�N�W 0 t  �V 0 x  �U "0 containerfolder containerFolder�T 0 
foldername 
folderName�S 0 d  �R 0 duration  �Q 0 msg  �P 0 c  �O 0 oldestfolder oldestFolder�N 0 todelete toDelete� j�M�L��K
�J�I�H�G�F�E�D�C�B�A�@�?�>�=�<��;�:�9�8�7�6�5T�4W�3ad�2������1�0�/�.�-�,����+���/13;�*�)aps�����(�'���		(	*	-	@	B	E	Z	\	_	r	t	w	�	�	�	�	�	�	�	�	�	�	�	�




#�M 0 gettimestamp getTimestamp�L 0 isbackingup isBackingUp�K 0 	stopwatch  
�J 
psxf�I 0 sourcefolder sourceFolder
�H 
TEXT
�G 
cfol
�F 
pnam�E 0 initialbackup initialBackup
�D 
kocl
�C 
insh�B 0 backupfolder backupFolder
�A 
prdt�@ 
�? .corecrel****      � null�> (0 activesourcefolder activeSourceFolder
�= 
cdis�< 	0 drive  �; 0 machinefolder machineFolder�: (0 activebackupfolder activeBackupFolder
�9 .misccurdldt    ��� null
�8 
time�7 0 	starttime 	startTime�6 &0 displaysleepstate displaySleepState�5 0 strawake strAwake
�4 
appr
�3 .sysonotfnull��� ��� TEXT�2 "0 destinationdisk destinationDisk
�1 .sysoexecTEXT���     TEXT�0 0 manualbackup manualBackup�/ 0 endtime endTime�. 0 getduration getDuration�- $0 messagescomplete messagesComplete
�, 
cobj
�+ .sysodelanull��� ��� nmbr
�* .ascrcmnt****      � ****�) *0 messagesencouraging messagesEncouraging
�( 
alis
�' 
psxp�X�*ek+  E�OeE�O*�k+ O�~*��/�&E�O*�/�,E�O�f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hO�e *j a ,E` O_ _  :b  e  a a a l Y b  f  a  a a !l Y hY hO_ "a #%_ %a $%�%a %%�&E�Oa &�%a '%�%a (%j )OfE�OfE` *O_ _  t*j a ,E` +O)j+ ,E�O_ -a ..E�Ob  e   a /�%a 0%�%a a 1l Okj 2Y )b  f  a 3�%a 4%a a 5l Okj 2Y hY hY�f �_ "a 6%_ %a 7%�%a 8%�%a 9%�&E�O_ "a :%_ %a ;%�%a <%�&E�Oa =�%j >O_ _  \*j a ,E` O_ ?a ..E�Ob  e  �a a @l Okj 2Y #b  f  a Aa a Bl Okj 2Y hY hOa C�%a D%�%a E%�%a F%j )OfE�OfE` *O*�/a G&a .-jv A��/�&E�O�a H,�&E�Oa I�%j >Oa J�%a K%E�O�j )O_ _ *j a ,E` +O)j+ ,E�O_ -a ..E�O�a L  Tb  e   a M�%a N%�%a a Ol Okj 2Y )b  f  a P�%a Q%a a Rl Okj 2Y hY Qb  e   a S�%a T%�%a a Ul Okj 2Y )b  f  a V�%a W%a a Xl Okj 2Y hOb  e  a Ya a Zl Y b  f  a [a a \l Y hY hY �_ _  �*j a ,E` +O)j+ ,E�O_ -a ..E�O�a L  Tb  e   a ]�%a ^%�%a a _l Okj 2Y )b  f  a `�%a a%a a bl Okj 2Y hY Qb  e   a c�%a d%�%a a el Okj 2Y )b  f  a f�%a g%a a hl Okj 2Y hY hY hUO*a ik+ � �&
M�%�$���#�& 0 itexists itExists�% �"��" �  �!� �! 0 
objecttype 
objectType�  
0 object  �$  � ��� 0 
objecttype 
objectType� 
0 object  � 
�
]��
k�
{�
� 
cdis
� .coredoexnull���     ****
� 
file
� 
cfol�# X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU� �
������� .0 getcomputeridentifier getComputerIdentifier�  �  � ���� 0 computername computerName� "0 strserialnumber strSerialNumber�  0 identifiername identifierName� ��
��
�
� .sysosigtsirr   ��� null
� 
sicn
� .sysoexecTEXT���     TEXT� *j  �,E�O�j E�O��%�%E�O�� �
������� 0 gettimestamp getTimestamp� ��� �  �
�
 0 isfolder isFolder�  � �	��������� �������������	 0 isfolder isFolder� 0 y  � 0 m  � 0 d  � 0 t  � 0 ty tY� 0 tm tM� 0 td tD� 0 tt tT�  
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  � ������������������������������<K������j������������
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� misccura
�� 
psof
�� 
psin�� 
�� .sysooffslong    ��� null
�� 
cha ��*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�� ��2���������� 0 comparesizes compareSizes�� ����� �  ������ 
0 source  �� 0 destination  ��  � ������������ 
0 source  �� 0 destination  �� 0 fit  �� 0 
foldersize 
folderSize�� 0 	freespace 	freeSpace� ���Y[����������������
�� 
psxf
�� .sysoexecTEXT���     TEXT
�� misccura�� �� d
�� .sysorondlong        doub
�� 
cdis
�� 
frsp
�� .ascrcmnt****      � ****�� keE�O*�/E�O� J�%�%j E�O� ��!� j U�!E�O*�/�,�!�!�!E�O� 	�� j U�!E�O��%j OPUO�� fE�Y hO�� ������������� 0 	stopwatch  �� ����� �  ���� 0 mode  ��  � ������ 0 mode  �� 0 x  � ���������� 0 gettimestamp getTimestamp
�� .ascrcmnt****      � ****�� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP� ������������ 0 getduration getDuration��  ��  � ���� 0 duration  � ������� 0 endtime endTime� 0 	starttime 	startTime� <
� misccura� d
� .sysorondlong        doub�� ���!E�O� 	�� j U�!E�O�� �@������ 0 setfocus setFocus�  �  �  � C�
� .miscactvnull��� ��� null� � *j U� �n������ $0 menuneedsupdate_ menuNeedsUpdate_� ��� �  �
� 
cmnu�  �  � �� 0 	makemenus 	makeMenus� )j+  � �������� 0 	makemenus 	makeMenus�  �  � ���� 0 i  � 0 	this_item  � 0 thismenuitem thisMenuItem� ���������������"���  0 removeallitems removeAllItems
� 
cobj
� 
nmbr
� 
TEXT
� misccura� 0 
nsmenuitem 
NSMenuItem� 	0 alloc  � J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� 0 additem_ addItem_� 0 
settarget_ 
setTarget_� �b  j+  O �kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y \��&�  ��,j+ ���m+ 
E�Y A��&�  ��,j+ ��a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ [OY�h� �N������  0 backuphandler_ backupHandler_� ��� �  �� 
0 sender  �  � ���� 
0 sender  � 40 messagesalreadybackingup messagesAlreadyBackingUp� 0 msg  � ��n�q��������� 0 isbackingup isBackingUp� 0 manualbackup manualBackup
� 
appr
� .sysonotfnull��� ��� TEXT
� .sysodelanull��� ��� nmbr
� 
cobj� ~�f  @eE�Ob  e  ���l Okj Y b  f  ���l Okj Y hOPY ;�e  4��.E�Ob  e  ���l Y b  f  ���l Y hY h� ��������  0 nsfwonhandler_ nsfwOnHandler_� ��� �  �� 
0 sender  �  � �� 
0 sender  � ���� eEc  O���mvEc  OP� �������� "0 nsfwoffhandler_ nsfwOffHandler_� � �    �� 
0 sender  �  � �� 
0 sender  � ���� fEc  O���mvEc  OP� ��~�}�|� 0 quithandler_ quitHandler_�~ �{�{   �z�z 
0 sender  �}   �y�x�y 
0 sender  �x 	0 reply   �w�v�u#&�t�s�r�q03�p6�oKRU_e�w 0 isbackingup isBackingUp�v 0 setfocus setFocus
�u 
btns
�t 
dflt�s 
�r .sysodlogaskr        TEXT
�q 
bhit
�p .aevtquitnull��� ��� null
�o .ascrcmnt****      � ****�| p�e  /*j+ O����lv�l� �,E�O��  
�j Y �j Y >�f  7*j+ O��a a lv�l� �,E�O�a   
�j Y 	a j Y h� �ny�m�l�k�n 0 makestatusbar makeStatusBar�m  �l   �j�i�h�j 0 bar  �i 0 	imagepath 	imagePath�h 	0 image   �g�f�e��d�c�b�a�`��_�^�]�\�[�Z��Y�X�W
�g misccura�f 0 nsstatusbar NSStatusBar�e "0 systemstatusbar systemStatusBar�d .0 statusitemwithlength_ statusItemWithLength_
�c 
alis
�b 
rtyp
�a 
ctxt
�` .earsffdralis        afdr
�_ 
psxp�^ 0 nsimage NSImage�] 	0 alloc  �\ 20 initwithcontentsoffile_ initWithContentsOfFile_�[ 0 	setimage_ 	setImage_�Z 0 nsmenu NSMenu�Y  0 initwithtitle_ initWithTitle_�X 0 setdelegate_ setDelegate_�W 0 setmenu_ setMenu_�k s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ � �V�U�T�S�V 0 runonce runOnce�U  �T     �R�Q�P�O�N
�R misccura�Q 0 nsthread NSThread�P 0 ismainthread isMainThread
�O 
bool�N 0 	plistinit 	plistInit�S ��,j+ �& hY hO*j+ � �MO�L�K	�J
�M .miscidlenmbr    ��� null�L  �K   �I�I 0 m  	 �H�G�F�E�D�C�B�A�@�?�>�=�<�;�:�H &0 displaysleepstate displaySleepState�G 0 strawake strAwake�F 0 isbackingup isBackingUp�E 0 manualbackup manualBackup
�D .misccurdldt    ��� null
�C 
hour�B 
�A 
min �@ �? -
�> 
bool�= 0 scheduledtime scheduledTime�< 0 	plistinit 	plistInit�; �: <�J ��� x�f  n�f  V*j �,� F*j �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY �e  
*j+ Y hY hY hO�� �9
�8�7�6
�9 .aevtoappnull  �   � ****
 k     �  �  �  �  �  �  7 b � � � � 1 6�5�5  �8  �7     C�4�3�2�1�0 ��/�.�-�, � ��+�* $(,03�)�(?CGKOSW[^�'jnrvz~���&�%����������$��#��"��!� ��
�4 afdrdlib
�3 
from
�2 fldmfldu
�1 .earsffdralis        afdr
�0 
psxp�/ 	0 plist  �. 0 initialbackup initialBackup�- 0 manualbackup manualBackup�, 0 isbackingup isBackingUp�+ �* "0 messagesmissing messagesMissing�) 	�( *0 messagesencouraging messagesEncouraging�' $0 messagescomplete messagesComplete�& �% &0 messagescancelled messagesCancelled�$ 40 messagesalreadybackingup messagesAlreadyBackingUp�# 0 strawake strAwake�" 0 strsleep strSleep
�! .sysoexecTEXT���     TEXT�  &0 displaysleepstate displaySleepState� 0 makestatusbar makeStatusBar� 0 runonce runOnce�6 ����l �,�%E�OeE�OfE�OfE�O������vE` Oa a a a a a a a a a vE` Oa a a a a  a !a "a #a $a vE` %Oa &a 'a (a )a *a +a ,a -a .vE` /Oa 0a 1a 2a 3a 4a 5a 6a 7a 8a vE` 9Oa :E` ;Oa <E` =Oa >j ?E` @O)j+ AO*j+ Bascr  ��ޭ