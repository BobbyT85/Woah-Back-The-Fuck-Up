FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 A ; Global variable declaration for use in different functions    6 � 7 7 v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s 4  8 9 8 p       : : �� ;�� 	0 plist   ; �� <�� 0 sourcefolder sourceFolder < �� =�� "0 destinationdisk destinationDisk = �� >�� 	0 drive   > �� ?�� 0 backupfolder backupFolder ? �� @�� (0 activesourcefolder activeSourceFolder @ �� A�� (0 activebackupfolder activeBackupFolder A �� B�� 0 latestfolder latestFolder B ������ 0 initialbackup initialBackup��   9  C D C p       E E ������ 0 scheduledtime scheduledTime��   D  F G F p       H H �� I�� 0 isbackingup isBackingUp I �� J�� "0 messagesmissing messagesMissing J �� K�� *0 messagesencouraging messagesEncouraging K �� L�� $0 messagescomplete messagesComplete L ������ &0 messagescancelled messagesCancelled��   G  M N M p       O O �� P�� 0 	starttime 	startTime P ������ 0 endtime endTime��   N  Q R Q l     ��������  ��  ��   R  S T S l     �� U V��   U !  Global variable assignment    V � W W 6   G l o b a l   v a r i a b l e   a s s i g n m e n t T  X Y X l     Z���� Z r      [ \ [ b      ] ^ ] n    	 _ ` _ 1    	��
�� 
psxp ` l     a���� a I    �� b c
�� .earsffdralis        afdr b m     ��
�� afdrdlib c �� d��
�� 
from d m    ��
�� fldmfldu��  ��  ��   ^ m   	 
 e e � f f f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t \ o      ���� 	0 plist  ��  ��   Y  g h g l    i���� i r     j k j m    ��
�� boovtrue k o      ���� 0 initialbackup initialBackup��  ��   h  l m l l    n���� n r     o p o m    ��
�� boovfals p o      ���� 0 isbackingup isBackingUp��  ��   m  q r q l    s���� s r     t u t J     v v  w x w m     y y � z z t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . x  { | { m     } } � ~ ~ V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . |   �  m     � � � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d ! �  � � � m     � � � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �  ��� � m     � � � � � � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .��   u o      ���� "0 messagesmissing messagesMissing��  ��   r  � � � l    C ����� � r     C � � � J     ? � �  � � � m     # � � � � �  C o m e   o n ! �  � � � m   # & � � � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r ! �  � � � m   & ) � � � � � " N o   p a i n !   N o   p a i n ! �  � � � m   ) , � � � � � 0 L e t ' s   d o   t h i s   a s   a   t e a m ! �  � � � m   , / � � � � �  W e   c a n   d o   i t ! �  � � � m   / 2 � � � � �  F r e e e e e e e e e d o m ! �  � � � m   2 5 � � � � � 2 A l t o g e t h e r   o r   n o t   a t   a l l ! �  � � � m   5 8 � � � � � H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! ! �  ��� � m   8 ; � � � � � H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !��   � o      ���� *0 messagesencouraging messagesEncouraging��  ��   �  � � � l  D g ����� � r   D g � � � J   D c � �  � � � m   D G � � � � �  N i c e   o n e ! �  � � � m   G J � � � � � * Y o u   a b s o l u t e   l e g   e n d ! �  � � � m   J M � � � � �  G o o d   l a d ! �  � � � m   M P � � � � �  P e r f i c k ! �  � � � m   P S � � � � �  H a p p y   d a y s ! �  � � � m   S V � � � � �  F r e e e e e e e e e d o m ! �  � � � m   V Y � � � � � H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! ! �  � � � m   Y \ � � � � � B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s ! �  ��� � m   \ _ � � � � � d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !��   � o      ���� $0 messagescomplete messagesComplete��  ��   �  � � � l  h � ����� � r   h � � � � J   h � � �  � � � m   h k � � � � �  T h a t ' s   a   s h a m e �  � � � m   k n � � � � �  A h   m a n ,   u n l u c k y �  � � � m   n q � � � � �  O h   w e l l �  � � � m   q t � � � � � @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e n �  � � � m   t w � � � � � , W e l l   t h a t ' s   a   l e t   d o w n �  � � � m   w z � � � � � P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ? �  � � � m   z } � � � � �  A r r o g a n t �  ��� � m   } � � � �   > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0��   � o      ���� &0 messagescancelled messagesCancelled��  ��   �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��   	
	 l     ��������  ��  ��  
  l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����   � �-----------------------------------------------------------------------------------------------------------------------------------------------    � - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ����   � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------    � - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��������  ��  ��    l     ����   6 0 Function to check for a .plist preferences file    �   `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e !"! l     ��#$��  # m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   $ �%% �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e" &'& l     ��()��  ( b \ If a .plist file is present, it assigns preferences to their corresponding global variables   ) �** �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s' +,+ i     -.- I      �������� 0 	plistinit 	plistInit��  ��  . k    i// 010 q      22 ��3�� 0 
foldername 
folderName3 ��4�� 0 
backupdisk 
backupDisk4 ������ 0 
backuptime 
backupTime��  1 565 r     787 m     ����  8 o      ���� 0 
backuptime 
backupTime6 9:9 l   ��������  ��  ��  : ;<; Z   E=>�?= l   @�~�}@ =    ABA I    �|C�{�| 0 itexists itExistsC DED m    FF �GG  f i l eE H�zH o    �y�y 	0 plist  �z  �{  B m    �x
�x boovtrue�~  �}  > O    7IJI k    6KK LML r    NON n    PQP 1    �w
�w 
pcntQ 4    �vR
�v 
plifR o    �u�u 	0 plist  O o      �t�t 0 thecontents theContentsM STS r    "UVU n     WXW 1     �s
�s 
valLX o    �r�r 0 thecontents theContentsV o      �q�q 0 thevalue theValueT YZY l  # #�p�o�n�p  �o  �n  Z [\[ r   # (]^] n   # &_`_ o   $ &�m�m  0 foldertobackup FolderToBackup` o   # $�l�l 0 thevalue theValue^ o      �k�k 0 
foldername 
folderName\ aba r   ) .cdc n   ) ,efe o   * ,�j�j 0 backupdrive BackupDrivef o   ) *�i�i 0 thevalue theValued o      �h�h 0 
backupdisk 
backupDiskb ghg r   / 4iji n   / 2klk o   0 2�g�g 0 scheduledtime scheduledTimel o   / 0�f�f 0 thevalue theValuej o      �e�e 0 
backuptime 
backupTimeh mnm l  5 5�d�c�b�d  �c  �b  n o�ao l  5 5�`pq�`  p . (log {folderName, backupDisk, backupTime}   q �rr P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�a  J m    ss�                                                                                  sevs  alis    �  Macintosh HD               �e�H+  ��1System Events.app                                              ��e����        ����  	                CoreServices    �e��      ����    ��1��0��/  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  �  ? k   :Ett uvu O   : �wxw k   > �yy z{z r   > G|}| l  > E~�_�^~ I  > E�]�\
�] .sysostflalis    ��� null�\   �[��Z
�[ 
prmp� m   @ A�� ��� @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p�Z  �_  �^  } o      �Y�Y 0 
foldername 
folderName{ ��� r   H S��� c   H Q��� l  H O��X�W� I  H O�V�U�
�V .sysostflalis    ��� null�U  � �T��S
�T 
prmp� m   J K�� ��� R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o�S  �X  �W  � m   O P�R
�R 
TEXT� o      �Q�Q 0 
backupdisk 
backupDisk� ��� l  T T�P�O�N�P  �O  �N  � ��� r   T [��� n   T Y��� 1   U Y�M
�M 
psxp� o   T U�L�L 0 
foldername 
folderName� o      �K�K 0 
foldername 
folderName� ��� r   \ c��� n   \ a��� 1   ] a�J
�J 
psxp� o   \ ]�I�I 0 
backupdisk 
backupDisk� o      �H�H 0 
backupdisk 
backupDisk� ��� l  d d�G�F�E�G  �F  �E  � ��� r   d q��� J   d o�� ��� m   d g�� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r� ��� m   g j�� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r� ��D� m   j m�� ��� , E v e r y   h o u r   o n   t h e   h o u r�D  � o      �C�C 0 backuptimes backupTimes� ��� r   r ���� c   r ��� J   r }�� ��B� I  r {�A��
�A .gtqpchltns    @   @ ns  � o   r s�@�@ 0 backuptimes backupTimes� �?��>
�? 
prmp� m   t w�� ��� 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?�>  �B  � m   } ~�=
�= 
TEXT� o      �<�< 0 selectedtime selectedTime� ��� l  � ��;���;  �  log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )� ��� l  � ��:�9�8�:  �9  �8  � ��� Z   � �����7� l  � ���6�5� =   � ���� o   � ��4�4 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r�6  �5  � r   � ���� m   � ��3�3 � o      �2�2 0 
backuptime 
backupTime� ��� l  � ���1�0� =   � ���� o   � ��/�/ 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r�1  �0  � ��� r   � ���� m   � ��.�. � o      �-�- 0 
backuptime 
backupTime� ��� l  � ���,�+� =   � ���� o   � ��*�* 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r�,  �+  � ��)� r   � ���� m   � ��(�( <� o      �'�' 0 
backuptime 
backupTime�)  �7  � ��� l  � ��&�%�$�&  �%  �$  � ��#� l  � ��"���"  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�#  x m   : ;���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  v ��� l  � ��!� ��!  �   �  � ��� O   �E��� O   �D��� k   �C�� ��� I  � ����
� .corecrel****      � null�  � ���
� 
kocl� m   � ��
� 
plii� ���
� 
insh�  ;   � �� ���
� 
prdt� K   � ��� ���
� 
kind� m   � ��
� 
TEXT� ���
� 
pnam� m   � ��� �    F o l d e r T o B a c k u p� ��
� 
valL o   � ��� 0 
foldername 
folderName�  �  �  I  ���
� .corecrel****      � null�   �
� 
kocl m   � ��
� 
plii �
� 
insh  ;   �	�

� 
prdt	 K  

 �	
�	 
kind m  
�
� 
TEXT �
� 
pnam m   �  B a c k u p D r i v e ��
� 
valL o  �� 0 
backupdisk 
backupDisk�  �
   � I C��
� .corecrel****      � null�   � 
�  
kocl m  "%��
�� 
plii ��
�� 
insh  ;  (* ����
�� 
prdt K  -= ��
�� 
kind m  01��
�� 
TEXT ��
�� 
pnam m  47 �  S c h e d u l e d T i m e �� ��
�� 
valL  o  89���� 0 
backuptime 
backupTime��  ��  �  � l  � �!����! I  � �����"
�� .corecrel****      � null��  " ��#$
�� 
kocl# m   � ���
�� 
plif$ ��%��
�� 
prdt% K   � �&& ��'��
�� 
pnam' o   � ����� 	0 plist  ��  ��  ��  ��  � m   � �((�                                                                                  sevs  alis    �  Macintosh HD               �e�H+  ��1System Events.app                                              ��e����        ����  	                CoreServices    �e��      ����    ��1��0��/  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  �  < )*) l FF��������  ��  ��  * +,+ r  FK-.- o  FG���� 0 
foldername 
folderName. o      ���� 0 sourcefolder sourceFolder, /0/ r  LQ121 o  LM���� 0 
backupdisk 
backupDisk2 o      ���� "0 destinationdisk destinationDisk0 343 r  RU565 o  RS���� 0 
backuptime 
backupTime6 o      ���� 0 scheduledtime scheduledTime4 787 I Vc��9��
�� .ascrcmnt****      � ****9 J  V_:: ;<; o  VY���� 0 sourcefolder sourceFolder< =>= o  Y\���� "0 destinationdisk destinationDisk> ?��? o  \]���� 0 scheduledtime scheduledTime��  ��  8 @A@ l dd��������  ��  ��  A B��B I  di�������� 0 diskinit diskInit��  ��  ��  , CDC l     ��������  ��  ��  D EFE l     ��������  ��  ��  F GHG l     ��������  ��  ��  H IJI l     ��������  ��  ��  J KLK l     ��������  ��  ��  L MNM l     ��OP��  O H B Function to detect if the selected hard drive is connected or not   P �QQ �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o tN RSR l     ��TU��  T T N This only happens once a hard drive has been selected and provides a reminder   U �VV �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e rS WXW i    YZY I      �������� 0 diskinit diskInit��  ��  Z Z     K[\��][ l    	^����^ =     	_`_ I     ��a���� 0 itexists itExistsa bcb m    dd �ee  d i s kc f��f o    ���� "0 destinationdisk destinationDisk��  ��  ` m    ��
�� boovtrue��  ��  \ I    ��g���� 0 freespaceinit freeSpaceInitg h��h m    ��
�� boovfals��  ��  ��  ] k    Kii jkj r    lml n    non 3    ��
�� 
cobjo o    ���� "0 messagesmissing messagesMissingm o      ���� 0 msg  k pqp r    +rsr l   )t����t n    )uvu 1   ' )��
�� 
bhitv l   'w����w I   '��xy
�� .sysodlogaskr        TEXTx o    ���� 0 msg  y ��z{
�� 
btnsz J    !|| }~} m     ���  C a n c e l   B a c k u p~ ���� m    �� ���  O K��  { �����
�� 
dflt� m   " #���� ��  ��  ��  ��  ��  s o      ���� 	0 reply  q ���� Z   , K������ l  , /������ =   , /��� o   , -���� 	0 reply  � m   - .�� ���  O K��  ��  � I   2 7�������� 0 diskinit diskInit��  ��  ��  � k   : K�� ��� r   : ?��� n   : =��� 3   ; =��
�� 
cobj� o   : ;���� &0 messagescancelled messagesCancelled� o      ���� 0 msg  � ���� I  @ K����
�� .sysonotfnull��� ��� TEXT� o   @ A���� 0 msg  � �����
�� 
appr� m   D G�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  ��  ��  X ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i    ��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      �� 	0 again  ��  ��  � Z     k���~�� l    	��}�|� =     	��� I     �{��z�{ 0 comparesizes compareSizes� ��� o    �y�y 0 sourcefolder sourceFolder� ��x� o    �w�w "0 destinationdisk destinationDisk�x  �z  � m    �v
�v boovtrue�}  �|  � I    �u�t�s�u 0 
backupinit 
backupInit�t  �s  �~  � k    k�� ��� Z    G����r� l   ��q�p� =    ��� o    �o�o 	0 again  � m    �n
�n boovfals�q  �p  � r    *��� l   (��m�l� n    (��� 1   & (�k
�k 
bhit� l   &��j�i� I   &�h��
�h .sysodlogaskr        TEXT� m    �� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� �g��
�g 
btns� J     �� ��� m    �� ���  Y e s� ��f� m    �� ���  C a n c e l   B a c k u p�f  � �e��d
�e 
dflt� m   ! "�c�c �d  �j  �i  �m  �l  � o      �b�b 
0 reply1  � ��� l  - 0��a�`� =   - 0��� o   - .�_�_ 	0 again  � m   . /�^
�^ boovtrue�a  �`  � ��]� r   3 C��� l  3 A��\�[� n   3 A��� 1   ? A�Z
�Z 
bhit� l  3 ?��Y�X� I  3 ?�W��
�W .sysodlogaskr        TEXT� m   3 4�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� �V��
�V 
btns� J   5 9�� ��� m   5 6�� ���  Y e s� ��U� m   6 7�� ���  C a n c e l   B a c k u p�U  � �T��S
�T 
dflt� m   : ;�R�R �S  �Y  �X  �\  �[  � o      �Q�Q 
0 reply1  �]  �r  � ��� l  H H�P�O�N�P  �O  �N  � ��M� Z   H k �L  l  H K�K�J =   H K o   H I�I�I 
0 reply1   m   I J �  Y e s�K  �J   I   N S�H�G�F�H 0 tidyup tidyUp�G  �F  �L   k   V k 	
	 r   V _ n   V ] 3   Y ]�E
�E 
cobj o   V Y�D�D &0 messagescancelled messagesCancelled o      �C�C 0 msg  
 �B I  ` k�A
�A .sysonotfnull��� ��� TEXT o   ` a�@�@ 0 msg   �?�>
�? 
appr m   d g � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�>  �B  �M  �  l     �=�<�;�=  �<  �;    l     �:�9�8�:  �9  �8    l     �7�6�5�7  �6  �5    l     �4�3�2�4  �3  �2    l     �1�0�/�1  �0  �/     l     �.!"�.  !,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   " �##L   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .  $%$ l     �-&'�-  & g a These folders, if required, are then set to their corresponding global variables declared above.   ' �(( �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .% )*) i    +,+ I      �,�+�*�, 0 
backupinit 
backupInit�+  �*  , k     �-- ./. r     010 4     �)2
�) 
psxf2 o    �(�( "0 destinationdisk destinationDisk1 o      �'�' 	0 drive  / 343 l   �&56�&  5 ' !log (destinationDisk & "Backups")   6 �77 B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )4 898 l   �%�$�#�%  �$  �#  9 :;: Z    ,<=�"�!< l   >� �> =    ?@? I    �A�� 0 itexists itExistsA BCB m    	DD �EE  f o l d e rC F�F b   	 GHG o   	 
�� "0 destinationdisk destinationDiskH m   
 II �JJ  B a c k u p s�  �  @ m    �
� boovfals�   �  = O    (KLK I   '��M
� .corecrel****      � null�  M �NO
� 
koclN m    �
� 
cfolO �PQ
� 
inshP o    �� 	0 drive  Q �R�
� 
prdtR K    #SS �T�
� 
pnamT m     !UU �VV  B a c k u p s�  �  L m    WW�                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  �"  �!  ; XYX l  - -����  �  �  Y Z[Z O   - A\]\ k   1 @^^ _`_ r   1 >aba n   1 :cdc 4   5 :�e
� 
cfole m   6 9ff �gg  B a c k u p sd 4   1 5�h
� 
cdish o   3 4�
�
 	0 drive  b o      �	�	 0 backupfolder backupFolder` i�i l  ? ?�jk�  j  log (backupFolder)   k �ll $ l o g   ( b a c k u p F o l d e r )�  ] m   - .mm�                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  [ non l  B B����  �  �  o pqp Z   B srs�tr l  B Qu��u =   B Qvwv I   B O� x���  0 itexists itExistsx yzy m   C F{{ �||  f o l d e rz }��} b   F K~~ o   F G���� "0 destinationdisk destinationDisk m   G J�� ���  B a c k u p s / L a t e s t��  ��  w m   O P��
�� boovfals�  �  s O   T k��� I  X j�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   Z [��
�� 
cfol� ����
�� 
insh� o   \ _���� 0 backupfolder backupFolder� �����
�� 
prdt� K   ` f�� �����
�� 
pnam� m   a d�� ���  L a t e s t��  ��  � m   T U���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  �  t r   n s��� m   n o��
�� boovfals� o      ���� 0 initialbackup initialBackupq ��� l  t t��������  ��  ��  � ��� O   t ���� k   x ��� ��� r   x ���� n   x ���� 4   � ����
�� 
cfol� m   � ��� ���  L a t e s t� n   x ���� 4   | ����
�� 
cfol� m   } ��� ���  B a c k u p s� 4   x |���
�� 
cdis� o   z {���� 	0 drive  � o      ���� 0 latestfolder latestFolder� ���� l  � �������  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )��  � m   t u���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ���������  ��  ��  � ���� I   � ��������� 
0 backup  ��  ��  ��  * ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.   � ��� �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .� ��� l     ������  � � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.   � ����   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .� ��� l     ������  � � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.   � ���P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .� ��� l     ������  � � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.   � ���>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .� ��� i    ��� I      �������� 0 tidyup tidyUp��  ��  � O     ���� k    ��� ��� r    ��� n    	��� 1    	��
�� 
ascd� n    ��� 2   ��
�� 
cobj� o    ���� 0 backupfolder backupFolder� o      ���� 0 creationdates creationDates� ��� r    ��� n    ��� 4    ���
�� 
cobj� m    ���� � o    ���� 0 creationdates creationDates� o      ���� 0 theoldestdate theOldestDate� ��� r    ��� m    ���� � o      ���� 0 j  � ��� l   ��������  ��  ��  � ��� Y    K�������� k   % F�� ��� r   % +��� n   % )��� 4   & )���
�� 
cobj� o   ' (���� 0 i  � o   % &���� 0 creationdates creationDates� o      ���� 0 thisdate thisDate� ���� Z   , F������� l  , /������ A   , /��� o   , -���� 0 thisdate thisDate� o   - .���� 0 theoldestdate theOldestDate��  ��  � k   2 B�� ��� r   2 5� � o   2 3���� 0 thisdate thisDate  o      ���� 0 theoldestdate theOldestDate�  r   6 9 o   6 7���� 0 i   o      ���� 0 j   �� I  : B����
�� .ascrcmnt****      � **** l  : >���� n   : >	 4   ; >��

�� 
cobj
 o   < =���� 0 j  	 o   : ;���� 0 backupfolder backupFolder��  ��  ��  ��  ��  ��  ��  �� 0 i  � m    ���� � I    ����
�� .corecnte****       **** o    ���� 0 creationdates creationDates��  ��  �  l  L L��������  ��  ��    l  L L����   ! -- Delete the oldest folder    � 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r  I  L T����
�� .coredeloobj        obj  n   L P 4   M P��
�� 
cobj o   N O���� 0 j   o   L M���� 0 backupfolder backupFolder��    l  U U��������  ��  ��    l  U U��������  ��  ��    l   U U�� ��   � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete     �!!� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e "#" l  U U��������  ��  ��  # $%$ l  U U��������  ��  ��  % &'& r   U e()( l  U c*����* n   U c+,+ 1   a c��
�� 
bhit, l  U a-����- I  U a��./
�� .sysodlogaskr        TEXT. m   U V00 �11D Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .  / ��23
�� 
btns2 J   W [44 565 m   W X77 �88  E m p t y   T r a s h6 9��9 m   X Y:: �;;  C a n c e l   B a c k u p��  3 ��<�
�� 
dflt< m   \ ]�~�~ �  ��  ��  ��  ��  ) o      �}�} 
0 reply2  ' =>= Z   f �?@�|A? l  f iB�{�zB =   f iCDC o   f g�y�y 
0 reply2  D m   g hEE �FF  E m p t y   T r a s h�{  �z  @ I  l u�xG�w
�x .fndremptnull��� ��� obj G l  l qH�v�uH 1   l q�t
�t 
trsh�v  �u  �w  �|  A k   x �II JKJ r   x LML n   x }NON 3   { }�s
�s 
cobjO o   x {�r�r &0 messagescancelled messagesCancelledM o      �q�q 0 msg  K P�pP I  � ��oQR
�o .sysonotfnull��� ��� TEXTQ o   � ��n�n 0 msg  R �mS�l
�m 
apprS m   � �TT �UU > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�l  �p  > VWV l  � ��k�j�i�k  �j  �i  W XYX I   � ��hZ�g�h 0 freespaceinit freeSpaceInitZ [�f[ m   � ��e
�e boovtrue�f  �g  Y \�d\ I  � ��c]^
�c .sysonotfnull��� ��� TEXT] m   � �__ �`` � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .^ �ba�a
�b 
appra m   � �bb �cc 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�a  �d  � m     dd�                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  � efe l     �`�_�^�`  �_  �^  f ghg l     �]�\�[�]  �\  �[  h iji l     �Z�Y�X�Z  �Y  �X  j klk l     �W�V�U�W  �V  �U  l mnm l     �T�S�R�T  �S  �R  n opo l     �Qqr�Q  q M G Function that carries out the backup process and is split in 2 parts.    r �ss �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  p tut l     �Pvw�P  v*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   w �xxH   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .u yzy l     �O{|�O  { � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   | �}}   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .z ~~ i    ��� I      �N�M�L�N 
0 backup  �M  �L  � k    I�� ��� q      �� �K��K 0 t  � �J��J 0 x  � �I�H�I "0 containerfolder containerFolder�H  � ��� r     ��� I     �G��F�G 0 gettimestamp getTimestamp� ��E� m    �D
�D boovtrue�E  �F  � o      �C�C 0 t  � ��� l  	 	�B�A�@�B  �A  �@  � ��� r   	 ��� m   	 
�?
�? boovtrue� o      �>�> 0 isbackingup isBackingUp� ��� l   �=�<�;�=  �<  �;  � ��� I    �:��9�: 0 	stopwatch  � ��8� m    �� ��� 
 s t a r t�8  �9  � ��� l   �7�6�5�7  �6  �5  � ��� O   @��� k   ?�� ��� l   �4���4  � f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d� ��� l   �3���3  � x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p� ��� r     ��� c    ��� 4    �2�
�2 
psxf� o    �1�1 0 sourcefolder sourceFolder� m    �0
�0 
TEXT� o      �/�/ 0 
foldername 
folderName� ��� r   ! )��� n   ! '��� 1   % '�.
�. 
pnam� 4   ! %�-�
�- 
cfol� o   # $�,�, 0 
foldername 
folderName� o      �+�+ 0 
foldername 
folderName� ��� l  * *�*���*  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  * *�)�(�'�)  �(  �'  � ��� Z   * ~���&�%� l  * -��$�#� =   * -��� o   * +�"�" 0 initialbackup initialBackup� m   + ,�!
�! boovfals�$  �#  � k   0 z�� ��� I  0 >� ��
�  .corecrel****      � null�  � ���
� 
kocl� m   2 3�
� 
cfol� ���
� 
insh� o   4 5�� 0 backupfolder backupFolder� ���
� 
prdt� K   6 :�� ���
� 
pnam� o   7 8�� 0 t  �  �  � ��� l  ? ?����  �  �  � ��� r   ? I��� c   ? E��� 4   ? C��
� 
psxf� o   A B�� 0 sourcefolder sourceFolder� m   C D�
� 
TEXT� o      �� (0 activesourcefolder activeSourceFolder� ��� r   J T��� 4   J P��
� 
cfol� o   L O�� (0 activesourcefolder activeSourceFolder� o      �� (0 activesourcefolder activeSourceFolder� ��� l  U U����  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   U i��� n   U e��� 4   b e�
�
�
 
cfol� o   c d�	�	 0 t  � n   U b��� 4   ] b��
� 
cfol� m   ^ a�� ���  B a c k u p s� 4   U ]��
� 
cdis� o   Y \�� 	0 drive  � o      �� (0 activebackupfolder activeBackupFolder� ��� l  j j����  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ��� I  j z���
� .corecrel****      � null�  � � ��
�  
kocl� m   l m��
�� 
cfol� ����
�� 
insh� o   n q���� (0 activebackupfolder activeBackupFolder� �����
�� 
prdt� K   r v   ����
�� 
pnam o   s t���� 0 
foldername 
folderName��  ��  �  �&  �%  �  l   ��������  ��  ��    l   ��������  ��  ��    l   ��	��   o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   	 �

 �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e  l   ����   q k An atomic version would just to do a straight backup without versioning to see the function at full effect    � �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t  l   ����   D > This means that only new or updated files will be copied over    � |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r  l   ����   ] W Any deleted files in the source folder will be deleted in the destination folder too		    � �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	  l   ����     Original sync code     � (   O r i g i n a l   s y n c   c o d e     l   ��!"��  ! z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   " �## �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "  $%$ l   ��������  ��  ��  % &'& l   ��()��  (   Original code   ) �**    O r i g i n a l   c o d e' +,+ l   ��-.��  - i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   . �// �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h, 010 l   ��23��  2 x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   3 �44 �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .1 565 l   ��78��  7 p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   8 �99 �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .6 :;: l   ��<=��  < � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   = �>>�   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .; ?@? l   ��AB��  A � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   B �CCv   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .@ DED l   ��������  ��  ��  E FGF l   ��������  ��  ��  G H��H Z   ?IJK��I l   �L����L =    �MNM o    ����� 0 initialbackup initialBackupN m   � ���
�� boovtrue��  ��  J k   �OO PQP r   � �RSR n   � �TUT 1   � ���
�� 
timeU l  � �V����V I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  S o      ���� 0 	starttime 	startTimeQ WXW I  � ���YZ
�� .sysonotfnull��� ��� TEXTY m   � �[[ �\\ � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .Z ��]��
�� 
appr] m   � �^^ �__ 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  X `a` l  � ���������  ��  ��  a bcb r   � �ded c   � �fgf l  � �h����h b   � �iji b   � �klk b   � �mnm o   � ����� "0 destinationdisk destinationDiskn m   � �oo �pp  B a c k u p s / L a t e s t /l o   � ����� 0 
foldername 
folderNamej m   � �qq �rr  /��  ��  g m   � ���
�� 
TEXTe o      ���� 0 d  c sts I  � ���u��
�� .sysoexecTEXT���     TEXTu b   � �vwv b   � �xyx b   � �z{z b   � �|}| m   � �~~ �  d i t t o   '} o   � ����� 0 sourcefolder sourceFolder{ m   � ��� ���  '   'y o   � ����� 0 d  w m   � ��� ���  / '��  t ��� l  � ���������  ��  ��  � ��� r   � ���� m   � ���
�� boovfals� o      ���� 0 isbackingup isBackingUp� ��� l  � ���������  ��  ��  � ��� r   � ���� n   � ���� 1   � ���
�� 
time� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 endtime endTime� ��� r   � ���� I   � ��������� 0 getduration getDuration��  ��  � o      ���� 0 duration  � ��� r   � ���� n   � ���� 3   � ���
�� 
cobj� o   � ����� $0 messagescomplete messagesComplete� o      ���� 0 msg  � ��� I  � ����
�� .sysonotfnull��� ��� TEXT� b   � ���� b   � ���� b   � ���� m   � ��� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o   � ����� 0 duration  � m   � ��� ���    m i n u t e s ! 
� o   � ����� 0 msg  � �����
�� 
appr� m   � ��� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  � ���� I �����
�� .sysodelanull��� ��� nmbr� m  ���� ��  ��  K ��� l 	������ =  	��� o  	
���� 0 initialbackup initialBackup� m  
��
�� boovfals��  ��  � ���� k  ;�� ��� r  &��� c  $��� l "������ b  "��� b  ��� b  ��� b  ��� b  ��� o  ���� "0 destinationdisk destinationDisk� m  �� ���  B a c k u p s /� o  ���� 0 t  � m  �� ���  /� o  ���� 0 
foldername 
folderName� m  !�� ���  /��  ��  � m  "#��
�� 
TEXT� o      ���� 0 c  � ��� r  '8��� c  '6��� l '4������ b  '4��� b  '0��� b  '.��� o  '*���� "0 destinationdisk destinationDisk� m  *-�� ���  B a c k u p s / L a t e s t /� o  ./���� 0 
foldername 
folderName� m  03�� ���  /��  ��  � m  45��
�� 
TEXT� o      ���� 0 d  � ��� I 9B�����
�� .ascrcmnt****      � ****� l 9>������ b  9>��� m  9<�� ���  b a c k i n g   u p   t o :  � o  <=���� 0 d  ��  ��  ��  � ��� l CC��������  ��  ��  � ��� r  CP��� n  CL��� 1  HL��
�� 
time� l CH������ I CH������
�� .misccurdldt    ��� null��  ��  ��  ��  � o      ���� 0 	starttime 	startTime� ��� r  QZ��� n  QX��� 3  TX�
� 
cobj� o  QT�~�~ *0 messagesencouraging messagesEncouraging� o      �}�} 0 msg  � ��� I [f�|��
�| .sysonotfnull��� ��� TEXT� o  [\�{�{ 0 msg  � �z��y
�z 
appr� m  _b�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�y  � ��� l gg�x�w�v�x  �w  �v  � � � I g��u�t
�u .sysoexecTEXT���     TEXT b  g| b  gx b  gv b  gr	 b  gp

 b  gl m  gj � V r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = ' o  jk�s�s 0 c   m  lo �  '   '	 o  pq�r�r 0 sourcefolder sourceFolder m  ru �  '   ' o  vw�q�q 0 d   m  x{ �  / '�t     l ���p�o�n�p  �o  �n    r  �� m  ���m
�m boovfals o      �l�l 0 isbackingup isBackingUp  l ���k�j�i�k  �j  �i   �h Z  �; �g! = ��"#" n  ��$%$ 2 ���f
�f 
cobj% l ��&�e�d& c  ��'(' 4  ���c)
�c 
psxf) o  ���b�b 0 c  ( m  ���a
�a 
alis�e  �d  # J  ���`�`    k  ��** +,+ l ���_-.�_  - % delete folder t of backupFolder   . �// > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r, 010 l ���^�]�\�^  �]  �\  1 232 r  ��454 c  ��676 n  ��898 4  ���[:
�[ 
cfol: o  ���Z�Z 0 t  9 o  ���Y�Y 0 backupfolder backupFolder7 m  ���X
�X 
TEXT5 o      �W�W 0 oldestfolder oldestFolder3 ;<; r  ��=>= c  ��?@? n  ��ABA 1  ���V
�V 
psxpB o  ���U�U 0 oldestfolder oldestFolder@ m  ���T
�T 
TEXT> o      �S�S 0 oldestfolder oldestFolder< CDC I ���RE�Q
�R .ascrcmnt****      � ****E l ��F�P�OF b  ��GHG m  ��II �JJ  t o   d e l e t e :  H o  ���N�N 0 oldestfolder oldestFolder�P  �O  �Q  D KLK l ���M�L�K�M  �L  �K  L MNM r  ��OPO b  ��QRQ m  ��SS �TT  r m   - r f  R o  ���J�J 0 oldestfolder oldestFolderP o      �I�I 0 todelete toDeleteN UVU I ���HW�G
�H .sysoexecTEXT���     TEXTW o  ���F�F 0 todelete toDelete�G  V XYX l ���E�D�C�E  �D  �C  Y Z[Z r  ��\]\ n  ��^_^ 1  ���B
�B 
time_ l ��`�A�@` I ���?�>�=
�? .misccurdldt    ��� null�>  �=  �A  �@  ] o      �<�< 0 endtime endTime[ aba r  ��cdc I  ���;�:�9�; 0 getduration getDuration�:  �9  d o      �8�8 0 duration  b efe l ���7gh�7  g � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   h �ii d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "f jkj l ���6lm�6  l  delay 2   m �nn  d e l a y   2k opo l ���5�4�3�5  �4  �3  p qrq r  ��sts n  ��uvu 3  ���2
�2 
cobjv o  ���1�1 $0 messagescomplete messagesCompletet o      �0�0 0 msg  r wxw I ���/yz
�/ .sysonotfnull��� ��� TEXTy b  ��{|{ b  ��}~} b  ��� m  ���� ���  A n d   i n  � o  ���.�. 0 duration  ~ m  ���� ���    m i n u t e s ! 
| o  ���-�- 0 msg  z �,��+
�, 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�+  x ��*� I ���)��(
�) .sysodelanull��� ��� nmbr� m  ���'�' �(  �*  �g  ! k   ;�� ��� r   ��� n   	��� 1  	�&
�& 
time� l  ��%�$� I  �#�"�!
�# .misccurdldt    ��� null�"  �!  �%  �$  � o      � �  0 endtime endTime� ��� r  ��� I  ���� 0 getduration getDuration�  �  � o      �� 0 duration  � ��� r  ��� n  ��� 3  �
� 
cobj� o  �� $0 messagescomplete messagesComplete� o      �� 0 msg  � ��� I  5���
� .sysonotfnull��� ��� TEXT� b   +��� b   )��� b   %��� m   #�� ���  A n d   i n  � o  #$�� 0 duration  � m  %(�� ���    m i n u t e s ! 
� o  )*�� 0 msg  � ���
� 
appr� m  .1�� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�  � ��� I 6;���
� .sysodelanull��� ��� nmbr� m  67�� �  �  �h  ��  ��  ��  � m    ���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  � ��� l AA����  �  �  � ��� I  AI���
� 0 	stopwatch  � ��	� m  BE�� ���  f i n i s h�	  �
  �   ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ��� �  �  �   � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ������  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ������  � � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   � ��� - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     ��������  ��  ��  � ��� l     ������  � � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   � ���   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .� ��� l     ������  � � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   � ����   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .� ��� i    ��� I      ������� 0 itexists itExists� ��� o      ���� 0 
objecttype 
objectType� ���� o      ���� 
0 object  ��  ��  � l    W���� O     W��� Z    V������ l   ������ =    ��� o    ���� 0 
objecttype 
objectType� m    �� ���  d i s k��  ��  � Z   
 ������ I  
 �����
�� .coredoexnull���     ****� 4   
 ���
�� 
cdis� o    ���� 
0 object  ��  � L    �� m    ��
�� boovtrue��  � L    �� m    ��
�� boovfals� ��� l   "������ =    "��� o     ���� 0 
objecttype 
objectType� m     !�� ���  f i l e��  ��  � � � Z   % 7�� I  % -����
�� .coredoexnull���     **** 4   % )��
�� 
file o   ' (���� 
0 object  ��   L   0 2 m   0 1��
�� boovtrue��   L   5 7 m   5 6��
�� boovfals  	 l  : =
����
 =   : = o   : ;���� 0 
objecttype 
objectType m   ; < �  f o l d e r��  ��  	 �� Z   @ R�� I  @ H����
�� .coredoexnull���     **** 4   @ D��
�� 
cfol o   B C���� 
0 object  ��   L   K M m   K L��
�� boovtrue��   L   P R m   P Q��
�� boovfals��  ��  � m     �                                                                                  sevs  alis    �  Macintosh HD               �e�H+  ��1System Events.app                                              ��e����        ����  	                CoreServices    �e��      ����    ��1��0��/  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  � "  (string, string) as Boolean   � � 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n�  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��     l     ��������  ��  ��    !"! l     ��������  ��  ��  " #$# l     ��%&��  % � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   & �''   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .$ ()( l     ��*+��  * � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   + �,,   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .) -.- i    /0/ I      ��1���� 0 gettimestamp getTimestamp1 2��2 o      ���� 0 isfolder isFolder��  ��  0 l   m3453 k    m66 787 l     ��9:��  9   Date variables   : �;;    D a t e   v a r i a b l e s8 <=< r     )>?> l     @����@ I     ������
�� .misccurdldt    ��� null��  ��  ��  ��  ? K    AA ��BC
�� 
yearB o    ���� 0 y  C ��DE
�� 
mnthD o    ���� 0 m  E ��FG
�� 
day F o    ���� 0 d  G ��H��
�� 
timeH o   	 
���� 0 t  ��  = IJI r   * 1KLK c   * /MNM l  * -O����O c   * -PQP o   * +���� 0 y  Q m   + ,��
�� 
long��  ��  N m   - .��
�� 
TEXTL o      ���� 0 ty tYJ RSR r   2 9TUT c   2 7VWV l  2 5X����X c   2 5YZY o   2 3���� 0 y  Z m   3 4��
�� 
long��  ��  W m   5 6��
�� 
TEXTU o      ���� 0 ty tYS [\[ r   : A]^] c   : ?_`_ l  : =a����a c   : =bcb o   : ;���� 0 m  c m   ; <��
�� 
long��  ��  ` m   = >��
�� 
TEXT^ o      ���� 0 tm tM\ ded r   B Ifgf c   B Ghih l  B Ej����j c   B Eklk o   B C���� 0 d  l m   C D��
�� 
long��  ��  i m   E F��
�� 
TEXTg o      ���� 0 td tDe mnm r   J Qopo c   J Oqrq l  J Ms����s c   J Mtut o   J K���� 0 t  u m   K L��
�� 
long��  ��  r m   M N��
�� 
TEXTp o      ���� 0 tt tTn vwv l  R R��������  ��  ��  w xyx l  R R��z{��  z U O Append the month or day with a 0 if the string length is only 1 character long   { �|| �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n gy }~} r   R [� c   R Y��� l  R W������ I  R W�����
�� .corecnte****       ****� o   R S���� 0 tm tM��  ��  ��  � m   W X��
�� 
nmbr� o      ���� 
0 tml tML~ ��� r   \ e��� c   \ c��� l  \ a���~� I  \ a�}��|
�} .corecnte****       ****� o   \ ]�{�{ 0 tm tM�|  �  �~  � m   a b�z
�z 
nmbr� o      �y�y 
0 tdl tDL� ��� l  f f�x�w�v�x  �w  �v  � ��� Z   f u���u�t� l  f i��s�r� =   f i��� o   f g�q�q 
0 tml tML� m   g h�p�p �s  �r  � r   l q��� b   l o��� m   l m�� ���  0� o   m n�o�o 0 tm tM� o      �n�n 0 tm tM�u  �t  � ��� l  v v�m�l�k�m  �l  �k  � ��� Z   v ����j�i� l  v y��h�g� =   v y��� o   v w�f�f 
0 tdl tDL� m   w x�e�e �h  �g  � r   | ���� b   | ���� m   | �� ���  0� o    ��d�d 0 td tD� o      �c�c 0 td tD�j  �i  � ��� l  � ��b�a�`�b  �a  �`  � ��� l  � ��_�^�]�_  �^  �]  � ��� l  � ��\���\  �   Time variables	   � ���     T i m e   v a r i a b l e s 	� ��� l  � ��[���[  �  	 Get hour   � ���    G e t   h o u r� ��� r   � ���� n   � ���� 1   � ��Z
�Z 
tstr� l  � ���Y�X� I  � ��W�V�U
�W .misccurdldt    ��� null�V  �U  �Y  �X  � o      �T�T 0 timestr timeStr� ��� r   � ���� I  � ��S�R�
�S .sysooffslong    ��� null�R  � �Q��
�Q 
psof� m   � ��� ���  :� �P��O
�P 
psin� o   � ��N�N 0 timestr timeStr�O  � o      �M�M 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � ��L��
�L 
cha � m   � ��K�K � l  � ���J�I� \   � ���� o   � ��H�H 0 pos  � m   � ��G�G �J  �I  � o   � ��F�F 0 timestr timeStr� m   � ��E
�E 
TEXT� o      �D�D 0 h  � ��� r   � ���� c   � ���� n   � ���� 7 � ��C��
�C 
cha � l  � ���B�A� [   � ���� o   � ��@�@ 0 pos  � m   � ��?�? �B  �A  �  ;   � �� o   � ��>�> 0 timestr timeStr� m   � ��=
�= 
TEXT� o      �<�< 0 timestr timeStr� ��� l  � ��;�:�9�;  �:  �9  � ��� l  � ��8���8  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ��7�6�
�7 .sysooffslong    ��� null�6  � �5��
�5 
psof� m   � ��� ���  :� �4��3
�4 
psin� o   � ��2�2 0 timestr timeStr�3  � o      �1�1 0 pos  � ��� r   � ���� c   � ���� n   � ���� 7  � ��0��
�0 
cha � m   � ��/�/ � l  � ���.�-� \   � �   o   � ��,�, 0 pos   m   � ��+�+ �.  �-  � o   � ��*�* 0 timestr timeStr� m   � ��)
�) 
TEXT� o      �(�( 0 m  �  r   �	 c   � n   �	 7 ��'

�' 
cha 
 l  ��&�% [   � o   � �$�$ 0 pos   m   �#�# �&  �%    ;  	 o   � ��"�" 0 timestr timeStr m  �!
�! 
TEXT o      � �  0 timestr timeStr  l 

����  �  �    l 

��     Get AM or PM    �    G e t   A M   o r   P M  r  
 I 
��
� .sysooffslong    ��� null�   �
� 
psof m   �    ��
� 
psin o  �� 0 timestr timeStr�   o      �� 0 pos    !  r  0"#" c  .$%$ n  ,&'& 7,�()
� 
cha ( l %)*��* [  %)+,+ o  &'�� 0 pos  , m  '(�� �  �  )  ;  *+' o  �� 0 timestr timeStr% m  ,-�
� 
TEXT# o      �� 0 s  ! -.- l 11���
�  �  �
  . /0/ l 11�	���	  �  �  0 121 Z  1j345�3 l 146��6 =  14787 o  12�� 0 isfolder isFolder8 m  23�
� boovtrue�  �  4 k  7L99 :;: l 77�<=�  <   Create folder timestamp   = �>> 0   C r e a t e   f o l d e r   t i m e s t a m p; ?� ? r  7L@A@ b  7JBCB b  7HDED b  7FFGF b  7DHIH b  7@JKJ b  7>LML b  7<NON m  7:PP �QQ  b a c k u p _O o  :;���� 0 ty tYM o  <=���� 0 tm tMK o  >?���� 0 td tDI m  @CRR �SS  _G o  DE���� 0 h  E o  FG���� 0 m  C o  HI���� 0 s  A o      ���� 0 	timestamp  �   5 TUT l ORV����V =  ORWXW o  OP���� 0 isfolder isFolderX m  PQ��
�� boovfals��  ��  U Y��Y k  UfZZ [\[ l UU��]^��  ]   Create timestamp   ^ �__ "   C r e a t e   t i m e s t a m p\ `��` r  Ufaba b  Udcdc b  Ubefe b  U`ghg b  U^iji b  UZklk b  UXmnm o  UV���� 0 ty tYn o  VW���� 0 tm tMl o  XY���� 0 td tDj m  Z]oo �pp  _h o  ^_���� 0 h  f o  `a���� 0 m  d o  bc���� 0 s  b o      ���� 0 	timestamp  ��  ��  �  2 qrq l kk��������  ��  ��  r s��s L  kmtt o  kl���� 0 	timestamp  ��  4  	(boolean)   5 �uu  ( b o o l e a n ). vwv l     ��������  ��  ��  w xyx l     ��������  ��  ��  y z{z l     ��������  ��  ��  { |}| l     ��������  ��  ��  } ~~ l     ��������  ��  ��   ��� l     ������  � q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   � ��� �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .� ��� l     ������  � w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   � ��� �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .� ��� i     #��� I      ������� 0 comparesizes compareSizes� ��� o      ���� 
0 source  � ���� o      ���� 0 destination  ��  ��  � l    ^���� k     ^�� ��� r     ��� m     ��
�� boovtrue� o      ���� 0 fit  � ��� r    
��� 4    ���
�� 
psxf� o    ���� 0 destination  � o      ���� 0 destination  � ��� l   ��������  ��  ��  � ��� O    L��� k    K�� ��� r    ��� I   �����
�� .sysoexecTEXT���     TEXT� b    ��� b    ��� m    �� ���  d u   - m s  � o    ���� 
0 source  � m    �� ���    |   c u t   - f   1��  � o      ���� 0 
foldersize 
folderSize� ��� r    (��� ^    &��� l   $������ I   $�����
�� .sysorondlong        doub� ]     ��� l   ������ ^    ��� o    ���� 0 
foldersize 
folderSize� m    ���� ��  ��  � m    ���� d��  ��  ��  � m   $ %���� d� o      ���� 0 
foldersize 
folderSize� ��� l  ) )��������  ��  ��  � ��� r   ) 7��� ^   ) 5��� ^   ) 3��� ^   ) 1��� l  ) /������ l  ) /������ n   ) /��� 1   - /��
�� 
frsp� 4   ) -���
�� 
cdis� o   + ,���� 0 destination  ��  ��  ��  ��  � m   / 0���� � m   1 2���� � m   3 4���� � o      ���� 0 	freespace 	freeSpace� ��� r   8 C��� ^   8 A��� l  8 ?������ I  8 ?�����
�� .sysorondlong        doub� l  8 ;������ ]   8 ;��� o   8 9���� 0 	freespace 	freeSpace� m   9 :���� d��  ��  ��  ��  ��  � m   ? @���� d� o      ���� 0 	freespace 	freeSpace� ��� l  D D��������  ��  ��  � ���� I  D K�����
�� .ascrcmnt****      � ****� l  D G������ b   D G��� o   D E���� 0 
foldersize 
folderSize� o   E F���� 0 	freespace 	freeSpace��  ��  ��  ��  � m    ���                                                                                  MACS  alis    t  Macintosh HD               �e�H+  ��1
Finder.app                                                     Ѓ���        ����  	                CoreServices    �e��      ���    ��1��0��/  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  � ��� l  M M��������  ��  ��  � ��� Z   M [������� H   M Q�� l  M P������ A   M P��� o   M N���� 0 
foldersize 
folderSize� o   N O���� 0 	freespace 	freeSpace��  ��  � r   T W��� m   T U��
�� boovfals� o      ���� 0 fit  ��  ��  � ��� l  \ \��������  ��  ��  � ���� L   \ ^�� o   \ ]���� 0 fit  ��  �  (string, string)   � ���   ( s t r i n g ,   s t r i n g )� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ����~��  �  �~  � ��� l     �}�|�{�}  �|  �{  � ��� l     �z�y�x�z  �y  �x  � �	 � l     �w		�w  	 m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   	 �		 �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .	  			 i   $ '			 I      �v	�u�v 0 	stopwatch  	 		�t		 o      �s�s 0 mode  �t  �u  	 l    5	
			
 k     5		 			 q      		 �r�q�r 0 x  �q  	 			 Z     3			�p	 l    	�o�n	 =     			 o     �m�m 0 mode  	 m    		 �		 
 s t a r t�o  �n  	 k    		 			 r    			 I    �l	 �k�l 0 gettimestamp getTimestamp	  	!�j	! m    �i
�i boovfals�j  �k  	 o      �h�h 0 x  	 	"�g	" I   �f	#�e
�f .ascrcmnt****      � ****	# l   	$�d�c	$ b    	%	&	% m    	'	' �	(	(   b a c k u p   s t a r t e d :  	& o    �b�b 0 x  �d  �c  �e  �g  	 	)	*	) l   	+�a�`	+ =    	,	-	, o    �_�_ 0 mode  	- m    	.	. �	/	/  f i n i s h�a  �`  	* 	0�^	0 k    /	1	1 	2	3	2 r    '	4	5	4 I    %�]	6�\�] 0 gettimestamp getTimestamp	6 	7�[	7 m     !�Z
�Z boovfals�[  �\  	5 o      �Y�Y 0 x  	3 	8�X	8 I  ( /�W	9�V
�W .ascrcmnt****      � ****	9 l  ( +	:�U�T	: b   ( +	;	<	; m   ( )	=	= �	>	> " b a c k u p   f i n i s h e d :  	< o   ) *�S�S 0 x  �U  �T  �V  �X  �^  �p  	 	?�R	? l  4 4�Q�P�O�Q  �P  �O  �R  	  (string)   	 �	@	@  ( s t r i n g )	 	A	B	A l     �N�M�L�N  �M  �L  	B 	C	D	C l     �K�J�I�K  �J  �I  	D 	E	F	E l     �H�G�F�H  �G  �F  	F 	G	H	G l     �E�D�C�E  �D  �C  	H 	I	J	I l     �B�A�@�B  �A  �@  	J 	K	L	K l     �?	M	N�?  	M M G Utility function to get the duration of the backup process in minutes.   	N �	O	O �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s .	L 	P	Q	P i   ( +	R	S	R I      �>�=�<�> 0 getduration getDuration�=  �<  	S k     	T	T 	U	V	U r     	W	X	W ^     	Y	Z	Y l    	[�;�:	[ \     	\	]	\ o     �9�9 0 endtime endTime	] o    �8�8 0 	starttime 	startTime�;  �:  	Z m    �7�7 <	X o      �6�6 0 duration  	V 	^	_	^ r    	`	a	` ^    	b	c	b l   	d�5�4	d I   �3	e�2
�3 .sysorondlong        doub	e l   	f�1�0	f ]    	g	h	g o    	�/�/ 0 duration  	h m   	 
�.�. d�1  �0  �2  �5  �4  	c m    �-�- d	a o      �,�, 0 duration  	_ 	i�+	i L    	j	j o    �*�* 0 duration  �+  	Q 	k	l	k l     �)�(�'�)  �(  �'  	l 	m	n	m l     �&	o	p�&  	o � �-----------------------------------------------------------------------------------------------------------------------------------------------   	p �	q	q - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	n 	r	s	r l     �%	t	u�%  	t � �-----------------------------------------------------------------------------------------------------------------------------------------------   	u �	v	v - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	s 	w	x	w l     �$�#�"�$  �#  �"  	x 	y	z	y l     �!� ��!  �   �  	z 	{	|	{ l     ����  �  �  	| 	}	~	} l     ����  �  �  	~ 		�	 l     ����  �  �  	� 	�	�	� i   , /	�	�	� I      ���� 0 runonce runOnce�  �  	� I     ���� 0 	plistinit 	plistInit�  �  	� 	�	�	� l     ����  �  �  	� 	�	�	� l  � �	���	� I   � ��
�	��
 0 runonce runOnce�	  �  �  �  	� 	�	�	� l     ����  �  �  	� 	�	�	� l     ����  �  �  	� 	�	�	� l     �	�	��  	� w q Function that is always running in the background. This doesn't need to get called as it is running from the off   	� �	�	� �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f	� 	�	�	� l     � 	�	��   	� � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   	� �	�	��   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .	� 	���	� i   0 3	�	�	� I     ������
�� .miscidlenmbr    ��� null��  ��  	� k     ^	�	� 	�	�	� Z     [	�	�����	� l    	�����	� =     	�	�	� o     ���� 0 isbackingup isBackingUp	� m    ��
�� boovfals��  ��  	� Z    W	�	�����	� l   	�����	� A    	�	�	� l   	�����	� n    	�	�	� 1    ��
�� 
hour	� l   	�����	� I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  	� m    ���� ��  ��  	� k    S	�	� 	�	�	� r    	�	�	� l   	�����	� n    	�	�	� 1    ��
�� 
min 	� l   	�����	� l   	�����	� I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  ��  ��  	� o      ���� 0 m  	� 	�	�	� l   ��������  ��  ��  	� 	���	� Z    S	�	�	���	� G    '	�	�	� l   	�����	� =    	�	�	� o    ���� 0 m  	� m    ���� ��  ��  	� l  " %	�����	� =   " %	�	�	� o   " #���� 0 m  	� m   # $���� -��  ��  	� Z   * 9	�	�����	� l  * -	�����	� =   * -	�	�	� o   * +���� 0 scheduledtime scheduledTime	� m   + ,���� ��  ��  	� I   0 5�������� 0 	plistinit 	plistInit��  ��  ��  ��  	� 	�	�	� G   < G	�	�	� l  < ?	�����	� =   < ?	�	�	� o   < =���� 0 m  	� m   = >����  ��  ��  	� l  B E	�����	� =   B E	�	�	� o   B C���� 0 m  	� m   C D���� ��  ��  	� 	���	� I   J O�������� 0 	plistinit 	plistInit��  ��  ��  ��  ��  ��  ��  ��  ��  	� 	�	�	� l  \ \��������  ��  ��  	� 	���	� L   \ ^	�	� m   \ ]���� <��  ��       ��	�	�	�	�	�	�	�	�	�	�	�	�	�	�	���  	� ������������������������������ 0 	plistinit 	plistInit�� 0 diskinit diskInit�� 0 freespaceinit freeSpaceInit�� 0 
backupinit 
backupInit�� 0 tidyup tidyUp�� 
0 backup  �� 0 itexists itExists�� 0 gettimestamp getTimestamp�� 0 comparesizes compareSizes�� 0 	stopwatch  �� 0 getduration getDuration�� 0 runonce runOnce
�� .miscidlenmbr    ��� null
�� .aevtoappnull  �   � ****	� ��.����	�	����� 0 	plistinit 	plistInit��  ��  	� ���������������� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk�� 0 
backuptime 
backupTime�� 0 thecontents theContents�� 0 thevalue theValue�� 0 backuptimes backupTimes�� 0 selectedtime selectedTime	� ,F����s������������������������������������������������������������������� 	0 plist  �� 0 itexists itExists
�� 
plif
�� 
pcnt
�� 
valL��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive�� 0 scheduledtime scheduledTime
�� 
prmp
�� .sysostflalis    ��� null
�� 
TEXT
�� 
psxp
�� .gtqpchltns    @   @ ns  �� �� �� <
�� 
kocl
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null
�� 
plii
�� 
insh
�� 
kind�� �� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk
�� .ascrcmnt****      � ****�� 0 diskinit diskInit��jjE�O*��l+ e  ,� $*��/�,E�O��,E�O��,E�O��,E�O��,E�OPUY� y*��l E�O*��l �&E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kv�&E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPUO� �*a �a a �la    s*a a !a "*6a a #�a a $�a %a %  O*a a !a "*6a a #�a a &�a %a %  O*a a !a "*6a a #�a a '�a %a %  UUO�E` (O�E` )O�E�O_ (_ )�mvj *O*j+ +	� �Z�~�}	�	��|� 0 diskinit diskInit�~  �}  	� �{�z�{ 0 msg  �z 	0 reply  	� d�y�x�w�v�u�t��s�r�q�p��o�n�m��l�y "0 destinationdisk destinationDisk�x 0 itexists itExists�w 0 freespaceinit freeSpaceInit�v "0 messagesmissing messagesMissing
�u 
cobj
�t 
btns
�s 
dflt�r 
�q .sysodlogaskr        TEXT
�p 
bhit�o 0 diskinit diskInit�n &0 messagescancelled messagesCancelled
�m 
appr
�l .sysonotfnull��� ��� TEXT�| L*��l+ e  *fk+ Y 8��.E�O����lv�l� �,E�O��  
*j+ Y ��.E�O�a a l 	� �k��j�i	�	��h�k 0 freespaceinit freeSpaceInit�j �g	��g 	�  �f�f 	0 again  �i  	� �e�d�c�e 	0 again  �d 
0 reply1  �c 0 msg  	� �b�a�`�_��^���]�\�[�Z����Y�X�W�V�U�b 0 sourcefolder sourceFolder�a "0 destinationdisk destinationDisk�` 0 comparesizes compareSizes�_ 0 
backupinit 
backupInit
�^ 
btns
�] 
dflt�\ 
�[ .sysodlogaskr        TEXT
�Z 
bhit�Y 0 tidyup tidyUp�X &0 messagescancelled messagesCancelled
�W 
cobj
�V 
appr
�U .sysonotfnull��� ��� TEXT�h l*��l+ e  
*j+ Y Y�f  ����lv�l� 
�,E�Y �e  ����lv�l� 
�,E�Y hO��  
*j+ Y _ a .E�O�a a l 	� �T,�S�R	�	��Q�T 0 
backupinit 
backupInit�S  �R  	�  	� �P�O�NDI�MW�L�K�J�I�HU�G�F�Ef�D{���C���B�A
�P 
psxf�O "0 destinationdisk destinationDisk�N 	0 drive  �M 0 itexists itExists
�L 
kocl
�K 
cfol
�J 
insh
�I 
prdt
�H 
pnam�G 
�F .corecrel****      � null
�E 
cdis�D 0 backupfolder backupFolder�C 0 initialbackup initialBackup�B 0 latestfolder latestFolder�A 
0 backup  �Q �*��/E�O*���%l+ f  � *�������l� UY hO� *��/�a /E` OPUO*a �a %l+ f  � *���_ ��a l� UY fE` O� *��/�a /�a /E` OPUO*j+ 	� �@��?�>	�	��=�@ 0 tidyup tidyUp�?  �>  	� �<�;�:�9�8�7�6�< 0 creationdates creationDates�; 0 theoldestdate theOldestDate�: 0 j  �9 0 i  �8 0 thisdate thisDate�7 
0 reply2  �6 0 msg  	� d�5�4�3�2�1�00�/7:�.�-�,�+E�*�)�(�'T�&�%_b�5 0 backupfolder backupFolder
�4 
cobj
�3 
ascd
�2 .corecnte****       ****
�1 .ascrcmnt****      � ****
�0 .coredeloobj        obj 
�/ 
btns
�. 
dflt�- 
�, .sysodlogaskr        TEXT
�+ 
bhit
�* 
trsh
�) .fndremptnull��� ��� obj �( &0 messagescancelled messagesCancelled
�' 
appr
�& .sysonotfnull��� ��� TEXT�% 0 freespaceinit freeSpaceInit�= �� ���-�,E�O��k/E�OkE�O 3k�j kh ��/E�O�� �E�O�E�O��/j Y h[OY��O��/j O����lv�l� �,E�O��  *a ,j Y _ �.E�O�a a l O*ek+ Oa a a l U	� �$��#�"	�	��!�$ 
0 backup  �#  �"  	� 
� ����������  0 t  � 0 x  � "0 containerfolder containerFolder� 0 
foldername 
folderName� 0 d  � 0 duration  � 0 msg  � 0 c  � 0 oldestfolder oldestFolder� 0 todelete toDelete	� D���������������
�	���������[� ^����oq~��������������������������������IS�������� 0 gettimestamp getTimestamp� 0 isbackingup isBackingUp� 0 	stopwatch  
� 
psxf� 0 sourcefolder sourceFolder
� 
TEXT
� 
cfol
� 
pnam� 0 initialbackup initialBackup
� 
kocl
� 
insh� 0 backupfolder backupFolder
�
 
prdt�	 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
�  
appr
�� .sysonotfnull��� ��� TEXT�� "0 destinationdisk destinationDisk
�� .sysoexecTEXT���     TEXT�� 0 endtime endTime�� 0 getduration getDuration�� $0 messagescomplete messagesComplete
�� 
cobj
�� .sysodelanull��� ��� nmbr
�� .ascrcmnt****      � ****�� *0 messagesencouraging messagesEncouraging
�� 
alis
�� 
psxp�!J*ek+  E�OeE�O*�k+ O�)*��/�&E�O*�/�,E�O�f  O*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�/E` O*���_ ��l� Y hO�e  �*j a ,E` Oa a a l O_ a %�%a %�&E�Oa  �%a !%�%a "%j #OfE�O*j a ,E` $O*j+ %E�O_ &a '.E�Oa (�%a )%�%a a *l Okj +Y8�f 1_ a ,%�%a -%�%a .%�&E�O_ a /%�%a 0%�&E�Oa 1�%j 2O*j a ,E` O_ 3a '.E�O�a a 4l Oa 5�%a 6%�%a 7%�%a 8%j #OfE�O*�/a 9&a '-jv  k��/�&E�O�a :,�&E�Oa ;�%j 2Oa <�%E�O�j #O*j a ,E` $O*j+ %E�O_ &a '.E�Oa =�%a >%�%a a ?l Okj +Y =*j a ,E` $O*j+ %E�O_ &a '.E�Oa @�%a A%�%a a Bl Okj +Y hUO*a Ck+ 	� �������	�	����� 0 itexists itExists�� ��	��� 	�  ������ 0 
objecttype 
objectType�� 
0 object  ��  	� ������ 0 
objecttype 
objectType�� 
0 object  	� ����������
�� 
cdis
�� .coredoexnull���     ****
�� 
file
�� 
cfol�� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU	� ��0����	�	����� 0 gettimestamp getTimestamp�� ��	��� 	�  ���� 0 isfolder isFolder��  	� ���������������������������������� 0 isfolder isFolder�� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  	� ����������������������������������������������PRo
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� 
psof
�� 
psin�� 
�� .sysooffslong    ��� null
�� 
cha ��n*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�	� �������	�	����� 0 comparesizes compareSizes�� ��	��� 	�  ������ 
0 source  �� 0 destination  ��  	� ������������ 
0 source  �� 0 destination  �� 0 fit  �� 0 
foldersize 
folderSize�� 0 	freespace 	freeSpace	� �������������������
�� 
psxf
�� .sysoexecTEXT���     TEXT�� �� d
�� .sysorondlong        doub
�� 
cdis
�� 
frsp
�� .ascrcmnt****      � ****�� _eE�O*�/E�O� >�%�%j E�O��!� j �!E�O*�/�,�!�!�!E�O�� j �!E�O��%j 
UO�� fE�Y hO�	� ��	����	�	����� 0 	stopwatch  �� ��	��� 	�  ���� 0 mode  ��  	� ������ 0 mode  �� 0 x  	� 	��	'��	.	=�� 0 gettimestamp getTimestamp
�� .ascrcmnt****      � ****�� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP	� ��	S����
 
���� 0 getduration getDuration��  ��  
  ���� 0 duration  
 ������������ 0 endtime endTime�� 0 	starttime 	startTime�� <�� d
�� .sysorondlong        doub�� ���!E�O�� j �!E�O�	� ��	�����

���� 0 runonce runOnce��  ��  
  
 ���� 0 	plistinit 	plistInit�� *j+  	� ��	�����

��
�� .miscidlenmbr    ��� null��  ��  
 ���� 0 m  
 ����������������������~�� 0 isbackingup isBackingUp
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit� �~ <�� _�f  V*j �,� F*j �,E�O�� 
 �� �& ��  
*j+ 	Y hY �j 
 �� �& 
*j+ 	Y hY hY hO�	� �}
�|�{

�z
�} .aevtoappnull  �   � ****
 k     �
	
	  X



  g

  l

  q

  �

  �

  �

 	��y�y  �|  �{  
  
 0�x�w�v�u�t e�s�r�q y } � � ��p�o � � � � � � � � ��n�m � � � � � � � � ��l � � � � � � � ��k�j�i
�x afdrdlib
�w 
from
�v fldmfldu
�u .earsffdralis        afdr
�t 
psxp�s 	0 plist  �r 0 initialbackup initialBackup�q 0 isbackingup isBackingUp�p �o "0 messagesmissing messagesMissing�n 	�m *0 messagesencouraging messagesEncouraging�l $0 messagescomplete messagesComplete�k �j &0 messagescancelled messagesCancelled�i 0 runonce runOnce�z ����l �,�%E�OeE�OfE�O������vE�Oa a a a a a a a a a vE` Oa a a a a a  a !a "a #a vE` $Oa %a &a 'a (a )a *a +a ,a -vE` .O*j+ /ascr  ��ޭ