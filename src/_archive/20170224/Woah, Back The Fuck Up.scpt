FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 l     �� 5 6��   5 � � To package up as an app, File > Export... changing the file format to Application and ticking the Stay open after run handler box    6 � 7 7   T o   p a c k a g e   u p   a s   a n   a p p ,   F i l e   >   E x p o r t . . .   c h a n g i n g   t h e   f i l e   f o r m a t   t o   A p p l i c a t i o n   a n d   t i c k i n g   t h e   S t a y   o p e n   a f t e r   r u n   h a n d l e r   b o x 4  8 9 8 l     ��������  ��  ��   9  : ; : l     ��������  ��  ��   ;  < = < l     ��������  ��  ��   =  > ? > l     ��������  ��  ��   ?  @ A @ l     ��������  ��  ��   A  B C B l     �� D E��   D . ( Properties and includes for the menubar    E � F F P   P r o p e r t i e s   a n d   i n c l u d e s   f o r   t h e   m e n u b a r C  G H G x     �� I J��   I 1      ��
�� 
ascr J �� K��
�� 
minv K m       L L � M M  2 . 4��   H  N O N x    �� P����   P 2  	 ��
�� 
osax��   O  Q R Q x     �� S����   S 4    �� T
�� 
frmk T m     U U � V V  F o u n d a t i o n��   R  W X W x     -�� Y����   Y 4   " &�� Z
�� 
frmk Z m   $ % [ [ � \ \  A p p K i t��   X  ] ^ ] l     ��������  ��  ��   ^  _ ` _ j   - /�� a�� 0 
statusitem 
StatusItem a m   - .��
�� 
msng `  b c b l     ��������  ��  ��   c  d e d j   0 2�� f�� 0 
thedisplay 
theDisplay f m   0 1 g g � h h   e  i j i j   3 9�� k�� 0 defaults   k 4   3 8�� l
�� 
pcls l m   5 6 m m � n n  N S U s e r D e f a u l t s j  o p o j   : @�� q�� $0 internalmenuitem internalMenuItem q 4   : ?�� r
�� 
pcls r m   < = s s � t t  N S M e n u I t e m p  u v u j   A I�� w�� $0 externalmenuitem externalMenuItem w 4   A H�� x
�� 
pcls x m   C F y y � z z  N S M e n u I t e m v  { | { j   J R�� }�� 0 newmenu newMenu } 4   J Q�� ~
�� 
pcls ~ m   L O   � � �  N S M e n u |  � � � l     ��������  ��  ��   �  � � � j   S d�� ��� 0 thelist theList � J   S c � �  � � � m   S V � � � � �  B a c k u p   n o w �  � � � m   V Y � � � � � " T u r n   o n   t h e   b a n t s �  � � � m   Y \ � � � � � , T u r n   o f f   n o t i f i c a t i o n s �  ��� � m   \ _ � � � � �  Q u i t��   �  � � � l     ��������  ��  ��   �  � � � j   e g�� ��� 0 nsfw   � m   e f��
�� boovfals �  � � � j   h j�� ��� 0 notifications   � m   h i��
�� boovtrue �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �  property iconCounter : 0    � � � � 0 p r o p e r t y   i c o n C o u n t e r   :   0 �  � � � l     �� � ���   �  property iconMax : 10    � � � � * p r o p e r t y   i c o n M a x   :   1 0 �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � A ; Global variable declaration for use in different functions    � � � � v   G l o b a l   v a r i a b l e   d e c l a r a t i o n   f o r   u s e   i n   d i f f e r e n t   f u n c t i o n s �  � � � p   k k � � �� ��� 0 sourcefolder sourceFolder � �� ��� "0 destinationdisk destinationDisk � �� ��� 	0 drive   � �� ��� 0 machinefolder machineFolder � �� ��� 0 backupfolder backupFolder � �� ��� (0 activesourcefolder activeSourceFolder � �� ��� (0 activebackupfolder activeBackupFolder � ������ 0 latestfolder latestFolder��   �  � � � p   k k � � �� ��� 0 scheduledtime scheduledTime � �� ��� 0 	starttime 	startTime � ������ 0 endtime endTime��   �  � � � l     ��������  ��  ��   �  � � � l     ����~��  �  �~   �  � � � l     �} � ��}   �   Property assignment    � � � � (   P r o p e r t y   a s s i g n m e n t �  � � � j   k ��| ��| 	0 plist   � b   k � � � � n  k | � � � 1   x |�{
�{ 
psxp � l  k x ��z�y � I  k x�x � �
�x .earsffdralis        afdr � m   k n�w
�w afdrdlib � �v ��u
�v 
from � m   q t�t
�t fldmfldu�u  �z  �y   � m   |  � � � � � f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t �  � � � l     �s�r�q�s  �r  �q   �  � � � j   � ��p ��p 0 initialbackup initialBackup � m   � ��o
�o boovtrue �  � � � j   � ��n ��n 0 manualbackup manualBackup � m   � ��m
�m boovfals �  � � � j   � ��l ��l 0 isbackingup isBackingUp � m   � ��k
�k boovfals �  � � � l     �j�i�h�j  �i  �h   �  � � � j   � ��g ��g "0 messagesmissing messagesMissing � J   � � � �  � � � m   � � � � � � � t T h i s   i s   a w k w a r d   b e c a u s e   y o u r   h a r d   d r i v e   i s n ' t   c o n n e c t e d . . . �  � � � m   � � � � � � � V E r m m m ,   c o n n e c t y   h a r d   d r i v e y   t o   c o m p u t e r y . . . �  � � � m   � � � � � � � ` O i !   W h e r e ' s   t h e   h a r d   d r i v e ? !   I t ' s   n o t   c o n n e c t e d ! �  � � � m   � � � � � � � < H e l l o ?   H a r d   d r i v e ' s   n o t   t h e r e ? �  ��f � m   � �   � � I s   t h i s   y o u r   f i r s t   d a y   o n   t h e   j o b ?   Y o u   n e e d   a   h a r d   d r i v e   t o   b a c k u p   t o . . .�f   �  l     �e�d�c�e  �d  �c    j   � ��b�b *0 messagesencouraging messagesEncouraging J   � � 	 m   � �

 �  C o m e   o n !	  m   � � � 4 C o m e   o n !   E y e   o f   t h e   t i g e r !  m   � � � " N o   p a i n !   N o   p a i n !  m   � � � 0 L e t ' s   d o   t h i s   a s   a   t e a m !  m   � � �  W e   c a n   d o   i t !  m   � � �  F r e e e e e e e e e d o m !  !  m   � �"" �## 2 A l t o g e t h e r   o r   n o t   a t   a l l !! $%$ m   � �&& �'' H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !% (�a( m   � �)) �** H L e t ' s   p u n c h   t h i s   b a c k u p   i n   t h e   f a c e !�a   +,+ l     �`�_�^�`  �_  �^  , -.- j   � ��]/�] $0 messagescomplete messagesComplete/ J   � �00 121 m   � �33 �44  N i c e   o n e !2 565 m   � �77 �88 * Y o u   a b s o l u t e   l e g   e n d !6 9:9 m   � �;; �<<  G o o d   l a d !: =>= m   � �?? �@@  P e r f i c k !> ABA m   � �CC �DD  H a p p y   d a y s !B EFE m   � �GG �HH  F r e e e e e e e e e d o m !F IJI m   � �KK �LL H Y A A A A A A S S S S S S S S S S S S S S S S S S S ! ! ! ! ! ! ! ! ! !J MNM m   � �OO �PP B Y o u ' v e   g o n e   u p   i n   m y   e s t i m a t i o n s !N Q�\Q m   � �RR �SS d Y o u ' v e   j u s t   e a r n e d   y o u r s e l f   a   g o l d   s t a r   m y   f r i e n d !�\  . TUT l     �[�Z�Y�[  �Z  �Y  U VWV j   ��XX�X &0 messagescancelled messagesCancelledX J   � YY Z[Z m   � �\\ �]]  T h a t ' s   a   s h a m e[ ^_^ m   � �`` �aa  A h   m a n ,   u n l u c k y_ bcb m   � �dd �ee  O h   w e l lc fgf m   � �hh �ii @ L e t ' s   i m a g i n e   w h a t   c o u l d ' v e   b e e ng jkj m   � �ll �mm , W e l l   t h a t ' s   a   l e t   d o w nk non m   � �pp �qq P O h   s o   y o u   h a v e   b e t t e r   t h i n g s   t o   d o   t h e n ?o rsr m   � �tt �uu  A r r o g a n ts v�Wv m   � �ww �xx > T h a n k s   f o r   t h a t .   Y o u   o w e   m e   � 2 0�W  W yzy l     �V�U�T�V  �U  �T  z {|{ j  &�S}�S 40 messagesalreadybackingup messagesAlreadyBackingUp} J  #~~ � m  �� ���  T h a t ' s   a   s h a m e� ��� m  
�� ��� . T h o u g h t   y o u   w e r e   c l e v e r� ��� m  
�� ��� > T h o u g h t   y o u ' r e   b e t t e r   t h a n   t h a t� ��� m  �� ���  D i c k� ��� m  �� ���  F u c k t a r d� ��� m  �� ��� J S o   y o u ' v e   j s u t   g o n e   i n   m y   e s t i m a t i o n s� ��� m  �� ���  A r r o g a n t� ��� m  �� ��� $ C h i l l   t h e   f u c k   o u t� ��R� m  �� ���  H o l d   f i r e�R  | ��� l     �Q�P�O�Q  �P  �O  � ��� j  '-�N��N 0 strawake strAwake� m  '*�� ��� * " C u r r e n t P o w e r S t a t e " = 4� ��� j  .4�M��M 0 strsleep strSleep� m  .1�� ��� * " C u r r e n t P o w e r S t a t e " = 1� ��� j  5?�L��L &0 displaysleepstate displaySleepState� I 5<�K��J
�K .sysoexecTEXT���     TEXT� m  58�� ��� j i o r e g   - n   I O D i s p l a y W r a n g l e r   | g r e p   - i   I O P o w e r M a n a g e m e n t�J  � ��� l     �I�H�G�I  �H  �G  � ��� l     �F�E�D�F  �E  �D  � ��� l     �C�B�A�C  �B  �A  � ��� l     �@���@  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �?���?  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �>���>  � � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------   � ��� - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �=�<�;�=  �<  �;  � ��� l     �:���:  � 6 0 Function to check for a .plist preferences file   � ��� `   F u n c t i o n   t o   c h e c k   f o r   a   . p l i s t   p r e f e r e n c e s   f i l e� ��� l     �9���9  � m g If one isn't found it creates one and assigns folder, disk and interval preferences to the .plist file   � ��� �   I f   o n e   i s n ' t   f o u n d   i t   c r e a t e s   o n e   a n d   a s s i g n s   f o l d e r ,   d i s k   a n d   i n t e r v a l   p r e f e r e n c e s   t o   t h e   . p l i s t   f i l e� ��� l     �8���8  � b \ If a .plist file is present, it assigns preferences to their corresponding global variables   � ��� �   I f   a   . p l i s t   f i l e   i s   p r e s e n t ,   i t   a s s i g n s   p r e f e r e n c e s   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s� ��� i  @C��� I      �7�6�5�7 0 	plistinit 	plistInit�6  �5  � k    ��� ��� q      �� �4��4 0 
foldername 
folderName� �3��3 0 
backupdisk 
backupDisk� �2��2  0 computerfolder computerFolder� �1�0�1 0 
backuptime 
backupTime�0  � ��� r     ��� m     �/�/  � o      �.�. 0 
backuptime 
backupTime� ��� l   �-�,�+�-  �,  �+  � ��� Z   ����*�� l   ��)�(� =    ��� I    �'��&�' 0 itexists itExists� ��� m    �� ���  f i l e� ��%� o    �$�$ 	0 plist  �%  �&  � m    �#
�# boovtrue�)  �(  � O    E��� k    D�� ��� r    $��� n    "   1     "�"
�" 
pcnt 4     �!
�! 
plif o    � �  	0 plist  � o      �� 0 thecontents theContents�  r   % * n   % ( 1   & (�
� 
valL o   % &�� 0 thecontents theContents o      �� 0 thevalue theValue 	
	 l  + +����  �  �  
  r   + 0 n   + . o   , .��  0 foldertobackup FolderToBackup o   + ,�� 0 thevalue theValue o      �� 0 
foldername 
folderName  r   1 6 n   1 4 o   2 4�� 0 backupdrive BackupDrive o   1 2�� 0 thevalue theValue o      �� 0 
backupdisk 
backupDisk  r   7 < n   7 : o   8 :��  0 computerfolder computerFolder o   7 8�� 0 thevalue theValue o      ��  0 computerfolder computerFolder  r   = B  n   = @!"! o   > @�� 0 scheduledtime scheduledTime" o   = >�� 0 thevalue theValue  o      �� 0 
backuptime 
backupTime #$# l  C C���
�  �  �
  $ %�	% l  C C�&'�  & . (log {folderName, backupDisk, backupTime}   ' �(( P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }�	  � m    ))�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �*  � k   H�** +,+ r   H O-.- I   H M���� .0 getcomputeridentifier getComputerIdentifier�  �  . o      ��  0 computerfolder computerFolder, /0/ l  P P����  �  �  0 121 O   P �343 t   T �565 k   V �77 898 I   V [� �����  0 setfocus setFocus��  ��  9 :;: l  \ \��������  ��  ��  ; <=< r   \ e>?> l  \ c@����@ I  \ c����A
�� .sysostflalis    ��� null��  A ��B��
�� 
prmpB m   ^ _CC �DD @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p��  ��  ��  ? o      ���� 0 
foldername 
folderName= EFE r   f uGHG c   f sIJI l  f oK����K I  f o����L
�� .sysostflalis    ��� null��  L ��M��
�� 
prmpM m   h kNN �OO R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o��  ��  ��  J m   o r��
�� 
TEXTH o      ���� 0 
backupdisk 
backupDiskF PQP l  v v��������  ��  ��  Q RSR r   v }TUT n   v {VWV 1   w {��
�� 
psxpW o   v w���� 0 
foldername 
folderNameU o      ���� 0 
foldername 
folderNameS XYX r   ~ �Z[Z n   ~ �\]\ 1    ���
�� 
psxp] o   ~ ���� 0 
backupdisk 
backupDisk[ o      ���� 0 
backupdisk 
backupDiskY ^_^ l  � ���������  ��  ��  _ `a` r   � �bcb J   � �dd efe m   � �gg �hh 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u rf iji m   � �kk �ll 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u rj m��m m   � �nn �oo , E v e r y   h o u r   o n   t h e   h o u r��  c o      ���� 0 backuptimes backupTimesa pqp r   � �rsr c   � �tut J   � �vv w��w I  � ���xy
�� .gtqpchltns    @   @ ns  x o   � ����� 0 backuptimes backupTimesy ��z��
�� 
prmpz m   � �{{ �|| 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?��  ��  u m   � ���
�� 
TEXTs o      ���� 0 selectedtime selectedTimeq }~} l  � ������    log (selectedTime)   � ��� $ l o g   ( s e l e c t e d T i m e )~ ��� l  � ���������  ��  ��  � ��� Z   � ������� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r��  ��  � r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r��  ��  � ��� r   � ���� m   � ����� � o      ���� 0 
backuptime 
backupTime� ��� l  � ������� =   � ���� o   � ����� 0 selectedtime selectedTime� m   � ��� ��� , E v e r y   h o u r   o n   t h e   h o u r��  ��  � ���� r   � ���� m   � ����� <� o      ���� 0 
backuptime 
backupTime��  ��  � ��� l  � ���������  ��  ��  � ���� l  � �������  � . (log {folderName, backupDisk, backupTime}   � ��� P l o g   { f o l d e r N a m e ,   b a c k u p D i s k ,   b a c k u p T i m e }��  6 m   T U����  ��4 m   P Q���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  2 ��� l  � ���������  ��  ��  � ���� O   ����� O   ����� k   ���� ��� I  �"�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   ���
�� 
plii� ����
�� 
insh�  ;  � �����
�� 
prdt� K  
�� ����
�� 
kind� m  ��
�� 
TEXT� ����
�� 
pnam� m  �� ���  F o l d e r T o B a c k u p� �����
�� 
valL� o  ���� 0 
foldername 
folderName��  ��  � ��� I #J�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  '*��
�� 
plii� ����
�� 
insh�  ;  -/� �����
�� 
prdt� K  2D�� ����
�� 
kind� m  58��
�� 
TEXT� ����
�� 
pnam� m  ;>�� ���  B a c k u p D r i v e� �����
�� 
valL� o  ?@���� 0 
backupdisk 
backupDisk��  ��  � ��� I Kr�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  OR��
�� 
plii� ����
�� 
insh�  ;  UW� �����
�� 
prdt� K  Zl�� ����
�� 
kind� m  ]`��
�� 
TEXT� ����
�� 
pnam� m  cf�� ���  C o m p u t e r F o l d e r� �����
�� 
valL� o  gh����  0 computerfolder computerFolder��  ��  � ���� I s������
�� .corecrel****      � null��  � ����
�� 
kocl� m  wz��
�� 
plii� ����
�� 
insh�  ;  }� �����
�� 
prdt� K  ���� ����
�� 
kind� m  ����
�� 
TEXT� ����
�� 
pnam� m  ���� ���  S c h e d u l e d T i m e� �����
�� 
valL� o  ������ 0 
backuptime 
backupTime��  ��  ��  � l  � ������� I  � �����
�� .corecrel****      � null�  � �~��
�~ 
kocl� m   � ��}
�} 
plif� �|��{
�| 
prdt� K   � ��� �z��y
�z 
pnam� o   � ��x�x 	0 plist  �y  �{  ��  ��  � m   � ����                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��  � ��� l ���w�v�u�w  �v  �u  � ��� r  ��� � o  ���t�t 0 
foldername 
folderName  o      �s�s 0 sourcefolder sourceFolder�  r  �� o  ���r�r 0 
backupdisk 
backupDisk o      �q�q "0 destinationdisk destinationDisk  r  �� o  ���p�p  0 computerfolder computerFolder o      �o�o 0 machinefolder machineFolder 	
	 r  �� o  ���n�n 0 
backuptime 
backupTime o      �m�m 0 scheduledtime scheduledTime
  I ���l�k
�l .ascrcmnt****      � **** J  ��  o  ���j�j 0 sourcefolder sourceFolder  o  ���i�i "0 destinationdisk destinationDisk  o  ���h�h 0 machinefolder machineFolder �g o  ���f�f 0 scheduledtime scheduledTime�g  �k    l ���e�d�c�e  �d  �c   �b I  ���a�`�_�a 0 diskinit diskInit�`  �_  �b  �  l     �^�]�\�^  �]  �\    l     �[�Z�Y�[  �Z  �Y     l     �X�W�V�X  �W  �V    !"! l     �U�T�S�U  �T  �S  " #$# l     �R�Q�P�R  �Q  �P  $ %&% l     �O'(�O  ' H B Function to detect if the selected hard drive is connected or not   ( �)) �   F u n c t i o n   t o   d e t e c t   i f   t h e   s e l e c t e d   h a r d   d r i v e   i s   c o n n e c t e d   o r   n o t& *+* l     �N,-�N  , T N This only happens once a hard drive has been selected and provides a reminder   - �.. �   T h i s   o n l y   h a p p e n s   o n c e   a   h a r d   d r i v e   h a s   b e e n   s e l e c t e d   a n d   p r o v i d e s   a   r e m i n d e r+ /0/ i  DG121 I      �M�L�K�M 0 diskinit diskInit�L  �K  2 Z     �34�J53 l    	6�I�H6 =     	787 I     �G9�F�G 0 itexists itExists9 :;: m    << �==  d i s k; >�E> o    �D�D "0 destinationdisk destinationDisk�E  �F  8 m    �C
�C boovtrue�I  �H  4 I    �B?�A�B 0 freespaceinit freeSpaceInit? @�@@ m    �?
�? boovfals�@  �A  �J  5 k    �AA BCB I    �>�=�<�> 0 setfocus setFocus�=  �<  C DED r    $FGF n    "HIH 3     "�;
�; 
cobjI o     �:�: "0 messagesmissing messagesMissingG o      �9�9 0 msg  E JKJ r   % 5LML l  % 3N�8�7N n   % 3OPO 1   1 3�6
�6 
bhitP l  % 1Q�5�4Q I  % 1�3RS
�3 .sysodlogaskr        TEXTR o   % &�2�2 0 msg  S �1TU
�1 
btnsT J   ' +VV WXW m   ' (YY �ZZ  C a n c e l   B a c k u pX [�0[ m   ( )\\ �]]  O K�0  U �/^�.
�/ 
dflt^ m   , -�-�- �.  �5  �4  �8  �7  M o      �,�, 	0 reply  K _�+_ Z   6 �`a�*b` l  6 9c�)�(c =   6 9ded o   6 7�'�' 	0 reply  e m   7 8ff �gg  O K�)  �(  a I   < A�&�%�$�& 0 diskinit diskInit�%  �$  �*  b Z   D �hi�#�"h l  D Oj�!� j E   D Oklk o   D I�� &0 displaysleepstate displaySleepStatel o   I N�� 0 strawake strAwake�!  �   i k   R �mm non r   R [pqp n   R Yrsr 3   W Y�
� 
cobjs o   R W�� &0 messagescancelled messagesCancelledq o      �� 0 msg  o t�t Z   \ �uv��u l  \ cw��w =   \ cxyx o   \ a�� 0 notifications  y m   a b�
� boovtrue�  �  v Z   f �z{|�z l  f m}��} =   f m~~ o   f k�� 0 nsfw   m   k l�
� boovtrue�  �  { I  p y���
� .sysonotfnull��� ��� TEXT� o   p q�� 0 msg  � ���
� 
appr� m   r u�� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  | ��� l  | ���
�	� =   | ���� o   | ��� 0 nsfw  � m   � ��
� boovfals�
  �	  � ��� I  � ����
� .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� ���
� 
appr� m   � ��� ��� 
 W B T F U�  �  �  �  �  �  �#  �"  �+  0 ��� l     ��� �  �  �   � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � � � Function that gets the size of the source folder and the destination drive and compares the 2 to make sure the drive has enough capacity   � ���   F u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   d e s t i n a t i o n   d r i v e   a n d   c o m p a r e s   t h e   2   t o   m a k e   s u r e   t h e   d r i v e   h a s   e n o u g h   c a p a c i t y� ��� l     ������  � s m There's a paramater called 'again' as the function can be called again but provides a different dialogue box   � ��� �   T h e r e ' s   a   p a r a m a t e r   c a l l e d   ' a g a i n '   a s   t h e   f u n c t i o n   c a n   b e   c a l l e d   a g a i n   b u t   p r o v i d e s   a   d i f f e r e n t   d i a l o g u e   b o x� ��� l     ������  � � � This function checks to make sure there's enough free space on the drive and either starts the backup or asks the user to either automatically delete the oldest backup or to cancel the whole process entirely   � ����   T h i s   f u n c t i o n   c h e c k s   t o   m a k e   s u r e   t h e r e ' s   e n o u g h   f r e e   s p a c e   o n   t h e   d r i v e   a n d   e i t h e r   s t a r t s   t h e   b a c k u p   o r   a s k s   t h e   u s e r   t o   e i t h e r   a u t o m a t i c a l l y   d e l e t e   t h e   o l d e s t   b a c k u p   o r   t o   c a n c e l   t h e   w h o l e   p r o c e s s   e n t i r e l y� ��� i  HK��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � Z     ������� l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    ��� ��� I    �������� 0 setfocus setFocus��  ��  � ��� l   ��������  ��  ��  � ��� Z    M������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r     0��� l    .������ n     .��� 1   , .��
�� 
bhit� l    ,������ I    ,����
�� .sysodlogaskr        TEXT� m     !�� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   " &�� ��� m   " #�� ���  Y e s� ���� m   # $�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   ' (���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  � ��� l  3 6������ =   3 6��� o   3 4���� 	0 again  � m   4 5��
�� boovtrue��  ��  � ���� r   9 I��� l  9 G������ n   9 G��� 1   E G��
�� 
bhit� l  9 E������ I  9 E����
�� .sysodlogaskr        TEXT� m   9 :�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J   ; ?�� ��� m   ; <�� ���  Y e s� ���� m   < =�� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   @ A���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  ��  ��  � ��� l  N N��������  ��  ��  � ���� Z   N ������� l  N S������ =   N S� � o   N O���� 
0 reply1    m   O R �  Y e s��  ��  � I   V [�������� 0 tidyup tidyUp��  ��  ��  � k   ^ �  Z   ^ {���� l  ^ i���� E   ^ i	
	 o   ^ c���� &0 displaysleepstate displaySleepState
 o   c h���� 0 strawake strAwake��  ��   r   l w n   l u 3   q u��
�� 
cobj o   l q���� &0 messagescancelled messagesCancelled o      ���� 0 msg  ��  ��    l  | |��������  ��  ��   �� Z   | ����� l  | ����� =   | � o   | ����� 0 notifications   m   � ���
�� boovtrue��  ��   Z   � ��� l  � ����� =   � � o   � ����� 0 nsfw   m   � ���
�� boovtrue��  ��   I  � ���
�� .sysonotfnull��� ��� TEXT o   � ����� 0 msg   ����
�� 
appr m   � �   �!! > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��   "#" l  � �$����$ =   � �%&% o   � ����� 0 nsfw  & m   � ���
�� boovfals��  ��  # '��' I  � ���()
�� .sysonotfnull��� ��� TEXT( m   � �** �++ , Y o u   s t o p p e d   b a c k i n g   u p) ��,��
�� 
appr, m   � �-- �.. 
 W B T F U��  ��  ��  ��  ��  ��  ��  � /0/ l     ��������  ��  ��  0 121 l     ��������  ��  ��  2 343 l     ��������  ��  ��  4 565 l     ����~��  �  �~  6 787 l     �}�|�{�}  �|  �{  8 9:9 l     �z;<�z  ;,& Function that intialises the backup process and creates the 'Backup' folder on the destination drive if not already there and the 'Latest' folder in the 'Backup' folder if not already there as well. The latter folder is where the most up to date version of the source folder will be copied to.   < �==L   F u n c t i o n   t h a t   i n t i a l i s e s   t h e   b a c k u p   p r o c e s s   a n d   c r e a t e s   t h e   ' B a c k u p '   f o l d e r   o n   t h e   d e s t i n a t i o n   d r i v e   i f   n o t   a l r e a d y   t h e r e   a n d   t h e   ' L a t e s t '   f o l d e r   i n   t h e   ' B a c k u p '   f o l d e r   i f   n o t   a l r e a d y   t h e r e   a s   w e l l .   T h e   l a t t e r   f o l d e r   i s   w h e r e   t h e   m o s t   u p   t o   d a t e   v e r s i o n   o f   t h e   s o u r c e   f o l d e r   w i l l   b e   c o p i e d   t o .: >?> l     �y@A�y  @ g a These folders, if required, are then set to their corresponding global variables declared above.   A �BB �   T h e s e   f o l d e r s ,   i f   r e q u i r e d ,   a r e   t h e n   s e t   t o   t h e i r   c o r r e s p o n d i n g   g l o b a l   v a r i a b l e s   d e c l a r e d   a b o v e .? CDC i  LOEFE I      �x�w�v�x 0 
backupinit 
backupInit�w  �v  F k     �GG HIH r     JKJ 4     �uL
�u 
psxfL o    �t�t "0 destinationdisk destinationDiskK o      �s�s 	0 drive  I MNM l   �rOP�r  O ' !log (destinationDisk & "Backups")   P �QQ B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )N RSR l   �q�p�o�q  �p  �o  S TUT Z    ,VW�n�mV l   X�l�kX =    YZY I    �j[�i�j 0 itexists itExists[ \]\ m    	^^ �__  f o l d e r] `�h` b   	 aba o   	 
�g�g "0 destinationdisk destinationDiskb m   
 cc �dd  B a c k u p s�h  �i  Z m    �f
�f boovfals�l  �k  W O    (efe I   '�e�dg
�e .corecrel****      � null�d  g �chi
�c 
koclh m    �b
�b 
cfoli �ajk
�a 
inshj o    �`�` 	0 drive  k �_l�^
�_ 
prdtl K    #mm �]n�\
�] 
pnamn m     !oo �pp  B a c k u p s�\  �^  f m    qq�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �n  �m  U rsr l  - -�[�Z�Y�[  �Z  �Y  s tut Z   - dvw�X�Wv l  - >x�V�Ux =   - >yzy I   - <�T{�S�T 0 itexists itExists{ |}| m   . /~~ �  f o l d e r} ��R� b   / 8��� b   / 4��� o   / 0�Q�Q "0 destinationdisk destinationDisk� m   0 3�� ���  B a c k u p s /� o   4 7�P�P 0 machinefolder machineFolder�R  �S  z m   < =�O
�O boovfals�V  �U  w O   A `��� I  E _�N�M�
�N .corecrel****      � null�M  � �L��
�L 
kocl� m   G H�K
�K 
cfol� �J��
�J 
insh� n   I T��� 4   O T�I�
�I 
cfol� m   P S�� ���  B a c k u p s� 4   I O�H�
�H 
cdis� o   M N�G�G 	0 drive  � �F��E
�F 
prdt� K   U [�� �D��C
�D 
pnam� o   V Y�B�B 0 machinefolder machineFolder�C  �E  � m   A B���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �X  �W  u ��� l  e e�A�@�?�A  �@  �?  � ��� O   e ���� k   i �� ��� l  i i�>���>  � 8 2set backupFolder to folder "Backups" of disk drive   � ��� d s e t   b a c k u p F o l d e r   t o   f o l d e r   " B a c k u p s "   o f   d i s k   d r i v e� ��� r   i }��� n   i y��� 4   t y�=�
�= 
cfol� o   u x�<�< 0 machinefolder machineFolder� n   i t��� 4   o t�;�
�; 
cfol� m   p s�� ���  B a c k u p s� 4   i o�:�
�: 
cdis� o   m n�9�9 	0 drive  � o      �8�8 0 backupfolder backupFolder� ��7� l  ~ ~�6���6  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�7  � m   e f���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � ��5�4�3�5  �4  �3  � ��� Z   � ����2�� l  � ���1�0� =   � ���� I   � ��/��.�/ 0 itexists itExists� ��� m   � ��� ���  f o l d e r� ��-� b   � ���� b   � ���� b   � ���� o   � ��,�, "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ��+�+ 0 machinefolder machineFolder� m   � ��� ���  / L a t e s t�-  �.  � m   � ��*
�* boovfals�1  �0  � O   � ���� I  � ��)�(�
�) .corecrel****      � null�(  � �'��
�' 
kocl� m   � ��&
�& 
cfol� �%��
�% 
insh� o   � ��$�$ 0 backupfolder backupFolder� �#��"
�# 
prdt� K   � ��� �!�� 
�! 
pnam� m   � ��� ���  L a t e s t�   �"  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �2  � r   � ���� m   � ��
� boovfals� o      �� 0 initialbackup initialBackup� ��� l  � �����  �  �  � ��� O   � ���� k   � ��� ��� r   � ���� n   � ���� 4   � ���
� 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ���
� 
cfol� o   � ��� 0 machinefolder machineFolder� n   � ���� 4   � ���
� 
cfol� m   � ��� ���  B a c k u p s� 4   � ���
� 
cdis� o   � ��� 	0 drive  � o      �� 0 latestfolder latestFolder� ��� l  � �����  �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�  � m   � ����                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � �����  �  �  � ��� I   � ����� 
0 backup  �  �  �  D    l     �
�	��
  �	  �    l     ����  �  �    l     ����  �  �    l     �� ���  �   ��   	 l     ��������  ��  ��  	 

 l     ����   j d Function that finds the oldest backup folder inside the 'Backup' folder and copies it to the Trash.    � �   F u n c t i o n   t h a t   f i n d s   t h e   o l d e s t   b a c k u p   f o l d e r   i n s i d e   t h e   ' B a c k u p '   f o l d e r   a n d   c o p i e s   i t   t o   t h e   T r a s h .  l     ����   � There is code that automatically and permanently deletes the folder without sending it to the Trash, but just for a level of verification, the code actually used just sends it to the Trash so the user can double check the files that are to be deleted.    ��   T h e r e   i s   c o d e   t h a t   a u t o m a t i c a l l y   a n d   p e r m a n e n t l y   d e l e t e s   t h e   f o l d e r   w i t h o u t   s e n d i n g   i t   t o   t h e   T r a s h ,   b u t   j u s t   f o r   a   l e v e l   o f   v e r i f i c a t i o n ,   t h e   c o d e   a c t u a l l y   u s e d   j u s t   s e n d s   i t   t o   t h e   T r a s h   s o   t h e   u s e r   c a n   d o u b l e   c h e c k   t h e   f i l e s   t h a t   a r e   t o   b e   d e l e t e d .  l     ����   � � If the user is ok with the folder being sent to the Trash, the user can then empty the Trash and any other files in there directly or can cancel the backup altogether.    �P   I f   t h e   u s e r   i s   o k   w i t h   t h e   f o l d e r   b e i n g   s e n t   t o   t h e   T r a s h ,   t h e   u s e r   c a n   t h e n   e m p t y   t h e   T r a s h   a n d   a n y   o t h e r   f i l e s   i n   t h e r e   d i r e c t l y   o r   c a n   c a n c e l   t h e   b a c k u p   a l t o g e t h e r .  l     ����   � � If the Trash is emptied, the freeSpaceInit function is called with the parameter set to 'true' to check the free space to start the backup process once again.    �>   I f   t h e   T r a s h   i s   e m p t i e d ,   t h e   f r e e S p a c e I n i t   f u n c t i o n   i s   c a l l e d   w i t h   t h e   p a r a m e t e r   s e t   t o   ' t r u e '   t o   c h e c k   t h e   f r e e   s p a c e   t o   s t a r t   t h e   b a c k u p   p r o c e s s   o n c e   a g a i n .  i  PS !  I      �������� 0 tidyup tidyUp��  ��  ! k    z"" #$# r     %&% 4     ��'
�� 
psxf' o    ���� "0 destinationdisk destinationDisk& o      ���� 	0 drive  $ ()( l   ��������  ��  ��  ) *��* O   z+,+ k   y-- ./. l   ��01��  0 A ;set creationDates to creation date of items of backupFolder   1 �22 v s e t   c r e a t i o n D a t e s   t o   c r e a t i o n   d a t e   o f   i t e m s   o f   b a c k u p F o l d e r/ 343 r    565 n    787 4    ��9
�� 
cfol9 o    ���� 0 machinefolder machineFolder8 n    :;: 4    ��<
�� 
cfol< m    == �>>  B a c k u p s; 4    ��?
�� 
cdis? o    ���� 	0 drive  6 o      ���� 0 bf bF4 @A@ r    BCB n    DED 1    ��
�� 
ascdE n    FGF 2   ��
�� 
cobjG o    ���� 0 bf bFC o      ���� 0 creationdates creationDatesA HIH r     &JKJ n     $LML 4   ! $��N
�� 
cobjN m   " #���� M o     !���� 0 creationdates creationDatesK o      ���� 0 theoldestdate theOldestDateI OPO r   ' *QRQ m   ' (���� R o      ���� 0 j  P STS l  + +��������  ��  ��  T UVU Y   + nW��XY��W k   9 iZZ [\[ r   9 ?]^] n   9 =_`_ 4   : =��a
�� 
cobja o   ; <���� 0 i  ` o   9 :���� 0 creationdates creationDates^ o      ���� 0 thisdate thisDate\ b��b Z   @ icd����c l  @ He����e >  @ Hfgf n   @ Fhih 1   D F��
�� 
pnami 4   @ D��j
�� 
cobjj o   B C���� 0 i  g m   F Gkk �ll  L a t e s t��  ��  d Z   K emn����m l  K No����o A   K Npqp o   K L���� 0 thisdate thisDateq o   L M���� 0 theoldestdate theOldestDate��  ��  n k   Q arr sts r   Q Tuvu o   Q R���� 0 thisdate thisDatev o      ���� 0 theoldestdate theOldestDatet wxw r   U Xyzy o   U V���� 0 i  z o      ���� 0 j  x {|{ l  Y Y��}~��  } " log (item j of backupFolder)   ~ � 8 l o g   ( i t e m   j   o f   b a c k u p F o l d e r )| ���� I  Y a�����
�� .ascrcmnt****      � ****� l  Y ]������ n   Y ]��� 4   Z ]���
�� 
cobj� o   [ \���� 0 j  � o   Y Z���� 0 bf bF��  ��  ��  ��  ��  ��  ��  ��  ��  �� 0 i  X m   . /���� Y I  / 4�����
�� .corecnte****       ****� o   / 0���� 0 creationdates creationDates��  ��  V ��� l  o o��������  ��  ��  � ��� l  o o������  � ! -- Delete the oldest folder   � ��� 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r� ��� l  o o������  � # delete item j of backupFolder   � ��� : d e l e t e   i t e m   j   o f   b a c k u p F o l d e r� ��� I  o y�����
�� .coredeloobj        obj � n   o u��� 4   p u���
�� 
cobj� l  q t������ [   q t��� o   q r���� 0 j  � m   r s���� ��  ��  � o   o p���� 0 bf bF��  � ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� l   z z������  � � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete   � ���� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e� ��� l  z z��������  ��  ��  � ��� l  z z��������  ��  ��  � ��� I   z �������� 0 setfocus setFocus��  ��  � ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
bhit� l  � ������� I  � �����
�� .sysodlogaskr        TEXT� m   � ��� ���B Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .� ����
�� 
btns� J   � ��� ��� m   � ��� ���  E m p t y   T r a s h� ���� m   � ��� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   � ����� ��  ��  ��  ��  ��  � o      ���� 
0 reply2  � ��� Z   ������� l  � ������� =   � ���� o   � ����� 
0 reply2  � m   � ��� ���  E m p t y   T r a s h��  ��  � I  � ������
�� .fndremptnull��� ��� obj � l  � ������� 1   � ���
�� 
trsh��  ��  ��  ��  � Z   �������� l  � ������ E   � ���� o   � ��~�~ &0 displaysleepstate displaySleepState� o   � ��}�} 0 strawake strAwake��  �  � k   ��� ��� r   � ���� n   � ���� 3   � ��|
�| 
cobj� o   � ��{�{ &0 messagescancelled messagesCancelled� o      �z�z 0 msg  � ��y� Z   ����x�w� l  � ���v�u� =   � ���� o   � ��t�t 0 notifications  � m   � ��s
�s boovtrue�v  �u  � Z   �����r� l  � ���q�p� =   � ���� o   � ��o�o 0 nsfw  � m   � ��n
�n boovtrue�q  �p  � I  � ��m��
�m .sysonotfnull��� ��� TEXT� o   � ��l�l 0 msg  � �k��j
�k 
appr� m   � ��� ��� > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�j  � ��� l  � ���i�h� =   � ���� o   � ��g�g 0 nsfw  � m   � ��f
�f boovfals�i  �h  � ��e� I  ��d��
�d .sysonotfnull��� ��� TEXT� m   � ��� ��� , Y o u   s t o p p e d   b a c k i n g   u p� �c��b
�c 
appr� m   � �� ��� 
 W B T F U�b  �e  �r  �x  �w  �y  ��  ��  � ��� l �a�`�_�a  �`  �_  � ��� I  �^��]�^ 0 freespaceinit freeSpaceInit� ��\� m  �[
�[ boovtrue�\  �]  � ��Z� Z  y� �Y�X� l #�W�V E  # o  �U�U &0 displaysleepstate displaySleepState o  "�T�T 0 strawake strAwake�W  �V    Z  &u�S�R l &-�Q�P =  &- o  &+�O�O 0 notifications   m  +,�N
�N boovtrue�Q  �P   Z  0q	
�M	 l 07�L�K =  07 o  05�J�J 0 nsfw   m  56�I
�I boovtrue�L  �K  
 k  :M  I :G�H
�H .sysonotfnull��� ��� TEXT m  := � � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . . �G�F
�G 
appr m  @C � 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�F   �E I HM�D�C
�D .sysodelanull��� ��� nmbr m  HI�B�B �C  �E    l PW�A�@ =  PW o  PU�?�? 0 nsfw   m  UV�>
�> boovfals�A  �@    �=  k  Zm!! "#" I Zg�<$%
�< .sysonotfnull��� ��� TEXT$ m  Z]&& �'' t D e l e t e d   t h e   o l d e s t   b a c k u p .   E m p t i e d   t h e   t r a s h .   T r y i n g   a g a i n% �;(�:
�; 
appr( m  `c)) �** 
 W B T F U�:  # +�9+ I hm�8,�7
�8 .sysodelanull��� ��� nmbr, m  hi�6�6 �7  �9  �=  �M  �S  �R  �Y  �X  �Z  , m    --�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  ��   ./. l     �5�4�3�5  �4  �3  / 010 l     �2�1�0�2  �1  �0  1 232 l     �/�.�-�/  �.  �-  3 454 l     �,�+�*�,  �+  �*  5 676 l     �)�(�'�)  �(  �'  7 898 l     �&:;�&  : M G Function that carries out the backup process and is split in 2 parts.    ; �<< �   F u n c t i o n   t h a t   c a r r i e s   o u t   t h e   b a c k u p   p r o c e s s   a n d   i s   s p l i t   i n   2   p a r t s .  9 =>= l     �%?@�%  ?*$ It first checks to see if it's the very first backup and just copies the source folder to the 'Latest' folder on the backup drive, otherwise it syncs the source folder to the 'Latest' folder and copies the changed files to the newly created and timestamped backup folder on the backup drive.   @ �AAH   I t   f i r s t   c h e c k s   t o   s e e   i f   i t ' s   t h e   v e r y   f i r s t   b a c k u p   a n d   j u s t   c o p i e s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   o n   t h e   b a c k u p   d r i v e ,   o t h e r w i s e   i t   s y n c s   t h e   s o u r c e   f o l d e r   t o   t h e   ' L a t e s t '   f o l d e r   a n d   c o p i e s   t h e   c h a n g e d   f i l e s   t o   t h e   n e w l y   c r e a t e d   a n d   t i m e s t a m p e d   b a c k u p   f o l d e r   o n   t h e   b a c k u p   d r i v e .> BCB l     �$DE�$  D � � If no changes are found then the timestamped folder that was created is deleted straight away without it being sent to the Trash.   E �FF   I f   n o   c h a n g e s   a r e   f o u n d   t h e n   t h e   t i m e s t a m p e d   f o l d e r   t h a t   w a s   c r e a t e d   i s   d e l e t e d   s t r a i g h t   a w a y   w i t h o u t   i t   b e i n g   s e n t   t o   t h e   T r a s h .C GHG i  TWIJI I      �#�"�!�# 
0 backup  �"  �!  J k    4KK LML q      NN � O�  0 t  O �P� 0 x  P ��� "0 containerfolder containerFolder�  M QRQ r     STS I     �U�� 0 gettimestamp getTimestampU V�V m    �
� boovtrue�  �  T o      �� 0 t  R WXW l  	 	����  �  �  X YZY r   	 [\[ m   	 
�
� boovtrue\ o      �� 0 isbackingup isBackingUpZ ]^] l   ����  �  �  ^ _`_ I    ���� (0 animatemenubaricon animateMenuBarIcon�  �  ` aba l   ���
�  �  �
  b cdc I    �	e��	 0 	stopwatch  e f�f m    gg �hh 
 s t a r t�  �  d iji l   ����  �  �  j klk O   +mnm k   "*oo pqp l  " "�rs�  r f ` Gets the name of the source folder as a duplicate folder with the same name needs to be created   s �tt �   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e dq uvu l  " "�wx�  w x r This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   x �yy �   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u pv z{z r   " *|}| c   " (~~ 4   " &��
� 
psxf� o   $ %� �  0 sourcefolder sourceFolder m   & '��
�� 
TEXT} o      ���� 0 
foldername 
folderName{ ��� r   + 3��� n   + 1��� 1   / 1��
�� 
pnam� 4   + /���
�� 
cfol� o   - .���� 0 
foldername 
folderName� o      ���� 0 
foldername 
folderName� ��� l  4 4������  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  4 4��������  ��  ��  � ��� Z   4 �������� l  4 ;������ =   4 ;��� o   4 9���� 0 initialbackup initialBackup� m   9 :��
�� boovfals��  ��  � k   > ��� ��� I  > L�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   @ A��
�� 
cfol� ����
�� 
insh� o   B C���� 0 backupfolder backupFolder� �����
�� 
prdt� K   D H�� �����
�� 
pnam� o   E F���� 0 t  ��  ��  � ��� l  M M��������  ��  ��  � ��� r   M W��� c   M S��� 4   M Q���
�� 
psxf� o   O P���� 0 sourcefolder sourceFolder� m   Q R��
�� 
TEXT� o      ���� (0 activesourcefolder activeSourceFolder� ��� r   X b��� 4   X ^���
�� 
cfol� o   Z ]���� (0 activesourcefolder activeSourceFolder� o      ���� (0 activesourcefolder activeSourceFolder� ��� l  c c������  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   c |��� n   c x��� 4   u x���
�� 
cfol� o   v w���� 0 t  � n   c u��� 4   p u���
�� 
cfol� o   q t���� 0 machinefolder machineFolder� n   c p��� 4   k p���
�� 
cfol� m   l o�� ���  B a c k u p s� 4   c k���
�� 
cdis� o   g j���� 	0 drive  � o      ���� (0 activebackupfolder activeBackupFolder� ��� l  } }������  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ���� I  } ������
�� .corecrel****      � null��  � ����
�� 
kocl� m    ���
�� 
cfol� ����
�� 
insh� o   � ����� (0 activebackupfolder activeBackupFolder� �����
�� 
prdt� K   � ��� �����
�� 
pnam� o   � ����� 0 
foldername 
folderName��  ��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � o i The script that starts the backup. It scans all the files and essentially syncs the folder to the volume   � ��� �   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e� ��� l  � �������  � q k An atomic version would just to do a straight backup without versioning to see the function at full effect   � ��� �   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t� ��� l  � �������  � D > This means that only new or updated files will be copied over   � ��� |   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r� ��� l  � �������  � ] W Any deleted files in the source folder will be deleted in the destination folder too		   � ��� �   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	� ��� l  � �������  �   Original sync code    � ��� (   O r i g i n a l   s y n c   c o d e  � ��� l  � �������  � z t do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   � ��� �   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Original code   � ���    O r i g i n a l   c o d e� ��� l  � �������  � i c $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   � ��� �   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h�    l  � �����   x r -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.    � �   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .  l  � �����   p j --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.    �		 �   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e . 

 l  � �����   � � If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.    ��   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .  l  � �����   � � The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.    �v   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .  l  � ���������  ��  ��    l  � ���������  ��  ��   �� Z   �*�� l  � ����� =   � � o   � ����� 0 initialbackup initialBackup m   � ���
�� boovtrue��  ��   k   ��  !  r   � �"#" n   � �$%$ 1   � ���
�� 
time% l  � �&����& I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  # o      ���� 0 	starttime 	startTime! '(' Z   � �)*����) l  � �+����+ E   � �,-, o   � ����� &0 displaysleepstate displaySleepState- o   � ����� 0 strawake strAwake��  ��  * Z   � �./����. l  � �0����0 =   � �121 o   � ����� 0 notifications  2 m   � ���
�� boovtrue��  ��  / Z   � �345��3 l  � �6����6 =   � �787 o   � ����� 0 nsfw  8 m   � ���
�� boovtrue��  ��  4 I  � ���9:
�� .sysonotfnull��� ��� TEXT9 m   � �;; �<< � I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e . 
 I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .: ��=��
�� 
appr= m   � �>> �?? 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  5 @A@ l  � �B����B =   � �CDC o   � ����� 0 nsfw  D m   � ���
�� boovfals��  ��  A E��E I  � ���FG
�� .sysonotfnull��� ��� TEXTF m   � �HH �II \ I t ' s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m eG ��J��
�� 
apprJ m   � �KK �LL 
 W B T F U��  ��  ��  ��  ��  ��  ��  ( MNM l   ����~��  �  �~  N OPO l   �}�|�{�}  �|  �{  P QRQ l   �z�y�x�z  �y  �x  R STS l   �w�v�u�w  �v  �u  T UVU r   WXW c   YZY l  [�t�s[ b   \]\ b   ^_^ b   `a` b   bcb b   ded o   �r�r "0 destinationdisk destinationDiske m  ff �gg  B a c k u p s /c o  
�q�q 0 machinefolder machineFoldera m  hh �ii  / L a t e s t /_ o  �p�p 0 
foldername 
folderName] m  jj �kk  /�t  �s  Z m  �o
�o 
TEXTX o      �n�n 0 d  V lml l �mno�m  n D >do shell script "ditto '" & sourceFolder & "' '" & d & "/'"			   o �pp | d o   s h e l l   s c r i p t   " d i t t o   ' "   &   s o u r c e F o l d e r   &   " '   ' "   &   d   &   " / ' " 	 	 	m qrq I -�ls�k
�l .sysoexecTEXT���     TEXTs b  )tut b  %vwv b  #xyx b  z{z m  || �}}� r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   '{ o  �j�j 0 sourcefolder sourceFoldery m  "~~ �  '   'w o  #$�i�i 0 d  u m  %(�� ���  / '�k  r ��� l ..�h�g�f�h  �g  �f  � ��� l ..�e�d�c�e  �d  �c  � ��� l ..�b�a�`�b  �a  �`  � ��� l ..�_�^�]�_  �^  �]  � ��� r  .5��� m  ./�\
�\ boovfals� o      �[�[ 0 isbackingup isBackingUp� ��� r  6=��� m  67�Z
�Z boovfals� o      �Y�Y 0 manualbackup manualBackup� ��� l >>�X�W�V�X  �W  �V  � ��U� Z  >����T�S� l >I��R�Q� E  >I��� o  >C�P�P &0 displaysleepstate displaySleepState� o  CH�O�O 0 strawake strAwake�R  �Q  � k  L��� ��� r  LY��� n  LU��� 1  QU�N
�N 
time� l LQ��M�L� I LQ�K�J�I
�K .misccurdldt    ��� null�J  �I  �M  �L  � o      �H�H 0 endtime endTime� ��� r  Za��� n Z_��� I  [_�G�F�E�G 0 getduration getDuration�F  �E  �  f  Z[� o      �D�D 0 duration  � ��� r  bm��� n  bk��� 3  gk�C
�C 
cobj� o  bg�B�B $0 messagescomplete messagesComplete� o      �A�A 0 msg  � ��� Z  n����@�?� l nu��>�=� =  nu��� o  ns�<�< 0 notifications  � m  st�;
�; boovtrue�>  �=  � Z  x�����:� l x��9�8� =  x��� o  x}�7�7 0 nsfw  � m  }~�6
�6 boovtrue�9  �8  � k  ���� ��� I ���5��
�5 .sysonotfnull��� ��� TEXT� b  ����� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���4�4 0 duration  � m  ���� ���    m i n u t e s ! 
� o  ���3�3 0 msg  � �2��1
�2 
appr� m  ���� ��� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�1  � ��0� I ���/��.
�/ .sysodelanull��� ��� nmbr� m  ���-�- �.  �0  � ��� l ����,�+� =  ����� o  ���*�* 0 nsfw  � m  ���)
�) boovfals�,  �+  � ��(� k  ���� ��� I ���'��
�' .sysonotfnull��� ��� TEXT� b  ����� b  ����� m  ���� ��� 6 F o r   t h e   f i r s t   t i m e   e v e r   i n  � o  ���&�& 0 duration  � m  ���� ���    m i n u t e s ! 
� �%��$
�% 
appr� m  ���� ���  B a c k e d   u p !�$  � ��#� I ���"��!
�" .sysodelanull��� ��� nmbr� m  ��� �  �!  �#  �(  �:  �@  �?  � ��� l ������  �  �  � ��� n ����� I  ������ $0 resetmenubaricon resetMenuBarIcon�  �  �  f  ���  �T  �S  �U   ��� l ������ =  ����� o  ���� 0 initialbackup initialBackup� m  ���
� boovfals�  �  � ��� k  �&�� ��� r  ���� c  ����� l ������ b  ����� b  ����� b  ����� b  ��   b  �� b  �� b  �� o  ���� "0 destinationdisk destinationDisk m  �� �		  B a c k u p s / o  ���� 0 machinefolder machineFolder m  ��

 �  / o  ���� 0 t  � m  �� �  /� o  ���� 0 
foldername 
folderName� m  �� �  /�  �  � m  ���
� 
TEXT� o      �� 0 c  �  r   c   l ��
 b   b   b   b   b  	  o  �	�	 "0 destinationdisk destinationDisk  m  !! �""  B a c k u p s / o  	�� 0 machinefolder machineFolder m  ## �$$  / L a t e s t / o  �� 0 
foldername 
folderName m  %% �&&  /�  �
   m  �
� 
TEXT o      �� 0 d   '(' I %�)�
� .ascrcmnt****      � ****) l !*��* b  !+,+ m  -- �..  b a c k i n g   u p   t o :  , o   � �  0 d  �  �  �  ( /0/ l &&��������  ��  ��  0 121 Z  &�34����3 l &15����5 E  &1676 o  &+���� &0 displaysleepstate displaySleepState7 o  +0���� 0 strawake strAwake��  ��  4 k  4�88 9:9 r  4A;<; n  4==>= 1  9=��
�� 
time> l 49?����? I 49������
�� .misccurdldt    ��� null��  ��  ��  ��  < o      ���� 0 	starttime 	startTime: @A@ r  BMBCB n  BKDED 3  GK��
�� 
cobjE o  BG���� *0 messagesencouraging messagesEncouragingC o      ���� 0 msg  A F��F Z  N�GH����G l NUI����I =  NUJKJ o  NS���� 0 notifications  K m  ST��
�� boovtrue��  ��  H Z  X�LMN��L l X_O����O =  X_PQP o  X]���� 0 nsfw  Q m  ]^��
�� boovtrue��  ��  M k  bsRR STS I bm��UV
�� .sysonotfnull��� ��� TEXTU o  bc���� 0 msg  V ��W��
�� 
apprW m  fiXX �YY 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  T Z��Z I ns��[��
�� .sysodelanull��� ��� nmbr[ m  no���� ��  ��  N \]\ l v}^����^ =  v}_`_ o  v{���� 0 nsfw  ` m  {|��
�� boovfals��  ��  ] a��a k  ��bb cdc I ����ef
�� .sysonotfnull��� ��� TEXTe m  ��gg �hh  B a c k i n g   u pf ��i��
�� 
appri m  ��jj �kk 
 W B T F U��  d l��l I ����m��
�� .sysodelanull��� ��� nmbrm m  ������ ��  ��  ��  ��  ��  ��  ��  ��  ��  2 non l ����������  ��  ��  o pqp l ����������  ��  ��  q rsr l ����������  ��  ��  s tut l ����������  ��  ��  u vwv I ����x��
�� .sysoexecTEXT���     TEXTx b  ��yzy b  ��{|{ b  ��}~} b  ��� b  ����� b  ����� m  ���� ���( r s y n c   - a v z   - - c v s - e x c l u d e   - - i n c l u d e = ' c o m . g o o g l e . C h r o m e . s a v e d S t a t e / '   - - e x c l u d e = ' c o m . a p p l e . l o g i n w i n d o w . p l i s t '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . p l i s t . * '   - - e x c l u d e = ' * . p l i s t . * '   - - e x c l u d e = ' c o m . a p p l e . N e t I n f o M a n a g e r . p l i s t '   - - e x c l u d e = ' . l o c a l i z e d '   - - e x c l u d e = ' . F B C '   - - e x c l u d e = ' . D S '   - - e x c l u d e = ' c o m . a p p l e . n s u r l s e s s i o n d / '   - - e x c l u d e = ' L o g s / '   - - e x c l u d e = ' s a v e d - t e l e m e t r y - p i n g s '   - - e x c l u d e = ' S e s s i o n   S t o r a g e / '   - - e x c l u d e = ' C a c h e / '   - - e x c l u d e = ' C a c h e s / '   - - e x c l u d e = ' c o m . a p p l e . f i n d e r . s a v e d S t a t e / '   - - e x c l u d e = ' S a v e d   A p p l i c a t i o n   S t a t e '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s / '   - - e x c l u d e = ' M o b i l e   D o c u m e n t s . * '   - - e x c l u d e = ' . w e b t m p '   - - e x c l u d e = ' * . w a f '   - - e x c l u d e = ' . T r a s h '   - - p r o g r e s s   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ������ 0 c  � m  ���� ���  '   '� o  ������ 0 sourcefolder sourceFolder~ m  ���� ���  '   '| o  ������ 0 d  z m  ���� ���  / '��  w ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� r  ����� m  ����
�� boovfals� o      ���� 0 isbackingup isBackingUp� ��� r  ����� m  ����
�� boovfals� o      ���� 0 manualbackup manualBackup� ��� l ����������  ��  ��  � ���� Z  �&������ = ����� n  ����� 2 ����
�� 
cobj� l �������� c  ����� 4  �����
�� 
psxf� o  ������ 0 c  � m  ����
�� 
alis��  ��  � J  ������  � k  �2�� ��� l ��������  � % delete folder t of backupFolder   � ��� > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r� ��� l ����������  ��  ��  � ��� r  ����� c  ����� n  ����� 4  �����
�� 
cfol� o  ������ 0 t  � o  ������ 0 backupfolder backupFolder� m  ����
�� 
TEXT� o      ���� 0 oldestfolder oldestFolder� ��� r  ����� c  ����� n  ����� 1  ����
�� 
psxp� o  ������ 0 oldestfolder oldestFolder� m  ����
�� 
TEXT� o      ���� 0 oldestfolder oldestFolder� ��� I �������
�� .ascrcmnt****      � ****� l �������� b  ����� m  ���� ���  t o   d e l e t e :  � o  ������ 0 oldestfolder oldestFolder��  ��  ��  � ��� l ����������  ��  ��  � ��� r  ���� b  ���� b  ����� m  ���� ���  r m   - r f   '� o  ������ 0 oldestfolder oldestFolder� m  ��� ���  '� o      ���� 0 todelete toDelete� ��� I 
�����
�� .sysoexecTEXT���     TEXT� o  ���� 0 todelete toDelete��  � ��� l ��������  ��  ��  � ���� Z  2������� l ���~� E  ��� o  �}�} &0 displaysleepstate displaySleepState� o  �|�| 0 strawake strAwake�  �~  � k  .�� ��� r  &��� n  "��� 1  "�{
�{ 
time� l ��z�y� I �x�w�v
�x .misccurdldt    ��� null�w  �v  �z  �y  � o      �u�u 0 endtime endTime� ��� r  '.��� n ',��� I  (,�t�s�r�t 0 getduration getDuration�s  �r  �  f  '(� o      �q�q 0 duration  � ��� l //�p���p  � � �display notification "I backed up any new files found in " & duration & "minutes but found no other changes so I just strolled on by..."   � ��� d i s p l a y   n o t i f i c a t i o n   " I   b a c k e d   u p   a n y   n e w   f i l e s   f o u n d   i n   "   &   d u r a t i o n   &   " m i n u t e s   b u t   f o u n d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . . "� ��� l //�o���o  �  delay 2   � ���  d e l a y   2� ��� l //�n�m�l�n  �m  �l  � �	 � r  /:			 n  /8			 3  48�k
�k 
cobj	 o  /4�j�j $0 messagescomplete messagesComplete	 o      �i�i 0 msg  	  			 Z  ;(		�h�g	 l ;B		�f�e		 =  ;B	
		
 o  ;@�d�d 0 notifications  	 m  @A�c
�c boovtrue�f  �e  	 k  E$		 			 Z  E�		�b		 l EJ	�a�`	 =  EJ			 o  EF�_�_ 0 duration  	 m  FI		 ?�      �a  �`  	 Z  M�			�^	 l MT	�]�\	 =  MT			 o  MR�[�[ 0 nsfw  	 m  RS�Z
�Z boovtrue�]  �\  	 k  Wr		 			 I Wl�Y		 
�Y .sysonotfnull��� ��� TEXT	 b  Wb	!	"	! b  W`	#	$	# b  W\	%	&	% m  WZ	'	' �	(	(  A n d   i n  	& o  Z[�X�X 0 duration  	$ m  \_	)	) �	*	*    m i n u t e ! 
	" o  `a�W�W 0 msg  	  �V	+�U
�V 
appr	+ m  eh	,	, �	-	- : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�U  	 	.�T	. I mr�S	/�R
�S .sysodelanull��� ��� nmbr	/ m  mn�Q�Q �R  �T  	 	0	1	0 l u|	2�P�O	2 =  u|	3	4	3 o  uz�N�N 0 nsfw  	4 m  z{�M
�M boovfals�P  �O  	1 	5�L	5 k  �	6	6 	7	8	7 I ��K	9	:
�K .sysonotfnull��� ��� TEXT	9 b  �	;	<	; b  �	=	>	= m  �	?	? �	@	@  A n d   i n  	> o  ���J�J 0 duration  	< m  ��	A	A �	B	B    m i n u t e ! 
	: �I	C�H
�I 
appr	C m  ��	D	D �	E	E  B a c k e d   u p !�H  	8 	F�G	F I ���F	G�E
�F .sysodelanull��� ��� nmbr	G m  ���D�D �E  �G  �L  �^  �b  	 Z  ��	H	I	J�C	H l ��	K�B�A	K =  ��	L	M	L o  ���@�@ 0 nsfw  	M m  ���?
�? boovtrue�B  �A  	I k  ��	N	N 	O	P	O I ���>	Q	R
�> .sysonotfnull��� ��� TEXT	Q b  ��	S	T	S b  ��	U	V	U b  ��	W	X	W m  ��	Y	Y �	Z	Z  A n d   i n  	X o  ���=�= 0 duration  	V m  ��	[	[ �	\	\    m i n u t e s ! 
	T o  ���<�< 0 msg  	R �;	]�:
�; 
appr	] m  ��	^	^ �	_	_ : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !�:  	P 	`�9	` I ���8	a�7
�8 .sysodelanull��� ��� nmbr	a m  ���6�6 �7  �9  	J 	b	c	b l ��	d�5�4	d =  ��	e	f	e o  ���3�3 0 nsfw  	f m  ���2
�2 boovfals�5  �4  	c 	g�1	g k  ��	h	h 	i	j	i I ���0	k	l
�0 .sysonotfnull��� ��� TEXT	k b  ��	m	n	m b  ��	o	p	o m  ��	q	q �	r	r  A n d   i n  	p o  ���/�/ 0 duration  	n m  ��	s	s �	t	t    m i n u t e s ! 
	l �.	u�-
�. 
appr	u m  ��	v	v �	w	w  B a c k e d   u p !�-  	j 	x�,	x I ���+	y�*
�+ .sysodelanull��� ��� nmbr	y m  ���)�) �*  �,  �1  �C  	 	z	{	z l ���(�'�&�(  �'  �&  	{ 	|�%	| Z  �$	}	~	�$	} l ��	��#�"	� =  ��	�	�	� o  ���!�! 0 nsfw  	� m  ��� 
�  boovtrue�#  �"  	~ I ��	�	�
� .sysonotfnull��� ��� TEXT	� m  ��	�	� �	�	� � B e c a u s e   i t   w a s   e m p t y   b e c a u s e   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d . 
 B e c a u s e   I ' m   O C D .	� �	��
� 
appr	� m  �	�	� �	�	� @ I   d e l e t e d   t h a t   n e w   b a c k u p   f o l d e r�  	 	�	�	� l 		���	� =  		�	�	� o  	�� 0 nsfw  	� m  �
� boovfals�  �  	� 	��	� I  �	�	�
� .sysonotfnull��� ��� TEXT	� m  	�	� �	�	� p B e c a u s e   i t   w a s   e m p t y   a s   n o   f i l e s   w e r e   c h a n g e d   o r   d e l e t e d	� �	��
� 
appr	� m  	�	� �	�	� 2 D e l e t e d   n e w   b a c k u p   f o l d e r�  �  �$  �%  �h  �g  	 	�	�	� l ))����  �  �  	� 	��	� n ).	�	�	� I  *.���� $0 resetmenubaricon resetMenuBarIcon�  �  	�  f  )*�  ��  ��  ��  ��  � k  5&	�	� 	�	�	� Z  5 	�	���	� l 5@	���
	� E  5@	�	�	� o  5:�	�	 &0 displaysleepstate displaySleepState	� o  :?�� 0 strawake strAwake�  �
  	� k  C	�	� 	�	�	� r  CP	�	�	� n  CL	�	�	� 1  HL�
� 
time	� l CH	���	� I CH���
� .misccurdldt    ��� null�  �  �  �  	� o      �� 0 endtime endTime	� 	�	�	� r  QX	�	�	� n QV	�	�	� I  RV� �����  0 getduration getDuration��  ��  	�  f  QR	� o      ���� 0 duration  	� 	�	�	� r  Yd	�	�	� n  Yb	�	�	� 3  ^b��
�� 
cobj	� o  Y^���� $0 messagescomplete messagesComplete	� o      ���� 0 msg  	� 	���	� Z  e	�	�����	� l el	�����	� =  el	�	�	� o  ej���� 0 notifications  	� m  jk��
�� boovtrue��  ��  	� Z  o	�	���	�	� l ot	�����	� =  ot	�	�	� o  op���� 0 duration  	� m  ps	�	� ?�      ��  ��  	� Z  w�	�	�	���	� l w~	�����	� =  w~	�	�	� o  w|���� 0 nsfw  	� m  |}��
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� o  ������ 0 msg  	� ��	���
�� 
appr	� m  ��	�	� �	�	� : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  	� 	�	�	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovfals��  ��  	� 	���	� k  ��	�	� 	�	�	� I ����	�	�
�� .sysonotfnull��� ��� TEXT	� b  ��	�	�	� b  ��	�	�	� m  ��	�	� �	�	�  A n d   i n  	� o  ������ 0 duration  	� m  ��	�	� �	�	�    m i n u t e ! 
	� ��	���
�� 
appr	� m  ��	�	� �	�	�  B a c k e d   u p !��  	� 	���	� I ����	���
�� .sysodelanull��� ��� nmbr	� m  ������ ��  ��  ��  ��  ��  	� Z  �	�	�	���	� l ��	�����	� =  ��	�	�	� o  ������ 0 nsfw  	� m  ����
�� boovtrue��  ��  	� k  ��	�	� 	�	�	� I ����
 

�� .sysonotfnull��� ��� TEXT
  b  ��


 b  ��


 b  ��


 m  ��

 �
	
	  A n d   i n  
 o  ������ 0 duration  
 m  ��



 �

    m i n u t e s ! 

 o  ������ 0 msg  
 ��
��
�� 
appr
 m  ��

 �

 : W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !��  	� 
��
 I ����
��
�� .sysodelanull��� ��� nmbr
 m  ������ ��  ��  	� 


 l ��
����
 =  ��


 o  ������ 0 nsfw  
 m  ����
�� boovfals��  ��  
 
��
 k  �

 


 I ���


�� .sysonotfnull��� ��� TEXT
 b  �


 b  � 


 m  ��
 
  �
!
!  A n d   i n  
 o  ������ 0 duration  
 m   
"
" �
#
#    m i n u t e s ! 

 ��
$��
�� 
appr
$ m  

%
% �
&
&  B a c k e d   u p !��  
 
'��
' I ��
(��
�� .sysodelanull��� ��� nmbr
( m  ���� ��  ��  ��  ��  ��  ��  ��  �  �  	� 
)
*
) l !!��������  ��  ��  
* 
+��
+ n !&
,
-
, I  "&�������� $0 resetmenubaricon resetMenuBarIcon��  ��  
-  f  !"��  ��  �  ��  ��  n m    
.
.�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  l 
/
0
/ l ,,��������  ��  ��  
0 
1��
1 I  ,4��
2���� 0 	stopwatch  
2 
3��
3 m  -0
4
4 �
5
5  f i n i s h��  ��  ��  H 
6
7
6 l     ��������  ��  ��  
7 
8
9
8 l     ��������  ��  ��  
9 
:
;
: l     ��������  ��  ��  
; 
<
=
< l     ��������  ��  ��  
= 
>
?
> l     ��������  ��  ��  
? 
@
A
@ l     ��
B
C��  
B � �-----------------------------------------------------------------------------------------------------------------------------------------------   
C �
D
D - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
A 
E
F
E l     ��
G
H��  
G � �-----------------------------------------------------------------------------------------------------------------------------------------------   
H �
I
I - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
F 
J
K
J l     ��
L
M��  
L � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   
M �
N
N - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
K 
O
P
O l     ��������  ��  ��  
P 
Q
R
Q l     ��
S
T��  
S � � Utility function that requires 2 paraments, both strings, that determine the type of object that needs to be checked and the object itself.   
T �
U
U   U t i l i t y   f u n c t i o n   t h a t   r e q u i r e s   2   p a r a m e n t s ,   b o t h   s t r i n g s ,   t h a t   d e t e r m i n e   t h e   t y p e   o f   o b j e c t   t h a t   n e e d s   t o   b e   c h e c k e d   a n d   t h e   o b j e c t   i t s e l f .
R 
V
W
V l     ��
X
Y��  
X � � Since a switch isn't used in AppleScript, it uses an if statement that runs through types of 'disk', 'file' and 'folder' so the correct checks can be made with the corresponding objects that has been passed through to the function.   
Y �
Z
Z�   S i n c e   a   s w i t c h   i s n ' t   u s e d   i n   A p p l e S c r i p t ,   i t   u s e s   a n   i f   s t a t e m e n t   t h a t   r u n s   t h r o u g h   t y p e s   o f   ' d i s k ' ,   ' f i l e '   a n d   ' f o l d e r '   s o   t h e   c o r r e c t   c h e c k s   c a n   b e   m a d e   w i t h   t h e   c o r r e s p o n d i n g   o b j e c t s   t h a t   h a s   b e e n   p a s s e d   t h r o u g h   t o   t h e   f u n c t i o n .
W 
[
\
[ i  X[
]
^
] I      ��
_���� 0 itexists itExists
_ 
`
a
` o      ���� 0 
objecttype 
objectType
a 
b��
b o      ���� 
0 object  ��  ��  
^ l    W
c
d
e
c O     W
f
g
f Z    V
h
i
j��
h l   
k����
k =    
l
m
l o    ���� 0 
objecttype 
objectType
m m    
n
n �
o
o  d i s k��  ��  
i Z   
 
p
q��
r
p I  
 ��
s��
�� .coredoexnull���     ****
s 4   
 ��
t
�� 
cdis
t o    ���� 
0 object  ��  
q L    
u
u m    ��
�� boovtrue��  
r L    
v
v m    ��
�� boovfals
j 
w
x
w l   "
y����
y =    "
z
{
z o     ���� 0 
objecttype 
objectType
{ m     !
|
| �
}
}  f i l e��  ��  
x 
~

~ Z   % 7
�
���
�
� I  % -�
��~
� .coredoexnull���     ****
� 4   % )�}
�
�} 
file
� o   ' (�|�| 
0 object  �~  
� L   0 2
�
� m   0 1�{
�{ boovtrue��  
� L   5 7
�
� m   5 6�z
�z boovfals
 
�
�
� l  : =
��y�x
� =   : =
�
�
� o   : ;�w�w 0 
objecttype 
objectType
� m   ; <
�
� �
�
�  f o l d e r�y  �x  
� 
��v
� Z   @ R
�
��u
�
� I  @ H�t
��s
�t .coredoexnull���     ****
� 4   @ D�r
�
�r 
cfol
� o   B C�q�q 
0 object  �s  
� L   K M
�
� m   K L�p
�p boovtrue�u  
� L   P R
�
� m   P Q�o
�o boovfals�v  ��  
g m     
�
��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  
d "  (string, string) as Boolean   
e �
�
� 8   ( s t r i n g ,   s t r i n g )   a s   B o o l e a n
\ 
�
�
� l     �n�m�l�n  �m  �l  
� 
�
�
� l     �k�j�i�k  �j  �i  
� 
�
�
� l     �h�g�f�h  �g  �f  
� 
�
�
� l     �e�d�c�e  �d  �c  
� 
�
�
� l     �b�a�`�b  �a  �`  
� 
�
�
� l     �_
�
��_  
� � � Utility function to get the name of the machine and serial number to set as a folder name in case the hard drive being used is to be used for backups on different machines.   
� �
�
�Z   U t i l i t y   f u n c t i o n   t o   g e t   t h e   n a m e   o f   t h e   m a c h i n e   a n d   s e r i a l   n u m b e r   t o   s e t   a s   a   f o l d e r   n a m e   i n   c a s e   t h e   h a r d   d r i v e   b e i n g   u s e d   i s   t o   b e   u s e d   f o r   b a c k u p s   o n   d i f f e r e n t   m a c h i n e s .
� 
�
�
� l     �^
�
��^  
� P J This means that backups can be identified from what machine they're from.   
� �
�
� �   T h i s   m e a n s   t h a t   b a c k u p s   c a n   b e   i d e n t i f i e d   f r o m   w h a t   m a c h i n e   t h e y ' r e   f r o m .
� 
�
�
� i  \_
�
�
� I      �]�\�[�] .0 getcomputeridentifier getComputerIdentifier�\  �[  
� k     
�
� 
�
�
� r     	
�
�
� n     
�
�
� 1    �Z
�Z 
sicn
� l    
��Y�X
� I    �W�V�U
�W .sysosigtsirr   ��� null�V  �U  �Y  �X  
� o      �T�T 0 computername computerName
� 
�
�
� r   
 
�
�
� I  
 �S
��R
�S .sysoexecTEXT���     TEXT
� m   
 
�
� �
�
� � / u s r / s b i n / s y s t e m _ p r o f i l e r   S P H a r d w a r e D a t a T y p e   |   a w k   ' / S e r i a l /   {   p r i n t   $ N F   } '  �R  
� o      �Q�Q "0 strserialnumber strSerialNumber
� 
�
�
� r    
�
�
� l   
��P�O
� b    
�
�
� b    
�
�
� o    �N�N 0 computername computerName
� m    
�
� �
�
�    -  
� o    �M�M "0 strserialnumber strSerialNumber�P  �O  
� o      �L�L  0 identifiername identifierName
� 
��K
� L    
�
� o    �J�J  0 identifiername identifierName�K  
� 
�
�
� l     �I�H�G�I  �H  �G  
� 
�
�
� l     �F�E�D�F  �E  �D  
� 
�
�
� l     �C�B�A�C  �B  �A  
� 
�
�
� l     �@�?�>�@  �?  �>  
� 
�
�
� l     �=�<�;�=  �<  �;  
� 
�
�
� l     �:
�
��:  
� � � Utility function that gets the current date and generates a timestamp in the format YYYYMMDD_HHMMSS with the parameter set to 'false'.   
� �
�
�   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   c u r r e n t   d a t e   a n d   g e n e r a t e s   a   t i m e s t a m p   i n   t h e   f o r m a t   Y Y Y Y M M D D _ H H M M S S   w i t h   t h e   p a r a m e t e r   s e t   t o   ' f a l s e ' .
� 
�
�
� l     �9
�
��9  
� � � If the parameter is 'true' to signify that it's for use as a timestamp folder name, the returned value is 'backup_YYYYMMDD_HHMMSS'.   
� �
�
�   I f   t h e   p a r a m e t e r   i s   ' t r u e '   t o   s i g n i f y   t h a t   i t ' s   f o r   u s e   a s   a   t i m e s t a m p   f o l d e r   n a m e ,   t h e   r e t u r n e d   v a l u e   i s   ' b a c k u p _ Y Y Y Y M M D D _ H H M M S S ' .
� 
�
�
� i  `c
�
�
� I      �8
��7�8 0 gettimestamp getTimestamp
� 
��6
� o      �5�5 0 isfolder isFolder�6  �7  
� l   �
�
�
�
� k    �
�
� 
�
�
� l     �4
�
��4  
�   Date variables   
� �
�
�    D a t e   v a r i a b l e s
� 
�
�
� r     )
�
�
� l     
��3�2
� I     �1�0�/
�1 .misccurdldt    ��� null�0  �/  �3  �2  
� K    
�
� �.
�
�
�. 
year
� o    �-�- 0 y  
� �,
�
�
�, 
mnth
� o    �+�+ 0 m  
� �*
�
�
�* 
day 
� o    �)�) 0 d  
� �(
��'
�( 
time
� o   	 
�&�& 0 t  �'  
� 
�
�
� r   * 1
�
�
� c   * /   l  * -�%�$ c   * - o   * +�#�# 0 y   m   + ,�"
�" 
long�%  �$   m   - .�!
�! 
TEXT
� o      � �  0 ty tY
�  r   2 9 c   2 7	
	 l  2 5�� c   2 5 o   2 3�� 0 y   m   3 4�
� 
long�  �  
 m   5 6�
� 
TEXT o      �� 0 ty tY  r   : A c   : ? l  : =�� c   : = o   : ;�� 0 m   m   ; <�
� 
long�  �   m   = >�
� 
TEXT o      �� 0 tm tM  r   B I c   B G l  B E�� c   B E o   B C�� 0 d   m   C D�
� 
long�  �   m   E F�
� 
TEXT o      �� 0 td tD  !  r   J Q"#" c   J O$%$ l  J M&��& c   J M'(' o   J K�� 0 t  ( m   K L�

�
 
long�  �  % m   M N�	
�	 
TEXT# o      �� 0 tt tT! )*) l  R R����  �  �  * +,+ l  R R�-.�  - U O Append the month or day with a 0 if the string length is only 1 character long   . �// �   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g, 010 r   R [232 c   R Y454 l  R W6��6 I  R W�7� 
� .corecnte****       ****7 o   R S���� 0 tm tM�   �  �  5 m   W X��
�� 
nmbr3 o      ���� 
0 tml tML1 898 r   \ e:;: c   \ c<=< l  \ a>����> I  \ a��?��
�� .corecnte****       ****? o   \ ]���� 0 tm tM��  ��  ��  = m   a b��
�� 
nmbr; o      ���� 
0 tdl tDL9 @A@ l  f f��������  ��  ��  A BCB Z   f uDE����D l  f iF����F =   f iGHG o   f g���� 
0 tml tMLH m   g h���� ��  ��  E r   l qIJI b   l oKLK m   l mMM �NN  0L o   m n���� 0 tm tMJ o      ���� 0 tm tM��  ��  C OPO l  v v��������  ��  ��  P QRQ Z   v �ST����S l  v yU����U =   v yVWV o   v w���� 
0 tdl tDLW m   w x���� ��  ��  T r   | �XYX b   | �Z[Z m   | \\ �]]  0[ o    ����� 0 td tDY o      ���� 0 td tD��  ��  R ^_^ l  � ���������  ��  ��  _ `a` l  � ���������  ��  ��  a bcb l  � ���de��  d   Time variables	   e �ff     T i m e   v a r i a b l e s 	c ghg l  � ���ij��  i  	 Get hour   j �kk    G e t   h o u rh lml r   � �non n   � �pqp 1   � ���
�� 
tstrq l  � �r����r I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  o o      ���� 0 timestr timeStrm sts r   � �uvu I  � �w��xw z����
�� .sysooffslong    ��� null
�� misccura��  x ��yz
�� 
psofy m   � �{{ �||  :z ��}��
�� 
psin} o   � ����� 0 timestr timeStr��  v o      ���� 0 pos  t ~~ r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 h   ��� r   � ���� c   � ���� n   � ���� 7 � �����
�� 
cha � l  � ������� [   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  �  ;   � �� o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l  � ���������  ��  ��  � ��� l  � �������  �   Get minute   � ���    G e t   m i n u t e� ��� r   � ���� I  � ������ z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r   ���� c   ���� n   � ��� 7  � ����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   ��
�� 
TEXT� o      ���� 0 m  � ��� r  ��� c  ��� n  ��� 7����
�� 
cha � l ������ [  ��� o  ���� 0 pos  � m  ���� ��  ��  �  ;  � o  ���� 0 timestr timeStr� m  ��
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l ��������  ��  ��  � ��� l ������  �   Get AM or PM   � ���    G e t   A M   o r   P M� ��� r  2��� I 0����� z����
�� .sysooffslong    ��� null
�� misccura��  � ����
�� 
psof� m  "%�� ���   � �����
�� 
psin� o  ()���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  3E��� c  3C��� n  3A��� 74A����
�� 
cha � l :>������ [  :>��� o  ;<���� 0 pos  � m  <=���� ��  ��  �  ;  ?@� o  34���� 0 timestr timeStr� m  AB��
�� 
TEXT� o      ���� 0 s  � ��� l FF��������  ��  ��  � ��� l FF��������  ��  ��  � ��� Z  F������ l FI���~� =  FI��� o  FG�}�} 0 isfolder isFolder� m  GH�|
�| boovtrue�  �~  � k  La�� ��� l LL�{���{  �   Create folder timestamp   � ��� 0   C r e a t e   f o l d e r   t i m e s t a m p� ��z� r  La��� b  L_��� b  L]��� b  L[��� b  LY��� b  LU   b  LS b  LQ m  LO �  b a c k u p _ o  OP�y�y 0 ty tY o  QR�x�x 0 tm tM o  ST�w�w 0 td tD� m  UX �		  _� o  YZ�v�v 0 h  � o  [\�u�u 0 m  � o  ]^�t�t 0 s  � o      �s�s 0 	timestamp  �z  � 

 l dg�r�q =  dg o  de�p�p 0 isfolder isFolder m  ef�o
�o boovfals�r  �q   �n k  j{  l jj�m�m     Create timestamp    � "   C r e a t e   t i m e s t a m p �l r  j{ b  jy b  jw b  ju b  js  b  jo!"! b  jm#$# o  jk�k�k 0 ty tY$ o  kl�j�j 0 tm tM" o  mn�i�i 0 td tD  m  or%% �&&  _ o  st�h�h 0 h   o  uv�g�g 0 m   o  wx�f�f 0 s   o      �e�e 0 	timestamp  �l  �n  ��  � '(' l ���d�c�b�d  �c  �b  ( )�a) L  ��** o  ���`�` 0 	timestamp  �a  
�  	(boolean)   
� �++  ( b o o l e a n )
� ,-, l     �_�^�]�_  �^  �]  - ./. l     �\�[�Z�\  �[  �Z  / 010 l     �Y�X�W�Y  �X  �W  1 232 l     �V�U�T�V  �U  �T  3 454 l     �S�R�Q�S  �R  �Q  5 676 l     �P89�P  8 q k Utility function that gets the size of the source folder and the remaining free space of the backup drive.   9 �:: �   U t i l i t y   f u n c t i o n   t h a t   g e t s   t h e   s i z e   o f   t h e   s o u r c e   f o l d e r   a n d   t h e   r e m a i n i n g   f r e e   s p a c e   o f   t h e   b a c k u p   d r i v e .7 ;<; l     �O=>�O  = w q This uses shell script to get the former and have had to adjust the values to GB to do the correct calculations.   > �?? �   T h i s   u s e s   s h e l l   s c r i p t   t o   g e t   t h e   f o r m e r   a n d   h a v e   h a d   t o   a d j u s t   t h e   v a l u e s   t o   G B   t o   d o   t h e   c o r r e c t   c a l c u l a t i o n s .< @A@ i  dgBCB I      �ND�M�N 0 comparesizes compareSizesD EFE o      �L�L 
0 source  F G�KG o      �J�J 0 destination  �K  �M  C l    hHIJH k     hKK LML r     NON m     �I
�I boovtrueO o      �H�H 0 fit  M PQP r    
RSR 4    �GT
�G 
psxfT o    �F�F 0 destination  S o      �E�E 0 destination  Q UVU l   �D�C�B�D  �C  �B  V WXW O    VYZY k    U[[ \]\ r    ^_^ I   �A`�@
�A .sysoexecTEXT���     TEXT` b    aba b    cdc m    ee �ff  d u   - m s  d o    �?�? 
0 source  b m    gg �hh    |   c u t   - f   1�@  _ o      �>�> 0 
foldersize 
folderSize] iji r    -klk ^    +mnm l   )o�=�<o I   )pq�;p z�:�9
�: .sysorondlong        doub
�9 misccuraq ]    $rsr l   "t�8�7t ^    "uvu o     �6�6 0 
foldersize 
folderSizev m     !�5�5 �8  �7  s m   " #�4�4 d�;  �=  �<  n m   ) *�3�3 dl o      �2�2 0 
foldersize 
folderSizej wxw l  . .�1�0�/�1  �0  �/  x yzy r   . <{|{ ^   . :}~} ^   . 8� ^   . 6��� l  . 4��.�-� l  . 4��,�+� n   . 4��� 1   2 4�*
�* 
frsp� 4   . 2�)�
�) 
cdis� o   0 1�(�( 0 destination  �,  �+  �.  �-  � m   4 5�'�' � m   6 7�&�& ~ m   8 9�%�% | o      �$�$ 0 	freespace 	freeSpacez ��� r   = M��� ^   = K��� l  = I��#�"� I  = I���!� z� �
�  .sysorondlong        doub
� misccura� l  A D���� ]   A D��� o   A B�� 0 	freespace 	freeSpace� m   B C�� d�  �  �!  �#  �"  � m   I J�� d� o      �� 0 	freespace 	freeSpace� ��� l  N N����  �  �  � ��� I  N U���
� .ascrcmnt****      � ****� l  N Q���� b   N Q��� o   N O�� 0 
foldersize 
folderSize� o   O P�� 0 	freespace 	freeSpace�  �  �  �  Z m    ���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  X ��� l  W W����  �  �  � ��� Z   W e����
� H   W [�� l  W Z��	�� A   W Z��� o   W X�� 0 
foldersize 
folderSize� o   X Y�� 0 	freespace 	freeSpace�	  �  � r   ^ a��� m   ^ _�
� boovfals� o      �� 0 fit  �  �
  � ��� l  f f����  �  �  � �� � L   f h�� o   f g���� 0 fit  �   I  (string, string)   J ���   ( s t r i n g ,   s t r i n g )A ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � m g Utility function for debugging as a stopwatch to see when the backup process has started and finished.   � ��� �   U t i l i t y   f u n c t i o n   f o r   d e b u g g i n g   a s   a   s t o p w a t c h   t o   s e e   w h e n   t h e   b a c k u p   p r o c e s s   h a s   s t a r t e d   a n d   f i n i s h e d .� ��� i  hk��� I      ������� 0 	stopwatch  � ���� o      ���� 0 mode  ��  ��  � l    5���� k     5�� ��� q      �� ������ 0 x  ��  � ��� Z     3������ l    ������ =     ��� o     ���� 0 mode  � m    �� ��� 
 s t a r t��  ��  � k    �� ��� r    ��� I    ������� 0 gettimestamp getTimestamp� ���� m    ��
�� boovfals��  ��  � o      ���� 0 x  � ���� I   �����
�� .ascrcmnt****      � ****� l   ������ b    ��� m    �� ���   b a c k u p   s t a r t e d :  � o    ���� 0 x  ��  ��  ��  ��  � ��� l   ������ =    ��� o    ���� 0 mode  � m    �� ���  f i n i s h��  ��  � ���� k    /�� ��� r    '��� I    %������� 0 gettimestamp getTimestamp� ���� m     !��
�� boovfals��  ��  � o      ���� 0 x  � ���� I  ( /�����
�� .ascrcmnt****      � ****� l  ( +������ b   ( +��� m   ( )�� ��� " b a c k u p   f i n i s h e d :  � o   ) *���� 0 x  ��  ��  ��  ��  ��  ��  � ���� l  4 4��������  ��  ��  ��  �  (string)   � ���  ( s t r i n g )� ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � � � l     ��������  ��  ��     l     ��������  ��  ��    l     ����   M G Utility function to get the duration of the backup process in minutes.    � �   U t i l i t y   f u n c t i o n   t o   g e t   t h e   d u r a t i o n   o f   t h e   b a c k u p   p r o c e s s   i n   m i n u t e s . 	 i  lo

 I      �������� 0 getduration getDuration��  ��   k       r      ^      l    ���� \      o     ���� 0 endtime endTime o    ���� 0 	starttime 	startTime��  ��   m    ���� < o      ���� 0 duration    r     ^     l   ���� I   �� z����
�� .sysorondlong        doub
�� misccura l   ���� ]     !  o    ���� 0 duration  ! m    ���� d��  ��  ��  ��  ��   m    ���� d o      ���� 0 duration   "��" L    ## o    ���� 0 duration  ��  	 $%$ l     ��������  ��  ��  % &'& l     ��������  ��  ��  ' ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��������  ��  ��  - ./. l     ��01��  0 ; 5 Utility function bring WBTFU to the front with focus   1 �22 j   U t i l i t y   f u n c t i o n   b r i n g   W B T F U   t o   t h e   f r o n t   w i t h   f o c u s/ 343 i  ps565 I      �������� 0 setfocus setFocus��  ��  6 O     
787 I   	������
�� .miscactvnull��� ��� null��  ��  8 m     99                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  4 :;: l     ��������  ��  ��  ; <=< l     ��>?��  > � �-----------------------------------------------------------------------------------------------------------------------------------------------   ? �@@ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -= ABA l     ��CD��  C � �-----------------------------------------------------------------------------------------------------------------------------------------------   D �EE - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -B FGF l     ��������  ��  ��  G HIH l     ��������  ��  ��  I JKJ l     ��~�}�  �~  �}  K LML l     �|�{�z�|  �{  �z  M NON l     �y�x�w�y  �x  �w  O PQP l     �vRS�v  R � �-----------------------------------------------------------------------------------------------------------------------------------------------   S �TT - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -Q UVU l     �uWX�u  W � �-----------------------------------------------------------------------------------------------------------------------------------------------   X �YY - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -V Z[Z l     �t\]�t  \ � �-- MENU BAR FUNCTIONS -----------------------------------------------------------------------------------------------------------------   ] �^^ - -   M E N U   B A R   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -[ _`_ l     �s�r�q�s  �r  �q  ` aba i  twcdc I      �pe�o�p $0 menuneedsupdate_ menuNeedsUpdate_e f�nf l     g�m�lg m      �k
�k 
cmnu�m  �l  �n  �o  d k     hh iji l     �jkl�j  k J D NSMenu's delegates method, when the menu is clicked this is called.   l �mm �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d .j non l     �ipq�i  p j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.   q �rr �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s .o sts l     �huv�h  u < 6 This means the menu items can be changed dynamically.   v �ww l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y .t x�gx n    yzy I    �f�e�d�f 0 	makemenus 	makeMenus�e  �d  z  f     �g  b {|{ l     �c�b�a�c  �b  �a  | }~} l     �`�_�^�`  �_  �^  ~ � l     �]�\�[�]  �\  �[  � ��� l     �Z�Y�X�Z  �Y  �X  � ��� l     �W�V�U�W  �V  �U  � ��� i  x{��� I      �T�S�R�T 0 	makemenus 	makeMenus�S  �R  � k     ��� ��� l    	���� n    	��� I    	�Q�P�O�Q  0 removeallitems removeAllItems�P  �O  � o     �N�N 0 newmenu newMenu� !  remove existing menu items   � ��� 6   r e m o v e   e x i s t i n g   m e n u   i t e m s� ��� l  
 
�M�L�K�M  �L  �K  � ��J� Y   
 ���I���H� k    ��� ��� r    '��� n    %��� 4   " %�G�
�G 
cobj� o   # $�F�F 0 i  � o    "�E�E 0 thelist theList� o      �D�D 0 	this_item  � ��� l  ( (�C�B�A�C  �B  �A  � ��� Z   ( �����@� l  ( -��?�>� =   ( -��� l  ( +��=�<� c   ( +��� o   ( )�;�; 0 	this_item  � m   ) *�:
�: 
TEXT�=  �<  � m   + ,�� ���  B a c k u p   n o w�?  �>  � r   0 @��� l  0 >��9�8� n  0 >��� I   7 >�7��6�7 J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   7 8�5�5 0 	this_item  � ��� m   8 9�� ���  b a c k u p H a n d l e r :� ��4� m   9 :�� ���  �4  �6  � n  0 7��� I   3 7�3�2�1�3 	0 alloc  �2  �1  � n  0 3��� o   1 3�0�0 0 
nsmenuitem 
NSMenuItem� m   0 1�/
�/ misccura�9  �8  � o      �.�. 0 thismenuitem thisMenuItem� ��� l  C H��-�,� =   C H��� l  C F��+�*� c   C F��� o   C D�)�) 0 	this_item  � m   D E�(
�( 
TEXT�+  �*  � m   F G�� ��� " T u r n   o n   t h e   b a n t s�-  �,  � ��� r   K [��� l  K Y��'�&� n  K Y��� I   R Y�%��$�% J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   R S�#�# 0 	this_item  � ��� m   S T�� ���  n s f w O n H a n d l e r :� ��"� m   T U�� ���  �"  �$  � n  K R��� I   N R�!� ��! 	0 alloc  �   �  � n  K N��� o   L N�� 0 
nsmenuitem 
NSMenuItem� m   K L�
� misccura�'  �&  � o      �� 0 thismenuitem thisMenuItem� ��� l  ^ c���� =   ^ c��� l  ^ a���� c   ^ a��� o   ^ _�� 0 	this_item  � m   _ `�
� 
TEXT�  �  � m   a b�� ��� $ T u r n   o f f   t h e   b a n t s�  �  � ��� r   f x��� l  f v���� n  f v��� I   m v���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� ��� o   m n�� 0 	this_item  � ��� m   n o�� ���  n s f w O f f H a n d l e r :� ��� m   o r�� ���  �  �  � n  f m��� I   i m���� 	0 alloc  �  �  � n  f i��� o   g i�� 0 
nsmenuitem 
NSMenuItem� m   f g�
� misccura�  �  � o      �
�
 0 thismenuitem thisMenuItem�    l  { ��	� =   { � l  { ~�� c   { ~ o   { |�� 0 	this_item   m   | }�
� 
TEXT�  �   m   ~ � �		 * T u r n   o n   n o t i f i c a t i o n s�	  �   

 r   � � l  � ��� n  � � I   � ��� � J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_  o   � ����� 0 	this_item    m   � � � , n o t i f i c a t i o n O n H a n d l e r : �� m   � � �  ��  �    n  � � I   � ��������� 	0 alloc  ��  ��   n  � � o   � ����� 0 
nsmenuitem 
NSMenuItem m   � ���
�� misccura�  �   o      ���� 0 thismenuitem thisMenuItem   l  � �!����! =   � �"#" l  � �$����$ c   � �%&% o   � ����� 0 	this_item  & m   � ���
�� 
TEXT��  ��  # m   � �'' �(( , T u r n   o f f   n o t i f i c a t i o n s��  ��    )*) r   � �+,+ l  � �-����- n  � �./. I   � ���0���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_0 121 o   � ����� 0 	this_item  2 343 m   � �55 �66 . n o t i f i c a t i o n O f f H a n d l e r :4 7��7 m   � �88 �99  ��  ��  / n  � �:;: I   � ��������� 	0 alloc  ��  ��  ; n  � �<=< o   � ����� 0 
nsmenuitem 
NSMenuItem= m   � ���
�� misccura��  ��  , o      ���� 0 thismenuitem thisMenuItem* >?> l  � �@����@ =   � �ABA l  � �C����C c   � �DED o   � ����� 0 	this_item  E m   � ���
�� 
TEXT��  ��  B m   � �FF �GG  Q u i t��  ��  ? H��H r   � �IJI l  � �K����K n  � �LML I   � ���N���� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_N OPO o   � ����� 0 	this_item  P QRQ m   � �SS �TT  q u i t H a n d l e r :R U��U m   � �VV �WW  ��  ��  M n  � �XYX I   � ��������� 	0 alloc  ��  ��  Y n  � �Z[Z o   � ����� 0 
nsmenuitem 
NSMenuItem[ m   � ���
�� misccura��  ��  J o      ���� 0 thismenuitem thisMenuItem��  �@  � \]\ l  � ���������  ��  ��  ] ^_^ l  � �`����` n  � �aba I   � ���c���� 0 additem_ addItem_c d��d o   � ����� 0 thismenuitem thisMenuItem��  ��  b o   � ����� 0 newmenu newMenu��  ��  _ efe l  � ���������  ��  ��  f g��g l  � �hijh l  � �k����k n  � �lml I   � ���n���� 0 
settarget_ 
setTarget_n o��o  f   � ���  ��  m o   � ����� 0 thismenuitem thisMenuItem��  ��  i * $ required for enabling the menu item   j �pp H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m��  �I 0 i  � m    ���� � n    qrq m    ��
�� 
nmbrr n   sts 2   ��
�� 
cobjt o    ���� 0 thelist theList�H  �J  � uvu l     ��������  ��  ��  v wxw l     ��������  ��  ��  x yzy l     ��������  ��  ��  z {|{ l     ��������  ��  ��  | }~} l     ��������  ��  ��  ~ � i  |��� I      �������  0 backuphandler_ backupHandler_� ���� o      ���� 
0 sender  ��  ��  � Z     ������� l    ������ =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovfals��  ��  � k   
 U�� ��� l  
 
������  �  animateMenuBarIcon()   � ��� ( a n i m a t e M e n u B a r I c o n ( )� ��� l  
 
��������  ��  ��  � ��� r   
 ��� m   
 ��
�� boovtrue� o      ���� 0 manualbackup manualBackup� ���� Z    U������� l   ������ =    ��� o    ���� 0 notifications  � m    ��
�� boovtrue��  ��  � Z    Q������ l   #������ =    #��� o    !���� 0 nsfw  � m   ! "��
�� boovtrue��  ��  � k   & 3�� ��� I  & -����
�� .sysonotfnull��� ��� TEXT� m   & '�� ��� T G e t t i n g   y o u r   s h i t   t o g e t h e r   s o n ,   s o   h o l d   u p� �����
�� 
appr� m   ( )�� ��� 2 W o a h ,   b a c k i n g   t h e   f u c k   u p��  � ���� I  . 3�����
�� .sysodelanull��� ��� nmbr� m   . /���� ��  ��  � ��� l  6 =������ =   6 =��� o   6 ;���� 0 nsfw  � m   ; <��
�� boovfals��  ��  � ���� k   @ M�� ��� I  @ G����
�� .sysonotfnull��� ��� TEXT� m   @ A�� ���   P r e p a r i n g   b a c k u p� �����
�� 
appr� m   B C�� ��� 
 W B T F U��  � ���� I  H M����
�� .sysodelanull��� ��� nmbr� m   H I�~�~ �  ��  ��  ��  ��  ��  ��  � ��� l  X _��}�|� =   X _��� o   X ]�{�{ 0 isbackingup isBackingUp� m   ] ^�z
�z boovtrue�}  �|  � ��y� k   b ��� ��� r   b k��� n   b i��� 3   g i�x
�x 
cobj� o   b g�w�w 40 messagesalreadybackingup messagesAlreadyBackingUp� o      �v�v 0 msg  � ��u� Z   l ����t�s� l  l s��r�q� =   l s��� o   l q�p�p 0 notifications  � m   q r�o
�o boovtrue�r  �q  � Z   v �����n� l  v }��m�l� =   v }��� o   v {�k�k 0 nsfw  � m   { |�j
�j boovtrue�m  �l  � I  � ��i��
�i .sysonotfnull��� ��� TEXT� o   � ��h�h 0 msg  � �g��f
�g 
appr� m   � ��� ��� P W o a h ,   y o u ' r e   a l r e a d y   b a c k i n g   t h e   f u c k   u p�f  � ��� l  � ���e�d� =   � ���� o   � ��c�c 0 nsfw  � m   � ��b
�b boovfals�e  �d  � ��a� I  � ��`��
�` .sysonotfnull��� ��� TEXT� m   � ��� ��� 2 Y o u ' r e   a l r e a d y   b a c k i n g   u p� �_��^
�_ 
appr� m   � ��� ��� 
 W B T F U�^  �a  �n  �t  �s  �u  �y  ��  � ��� l     �]�\�[�]  �\  �[  � ��� l     �Z�Y�X�Z  �Y  �X  � ��� l     �W�V�U�W  �V  �U  � ��� l     �T�S�R�T  �S  �R  � ��� l     �Q�P�O�Q  �P  �O  � ��� i  ����� I      �N��M�N  0 nsfwonhandler_ nsfwOnHandler_� ��L� o      �K�K 
0 sender  �L  �M  � k     ;�� ��� r     � � m     �J
�J boovtrue  o      �I�I 0 nsfw  � �H Z    ;�G l   �F�E =     o    �D�D 0 notifications   m    �C
�C boovtrue�F  �E   r    	 J    

  m     �  B a c k u p   n o w  m     � $ T u r n   o f f   t h e   b a n t s  m     � , T u r n   o f f   n o t i f i c a t i o n s �B m     �  Q u i t�B  	 o      �A�A 0 thelist theList  l  ! (�@�? =   ! ( o   ! &�>�> 0 notifications   m   & '�=
�= boovfals�@  �?   �< r   + 7 !  J   + 1"" #$# m   + ,%% �&&  B a c k u p   n o w$ '(' m   , -)) �** $ T u r n   o f f   t h e   b a n t s( +,+ m   - .-- �.. * T u r n   o n   n o t i f i c a t i o n s, /�;/ m   . /00 �11  Q u i t�;  ! o      �:�: 0 thelist theList�<  �G  �H  � 232 l     �9�8�7�9  �8  �7  3 454 i  ��676 I      �68�5�6 "0 nsfwoffhandler_ nsfwOffHandler_8 9�49 o      �3�3 
0 sender  �4  �5  7 k     ;:: ;<; r     =>= m     �2
�2 boovfals> o      �1�1 0 nsfw  < ?�0? Z    ;@AB�/@ l   C�.�-C =    DED o    �,�, 0 notifications  E m    �+
�+ boovtrue�.  �-  A r    FGF J    HH IJI m    KK �LL  B a c k u p   n o wJ MNM m    OO �PP " T u r n   o n   t h e   b a n t sN QRQ m    SS �TT , T u r n   o f f   n o t i f i c a t i o n sR U�*U m    VV �WW  Q u i t�*  G o      �)�) 0 thelist theListB XYX l  ! (Z�(�'Z =   ! ([\[ o   ! &�&�& 0 notifications  \ m   & '�%
�% boovfals�(  �'  Y ]�$] r   + 7^_^ J   + 1`` aba m   + ,cc �dd  B a c k u p   n o wb efe m   , -gg �hh " T u r n   o n   t h e   b a n t sf iji m   - .kk �ll * T u r n   o n   n o t i f i c a t i o n sj m�#m m   . /nn �oo  Q u i t�#  _ o      �"�" 0 thelist theList�$  �/  �0  5 pqp l     �!� ��!  �   �  q rsr l     ����  �  �  s tut l     ����  �  �  u vwv l     ����  �  �  w xyx l     ����  �  �  y z{z i  ��|}| I      �~�� 00 notificationonhandler_ notificationOnHandler_~ � o      �� 
0 sender  �  �  } k     ;�� ��� r     ��� m     �
� boovtrue� o      �� 0 notifications  � ��� Z    ;����� l   ��
�	� =    ��� o    �� 0 nsfw  � m    �
� boovtrue�
  �	  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� , T u r n   o f f   n o t i f i c a t i o n s� ��� m    �� ���  Q u i t�  � o      �� 0 thelist theList� ��� l  ! (���� =   ! (��� o   ! &�� 0 nsfw  � m   & '�
� boovfals�  �  � �� � r   + 7��� J   + 1�� ��� m   + ,�� ���  B a c k u p   n o w� ��� m   , -�� ��� " T u r n   o n   t h e   b a n t s� ��� m   - .�� ��� , T u r n   o f f   n o t i f i c a t i o n s� ���� m   . /�� ���  Q u i t��  � o      ���� 0 thelist theList�   �  �  { ��� l     ��������  ��  ��  � ��� i  ����� I      ������� 20 notificationoffhandler_ notificationOffHandler_� ���� o      ���� 
0 sender  ��  ��  � k     ;�� ��� r     ��� m     ��
�� boovfals� o      ���� 0 notifications  � ���� Z    ;������ l   ������ =    ��� o    ���� 0 nsfw  � m    ��
�� boovtrue��  ��  � r    ��� J    �� ��� m    �� ���  B a c k u p   n o w� ��� m    �� ��� $ T u r n   o f f   t h e   b a n t s� ��� m    �� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m    �� ���  Q u i t��  � o      ���� 0 thelist theList� ��� l  ! (������ =   ! (��� o   ! &���� 0 nsfw  � m   & '��
�� boovfals��  ��  � ���� r   + 7��� J   + 1�� ��� m   + ,�� ���  B a c k u p   n o w� ��� m   , -�� ��� " T u r n   o n   t h e   b a n t s� ��� m   - .�� ��� * T u r n   o n   n o t i f i c a t i o n s� ���� m   . /�� ���  Q u i t��  � o      ���� 0 thelist theList��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ��   I      ������ 0 quithandler_ quitHandler_ �� o      ���� 
0 sender  ��  ��   Z     u�� l    ���� =     	 o     ���� 0 isbackingup isBackingUp	 m    ��
�� boovtrue��  ��   k   
 4

  I   
 �������� 0 setfocus setFocus��  ��    r      l   ���� n     1    ��
�� 
bhit l   ���� I   ��
�� .sysodlogaskr        TEXT m     � � Y o u ' r e   b a c k i n g   t h e   f u c k   u p   r i g h t   n o w . 
 A r e   y o u   s u r e   y o u   w a n t   t o   c a n c e l   a n d   q u i t ? ��
�� 
btns J      m     �  C a n c e l   &   Q u i t  ��  m    !! �""  R e s u m e��   ��#��
�� 
dflt# m    ���� ��  ��  ��  ��  ��   o      ���� 	0 reply   $��$ Z   ! 4%&��'% l  ! $(����( =   ! $)*) o   ! "���� 	0 reply  * m   " #++ �,,  C a n c e l   &   Q u i t��  ��  & I  ' ,��-��
�� .aevtquitnull��� ��� null- m   ' (..                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  ' I  / 4��/��
�� .ascrcmnt****      � ****/ l  / 00����0 m   / 011 �22  W B T F U   r e s u m e d��  ��  ��  ��   343 l  7 >5����5 =   7 >676 o   7 <���� 0 isbackingup isBackingUp7 m   < =��
�� boovfals��  ��  4 8��8 k   A q99 :;: I   A F�������� 0 setfocus setFocus��  ��  ; <=< r   G Y>?> l  G W@����@ n   G WABA 1   U W��
�� 
bhitB l  G UC����C I  G U��DE
�� .sysodlogaskr        TEXTD m   G HFF �GG < A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ?E ��HI
�� 
btnsH J   I OJJ KLK m   I JMM �NN  Q u i tL O��O m   J MPP �QQ  R e s u m e��  I ��R��
�� 
dfltR m   P Q���� ��  ��  ��  ��  ��  ? o      ���� 	0 reply  = S��S Z   Z qTU��VT l  Z _W����W =   Z _XYX o   Z [���� 	0 reply  Y m   [ ^ZZ �[[  Q u i t��  ��  U I  b g��\��
�� .aevtquitnull��� ��� null\ m   b c]]                                                                                      @ alis    �  W00721ML                   ���H+  Q7�Woah, Back The Fuck Up.app                                     x�����T        ����  	                Desktop     ���      ���T    Q7�Q7� ��  DW00721ML:Users: robert.tesalona: Desktop: Woah, Back The Fuck Up.app  6  W o a h ,   B a c k   T h e   F u c k   U p . a p p    W 0 0 7 2 1 M L  8Users/robert.tesalona/Desktop/Woah, Back The Fuck Up.app  /    ��  ��  ��  V I  j q��^��
�� .ascrcmnt****      � ****^ l  j m_����_ m   j m`` �aa  W B T F U   r e s u m e d��  ��  ��  ��  ��  ��  � bcb l     ��������  ��  ��  c ded l     ��������  ��  ��  e fgf l     ��������  ��  ��  g hih l     ��������  ��  ��  i jkj l     ��������  ��  ��  k lml i  ��non I      �������� (0 animatemenubaricon animateMenuBarIcon��  ��  o k     .pp qrq l      ��st��  s�~set counter to 0	set max to 10		repeat while isBackingUp is true		delay 0.1				if (counter = max) then			set counter to 1		else			set counter to counter + 1		end if				set imagePath to alias ((path to me as text) & "Contents:Resources:menubar_animated_" & counter & ".png")		set imagePath to POSIX path of imagePath		set image to current application's NSImage's alloc()'s initWithContentsOfFile:imagePath				--set image to current application's NSImage's alloc()'s initWithContentsOfFile:("/Users/robert.tesalona/Desktop/_temp/WBTFU/black/menubar_animated_" & counter & ".png")				StatusItem's setImage:image	end repeat   t �uu� s e t   c o u n t e r   t o   0  	 s e t   m a x   t o   1 0  	  	 r e p e a t   w h i l e   i s B a c k i n g U p   i s   t r u e  	 	 d e l a y   0 . 1  	 	  	 	 i f   ( c o u n t e r   =   m a x )   t h e n  	 	 	 s e t   c o u n t e r   t o   1  	 	 e l s e  	 	 	 s e t   c o u n t e r   t o   c o u n t e r   +   1  	 	 e n d   i f  	 	  	 	 s e t   i m a g e P a t h   t o   a l i a s   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : m e n u b a r _ a n i m a t e d _ "   &   c o u n t e r   &   " . p n g " )  	 	 s e t   i m a g e P a t h   t o   P O S I X   p a t h   o f   i m a g e P a t h  	 	 s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : i m a g e P a t h  	 	  	 	 - - s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : ( " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / W B T F U / b l a c k / m e n u b a r _ a n i m a t e d _ "   &   c o u n t e r   &   " . p n g " )  	 	  	 	 S t a t u s I t e m ' s   s e t I m a g e : i m a g e  	 e n d   r e p e a tr vwv l     �������  ��  �  w xyx r     z{z 4     �~|
�~ 
alis| l   }�}�|} b    ~~ l   	��{�z� I   	�y��
�y .earsffdralis        afdr�  f    � �x��w
�x 
rtyp� m    �v
�v 
ctxt�w  �{  �z   m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ a c t i v e . p n g�}  �|  { o      �u�u 0 	imagepath 	imagePathy ��� r    ��� n    ��� 1    �t
�t 
psxp� o    �s�s 0 	imagepath 	imagePath� o      �r�r 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�q��p�q 20 initwithcontentsoffile_ initWithContentsOfFile_� ��o� o    �n�n 0 	imagepath 	imagePath�o  �p  � n   ��� I    �m�l�k�m 	0 alloc  �l  �k  � n   ��� o    �j�j 0 nsimage NSImage� m    �i
�i misccura� o      �h�h 	0 image  � ��g� n  $ .��� I   ) .�f��e�f 0 	setimage_ 	setImage_� ��d� o   ) *�c�c 	0 image  �d  �e  � o   $ )�b�b 0 
statusitem 
StatusItem�g  m ��� l     �a�`�_�a  �`  �_  � ��� l     �^�]�\�^  �]  �\  � ��� l     �[�Z�Y�[  �Z  �Y  � ��� l     �X�W�V�X  �W  �V  � ��� l     �U�T�S�U  �T  �S  � ��� i  ����� I      �R�Q�P�R $0 resetmenubaricon resetMenuBarIcon�Q  �P  � k     .�� ��� r     ��� 4     �O�
�O 
alis� l   ��N�M� b    ��� l   	��L�K� I   	�J��
�J .earsffdralis        afdr�  f    � �I��H
�I 
rtyp� m    �G
�G 
ctxt�H  �L  �K  � m   	 
�� ��� J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�N  �M  � o      �F�F 0 	imagepath 	imagePath� ��� r    ��� n    ��� 1    �E
�E 
psxp� o    �D�D 0 	imagepath 	imagePath� o      �C�C 0 	imagepath 	imagePath� ��� r    #��� n   !��� I    !�B��A�B 20 initwithcontentsoffile_ initWithContentsOfFile_� ��@� o    �?�? 0 	imagepath 	imagePath�@  �A  � n   ��� I    �>�=�<�> 	0 alloc  �=  �<  � n   ��� o    �;�; 0 nsimage NSImage� m    �:
�: misccura� o      �9�9 	0 image  � ��8� n  $ .��� I   ) .�7��6�7 0 	setimage_ 	setImage_� ��5� o   ) *�4�4 	0 image  �5  �6  � o   $ )�3�3 0 
statusitem 
StatusItem�8  � ��� l     �2�1�0�2  �1  �0  � ��� l     �/�.�-�/  �.  �-  � ��� l     �,�+�*�,  �+  �*  � ��� l     �)�(�'�)  �(  �'  � ��� l     �&�%�$�&  �%  �$  � ��� l     �#���#  �   create an NSStatusBar   � ��� ,   c r e a t e   a n   N S S t a t u s B a r� ��� i  ����� I      �"�!� �" 0 makestatusbar makeStatusBar�!  �   � k     r�� ��� l     ����  �  log ("Make status bar")   � ��� . l o g   ( " M a k e   s t a t u s   b a r " )� ��� r     ��� n    ��� o    �� "0 systemstatusbar systemStatusBar� n    ��� o    �� 0 nsstatusbar NSStatusBar� m     �
� misccura� o      �� 0 bar  � ��� l   ����  �  �  � ��� r    ��� n   ��� I   	 ���� .0 statusitemwithlength_ statusItemWithLength_� ��� m   	 
�� ��      �  �  � o    	�� 0 bar  � o      �� 0 
statusitem 
StatusItem� ��� l   ����  �  �  � � � r    # 4    !�
� 
alis l    �� b      l   �� I   �
	
�
 .earsffdralis        afdr  f    	 �	
�
�	 
rtyp
 m    �
� 
ctxt�  �  �   m     � J C o n t e n t s : R e s o u r c e s : m e n u b a r _ s t a t i c . p n g�  �   o      �� 0 	imagepath 	imagePath   r   $ ) n   $ ' 1   % '�
� 
psxp o   $ %�� 0 	imagepath 	imagePath o      �� 0 	imagepath 	imagePath  r   * 8 n  * 6 I   1 6��� 20 initwithcontentsoffile_ initWithContentsOfFile_ �  o   1 2���� 0 	imagepath 	imagePath�   �   n  * 1 I   - 1�������� 	0 alloc  ��  ��   n  * - o   + -���� 0 nsimage NSImage m   * +��
�� misccura o      ���� 	0 image     l  9 9��������  ��  ��    !"! l  9 9��#$��  # � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   $ �%% s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "" &'& l  9 9��������  ��  ��  ' ()( n  9 C*+* I   > C��,���� 0 	setimage_ 	setImage_, -��- o   > ?���� 	0 image  ��  ��  + o   9 >���� 0 
statusitem 
StatusItem) ./. l  D D��������  ��  ��  / 010 l  D D��23��  2 , & set up the initial NSStatusBars title   3 �44 L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l e1 565 l  D D��78��  7 # StatusItem's setTitle:"WBTFU"   8 �99 : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "6 :;: l  D D��������  ��  ��  ; <=< l  D D��>?��  > 1 + set up the initial NSMenu of the statusbar   ? �@@ V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r= ABA r   D XCDC n  D REFE I   K R��G����  0 initwithtitle_ initWithTitle_G H��H m   K NII �JJ  C u s t o m��  ��  F n  D KKLK I   G K�������� 	0 alloc  ��  ��  L n  D GMNM o   E G���� 0 nsmenu NSMenuN m   D E��
�� misccuraD o      ���� 0 newmenu newMenuB OPO l  Y Y��������  ��  ��  P QRQ n  Y cSTS I   ^ c��U���� 0 setdelegate_ setDelegate_U V��V  f   ^ _��  ��  T o   Y ^���� 0 newmenu newMenuR WXW l  d d��YZ��  Y � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   Z �[[0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .X \]\ l  d d��������  ��  ��  ] ^��^ n  d r_`_ I   i r��a���� 0 setmenu_ setMenu_a b��b o   i n���� 0 newmenu newMenu��  ��  ` o   d i���� 0 
statusitem 
StatusItem��  � cdc l     ��������  ��  ��  d efe l     ��gh��  g � �-----------------------------------------------------------------------------------------------------------------------------------------------   h �ii - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -f jkj l     ��lm��  l � �-----------------------------------------------------------------------------------------------------------------------------------------------   m �nn - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -k opo l     ��������  ��  ��  p qrq l     ��������  ��  ��  r sts l     ��������  ��  ��  t uvu l     ��������  ��  ��  v wxw l     ��������  ��  ��  x yzy l     ��������  ��  ��  z {|{ i  ��}~} I      �������� 0 runonce runOnce��  ��  ~ k      ��� Z     ������� H     
�� c     	��� l    ������ n    ��� I    �������� 0 ismainthread isMainThread��  ��  � n    ��� o    ���� 0 nsthread NSThread� m     ��
�� misccura��  ��  � m    ��
�� 
bool� k    �� ��� l   ������  � b \display alert "This script must be run from the main thread." buttons {"Cancel"} as critical   � ��� � d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l� ���� l   ������  �  error number -128   � ��� " e r r o r   n u m b e r   - 1 2 8��  ��  ��  � ��� l   ��������  ��  ��  � ���� I    �������� 0 	plistinit 	plistInit��  ��  ��  | ��� l     ��������  ��  ��  � ��� l    ������ n    ��� I    �������� 0 makestatusbar makeStatusBar��  ��  �  f     ��  ��  � ��� l   ������ I    ������� 0 runonce runOnce��  �  ��  ��  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  � w q Function that is always running in the background. This doesn't need to get called as it is running from the off   � ��� �   F u n c t i o n   t h a t   i s   a l w a y s   r u n n i n g   i n   t h e   b a c k g r o u n d .   T h i s   d o e s n ' t   n e e d   t o   g e t   c a l l e d   a s   i t   i s   r u n n i n g   f r o m   t h e   o f f� ��� l     �~���~  � � � This function in particular 'runs' every 60 seconds and checks to see if a backup is needed as long as the current time is before 17:00 and checks if the backup needs to run every 15 or 30 minutes or every hour on the hour respectively.   � ����   T h i s   f u n c t i o n   i n   p a r t i c u l a r   ' r u n s '   e v e r y   6 0   s e c o n d s   a n d   c h e c k s   t o   s e e   i f   a   b a c k u p   i s   n e e d e d   a s   l o n g   a s   t h e   c u r r e n t   t i m e   i s   b e f o r e   1 7 : 0 0   a n d   c h e c k s   i f   t h e   b a c k u p   n e e d s   t o   r u n   e v e r y   1 5   o r   3 0   m i n u t e s   o r   e v e r y   h o u r   o n   t h e   h o u r   r e s p e c t i v e l y .� ��}� i  ����� I     �|�{�z
�| .miscidlenmbr    ��� null�{  �z  � k     ��� ��� Z     ����y�x� l    ��w�v� E     ��� o     �u�u &0 displaysleepstate displaySleepState� o    
�t�t 0 strawake strAwake�w  �v  � Z    ����s�r� l   ��q�p� =    ��� o    �o�o 0 isbackingup isBackingUp� m    �n
�n boovfals�q  �p  � Z    �����m� l   ��l�k� =    ��� o    �j�j 0 manualbackup manualBackup� m    �i
�i boovfals�l  �k  � Z   " s���h�g� l  " +��f�e� A   " +��� l  " )��d�c� n   " )��� 1   ' )�b
�b 
hour� l  " '��a�`� I  " '�_�^�]
�_ .misccurdldt    ��� null�^  �]  �a  �`  �d  �c  � m   ) *�\�\ �f  �e  � k   . o�� ��� r   . 7��� l  . 5��[�Z� n   . 5��� 1   3 5�Y
�Y 
min � l  . 3��X�W� l  . 3��V�U� I  . 3�T�S�R
�T .misccurdldt    ��� null�S  �R  �V  �U  �X  �W  �[  �Z  � o      �Q�Q 0 m  � ��� l  8 8�P�O�N�P  �O  �N  � ��M� Z   8 o����L� G   8 C��� l  8 ;��K�J� =   8 ;��� o   8 9�I�I 0 m  � m   9 :�H�H �K  �J  � l  > A��G�F� =   > A��� o   > ?�E�E 0 m  � m   ? @�D�D -�G  �F  � Z   F U���C�B� l  F I��A�@� =   F I��� o   F G�?�? 0 scheduledtime scheduledTime� m   G H�>�> �A  �@  � I   L Q�=�<�;�= 0 	plistinit 	plistInit�<  �;  �C  �B  � ��� G   X c��� l  X [��:�9� =   X [��� o   X Y�8�8 0 m  � m   Y Z�7�7  �:  �9  � l  ^ a��6�5� =   ^ a��� o   ^ _�4�4 0 m  � m   _ `�3�3 �6  �5  � ��2� I   f k�1�0�/�1 0 	plistinit 	plistInit�0  �/  �2  �L  �M  �h  �g  � ��� l  v } �.�-  =   v } o   v {�,�, 0 manualbackup manualBackup m   { |�+
�+ boovtrue�.  �-  � �* I   � ��)�(�'�) 0 	plistinit 	plistInit�(  �'  �*  �m  �s  �r  �y  �x  �  l  � ��&�%�$�&  �%  �$    l   � ��#	�#  ��if (isBackingUp = true) then		if (iconCounter = iconMax) then			set iconCounter to 1		else			set iconCounter to iconCounter + 1		end if				set imagePath to alias ((path to me as text) & "Contents:Resources:menubar_animated_" & iconCounter & ".png")		set imagePath to POSIX path of imagePath		set image to current application's NSImage's alloc()'s initWithContentsOfFile:imagePath				StatusItem's setImage:image	end if   	 �

Z i f   ( i s B a c k i n g U p   =   t r u e )   t h e n  	 	 i f   ( i c o n C o u n t e r   =   i c o n M a x )   t h e n  	 	 	 s e t   i c o n C o u n t e r   t o   1  	 	 e l s e  	 	 	 s e t   i c o n C o u n t e r   t o   i c o n C o u n t e r   +   1  	 	 e n d   i f  	 	  	 	 s e t   i m a g e P a t h   t o   a l i a s   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : m e n u b a r _ a n i m a t e d _ "   &   i c o n C o u n t e r   &   " . p n g " )  	 	 s e t   i m a g e P a t h   t o   P O S I X   p a t h   o f   i m a g e P a t h  	 	 s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : i m a g e P a t h  	 	  	 	 S t a t u s I t e m ' s   s e t I m a g e : i m a g e  	 e n d   i f  l  � ��"�!� �"  �!  �     l  � ���    
return 0.2    �  r e t u r n   0 . 2 � L   � � m   � ��� �  �}       3�� g����� ��!"#$%&'()*+,-./0123456789:;<�   1������������
�	��������� ������������������������������������������������������
� 
pimr� 0 
statusitem 
StatusItem� 0 
thedisplay 
theDisplay� 0 defaults  � $0 internalmenuitem internalMenuItem� $0 externalmenuitem externalMenuItem� 0 newmenu newMenu� 0 thelist theList� 0 nsfw  � 0 notifications  � 	0 plist  �
 0 initialbackup initialBackup�	 0 manualbackup manualBackup� 0 isbackingup isBackingUp� "0 messagesmissing messagesMissing� *0 messagesencouraging messagesEncouraging� $0 messagescomplete messagesComplete� &0 messagescancelled messagesCancelled� 40 messagesalreadybackingup messagesAlreadyBackingUp� 0 strawake strAwake� 0 strsleep strSleep�  &0 displaysleepstate displaySleepState�� 0 	plistinit 	plistInit�� 0 diskinit diskInit�� 0 freespaceinit freeSpaceInit�� 0 
backupinit 
backupInit�� 0 tidyup tidyUp�� 
0 backup  �� 0 itexists itExists�� .0 getcomputeridentifier getComputerIdentifier�� 0 gettimestamp getTimestamp�� 0 comparesizes compareSizes�� 0 	stopwatch  �� 0 getduration getDuration�� 0 setfocus setFocus�� $0 menuneedsupdate_ menuNeedsUpdate_�� 0 	makemenus 	makeMenus��  0 backuphandler_ backupHandler_��  0 nsfwonhandler_ nsfwOnHandler_�� "0 nsfwoffhandler_ nsfwOffHandler_�� 00 notificationonhandler_ notificationOnHandler_�� 20 notificationoffhandler_ notificationOffHandler_�� 0 quithandler_ quitHandler_�� (0 animatemenubaricon animateMenuBarIcon�� $0 resetmenubaricon resetMenuBarIcon�� 0 makestatusbar makeStatusBar�� 0 runonce runOnce
�� .miscidlenmbr    ��� null
�� .aevtoappnull  �   � **** ��=�� =  >?@A> �� L��
�� 
vers��  ? ��B��
�� 
cobjB CC   ��
�� 
osax��  @ ��D��
�� 
cobjD EE   �� U
�� 
frmk��  A ��F��
�� 
cobjF GG   �� [
�� 
frmk��  
� 
msng HH ����I
�� misccura
�� 
pclsI �JJ  N S U s e r D e f a u l t s KK ����L
�� misccura
�� 
pclsL �MM  N S M e n u I t e m NN ����O
�� misccura
�� 
pclsO �PP  N S M e n u I t e m QQ ����R
�� misccura
�� 
pclsR �SS  N S M e n u ��T�� T   � � � �
� boovfals
� boovtrue �UU � / U s e r s / r o b e r t . t e s a l o n a / L i b r a r y / P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t
� boovtrue
� boovfals
� boovfals ��V�� V   � � � �  ��W�� 	W 	 
"&) ��X�� 	X 	 37;?CGKOR ��Y�� Y  \`dhlptw  ��Z�� 	Z 	 ���������! �[[�             |   |       " I O P o w e r M a n a g e m e n t "   =   { " C a p a b i l i t y F l a g s " = 3 2 8 3 2 , " M a x P o w e r S t a t e " = 4 , " A c t i v i t y T i c k l e s " = 2 3 3 7 7 8 , " D e v i c e P o w e r S t a t e " = 4 , " I d l e T i m e r P e r i o d " = 3 5 8 4 0 0 0 , " T i m e S i n c e L a s t T i c k l e " = 1 1 0 , " I d l e T i m e r E l a p s e d T i m e " = 6 0 5 2 7 9 , " C u r r e n t P o w e r S t a t e " = 4 }" �������\]���� 0 	plistinit 	plistInit��  ��  \ ����������������� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk��  0 computerfolder computerFolder�� 0 
backuptime 
backupTime�� 0 thecontents theContents�� 0 thevalue theValue�� 0 backuptimes backupTimes� 0 selectedtime selectedTime] 1��)������������C�N��gkn{�������������������������� 0 itexists itExists
� 
plif
� 
pcnt
� 
valL�  0 foldertobackup FolderToBackup� 0 backupdrive BackupDrive�  0 computerfolder computerFolder� 0 scheduledtime scheduledTime� .0 getcomputeridentifier getComputerIdentifier�  ��� 0 setfocus setFocus
� 
prmp
� .sysostflalis    ��� null
� 
TEXT
� 
psxp
� .gtqpchltns    @   @ ns  � � � <
� 
kocl
� 
prdt
� 
pnam� 
� .corecrel****      � null
� 
plii
� 
insh
� 
kind� � 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 machinefolder machineFolder
� .ascrcmnt****      � ****� 0 diskinit diskInit���jE�O*�b  
l+ e  6� .*�b  
/�,E�O��,E�O��,E�O��,E�O��,E�O��,E�OPUYV*j+ 
E�O� ��n*j+ O*��l E�O*�a l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hOPoUO� �*a �a  a !b  
la " # �*a a $a %*6a  a &a a !a '�a (a ( #O*a a $a %*6a  a &a a !a )�a (a ( #O*a a $a %*6a  a &a a !a *�a (a ( #O*a a $a %*6a  a &a a !a +�a (a ( #UUO�E` ,O�E` -O�E` .O�E�O_ ,_ -_ .�a "vj /O*j+ 0# �2��^_�� 0 diskinit diskInit�  �  ^ ��� 0 msg  � 	0 reply  _ <������Y\����f������� "0 destinationdisk destinationDisk� 0 itexists itExists� 0 freespaceinit freeSpaceInit� 0 setfocus setFocus
� 
cobj
� 
btns
� 
dflt� 
� .sysodlogaskr        TEXT
� 
bhit� 0 diskinit diskInit
� 
appr
� .sysonotfnull��� ��� TEXT� �*��l+ e  *fk+ Y �*j+ Ob  �.E�O����lv�l� �,E�O��  
*j+ Y [b  b   Lb  �.E�Ob  	e  4b  e  ��a l Y b  f  a �a l Y hY hY h$ ����`a�� 0 freespaceinit freeSpaceInit� �b� b  �� 	0 again  �  ` ���� 	0 again  � 
0 reply1  � 0 msg  a ����~�}��|���{�z�y�x����w�v�u �t*-� 0 sourcefolder sourceFolder� "0 destinationdisk destinationDisk� 0 comparesizes compareSizes�~ 0 
backupinit 
backupInit�} 0 setfocus setFocus
�| 
btns
�{ 
dflt�z 
�y .sysodlogaskr        TEXT
�x 
bhit�w 0 tidyup tidyUp
�v 
cobj
�u 
appr
�t .sysonotfnull��� ��� TEXT� �*��l+ e  
*j+ Y �*j+ O�f  ����lv�l� �,E�Y �e  ����lv�l� �,E�Y hO�a   
*j+ Y ab  b   b  a .E�Y hOb  	e  8b  e  �a a l Y b  f  a a a l Y hY h% �sF�r�qcd�p�s 0 
backupinit 
backupInit�r  �q  c  d �o�n�m^c�lq�k�j�i�h�go�f�e~��d�c���b�������a�`
�o 
psxf�n "0 destinationdisk destinationDisk�m 	0 drive  �l 0 itexists itExists
�k 
kocl
�j 
cfol
�i 
insh
�h 
prdt
�g 
pnam�f 
�e .corecrel****      � null�d 0 machinefolder machineFolder
�c 
cdis�b 0 backupfolder backupFolder�a 0 latestfolder latestFolder�` 
0 backup  �p �*��/E�O*���%l+ f  � *�������l� UY hO*��a %_ %l+ f  $� *���*a �/�a /��_ l� UY hO� *a �/�a /�_ /E` OPUO*a �a %_ %a %l+ f  � *���_ ��a l� UY 	fEc  O� *a �/�a /�_ /�a /E` OPUO*j+ & �_!�^�]ef�\�_ 0 tidyup tidyUp�^  �]  e �[�Z�Y�X�W�V�U�T�[ 0 bf bF�Z 0 creationdates creationDates�Y 0 theoldestdate theOldestDate�X 0 j  �W 0 i  �V 0 thisdate thisDate�U 
0 reply2  �T 0 msg  f &�S�R�Q-�P�O=�N�M�L�K�Jk�I�H�G��F���E�D�C�B��A�@�?��>���=�<&)
�S 
psxf�R "0 destinationdisk destinationDisk�Q 	0 drive  
�P 
cdis
�O 
cfol�N 0 machinefolder machineFolder
�M 
cobj
�L 
ascd
�K .corecnte****       ****
�J 
pnam
�I .ascrcmnt****      � ****
�H .coredeloobj        obj �G 0 setfocus setFocus
�F 
btns
�E 
dflt�D 
�C .sysodlogaskr        TEXT
�B 
bhit
�A 
trsh
�@ .fndremptnull��� ��� obj 
�? 
appr
�> .sysonotfnull��� ��� TEXT�= 0 freespaceinit freeSpaceInit
�< .sysodelanull��� ��� nmbr�\{*��/E�O�p*��/��/��/E�O��-�,E�O��k/E�OkE�O Bl�j 
kh ��/E�O*�/�,� �� �E�O�E�O��/j Y hY h[OY��O��k/j O*j+ Oa a a a lva la  a ,E�O�a   *a ,j Y _b  b   Pb  �.E�Ob  	e  8b  e  �a a l Y b  f  a a a l Y hY hY hO*ek+  Ob  b   Tb  	e  Fb  e  a !a a "l Okj #Y #b  f  a $a a %l Okj #Y hY hY hU' �;J�:�9gh�8�; 
0 backup  �:  �9  g 
�7�6�5�4�3�2�1�0�/�.�7 0 t  �6 0 x  �5 "0 containerfolder containerFolder�4 0 
foldername 
folderName�3 0 d  �2 0 duration  �1 0 msg  �0 0 c  �/ 0 oldestfolder oldestFolder�. 0 todelete toDeleteh e�-�,g�+
.�*�)�(�'�&�%�$�#�"�!� ���������;�>�HK�fhj|~�������������
!#%-�Xgj���������		'	)	,	?	A	D	Y	[	^	q	s	v	�	�	�	�	�	�	�	�	�	�




 
"
%
4�- 0 gettimestamp getTimestamp�, (0 animatemenubaricon animateMenuBarIcon�+ 0 	stopwatch  
�* 
psxf�) 0 sourcefolder sourceFolder
�( 
TEXT
�' 
cfol
�& 
pnam
�% 
kocl
�$ 
insh�# 0 backupfolder backupFolder
�" 
prdt�! 
�  .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � 0 machinefolder machineFolder� (0 activebackupfolder activeBackupFolder
� .misccurdldt    ��� null
� 
time� 0 	starttime 	startTime
� 
appr
� .sysonotfnull��� ��� TEXT� "0 destinationdisk destinationDisk
� .sysoexecTEXT���     TEXT� 0 endtime endTime� 0 getduration getDuration
� 
cobj
� .sysodelanull��� ��� nmbr� $0 resetmenubaricon resetMenuBarIcon
� .ascrcmnt****      � ****
� 
alis
� 
psxp�85*ek+  E�OeEc  O*j+ O*�k+ O�
*��/�&E�O*�/�,E�Ob  f  T*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�_ /�/E` O*���_ ��l� Y hOb  e >*j a ,E` Ob  b   Hb  	e  :b  e  a a a l Y b  f  a a a l Y hY hY hO_ a  %_ %a !%�%a "%�&E�Oa #�%a $%�%a %%j &OfEc  OfEc  Ob  b   �*j a ,E` 'O)j+ (E�Ob  a ).E�Ob  	e  Tb  e   a *�%a +%�%a a ,l Okj -Y )b  f  a .�%a /%a a 0l Okj -Y hY hO)j+ 1Y hYTb  f I_ a 2%_ %a 3%�%a 4%�%a 5%�&E�O_ a 6%_ %a 7%�%a 8%�&E�Oa 9�%j :Ob  b   l*j a ,E` Ob  a ).E�Ob  	e  Db  e  �a a ;l Okj -Y #b  f  a <a a =l Okj -Y hY hY hOa >�%a ?%�%a @%�%a A%j &OfEc  OfEc  O*�/a B&a )-jv [��/�&E�O�a C,�&E�Oa D�%j :Oa E�%a F%E�O�j &Ob  b  *j a ,E` 'O)j+ (E�Ob  a ).E�Ob  	e  �a G  Tb  e   a H�%a I%�%a a Jl Okj -Y )b  f  a K�%a L%a a Ml Okj -Y hY Qb  e   a N�%a O%�%a a Pl Okj -Y )b  f  a Q�%a R%a a Sl Okj -Y hOb  e  a Ta a Ul Y b  f  a Va a Wl Y hY hO)j+ 1Y hY �b  b   �*j a ,E` 'O)j+ (E�Ob  a ).E�Ob  	e  ��a G  Tb  e   a X�%a Y%�%a a Zl Okj -Y )b  f  a [�%a \%a a ]l Okj -Y hY Qb  e   a ^�%a _%�%a a `l Okj -Y )b  f  a a�%a b%a a cl Okj -Y hY hY hO)j+ 1Y hUO*a dk+ ( �
^�
�	ij�� 0 itexists itExists�
 �k� k  ��� 0 
objecttype 
objectType� 
0 object  �	  i ��� 0 
objecttype 
objectType� 
0 object  j 
�
n��
|� 
���
� 
cdis
� .coredoexnull���     ****
�  
file
�� 
cfol� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU) ��
�����lm���� .0 getcomputeridentifier getComputerIdentifier��  ��  l �������� 0 computername computerName�� "0 strserialnumber strSerialNumber��  0 identifiername identifierNamem ����
���
�
�� .sysosigtsirr   ��� null
�� 
sicn
�� .sysoexecTEXT���     TEXT�� *j  �,E�O�j E�O��%�%E�O�* ��
�����no���� 0 gettimestamp getTimestamp�� ��p�� p  ���� 0 isfolder isFolder��  n ���������������������������������� 0 isfolder isFolder�� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  o ������������������������������M\������{����������%
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� misccura
�� 
psof
�� 
psin�� 
�� .sysooffslong    ��� null
�� 
cha ���*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�Oa  *a a a �a  UE�O�[a \[Z�k\62�&E�O�e  a �%�%�%a %�%�%�%E�Y �f  ��%�%a %�%�%�%E�Y hO�+ ��C����qr���� 0 comparesizes compareSizes�� ��s�� s  ������ 
0 source  �� 0 destination  ��  q ��������� 
0 source  �� 0 destination  � 0 fit  � 0 
foldersize 
folderSize� 0 	freespace 	freeSpacer ��eg��������
� 
psxf
� .sysoexecTEXT���     TEXT
� misccura� � d
� .sysorondlong        doub
� 
cdis
� 
frsp
� .ascrcmnt****      � ****�� ieE�O*�/E�O� H�%�%j E�O� ��!� j U�!E�O*�/�,�!�!�!E�O� 	�� j U�!E�O��%j UO�� fE�Y hO�, ����tu�� 0 	stopwatch  � �v� v  �� 0 mode  �  t ��� 0 mode  � 0 x  u ������� 0 gettimestamp getTimestamp
� .ascrcmnt****      � ****� 6��  *fk+ E�O�%j Y ��  *fk+ E�O�%j Y hOP- ���wx�� 0 getduration getDuration�  �  w �� 0 duration  x ������� 0 endtime endTime� 0 	starttime 	startTime� <
� misccura� d
� .sysorondlong        doub� ���!E�O� 	�� j U�!E�O�. �6��yz�� 0 setfocus setFocus�  �  y  z 9�
� .miscactvnull��� ��� null� � *j U/ �d��{|�� $0 menuneedsupdate_ menuNeedsUpdate_� �}� }  �
� 
cmnu�  {  | �� 0 	makemenus 	makeMenus� )j+  0 ����~�� 0 	makemenus 	makeMenus�  �  ~ ���� 0 i  � 0 	this_item  � 0 thismenuitem thisMenuItem �����������������'58FSV���  0 removeallitems removeAllItems
� 
cobj
� 
nmbr
� 
TEXT
� misccura� 0 
nsmenuitem 
NSMenuItem� 	0 alloc  � J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_� 0 additem_ addItem_� 0 
settarget_ 
setTarget_� �b  j+  O �kb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ���m+ 
E�Y ���&�  ��,j+ ��a m+ 
E�Y f��&a   ��,j+ �a a m+ 
E�Y E��&a   ��,j+ �a a m+ 
E�Y $��&a   ��,j+ �a a m+ 
E�Y hOb  �k+ O�)k+ [OY�&1 �������~�  0 backuphandler_ backupHandler_� �}��} �  �|�| 
0 sender  �  � �{�z�{ 
0 sender  �z 0 msg  � ��y��x�w���v���
�y 
appr
�x .sysonotfnull��� ��� TEXT
�w .sysodelanull��� ��� nmbr
�v 
cobj�~ �b  f  PeEc  Ob  	e  :b  e  ���l Okj Y b  f  ���l Okj Y hY hY Qb  e  Fb  �.E�Ob  	e  .b  e  ���l Y b  f  ���l Y hY hY h2 �u��t�s���r�u  0 nsfwonhandler_ nsfwOnHandler_�t �q��q �  �p�p 
0 sender  �s  � �o�o 
0 sender  � 	�n%)-0�n �r <eEc  Ob  	e  �����vEc  Y b  	f  �����vEc  Y h3 �m7�l�k���j�m "0 nsfwoffhandler_ nsfwOffHandler_�l �i��i �  �h�h 
0 sender  �k  � �g�g 
0 sender  � 	KOSV�fcgkn�f �j <fEc  Ob  	e  �����vEc  Y b  	f  �����vEc  Y h4 �e}�d�c���b�e 00 notificationonhandler_ notificationOnHandler_�d �a��a �  �`�` 
0 sender  �c  � �_�_ 
0 sender  � 	�����^�����^ �b <eEc  	Ob  e  �����vEc  Y b  f  �����vEc  Y h5 �]��\�[���Z�] 20 notificationoffhandler_ notificationOffHandler_�\ �Y��Y �  �X�X 
0 sender  �[  � �W�W 
0 sender  � 	�����V�����V �Z <fEc  	Ob  e  �����vEc  Y b  f  �����vEc  Y h6 �U�T�S���R�U 0 quithandler_ quitHandler_�T �Q��Q �  �P�P 
0 sender  �S  � �O�N�O 
0 sender  �N 	0 reply  � �M�L!�K�J�I�H+.�G1�FFMPZ`�M 0 setfocus setFocus
�L 
btns
�K 
dflt�J 
�I .sysodlogaskr        TEXT
�H 
bhit
�G .aevtquitnull��� ��� null
�F .ascrcmnt****      � ****�R vb  e  /*j+  O����lv�l� �,E�O��  
�j Y �j Y @b  f  5*j+  O���a lv�l� �,E�O�a   
�j Y 	a j Y h7 �Eo�D�C���B�E (0 animatemenubaricon animateMenuBarIcon�D  �C  � �A�@�A 0 	imagepath 	imagePath�@ 	0 image  � �?�>�=�<��;�:�9�8�7�6
�? 
alis
�> 
rtyp
�= 
ctxt
�< .earsffdralis        afdr
�; 
psxp
�: misccura�9 0 nsimage NSImage�8 	0 alloc  �7 20 initwithcontentsoffile_ initWithContentsOfFile_�6 0 	setimage_ 	setImage_�B /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
8 �5��4�3���2�5 $0 resetmenubaricon resetMenuBarIcon�4  �3  � �1�0�1 0 	imagepath 	imagePath�0 	0 image  � �/�.�-�,��+�*�)�(�'�&
�/ 
alis
�. 
rtyp
�- 
ctxt
�, .earsffdralis        afdr
�+ 
psxp
�* misccura�) 0 nsimage NSImage�( 	0 alloc  �' 20 initwithcontentsoffile_ initWithContentsOfFile_�& 0 	setimage_ 	setImage_�2 /*�)��l �%/E�O��,E�O��,j+ �k+ 	E�Ob  �k+ 
9 �%��$�#���"�% 0 makestatusbar makeStatusBar�$  �#  � �!� ��! 0 bar  �  0 	imagepath 	imagePath� 	0 image  � ���������������I���
� misccura� 0 nsstatusbar NSStatusBar� "0 systemstatusbar systemStatusBar� .0 statusitemwithlength_ statusItemWithLength_
� 
alis
� 
rtyp
� 
ctxt
� .earsffdralis        afdr
� 
psxp� 0 nsimage NSImage� 	0 alloc  � 20 initwithcontentsoffile_ initWithContentsOfFile_� 0 	setimage_ 	setImage_� 0 nsmenu NSMenu�  0 initwithtitle_ initWithTitle_� 0 setdelegate_ setDelegate_� 0 setmenu_ setMenu_�" s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ : �~�����
� 0 runonce runOnce�  �  �  � �	����
�	 misccura� 0 nsthread NSThread� 0 ismainthread isMainThread
� 
bool� 0 	plistinit 	plistInit�
 ��,j+ �& hY hO*j+ ; �������
� .miscidlenmbr    ��� null�  �  � � �  0 m  � 
��������������������
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit�� � �b  b   �b  f  vb  f  V*j  �,� F*j  �,E�O�� 
 �� �& ��  
*j+ Y hY �j 
 �� �& 
*j+ Y hY hY b  e  
*j+ Y hY hY hO�< �����������
�� .aevtoappnull  �   � ****� k     �� ��� �����  ��  ��  �  � ������ 0 makestatusbar makeStatusBar�� 0 runonce runOnce�� )j+  O*j+  ascr  ��ޭ