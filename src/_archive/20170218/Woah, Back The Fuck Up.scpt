FasdUAS 1.101.10   ��   ��    k             l     ��  ��    * $ Copyright (C) 2017  Robert Tesalona     � 	 	 H   C o p y r i g h t   ( C )   2 0 1 7     R o b e r t   T e s a l o n a   
  
 l     ��������  ��  ��        l     ��  ��    � � This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by     �     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y      l     ��  ��    l f the Free Software Foundation, either version 3 of the License, or (at your option) any later version.     �   �   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n .      l     ��������  ��  ��        l     ��  ��    �  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of     �   �   T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l ,   b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f      l     ��   ��    q k MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.      � ! ! �   M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s .   " # " l     ��������  ��  ��   #  $ % $ l     �� & '��   & � � You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    ' � ( (   Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . %  ) * ) l     ��������  ��  ��   *  + , + l     ��������  ��  ��   ,  - . - l     ��������  ��  ��   .  / 0 / l     ��������  ��  ��   0  1 2 1 l     ��������  ��  ��   2  3 4 3 p       5 5 �� 6�� 	0 plist   6 �� 7�� 0 sourcefolder sourceFolder 7 �� 8�� "0 destinationdisk destinationDisk 8 �� 9�� 	0 drive   9 �� :�� 0 backupfolder backupFolder : �� ;�� (0 activesourcefolder activeSourceFolder ; �� <�� (0 activebackupfolder activeBackupFolder < �� =�� 0 latestfolder latestFolder = ������ 0 initialbackup initialBackup��   4  > ? > p       @ @ ������ 0 scheduledtime scheduledTime��   ?  A B A p       C C ������ 0 isbackingup isBackingUp��   B  D E D l     ��������  ��  ��   E  F G F l     H���� H r      I J I b      K L K n    	 M N M 1    	��
�� 
psxp N l     O���� O I    �� P Q
�� .earsffdralis        afdr P m     ��
�� afdrdlib Q �� R��
�� 
from R m    ��
�� fldmfldu��  ��  ��   L m   	 
 S S � T T f P r e f e r e n c e s / c o m . b o b b y j r i s m s . W o a h B a c k T h e F u c k U p . p l i s t J o      ���� 	0 plist  ��  ��   G  U V U l    W���� W r     X Y X m    ��
�� boovtrue Y o      ���� 0 initialbackup initialBackup��  ��   V  Z [ Z l    \���� \ r     ] ^ ] m    ��
�� boovfals ^ o      ���� 0 isbackingup isBackingUp��  ��   [  _ ` _ l     ��������  ��  ��   `  a b a l     ��������  ��  ��   b  c d c l     ��������  ��  ��   d  e f e l     ��������  ��  ��   f  g h g l     ��������  ��  ��   h  i j i l     �� k l��   k � �-----------------------------------------------------------------------------------------------------------------------------------------------    l � m m - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - j  n o n l     �� p q��   p � �-----------------------------------------------------------------------------------------------------------------------------------------------    q � r r - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - o  s t s l     �� u v��   u � �-- SCRIPT FUNCTIONS ---------------------------------------------------------------------------------------------------------------------    v � w w - -   S C R I P T   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - t  x y x l     ��������  ��  ��   y  z { z i      | } | I      �������� 0 	plistinit 	plistInit��  ��   } k    u ~ ~   �  q       � � �� ��� 0 
foldername 
folderName � �� ��� 0 
backupdisk 
backupDisk � ������ 0 
backuptime 
backupTime��   �  � � � r      � � � m     ����   � o      ���� 0 
backuptime 
backupTime �  � � � l   ��������  ��  ��   �  � � � Z   _ � ��� � � l    ����� � =     � � � I    �� ����� 0 itexists itExists �  � � � m     � � � � �  f i l e �  ��� � o    ���� 	0 plist  ��  ��   � m    ��
�� boovtrue��  ��   � O    ? � � � k    > � �  � � � r     � � � n     � � � 1    ��
�� 
pcnt � 4    �� �
�� 
plif � o    ���� 	0 plist   � o      ���� 0 thecontents theContents �  � � � r    " � � � n      � � � 1     ��
�� 
valL � o    ���� 0 thecontents theContents � o      ���� 0 thevalue theValue �  � � � l  # #��������  ��  ��   �  � � � r   # ( � � � n   # & � � � o   $ &����  0 foldertobackup FolderToBackup � o   # $���� 0 thevalue theValue � o      ���� 0 
foldername 
folderName �  � � � r   ) . � � � n   ) , � � � o   * ,���� 0 backupdrive BackupDrive � o   ) *���� 0 thevalue theValue � o      ���� 0 
backupdisk 
backupDisk �  � � � r   / 4 � � � n   / 2 � � � o   0 2���� 0 scheduledtime scheduledTime � o   / 0���� 0 thevalue theValue � o      ���� 0 
backuptime 
backupTime �  ��� � I  5 >� ��~
� .ascrcmnt****      � **** � J   5 : � �  � � � o   5 6�}�} 0 
foldername 
folderName �  � � � o   6 7�|�| 0 
backupdisk 
backupDisk �  ��{ � o   7 8�z�z 0 
backuptime 
backupTime�{  �~  ��   � m     � ��                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ��   � k   B_ � �  � � � O   B � � � � k   F � � �  � � � r   F O � � � l  F M ��y�x � I  F M�w�v �
�w .sysostflalis    ��� null�v   � �u ��t
�u 
prmp � m   H I � � � � � @ P l e a s e   c h o o s e   a   f o l d e r   t o   b a c k u p�t  �y  �x   � o      �s�s 0 
foldername 
folderName �  � � � r   P ] � � � c   P [ � � � l  P W ��r�q � I  P W�p�o �
�p .sysostflalis    ��� null�o   � �n ��m
�n 
prmp � m   R S � � � � � R P l e a s e   c h o o s e   t h e   h a r d   d r i v e   t o   b a c k u p   t o�m  �r  �q   � m   W Z�l
�l 
TEXT � o      �k�k 0 
backupdisk 
backupDisk �  � � � l  ^ ^�j�i�h�j  �i  �h   �  � � � r   ^ e � � � n   ^ c � � � 1   _ c�g
�g 
psxp � o   ^ _�f�f 0 
foldername 
folderName � o      �e�e 0 
foldername 
folderName �  � � � r   f m � � � n   f k � � � 1   g k�d
�d 
psxp � o   f g�c�c 0 
backupdisk 
backupDisk � o      �b�b 0 
backupdisk 
backupDisk �  � � � l  n n�a�`�_�a  �`  �_   �  � � � r   n { � � � J   n y � �  � � � m   n q � � � � � 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r �  � � � m   q t � � � � � 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r �  ��^ � m   t w � � � � � , E v e r y   h o u r   o n   t h e   h o u r�^   � o      �]�] 0 backuptimes backupTimes �  �  � r   | � c   | � J   | � �\ I  | ��[
�[ .gtqpchltns    @   @ ns   o   | }�Z�Z 0 backuptimes backupTimes �Y	�X
�Y 
prmp	 m   ~ �

 � 8 W h e n   d o   y o u   w a n t   t o   b a c k   u p ?�X  �\   m   � ��W
�W 
TEXT o      �V�V 0 selectedtime selectedTime   l  � ��U�U    log (selectedTime)    � $ l o g   ( s e l e c t e d T i m e )  l  � ��T�S�R�T  �S  �R    Z   � ��Q l  � ��P�O =   � � o   � ��N�N 0 selectedtime selectedTime m   � � � 8 E v e r y   1 5   m i n u t e s   o n   t h e   h o u r�P  �O   r   � � m   � ��M�M  o      �L�L 0 
backuptime 
backupTime   l  � �!�K�J! =   � �"#" o   � ��I�I 0 selectedtime selectedTime# m   � �$$ �%% 8 E v e r y   3 0   m i n u t e s   o n   t h e   h o u r�K  �J    &'& r   � �()( m   � ��H�H ) o      �G�G 0 
backuptime 
backupTime' *+* l  � �,�F�E, =   � �-.- o   � ��D�D 0 selectedtime selectedTime. m   � �// �00 , E v e r y   h o u r   o n   t h e   h o u r�F  �E  + 1�C1 r   � �232 m   � ��B�B <3 o      �A�A 0 
backuptime 
backupTime�C  �Q   454 l  � ��@�?�>�@  �?  �>  5 6�=6 I  � ��<7�;
�< .ascrcmnt****      � ****7 J   � �88 9:9 o   � ��:�: 0 
foldername 
folderName: ;<; o   � ��9�9 0 
backupdisk 
backupDisk< =�8= o   � ��7�7 0 
backuptime 
backupTime�8  �;  �=   � m   B C>>�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��   � ?@? l  � ��6�5�4�6  �5  �4  @ A�3A O   �_BCB O   �^DED k   �]FF GHG I  ��2�1I
�2 .corecrel****      � null�1  I �0JK
�0 
koclJ m   � ��/
�/ 
pliiK �.LM
�. 
inshL  ;   � �M �-N�,
�- 
prdtN K   �OO �+PQ
�+ 
kindP m   � ��*
�* 
TEXTQ �)RS
�) 
pnamR m   �TT �UU  F o l d e r T o B a c k u pS �(V�'
�( 
valLV o  �&�& 0 
foldername 
folderName�'  �,  H WXW I 5�%�$Y
�% .corecrel****      � null�$  Y �#Z[
�# 
koclZ m  �"
�" 
plii[ �!\]
�! 
insh\  ;  ] � ^�
�  
prdt^ K  /__ �`a
� 
kind` m   #�
� 
TEXTa �bc
� 
pnamb m  &)dd �ee  B a c k u p D r i v ec �f�
� 
valLf o  *+�� 0 
backupdisk 
backupDisk�  �  X g�g I 6]��h
� .corecrel****      � null�  h �ij
� 
kocli m  :=�
� 
pliij �kl
� 
inshk  ;  @Bl �m�
� 
prdtm K  EWnn �op
� 
kindo m  HK�
� 
TEXTp �qr
� 
pnamq m  NQss �tt  S c h e d u l e d T i m er �u�
� 
valLu o  RS�� 0 
backuptime 
backupTime�  �  �  E l  � �v�
�	v I  � ���w
� .corecrel****      � null�  w �xy
� 
koclx m   � ��
� 
plify �z�
� 
prdtz K   � �{{ �|�
� 
pnam| o   � �� �  	0 plist  �  �  �
  �	  C m   � �}}�                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  �3   � ~~ l ``��������  ��  ��   ��� r  `e��� o  `a���� 0 
foldername 
folderName� o      ���� 0 sourcefolder sourceFolder� ��� r  fk��� o  fg���� 0 
backupdisk 
backupDisk� o      ���� "0 destinationdisk destinationDisk� ��� r  lo��� o  lm���� 0 
backuptime 
backupTime� o      ���� 0 scheduledtime scheduledTime� ��� l pp��������  ��  ��  � ���� I  pu�������� 0 diskinit diskInit��  ��  ��   { ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i    ��� I      �������� 0 diskinit diskInit��  ��  � Z      ������ l    	������ =     	��� I     ������� 0 itexists itExists� ��� m    �� ���  d i s k� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    ������� 0 freespaceinit freeSpaceInit� ���� m    �� ��� 
 f a l s e��  ��  ��  � I    ����
�� .sysodlogaskr        TEXT� m    �� ��� � M o u n t   t h e   c o r r e c t   b a c k u p   h a r d   d r i v e   t o   b a c k   t h e   f u c k   u p   a n d   s t a r t   a g a i n� ����
�� 
btns� J    �� ���� m    �� ���  O K��  � �����
�� 
dflt� m    ���� ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i    ��� I      ������� 0 freespaceinit freeSpaceInit� ���� o      ���� 	0 again  ��  ��  � Z     ]������ l    	������ =     	��� I     ������� 0 comparesizes compareSizes� ��� o    ���� 0 sourcefolder sourceFolder� ���� o    ���� "0 destinationdisk destinationDisk��  ��  � m    ��
�� boovtrue��  ��  � I    �������� 0 
backupinit 
backupInit��  ��  ��  � k    ]�� ��� Z    G������ l   ������ =    ��� o    ���� 	0 again  � m    ��
�� boovfals��  ��  � r    *��� l   (������ n    (��� 1   & (��
�� 
bhit� l   &������ I   &����
�� .sysodlogaskr        TEXT� m    �� ��� � T h e r e   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� ����
�� 
btns� J     �� ��� m    �� ���  Y e s� ���� m    �� ���  C a n c e l   B a c k u p��  � �����
�� 
dflt� m   ! "���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  � ��� l  - 0������ =   - 0��� o   - .���� 	0 again  � m   . /��
�� boovtrue��  ��  � ���� r   3 C��� l  3 A������ n   3 A��� 1   ? A��
�� 
bhit� l  3 ?������ I  3 ?����
�� .sysodlogaskr        TEXT� m   3 4�� ��� � T h e r e   s t i l l   i s n ' t   e n o u g h   f r e e   s p a c e   t o   c o m p l e t e   a   b a c k u p .   
 D o   y o u   w a n t   t o   d e l e t e   t h e   o l d e s t   b a c k u p   a n d   c o n t i n u e ?� �� 
�� 
btns  J   5 9  m   5 6 �  Y e s �� m   6 7 �		  C a n c e l   B a c k u p��   ��
��
�� 
dflt
 m   : ;���� ��  ��  ��  ��  ��  � o      ���� 
0 reply1  ��  ��  �  l  H H��������  ��  ��   �� Z   H ]�� l  H K���� =   H K o   H I���� 
0 reply1   m   I J �  Y e s��  ��   I   N S�������� 0 tidyup tidyUp��  ��  ��   I  V ]���
�� .sysonotfnull��� ��� TEXT m   V Y � > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p�  ��  �  l     �~�}�|�~  �}  �|    l     �{�z�y�{  �z  �y    l     �x�w�v�x  �w  �v     l     �u�t�s�u  �t  �s    !"! l     �r�q�p�r  �q  �p  " #$# i    %&% I      �o�n�m�o 0 
backupinit 
backupInit�n  �m  & k     �'' ()( r     *+* 4     �l,
�l 
psxf, o    �k�k "0 destinationdisk destinationDisk+ o      �j�j 	0 drive  ) -.- l   �i�h�g�i  �h  �g  . /0/ l   �f12�f  1  log (tempDrive)   2 �33  l o g   ( t e m p D r i v e )0 454 l   �e67�e  6 ' !log (destinationDisk & "Backups")   7 �88 B l o g   ( d e s t i n a t i o n D i s k   &   " B a c k u p s " )5 9:9 l   �d�c�b�d  �c  �b  : ;<; I   �a=�`
�a .sysonotfnull��� ��� TEXT= m    >> �?? 2 W o a h ,   b a c k i n g   t h e   f u c k   u p�`  < @A@ l   �_�^�]�_  �^  �]  A BCB Z    2DE�\�[D l   F�Z�YF =    GHG I    �XI�W�X 0 itexists itExistsI JKJ m    LL �MM  f o l d e rK N�VN b    OPO o    �U�U "0 destinationdisk destinationDiskP m    QQ �RR  B a c k u p s�V  �W  H m    �T
�T boovfals�Z  �Y  E O    .STS I   -�S�RU
�S .corecrel****      � null�R  U �QVW
�Q 
koclV m   ! "�P
�P 
cfolW �OXY
�O 
inshX o   # $�N�N 	0 drive  Y �MZ�L
�M 
prdtZ K   % )[[ �K\�J
�K 
pnam\ m   & ']] �^^  B a c k u p s�J  �L  T m    __�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �\  �[  C `a` l  3 3�I�H�G�I  �H  �G  a bcb O   3 Ided k   7 Hff ghg r   7 Fiji n   7 Bklk 4   = B�Fm
�F 
cfolm m   > Ann �oo  B a c k u p sl 4   7 =�Ep
�E 
cdisp o   ; <�D�D 	0 drive  j o      �C�C 0 backupfolder backupFolderh q�Bq l  G G�Ars�A  r  log (backupFolder)   s �tt $ l o g   ( b a c k u p F o l d e r )�B  e m   3 4uu�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  c vwv l  J J�@�?�>�@  �?  �>  w xyx Z   J {z{�=|z l  J Y}�<�;} =   J Y~~ I   J W�:��9�: 0 itexists itExists� ��� m   K N�� ���  f o l d e r� ��8� b   N S��� o   N O�7�7 "0 destinationdisk destinationDisk� m   O R�� ���  B a c k u p s / L a t e s t�8  �9   m   W X�6
�6 boovfals�<  �;  { O   \ s��� I  ` r�5�4�
�5 .corecrel****      � null�4  � �3��
�3 
kocl� m   b c�2
�2 
cfol� �1��
�1 
insh� o   d g�0�0 0 backupfolder backupFolder� �/��.
�/ 
prdt� K   h n�� �-��,
�- 
pnam� m   i l�� ���  L a t e s t�,  �.  � m   \ ]���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  �=  | r   v {��� m   v w�+
�+ boovfals� o      �*�* 0 initialbackup initialBackupy ��� l  | |�)�(�'�)  �(  �'  � ��� O   | ���� k   � ��� ��� r   � ���� n   � ���� 4   � ��&�
�& 
cfol� m   � ��� ���  L a t e s t� n   � ���� 4   � ��%�
�% 
cfol� m   � ��� ���  B a c k u p s� 4   � ��$�
�$ 
cdis� o   � ��#�# 	0 drive  � o      �"�" 0 latestfolder latestFolder� ��!� l  � �� ���   �  log (backupFolder)   � ��� $ l o g   ( b a c k u p F o l d e r )�!  � m   | }���                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � ��� l  � �����  �  �  � ��� I   � ����� 
0 backup  �  �  �  $ ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ���
�  �  �
  � ��� i    ��� I      �	���	 0 tidyup tidyUp�  �  � k     ��� ��� O     ���� k    ��� ��� r    ��� n    	��� 1    	�
� 
ascd� n    ��� 2   �
� 
cobj� o    �� 0 backupfolder backupFolder� o      �� 0 creationdates creationDates� ��� r    ��� n    ��� 4    ��
� 
cobj� m    �� � o    � �  0 creationdates creationDates� o      ���� 0 theoldestdate theOldestDate� ��� r    ��� m    ���� � o      ���� 0 j  � ��� l   ��������  ��  ��  � ��� Y    K�������� k   % F�� ��� r   % +��� n   % )��� 4   & )���
�� 
cobj� o   ' (���� 0 i  � o   % &���� 0 creationdates creationDates� o      ���� 0 thisdate thisDate� ���� Z   , F������� l  , /������ A   , /��� o   , -���� 0 thisdate thisDate� o   - .���� 0 theoldestdate theOldestDate��  ��  � k   2 B�� ��� r   2 5��� o   2 3���� 0 thisdate thisDate� o      ���� 0 theoldestdate theOldestDate� ��� r   6 9��� o   6 7���� 0 i  � o      ���� 0 j  � ���� I  : B�����
�� .ascrcmnt****      � ****� l  : >������ n   : >� � 4   ; >��
�� 
cobj o   < =���� 0 j    o   : ;���� 0 backupfolder backupFolder��  ��  ��  ��  ��  ��  ��  �� 0 i  � m    ���� � I    ����
�� .corecnte****       **** o    ���� 0 creationdates creationDates��  ��  �  l  L L��������  ��  ��    l  L L����   ! -- Delete the oldest folder    �		 6 - -   D e l e t e   t h e   o l d e s t   f o l d e r 

 I  L T����
�� .coredeloobj        obj  n   L P 4   M P��
�� 
cobj o   N O���� 0 j   o   L M���� 0 backupfolder backupFolder��    l  U U��������  ��  ��    l   U U����   � �set oldestFolder to item j of backupFolder as string		set oldestFolder to POSIX path of oldestFolder as string		log ("to delete: " & oldestFolder)				set toDelete to "rm -rf " & oldestFolder		do shell script toDelete    �� s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e r   a s   s t r i n g  	 	 s e t   o l d e s t F o l d e r   t o   P O S I X   p a t h   o f   o l d e s t F o l d e r   a s   s t r i n g  	 	 l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )  	 	  	 	 s e t   t o D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e r  	 	 d o   s h e l l   s c r i p t   t o D e l e t e  l  U U��������  ��  ��    l  U U��������  ��  ��    r   U e l  U c���� n   U c !  1   a c��
�� 
bhit! l  U a"����" I  U a��#$
�� .sysodlogaskr        TEXT# m   U V%% �&&D Y o u   n e e d   t o   e m p t y   y o u r   t r a s h   f i r s t   b e f o r e   a   b a c k u p   c a n   h a p p e n . 
 C l i c k   E m p t y   T r a s h   t o   d o   t h i s   a u t o m a t i c a l l y   a n d   c o n t i n u e   b a c k i n g   u p ,   o r   C a n c e l   t o   s t o p   t h e   p r o c e s s .  $ ��'(
�� 
btns' J   W [)) *+* m   W X,, �--  E m p t y   T r a s h+ .��. m   X Y// �00  C a n c e l   B a c k u p��  ( ��1��
�� 
dflt1 m   \ ]���� ��  ��  ��  ��  ��   o      ���� 
0 reply2   232 Z   f 45��64 l  f i7����7 =   f i898 o   f g���� 
0 reply2  9 m   g h:: �;;  E m p t y   T r a s h��  ��  5 I  l u��<��
�� .fndremptnull��� ��� obj < l  l q=����= 1   l q��
�� 
trsh��  ��  ��  ��  6 I  x ��>��
�� .sysonotfnull��� ��� TEXT> m   x {?? �@@ > Y o u   s t o p p e d   b a c k i n g   t h e   f u c k   u p��  3 ABA l  � ���������  ��  ��  B CDC I   � ���E���� 0 freespaceinit freeSpaceInitE F��F m   � �GG �HH  t r u e��  ��  D I��I I  � ���J��
�� .sysonotfnull��� ��� TEXTJ m   � �KK �LL � I ' v e   d e l e t e d   t h e   o l d e s t   b a c k u p ,   e m p t i e d   t h e   t r a s h   a n d   t r y i n g   a g a i n   j u s t   f o r   y o u .   T a l k   a b o u t   b e i n g   n e e d y . . .��  ��  � m     MM�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � NON l  � ���������  ��  ��  O PQP l  � ���������  ��  ��  Q RSR l  � ���TU��  T o i-- An atomic delete function that seems to only work when using a local drive rather than an external one   U �VV � - -   A n   a t o m i c   d e l e t e   f u n c t i o n   t h a t   s e e m s   t o   o n l y   w o r k   w h e n   u s i n g   a   l o c a l   d r i v e   r a t h e r   t h a n   a n   e x t e r n a l   o n eS WXW l  � ���YZ��  Y 0 *set oldestFolder to item j of backupFolder   Z �[[ T s e t   o l d e s t F o l d e r   t o   i t e m   j   o f   b a c k u p F o l d e rX \]\ l  � ���^_��  ^ 0 *set oldestFolder to oldestFolder as string   _ �`` T s e t   o l d e s t F o l d e r   t o   o l d e s t F o l d e r   a s   s t r i n g] aba l  � ���cd��  c * $set n to name of folder oldestFolder   d �ee H s e t   n   t o   n a m e   o f   f o l d e r   o l d e s t F o l d e rb fgf l  � ���hi��  h : 4set oldestFolder to (posixDestination & n) as string   i �jj h s e t   o l d e s t F o l d e r   t o   ( p o s i x D e s t i n a t i o n   &   n )   a s   s t r i n gg klk l  � ���mn��  m ( "log ("to delete: " & oldestFolder)   n �oo D l o g   ( " t o   d e l e t e :   "   &   o l d e s t F o l d e r )l pqp l  � ���������  ��  ��  q rsr l  � ���tu��  t 2 ,set atomicDelete to "rm -rf " & oldestFolder   u �vv X s e t   a t o m i c D e l e t e   t o   " r m   - r f   "   &   o l d e s t F o l d e rs w��w l  � ���xy��  x " do shell script atomicDelete   y �zz 8 d o   s h e l l   s c r i p t   a t o m i c D e l e t e��  � {|{ l     ��������  ��  ��  | }~} l     ��������  ��  ��  ~ � l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i    ��� I      �������� 
0 backup  ��  ��  � k    ��� ��� q      �� ����� 0 t  � ����� 0 x  � ������ "0 containerfolder containerFolder��  � ��� r     ��� I     ������� (0 getfoldertimestamp getFolderTimestamp��  �  � o      �~�~ 0 t  � ��� l   �}�|�{�}  �|  �{  � ��� r    ��� m    	�z
�z boovtrue� o      �y�y 0 isbackingup isBackingUp� ��� l   �x�w�v�x  �w  �v  � ��� I    �u��t�u 0 	stopwatch  � ��s� m    �� ��� 
 s t a r t�s  �t  � ��� l   �r�q�p�r  �q  �p  � ��� O   ��� k   ~�� ��� l   �o���o  � h b-- Gets the name of the source folder as a duplicate folder with the same name needs to be created   � ��� � - -   G e t s   t h e   n a m e   o f   t h e   s o u r c e   f o l d e r   a s   a   d u p l i c a t e   f o l d e r   w i t h   t h e   s a m e   n a m e   n e e d s   t o   b e   c r e a t e d� ��� l   �n���n  � z t-- This will be created in the backup folder and is needed as only the contents, not the folder itself, is backed up   � ��� � - -   T h i s   w i l l   b e   c r e a t e d   i n   t h e   b a c k u p   f o l d e r   a n d   i s   n e e d e d   a s   o n l y   t h e   c o n t e n t s ,   n o t   t h e   f o l d e r   i t s e l f ,   i s   b a c k e d   u p� ��� r    ��� c    ��� 4    �m�
�m 
psxf� o    �l�l 0 sourcefolder sourceFolder� m    �k
�k 
TEXT� o      �j�j 0 
foldername 
folderName� ��� r     (��� n     &��� 1   $ &�i
�i 
pnam� 4     $�h�
�h 
cfol� o   " #�g�g 0 
foldername 
folderName� o      �f�f 0 
foldername 
folderName� ��� l  ) )�e���e  �  log (folderName)		   � ��� $ l o g   ( f o l d e r N a m e ) 	 	� ��� l  ) )�d�c�b�d  �c  �b  � ��� Z   ) }���a�`� l  ) ,��_�^� =   ) ,��� o   ) *�]�] 0 initialbackup initialBackup� m   * +�\
�\ boovfals�_  �^  � k   / y�� ��� I  / =�[�Z�
�[ .corecrel****      � null�Z  � �Y��
�Y 
kocl� m   1 2�X
�X 
cfol� �W��
�W 
insh� o   3 4�V�V 0 backupfolder backupFolder� �U��T
�U 
prdt� K   5 9�� �S��R
�S 
pnam� o   6 7�Q�Q 0 t  �R  �T  � ��� l  > >�P�O�N�P  �O  �N  � ��� r   > H��� c   > D��� 4   > B�M�
�M 
psxf� o   @ A�L�L 0 sourcefolder sourceFolder� m   B C�K
�K 
TEXT� o      �J�J (0 activesourcefolder activeSourceFolder� ��� r   I S��� 4   I O�I�
�I 
cfol� o   K N�H�H (0 activesourcefolder activeSourceFolder� o      �G�G (0 activesourcefolder activeSourceFolder� ��� l  T T�F���F  �  log (activeSourceFolder)   � ��� 0 l o g   ( a c t i v e S o u r c e F o l d e r )� ��� r   T h��� n   T d��� 4   a d�E�
�E 
cfol� o   b c�D�D 0 t  � n   T a��� 4   \ a�C�
�C 
cfol� m   ] `�� ���  B a c k u p s� 4   T \�B�
�B 
cdis� o   X [�A�A 	0 drive  � o      �@�@ (0 activebackupfolder activeBackupFolder� ��� l  i i�?���?  �  log (activeBackupFolder)   � ��� 0 l o g   ( a c t i v e B a c k u p F o l d e r )� ��>� I  i y�=�<�
�= .corecrel****      � null�<  � �; 
�; 
kocl  m   k l�:
�: 
cfol �9
�9 
insh o   m p�8�8 (0 activebackupfolder activeBackupFolder �7�6
�7 
prdt K   q u �5�4
�5 
pnam o   r s�3�3 0 
foldername 
folderName�4  �6  �>  �a  �`  �  l  ~ ~�2�1�0�2  �1  �0   	
	 l  ~ ~�/�/   q k-- The script that starts the backup. It scans all the files and essentially syncs the folder to the volume    � � - -   T h e   s c r i p t   t h a t   s t a r t s   t h e   b a c k u p .   I t   s c a n s   a l l   t h e   f i l e s   a n d   e s s e n t i a l l y   s y n c s   t h e   f o l d e r   t o   t h e   v o l u m e
  l  ~ ~�.�.   s m-- An atomic version would just to do a straight backup without versioning to see the function at full effect    � � - -   A n   a t o m i c   v e r s i o n   w o u l d   j u s t   t o   d o   a   s t r a i g h t   b a c k u p   w i t h o u t   v e r s i o n i n g   t o   s e e   t h e   f u n c t i o n   a t   f u l l   e f f e c t  l  ~ ~�-�-   F @-- This means that only new or updated files will be copied over    � � - -   T h i s   m e a n s   t h a t   o n l y   n e w   o r   u p d a t e d   f i l e s   w i l l   b e   c o p i e d   o v e r  l  ~ ~�,�,   _ Y-- Any deleted files in the source folder will be deleted in the destination folder too		    � � - -   A n y   d e l e t e d   f i l e s   i n   t h e   s o u r c e   f o l d e r   w i l l   b e   d e l e t e d   i n   t h e   d e s t i n a t i o n   f o l d e r   t o o 	 	  l  ~ ~�+ �+    -- Original sync code      �!! , - -   O r i g i n a l   s y n c   c o d e   "#" l  ~ ~�*$%�*  $ | v-- do shell script "rsync -aE --delete '/Users/robert.tesalona/Testies' '/Volumes/USB DISK/Backups/" & timestamp & "'"   % �&& � - -   d o   s h e l l   s c r i p t   " r s y n c   - a E   - - d e l e t e   ' / U s e r s / r o b e r t . t e s a l o n a / T e s t i e s '   ' / V o l u m e s / U S B   D I S K / B a c k u p s / "   &   t i m e s t a m p   &   " ' "# '(' l  ~ ~�)�(�'�)  �(  �'  ( )*) l  ~ ~�&+,�&  +  -- Original   , �--  - -   O r i g i n a l* ./. l  ~ ~�%01�%  0 k e-- $ rsync -avz --delete --backup --backup-dir="backup_$(date +\%Y-\%m-\%d)" /source/path/ /dest/path   1 �22 � - -   $   r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = " b a c k u p _ $ ( d a t e   + \ % Y - \ % m - \ % d ) "   / s o u r c e / p a t h /   / d e s t / p a t h/ 343 l  ~ ~�$56�$  5 z t-- -b, --backup, with this option, preexisting destination files are renamed as each file is transferred or deleted.   6 �77 � - -   - b ,   - - b a c k u p ,   w i t h   t h i s   o p t i o n ,   p r e e x i s t i n g   d e s t i n a t i o n   f i l e s   a r e   r e n a m e d   a s   e a c h   f i l e   i s   t r a n s f e r r e d   o r   d e l e t e d .4 898 l  ~ ~�#:;�#  : r l-- --backup-dir=DIR, this tells rsync to store all backups in the specified directory on the receiving side.   ; �<< � - -   - - b a c k u p - d i r = D I R ,   t h i s   t e l l s   r s y n c   t o   s t o r e   a l l   b a c k u p s   i n   t h e   s p e c i f i e d   d i r e c t o r y   o n   t h e   r e c e i v i n g   s i d e .9 =>= l  ~ ~�"?@�"  ? � �-- If the idea of the --delete option makes your buttocks clench it�s understandable since there is no recovering the deleted files. However, you can pass in the --backup option, this will make copies of any files due to be deleted or updated.   @ �AA� - -   I f   t h e   i d e a   o f   t h e   - - d e l e t e   o p t i o n   m a k e s   y o u r   b u t t o c k s   c l e n c h   i t  s   u n d e r s t a n d a b l e   s i n c e   t h e r e   i s   n o   r e c o v e r i n g   t h e   d e l e t e d   f i l e s .   H o w e v e r ,   y o u   c a n   p a s s   i n   t h e   - - b a c k u p   o p t i o n ,   t h i s   w i l l   m a k e   c o p i e s   o f   a n y   f i l e s   d u e   t o   b e   d e l e t e d   o r   u p d a t e d .> BCB l  ~ ~�!DE�!  D � �-- The --backup command needs a friend to work best, introducing --backup-dir. These options allow you to specify the location of the backups and a string to add to the end of the filename.   E �FFz - -   T h e   - - b a c k u p   c o m m a n d   n e e d s   a   f r i e n d   t o   w o r k   b e s t ,   i n t r o d u c i n g   - - b a c k u p - d i r .   T h e s e   o p t i o n s   a l l o w   y o u   t o   s p e c i f y   t h e   l o c a t i o n   o f   t h e   b a c k u p s   a n d   a   s t r i n g   t o   a d d   t o   t h e   e n d   o f   t h e   f i l e n a m e .C GHG l  ~ ~� ���   �  �  H I�I Z   ~~JKL�J l  ~ �M��M =   ~ �NON o   ~ �� 0 initialbackup initialBackupO m    ��
� boovtrue�  �  K k   � �PP QRQ l  � ��ST�  S � �display dialog "This is your first backup and will take a bit of time to complete just for this instance." buttons {"OK"} default button 1   T �UU d i s p l a y   d i a l o g   " T h i s   i s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   a   b i t   o f   t i m e   t o   c o m p l e t e   j u s t   f o r   t h i s   i n s t a n c e . "   b u t t o n s   { " O K " }   d e f a u l t   b u t t o n   1R VWV I  � ��X�
� .sysonotfnull��� ��� TEXTX m   � �YY �ZZ � T h i s   i s   y o u r   f i r s t   b a c k u p   a n d   w i l l   t a k e   s o m e   t i m e .   I ' m   d o i n g   a l l   t h e   w o r k   a n y w a y . . .�  W [\[ l  � �����  �  �  \ ]^] r   � �_`_ c   � �aba l  � �c��c b   � �ded b   � �fgf b   � �hih o   � ��� "0 destinationdisk destinationDiski m   � �jj �kk  B a c k u p s / L a t e s t /g o   � ��� 0 
foldername 
folderNamee m   � �ll �mm  /�  �  b m   � ��
� 
TEXT` o      �� 0 d  ^ non I  � ��p�

� .sysoexecTEXT���     TEXTp b   � �qrq b   � �sts b   � �uvu b   � �wxw m   � �yy �zz  d i t t o   'x o   � ��	�	 0 sourcefolder sourceFolderv m   � �{{ �||  '   't o   � ��� 0 d  r m   � �}} �~~  / '�
  o � l  � �����  �  �  � ��� r   � ���� m   � ��
� boovfals� o      �� 0 isbackingup isBackingUp� ��� l  � ���� �  �  �   � ��� I  � ������
�� .sysonotfnull��� ��� TEXT� m   � ��� ��� N W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !   G o o d   l a d !��  � ���� I  � ������
�� .sysodelanull��� ��� nmbr� m   � ����� ��  ��  L ��� l  � ������� =   � ���� o   � ����� 0 initialbackup initialBackup� m   � ���
�� boovfals��  ��  � ���� k   �z�� ��� r   � ���� c   � ���� l  � ������� b   � ���� b   � ���� b   � ���� b   � ���� b   � ���� o   � ����� "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s /� o   � ����� 0 t  � m   � ��� ���  /� o   � ����� 0 
foldername 
folderName� m   � ��� ���  /��  ��  � m   � ���
�� 
TEXT� o      ���� 0 c  � ��� r   � ���� c   � ���� l  � ������� b   � ���� b   � ���� b   � ���� o   � ����� "0 destinationdisk destinationDisk� m   � ��� ���  B a c k u p s / L a t e s t /� o   � ����� 0 
foldername 
folderName� m   � ��� ���  /��  ��  � m   � ���
�� 
TEXT� o      ���� 0 d  � ��� I  � ������
�� .ascrcmnt****      � ****� l  � ������� b   � ���� m   � ��� ���  b a c k i n g   u p   t o :  � o   � ����� 0 d  ��  ��  ��  � ��� l   ��������  ��  ��  � ��� I  �����
�� .sysoexecTEXT���     TEXT� b   ��� b   ��� b   ��� b   ��� b   	��� b   ��� m   �� ��� V r s y n c   - a v z   - - d e l e t e   - - b a c k u p   - - b a c k u p - d i r = '� o  ���� 0 c  � m  �� ���  '   '� o  	
���� 0 sourcefolder sourceFolder� m  �� ���  '   '� o  ���� 0 d  � m  �� ���  / '��  � ��� l ��������  ��  ��  � ��� r  ��� m  ��
�� boovfals� o      ���� 0 isbackingup isBackingUp� ��� l ��������  ��  ��  � ���� Z  z������ = -��� n  *��� 2 &*��
�� 
cobj� l &������ c  &��� 4  "���
�� 
psxf� o   !���� 0 c  � m  "%��
�� 
alis��  ��  � J  *,����  � k  0h�� ��� l 00������  � % delete folder t of backupFolder   � ��� > d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r� ��� l 00��������  ��  ��  � ��� r  08��� c  06��� n  04   4  14��
�� 
cfol o  23���� 0 t   o  01���� 0 backupfolder backupFolder� m  45��
�� 
TEXT� o      ���� 0 oldestfolder oldestFolder�  r  9B c  9@ n  9>	
	 1  :>��
�� 
psxp
 o  9:���� 0 oldestfolder oldestFolder m  >?��
�� 
TEXT o      ���� 0 oldestfolder oldestFolder  I CL����
�� .ascrcmnt****      � **** l CH���� b  CH m  CF �  t o   d e l e t e :   o  FG���� 0 oldestfolder oldestFolder��  ��  ��    l MM��������  ��  ��    r  MT b  MR m  MP �  r m   - r f   o  PQ���� 0 oldestfolder oldestFolder o      ���� 0 todelete toDelete  I UZ����
�� .sysoexecTEXT���     TEXT o  UV���� 0 todelete toDelete��    !  l [[��������  ��  ��  ! "#" I [b��$��
�� .sysonotfnull��� ��� TEXT$ m  [^%% �&& � A n y   n e w   f i l e s   f o u n d   w e r e   b a c k e d   u p   b u t   d e t e c t e d   n o   o t h e r   c h a n g e s   s o   I   j u s t   s t r o l l e d   o n   b y . . .��  # '��' I ch��(��
�� .sysodelanull��� ��� nmbr( m  cd���� ��  ��  ��  � k  kz)) *+* I kr��,��
�� .sysonotfnull��� ��� TEXT, m  kn-- �.. N W o a h ,   y o u   b a c k e d   t h e   f u c k   u p !   G o o d   l a d !��  + /0/ I sx��1��
�� .sysodelanull��� ��� nmbr1 m  st���� ��  0 232 l yy��������  ��  ��  3 4��4 l  yy��56��  5 �if items of (POSIX file c as alias) is {} then					delete folder t of backupFolder					display notification "Your new files were backed up but detected no other changes. The backup folder was therefore removed because I have OCD"					delay 1				end if   6 �77� i f   i t e m s   o f   ( P O S I X   f i l e   c   a s   a l i a s )   i s   { }   t h e n  	 	 	 	 	 d e l e t e   f o l d e r   t   o f   b a c k u p F o l d e r  	 	 	 	 	 d i s p l a y   n o t i f i c a t i o n   " Y o u r   n e w   f i l e s   w e r e   b a c k e d   u p   b u t   d e t e c t e d   n o   o t h e r   c h a n g e s .   T h e   b a c k u p   f o l d e r   w a s   t h e r e f o r e   r e m o v e d   b e c a u s e   I   h a v e   O C D "  	 	 	 	 	 d e l a y   1  	 	 	 	 e n d   i f��  ��  ��  �  �  � m    88�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��  � 9:9 l ����������  ��  ��  : ;��; I  ����<���� 0 	stopwatch  < =��= m  ��>> �??  f i n i s h��  ��  ��  � @A@ l     ��������  ��  ��  A BCB l     ��������  ��  ��  C DED l     ��FG��  F � �-----------------------------------------------------------------------------------------------------------------------------------------------   G �HH - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -E IJI l     ��KL��  K � �-----------------------------------------------------------------------------------------------------------------------------------------------   L �MM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -J NON l     ��PQ��  P � �-- UTIL FUNCTIONS ------------------------------------------------------------------------------------------------------------------------   Q �RR - -   U T I L   F U N C T I O N S   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -O STS l     ��������  ��  ��  T UVU i    WXW I      ��Y���� 0 itexists itExistsY Z[Z o      ���� 0 
objecttype 
objectType[ \��\ o      ���� 
0 object  ��  ��  X l    W]^_] O     W`a` Z    Vbcd��b l   e����e =    fgf o    ���� 0 
objecttype 
objectTypeg m    hh �ii  d i s k��  ��  c Z   
 jk��lj I  
 �m�~
� .coredoexnull���     ****m 4   
 �}n
�} 
cdisn o    �|�| 
0 object  �~  k L    oo m    �{
�{ boovtrue��  l L    pp m    �z
�z boovfalsd qrq l   "s�y�xs =    "tut o     �w�w 0 
objecttype 
objectTypeu m     !vv �ww  f i l e�y  �x  r xyx Z   % 7z{�v|z I  % -�u}�t
�u .coredoexnull���     ****} 4   % )�s~
�s 
file~ o   ' (�r�r 
0 object  �t  { L   0 2 m   0 1�q
�q boovtrue�v  | L   5 7�� m   5 6�p
�p boovfalsy ��� l  : =��o�n� =   : =��� o   : ;�m�m 0 
objecttype 
objectType� m   ; <�� ���  f o l d e r�o  �n  � ��l� Z   @ R���k�� I  @ H�j��i
�j .coredoexnull���     ****� 4   @ D�h�
�h 
cfol� o   B C�g�g 
0 object  �i  � L   K M�� m   K L�f
�f boovtrue�k  � L   P R�� m   P Q�e
�e boovfals�l  ��  a m     ���                                                                                  sevs  alis    �  W00721ML                   ���H+     *System Events.app                                               �W�2�w        ����  	                CoreServices    ���      �2�w       *        9W00721ML:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    W 0 0 7 2 1 M L  -System/Library/CoreServices/System Events.app   / ��  ^ "  (String, String) as Boolean   _ ��� 8   ( S t r i n g ,   S t r i n g )   a s   B o o l e a nV ��� l     �d�c�b�d  �c  �b  � ��� l     �a�`�_�a  �`  �_  � ��� l     �^�]�\�^  �]  �\  � ��� l     �[�Z�Y�[  �Z  �Y  � ��� l     �X�W�V�X  �W  �V  � ��� i    ��� I      �U�T�S�U (0 getfoldertimestamp getFolderTimestamp�T  �S  � k    I�� ��� l     �R���R  �  -- Date variables   � ��� " - -   D a t e   v a r i a b l e s� ��� r     )��� l     ��Q�P� I     �O�N�M
�O .misccurdldt    ��� null�N  �M  �Q  �P  � K    �� �L��
�L 
year� o    �K�K 0 y  � �J��
�J 
mnth� o    �I�I 0 m  � �H��
�H 
day � o    �G�G 0 d  � �F��E
�F 
time� o   	 
�D�D 0 t  �E  � ��� r   * 1��� c   * /��� l  * -��C�B� c   * -��� o   * +�A�A 0 y  � m   + ,�@
�@ 
long�C  �B  � m   - .�?
�? 
TEXT� o      �>�> 0 ty tY� ��� r   2 9��� c   2 7��� l  2 5��=�<� c   2 5��� o   2 3�;�; 0 y  � m   3 4�:
�: 
long�=  �<  � m   5 6�9
�9 
TEXT� o      �8�8 0 ty tY� ��� r   : A��� c   : ?��� l  : =��7�6� c   : =��� o   : ;�5�5 0 m  � m   ; <�4
�4 
long�7  �6  � m   = >�3
�3 
TEXT� o      �2�2 0 tm tM� ��� r   B I��� c   B G��� l  B E��1�0� c   B E��� o   B C�/�/ 0 d  � m   C D�.
�. 
long�1  �0  � m   E F�-
�- 
TEXT� o      �,�, 0 td tD� ��� r   J Q��� c   J O��� l  J M��+�*� c   J M��� o   J K�)�) 0 t  � m   K L�(
�( 
long�+  �*  � m   M N�'
�' 
TEXT� o      �&�& 0 tt tT� ��� l  R R�%�$�#�%  �$  �#  � ��� l  R R�"���"  � W Q-- Append the month or day with a 0 if the string length is only 1 character long   � ��� � - -   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g� ��� r   R [��� c   R Y��� l  R W��!� � I  R W���
� .corecnte****       ****� o   R S�� 0 tm tM�  �!  �   � m   W X�
� 
nmbr� o      �� 
0 tml tML� ��� r   \ e��� c   \ c��� l  \ a���� I  \ a���
� .corecnte****       ****� o   \ ]�� 0 tm tM�  �  �  � m   a b�
� 
nmbr� o      �� 
0 tdl tDL� ��� l  f f����  �  �  � ��� Z   f u����� l  f i���� =   f i��� o   f g�� 
0 tml tML� m   g h�� �  �  � r   l q   b   l o m   l m �  0 o   m n�
�
 0 tm tM o      �	�	 0 tm tM�  �  �  l  v v����  �  �   	 Z   v �
��
 l  v y�� =   v y o   v w�� 
0 tdl tDL m   w x� �  �  �   r   | � b   | � m   |  �  0 o    ����� 0 td tD o      ���� 0 td tD�  �  	  l  � ���������  ��  ��    l  � ���������  ��  ��    l  � �����    -- Time variables	    � $ - -   T i m e   v a r i a b l e s 	  l  � ��� !��     -- Get hour   ! �""  - -   G e t   h o u r #$# r   � �%&% n   � �'(' 1   � ���
�� 
tstr( l  � �)����) I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  & o      ���� 0 timestr timeStr$ *+* r   � �,-, I  � �����.
�� .sysooffslong    ��� null��  . ��/0
�� 
psof/ m   � �11 �22  :0 ��3��
�� 
psin3 o   � ����� 0 timestr timeStr��  - o      ���� 0 pos  + 454 r   � �676 c   � �898 n   � �:;: 7  � ���<=
�� 
cha < m   � ����� = l  � �>����> \   � �?@? o   � ����� 0 pos  @ m   � ����� ��  ��  ; o   � ����� 0 timestr timeStr9 m   � ���
�� 
TEXT7 o      ���� 0 h  5 ABA r   � �CDC c   � �EFE n   � �GHG 7 � ���IJ
�� 
cha I l  � �K����K [   � �LML o   � ����� 0 pos  M m   � ����� ��  ��  J  ;   � �H o   � ����� 0 timestr timeStrF m   � ���
�� 
TEXTD o      ���� 0 timestr timeStrB NON l  � ���������  ��  ��  O PQP l  � ���RS��  R  -- Get minute   S �TT  - -   G e t   m i n u t eQ UVU r   � �WXW I  � �����Y
�� .sysooffslong    ��� null��  Y ��Z[
�� 
psofZ m   � �\\ �]]  :[ ��^��
�� 
psin^ o   � ����� 0 timestr timeStr��  X o      ���� 0 pos  V _`_ r   � �aba c   � �cdc n   � �efe 7  � ���gh
�� 
cha g m   � ����� h l  � �i����i \   � �jkj o   � ����� 0 pos  k m   � ����� ��  ��  f o   � ����� 0 timestr timeStrd m   � ���
�� 
TEXTb o      ���� 0 m  ` lml r   �	non c   �pqp n   �rsr 7 ���tu
�� 
cha t l  �v����v [   �wxw o   � ���� 0 pos  x m   ���� ��  ��  u  ;  s o   � ����� 0 timestr timeStrq m  ��
�� 
TEXTo o      ���� 0 timestr timeStrm yzy l 

��������  ��  ��  z {|{ l 

��}~��  }  -- Get AM or PM   ~ �  - -   G e t   A M   o r   P M| ��� r  
��� I 
�����
�� .sysooffslong    ��� null��  � ����
�� 
psof� m  �� ���   � �����
�� 
psin� o  ���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  0��� c  .��� n  ,��� 7,����
�� 
cha � l %)������ [  %)��� o  &'���� 0 pos  � m  '(���� ��  ��  �  ;  *+� o  ���� 0 timestr timeStr� m  ,-��
�� 
TEXT� o      ���� 0 s  � ��� l 11��������  ��  ��  � ��� l 11��������  ��  ��  � ��� l 11������  � % -- Create timestamp folder name   � ��� > - -   C r e a t e   t i m e s t a m p   f o l d e r   n a m e� ��� r  1F��� b  1D��� b  1B��� b  1@��� b  1>��� b  1:��� b  18��� b  16��� m  14�� ���  b a c k u p _� o  45���� 0 ty tY� o  67���� 0 tm tM� o  89���� 0 td tD� m  :=�� ���  _� o  >?���� 0 h  � o  @A���� 0 m  � o  BC���� 0 s  � o      ���� 0 	timestamp  � ��� l GG��������  ��  ��  � ���� L  GI�� o  GH���� 0 	timestamp  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i     #��� I      �������� 0 gettimestamp getTimestamp��  ��  � k    E�� ��� l     ������  �  -- Date variables   � ��� " - -   D a t e   v a r i a b l e s� ��� r     )��� l     ������ I     ��~�}
� .misccurdldt    ��� null�~  �}  ��  ��  � K    �� �|��
�| 
year� o    �{�{ 0 y  � �z��
�z 
mnth� o    �y�y 0 m  � �x��
�x 
day � o    �w�w 0 d  � �v��u
�v 
time� o   	 
�t�t 0 t  �u  � ��� r   * 1��� c   * /��� l  * -��s�r� c   * -��� o   * +�q�q 0 y  � m   + ,�p
�p 
long�s  �r  � m   - .�o
�o 
TEXT� o      �n�n 0 ty tY� ��� r   2 9��� c   2 7��� l  2 5��m�l� c   2 5��� o   2 3�k�k 0 y  � m   3 4�j
�j 
long�m  �l  � m   5 6�i
�i 
TEXT� o      �h�h 0 ty tY� ��� r   : A��� c   : ?��� l  : =��g�f� c   : =��� o   : ;�e�e 0 m  � m   ; <�d
�d 
long�g  �f  � m   = >�c
�c 
TEXT� o      �b�b 0 tm tM� ��� r   B I��� c   B G��� l  B E��a�`� c   B E��� o   B C�_�_ 0 d  � m   C D�^
�^ 
long�a  �`  � m   E F�]
�] 
TEXT� o      �\�\ 0 td tD� � � r   J Q c   J O l  J M�[�Z c   J M o   J K�Y�Y 0 t   m   K L�X
�X 
long�[  �Z   m   M N�W
�W 
TEXT o      �V�V 0 tt tT  	 l  R R�U�T�S�U  �T  �S  	 

 l  R R�R�R   W Q-- Append the month or day with a 0 if the string length is only 1 character long    � � - -   A p p e n d   t h e   m o n t h   o r   d a y   w i t h   a   0   i f   t h e   s t r i n g   l e n g t h   i s   o n l y   1   c h a r a c t e r   l o n g  r   R [ c   R Y l  R W�Q�P I  R W�O�N
�O .corecnte****       **** o   R S�M�M 0 tm tM�N  �Q  �P   m   W X�L
�L 
nmbr o      �K�K 
0 tml tML  r   \ e c   \ c l  \ a�J�I I  \ a�H�G
�H .corecnte****       **** o   \ ]�F�F 0 tm tM�G  �J  �I   m   a b�E
�E 
nmbr o      �D�D 
0 tdl tDL   l  f f�C�B�A�C  �B  �A    !"! Z   f u#$�@�?# l  f i%�>�=% =   f i&'& o   f g�<�< 
0 tml tML' m   g h�;�; �>  �=  $ r   l q()( b   l o*+* m   l m,, �--  0+ o   m n�:�: 0 tm tM) o      �9�9 0 tm tM�@  �?  " ./. l  v v�8�7�6�8  �7  �6  / 010 Z   v �23�5�42 l  v y4�3�24 =   v y565 o   v w�1�1 
0 tdl tDL6 m   w x�0�0 �3  �2  3 r   | �787 b   | �9:9 m   | ;; �<<  0: o    ��/�/ 0 td tD8 o      �.�. 0 td tD�5  �4  1 =>= l  � ��-�,�+�-  �,  �+  > ?@? l  � ��*�)�(�*  �)  �(  @ ABA l  � ��'CD�'  C  -- Time variables	   D �EE $ - -   T i m e   v a r i a b l e s 	B FGF l  � ��&HI�&  H  -- Get hour   I �JJ  - -   G e t   h o u rG KLK r   � �MNM n   � �OPO 1   � ��%
�% 
tstrP l  � �Q�$�#Q I  � ��"�!� 
�" .misccurdldt    ��� null�!  �   �$  �#  N o      �� 0 timestr timeStrL RSR r   � �TUT I  � ���V
� .sysooffslong    ��� null�  V �WX
� 
psofW m   � �YY �ZZ  :X �[�
� 
psin[ o   � ��� 0 timestr timeStr�  U o      �� 0 pos  S \]\ r   � �^_^ c   � �`a` n   � �bcb 7  � ��de
� 
cha d m   � ��� e l  � �f��f \   � �ghg o   � ��� 0 pos  h m   � ��� �  �  c o   � ��� 0 timestr timeStra m   � ��
� 
TEXT_ o      �� 0 h  ] iji r   � �klk c   � �mnm n   � �opo 7 � ��qr
� 
cha q l  � �s��s [   � �tut o   � ��� 0 pos  u m   � ��
�
 �  �  r  ;   � �p o   � ��	�	 0 timestr timeStrn m   � ��
� 
TEXTl o      �� 0 timestr timeStrj vwv l  � �����  �  �  w xyx l  � ��z{�  z  -- Get minute   { �||  - -   G e t   m i n u t ey }~} r   � �� I  � ����
� .sysooffslong    ��� null�  � � ��
�  
psof� m   � ��� ���  :� �����
�� 
psin� o   � ����� 0 timestr timeStr��  � o      ���� 0 pos  ~ ��� r   � ���� c   � ���� n   � ���� 7  � �����
�� 
cha � m   � ����� � l  � ������� \   � ���� o   � ����� 0 pos  � m   � ����� ��  ��  � o   � ����� 0 timestr timeStr� m   � ���
�� 
TEXT� o      ���� 0 m  � ��� r   �	��� c   ���� n   ���� 7 �����
�� 
cha � l  ������� [   ���� o   � ���� 0 pos  � m   ���� ��  ��  �  ;  � o   � ����� 0 timestr timeStr� m  ��
�� 
TEXT� o      ���� 0 timestr timeStr� ��� l 

��������  ��  ��  � ��� l 

������  �  -- Get AM or PM   � ���  - -   G e t   A M   o r   P M� ��� r  
��� I 
�����
�� .sysooffslong    ��� null��  � ����
�� 
psof� m  �� ���   � �����
�� 
psin� o  ���� 0 timestr timeStr��  � o      ���� 0 pos  � ��� r  0��� c  .��� n  ,��� 7,����
�� 
cha � l %)������ [  %)��� o  &'���� 0 pos  � m  '(���� ��  ��  �  ;  *+� o  ���� 0 timestr timeStr� m  ,-��
�� 
TEXT� o      ���� 0 s  � ��� l 11��������  ��  ��  � ��� l 11��������  ��  ��  � ��� l 11������  � % -- Create timestamp folder name   � ��� > - -   C r e a t e   t i m e s t a m p   f o l d e r   n a m e� ��� r  1B��� b  1@��� b  1>��� b  1<��� b  1:��� b  16��� b  14��� o  12���� 0 ty tY� o  23���� 0 tm tM� o  45���� 0 td tD� m  69�� ���  _� o  :;���� 0 h  � o  <=���� 0 m  � o  >?���� 0 s  � o      ���� 0 	timestamp  � ��� l CC��������  ��  ��  � ���� L  CE�� o  CD���� 0 	timestamp  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   $ '��� I      ������� 0 comparesizes compareSizes� ��� o      ���� 
0 source  � ���� o      ���� 0 destination  ��  ��  � l    ^���� k     ^�� ��� r     ��� m     ��
�� boovtrue� o      ���� 0 fit  � ��� r    
��� 4    ���
�� 
psxf� o    ���� 0 destination  � o      ���� 0 destination  � ��� l   ��������  ��  ��  � � � O    L k    K  r     I   ����
�� .sysoexecTEXT���     TEXT b    	
	 b     m     �  d u   - m s   o    ���� 
0 source  
 m     �    |   c u t   - f   1��   o      ���� 0 
foldersize 
folderSize  r    ( ^    & l   $���� I   $����
�� .sysorondlong        doub ]      l   ���� ^     o    ���� 0 
foldersize 
folderSize m    ���� ��  ��   m    ���� d��  ��  ��   m   $ %���� d o      ���� 0 
foldersize 
folderSize  l  ) )��������  ��  ��    !  r   ) 7"#" ^   ) 5$%$ ^   ) 3&'& ^   ) 1()( l  ) /*����* l  ) /+����+ n   ) /,-, 1   - /��
�� 
frsp- 4   ) -��.
�� 
cdis. o   + ,���� 0 destination  ��  ��  ��  ��  ) m   / 0���� ' m   1 2���� % m   3 4���� # o      ���� 0 	freespace 	freeSpace! /0/ r   8 C121 ^   8 A343 l  8 ?5����5 I  8 ?��6��
�� .sysorondlong        doub6 l  8 ;7����7 ]   8 ;898 o   8 9���� 0 	freespace 	freeSpace9 m   9 :���� d��  ��  ��  ��  ��  4 m   ? @���� d2 o      ���� 0 	freespace 	freeSpace0 :;: l  D D�������  ��  �  ; <�~< I  D K�}=�|
�} .ascrcmnt****      � ****= l  D G>�{�z> b   D G?@? o   D E�y�y 0 
foldersize 
folderSize@ o   E F�x�x 0 	freespace 	freeSpace�{  �z  �|  �~   m    AA�                                                                                  MACS  alis    h  W00721ML                   ���H+     *
Finder.app                                                      \�u�        ����  	                CoreServices    ���      �u��       *        2W00721ML:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    W 0 0 7 2 1 M L  &System/Library/CoreServices/Finder.app  / ��    BCB l  M M�w�v�u�w  �v  �u  C DED Z   M [FG�t�sF H   M QHH l  M PI�r�qI A   M PJKJ o   M N�p�p 0 
foldersize 
folderSizeK o   N O�o�o 0 	freespace 	freeSpace�r  �q  G r   T WLML m   T U�n
�n boovfalsM o      �m�m 0 fit  �t  �s  E NON l  \ \�l�k�j�l  �k  �j  O P�iP L   \ ^QQ o   \ ]�h�h 0 fit  �i  �  (string, string)   � �RR   ( s t r i n g ,   s t r i n g )� STS l     �g�f�e�g  �f  �e  T UVU l     �d�c�b�d  �c  �b  V WXW l     �a�`�_�a  �`  �_  X YZY l     �^�]�\�^  �]  �\  Z [\[ l     �[�Z�Y�[  �Z  �Y  \ ]^] i   ( +_`_ I      �Xa�W�X 0 	stopwatch  a b�Vb o      �U�U 0 mode  �V  �W  ` k     3cc ded q      ff �T�S�T 0 x  �S  e ghg Z     1ijk�Ri l    l�Q�Pl =     mnm o     �O�O 0 mode  n m    oo �pp 
 s t a r t�Q  �P  j k    qq rsr r    tut I    �N�M�L�N 0 gettimestamp getTimestamp�M  �L  u o      �K�K 0 x  s v�Jv I   �Iw�H
�I .ascrcmnt****      � ****w l   x�G�Fx b    yzy m    {{ �||   b a c k u p   s t a r t e d :  z o    �E�E 0 x  �G  �F  �H  �J  k }~} l   �D�C =    ��� o    �B�B 0 mode  � m    �� ���  f i n i s h�D  �C  ~ ��A� k    -�� ��� r    %��� I    #�@�?�>�@ 0 gettimestamp getTimestamp�?  �>  � o      �=�= 0 x  � ��<� I  & -�;��:
�; .ascrcmnt****      � ****� l  & )��9�8� b   & )��� m   & '�� ��� " b a c k u p   f i n i s h e d :  � o   ' (�7�7 0 x  �9  �8  �:  �<  �A  �R  h ��6� l  2 2�5�4�3�5  �4  �3  �6  ^ ��� l     �2�1�0�2  �1  �0  � ��� l     �/���/  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �.���.  � � �-----------------------------------------------------------------------------------------------------------------------------------------------   � ��� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -� ��� l     �-�,�+�-  �,  �+  � ��� l     �*�)�(�*  �)  �(  � ��� l     �'�&�%�'  �&  �%  � ��� l     �$�#�"�$  �#  �"  � ��� l     �!� ��!  �   �  � ��� l     ����  �  plistInit()   � ���  p l i s t I n i t ( )� ��� l     ����  �  �  � ��� i   , /��� I      ���� 0 runonce runOnce�  �  � I     ���� 0 	plistinit 	plistInit�  �  � ��� l     ����  �  �  � ��� l   ���� I    ���� 0 runonce runOnce�  �  �  �  � ��� l     ���
�  �  �
  � ��� l     �	���	  �  �  � ��� i   0 3��� I     ���
� .miscidlenmbr    ��� null�  �  � k     ^�� ��� Z     [����� l    �� ��� =     ��� o     ���� 0 isbackingup isBackingUp� m    ��
�� boovfals�   ��  � Z    W������� l   ������ A    ��� l   ������ n    ��� 1    ��
�� 
hour� l   ������ I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  � m    ���� ��  ��  � k    S�� ��� r    ��� l   ������ n    ��� 1    ��
�� 
min � l   ������ l   ������ I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  ��  ��  � o      ���� 0 m  � ��� l   ��������  ��  ��  � ��� l   ������  �  if (scheduledTime = )   � ��� * i f   ( s c h e d u l e d T i m e   =   )� ��� l   ��������  ��  ��  � ���� Z    S������ G    '��� l   ������ =    ��� o    ���� 0 m  � m    ���� ��  ��  � l  " %������ =   " %��� o   " #���� 0 m  � m   # $���� -��  ��  � Z   * 9������� l  * -������ =   * -��� o   * +���� 0 scheduledtime scheduledTime� m   + ,���� ��  ��  � I   0 5�������� 0 	plistinit 	plistInit��  ��  ��  ��  � ��� G   < G��� l  < ?������ =   < ?��� o   < =���� 0 m  � m   = >����  ��  ��  � l  B E������ =   B E��� o   B C���� 0 m  � m   C D���� ��  ��  � ���� I   J O�������� 0 	plistinit 	plistInit��  ��  ��  ��  ��  ��  ��  �  �  � �	 � l  \ \��������  ��  ��  	  	��	 L   \ ^		 m   \ ]���� <��  �       ��									
							��  	 ������������������������������ 0 	plistinit 	plistInit�� 0 diskinit diskInit�� 0 freespaceinit freeSpaceInit�� 0 
backupinit 
backupInit�� 0 tidyup tidyUp�� 
0 backup  �� 0 itexists itExists�� (0 getfoldertimestamp getFolderTimestamp�� 0 gettimestamp getTimestamp�� 0 comparesizes compareSizes�� 0 	stopwatch  �� 0 runonce runOnce
�� .miscidlenmbr    ��� null
�� .aevtoappnull  �   � ****	 �� }����		���� 0 	plistinit 	plistInit��  ��  	 ���������������� 0 
foldername 
folderName�� 0 
backupdisk 
backupDisk�� 0 
backuptime 
backupTime�� 0 thecontents theContents�� 0 thevalue theValue�� 0 backuptimes backupTimes�� 0 selectedtime selectedTime	 , ����� ���������������>�� ��� ����� � � �
����$��/������������������T��ds�������� 	0 plist  �� 0 itexists itExists
�� 
plif
�� 
pcnt
�� 
valL��  0 foldertobackup FolderToBackup�� 0 backupdrive BackupDrive�� 0 scheduledtime scheduledTime
�� .ascrcmnt****      � ****
�� 
prmp
�� .sysostflalis    ��� null
�� 
TEXT
�� 
psxp
�� .gtqpchltns    @   @ ns  �� �� �� <
�� 
kocl
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null
�� 
plii
�� 
insh
�� 
kind�� �� 0 sourcefolder sourceFolder�� "0 destinationdisk destinationDisk�� 0 diskinit diskInit��vjE�O*��l+ e  4� ,*��/�,E�O��,E�O��,E�O��,E�O��,E�O���mvj 
UY� �*��l E�O*��l a &E�O�a ,E�O�a ,E�Oa a a mvE�O��a l kva &E�O�a   
a E�Y #�a   
a E�Y �a   
a E�Y hO���mvj 
UO� �*a �a a �la   ! y*a a "a #*6a a $a a a %�a &a & !O*a a "a #*6a a $a a a '�a &a & !O*a a "a #*6a a $a a a (�a &a & !UUO�E` )O�E` *O�E�O*j+ +	 �������		��� 0 diskinit diskInit��  ��  	  	 ��~�}��|��{��z�y�x�~ "0 destinationdisk destinationDisk�} 0 itexists itExists�| 0 freespaceinit freeSpaceInit
�{ 
btns
�z 
dflt�y 
�x .sysodlogaskr        TEXT� !*��l+ e  *�k+ Y ���kv�k� 
	 �w��v�u		�t�w 0 freespaceinit freeSpaceInit�v �s	�s 	  �r�r 	0 again  �u  	 �q�p�q 	0 again  �p 
0 reply1  	 �o�n�m�l��k���j�i�h�g��f�e�o 0 sourcefolder sourceFolder�n "0 destinationdisk destinationDisk�m 0 comparesizes compareSizes�l 0 
backupinit 
backupInit
�k 
btns
�j 
dflt�i 
�h .sysodlogaskr        TEXT
�g 
bhit�f 0 tidyup tidyUp
�e .sysonotfnull��� ��� TEXT�t ^*��l+ e  
*j+ Y K�f  ����lv�l� 
�,E�Y �e  ����lv�l� 
�,E�Y hO��  
*j+ Y 	a j 	 �d&�c�b		�a�d 0 
backupinit 
backupInit�c  �b  	  	 �`�_�^>�]LQ�\_�[�Z�Y�X�W]�V�U�Tn�S����R���Q�P
�` 
psxf�_ "0 destinationdisk destinationDisk�^ 	0 drive  
�] .sysonotfnull��� ��� TEXT�\ 0 itexists itExists
�[ 
kocl
�Z 
cfol
�Y 
insh
�X 
prdt
�W 
pnam�V 
�U .corecrel****      � null
�T 
cdis�S 0 backupfolder backupFolder�R 0 initialbackup initialBackup�Q 0 latestfolder latestFolder�P 
0 backup  �a �*��/E�O�j O*���%l+ f  � *�������l� UY hO� *a �/�a /E` OPUO*a �a %l+ f  � *���_ ��a l� UY fE` O� *a �/�a /�a /E` OPUO*j+ 	 �O��N�M		�L�O 0 tidyup tidyUp�N  �M  	 �K�J�I�H�G�F�K 0 creationdates creationDates�J 0 theoldestdate theOldestDate�I 0 j  �H 0 i  �G 0 thisdate thisDate�F 
0 reply2  	 M�E�D�C�B�A�@%�?,/�>�=�<�;:�:�9?�8G�7K�E 0 backupfolder backupFolder
�D 
cobj
�C 
ascd
�B .corecnte****       ****
�A .ascrcmnt****      � ****
�@ .coredeloobj        obj 
�? 
btns
�> 
dflt�= 
�< .sysodlogaskr        TEXT
�; 
bhit
�: 
trsh
�9 .fndremptnull��� ��� obj 
�8 .sysonotfnull��� ��� TEXT�7 0 freespaceinit freeSpaceInit�L �� ���-�,E�O��k/E�OkE�O 3k�j kh ��/E�O�� �E�O�E�O��/j Y h[OY��O��/j O����lv�l� �,E�O��  *a ,j Y 	a j O*a k+ Oa j UOP		 �6��5�4		�3�6 
0 backup  �5  �4  	 �2�1�0�/�.�-�,�+�2 0 t  �1 0 x  �0 "0 containerfolder containerFolder�/ 0 
foldername 
folderName�. 0 d  �- 0 c  �, 0 oldestfolder oldestFolder�+ 0 todelete toDelete	 4�*�)��(8�'�&�%�$�#�"�!� ���������Y��jly{}�����������������%->�* (0 getfoldertimestamp getFolderTimestamp�) 0 isbackingup isBackingUp�( 0 	stopwatch  
�' 
psxf�& 0 sourcefolder sourceFolder
�% 
TEXT
�$ 
cfol
�# 
pnam�" 0 initialbackup initialBackup
�! 
kocl
�  
insh� 0 backupfolder backupFolder
� 
prdt� 
� .corecrel****      � null� (0 activesourcefolder activeSourceFolder
� 
cdis� 	0 drive  � (0 activebackupfolder activeBackupFolder
� .sysonotfnull��� ��� TEXT� "0 destinationdisk destinationDisk
� .sysoexecTEXT���     TEXT
� .sysodelanull��� ��� nmbr
� .ascrcmnt****      � ****
� 
alis
� 
cobj
� 
psxp�3�*j+  E�OeE�O*�k+ O�i*��/�&E�O*�/�,E�O�f  O*������l� O*��/�&E` O*�_ /E` O*a _ /�a /�/E` O*���_ ��l� Y hO�e  Da j O_ a %�%a %�&E�Oa �%a %�%a %j OfE�Oa j Okj  Y ��f  �_ a !%�%a "%�%a #%�&E�O_ a $%�%a %%�&E�Oa &�%j 'Oa (�%a )%�%a *%�%a +%j OfE�O*�/a ,&a --jv  =��/�&E�O�a .,�&E�Oa /�%j 'Oa 0�%E�O�j Oa 1j Olj  Y a 2j Okj  OPY hUO*a 3k+ 	
 �X��		 �� 0 itexists itExists� �	!� 	!  �
�	�
 0 
objecttype 
objectType�	 
0 object  �  	 ��� 0 
objecttype 
objectType� 
0 object  	  �h��v���
� 
cdis
� .coredoexnull���     ****
� 
file
� 
cfol� X� T��  *�/j  eY fY 9��  *�/j  eY fY ��  *�/j  eY fY hU	 ���� 	"	#��� (0 getfoldertimestamp getFolderTimestamp�  �   	" �������������������������������� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  	# ����������������������������������1��������\���
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� 
psof
�� 
psin�� 
�� .sysooffslong    ��� null
�� 
cha ��J*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Z�k\62�&E�Oa �%�%�%a %�%�%�%E�O�	 �������	$	%���� 0 gettimestamp getTimestamp��  ��  	$ �������������������������������� 0 y  �� 0 m  �� 0 d  �� 0 t  �� 0 ty tY�� 0 tm tM�� 0 td tD�� 0 tt tT�� 
0 tml tML�� 
0 tdl tDL�� 0 timestr timeStr�� 0 pos  �� 0 h  �� 0 s  �� 0 	timestamp  	% ������������������������������,;����Y�����������
�� 
Krtn
�� 
year�� 0 y  
�� 
mnth�� 0 m  
�� 
day �� 0 d  
�� 
time�� 0 t  �� 
�� .misccurdldt    ��� null
�� 
long
�� 
TEXT
�� .corecnte****       ****
�� 
nmbr
�� 
tstr
�� 
psof
�� 
psin�� 
�� .sysooffslong    ��� null
�� 
cha ��F*����������l 
E[�,E�Z[�,E�Z[�,E�Z[�,E�ZO��&�&E�O��&�&E�O��&�&E�O��&�&E�O��&�&E�O�j �&E�O�j �&E�O�k  
�%E�Y hO�k  a �%E�Y hO*j 
a ,E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Zk\Z�k2�&E�O�[a \[Z�k\62�&E�O*a a a �a  E�O�[a \[Z�k\62�&E�O��%�%a %�%�%�%E�O�	 �������	&	'���� 0 comparesizes compareSizes�� ��	(�� 	(  ������ 
0 source  �� 0 destination  ��  	& ������������ 
0 source  �� 0 destination  �� 0 fit  �� 0 
foldersize 
folderSize�� 0 	freespace 	freeSpace	' ��A��������������
�� 
psxf
�� .sysoexecTEXT���     TEXT�� �� d
�� .sysorondlong        doub
�� 
cdis
�� 
frsp
�� .ascrcmnt****      � ****�� _eE�O*�/E�O� >�%�%j E�O��!� j �!E�O*�/�,�!�!�!E�O�� j �!E�O��%j 
UO�� fE�Y hO�	 ��`����	)	*���� 0 	stopwatch  �� ��	+�� 	+  ���� 0 mode  ��  	) ������ 0 mode  �� 0 x  	* o��{������ 0 gettimestamp getTimestamp
�� .ascrcmnt****      � ****�� 4��  *j+ E�O�%j Y ��  *j+ E�O�%j Y hOP	 �������	,	-���� 0 runonce runOnce��  ��  	,  	- ���� 0 	plistinit 	plistInit�� *j+  	 �������	.	/��
�� .miscidlenmbr    ��� null��  ��  	. ���� 0 m  	/ ������������������������� 0 isbackingup isBackingUp
�� .misccurdldt    ��� null
�� 
hour�� 
�� 
min �� �� -
�� 
bool�� 0 scheduledtime scheduledTime�� 0 	plistinit 	plistInit�� � <�� _�f  V*j �,� F*j �,E�O�� 
 �� �& ��  
*j+ 	Y hY �j 
 �� �& 
*j+ 	Y hY hY hO�	 �~	0�}�|	1	2�{
�~ .aevtoappnull  �   � ****	0 k     	3	3  F	4	4  U	5	5  Z	6	6 ��z�z  �}  �|  	1  	2 
�y�x�w�v�u S�t�s�r�q
�y afdrdlib
�x 
from
�w fldmfldu
�v .earsffdralis        afdr
�u 
psxp�t 	0 plist  �s 0 initialbackup initialBackup�r 0 isbackingup isBackingUp�q 0 runonce runOnce�{ ���l �,�%E�OeE�OfE�O*j+ 	ascr  ��ޭ