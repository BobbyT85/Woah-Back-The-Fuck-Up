FasdUAS 1.101.10   ��   ��    k             x     ��  ��    1      ��
�� 
ascr  �� 	��
�� 
minv 	 m       
 
 �    2 . 4��        x    �� ����    2  	 ��
�� 
osax��        x     �� ����    4    �� 
�� 
frmk  m       �    F o u n d a t i o n��        x     -�� ����    4   " &�� 
�� 
frmk  m   $ %   �    A p p K i t��        l     ��������  ��  ��        j   - /�� �� 0 
statusitem 
StatusItem  m   - .��
�� 
msng     !   l     ��������  ��  ��   !  " # " j   0 2�� $�� 0 
thedisplay 
theDisplay $ m   0 1 % % � & &   #  ' ( ' j   3 9�� )�� 0 defaults   ) 4   3 8�� *
�� 
pcls * m   5 6 + + � , ,  N S U s e r D e f a u l t s (  - . - j   : @�� /�� $0 internalmenuitem internalMenuItem / 4   : ?�� 0
�� 
pcls 0 m   < = 1 1 � 2 2  N S M e n u I t e m .  3 4 3 j   A I�� 5�� $0 externalmenuitem externalMenuItem 5 4   A H�� 6
�� 
pcls 6 m   C F 7 7 � 8 8  N S M e n u I t e m 4  9 : 9 j   J R�� ;�� 0 newmenu newMenu ; 4   J Q�� <
�� 
pcls < m   L O = = � > >  N S M e n u :  ? @ ? l     ��������  ��  ��   @  A B A j   S \�� C�� 0 thelist theList C J   S [ D D  E F E m   S V G G � H H  B a c k u p   n o w F  I�� I m   V Y J J � K K  Q u i t��   B  L M L l     ��������  ��  ��   M  N O N l     ��������  ��  ��   O  P Q P l     �� R S��   R h b check we are running in foreground - YOU MUST RUN AS APPLICATION. to be thread safe and not crash    S � T T �   c h e c k   w e   a r e   r u n n i n g   i n   f o r e g r o u n d   -   Y O U   M U S T   R U N   A S   A P P L I C A T I O N .   t o   b e   t h r e a d   s a f e   a n d   n o t   c r a s h Q  U V U l     W���� W Z      X Y���� X H     
 Z Z c     	 [ \ [ l     ]���� ] n     ^ _ ^ I    �������� 0 ismainthread isMainThread��  ��   _ n     ` a ` o    ���� 0 nsthread NSThread a m     ��
�� misccura��  ��   \ m    ��
�� 
bool Y k     b b  c d c l   �� e f��   e b \display alert "This script must be run from the main thread." buttons {"Cancel"} as critical    f � g g � d i s p l a y   a l e r t   " T h i s   s c r i p t   m u s t   b e   r u n   f r o m   t h e   m a i n   t h r e a d . "   b u t t o n s   { " C a n c e l " }   a s   c r i t i c a l d  h�� h l   �� i j��   i  error number -128    j � k k " e r r o r   n u m b e r   - 1 2 8��  ��  ��  ��  ��   V  l m l l     ��������  ��  ��   m  n o n i   ] ` p q p I      �� r���� $0 menuneedsupdate_ menuNeedsUpdate_ r  s�� s l      t���� t m      ��
�� 
cmnu��  ��  ��  ��   q k      u u  v w v l     �� x y��   x J D NSMenu's delegates method, when the menu is clicked this is called.    y � z z �   N S M e n u ' s   d e l e g a t e s   m e t h o d ,   w h e n   t h e   m e n u   i s   c l i c k e d   t h i s   i s   c a l l e d . w  { | { l     �� } ~��   } j d We use it here to call the method makeMenus(). Which removes the old menuItems and builds new ones.    ~ �   �   W e   u s e   i t   h e r e   t o   c a l l   t h e   m e t h o d   m a k e M e n u s ( ) .   W h i c h   r e m o v e s   t h e   o l d   m e n u I t e m s   a n d   b u i l d s   n e w   o n e s . |  � � � l     �� � ���   � < 6 This means the menu items can be changed dynamically.    � � � � l   T h i s   m e a n s   t h e   m e n u   i t e m s   c a n   b e   c h a n g e d   d y n a m i c a l l y . �  ��� � n     � � � I    �������� 0 	makemenus 	makeMenus��  ��   �  f     ��   o  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � i   a d � � � I      �������� 0 	makemenus 	makeMenus��  ��   � k     v � �  � � � l    	 � � � � n    	 � � � I    	��������  0 removeallitems removeAllItems��  ��   � o     ���� 0 newmenu newMenu � !  remove existing menu items    � � � � 6   r e m o v e   e x i s t i n g   m e n u   i t e m s �  � � � l  
 
��������  ��  ��   �  ��� � Y   
 v ��� � ��� � k    q � �  � � � r    ' � � � n    % � � � 4   " %�� �
�� 
cobj � o   # $���� 0 i   � o    "���� 0 thelist theList � o      ���� 0 	this_item   �  � � � l  ( (��������  ��  ��   �  � � � Z   ( _ � � ��� � l  ( - ����� � =   ( - � � � l  ( + ����� � c   ( + � � � o   ( )���� 0 	this_item   � m   ) *��
�� 
TEXT��  ��   � m   + , � � � � �  B a c k u p   n o w��  ��   � r   0 @ � � � l  0 > ����� � n  0 > � � � I   7 >�� ����� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_ �  � � � o   7 8���� 0 	this_item   �  � � � m   8 9 � � � � �  b a c k u p H a n d l e r : �  ��� � m   9 : � � � � �  ��  ��   � n  0 7 � � � I   3 7�������� 	0 alloc  ��  ��   � n  0 3 � � � o   1 3���� 0 
nsmenuitem 
NSMenuItem � m   0 1��
�� misccura��  ��   � o      ���� 0 thismenuitem thisMenuItem �  � � � l  C H ���~ � =   C H � � � l  C F ��}�| � c   C F � � � o   C D�{�{ 0 	this_item   � m   D E�z
�z 
TEXT�}  �|   � m   F G � � � � �  Q u i t�  �~   �  ��y � r   K [ � � � l  K Y ��x�w � n  K Y � � � I   R Y�v ��u�v J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_ �  � � � o   R S�t�t 0 	this_item   �  � � � m   S T � � � � �  q u i t H a n d l e r : �  ��s � m   T U � � � � �  �s  �u   � n  K R � � � I   N R�r�q�p�r 	0 alloc  �q  �p   � n  K N � � � o   L N�o�o 0 
nsmenuitem 
NSMenuItem � m   K L�n
�n misccura�x  �w   � o      �m�m 0 thismenuitem thisMenuItem�y  ��   �  � � � l  ` `�l�k�j�l  �k  �j   �  � � � l  ` j ��i�h � n  ` j � � � I   e j�g ��f�g 0 additem_ addItem_ �  ��e � o   e f�d�d 0 thismenuitem thisMenuItem�e  �f   � o   ` e�c�c 0 newmenu newMenu�i  �h   �  � � � l  k k�b�a�`�b  �a  �`   �  ��_ � l  k q � � � � l  k q ��^�] � n  k q � � � I   l q�\ ��[�\ 0 
settarget_ 
setTarget_ �  �Z   f   l m�Z  �[   � o   k l�Y�Y 0 thismenuitem thisMenuItem�^  �]   � * $ required for enabling the menu item    � � H   r e q u i r e d   f o r   e n a b l i n g   t h e   m e n u   i t e m�_  �� 0 i   � m    �X�X  � n     m    �W
�W 
nmbr n    2   �V
�V 
cobj o    �U�U 0 thelist theList��  ��   �  l     �T�S�R�T  �S  �R   	 i   e h

 I      �Q�P�Q  0 backuphandler_ backupHandler_ �O o      �N�N 
0 sender  �O  �P   I    �M�L
�M .sysodlogaskr        TEXT m      �  b a c k u p�L  	  l     �K�J�I�K  �J  �I    i   i l I      �H�G�H 0 quithandler_ quitHandler_ �F o      �E�E 
0 sender  �F  �G   I    �D�C
�D .aevtquitnull��� ��� null m     Z                                                                                      @ alis    �  MINISTATION                ��7H+   )��menu bar.app                                                    )����!        ����  	                Desktop     ��7      ��!     )�� )�� )�� '�{ '�z  mMINISTATION:Backups: W00721ML - C02RT1SHFVH6: backup_201702021_164055: robert.tesalona: Desktop: menu bar.app     m e n u   b a r . a p p    M I N I S T A T I O N  ]/Backups/W00721ML - C02RT1SHFVH6/backup_201702021_164055/robert.tesalona/Desktop/menu bar.app   /Volumes/MINISTATION��  �C    l     �B�A�@�B  �A  �@    l     �?�>�=�?  �>  �=     l     �<�;�:�<  �;  �:    !"! l     �9�8�7�9  �8  �7  " #$# l     �6�5�4�6  �5  �4  $ %&% l     �3'(�3  '   create an NSStatusBar   ( �)) ,   c r e a t e   a n   N S S t a t u s B a r& *+* i   m p,-, I      �2�1�0�2 0 makestatusbar makeStatusBar�1  �0  - k     r.. /0/ r     121 n    343 o    �/�/ "0 systemstatusbar systemStatusBar4 n    565 o    �.�. 0 nsstatusbar NSStatusBar6 m     �-
�- misccura2 o      �,�, 0 bar  0 787 l   �+�*�)�+  �*  �)  8 9:9 r    ;<; n   =>= I   	 �(?�'�( .0 statusitemwithlength_ statusItemWithLength_? @�&@ m   	 
AA ��      �&  �'  > o    	�%�% 0 bar  < o      �$�$ 0 
statusitem 
StatusItem: BCB l   �#�"�!�#  �"  �!  C DED l   � FG�   F , & set up the initial NSStatusBars title   G �HH L   s e t   u p   t h e   i n i t i a l   N S S t a t u s B a r s   t i t l eE IJI l   ����  �  �  J KLK r    #MNM 4    !�O
� 
alisO l    P��P b     QRQ l   S��S I   �TU
� .earsffdralis        afdrT  f    U �V�
� 
rtypV m    �
� 
ctxt�  �  �  R m    WW �XX < C o n t e n t s : R e s o u r c e s : m e n u b a r . p n g�  �  N o      �� 0 	imagepath 	imagePathL YZY r   $ )[\[ n   $ ']^] 1   % '�
� 
psxp^ o   $ %�� 0 	imagepath 	imagePath\ o      �� 0 	imagepath 	imagePathZ _`_ l  * *����  �  �  ` aba l  * *�cd�  c � �set image to current application's NSImage's alloc()'s initWithContentsOfFile:"/Users/robert.tesalona/Desktop/_temp/wbtfu_menubar.png"   d �ee s e t   i m a g e   t o   c u r r e n t   a p p l i c a t i o n ' s   N S I m a g e ' s   a l l o c ( ) ' s   i n i t W i t h C o n t e n t s O f F i l e : " / U s e r s / r o b e r t . t e s a l o n a / D e s k t o p / _ t e m p / w b t f u _ m e n u b a r . p n g "b fgf r   * 8hih n  * 6jkj I   1 6�l�
� 20 initwithcontentsoffile_ initWithContentsOfFile_l m�	m o   1 2�� 0 	imagepath 	imagePath�	  �
  k n  * 1non I   - 1���� 	0 alloc  �  �  o n  * -pqp o   + -�� 0 nsimage NSImageq m   * +�
� misccurai o      �� 	0 image  g rsr n  9 Ctut I   > C�v� � 0 	setimage_ 	setImage_v w��w o   > ?���� 	0 image  ��  �   u o   9 >���� 0 
statusitem 
StatusItems xyx l  D D��������  ��  ��  y z{z l  D D��|}��  | # StatusItem's setTitle:"WBTFU"   } �~~ : S t a t u s I t e m ' s   s e t T i t l e : " W B T F U "{ � l  D D��������  ��  ��  � ��� l  D D������  � 1 + set up the initial NSMenu of the statusbar   � ��� V   s e t   u p   t h e   i n i t i a l   N S M e n u   o f   t h e   s t a t u s b a r� ��� r   D X��� n  D R��� I   K R�������  0 initwithtitle_ initWithTitle_� ���� m   K N�� ���  C u s t o m��  ��  � n  D K��� I   G K�������� 	0 alloc  ��  ��  � n  D G��� o   E G���� 0 nsmenu NSMenu� m   D E��
�� misccura� o      ���� 0 newmenu newMenu� ��� l  Y Y��������  ��  ��  � ��� n  Y c��� I   ^ c������� 0 setdelegate_ setDelegate_� ����  f   ^ _��  ��  � o   Y ^���� 0 newmenu newMenu� ��� l  d d������  � � � Required delegation for when the Status bar Menu is clicked  the menu will use the delegates method (menuNeedsUpdate:(menu)) to run dynamically update.   � ���0   R e q u i r e d   d e l e g a t i o n   f o r   w h e n   t h e   S t a t u s   b a r   M e n u   i s   c l i c k e d     t h e   m e n u   w i l l   u s e   t h e   d e l e g a t e s   m e t h o d   ( m e n u N e e d s U p d a t e : ( m e n u ) )   t o   r u n   d y n a m i c a l l y   u p d a t e .� ��� l  d d��������  ��  ��  � ���� n  d r��� I   i r������� 0 setmenu_ setMenu_� ���� o   i n���� 0 newmenu newMenu��  ��  � o   d i���� 0 
statusitem 
StatusItem��  + ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ���� l   ������ n   ��� I    �������� 0 makestatusbar makeStatusBar��  ��  �  f    ��  ��  ��       ������ %�������������  � ����������������������������
�� 
pimr�� 0 
statusitem 
StatusItem�� 0 
thedisplay 
theDisplay�� 0 defaults  �� $0 internalmenuitem internalMenuItem�� $0 externalmenuitem externalMenuItem�� 0 newmenu newMenu�� 0 thelist theList�� $0 menuneedsupdate_ menuNeedsUpdate_�� 0 	makemenus 	makeMenus��  0 backuphandler_ backupHandler_�� 0 quithandler_ quitHandler_�� 0 makestatusbar makeStatusBar
�� .aevtoappnull  �   � ****� ����� �  ����� �� 
��
�� 
vers��  � �����
�� 
cobj� ��   ��
�� 
osax��  � �����
�� 
cobj� ��   �� 
�� 
frmk��  � �����
�� 
cobj� ��   �� 
�� 
frmk��  
�� 
msng� �� �����
�� misccura
�� 
pcls� ���  N S U s e r D e f a u l t s� �� �����
�� misccura
�� 
pcls� ���  N S M e n u I t e m� �� �����
�� misccura
�� 
pcls� ���  N S M e n u I t e m� �� �����
�� misccura
�� 
pcls� ���  N S M e n u� ����� �   G J� �� q���������� $0 menuneedsupdate_ menuNeedsUpdate_�� ����� �  ��
�� 
cmnu��  �  � ���� 0 	makemenus 	makeMenus�� )j+  � �� ����������� 0 	makemenus 	makeMenus��  ��  � �������� 0 i  �� 0 	this_item  �� 0 thismenuitem thisMenuItem� �������� ������� � ��� � � �������  0 removeallitems removeAllItems
�� 
cobj
�� 
nmbr
�� 
TEXT
�� misccura�� 0 
nsmenuitem 
NSMenuItem�� 	0 alloc  �� J0 #initwithtitle_action_keyequivalent_ #initWithTitle_action_keyEquivalent_�� 0 additem_ addItem_�� 0 
settarget_ 
setTarget_�� wb  j+  O kkb  �-�,Ekh  b  �/E�O��&�  ��,j+ ���m+ 
E�Y ��&�  ��,j+ ���m+ 
E�Y hOb  �k+ O�)k+ [OY��� ������������  0 backuphandler_ backupHandler_�� ����� �  ���� 
0 sender  ��  � ���� 
0 sender  � ��
�� .sysodlogaskr        TEXT�� �j � ����~���}�� 0 quithandler_ quitHandler_� �|��| �  �{�{ 
0 sender  �~  � �z�z 
0 sender  � �y
�y .aevtquitnull��� ��� null�} �j � �x-�w�v���u�x 0 makestatusbar makeStatusBar�w  �v  � �t�s�r�t 0 bar  �s 0 	imagepath 	imagePath�r 	0 image  � �q�p�oA�n�m�l�k�jW�i�h�g�f�e�d��c�b�a
�q misccura�p 0 nsstatusbar NSStatusBar�o "0 systemstatusbar systemStatusBar�n .0 statusitemwithlength_ statusItemWithLength_
�m 
alis
�l 
rtyp
�k 
ctxt
�j .earsffdralis        afdr
�i 
psxp�h 0 nsimage NSImage�g 	0 alloc  �f 20 initwithcontentsoffile_ initWithContentsOfFile_�e 0 	setimage_ 	setImage_�d 0 nsmenu NSMenu�c  0 initwithtitle_ initWithTitle_�b 0 setdelegate_ setDelegate_�a 0 setmenu_ setMenu_�u s��,�,E�O��k+ Ec  O*�)��l �%/E�O��,E�O��,j+ �k+ E�Ob  �k+ O��,j+ a k+ Ec  Ob  )k+ Ob  b  k+ � �`��_�^���]
�` .aevtoappnull  �   � ****� k     ��  U�� ��\�\  �_  �^  �  � �[�Z�Y�X�W
�[ misccura�Z 0 nsthread NSThread�Y 0 ismainthread isMainThread
�X 
bool�W 0 makestatusbar makeStatusBar�] ��,j+ �& hY hO)j+ ascr  ��ޭ